package cs_ipc;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.junit.Test;

import com.util.charging.Constant.CouponType;
import com.util.charging.Constant.UseCarMode;
import com.util.charging.FeeException.DateParamParseException;
import com.util.charging.FeeException.EndTimeEarlierThanStartTimeException;
import com.util.charging.FeeException.NoSuchCouponTypeException;
import com.util.charging.FeeException.ParamMissingException;
import com.util.charging.FeeException.NoSuchUseCarModeException;
import com.util.charging.calculate.FeesAlg;
import com.util.charging.calculate.FeesAlgWithCoupon;

public class TestCalcuate {

	@Test
	public void test() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startTime;
		Date endTime;
		try {
			startTime = sdf.parse("2017-07-18 10:20:00");
			endTime = sdf.parse("2017-07-19 10:20:01");
		} catch (ParseException e) {
			throw new DateParamParseException("时间参数有误！");
		}
		
		Map<String, Integer> map;
		try {
			map = FeesAlg.calculateFees(UseCarMode.NORMAL_MODE, startTime, endTime, 1, 300, 1000, CouponType.COUPON_NORMAL, 
										false, 15, 120, 0, true);
			Integer totalPayment = map.get("totalPayment");
			Integer accountPayment = map.get("accountPayment");
			Integer cashPayment = map.get("cashPayment");
			
			System.out.println("\n应付总金额：" + totalPayment + "分，账户支付：" + accountPayment + "分，现金支付：" + cashPayment);
		} catch (ParamMissingException e) {
			System.out.println(e.getMessage());
		} catch (EndTimeEarlierThanStartTimeException e) {
			System.out.println(e.getMessage());
		} catch (NoSuchUseCarModeException e){
			System.out.println(e.getMessage());
		} catch (NoSuchCouponTypeException e){
			System.out.println(e.getMessage());
		}
	}
	
	@Test
	public void testCalcuateWithCoupon(){
		Map<String, Integer> map = FeesAlgWithCoupon.calculateFeesWithCoupon(4, 3, 2, CouponType.COUPON_NORMAL, 5, false, 0, true);
		Integer feesWithCoupon = map.get("cashPayment");
		System.out.println(feesWithCoupon);
	}

}











