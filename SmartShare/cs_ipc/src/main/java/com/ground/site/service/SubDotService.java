package com.ground.site.service;

import java.util.List;

import com.ground.site.entity.SubDot;

public interface SubDotService {
	
	public List<SubDot> getSubDotList(SubDot subDot);

}
