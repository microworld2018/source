package com.ground.site.controler;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;



import com.core.controller.BaseController;
import com.ground.site.entity.SubDot;
import com.ground.site.service.SubDotService;
import com.ground.user.entity.User;

@Controller
@RequestMapping("subdot")
public class SubDotControler  extends BaseController<SubDot>   {
	
	@Autowired
	private SubDotService subDotService;
	
	/**获取用户所管理的网点列表
	 * @param request
	 * @return
	 */
	@RequestMapping("/dotList")
	public ModelAndView dotList(HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			modelAndView.setViewName("/manage_car/login");
			return modelAndView;
		}
		SubDot subDot = new SubDot();
		subDot.setSubId(u.getId());
		List<SubDot> list = subDotService.getSubDotList(subDot);
		modelAndView.addObject("sunDot", list);
		modelAndView.setViewName("/manage_car/site_admin");
		return modelAndView;
	}

	
}
