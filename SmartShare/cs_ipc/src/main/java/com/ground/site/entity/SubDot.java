package com.ground.site.entity;

import java.io.Serializable;

/**用户网点关联实体类
 * @author 18833
 *
 */
public class SubDot   implements Serializable{

	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String subId;
	
	private String dotId;
	private String dotName;
	
	private String dotAddr;
	
	private Double dotLat;
	
	private Double dotLon;
	
	private String userName;
	
	private String userPhone;
	
	private Integer isUse;
	
	private Integer count;
	private Integer lase;
	private Integer repair;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getDotId() {
		return dotId;
	}

	public void setDotId(String dotId) {
		this.dotId = dotId;
	}

	public String getDotName() {
		return dotName;
	}

	public void setDotName(String dotName) {
		this.dotName = dotName;
	}

	public String getDotAddr() {
		return dotAddr;
	}

	public void setDotAddr(String dotAddr) {
		this.dotAddr = dotAddr;
	}

	public Double getDotLat() {
		return dotLat;
	}

	public void setDotLat(Double dotLat) {
		this.dotLat = dotLat;
	}

	public Double getDotLon() {
		return dotLon;
	}

	public void setDotLon(Double dotLon) {
		this.dotLon = dotLon;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getLase() {
		return lase;
	}

	public void setLase(Integer lase) {
		this.lase = lase;
	}

	public Integer getRepair() {
		return repair;
	}

	public void setRepair(Integer repair) {
		this.repair = repair;
	}

	public Integer getIsUse() {
		return isUse;
	}

	public void setIsUse(Integer isUse) {
		this.isUse = isUse;
	}
	
	
	
}
