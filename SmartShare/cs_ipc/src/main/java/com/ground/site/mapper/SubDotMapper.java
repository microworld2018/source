package com.ground.site.mapper;

import java.util.List;

import com.ground.site.entity.SubDot;

public interface SubDotMapper {
	
	public List<SubDot> getSubDotList(SubDot subDot);
}
