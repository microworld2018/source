package com.ground.site.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ground.site.entity.SubDot;
import com.ground.site.mapper.SubDotMapper;
import com.ground.site.service.SubDotService;
import com.leetu.car.entity.Car;
import com.leetu.car.mapper.CarMapper;
import com.leetu.car.service.CarService;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.mapper.DictMapper;
import com.leetu.sys.service.DictService;

@Service
@Transactional
public class SubDotServiceImpl implements SubDotService{
	
	@Autowired
	private SubDotMapper subDotMapper;
	
	@Autowired
	private CarMapper carMapper;
	
	@Autowired
	private DictMapper DictMapper;

	@Override
	public List<SubDot> getSubDotList(SubDot subDot) {
		List<SubDot> list = subDotMapper.getSubDotList(subDot);
		if(list != null){
			for(int i=0;i<list.size();i++){
				//查询租借中数量
				Dict d = DictMapper.getDictByCodes("carBizState", "1");
				String dotId=list.get(i).getDotId().toString();
				String statId=d.getId();
				List<Car> listCar = carMapper.getAvailableCars(list.get(i).getDotId().toString(),d.getId());
				if(listCar != null){
					list.get(i).setLase(listCar.size());
				}else{
					list.get(i).setLase(0);
				}
				//查询维修中数量
				Dict dd = DictMapper.getDictByCodes("carBizState", "2");
				List<Car> listCar1 = carMapper.getAvailableCars(list.get(i).getDotId().toString(), dd.getId());
				if(listCar1 != null){
					list.get(i).setRepair(listCar1.size());
				}else{
					list.get(i).setRepair(0);
				}
				//查询网点下车辆总数
				List<Car> listCar2 = carMapper.getAvailableCars(list.get(i).getDotId().toString(),null);
				if(listCar2 != null){
					list.get(i).setCount(listCar2.size());
				}else{
					list.get(i).setCount(0);
				}
			}
		}
		return list;
	}

}
