package com.ground.notice.service;

import java.util.List;
import java.util.Map;

import com.ground.notice.entity.GroundNotice;
import com.ground.notice.entity.SubNotice;

public interface SubNoticeService {
	
	/**添加消息
	 * @param groundNotice
	 */
	void addGrouneNotice(GroundNotice groundNotice);
	
	/**添加用户消息关联表
	 * @param subNotice
	 */
	void addSubNotice(SubNotice subNotice);
	
	/**更新消息关联表
	 * @param subNotice
	 */
	void updateSubNotice(SubNotice subNotice);
	
	/**查询用户消息列表
	 * @param userId
	 * @return
	 */
	List<Map<String, Object>> getNoticeList(String userId);

}
