package com.ground.notice.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ground.notice.entity.GroundNotice;
import com.ground.notice.entity.SubNotice;

public interface SubNoticeMapper {
	
	/**添加消息
	 * @param groundNotice
	 */
	void addGrouneNotice(GroundNotice groundNotice);
	
	/**添加用户消息关联表
	 * @param subNotice
	 */
	void addSubNotice(SubNotice subNotice);
	
	/**更新消息关联表
	 * @param subNotice
	 */
	void updateSubNotice(SubNotice subNotice);
	
	/**查询用户消息列表(获取三天内的消息)
	 * @param userId
	 * @param beforeDate 
	 * @return
	 */
	List<Map<String, Object>> getNoticeList(@Param("userId")String userId, @Param("beforeDate")String beforeDate);

}
