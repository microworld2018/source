package com.ground.notice.entity;

import java.io.Serializable;
import java.util.Date;

/**用户消息关联表
 * @author 18833
 *
 */
public class SubNotice implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String userId;
	
	private String noticeId;
	
	private Integer isRead;
	
	private Date readTime;
	
	private String dotId;

	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}

	public Integer getIsRead() {
		return isRead;
	}

	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}

	public Date getReadTime() {
		return readTime;
	}

	public void setReadTime(Date readTime) {
		this.readTime = readTime;
	}

	public String getDotId() {
		return dotId;
	}

	public void setDotId(String dotId) {
		this.dotId = dotId;
	}

}
