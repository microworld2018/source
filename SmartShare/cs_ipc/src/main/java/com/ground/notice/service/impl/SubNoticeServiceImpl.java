package com.ground.notice.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ground.notice.entity.GroundNotice;
import com.ground.notice.entity.SubNotice;
import com.ground.notice.mapper.SubNoticeMapper;
import com.ground.notice.service.SubNoticeService;

@Service
@Transactional
public class SubNoticeServiceImpl implements SubNoticeService {
	
	@Autowired
	private SubNoticeMapper subNoticeMapper;//用户消息关联表
	
	@Override
	public void addGrouneNotice(GroundNotice groundNotice) {
		subNoticeMapper.addGrouneNotice(groundNotice);
	}

	@Override
	public void addSubNotice(SubNotice subNotice) {
		subNoticeMapper.addSubNotice(subNotice);
	}

	@Override
	public void updateSubNotice(SubNotice subNotice) {
		subNoticeMapper.updateSubNotice(subNotice);
	}

	@Override
	/**
	 * 获取三天内的消息
	 */
	public List<Map<String, Object>> getNoticeList(String userId) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar1 = Calendar.getInstance();
		calendar1.add(Calendar.DATE, -3);
		String beforeDate = sdf.format(calendar1.getTime());
		return subNoticeMapper.getNoticeList(userId,beforeDate);
	}

}
