package com.ground.notice.controler;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.core.controller.BaseController;
import com.ground.notice.entity.SubNotice;
import com.ground.notice.service.SubNoticeService;
import com.ground.user.entity.User;
import com.util.Ajax;

@Controller
@RequestMapping("groundNotice")
public class GroundNoticeController  extends BaseController<SubNotice> {
	
	@Autowired
	private SubNoticeService subNoticeService;
	
	/**获取消息列表
	 * @param request
	 * @return
	 */
	@RequestMapping("/getNoticeList")
	public ModelAndView getNoticeList(HttpServletRequest request){
		ModelAndView modelAndView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		Integer noticeCount = Integer.valueOf(request.getParameter("noticeCount"));
		if(u == null){
			modelAndView.setViewName("/manage_car/login");
			return modelAndView;
		}
		List<Map<String, Object>> subNoticeList = subNoticeService.getNoticeList(u.getId());
		modelAndView.addObject("subNoticeList", subNoticeList);
		modelAndView.addObject("noticeCount", subNoticeList.size());
		if(noticeCount==null||noticeCount==subNoticeList.size()||noticeCount==0){
			modelAndView.addObject("noticeResult",0);
		}else{
			modelAndView.addObject("noticeResult",1);
		}
		modelAndView.setViewName("/manage_car/notice");
		return modelAndView;
	}
	
	
	/**更新已读消息
	 * @param noticeId
	 * @param request
	 * @return
	 */
	@RequestMapping("/updateReadNotice")
	public @ResponseBody String updateReadNotice(String noticeId,HttpServletRequest request){
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			return Ajax.AppJsonResult(1,"请登录！");
		}
		SubNotice subNotice = new SubNotice();
		subNotice.setNoticeId(noticeId);
		subNotice.setReadTime(new Date());
		subNotice.setUserId(u.getId());
		subNoticeService.updateSubNotice(subNotice);
		return Ajax.AppJsonResult(0,"更新完成");
	}
}
