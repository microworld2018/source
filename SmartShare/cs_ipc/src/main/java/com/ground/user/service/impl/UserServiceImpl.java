package com.ground.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ground.user.entity.User;
import com.ground.user.mapper.UserMapper;
import com.ground.user.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userMapper;
	
	@Override
	public User getByParam(User user) {
		return userMapper.getByParam(user);
	}

	@Override
	public void updatePassword(User user) {
		userMapper.updatePassword(user);
	}

}
