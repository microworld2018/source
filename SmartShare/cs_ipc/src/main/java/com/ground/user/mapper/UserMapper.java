package com.ground.user.mapper;

import com.ground.user.entity.User;

public interface UserMapper {
	
	/**根据实体类不为空的字段查询用户
	 * @param user
	 * @return
	 */
	User getByParam(User user);
	
	/**更新密码
	 * @param user
	 */
	void updatePassword(User user);
}
