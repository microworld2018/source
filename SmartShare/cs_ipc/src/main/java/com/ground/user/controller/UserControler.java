package com.ground.user.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.core.controller.BaseController;
import com.ground.user.entity.User;
import com.ground.user.service.UserService;
import com.util.Ajax;
import com.util.Constants;

@Controller
@RequestMapping("ground")
public class UserControler  extends BaseController<User>  {

	@Autowired
	private UserService userService;
	
	 /**打开登陆页面
	 * @return
	 */
	@RequestMapping("/openLogin")
	public ModelAndView  openLogin(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("/manage_car/login");
		return modelAndView;
	}
	
	@RequestMapping("/login")	
	public @ResponseBody String userLogin(User user,HttpServletRequest request,Model model){
		User u = (User) request.getSession().getAttribute("user");
		String result="";
		if(u == null){
			//未登录则去验证用户信息并存入session
			user.setIsUse(0);
			user = userService.getByParam(user);
			if(user == null){
				result = Ajax.AppJsonResult(1,"账号密码错误！");
			}else{
				if(user.getIsUse() == 1){
					result = Ajax.AppJsonResult(1,"账号已锁定不能登陆！");
				}
				request.getSession().setAttribute("user", user);
				result = Ajax.AppbzJsonResult(0,"登录成功！",user);
			}
		}else{
			//验证已登陆的信息是否合法
			user = userService.getByParam(user);
			if(user == null){
				result = Ajax.AppJsonResult(1,"账号密码错误！");
			}else{
				if(user.getIsUse() == 1){
					result = Ajax.AppJsonResult(1,"账号已锁定不能登陆！");
				}
				result = Ajax.AppbzJsonResult(0,"登录成功！",user);
			}
		}
		return result;
	}
	
	
	/**打开主页
	 * @return
	 */
	@RequestMapping("/openIndex")
	public ModelAndView openIndex(){
		ModelAndView modelAndView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			modelAndView.setViewName("/manage_car/login");
			return modelAndView;
		}
		modelAndView.addObject("user", u);
		modelAndView.setViewName("/manage_car/index");
		return modelAndView;
	}
	
	/**打开个人中心
	 * @return
	 */
	@RequestMapping("/openPersonalCenter")
	public ModelAndView openPersonalCenter(){
		ModelAndView modelAndView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			modelAndView.setViewName("/manage_car/login");
			return modelAndView;
		}
		modelAndView.addObject("user", u);
		modelAndView.setViewName("/manage_car/personal_center");
		return modelAndView;
	}
	
	/**退出登录
	 * @return
	 */
	@RequestMapping("/signOut")
	public ModelAndView signOut(){
		ModelAndView modelAndView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			modelAndView.setViewName("/manage_car/login");
			return modelAndView;
		}
		request.getSession().setAttribute("user", null);
		modelAndView.setViewName("/manage_car/login");
		return modelAndView;
	}
	
	/**打开密码修改页面
	 * @return
	 */
	@RequestMapping("/openUpdatePassword")
	public ModelAndView openUpdatePassword(){
		ModelAndView modelAndView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			modelAndView.setViewName("/manage_car/login");
			return modelAndView;
		}
		modelAndView.setViewName("/manage_car/personal_center_password");
		return modelAndView;
	}
	
	
	/**
	 * 验证旧密码是否正确
	 */
	@RequestMapping("/udatePassword")
	public @ResponseBody String udatePassword(String paswd,String lastpaword){
		User u = (User) request.getSession().getAttribute("user");
		String result="";
		if(u == null){
			result = Ajax.AppbzJsonResult(1,"请先登录！",u);
			return result;
		}else{
			if(u.getPassword().equals(paswd)){
				u.setPassword(lastpaword);
				userService.updatePassword(u);
				result = Ajax.AppJsonResult(0,"更新成功！");
			}else{
				result = Ajax.AppJsonResult(1,"旧密码不正确！");
			}
		}
		return result;
	}
}
