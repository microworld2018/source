package com.ground.order.service.impl;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ground.notice.entity.GroundNotice;
import com.ground.notice.entity.SubNotice;
import com.ground.notice.service.SubNoticeService;
import com.ground.order.entity.GroundOrder;
import com.ground.order.mapper.GroundOrderMapper;
import com.ground.order.service.GroundOrderService;
import com.ground.site.entity.SubDot;
import com.ground.site.service.SubDotService;
import com.ground.user.entity.User;
import com.leetu.car.mapper.CarMapper;
import com.leetu.car.param.CarParam;
import com.leetu.device.mapper.DeviceBindingMapper;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.feestrategy.mapper.StrategyBaseMapper;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.mapper.OrderMapper;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.service.PayOrderService;
import com.leetu.orders.util.OrderFee;
import com.leetu.place.mapper.BranchDotMapper;
import com.leetu.place.param.BranchDotParam;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.mapper.DictMapper;
import com.leetu.sys.service.DictService;
import com.leetu.sys.service.SysOperateLogService;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.HttpRequestUtil;
import com.util.PropertiesUtil;
import com.util.ToolDateTime;
import com.util.car.CurrentCarInfo;
import com.util.msg.MsgContent;
import com.util.orders.OrderNoUtil;

@Service
@Transactional
public class GroundOrderServiceImpl implements GroundOrderService {

	@Autowired
	private GroundOrderMapper groundOrderMapper;// 地勤订单

	@Autowired
	private CarMapper carMapper;// 车辆

	@Autowired
	private DictMapper dictMapper;// 字典表

	@Autowired
	private BranchDotMapper branchDotMapper;// 网点

	@Autowired
	private StrategyBaseMapper strategyBaseMapper;// 计费规则

	@Autowired
	private OrderMapper orderMapper;// 公共订单

	@Autowired
	private DeviceBindingMapper deviceBindingMapper;// 车辆绑定车机

	@Autowired
	private SysOperateLogService sysOperateLogService;

	@Autowired
	private DictService dictService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrdersDetailService ordersDetailService;

	@Autowired
	private PayOrderService payorderService;

	@Autowired
	private SubNoticeService subNoticeService;

	@Autowired
	private SubDotService subDotService;

	@Autowired
	private SubScriberService subScriberService;

	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public String addOrder(String carId, String tuneCarReason, String dotId, HttpServletRequest request,
			String userName) {
		String result = "";
		User u = (User) request.getSession().getAttribute("user");
		if (u == null) {
			result = Ajax.AppJsonResult(1, "亲要先登录哦！");
			return result;
		}
		// 获取字典表中车辆租借中状态
		Dict di = dictMapper.getDictByCodes("carBizState", "1");
		// 更新车辆状态为租借中
		Map<String, Object> car = branchDotMapper.carinfo(carId, null);
		String version = car.get("version").toString();
		int st = branchDotMapper.updateCarStatus(carId, di.getId(), version);
		if (st < 1) {
			result = Ajax.AppJsonResult(Constants.APP_CAR_YUYUE_OK, Constants.APP_CAR_YUYUE_OK_MSG);
			return result;
		}
		// 查看此车在订单中是否存在,如果存在,则直接返回下单失败
		int groundOrderCount = groundOrderMapper.selectGroundOrder(carId);
		if (groundOrderCount > 0) {
			result = Ajax.AppJsonResult(Constants.APP_CAR_YUYUE_OK, Constants.APP_CAR_YUYUE_OK_MSG);
			return result;
		}

		String orderNo = OrderNoUtil.getOrderNo();// 生成订单编号
		// 添加地勤专有订单表
		GroundOrder groundOrder = new GroundOrder();
		groundOrder.setSubId(u.getId());// 用户ID
		groundOrder.setOrderNo(orderNo);// 订单编号
		groundOrder.setCarId(carId);// 车辆ID
		groundOrder.setTakeDotId(dotId);// 取车网点ID
		groundOrder.setTakeTime(new Date());// 开始时间
		groundOrder.setOrderState(0);// 订单状态
		groundOrder.setTuneCarReason(tuneCarReason);// 调车原因
		groundOrderMapper.addOrder(groundOrder);
		// 添加公共订单表
		StrategyBase carfee = strategyBaseMapper.carfee(carId);// 获取计费规则
		// 租赁类型名字
		String type = carfee.getIsPrepaidPay() == 1 ? "日租" : "时租";
		Map<String, Object> param = new Hashtable<String, Object>();
		DecimalFormat df = new DecimalFormat("000");
		param.put("id", "");// 订单主键
		param.put("orderid", "");// 订单主键
		param.put("orders_no", orderNo);// 订单编号
		param.put("orders_detail_no", orderNo + df.format(1));// 订单详情编号
		param.put("dotId", dotId);// 网点id
		param.put("carId", carId);// 车辆id
		param.put("subId", u.getId());// 会员id
		param.put("source", Orders.PAY_CHANNEL_APP);// 会员id
		param.put("state", "3");// 订单状态 0预约取消 1预约成功 2预约超时 3租车开始 4租车结束
		param.put("isAbnormity", "0");// 订单是否异常 0 为正常 1为异常
		param.put("isAppraise", "0");// 订单是否评价 0 为未评价 1为评价
		param.put("strategyId", carfee.getId());// 计费规则ID
		param.put("strategyTitle", carfee.getName());// 计费规则名称
		param.put("isTimeout", "0");// 是否超时
		param.put("timeoutStrategyid", carfee.getId());// 对应超时策略ID
		param.put("isRunning", "0");// 是否在进行中
		param.put("preOrders", "");// 上笔订单ID
		param.put("isPrePay", "0");// 是否预付费
		param.put("isPaid", "0");// 是否已经付款
		param.put("typeId", carfee.getType());// 租赁类型ID
		param.put("typeName", type);// 租赁类型名字
		param.put("isOver", "0");// 标记订单是否完结
		param.put("isException", "0");// 标记订单异常
		param.put("isOpenBill", "0");// 是否开了发票
		param.put("isbill", "1");// 是否开了发票
		param.put("time", new Date());// 创建时间
		param.put("begintime", new Date());
		param.put("isGround", 1);// 是否是地勤订单（0：否1：是）
		param.put("paystatus", Orders.STATE_ORDER_NO_PAY);// 默认未支付
		// 保存订单
		orderMapper.addyyorder(param);
		// 添加消息记录表
		GroundNotice groundNotice = new GroundNotice();
		groundNotice.setCreateTime(new Date());
		// orderMapper.getOrderInfoByOrdersNo(ordersNo);

		GroundNotice groundNotice2 = subScriberService.querySubscriberByIdAndOrdernoTake(u.getId(), orderNo);
		String content = "【电动侠租车】用户:" + groundNotice2.getUserName() + "开始用车，手机号:" + groundNotice2.getUserPhone()
				+ "，车牌号:" + groundNotice2.getVehiclePlateId() + "。用车时间:" + sdf.format(groundNotice2.getTakeTime())
				+ ",取车地点:" + groundNotice2.getDot() + ".";
		groundNotice.setNoticeContent(content);
		groundNotice.setNoticeType(0);
//		String subId = "skdfj" + (new Date()).getTime() + "asjkdfj";
		String subId = UUID.randomUUID().toString().replace("-", "");
		groundNotice.setId(subId);
		subNoticeService.addGrouneNotice(groundNotice);
		// 获取取车网点所属的地勤人员
		SubDot subDot = new SubDot();
		subDot.setDotId(dotId);
		List<SubDot> subDotList = subDotService.getSubDotList(subDot);
		// 添加消息关联记录
		if (subDotList != null && subDotList.size() > 0) {
			for (int i = 0; i < subDotList.size(); i++) {
				SubNotice subNotice = new SubNotice();
//				String subAndDotId = "skdfj" + (new Date()).getTime() + "asjkdfj";
				String subAndDotId = UUID.randomUUID().toString().replace("-", "");
				subNotice.setDotId(dotId);
				subNotice.setUserId(subDotList.get(i).getSubId());
				subNotice.setNoticeId(subId);
				subNotice.setIsRead(0);
				subNotice.setId(subAndDotId);
				subNoticeService.addSubNotice(subNotice);
			}
		}
		// 保存订单详情
		param.put("orderid", param.get("id"));
		orderMapper.addyyorderDetail(param);
		// 根据车辆ID获取车辆设备绑定，车机编号SN
		Map<String, String> deviceBindingMap = deviceBindingMapper.getDeviceByCarId(carId);
		if (deviceBindingMap == null || StringUtils.isEmpty(deviceBindingMap)) {
			return Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
		}
		// 获取设备编号SN
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("sn", deviceBindingMap.get("device_no"));
		// 调用开始用车远程恢复供电
		String contents = HttpRequestUtil.getPostMethod(CurrentCarInfo.RESTORE_POWER_URL, paramMap);
		System.out.println(contents);
		// 调用开车门
		result = CarParam.getDeviceByCarIdOpDoor(deviceBindingMap, "1", CurrentCarInfo.OPEN_DOOR_URL);
		JSONObject jsStr = JSONObject.fromObject(result);
		if (!jsStr.get("resultCode").equals(252)) {
			result = Ajax.AppJsonResult(1, "开车门失败！", result);
		} else {
			result = Ajax.AppJsonResult(0, "下单成功！");
		}
		return result;
	}

	@Override
	public void update(GroundOrder groundOrder) {
		groundOrderMapper.update(groundOrder);
	}

	@Override
	public List<Map<String, Object>> getGrounOrderList(String userId) {
		return groundOrderMapper.getGrounOrderList(userId);
	}

	@Override
	public String overOrder(String orderNo, String backDotId, HttpServletRequest request) {
		String result = "";
		User u = (User) request.getSession().getAttribute("user");
		if (u == null) {
			result = Ajax.AppJsonResult(1, "亲要先登录哦！");
			return result;
		}
		GroundOrder groundOrder = groundOrderMapper.getGrounOrder(orderNo);
		if (groundOrder == null) {
			result = Ajax.AppJsonResult(1, "没有此订单信息！");
			return result;
		}
		Orders order = orderMapper.getOrderInfoByOrdersNo(groundOrder.getOrderNo());
		// 根据车辆ID获取车辆设备绑定，车机编号SN
		Map<String, String> deviceBindingMap = deviceBindingMapper.getDeviceByCarId(order.getCarId());
		if (deviceBindingMap == null || StringUtils.isEmpty(deviceBindingMap)) {
			return Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
		}
		// 获取车辆信息map
		Map<String, Object> carinfoMap = branchDotMapper.carinfo(groundOrder.getCarId(), null);
		// 订单结束时间
		String endTime = ToolDateTime.format(new Date(), ToolDateTime.pattern_ymd_hms);
		// 更新订单详情行驶里程
		Map<String, String> paramMap = new HashMap<String, String>();
		String msgUrl = PropertiesUtil.getStringValue("msg_url") + "/car/getCarMileageForGPS";
		// 获取总里程
		String mileage = "0";// 默认0
		// 获取里程传入车牌号、开始时间、结束时间
		paramMap.put("sn", carinfoMap.get("vehiclePlateId").toString());
		paramMap.put("beginTime", ToolDateTime.format(groundOrder.getTakeTime(), ToolDateTime.pattern_ymd_hms));
		paramMap.put("endTime", endTime);
		mileage = BranchDotParam.getCarMileageForGPS(paramMap, msgUrl, sysOperateLogService);
		if (mileage != null && !"".equals(mileage) && !"0".equals(mileage)) {
			// 计算里程、精确到小数点后两位
			mileage = String.valueOf(ArithUtil.div(Double.valueOf(mileage), 1000, 2));
		}
		ordersDetailService.updateByOrdersIdDetil(order.getId(), mileage, endTime); // 更新订单详情添加结束时间
		// 订单费用信息
		Map<String, Object> orderfee = OrderFee.orderFeeInfo(order, dictService, orderService, ordersDetailService,
				null);
		double actual_fee = 0.00;// 实际消费金额
		double payPrice = 0.00;// 第三方实际支付金额
		double userPrice = 0.00;// 账户支付金额
		double allPrice = Double.valueOf(orderfee.get("allPrice").toString());// 总费用
		// double minConsumption =
		// Double.valueOf(orderfee.get("minConsumption").toString());//最低消费金额
		double allMile = Double.valueOf(orderfee.get("allMile").toString());
		double allMilePrice = Double.valueOf(orderfee.get("allMilePrice").toString());// 里程消费
		double allTimePrice = Double.valueOf(orderfee.get("allTimePrice").toString());// 时长消费
		Map<String, Object> orders = new Hashtable<String, Object>();
		orders.put("endTime", endTime);
		orders.put("orderId", order.getId());
		orders.put("end_site_id", backDotId);
		orders.put("orders_duration", orderfee.get("allminutes"));
		orders.put("total_fee", allPrice);
		orders.put("actual_fee", actual_fee);
		orders.put("tpos_pay_fee", payPrice);
		orders.put("use_balance", userPrice);
		orders.put("coupon_fee", 0);
		orders.put("end_mileage", orderfee.get("endmile"));
		orders.put("state", Orders.STATE_ORDER_END);
		orders.put("channel", Orders.PAY_CHANNEL_APP);
		orders.put("allMile", allMile);
		orders.put("allMilePrice", allMilePrice);
		orders.put("allTimePrice", allTimePrice);
		orders.put("payType", Orders.PAY_TYPE_ACCOUNT);
		orders.put("is_paid", 1);
		orders.put("is_over", 1);
		orders.put("is_running", 1);
		// 更新公共订单信息
		payorderService.updateOrderInfo(orders);
		payorderService.updateOrderstate(orderNo);// 更新订单支付状态
		// 更新地勤专有订单表
		groundOrder.setBackDotId(backDotId);
		groundOrder.setBackTime(new Date());
		groundOrder.setOrderState(1);
		groundOrder.setOrderFee(allPrice);
		// groundOrder.setOrderTime(Double.parseDouble(orderfee.get("alltime").toString()));
		String time = orderfee.get("alltime").toString();
		char[] timeArray = time.toCharArray();
		StringBuffer allTime = new StringBuffer();
		for (int i = 0; i < timeArray.length; i++) {
			if (Character.isDigit(timeArray[i])) {
				allTime.append(timeArray[i]);
			}
		}
		groundOrder.setOrderTime(Double.parseDouble(allTime.toString()));
		groundOrder.setDrivenDistance(allMile);
		groundOrderMapper.update(groundOrder);
		// 获取字典表中车辆未租借状态
		Dict di = dictMapper.getDictByCodes("carBizState", "0");
		// 更新车辆状态为未租借
		Map<String, Object> car = branchDotMapper.carinfo(order.getCarId(), null);
		String version = car.get("version").toString();
		int st = branchDotMapper.updateCarStatus(order.getCarId(), di.getId(), version);
		if (st < 1) {
			result = Ajax.AppJsonResult(Constants.APP_CAR_YUYUE_OK, Constants.APP_CAR_YUYUE_OK_MSG);
			return result;
		}
		// 更新车辆所在网点
		Map<String, String> pmap = new HashMap<String, String>();
		pmap.put("carId", order.getCarId());
		pmap.put("dotId", backDotId);
		pmap.put("isused", "1");
		branchDotMapper.updotcar(pmap);

		GroundNotice groundNotice = new GroundNotice();
		groundNotice.setCreateTime(new Date());
		// orderMapper.getOrderInfoByOrdersNo(ordersNo);

		GroundNotice groundNotice2 = subScriberService.querySubscriberByIdAndOrdernoBack(u.getId(),
				groundOrder.getOrderNo(), backDotId);
		String content = "【电动侠租车】用户:" + groundNotice2.getUserName() + "开始还车，手机号:" + groundNotice2.getUserPhone()
				+ "，车牌号:" + groundNotice2.getVehiclePlateId() + "。还车时间:" + sdf.format(groundNotice2.getTakeTime())
				+ ",还车地点:" + groundNotice2.getDot() + ".";
		groundNotice.setNoticeContent(content);
		groundNotice.setNoticeType(0);
//		String subId = "skdfj" + (new Date()).getTime() + "asjkdfj";
		String subId = UUID.randomUUID().toString().replace("-", "");
		groundNotice.setId(subId);
		subNoticeService.addGrouneNotice(groundNotice);
		// 获取取车网点所属的地勤人员
		SubDot subDot = new SubDot();
		subDot.setDotId(backDotId);
		List<SubDot> subDotList = subDotService.getSubDotList(subDot);
		// 添加消息关联记录
		if (subDotList != null && subDotList.size() > 0) {
			for (int i = 0; i < subDotList.size(); i++) {
				SubNotice subNotice = new SubNotice();
//				String subAndDotId = "skdfj" + (new Date()).getTime() + "asjkdfj";
				String subAndDotId = UUID.randomUUID().toString().replace("-", "");
				subNotice.setDotId(backDotId);
				subNotice.setUserId(subDotList.get(i).getSubId());
				subNotice.setNoticeId(subId);
				subNotice.setIsRead(0);
				subNotice.setId(subAndDotId);
				subNoticeService.addSubNotice(subNotice);
			}
		}
		// 获取设备编号SN
		/*
		 * Map<String, String> paramMap = new HashMap<String, String>();
		 * paramMap.put("sn", deviceBindingMap.get("device_no"));
		 */
		/*
		 * //调用关闭中控锁并断电功能 String contents =
		 * HttpRequestUtil.getPostMethod(CurrentCarInfo.LOCK_POWER_OFF_URL,
		 * paramMap); System.out.println(contents);
		 */
		// 调用关车门
		result = CarParam.getDeviceByCarIdOpDoor(deviceBindingMap, "2", CurrentCarInfo.CLOSE_DOOR_URL);
		return result;
	}

	@Override
	public GroundOrder getGrounOrder(GroundOrder groundOrder) {
		// TODO Auto-generated method stub
		return null;
	}

}
