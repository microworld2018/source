package com.ground.order.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ground.order.entity.GroundOrder;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.sys.service.DictService;

public interface GroundOrderService {
	/**添加订单
	 * @param groundOrder
	 */
	String addOrder(String carId,String tuneCarReason,String dotId,HttpServletRequest request,String userName);
	
	/**更新订单
	 * @param groundOrder
	 */
	void update(GroundOrder groundOrder);
	
	/**获取订单集合
	 * @param groundOrder
	 * @return
	 */
	List<Map<String, Object>> getGrounOrderList(String userId);
	
	
	/**获取单条订单
	 * @param groundOrder
	 * @return
	 */
	GroundOrder getGrounOrder(GroundOrder groundOrder);

	/**结束订单
	 * @param orderNo
	 * @return
	 */
	public String overOrder(String orderNo,String backDotId,HttpServletRequest request);
}
