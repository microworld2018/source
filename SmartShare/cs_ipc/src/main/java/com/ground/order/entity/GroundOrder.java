package com.ground.order.entity;

import java.io.Serializable;
import java.util.Date;

public class GroundOrder   implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String id;
	
	private String orderNo;
	
	private String subId;
	
	private String carId;
	
	private String takeDotId;
	
	private String backDotId;
	
	private Date takeTime;
	
	private Date backTime;
	
	private Integer orderState;
	
	private String tuneCarReason;
	
	private Double orderFee;
	
	private Double drivenDistance;
	
	private Double orderTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getTakeDotId() {
		return takeDotId;
	}

	public void setTakeDotId(String takeDotId) {
		this.takeDotId = takeDotId;
	}

	public String getBackDotId() {
		return backDotId;
	}

	public void setBackDotId(String backDotId) {
		this.backDotId = backDotId;
	}

	public Date getTakeTime() {
		return takeTime;
	}

	public void setTakeTime(Date takeTime) {
		this.takeTime = takeTime;
	}

	public Date getBackTime() {
		return backTime;
	}

	public void setBackTime(Date backTime) {
		this.backTime = backTime;
	}

	public Integer getOrderState() {
		return orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	public String getTuneCarReason() {
		return tuneCarReason;
	}

	public void setTuneCarReason(String tuneCarReason) {
		this.tuneCarReason = tuneCarReason;
	}

	public Double getOrderFee() {
		return orderFee;
	}

	public void setOrderFee(Double orderFee) {
		this.orderFee = orderFee;
	}

	public Double getDrivenDistance() {
		return drivenDistance;
	}

	public void setDrivenDistance(Double drivenDistance) {
		this.drivenDistance = drivenDistance;
	}

	public Double getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(Double orderTime) {
		this.orderTime = orderTime;
	}
	
	

}
