package com.ground.order.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ground.order.entity.GroundOrder;

public interface GroundOrderMapper {

	void addOrder(GroundOrder groundOrder);
	
	void update(GroundOrder groundOrder);
	
	List<Map<String, Object>> getGrounOrderList(@Param("userId")String userId);
	
	GroundOrder getGrounOrder(@Param("orderNo")String orderNo);
    
	int selectGroundOrder(@Param("carId")String carId);
}
