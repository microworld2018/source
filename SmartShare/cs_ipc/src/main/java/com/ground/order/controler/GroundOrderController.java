package com.ground.order.controler;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.core.controller.BaseController;
import com.ground.order.entity.GroundOrder;
import com.ground.order.service.GroundOrderService;
import com.ground.user.entity.User;
import com.ground.user.service.UserService;
import com.leetu.orders.service.OrderService;
import com.util.Ajax;
import com.util.Utils;

import net.sf.json.JSONObject;

@Controller
@RequestMapping("gorundOrder")
public class GroundOrderController   extends BaseController<GroundOrder> {
	
	@Autowired
	private GroundOrderService groundOrderService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private UserService userService;
	
	/**下订单
	 * @return
	 */
	@RequestMapping("/placeAnOrder")
	public @ResponseBody String placeAnOrder(String carId,String tuneCarReason,String dotId,HttpServletRequest request){
		User u = (User) request.getSession().getAttribute("user");
		String result="";
		if(u == null){
			result = Ajax.AppJsonResult(1,"请先登录！");
			return result;
		}
		result = groundOrderService.addOrder(carId, tuneCarReason, dotId, request,u.getName());
		return result;
	}
	
	@RequestMapping("/backPlaceAnOrder")
	public void backPlaceAnOrder(String carId,String tuneCarReason,String dotId,HttpServletRequest request){
		String data = request.getParameter("data");
		JSONObject obj = JSONObject.fromObject(data);
		dotId = (String) obj.get("dotId");
		carId = (String) obj.get("carId");
		String userId = obj.getString("userId");
		tuneCarReason = obj.getString("tuneCarReason");
		User user = new User();
		user.setId(userId);
		User u = userService.getByParam(user);
		request.getSession().setAttribute("user", u);
		
		String result="";
		result = groundOrderService.addOrder(carId, tuneCarReason, dotId, request,u.getName());
		Utils.outJSONObject(result,response);
	}
	
	/**根据车辆ID查询该车辆下的所有订单
	 * @param carId
	 * @return
	 */
	@RequestMapping("/getCarByIdOrderList")
	public ModelAndView getCarByIdOrderList(String carId){
		ModelAndView modelView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			modelView.setViewName("/manage_car/login");
			return modelView;
		}
		List<Map<String, Object>> list = orderService.getCarByIdOrderList(carId);
		modelView.addObject("orderList", list);
		modelView.setViewName("/manage_car/control_order");
		return modelView;
	}
	
	/**查询该用户所管理网点下所有订单(以取车网点为准)
	 * @return
	 */
	@RequestMapping("/getDotByIdOrder")
	public ModelAndView getDotByIdOrder(HttpServletRequest request){
		ModelAndView modelView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			modelView.setViewName("/manage_car/login");
			return modelView;
		}
		List<Map<String, Object>> list = groundOrderService.getGrounOrderList(u.getId());
		modelView.addObject("orderList", list);
		modelView.setViewName("/manage_car/my_order");
		return modelView;
	}
	
	/**结束订单
	 * @return
	 */
	@RequestMapping("/overorder")
	public @ResponseBody String overOrder(String orderNo,String backDotId,HttpServletRequest request){
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			return Ajax.AppJsonResult(1,"用户未登录！"); 
		}
		return groundOrderService.overOrder(orderNo, backDotId,request);
	}

}
