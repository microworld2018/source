package com.core.init;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Service;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.leetu.device.mapper.DeviceMapper;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.mapper.DictMapper;

@Service
public class StartupListener implements ApplicationListener<ContextRefreshedEvent>{

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (event.getApplicationContext().getParent() == null) {
			//初始化
//			redisCarAndDevice();
			redisCarInfo();
			System.out.println("初始化成功============================"); 
		}
	}
	
	
	@Autowired
	private DeviceMapper deviceMapper;
	@Autowired
	private DictMapper dictMapper;
	
	/**
	 * 初始化车辆信息
	 */
	public void redisCarInfo(){
		//汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> ActiveIDs = new Hashtable<String,String>();
		//汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> ActiveSNs = new Hashtable<String,String>();
		//获取 已绑定设备的车辆信息
		List<Map<String,String>> cartilist = deviceMapper.carAndDevice(null);
		
		if(cartilist != null && cartilist.size() > 0){
			for (Map<String, String> map : cartilist) {
				String carId = map.get("car_id");
				String deviceNo = map.get("device_no");
				ActiveIDs.put(carId, deviceNo);
				ActiveSNs.put(deviceNo, carId);
			}
		}
		
		RedisClient.setobjdata("ActiveIDs", ActiveIDs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("ActiveSNs", ActiveSNs, DataBaseUtil.CS_CAR_MSG);
		
	}
	
	
	/**
	 * 初始化 车辆租借和为租借的车辆信息
	 */
	public void redisCarAndDevice() {
		//
		//租借中得汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> ActiveIDs = new Hashtable<String,String>();
		//租借中得汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> ActiveSNs = new Hashtable<String,String>();
		
		//未租借中得汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> InactiveIDs = new Hashtable<String,String>();
		//未借中得汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> InactiveSNs = new Hashtable<String,String>();

		//未租借状态
		Dict d = dictMapper.getDictByCodes("carBizState", "0");
		String t = d.getId();
		//租借中 状态
		Dict di = dictMapper.getDictByCodes("carBizState", "1");
		String ti = di.getId();
		//租借中的车辆车机 信息
		List<Map<String,String>> cartilist = deviceMapper.carAndDevice(ti);
		for (Map<String, String> map : cartilist) {
			String carId = map.get("car_id");
			String deviceNo = map.get("device_no");
			ActiveIDs.put(carId, deviceNo);
			ActiveSNs.put(deviceNo, carId);
		}
		//未租借的车辆车机 信息
		List<Map<String,String>> cartlist = deviceMapper.carAndDevice(t);
		for (Map<String, String> map : cartlist) {
			String carId = map.get("car_id");
			String deviceNo = map.get("device_no");
			InactiveIDs.put(carId, deviceNo);
			InactiveSNs.put(deviceNo, carId);
		}
		RedisClient.setobjdata("ActiveIDs", ActiveIDs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("ActiveSNs", ActiveSNs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("InactiveIDs", InactiveIDs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("InactiveSNs", InactiveSNs, DataBaseUtil.CS_CAR_MSG);
		
	}

}
