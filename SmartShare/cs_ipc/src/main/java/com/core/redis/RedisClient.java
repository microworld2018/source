package com.core.redis;

import java.util.Map;

import redis.clients.jedis.Jedis;

import com.core.redis.util.DataBaseUtil;
import com.core.redis.util.SerializeUtil;
import com.leetu.sys.entity.User;

/**
 * redis 操作api
 * 
 * @author hzw
 */
public class RedisClient {
	
	

	/**
	 * 保存数据 类型为 Map<String,String>
	 * 
	 * @param flag
	 * @param mapData
	 */
	public static boolean setMapDataToRedis(String flag,
			Map<String, String> mapData,int database) {
		boolean status = true;
		Jedis redisClient = null;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			redisClient.hmset(flag, mapData);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			status = false;
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return status;
	}
	
	/**
	 * 获取Map数据
	 * 
	 * @param flag
	 * @return
	 */
	public static Map<String, String> getMapData(String flag,int database) {
		Map<String, String> dataMap = null;

		Jedis redisClient = null;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			dataMap = redisClient.hgetAll(flag);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return dataMap;
	}
	
	/**
	 * 保存数据 类型为 key-value
	 * 
	 * @param flag
	 * @param field
	 * @param value
	 */
	public static boolean setDataToRedis(String flag, String field, String value,int database) {
		Jedis redisClient = null;
		boolean status = true;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			redisClient.hset(flag, field, value);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			status = false;
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return status;
	}
	/**
	 * 根据key和字段获取数据
	 * 
	 * @param flag
	 * @param field
	 * @return
	 */
	public static String getData(String flag, String field,int database) {
		String data = null;
		Jedis redisClient = null;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			data = redisClient.hget(flag, field);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}

		return data;
	}
	
	/**
	 * 存放key和value数据
	 * 
	 * @param key
	 * @param field
	 * @return
	 */
	public static boolean setstrdata(String key, String value,int database) {
		Jedis redisClient = null;
		boolean status = true;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			redisClient.set(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			status = false;
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return status;

	}
	/**
	 * 存放key和value数据和失效时间 
	 * 
	 * @param flag
	 * @param field
	 * @param timeout
	 * @return
	 */
	public static boolean settimedata(String flag, String value,int timeout,int database) {
		Jedis redisClient = null;
		boolean status = true;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			redisClient.set(flag, value);
			redisClient.expire(flag, timeout);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			status = false;
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return status;

	}
	/**
	 * 根据key获取value数据
	 * 
	 * @param flag
	 * @param field
	 * @return
	 */
	public static String getstrvalue(String key,int database) {
		String data = null;
		Jedis redisClient = null;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			data = redisClient.get(key);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}

		return data;
	}
	/**
	 * 判断某一键是否存在
	 * @param key
	 * @param database
	 * @return
	 */
	public static boolean keyexists(String key,int database) {
		boolean state = false;
		Jedis redisClient = null;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			state = redisClient.exists(key);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return state;
	}
	

	/**
	 * 更具 字段 删除 数据
	 * 
	 * @param flag
	 * @return
	 */
	public static long deleteData(String flag,int database) {
		long result = 0;
		Jedis redisClient = null;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			result = redisClient.del(flag);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return result;
	}

	/**
	 * 存储对象
	 * @param k
	 * @param obj
	 */
	public static boolean setobjdata(String k, Object obj,int database) {
		byte[] key = k.getBytes();
		byte[] value = SerializeUtil.serialize(obj);
		Jedis redisClient = null;
		boolean status = true;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			redisClient.set(key, value);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			status = false;
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return status;

	}
	/**
	 * 定时存储对象
	 * @param k
	 * @param obj
	 */
	public static boolean settimeobjdata(String k, Object obj,int timeout,int database) {
		byte[] key = k.getBytes();
		byte[] value = SerializeUtil.serialize(obj);
		Jedis redisClient = null;
		boolean status = true;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			redisClient.set(key, value);
			redisClient.expire(key, timeout);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			status = false;
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}
		return status;
		
	}
	/**
	 * 获取对象
	 * @param k
	 * @return
	 */
	public static Object getobjkeys(String k,int database) {
		byte[] key = k.getBytes();
		byte[] data = null;
		Jedis redisClient = null;
		try {
			redisClient = RedisClientPool.jedisPool.getResource();
			redisClient.select(database);
			data = redisClient.get(key);
		} catch (Exception e) {
			e.printStackTrace();
			// 销毁对象
			RedisClientPool.jedisPool.returnBrokenResource(redisClient);
		} finally {
			// 还原到连接池
			RedisClientPool.jedisPool.returnResource(redisClient);
		}

		return SerializeUtil.unserialize(data);
	}
	public static void main(String[] args) throws Exception {
//		User u = new User();
//		u.setId("666");
//		u.setName("admin");
//		if(setobjdata("u",u)){
//			System.out.println(getobjkeys("u"));
//		}
		
		settimedata("as","b",5,1);
		System.out.println(getstrvalue("as",1));
		System.out.println(getstrvalue("as",0));
//		deleteData ("ActiveIDs",  DataBaseUtil.CS_CAR_MSG);
//		deleteData ("ActiveSNs", DataBaseUtil.CS_CAR_MSG);
//		deleteData ("InactiveIDs", DataBaseUtil.CS_CAR_MSG);
//		deleteData ("InactiveSNs",DataBaseUtil.CS_CAR_MSG);
		System.out.println(RedisClient.getobjkeys("ActiveIDs",  DataBaseUtil.CS_CAR_MSG));
		System.out.println(RedisClient.getobjkeys("ActiveSNs", DataBaseUtil.CS_CAR_MSG));
		System.out.println(RedisClient.getobjkeys("InactiveIDs", DataBaseUtil.CS_CAR_MSG));
		System.out.println(RedisClient.getobjkeys("InactiveSNs",DataBaseUtil.CS_CAR_MSG));
	}
}
