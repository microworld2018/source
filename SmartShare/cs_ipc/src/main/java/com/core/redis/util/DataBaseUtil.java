package com.core.redis.util;

/**
 * redis分库公共变量
 * @author hzw
 *
 */
public class DataBaseUtil {

	public static final Integer LOGIN_COMMON = 0;
	
	public static final Integer CS_IPC = 1;
	
	public static final Integer CS_CAR_MSG = 2;
	
	public static final Integer CS_MINI = 3;
	
}
