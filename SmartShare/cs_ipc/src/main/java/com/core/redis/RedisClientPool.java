package com.core.redis;

import com.util.PropertiesUtil;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisClientPool {

	public static RedisClientPool redisClientPool = getInstance();

	public static JedisPool jedisPool;

	public static synchronized RedisClientPool getInstance() {
		if (null == redisClientPool) {
			redisClientPool = new RedisClientPool();
		}
		return redisClientPool;
	}

	public RedisClientPool() {
		if (null == jedisPool) {
			init();
		}
	}

	/**
	 * 初始化jedis连接池
	 */
	public static void init() {
		JedisPoolConfig jedisPoolConfig = initPoolConfig();
		String host = PropertiesUtil.getStringValue("redis.host");
		int port = Integer
				.parseInt(PropertiesUtil.getStringValue("redis.port"));
		String password = PropertiesUtil.getStringValue("redis.pwd");
		int timeout = 60000;
		System.out.println(host +":"+ port);
		// 构造连接池
		jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password); 
//		jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout);
	}

	/**
	 * 初始化Jedis <一句话功能简述> <功能详细描述>
	 * 
	 * @return
	 * @see [类、类#方法、类#成员]
	 */
	private static JedisPoolConfig initPoolConfig() {
		JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
		// 控制一个pool最多有多少个状态为idle的jedis实例
		jedisPoolConfig.setMaxIdle(1000);
		// 最大能够保持空闲状态的对象数
		jedisPoolConfig.setMaxIdle(300);
		// 超时时间
		jedisPoolConfig.setMaxWait(1000);
		// 在borrow一个jedis实例时，是否提前进行alidate操作；如果为true，则得到的jedis实例均是可用的；
		jedisPoolConfig.setTestOnBorrow(true);
		// 在还会给pool时，是否提前进行validate操作
		jedisPoolConfig.setTestOnReturn(true);

		return jedisPoolConfig;
	}
}
