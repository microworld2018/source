package com.core.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.util.CryptoTools;
import com.util.TokenUtils;


/**
 * @desc 拦截器
 * @author Administrator
 * 
 */
public class AppLoginFilter implements HandlerInterceptor {

	private final Logger logger = Logger.getLogger(AppLoginFilter.class);
	
	@Autowired
	private SubScriberService subScriberService;

	//不拦截:des加密的接口。 如：app发送短信 登录接口
	private String[] escapeUrls = {
			"/sub/isnotoken",
			"/sub/eventStateFull",
			"/sub/mobileCodeLogin",
			"/sub/exit",
			"/sub/sendCode",
			"/recharge/rechargeByAliCallBack",
			"/place/appbranchdots",
			"/order/backAddOrder",
			"/app/getApp",
			"/agreement/subAgreement.html",
			"/deposit/xieyi.html",
			"/coupon/registrationCertificateCoupon.html",
			"/coupon/registrationCertificate.html",
			"/activity/activity.html",
			"/activity/deposit.html",
			"/activity/pic_pk.html",
			"/activity/car_five.html",
			"/activity/whats.html",
			"/activity/dad_day.html",
			"/activity/xiaoshu.html",
			"/activity/seek_527.html",
			"/activity/dragon.html",
			"/activity/examination.html",
			"/activity/shutdown.html",
			"/activity/xiyuehui/xiyuehui.html",//熙悦汇活动
			"/activity/xiyuehui/index.html",
			"/activity/xiyuehui/xiyuehui.html",
			"/coupon/activities.html",
			"/car/addCarInfoRedis",
			"/pay/downLoadUrl",
			"/system/smsSend",
			"/system/cancelOrder",
			"/message/pushAll",
			"/message/getMessage",
			"/activityMember/saveActivityMember",
			"/appStart/getAppStart",
			"/ground/openLogin",//打开地勤端登陆页面
			"/ground/login",//地勤登录接口
			"/ground/openIndex",//打开地勤主页接口
			"/subdot/dotList",//获取地勤用户名下的网点列表接口
			"/car/getCarGroundList",//获取地勤用户名下的车辆列表接口
			"/gorundOrder/placeAnOrder",//地勤下订单接口
			"/gorundOrder/backPlaceAnOrder",//后台下地勤订单接口
			"/gorundOrder/getCarByIdOrderList",//根据车辆ID查询该车辆所有订单
			"/gorundOrder/getDotByIdOrder",//获取地勤用户名下网点所有的订单列表
			"/gorundOrder/overorder",//结束订单
			"/car/getCarCurrentAddr",//地勤获取车辆当前位置
			"/car/whistleFindCar",//地勤鸣笛寻车开关车门接口
			"/car/powerUpsORProwerOff",//地勤通电断电接口
			"/groundNotice/getNoticeList",//地勤端消息列表
			"/groundNotice/updateReadNotice",//更新消息为已读状态
			"/car/openCarControl",//跳转车辆操控页面
			"/ground/openPersonalCenter",//打开个人中心页面
			"/ground/signOut",//退出登录
			"/ground/openUpdatePassword",//打开修改密码页面
			"/ground/udatePassword",//修改密码
			"/car/initCarInfoWhenCarOnline",//后台上线车辆，初始化车辆信息
			"/pay/tradeResults",//微信查询接口
			"/pay/payOrder",//订单支付接口（新）
			"/pay/tradeResultsNew",//微信查询接口（新）
			"/activity/downSendCoupon"//熙慧轩活动
			};
	//不拦截:没有des加密的接口。如:支付宝回调接口
	private String[] sanfangUrls = {
			"/order/share",
			"/pay/tbalipayCallBack",
			"/pay/alipayCallBack",
			"/pay/alipayCallBackNew",
			"/pay/verificationPayment",
			"/pay/wxCallBack",
			"/pay/wxCallBackNew",
			"/app/wxCallBack",
			"/app/alipayCallBack",
			"/app/alipayCallBackAccident",
			"/app/wxCallBackAccident",
			"/protocol/getprotocol"
			
	};

	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String url = request.getRequestURI();
		String path = request.getContextPath();
		try {
			if (url.startsWith(path)|| (url.endsWith(".action") || url.endsWith(".jsp"))) {
				url = url.replaceFirst(path, "");
			}
			//判断请求是否需要拦截
			if(!check(url,escapeUrls)){
				String data = request.getParameter("data");//des验证参数
				if(data!=null){
					Subscriber sub = TokenUtils.getSubscriber(data);
					Subscriber subscriber = subScriberService.querySubscriberById(sub.getId());
					
					if(sub != null && !"".equals(sub) && subscriber != null && !"".equals(subscriber)){
						//判断当前会员是否属于全锁状态
						if(subscriber.getEventState()!= null && !"".equals(subscriber.getEventState())){//全锁
							if(subscriber.getEventState().equals(Subscriber.EVENT_STATE_FUll)){
								CryptoTools des = new CryptoTools("HeGeA8G3".getBytes(),"6LA2EyQm".getBytes());//自定义密钥
								String token = des.decode(data);
								JSONObject jsonObject = JSONObject.fromObject(token);
								response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/sub/eventStateFull?token="+jsonObject.getString("token"));
								return false;
							}
						}
					}
					//判断token是否失效
					if(TokenUtils.isToken(data)){
						response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/sub/isnotoken");
						return false;
					}
				}else{
					if(!check(url,sanfangUrls)){
						response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/sub/isnotoken");
						return false;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/sub/isparamerror");
			return false;
		}
		return true;
	}
	
	/**
	 * 判断 elements是包含：element
	 * @param element
	 * @param elements
	 * @return
	 */
	private boolean check(String element, String[] elements) {
		if(element.contains("/css")||element.contains("/js")||element.contains("/images")){
			return true;
		}
		for (String e : elements) {
			if (element.endsWith(e)) {
				return true;
			}
		}
		return false;
	}
	
}
