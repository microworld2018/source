package com.leetu.coupon.mapper;

import org.apache.ibatis.annotations.Param;

import com.leetu.coupon.entity.Coupon;

public interface CouponMapper {

	/**
	 * 根据类型获取优惠卷
	 * @param coupon
	 * @return
	 */
	Coupon getCouponByType(@Param("couponType")Integer couponType);
	
	/**
	 * 优惠卷修改发放数量
	 * @param coupon
	 */
	void updateCouponSendNumber(Coupon coupon);
	
	/**
	 * 根据订单ID查询优惠卷信息
	 * @param orderId
	 * @return
	 */
	Coupon getCouonByOrderId(@Param("orderId")String orderId);

	/**
	 * 根据优惠券类型查询当前时间内有无发放的优惠券
	 * @param couponTypeWechatshare
	 * @return
	 */
	Coupon getCouponByTypeInNow(@Param("couponType")Integer couponTypeWechatshare);
	
	
	/**根据ID查询优惠券信息
	 * @param id
	 * @return
	 */
	Coupon getCouponById(@Param("id")String id);
}
