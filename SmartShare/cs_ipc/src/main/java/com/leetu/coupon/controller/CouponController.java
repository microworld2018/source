package com.leetu.coupon.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.coupon.service.SubCouponService;
import com.util.Ajax;
import com.util.Constants;
import com.util.Utils;

@Controller
@RequestMapping("/coupon")
public class CouponController extends BaseController<CouponController>{

	public static final Log logger = LogFactory.getLog(CouponController.class);
	
	@Autowired
	private SubCouponService subCouponService;
	
	/**
	 * 注册送优惠卷接口
	 * version 1.0
	 */
	@RequestMapping("/loginSendCoupon")
	public void loginSendCoupon(){
		 String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = subCouponService.loginSendCoupon(data);
			
		 } catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 分享送优惠卷接口
	 * version 1.0
	 */
	@RequestMapping("/shareSendCoupon")
	public void shareSendCoupon(){
		 String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = subCouponService.shareSendCoupon(data);
			
		 } catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 分享朋友圈活动送优惠卷接口
	 * version 1.0
	 */
	@RequestMapping("/weChatShareSendCoupon")
	public void weChatShareSendCoupon(){
		 String result = null;
		 try {
			String data = request.getParameter("data");
			result = subCouponService.weChatShareSendCoupon(data);
		 } catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 获取当前用户所有优惠卷列表
	 * version 1.0
	 */
	@RequestMapping("/getCouponAllList")
	public void getCouponAllList(){
		
		String result = null;
		
		try {
			
			String data = request.getParameter("data");
			
			result = subCouponService.getCouponAllList(data);
		}catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 获取用户可用优惠卷列表
	 * version 1.0
	 */
	@RequestMapping("/getCouponIsUedList")
	public void getCouponIsUedList(){
		
		String result = null;
		try {
			
			String data = request.getParameter("data");
			
			result = subCouponService.getCouponIsUedList(data);
		}catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 确认优惠卷接口 
	 * version 1.0
	 */
	@RequestMapping("/confirmCoupon")
	public void confirmCoupon(){
		String result = null;
		try {
			
			String data = request.getParameter("data");
			
			result = subCouponService.confirmCoupon(data);
		}catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	
}
