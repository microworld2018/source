package com.leetu.coupon.mapper;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.coupon.entity.SubCoupon;

public interface SubCouponMapper {

	/**
	 * 添加
	 * @param subCoupon
	 */
	int addSubCoupon(SubCoupon subCoupon);
	
	/**
	 * 修改
	 * @param subCoupon
	 */
	int updateSubCoupon(SubCoupon subCoupon);
	
	int updateSubCouponInfo(@Param("orderId")String orderId,@Param("isUsed")String isUsed,@Param("id")String id);
	
	/**
	 * 根据id删除
	 * @param id
	 */
	int deleteById(String id);
	
	/**
	 * 根据条件查询
	 * @param subCoupon
	 * @return
	 */
	List<SubCoupon> getSubCouponList(SubCoupon subCoupon);
	
	/**
	 * 根据条件查询分页
	 * @param paramMap
	 * @return
	 */
	List<Map<String, String>> getSubCouponPage(Map<String, Object> paramMap);
	
	LinkedList<Map<String, String>> getSubCouponPageAllList(Map<String, Object> paramMap);
	
	/**
	 * 根据条件查询数量
	 * @param subCoupon
	 * @return
	 */
	Integer getSubCouponCount(Map<String, Object> paramMap);
	
	/**
	 * 根据会员ID、订单ID查询个人优惠卷
	 * @param orderId
	 * @return
	 */
	SubCoupon getSubCouponByOrderId(@Param("orderId")String orderId,@Param("subId")String subId);
	
	/**
	 * 获取会员可用优惠卷数量
	 * @param paramMap
	 * @return
	 */
	Integer getSubCouponUsedCount(Map<String, Object> paramMap);
	
	/**
	 * 根据优惠券id和用户id获取用户有没有该类型优惠券
	 * @param param
	 * @return
	 */
	SubCoupon getSubCouponByParam(@Param("couponId")String couponId, @Param("subId")String subId);
	
	/**根据id获取关联信息
	 * @param id
	 * @return
	 */
	SubCoupon getById(@Param("id")String id);
    
	/**
	 * @Description 根据订单ID,更改关联信息的订单id
	 * @param       subCoupon
	 * @author      ge jiaming
	 * @date        201-08-07
	 */
	void updateSubCouponByOrderId(SubCoupon subCoupon);
}
