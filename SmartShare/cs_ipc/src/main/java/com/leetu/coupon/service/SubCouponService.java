package com.leetu.coupon.service;

import java.util.List;
import java.util.Map;

import com.leetu.coupon.entity.SubCoupon;

public interface SubCouponService {

	/**
	 * 添加
	 * @param subCoupon
	 */
	void addSubCoupon(SubCoupon subCoupon);
	
	/**
	 * 修改
	 * @param subCoupon
	 */
	void updateSubCoupon(SubCoupon subCoupon);
	
	/**
	 * 根据id删除
	 * @param id
	 */
	void deleteById(String id);
	
	/**
	 * 根据条件查询
	 * @param subCoupon
	 * @return
	 */
	List<SubCoupon> getSubCouponList(SubCoupon subCoupon);
	
	/**
	 * 根据条件查询分页
	 * @param paramMap
	 * @return
	 */
	List<Map<String, String>> getSubCouponPage(Map<String, Object> paramMap);
	
	/**
	 * 根据条件查询数量
	 * @param subCoupon
	 * @return
	 */
	Integer getSubCouponCount(Map<String, Object> paramMap);
	
	/**
	 * 根据会员ID、订单ID查询个人优惠卷
	 * @param orderId
	 * @param subId
	 * @return
	 */
	SubCoupon getSubCouponByOrderId(String orderId,String subId);
	
	/**
	 * 注册送优惠卷接口
	 * @param data
	 * @return
	 */
	String loginSendCoupon(String data);
	
	/**
	 * 分享送优惠卷接口
	 * @param data
	 * @return
	 */
	String shareSendCoupon(String data);
	
	/**
	 * 朋友圈分享活动送优惠卷接口
	 * @param data
	 * @return
	 */
	String weChatShareSendCoupon(String data);
	
	/**
	 * 获取用户所有优惠卷接口
	 * @param data
	 * @return
	 */
	String getCouponAllList(String data);
	
	/**
	 * 获取当前用户可用优惠卷接口
	 * @param data
	 * @return
	 */
	String getCouponIsUedList(String data);
	
	/**
	 * 确认优惠卷接口
	 * @param data
	 * @return
	 */
	String confirmCoupon(String data);
	
	/**
	 * 获取会员可用优惠卷数量
	 * @param paramMap
	 * @return
	 */
	Integer getSubCouponUsedCount(Map<String, Object> paramMap);

	
}
