package com.leetu.coupon.service.impl;

import com.leetu.coupon.entity.SpoCouponHoliday;
import com.leetu.coupon.mapper.SpoCouponHolidayMapper;
import com.leetu.coupon.service.SpoCouponHolidayService;

public class SpoCouponHolidayServiceImpl implements SpoCouponHolidayService {
	
	private SpoCouponHolidayMapper spoCouponHolidayMapper;

	@Override
	public SpoCouponHoliday getParam(String data) {
		return spoCouponHolidayMapper.getParam(data);
	}

}
