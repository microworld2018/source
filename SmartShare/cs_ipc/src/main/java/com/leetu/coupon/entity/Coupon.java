package com.leetu.coupon.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
/**
 * 优惠券
 * @author hzw
 *
 */
public class Coupon  implements Serializable{

	private static final long serialVersionUID = -4202966135865296648L;

	public static final Integer COUPON_IS_USED_NO = 0;//不可用
	public static final Integer COUPON_IS_USED_YES = 1;//可用 
	
	
	public static final Integer COUPON_TYPE_LOGIN = 7;//注册登录优惠卷
	
	public static final Integer COUPON_TYPE_SHARE = 2;//分享送优惠卷
	
	public static final Integer COUPON_TYPE_WECHATSHARE = 11;//朋友圈分享送券
	
	private String id;
	
	private String couponName;//优惠券名字
	
	private Integer couponType;//类型
	
	private String couponContent;//描述
	
	private BigDecimal price;//金额
	
	private Integer isUsed;//是否启用 0否 1是
	
	private Date startValidityTime;//有效期开始时间
	
	private Date endValidityTime;//有效期结束时间
	
	private Date ts;//时间戳
	
	private Integer number;//发行数量
	
	private Integer sendNumber;//发放数量
	
	private Integer sendType;//发放类型 0:安用户发放
	
	private String picture;//图片地址
	
	private BigDecimal orderUsePrice;//可使用订单金额
	
	private Date sendStartTime;//发放开始日期
	
	private Date sendEndTime;//发放结束日期
	
	private String remark;
	
	private Integer validPeriod;//有效天数
	
	private Integer isHoliday;//是否节假日用券（0：通用1：工作日2：节假日）
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getCouponName() {
		return couponName;
	}
	
	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}
	
	public Integer getCouponType() {
		return couponType;
	}
	
	public void setCouponType(Integer couponType) {
		this.couponType = couponType;
	}
	
	public String getCouponContent() {
		return couponContent;
	}
	
	public void setCouponContent(String couponContent) {
		this.couponContent = couponContent;
	}
	
	public BigDecimal getPrice() {
		return price;
	}
	
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	public Integer getIsUsed() {
		return isUsed;
	}
	
	public void setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
	}
	
	public Date getStartValidityTime() {
		return startValidityTime;
	}
	
	public void setStartValidityTime(Date startValidityTime) {
		this.startValidityTime = startValidityTime;
	}
	
	public Date getEndValidityTime() {
		return endValidityTime;
	}
	
	public void setEndValidityTime(Date endValidityTime) {
		this.endValidityTime = endValidityTime;
	}
	
	public Date getTs() {
		
		return ts;
	}
	
	public void setTs(Date ts) {
		this.ts = ts;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getSendNumber() {
		return sendNumber;
	}

	public void setSendNumber(Integer sendNumber) {
		this.sendNumber = sendNumber;
	}

	public Integer getSendType() {
		return sendType;
	}

	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public BigDecimal getOrderUsePrice() {
		return orderUsePrice;
	}

	public void setOrderUsePrice(BigDecimal orderUsePrice) {
		this.orderUsePrice = orderUsePrice;
	}

	public Date getSendStartTime() {
		return sendStartTime;
	}

	public void setSendStartTime(Date sendStartTime) {
		this.sendStartTime = sendStartTime;
	}

	public Date getSendEndTime() {
		return sendEndTime;
	}

	public void setSendEndTime(Date sendEndTime) {
		this.sendEndTime = sendEndTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getValidPeriod() {
		return validPeriod;
	}

	public void setValidPeriod(Integer validPeriod) {
		this.validPeriod = validPeriod;
	}

	public Integer getIsHoliday() {
		return isHoliday;
	}

	public void setIsHoliday(Integer isHoliday) {
		this.isHoliday = isHoliday;
	}
	
}
