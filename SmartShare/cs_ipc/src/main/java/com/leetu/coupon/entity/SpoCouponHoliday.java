package com.leetu.coupon.entity;

import java.io.Serializable;
import java.util.Date;

/**假日实体类
 * @author 18833
 *
 */
public class SpoCouponHoliday  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id;//假日ID
	
	private Date holiday;//假日时间
	
	private Integer isUse;//是否可用

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getHoliday() {
		return holiday;
	}

	public void setHoliday(Date holiday) {
		this.holiday = holiday;
	}

	public Integer getIsUse() {
		return isUse;
	}

	public void setIsUse(Integer isUse) {
		this.isUse = isUse;
	}
	
}
