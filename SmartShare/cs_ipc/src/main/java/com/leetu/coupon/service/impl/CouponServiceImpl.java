package com.leetu.coupon.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.mapper.CouponMapper;
import com.leetu.coupon.service.CouponService;

@Transactional
@Service
public class CouponServiceImpl implements CouponService{

	@Autowired
	private CouponMapper couponMapper;
	
	@Override
	public Coupon getCouponByType(Integer couponType) {
		return couponMapper.getCouponByType(couponType);
	}

	@Override
	public void updateCouponSendNumber(Coupon coupon) {
		couponMapper.updateCouponSendNumber(coupon);
	}

	@Override
	public Coupon getCouonByOrderId(String orderId) {
		return couponMapper.getCouonByOrderId(orderId);
	}

}
