package com.leetu.coupon.mapper;

import org.apache.ibatis.annotations.Param;

import com.leetu.coupon.entity.SpoCouponHoliday;

public interface SpoCouponHolidayMapper {
	/**根据时间查询
	 * @param data
	 * @return
	 */
	SpoCouponHoliday getParam(@Param("holiday")String holiday);

}
