package com.leetu.coupon.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.entity.SpoCouponHoliday;
import com.leetu.coupon.entity.SubCoupon;
import com.leetu.coupon.mapper.CouponMapper;
import com.leetu.coupon.mapper.SpoCouponHolidayMapper;
import com.leetu.coupon.mapper.SubCouponMapper;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.subscriber.entity.Subscriber;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.DateUtil;
import com.util.JsonTools;
import com.util.TokenUtils;
import com.util.ToolDateTime;
import com.util.page.Pager;
@Service
@Transactional
public class SubCouponServiceImpl implements SubCouponService {

	@Autowired
	private SubCouponMapper subCouponMapper;
	@Autowired
	private CouponMapper couponMapper;
	@Autowired
	private SpoCouponHolidayMapper spoCouponHolidayMapper;
	
	@Override
	public void addSubCoupon(SubCoupon subCoupon) {
		subCouponMapper.addSubCoupon(subCoupon);
	}

	@Override
	public void updateSubCoupon(SubCoupon subCoupon) {
		subCouponMapper.updateSubCoupon(subCoupon);

	}

	@Override
	public void deleteById(String id) {
		subCouponMapper.deleteById(id);
	}

	@Override
	public List<SubCoupon> getSubCouponList(SubCoupon subCoupon) {
		return subCouponMapper.getSubCouponList(subCoupon);
	}

	@Override
	public List<Map<String, String>> getSubCouponPage(Map<String, Object> paramMap) {
		return subCouponMapper.getSubCouponPage(paramMap);
	}

	@Override
	public Integer getSubCouponCount(Map<String, Object> paramMap) {
		return subCouponMapper.getSubCouponCount(paramMap);
	}
	
	/**
	 * 获取会员可用优惠卷数量
	 * @param paramMap
	 * @return
	 */
	@Override
	public Integer getSubCouponUsedCount(Map<String, Object> paramMap){
		return subCouponMapper.getSubCouponUsedCount(paramMap);
	}

	/**
	 * 注册送优惠卷接口
	 */
	public String loginSendCoupon(String subId){
		
		String result = null;
		
		Integer sendNumber = 0; //优惠卷发放数量
		
		//查询注册优惠卷
		Coupon coupon = couponMapper.getCouponByType(Coupon.COUPON_TYPE_LOGIN);
		
		if(coupon == null || "".equals(coupon)){
			return Ajax.AppJsonResult(Constants.APP_COUPON_LOGIN_NULL,Constants.APP_COUPON_LOGIN_NULL_MSG);
		}
		
		sendNumber = coupon.getSendNumber();
		
		SubCoupon subCoupon = new SubCoupon();
		
		subCoupon.setCouponId(coupon.getId());
		
		subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_NO);//未使用
		
		subCoupon.setSubId(subId);
		
		//计算用户优惠卷过期时间 =优惠卷有效天数+用户获取优惠卷时间
		subCoupon.setExpirationTime(ToolDateTime.getFetureDate(coupon.getValidPeriod()));
		
		int count = subCouponMapper.addSubCoupon(subCoupon);
		
		if(count > 0){
			
			sendNumber ++;
			coupon.setSendNumber(sendNumber);
			
			couponMapper.updateCouponSendNumber(coupon);
			
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS,Constants.APP_SUCCESS_MSG);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_ERROR,Constants.APP_ERROR_MSG);
		}
		
		return result;
	}
	
	/**
	 * 分享送优惠卷接口
	 */
	public String shareSendCoupon(String data){
		
		Subscriber s = TokenUtils.getSubscriber(data);
		
		String result = null;
		
		Integer sendNumber = 0; //优惠卷发放数量
		
		Map<String, String> map = JsonTools.desjsonForMap(data);
		if(map.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		
		//查询注册优惠卷
		Coupon coupon = couponMapper.getCouponByType(Coupon.COUPON_TYPE_SHARE);
		
		if(coupon == null || "".equals(coupon)){
			return Ajax.AppJsonResult(Constants.APP_COUPON_LOGIN_NULL,Constants.APP_COUPON_LOGIN_NULL_MSG);
		}
		
		sendNumber = coupon.getSendNumber();
		
		SubCoupon subCoupon = new SubCoupon();
		
		subCoupon.setCouponId(coupon.getId());
		
		subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_NO);//未使用
		
		subCoupon.setSubId(s.getId());
		
		//计算用户优惠卷过期时间 =优惠卷有效天数+用户获取优惠卷时间
		subCoupon.setExpirationTime(ToolDateTime.getFetureDate(coupon.getValidPeriod()));
		
		int count = subCouponMapper.addSubCoupon(subCoupon);
		
		if(count > 0){
			
			sendNumber ++;
			coupon.setSendNumber(sendNumber);
			
			couponMapper.updateCouponSendNumber(coupon);
			
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS,Constants.APP_SUCCESS_MSG);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_ERROR,Constants.APP_ERROR_MSG);
		}
		
		return result;
	}
	
	/**
	 * 朋友圈分享活动送优惠卷接口
	 */
	@Override
	public String weChatShareSendCoupon(String data){
		Subscriber s = TokenUtils.getSubscriber(data);
		String result = null;
		Integer sendNumber = 0; //优惠卷发放数量
		
		Map<String, String> map = JsonTools.desjsonForMap(data);
		if(map.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		
		//查询朋友圈分享活动优惠卷
		Coupon coupon = couponMapper.getCouponByTypeInNow(Coupon.COUPON_TYPE_WECHATSHARE);
		if(coupon == null || "".equals(coupon)){
			return Ajax.AppJsonResult(Constants.APP_NOTIN_ACTIVITIES,Constants.APP_NOTIN_ACTIVITIES_MSG);
		}
		
		//根据优惠券id和用户id查询该用户是否有该类型的优惠券
		SubCoupon subCouponByCouponId = subCouponMapper.getSubCouponByParam(coupon.getId(), s.getId());
		if(subCouponByCouponId == null || "".equals(subCouponByCouponId)){
			sendNumber = coupon.getSendNumber();
			
			SubCoupon subCoupon = new SubCoupon();
			subCoupon.setCouponId(coupon.getId());
			subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_NO);//未使用
			subCoupon.setSubId(s.getId());
			
			//计算用户优惠卷过期时间 =优惠卷有效天数+用户获取优惠卷时间
			subCoupon.setExpirationTime(ToolDateTime.getFetureDate(coupon.getValidPeriod()));
			
			int count = subCouponMapper.addSubCoupon(subCoupon);
			if(count > 0){
				sendNumber ++;
				coupon.setSendNumber(sendNumber);
				couponMapper.updateCouponSendNumber(coupon);
				result = Ajax.AppJsonResult(Constants.APP_SUCCESS,Constants.APP_SUCCESS_MSG);
			}else{
				result = Ajax.AppJsonResult(Constants.APP_ERROR,Constants.APP_ERROR_MSG);
			}
		}else{
			return Ajax.AppJsonResult(Constants.APP_HAS_SAMECOUPON,Constants.APP_HAS_SAMECOUPON_MSG);
		}
		
		return result;
	}

	@Override
	public String getCouponAllList(String data) {
		
		LinkedList<Map<String, String>> subCouponList = new LinkedList<Map<String,String>>();
		
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		if(pmap.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(null == subscriber){
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		if(pmap.containsKey("currentPage")){
			
			if(pmap.get("currentPage") == null || "".equals(pmap.get("currentPage"))){
				return Ajax.AppJsonResult(Constants.APP_CURRENT_PAGE_NULL, Constants.APP_CURRENT_PAGE_NULL_MSG);
			}
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		SubCoupon subCoupon = new SubCoupon();
		subCoupon.setSubId(subscriber.getId());
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("subCoupon", subCoupon);
		paramMap.put("coupon", new Coupon());
		
		int countNum = subCouponMapper.getSubCouponCount(paramMap);//总共多少条数据
		
		Pager pager = new Pager();
		pager.setCurPage(Integer.parseInt(pmap.get("currentPage")));//当前页
		pager.setPageSize(10);//每页显示数量
		pager.setTotalRow(countNum); //共条数据
		pager.setStart(pager.getStart()); //开始行
		paramMap.put("pager", pager);
		
		LinkedList<Map<String, String>> list = subCouponMapper.getSubCouponPageAllList(paramMap);
		//未使用、未过期优惠卷存储
		LinkedList<Map<String, String>> noUsedList = new LinkedList<Map<String,String>>();
		//已使用、未过期/未使用、已过期优惠卷存储
		LinkedList<Map<String,String>>  usedList = new LinkedList<Map<String,String>>();
		
		Map<String, Object> res = new Hashtable<String, Object>();
		
		if(!list.isEmpty()){
			//循环集合，筛选数据
			for(Map<String, String> map : list){
				//遍历map
				//计算时间差 当前日期-有效日期
				int days = ToolDateTime.getDateDaySpace(ToolDateTime.parse(map.get("dateTime").toString(),ToolDateTime.pattern_ymd),ToolDateTime.parse(map.get("endValidityTime"),ToolDateTime.pattern_ymd));
				//未使用、未过期优惠卷
				if(String.valueOf(map.get("isUsed")).equals(SubCoupon.SUB_COUPON_IS_USED_NO+"") && days >=0){
					noUsedList.add(map);
				}else{
					usedList.add(map);
				}
			}
			
			if(noUsedList != null && noUsedList.size()> 0){
				
				for(Map<String, String> noUsedMap:noUsedList){
					subCouponList.add(noUsedMap);
				}
			}
			
			if(usedList != null && usedList.size() > 0){
				for(Map<String, String> usedMap :usedList){
					subCouponList.add(usedMap);
				}
			}
			
			LinkedList<Map<String, String>> newList = new LinkedList<Map<String,String>>();
			
			if(subCouponList != null && subCouponList.size() > 0){
				for(int i = pager.getStart();i<pager.getEnd();i++){
					newList.add(subCouponList.get(i));
				}
			}
			
			res.put("coupon", newList);
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_COUPON_LOGIN_NULL, Constants.APP_COUPON_LOGIN_NULL_MSG);
		}
		
		return result;
	}

	/**
	 * 获取用户可用优惠卷列表
	 */
	@Override
	public String getCouponIsUedList(String data) {
		
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		if(pmap.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(null == subscriber){
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		if(pmap.containsKey("currentPage")){
			if(pmap.get("currentPage") == null || "".equals(pmap.get("currentPage"))){
				return Ajax.AppJsonResult(Constants.APP_CURRENT_PAGE_NULL, Constants.APP_CURRENT_PAGE_NULL_MSG);
			}
		}else{
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		//金额
		if(pmap.containsKey("orderPrice")){
			if(pmap.get("orderPrice") == null || "".equals(pmap.get("orderPrice"))){
				return Ajax.AppJsonResult(Constants.APP_ORDERPRICE_NULL, Constants.APP_ORDERPRICE_NULL_MSG);
			}
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		SubCoupon subCoupon = new SubCoupon();
		subCoupon.setSubId(subscriber.getId());
		subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_NO);
		Coupon coupon = new Coupon();
		coupon.setIsUsed(Coupon.COUPON_IS_USED_YES);//可用
		
		BigDecimal orderPrice = new BigDecimal(Double.valueOf(pmap.get("orderPrice")));
		//四舍五入保留小数点后两位
		BigDecimal setScale = orderPrice.setScale(2,BigDecimal.ROUND_HALF_DOWN);
		
		coupon.setOrderUsePrice(setScale);
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("subCoupon", subCoupon);
		paramMap.put("coupon", coupon);
		
		Pager pager = new Pager();
		pager.setCurPage(Integer.parseInt(pmap.get("currentPage")));//当前页
		pager.setPageSize(10);//每页显示数量
		pager.setTotalRow(subCouponMapper.getSubCouponCount(paramMap)); //共条数据
		pager.setStart(pager.getStart()); //开始行
		paramMap.put("pager", pager);
		
		List<Map<String, String>> subCouponList = subCouponMapper.getSubCouponPage(paramMap);
		
		Map<String, Object> res = new Hashtable<String, Object>();
		
		if(!subCouponList.isEmpty()){
			res.put("coupon", subCouponList);
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_COUPON_LOGIN_NULL, Constants.APP_COUPON_LOGIN_NULL_MSG);
		}
		
		return result;
	}

	@Override
	public String confirmCoupon(String data) {
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		if(pmap.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(null == subscriber){
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		//优惠卷关联ID
		if(pmap.containsKey("subCouponId")){
			if(pmap.get("subCouponId") == null || "".equals(pmap.get("subCouponId"))){
				return Ajax.AppJsonResult(Constants.APP_COUPON_ID_NULL, Constants.APP_COUPON_ID_NULL_MSG);
			}
		}else{
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		//获取优惠券ID
		SubCoupon subCoupon1 = subCouponMapper.getById(pmap.get("subCouponId"));
		if(subCoupon1 == null){
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, "优惠券信息错误！");
		}
		
		//判断当前优惠券是否可用
		Coupon coupon = couponMapper.getCouponById(subCoupon1.getCouponId());
		if(coupon.getIsHoliday() != null){
			if(coupon.getIsHoliday() == 1 || coupon.getIsHoliday() == 2){
				//判断是否是工作日
				if(coupon.getIsHoliday() == 1){
					Date data1 = new Date();
					String data2 = DateUtil.formatDate(data1, DateUtil.getChar10DateString());
					try {
						SpoCouponHoliday couponHoliday = spoCouponHolidayMapper.getParam(data2);
						if(couponHoliday != null){
							return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL,"工作日用券，节假日不可用");
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				//判断是否是节假日
				if(coupon.getIsHoliday() == 2){
					Date data1 = new Date();
					String data2 = DateUtil.formatDate(data1, DateUtil.getChar10DateString());
					SpoCouponHoliday couponHoliday = spoCouponHolidayMapper.getParam(data2);
					if(couponHoliday == null){
						return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL,"节假日用券，工作日不可用");
					}
				}
			}
		}
		
		//订单ID
		if(pmap.containsKey("orderId")){
			
			if(pmap.get("orderId") == null || "".equals(pmap.get("orderId"))){
				return Ajax.AppJsonResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG);
			}
		}else{
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		SubCoupon sb = subCouponMapper.getSubCouponByOrderId(pmap.get("orderId"), subscriber.getId());
		
		if(sb != null && !"".equals(sb)){
			subCouponMapper.updateSubCouponInfo("",SubCoupon.SUB_COUPON_IS_USED_NO+"",sb.getId());
		}
		
		SubCoupon subCoupon = new SubCoupon();
		subCoupon.setSubId(subscriber.getId());
		subCoupon.setOrderId(pmap.get("orderId"));
		subCoupon.setId(pmap.get("subCouponId"));
//		subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
		
		
		//更新优惠卷关联表
		int count = subCouponMapper.updateSubCoupon(subCoupon);
		
		if(count > 0){
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS,Constants.APP_SUCCESS_MSG);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_ERROR,Constants.APP_ERROR_MSG);
		}
		
		return result;
	}

	@Override
	public SubCoupon getSubCouponByOrderId(String orderId,
			String subId) {
		return subCouponMapper.getSubCouponByOrderId(orderId, subId);
	}

}
