package com.leetu.coupon.service;

import com.leetu.coupon.entity.Coupon;

public interface CouponService {

	/**
	 * 根据类型获取优惠卷
	 * @param coupon
	 * @return
	 */
	Coupon getCouponByType(Integer couponType);
	
	/**
	 * 优惠卷修改发放数量
	 * @param coupon
	 */
	void updateCouponSendNumber(Coupon coupon);
	
	/**
	 * 根据订单ID查询优惠卷信息
	 * @param orderId
	 * @return
	 */
	Coupon getCouonByOrderId(String orderId);
}
