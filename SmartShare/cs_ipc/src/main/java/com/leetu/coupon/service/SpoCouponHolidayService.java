package com.leetu.coupon.service;

import com.leetu.coupon.entity.SpoCouponHoliday;

public interface SpoCouponHolidayService {
	
	/**根据时间查询
	 * @param data
	 * @return
	 */
	SpoCouponHoliday getParam(String data);

}
