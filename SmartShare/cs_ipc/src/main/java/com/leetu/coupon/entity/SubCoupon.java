package com.leetu.coupon.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class SubCoupon implements Serializable{
	
	private static final long serialVersionUID = -5399020798342638234L;

	public static final Integer SUB_COUPON_IS_USED_NO = 0;//未使用
	public static final Integer SUB_COUPON_IS_USED_YES = 1;//已使用
	
	private String id;//id
	
	private String couponId;//优惠卷ID
	
	private String subId;//会员ID
	
	private int isUsed;//是否使用0:未使用 1:已使用
	
	private Date ts;//时间戳
	
	private String orderId;//订单ID
	
	private Date expirationTime;//过期时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public int getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(int isUsed) {
		this.isUsed = isUsed;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}
	
}
