package com.leetu.deposit.mapper;

import java.util.Map;

public interface RefundRecordMapper {

	
	/**
	 * 会员退款申请记录添加
	 * @param paramMap
	 */
	void addRefundRecord(Map<String, Object> paramMap);
}
