package com.leetu.deposit.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.deposit.mapper.RefundRecordMapper;
import com.leetu.deposit.service.RefundRecordService;

@Service
@Transactional
public class RefundRecordServiceImpl implements RefundRecordService{

	@Autowired
	private RefundRecordMapper refundRecordMapper;
	
	/**
	 * 会员退款申请记录添加
	 * @param paramMap
	 */
	@Override
	public void addRefundRecord(Map<String, Object> paramMap){
		refundRecordMapper.addRefundRecord(paramMap);
	}
}
