package com.leetu.deposit.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface DepositRecordMapper {
	
	/**
	 * 获取会员押金记录信息
	 * @param subId
	 * @return
	 */
	List<Map<?, ?>> getDepositRecordBySubId(Map<String, Object> paramMap);
	
	/**
	 * 获取会员押金记录信息个数-分页使用
	 * @param subId
	 * @return
	 */
	Integer getDepositRecordCount(@Param("subId")String subId);
	
	/**
	 * 根据交易编号获取会员押金记录信息
	 * @param tradeOrderNo
	 * @return
	 */
	Map<String, Object> getDepositRecordByRradeOrderNo(@Param("tradeOrderNo")String tradeOrderNo);
	
	/**
	 * 根据交易编号修改会员押金记录状态
	 * @param tradeOrderNo
	 * @return
	 */
	void upDepositRecordByTradeOrderNo(@Param("tradeOrderNo")String tradeOrderNo);
	
	/**
	 * 会员押金记录添加
	 * @param paramMap
	 */
	void addDepositRecord(Map<String, Object> paramMap);
	
	/**
	 * 根据会员ID获取押金信息
	 * @param subId
	 * @return
	 */
	List<Map<String, Object>> getDepositBySubId(@Param("subId")String subId);
	
}
