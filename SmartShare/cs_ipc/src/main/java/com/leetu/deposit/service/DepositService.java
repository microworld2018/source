package com.leetu.deposit.service;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface DepositService {

	/**
	 * 根据会员ID获取会员押金信息
	 * @param subId
	 * @return
	 */
	Map<String, Object> getDepositBySubId(String subId);

	/**
	 * 根据会员ID修改会员押金信息
	 * @param subId
	 */
	void updateDepositBySubId(Map<String, Object> paramMap);
	
	/**
	 * 添加会员押金信息
	 * @param paramMap
	 */
	void addDeposit(Map<String, Object> paramMap);
}
