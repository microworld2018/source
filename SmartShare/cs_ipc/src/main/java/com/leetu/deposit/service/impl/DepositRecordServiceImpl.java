package com.leetu.deposit.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.deposit.mapper.DepositRecordMapper;
import com.leetu.deposit.service.DepositRecordService;

@Service
@Transactional
public class DepositRecordServiceImpl implements DepositRecordService {

	@Autowired
	private DepositRecordMapper depositRecordMapper;
	
	/**
	 * 获取会员押金记录信息
	 * @param subId
	 * @return
	 */
	@Override
	public List<Map<?, ?>> getDepositRecordBySubId(Map<String, Object> paramMap) {
		
		return depositRecordMapper.getDepositRecordBySubId(paramMap);
	}
	
	public Integer getDepositRecordCount(String subId){
		
		return depositRecordMapper.getDepositRecordCount(subId);
	}
	
	/**
	 * 根据交易编号获取会员押金记录信息
	 * @param tradeOrderNo
	 * @return
	 */
	public Map<String, Object> getDepositRecordByRradeOrderNo(String tradeOrderNo){
		
		return depositRecordMapper.getDepositRecordByRradeOrderNo(tradeOrderNo);
	}
	
	/**
	 * 根据交易编号修改会员押金记录状态
	 * @param tradeOrderNo
	 * @return
	 */
	public void updateDepositRecordByTradeOrderNo(String tradeOrderNo){
		
		depositRecordMapper.upDepositRecordByTradeOrderNo(tradeOrderNo);
	}
	
	/**
	 * 会员押金记录添加
	 * @param paramMap
	 */
	public void addDepositRecord(Map<String, Object> paramMap){
		
		depositRecordMapper.addDepositRecord(paramMap);
	}
	
	/**
	 * 根据会员ID获取押金信息
	 * @param subId
	 * @return
	 */
	public List<Map<String, Object>> getDepositBySubId(String subId){
		return depositRecordMapper.getDepositBySubId(subId);
	}

}
