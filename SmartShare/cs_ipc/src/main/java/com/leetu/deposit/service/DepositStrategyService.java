package com.leetu.deposit.service;

import java.util.Map;

public interface DepositStrategyService {

	/**
	 * 获取租车押金金额
	 * @param type
	 * @return
	 */
	Map<String, Object> getDepositStrategy(String type);
}
