package com.leetu.deposit.mapper;

import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface DepositStrategyMapper {

	/**
	 * 获取租车押金金额
	 * @param type
	 * @return
	 */
	Map<String, Object> getDepositStrategy(@Param("type")String type);
}
