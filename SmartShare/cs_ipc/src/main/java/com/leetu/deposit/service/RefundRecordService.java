package com.leetu.deposit.service;

import java.util.Map;

public interface RefundRecordService {

	/**
	 * 会员退款申请记录添加
	 * @param paramMap
	 */
	void addRefundRecord(Map<String, Object> paramMap);
}
