package com.leetu.deposit.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 会员账户表
 * @author GodZilla
 *
 */
public class Deposit implements Serializable{

	private static final long serialVersionUID = 8359973261456962264L;

	private String id;//id
	
	private String subscriberId;//会员ID
	
	private Integer amountType;//金额类型0:账户金额1:押金金额
	
	private BigDecimal amount;//金额
	
	private BigDecimal frozenAmount;//冻结金额
	
	private BigDecimal usableAmount;//可用金额
	
	private Integer isRefund;//是否退款1.使用中2.申请退款3.退款中4.已退款
	
	private Date lastOperateTime;//最后操作时间
	
	private BigDecimal lastOperateAmount;//最后操作总额
	
	private Integer lastOperateType;//最后操作类型
	
	private Date createTime;//创建时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public Integer getAmountType() {
		return amountType;
	}

	public void setAmountType(Integer amountType) {
		this.amountType = amountType;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getFrozenAmount() {
		return frozenAmount;
	}

	public void setFrozenAmount(BigDecimal frozenAmount) {
		this.frozenAmount = frozenAmount;
	}

	public BigDecimal getUsableAmount() {
		return usableAmount;
	}

	public void setUsableAmount(BigDecimal usableAmount) {
		this.usableAmount = usableAmount;
	}

	public Integer getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(Integer isRefund) {
		this.isRefund = isRefund;
	}

	public Date getLastOperateTime() {
		return lastOperateTime;
	}

	public void setLastOperateTime(Date lastOperateTime) {
		this.lastOperateTime = lastOperateTime;
	}

	public BigDecimal getLastOperateAmount() {
		return lastOperateAmount;
	}

	public void setLastOperateAmount(BigDecimal lastOperateAmount) {
		this.lastOperateAmount = lastOperateAmount;
	}

	public Integer getLastOperateType() {
		return lastOperateType;
	}

	public void setLastOperateType(Integer lastOperateType) {
		this.lastOperateType = lastOperateType;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
