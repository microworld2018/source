package com.leetu.deposit.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * 会员退款申请记录
 * @author GodZilla
 *
 */
public class RefundRecord implements Serializable{

	private static final long serialVersionUID = -4505852105745907445L;
	
	public static final Integer REFUND_TYPE_ACCOUNT = 0;//账户余额退款
	public static final Integer REFUND_TYPE_DEPOSIT = 1;//押金退款
	
	public static final Integer REFUND_STATUS_APPLY = 1;//申请退款
	public static final Integer REFUND_STATUS_REFUND = 2;//退款中
	public static final Integer REFUND_STATUS_INTERRUPT = 3;//退款中断
	public static final Integer REFUND_STATUS_ALREADY = 4;//已退款
	
	private String id;
	
	private String subId;
	
	private String type;
	
	private String status;
	
	private String name;
	
	private String desc;
	
	private String money;
	
	private String actualMoney;
	
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getActualMoney() {
		return actualMoney;
	}

	public void setActualMoney(String actualMoney) {
		this.actualMoney = actualMoney;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
