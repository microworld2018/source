package com.leetu.deposit.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.deposit.mapper.DepositStrategyMapper;
import com.leetu.deposit.service.DepositStrategyService;

@Service
@Transactional
public class DepositStrategyServiceImpl implements DepositStrategyService{

	@Autowired
	private DepositStrategyMapper depositStrategyMapper;
	
	/**
	 * 获取租车押金金额
	 * @param type
	 * @return
	 */
	@Override
	public Map<String, Object> getDepositStrategy(String type){
		
		return depositStrategyMapper.getDepositStrategy(type);
	}
}
