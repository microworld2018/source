package com.leetu.deposit.entity;

import java.io.Serializable;

/**
 * 押金规则-实体类
 * @author GodZilla
 *
 */
public class DepositStrategy implements Serializable{

	private static final long serialVersionUID = 5151120873028771384L;
	
	public static final String TYPE_DEPOSIT = "0";//租车押金类型
	
	private int id;
	
	private int type;
	
	private int isActive;
	
	private String desc;
	
	private double money;
	
	private String creatorId;
	
	private String createTime;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	
}
