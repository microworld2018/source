package com.leetu.deposit.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface DepositMapper {

	/**
	 * 根据会员ID获取会员押金信息
	 * @param subId
	 */
	Map<String, Object> getDepositBySubId(@Param("subId")String subId);
	
	/**
	 * 根据会员ID修改会员押金信息
	 * @param subId
	 */
	void upDepositBySubId(Map<String, Object> paramMap);
	
	/**
	 * 添加会员押金信息
	 * @param paramMap
	 */
	void addDeposit(Map<String, Object> paramMap);
}
