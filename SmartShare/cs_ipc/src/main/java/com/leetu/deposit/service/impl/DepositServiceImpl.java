package com.leetu.deposit.service.impl;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.leetu.deposit.mapper.DepositMapper;
import com.leetu.deposit.mapper.DepositRecordMapper;
import com.leetu.deposit.service.DepositService;
import com.leetu.subscriber.entity.Subscriber;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.TokenUtils;
import com.util.page.Pager;

@Transactional
@Service
public class DepositServiceImpl implements DepositService{

	@Autowired
	private DepositMapper depositMapper;
	
	@Autowired
	private DepositRecordMapper depositRecordMapper;

	/**
	 * 根据会员ID获取会员押金信息
	 * @param subId
	 * @return
	 */
	@Override
	public Map<String, Object> getDepositBySubId(String subId) {
		return depositMapper.getDepositBySubId(subId);
	}
	
	/**
	 * 根据会员ID修改会员押金信息
	 * @param subId
	 */
	public void updateDepositBySubId(Map<String, Object> paramMap){
		depositMapper.upDepositBySubId(paramMap);
	}
	
	/**
	 * 添加会员押金信息
	 * @param paramMap
	 */
	public void addDeposit(Map<String, Object> paramMap){
		depositMapper.addDeposit(paramMap);
	}
	
}
