package com.leetu.deposit.controller;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.core.controller.BaseController;
import com.leetu.coupon.controller.CouponController;
import com.leetu.deposit.service.DepositService;

@Controller
@RequestMapping("/deposit")
public class DepositController extends BaseController<DepositController>{

	public static final Log logger = LogFactory.getLog(CouponController.class); 
	
	@Autowired
	private DepositService depositService;
	
}
