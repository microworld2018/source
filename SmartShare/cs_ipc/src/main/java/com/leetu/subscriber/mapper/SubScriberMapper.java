package com.leetu.subscriber.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ground.notice.entity.GroundNotice;
import com.leetu.account.entity.Account;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.entity.SubscriberConfirm;
import com.leetu.subscriber.entity.SubscriberLoginRecord;
import com.leetu.sys.entity.BusinessFlow;
import com.leetu.sys.entity.SMSCode;
import com.leetu.sys.entity.SMSRecord;

public interface SubScriberMapper {

	/**
	 * 记录短信记录
	 * @param smsCode2
	 */
	void addSMSCode(SMSCode smsCode2);

	/**
	 * 手机号查询用户
	 * @param phoneNo
	 * @return
	 */
	Subscriber querySubscriberByPhoneNo(@Param("phoneNo")String phoneNo);
	
	/**根据身份证号查询用户信息
	 * @param idCard
	 * @return
	 */
	Subscriber querySubscriberByIdCard(@Param("idCard")String idCard);

	/**
	 * 
	 * @param type
	 * @param channel
	 * @param phoneNo
	 * @param tm
	 * @return
	 */
	SMSCode LatestSMSCode(@Param("type")String type, @Param("channel")Integer channel, @Param("phoneNo")String phoneNo,
			@Param("tm")Integer tm);

	/**
	 * 创建会员基本信息
	 * @param subscriber
	 */
	void addSubscriber(Subscriber subscriber);

	/**
	 * 创建会员账户信息
	 * @param account
	 */
	void addAccount(Account account);

	/**
	 * 更新最后登录时间
	 * @param loginSubscriber
	 */
	void updateSubscriber(Subscriber loginSubscriber);

	/**
	 * 添加认证信息
	 * @param s
	 */
	void updateSubRegLH(Subscriber s);
	
	/**
	 * 记录登录日志
	 * @param loginRecord
	 */
	void addSubscriberLoginRecord(SubscriberLoginRecord loginRecord);

	/**
	 * 通过id查询用户信息
	 * @param id
	 * @return
	 */
	Subscriber querySubscriberById(@Param("id")String id);

	/**
	 * 添加审核信息
	 * @param subscriberConfirm
	 */
	void addSubscriberConfirm(SubscriberConfirm subscriberConfirm);

	/**
	 * 添加审核信息
	 * @param businessFlow
	 */
	void addBusinessFlow(BusinessFlow businessFlow);

	/**
	 * 添加短信日志
	 * @param record
	 */
	void addSmsRecord(SMSRecord record);

	/**
	 * 修改个人信息
	 * @param param
	 */
	void upSubsinfo(Map<String, Object> param);

	/**
	 * 审核失败的原因
	 * @param id
	 * @return
	 */
	List<Map<String, Object>>businessFlow(@Param("id")String id);
	
	/**
	 * 获取所有会员信息
	 * @return
	 */
	List<Subscriber> getAllSubScriberList();

	GroundNotice querySubscriberByIdAndOrdernoTake(String id, String orderNo);
	
	GroundNotice querySubscriberByIdAndOrdernoBack(String id, String orderNo, String backDotId);

}
