package com.leetu.subscriber.mapper;

import org.apache.ibatis.annotations.Param;

import com.leetu.subscriber.entity.Activity;

public interface ActivityMapper {

	int addUserInformation(Activity activity);

	int getActivityIdByUUID(@Param("activityId") String activityId);

}
