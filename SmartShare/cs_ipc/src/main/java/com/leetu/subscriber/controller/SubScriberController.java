package com.leetu.subscriber.controller;

import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.core.controller.BaseController;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.subscriber.service.impl.SubscriberServiceImpl;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.Utils;
/**
 * 个人中心 注册登录
 * @author hzw
 *
 */
@Controller
@RequestMapping("sub")
public class SubScriberController extends BaseController<SubScriberController> {

	public static final Log logger = LogFactory.getLog(SubScriberController.class);
	
	@Autowired
	private SubScriberService scriberService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private SubscriberServiceImpl subscriberServiceImpl;
	
	/**
	 * token失效
	 */
	@RequestMapping("/isnotoken")
	public void isnotoken() {
		
		String result = Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		
		Utils.outJSONObject(result,response);
	}
	/**
	 * 全锁状态，清空token
	 */
	@RequestMapping("/eventStateFull")
	public void eventStateFull(){
		
		String result = null;
		
		try {
			//获取参数
			String token = request.getParameter("token");
			//业务处理
			result = subscriberServiceImpl.eventStateFull(token);
			
		} catch (Exception e) {//异常处理
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	/**
	 * 参数格式错误
	 */
	@RequestMapping("/isparamerror")
	public void isparamerror() {
		
		String result = null;
		
		try {
			//获取参数
			String data = request.getParameter("data");
			
			Map<String, String> map = JsonTools.desjsonForMap(data);
			
			if(map.get("param").equals("error")){
				
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			}
		} catch (Exception e) {//异常处理
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 1.发送短信验证码 (1.0.1)
	 * @author hzw
	 */
	@RequestMapping("/sendCode")
	public void sendCode(){
		
	    String result = null;
	    
		try {
			//获取参数
			String data = request.getParameter("data");
			//业务处理
			result = subscriberServiceImpl.sendCode(data);
			
	    } catch (Exception e) {//异常处理
	    	
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
	    }
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 2.app手机短信登录 (1.0.1)
	 * @author hzw
	 */
	@RequestMapping("/mobileCodeLogin")
	public void mobileCodeLogin(){
		
		String result = null;
		
		try {
			//获取参数
			String data = request.getParameter("data");
			//业务处理
			result = subscriberServiceImpl.mobileCodeLogin(data,request);
			
		} catch (Exception e) {//异常处理
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	
	/**
	* 3. 获取个人资料信息(1.0.1)
	* @author hzw
	*/
	@RequestMapping("/appSubsinfo")
	public void appSubsinfo(){
		
		String result = null;
		
		try{
			//获取参数
			String data = request.getParameter("data");
			//业务处理
			result = subscriberServiceImpl.appSubsinfo(data);
			
		}catch(Exception e){//异常处理
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 4.实名认证 LH (1.0.1)
	 *@author hzw
	 */
	@RequestMapping("/appregLH")
	public void appregLH(){
		
		String result = null; 
		
		try {
			//获取参数
			String data = request.getParameter("data");
			//业务处理
//			result = subscriberServiceImpl.realName(data);
			result = Ajax.AppJsonResult(Constants.SYS_VERSION_OLD, Constants.SYS_VERSION_OLD_MSG);
			
		} catch (Exception e) {//异常处理
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	
	/**
	 * 4.实名认证 LH (1.5.1)
	 *@author hzw
	 */
	@RequestMapping("/appregLHNew1.5.1")
	public void appregLHNew(){
		
		String result = null; 
		
		try {
			//获取参数
			String data = request.getParameter("data");
			//业务处理
			result = subscriberServiceImpl.realNameNew(data);
			
		} catch (Exception e) {//异常处理
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 10.修改个人信息(1.0.1)
	 * @author hzw
	 */
	@RequestMapping("/upSubsinfo")
	public void upSubsinfo(){
		
		String result = null; 
		
		try {
			//获取参数
			String data = request.getParameter("data");
			//业务处理
			result = subscriberServiceImpl.upSubsinfo(data);
			
		} catch (Exception e) {//异常处理
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 安全退出
	 */
	@RequestMapping("/exit")
	public void exit(){
		
		String result = null;
		
		try {
			//获取参数
			String data = request.getParameter("data");
			//业务处理
			result = subscriberServiceImpl.exit(data);
			
		} catch (Exception e) {//异常处理
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
	
}
