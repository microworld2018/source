package com.leetu.subscriber.service;

import java.util.List;
import java.util.Map;

import com.ground.notice.entity.GroundNotice;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.entity.SubscriberLoginRecord;
import com.leetu.sys.entity.SMSCode;
import com.leetu.sys.entity.SMSRecord;

public interface SubScriberService {

	/**
	 * 手机号查询用户信息
	 * @param phoneNo
	 * @return
	 */
	Subscriber querySubscriberByPhoneNo(String phoneNo);
	
	
	/**根据身份证号查询用户信息
	 * @param idCard
	 * @return
	 */
	Subscriber querySubscriberByIdCard(String idCard);

	/**
	 * 发送短信
	 * @param type
	 * @param phoneNo
	 * @param payChannelApp
	 * @return
	 */
	String sendSMSCode(String type, String phoneNo, Integer payChannelApp);

	/**
	 * 验证码校验
	 * @param smsCode
	 * @param registerSmsValidMinute
	 * @return
	 */
	SMSCode LatestSMSCode(SMSCode smsCode, Integer registerSmsValidMinute);

	/**
	 * 注册
	 * @param subscriber
	 * @return
	 */
	Subscriber addSubscriber(Subscriber subscriber);

	/**
	 * 更新登录状态
	 * @param loginSubscriber
	 */
	void updateSubscriber(Subscriber loginSubscriber);

	/**
	 * 添加登录日志
	 * @param loginRecord
	 */
	void addSubscriberLoginRecord(SubscriberLoginRecord loginRecord);

	/**
	 * 实名认证
	 * @param subscriber
	 * @param b
	 */
	void appregLH(Subscriber subscriber, boolean b);

	/**
	 * 通过id查询用户信息
	 * @param id
	 * @return
	 */
	Subscriber querySubscriberById(String id);

	/**
	 * 添加短信日志
	 * @param record
	 */
	void addSmsRecord(SMSRecord record);

	/**
	 * 修改个人信息
	 * @param param
	 */
	void upSubsinfo(Map<String, Object> param);

	/**
	 * 审核失败的原因
	 * @param id
	 * @return
	 */
	List<Map<String, Object>> businessFlow(String id);

	/**
	 * 账户信息
	 * @param id
	 * @return
	 */
	Map<String, Object> subAccountInfo(String id);

    /**
     * 获取发送给用户的取车消息内容
     * @param id
     * @param orderNo
     * @return
     */
	GroundNotice querySubscriberByIdAndOrdernoTake(String id, String orderNo);

	/**
     * 获取发送给用户的还车消息内容
     * @param id
     * @param orderNo
     * @return
     */
	GroundNotice querySubscriberByIdAndOrdernoBack(String id, String orderNo, String backDotId);
	

}
