package com.leetu.subscriber.mapper;

import com.leetu.subscriber.entity.ActivityMember;

public interface ActivityMemberMpper {
	
	/**添加活动会员信息
	 * @param activityMember
	 */
	public void saveActivityMember(ActivityMember activityMember);

}
