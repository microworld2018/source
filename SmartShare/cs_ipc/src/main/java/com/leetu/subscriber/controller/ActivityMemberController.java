package com.leetu.subscriber.controller;

import java.util.Date;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.subscriber.entity.ActivityMember;
import com.leetu.subscriber.service.ActivityMemberService;
import com.util.Ajax;
import com.util.Constants;
import com.util.Utils;


@Controller
@RequestMapping("/activityMember")
public class ActivityMemberController  extends BaseController<ActivityMember>{
	
	public static final Log logger = LogFactory.getLog(ActivityMemberController.class);
	
	@Autowired
	private ActivityMemberService activityMemberService; 
	
	/**
	 * 添加活动会员信息
	 */
	@RequestMapping("/saveActivityMember")
	public void saveActivityMember(){
		String result = null;
		//获取参数
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
//		String data = "";
		try {
			result = activityMemberService.saveActivityMember(name,phone);
			
		} catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
}
