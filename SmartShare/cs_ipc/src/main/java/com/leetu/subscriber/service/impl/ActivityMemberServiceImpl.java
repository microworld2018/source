package com.leetu.subscriber.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.account.entity.Account;
import com.leetu.subscriber.entity.ActivityMember;
import com.leetu.subscriber.mapper.ActivityMemberMpper;
import com.leetu.subscriber.service.ActivityMemberService;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;

@Service
@Transactional
public class ActivityMemberServiceImpl implements ActivityMemberService {
	
	@Autowired
	private ActivityMemberMpper activityMemberMpper;

	@Override
	public String saveActivityMember(String name,String phone) {
		String result = null;
		Map<String,String> map = new HashMap<String, String>();
		ActivityMember activityMember = new ActivityMember();
//		activityMember.setName("sdjfkls");
//		activityMember.setPhone("151561");
		
//		map = JsonTools.jsonForMap(data);
		activityMember.setName(name);
		activityMember.setPhone(phone);
		
		try {
//			map = JsonTools.jsonForMap(data);
//			name = map.get("name");
//			phone = map.get("phone");
			
			
			
			activityMember.setCreateTime(new Date());
		} catch (Exception e) {
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
//		activityMember.setName(name);
//		activityMember.setPhone(phone);
		
		try {
			activityMemberMpper.saveActivityMember(activityMember);
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		} catch (Exception e) {
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			e.printStackTrace();
		}
		return result; 
	}

}
