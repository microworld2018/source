package com.leetu.subscriber.entity;

import java.io.Serializable;
import java.util.Date;

public class ActivityMember implements Serializable{


	private static final long serialVersionUID = 1L;

	private String id;
	
	private String name;
	
	private String phone;
	
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
}
