package com.leetu.subscriber.controller;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.core.controller.BaseController;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.subscriber.entity.Activity;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.mapper.SubScriberMapper;
import com.leetu.subscriber.service.ActivityService;
import com.leetu.subscriber.service.impl.SubscriberServiceImpl;
import com.util.Ajax;
import com.util.Utils;
/**
 * @Description  活动类,主要用来管理各种活动
 * @author ge
 * @Date   2017-08-17
 *
 */
@Controller
@RequestMapping("/activity")
public class ActivityController extends BaseController<Activity>{
	@Autowired
	private SubScriberMapper subScriberMapper;
	
	@Autowired
	private SubscriberServiceImpl subscriberServiceImpl;
	
	@Autowired
	private SubCouponService subCouponService;
	
	@Autowired
	private ActivityService activityService;
	
	/**
	 * @Dscription  
	 * @param phone
	 * @param activityId
	 * @return
	 * @author ge
	 * @Date   2017-08-18
	 */
	@RequestMapping("/downSendCoupon")
	public void downSendCoupon(String phone,String activityId){
		String result="";
		if(phone.trim().length() == 0){
			result = Ajax.AppJsonResult(0,"请输入手机号！");
			 Utils.outJSONObject(result,response);
		}
		if(phone.trim().length() != 11){
			result = Ajax.AppJsonResult(1,"手机号位数不对！");
			 Utils.outJSONObject(result,response);
		}
		boolean isMobile = isMobile(phone);
		if(!isMobile){
			result = Ajax.AppJsonResult(2,"手机号输入有误！");
			 Utils.outJSONObject(result,response);
		}
		Subscriber loginSubscriber=subScriberMapper.querySubscriberByPhoneNo(phone);
		if(loginSubscriber == null){
			//获取活动id
			int activity_id=activityService.getActivityIdByUUID(activityId);
			if(activity_id == 0){
				result = Ajax.AppJsonResult(3,"活动失效！");
				 Utils.outJSONObject(result,response);
			}
			//向活动列表中添加用户记录
			Activity activity=new Activity();
			activity.setCreateTime(new Date());
			activity.setMobile(phone);
			activity.setActivityId(activity_id);
			int addResult=activityService.addUserInformation(activity);
			if(addResult == 0){
				result = Ajax.AppJsonResult(4,"添加活动记录失败！");
				 Utils.outJSONObject(result,response);
			}
			Subscriber subscriber = new Subscriber();
			subscriber.setPhoneNo(phone);
			loginSubscriber=subscriberServiceImpl.addSubscriber(subscriber);
			loginSubscriber=subScriberMapper.querySubscriberByPhoneNo(phone);
			String id = loginSubscriber.getId();
			//添加优惠卷
			subCouponService.loginSendCoupon(id);
			result = Ajax.AppJsonResult(5,"注册成功,优惠券已发放！");
		}else{
			result = Ajax.AppJsonResult(6,"手机号已注册！");
		}
		 Utils.outJSONObject(result,response);
	}
	
	
	/** 
     * 手机号验证 
     *  
     * @param  str 
     * @return 验证通过返回true 
     */  
    public static boolean isMobile(String str) {   
        Pattern p = null;  
        Matcher m = null;  
        boolean b = false;   
        p = Pattern.compile("^[1][3,4,5,7,8][0-9]{9}$"); // 验证手机号  
        m = p.matcher(str);  
        b = m.matches();   
        return b;  
    }  
}
