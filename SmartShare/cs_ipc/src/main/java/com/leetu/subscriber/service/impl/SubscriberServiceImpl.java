package com.leetu.subscriber.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.ground.notice.entity.GroundNotice;
import com.leetu.account.entity.Account;
import com.leetu.account.mapper.AccountMapper;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.deposit.mapper.DepositMapper;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.entity.SubscriberConfirm;
import com.leetu.subscriber.entity.SubscriberLoginRecord;
import com.leetu.subscriber.mapper.SubScriberMapper;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.entity.BusinessFlow;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.entity.RunLog;
import com.leetu.sys.entity.SMSCode;
import com.leetu.sys.entity.SMSRecord;
import com.leetu.sys.mapper.DictMapper;
import com.leetu.sys.service.RunLogService;
import com.util.Ajax;
import com.util.Constants;
import com.util.CryptoTools;
import com.util.IpUtil;
import com.util.JsonTools;
import com.util.Md5Util;
import com.util.PropertiesUtil;
import com.util.Sha1Util;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.ValidatorUtil;
import com.util.msg.MsgContent;
import com.util.msg.sendMSg;

@Service
@Transactional
public class SubscriberServiceImpl implements SubScriberService{

	@Autowired
	private SubScriberMapper subScriberMapper;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private DepositMapper depositMapper;
	
	@Autowired
	private DictMapper dictMapper;
	
	@Autowired
	private SubCouponService subCouponService;
	
	@Autowired
	private RunLogService runLogService;
	
	
	@Override
	public Subscriber querySubscriberByPhoneNo(String phoneNo) {
		return subScriberMapper.querySubscriberByPhoneNo(phoneNo);
	}

	/**
	 * 发送短信
	 */
	@Override
	public String sendSMSCode(String type, String phoneNo, Integer channel) {
		
		if(StringUtils.isEmpty(phoneNo)){
			return Ajax.AppJsonResult(Constants.APP_MOBILE_NULL, Constants.APP_MOBILE_NULL_MSG);
		}
		
		Subscriber s=querySubscriberByPhoneNo(phoneNo);
		
		SMSCode smsCode=new SMSCode();
		smsCode.setPhoneNo(phoneNo);
		
		if(channel!=null){
			smsCode.setChannel(channel);
		}
		
		smsCode.setType(type);
		smsCode=LatestSMSCode(smsCode, Constants.REGISTER_SMS_VALID_RETRY_MINUTE);
		
		if(smsCode!=null){
			return Ajax.AppJsonResult(Constants.APP_SMS_TIMER_OUT, Constants.APP_SMS_TIMER_OUT_MSG);
		}
		
		StringBuffer code=new StringBuffer(); 
		Random r=new Random(); 
		
		for(int i=0;i<4;i++){ 
			code.append(r.nextInt(10));
		}; 
		
		
		String message=null;
		
		if(Constants.SUBSCIRBER_REGISTER_PHONE_CODE.equals(type)){
			
//			 message="【电动侠租车】用户注册验证码:"+code+"，此验证码5分钟内有效，如非本人操作请忽略。";
			 message = MsgContent.getSmsMsg(MsgContent.USER_REGISTRATION_VERIFICATION_CODE_MSG,code.toString());
			 
			 if(s != null){
				  return Ajax.AppJsonResult(Constants.APP_MOBILE_EXIST, Constants.APP_MOBILE_EXIST_MSG);
			 }
				
		}else if(Constants.SUBSCIRBER_CHANGE_NEW_PHONE_CODE.equals(type)){
			
//			 message="【电动侠租车】用户修改手机号验证码:"+code+"，此验证码5分钟内有效，如非本人操作请忽略。";
			 message = MsgContent.getSmsMsg(MsgContent.USER_UPDATE_PHONE_CODE_MSG,code.toString());
			 if(s != null){
				  return Ajax.AppJsonResult(Constants.APP_MOBILE_EXIST, Constants.APP_MOBILE_EXIST_MSG);
			}
		}else if(Constants.SUBSCIRBER_CHANGE_OLD_PHONE_CODE.equals(type)){
//			 message=" 【电动侠租车】用户修改手机号验证码:"+code+"，此验证码5分钟内有效，如非本人操作请忽略。";
			 message = MsgContent.getSmsMsg(MsgContent.USER_UPDATE_PHONE_CODE_MSG,code.toString());
			
			 if(s == null){
				  return Ajax.AppJsonResult(Constants.APP_MOBILE_NULL,Constants.APP_MOBILE_NULL_MSG);
			 }
		}else if(Constants.SUBSCRIBER_LOGIN_CODE.equals(type)){
//			 message=" 【电动侠租车】用户登录验证码:"+code+"，此验证码5分钟内有效，如非本人操作请忽略。";
			 message = MsgContent.getSmsMsg(MsgContent.USER_LOGIN_COCE_MSG,code.toString());
			 if(s == null){
				  //用户未注册
				  return Ajax.AppJsonResult(Constants.APP_NOT_REGISTERED, Constants.APP_NOT_REGISTERED_MSG);
			 }
		}
		
		
		if(sendMSg.send(phoneNo, message)){
			
			SMSRecord record = new SMSRecord();
			record.setContent(message);
			record.setPhoneNo(phoneNo);
			record.setType(1);
			
			if(s!=null){
				record.setUserId(s.getId());
				record.setUserName(s.getName());
			}
			
			record.setResult(phoneNo+":Success");
			
			subScriberMapper.addSmsRecord(record);
			
			SMSCode smsCode2=new SMSCode();
			smsCode2.setType(type);
			smsCode2.setPhoneNo(phoneNo);
			smsCode2.setChannel(channel);
			smsCode2.setMessage(message);
			smsCode2.setCode(code.toString());
			smsCode2.setTs(new Date());
			
			subScriberMapper.addSMSCode(smsCode2);
			
			return Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_SENT_FAILED, Constants.APP_SENT_FAILED_MSG);
		}
	}
	

	/**
	 * 120秒内只能获取一次验证码
	 * @param smsCode
	 * @param registerSmsValidRetryMinute
	 * @return
	 */
	public SMSCode LatestSMSCode(SMSCode smsCode,
			Integer tm) {
		String type = smsCode.getType();
		Integer channel = smsCode.getChannel();
		String phoneNo = smsCode.getPhoneNo();
 		SMSCode MSCode = subScriberMapper.LatestSMSCode(type,channel,phoneNo,tm);
		return MSCode;
	}

	/**
	 * 注册
	 */
	@Override
	public Subscriber addSubscriber(Subscriber s) {
		if(s==null){
			throw new RuntimeException("创建注册用户参数为空");
		}
		if(StringUtils.isEmpty(s.getPhoneNo())){
			throw new RuntimeException("创建注册用户手机号为空");
		}
			
		
		//创建会员基本信息
		Subscriber subscriber = new Subscriber();
		subscriber.setPhoneNo(s.getPhoneNo());
		subscriber.setPassword(Sha1Util.SHA1Encode(s.getPassword()));
		subscriber.setState(Subscriber.STATE_UNCONFIRMED);
		subscriber.setTs(new Date());
		subscriber.setCreateDate(new Date());
		subscriber.setSource(s.SOURCE_PORTAL);
		subScriberMapper.addSubscriber(subscriber);
		
		//创建会员账户信息
		Account account=new Account();
		account.setSubscriberId(subscriber.getId());
		account.setAmount(0d);
		account.setFrozenAmount(0d);
		account.setUsableAmount(0d);
		account.setTs(new Date());
		subScriberMapper.addAccount(account);
		
		Map<String, Object> depositMap = new HashMap<String, Object>();
		
		depositMap.put("subId", subscriber.getId());
		depositMap.put("amount", 0);
		depositMap.put("frozenAmount", 0);
		depositMap.put("usableAmount", 0);
		depositMap.put("isRefund", 0);
		//创建会员押金信息
		depositMapper.addDeposit(depositMap);
		return null;
	}

	/**
	 * 更新最后登录时间
	 */
	@Override
	public void updateSubscriber(Subscriber loginSubscriber) {
		subScriberMapper.updateSubscriber(loginSubscriber);
	}

	/**
	 * 记录登录日志
	 */
	@Override
	public void addSubscriberLoginRecord(SubscriberLoginRecord loginRecord) {
		subScriberMapper.addSubscriberLoginRecord(loginRecord);
	}

	/**
	 * 实名认证
	 */
	@Override
	public void appregLH(Subscriber s, boolean isMobile) {
		//保存用户上传信息
		subScriberMapper.updateSubRegLH(s);
		//审核信息
		SubscriberConfirm subscriberConfirm= new SubscriberConfirm();
		subscriberConfirm.setSubscriberId(s.getId());
		subscriberConfirm.setIsComplete(SubscriberConfirm.IS_COMPLETE_FALSE);
		if(isMobile){
			subscriberConfirm.setAttribute_1("app");
		}
		subScriberMapper.addSubscriberConfirm(subscriberConfirm);
		
		BusinessFlow businessFlow = new BusinessFlow();
		businessFlow.setApplicantId(s.getId());
		businessFlow.setApplyTime(new Date());
		businessFlow.setTs(new Date());
		businessFlow.setConfirmId(subscriberConfirm.getId());
		businessFlow.setBusinessType(BusinessFlow.BUSINESS_TYPE_CONFIRM);
		subScriberMapper.addBusinessFlow(businessFlow);
		
	}

	@Override
	public Subscriber querySubscriberById(String id) {
		return subScriberMapper.querySubscriberById(id);
	}

	@Override
	public void addSmsRecord(SMSRecord record) {
		subScriberMapper.addSmsRecord(record);
	}

	@Override
	public void upSubsinfo(Map<String, Object> param) {
		subScriberMapper.upSubsinfo(param);
	}

	@Override
	public List<Map<String, Object>> businessFlow(String id) {
		return subScriberMapper.businessFlow(id);
	}

	@Override
	public Map<String, Object> subAccountInfo(String id) {
		return accountMapper.subAccountInfo(id);
	}
	
	/**
	 * 全锁状态清空缓存
	 * @param request
	 * @return
	 */
	public String eventStateFull(String token){
		
		int database = DataBaseUtil.LOGIN_COMMON;
		Map<String, String> toktenmap = TokenUtils.getTokenDecoder(token);
		String key = toktenmap.get("md5Code")+"token";
		//token缓存中移除
		RedisClient.deleteData(key, database);
		
		return Ajax.AppResult(Constants.APP_EVENT_STATE_FUll,Constants.APP_EVENT_STATE_FUll_MSG,null);
	}
	
	/**
	 * 发送短信
	 * @param request
	 * @param scriberService
	 * @return
	 */
	public String sendCode(String data){
		try {
			
			Map<String, String> map = JsonTools.desjsonForMap(data);
			
			if(map.get("param").equals("error")){
				return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			}
			
			String type = map.get("type");
			String phoneNo = map.get("phoneNo");
			
			if(!StringUtils.isEmpty(type)){
				
				if("new".equals(type)){
					
					type=Constants.SUBSCIRBER_CHANGE_NEW_PHONE_CODE;
				}else if("old".equals(type)){
					
					type=Constants.SUBSCIRBER_CHANGE_OLD_PHONE_CODE;
				}else if("login".equals(type)){
					
					Subscriber s=subScriberMapper.querySubscriberByPhoneNo(phoneNo);
					
					if(s!=null){
						
						type=Constants.SUBSCRIBER_LOGIN_CODE;
					}else{
						
						type=Constants.SUBSCIRBER_REGISTER_PHONE_CODE;
					}
				}
			}else{
				
				return Ajax.AppJsonResult(Constants.APP_TYPE_NULL, Constants.APP_TYPE_NULL_MSG);
			}
			
			if(phoneNo.isEmpty()){
				
				return Ajax.AppJsonResult(Constants.APP_MOBILE_NULL, Constants.APP_MOBILE_NULL_MSG);
			}
			
			if(!ValidatorUtil.isPhone(phoneNo)){
				
				return Ajax.AppJsonResult(Constants.APP_LOGIN_MOBILE_STYLE, Constants.APP_LOGIN_MOBILE_STYLE_MSG);
			}
			
			return sendSMSCode(type,phoneNo,Account.PAY_CHANNEL_APP);
		} catch (Exception e) {
			
			e.printStackTrace();
			return Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG); 
		}
	}
	
	/**
	 * 手机短信登录
	 * @param request
	 * @param scriberService
	 * @return
	 */
	public String mobileCodeLogin(String data,HttpServletRequest request){
		String result = null;
		int timeout = 7*24*60*60;//token失效时间  一个星期
		try{
			Map<String, String> map = JsonTools.desjsonForMap(data);
			if(map.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			/** 请求参数校验**/
			String  phoneNoStr=map.get("phoneNo");
			String  smsCodeStr=map.get("smsCode");
			if(StringUtils.isEmpty(phoneNoStr)||StringUtils.isEmpty(smsCodeStr)){
				result=Ajax.AppJsonResult(Constants.APP_LOGIN_NULL, Constants.APP_LOGIN_NULL_MSG);
				return result;
			}
			if(!ValidatorUtil.isPhone(phoneNoStr)){
				result = Ajax.AppJsonResult(Constants.APP_LOGIN_MOBILE_STYLE, Constants.APP_LOGIN_MOBILE_STYLE_MSG);
				return result;
			}
			Subscriber loginSubscriber=subScriberMapper.querySubscriberByPhoneNo(phoneNoStr);
			
			/**
			 * 发布测试使用
			 */
			 /*if(!phoneNoStr.equals("15688889928")){
				 	*//**
					 * 验证码校验
					 *//*
					SMSCode smsCode=new SMSCode();
					smsCode.setPhoneNo(phoneNoStr);
					if(loginSubscriber==null){
						smsCode.setType(Constants.SUBSCIRBER_REGISTER_PHONE_CODE);
					}else{
						smsCode.setType(Constants.SUBSCRIBER_LOGIN_CODE);
					}
					smsCode.setChannel(Account.PAY_CHANNEL_APP);
					smsCode = LatestSMSCode(smsCode,Constants.REGISTER_SMS_VALID_MINUTE );
					if(smsCode==null){
						result = Ajax.AppJsonResult(Constants.APP_MOBILE_CODE_NO, Constants.APP_MOBILE_CODE_MSG);
						return result;
					}
					
					if(!smsCodeStr.equals(smsCode.getCode())){
						result = Ajax.AppJsonResult(Constants.APP_MOBILE_CODE_ERROR, Constants.APP_MOBILE_CODE_ERROR_MSG);
						return result;
					}
			 }*/
			
			//注册流程
			if(loginSubscriber==null){
				Subscriber subscriber = new Subscriber();
				subscriber.setPhoneNo(phoneNoStr);
				loginSubscriber=addSubscriber(subscriber);
				loginSubscriber=subScriberMapper.querySubscriberByPhoneNo(phoneNoStr);
				//生成Token存入MemCached
				String id = loginSubscriber.getId();
				Date date= new Date();
				String token = TokenUtils.getTokenEncode(String.valueOf(date.getTime()),id);
				String md5id = Md5Util.MD5Encode(id);
				
				boolean r = RedisClient.settimedata(md5id+"token", token,timeout,DataBaseUtil.LOGIN_COMMON);
				boolean redis = RedisClient.settimeobjdata(md5id, loginSubscriber,timeout,DataBaseUtil.LOGIN_COMMON);
				if(!r&&!redis){
					result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
					return result;
				}
						
				Map<String,Object> rm = new HashMap<String,Object>();
				rm.put("token", token);
				rm.put("phoneNoStr", phoneNoStr);
				result = Ajax.AppResult(Constants.APP_LOGIN_SUCCESS, Constants.APP_LOGIN_SUCCESS_MSG,rm);
				//添加优惠卷
				subCouponService.loginSendCoupon(id);
				
			}else{
				//登录流程	
				if(Subscriber.EVENT_STATE_FUll.equals(loginSubscriber.getEventState())){
					result=Ajax.AppJsonResult(Constants.APP_EVENT_STATE_FUll, Constants.APP_EVENT_STATE_FUll_MSG);
					return result;
				}
				 updateSubscriber(loginSubscriber);
		         //生成Token存入MemCached
				 String id = loginSubscriber.getId();
				 Date date= new Date();
				 String token = TokenUtils.getTokenEncode(String.valueOf(date.getTime()),id);
				 String md5id = Md5Util.MD5Encode(id);
				 
				boolean r = RedisClient.settimedata(md5id+"token", token,timeout,DataBaseUtil.LOGIN_COMMON);
				boolean redis = RedisClient.settimeobjdata(md5id, loginSubscriber,timeout,DataBaseUtil.LOGIN_COMMON);
				if(!r&&!redis){
					result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
					return result;
				}
				
				 
				Map<String,Object> rm = new HashMap<String,Object>();
				rm.put("token", token);
				rm.put("phoneNoStr", phoneNoStr);
				result = Ajax.AppResult(Constants.APP_LOGIN_SUCCESS, Constants.APP_LOGIN_SUCCESS_MSG,rm);
			}
			
			runLogService.addRunLogAppLogin(loginSubscriber.getName(), RunLog.APP_LOGIN_MSG,IpUtil.getIpAddr(request));
			
			/*SubscriberLoginRecord loginRecord =new SubscriberLoginRecord();
			loginRecord.setLoginTime(new Date());
			loginRecord.setSubscriberId(loginSubscriber.getId());
			loginRecord.setTs(new Date());
			loginRecord.setIp(IpUtil.getIpAddr(request));
			addSubscriberLoginRecord(loginRecord);*/

		}catch(Exception e){
			
			result=Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
			e.printStackTrace();
		}
		return result;
	}
	
	/**
	 * 获取个人信息资料
	 * @param data
	 * @return
	 */
	public String appSubsinfo(String data){
		
		String result = "";
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);
		
		subscriber = querySubscriberById(subscriber.getId());
		
		if(subscriber != null && !"".equals(subscriber) && !"null".equals(subscriber)){
			//个人资料集合
			Map<String,Object> map = new HashMap<String,Object>();
			//赋值
			map.put("name", subscriber.getName());
			map.put("phoneNo", subscriber.getPhoneNo());
			map.put("state", subscriber.getState());
			map.put("nickname", subscriber.getNickname());
			map.put("nickstatus", subscriber.getNickstatus());
			map.put("drivingLicenseImg", subscriber.getDrivingLicenseImg());
			map.put("idCardImg", subscriber.getIdCardImg());
			map.put("subId", subscriber.getId());//会员ID
			map.put("IDCard", subscriber.getIDCard());//身份证ID
			map.put("handHeldIdCard", subscriber.getHandHeldIdCard());//手持照片
			
			if(subscriber.getState().equals(Subscriber.STATE_NO_CONFIRMED)){
				//查询审核失败的原因
				/*List<Map<String, Object>> businessflow = subScriberMapper.businessFlow(subscriber.getId());
				
				if(businessflow != null && !"".equals(businessflow)){
					String [] result_desc = businessflow.get(0).get("result_desc").toString().split(",");
					String rdesc = "";
					for (int i = 0; i < result_desc.length; i++) {
						//获取字典表中车辆未租借状态
						Dict d = dictMapper.getDictByCodes("6", result_desc[i]);
						rdesc+=d.getCnName()+";";
					}
					map.put("stateDesc", rdesc);
				}*/
				map.put("stateDesc", "资料审核未通过！");
			}else if(subscriber.getState().equals(Subscriber.STATE_NORMAL)){
				map.put("stateDesc", "资料已审核通过！");
			}else if(subscriber.getState().equals(Subscriber.STATE_UNCONFIRMED)){
				map.put("stateDesc", "资料未提交");
			}else if(subscriber.getState().equals(Subscriber.STATE_WAIT_CONFIRMED)){
				map.put("stateDesc", "资料待审核");
			}
			result=Ajax.AppResult(Constants.APP_SUCCESS,Constants.APP_SUCCESS_MSG,map);
		}else{
			result=Ajax.AppResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG,null);
		}
		
		return result;
	}
	
	
	/**
	 * 实名认证
	 * @param request
	 * @param scriberService
	 * @return
	 */
	public String realName(String data){
		String result = null;
		//获取参数
		Map<String, String> map = JsonTools.desjsonForMap(data);
		if(map.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		String name = map.get("name");
		String idCardImg=map.get("idCardImg");//身份证
		String drivingImg=map.get("drivingImg");//驾驶证
		String IDCard = map.get("IDCard");
			
		//获取用户信息
		Subscriber subscriber = TokenUtils.getSubscriber(data);
		String id = subscriber.getId();
		String md5id = Md5Util.MD5Encode(id);
		subscriber = querySubscriberById(id);
		String state = subscriber.getState().toString();
		//验证参数
		if(StringUtils.isEmpty(name)){
			result = Ajax.AppJsonResult(Constants.APP_NAME_NULL, Constants.APP_NAME_NULL_MSG);
			return result;
		}
		if(StringUtils.isEmpty(idCardImg)){
			result = Ajax.AppJsonResult(Constants.APP_IDCARD_NULL, Constants.APP_IDCARD_NULL_MSG);
			return result;
		}
		if(StringUtils.isEmpty(drivingImg)){
			result = Ajax.AppJsonResult(Constants.APP_DRIVING_NULL, Constants.APP_DRIVING_NULL_MSG);
			return result;
		}
	    if(state.equals(Subscriber.STATE_WAIT_CONFIRMED+"")){
	    	result = Ajax.AppJsonResult(Constants.APP_STATE_WAIT_CONFIRMED, Constants.APP_STATE_WAIT_CONFIRMED_MSG);
			return result;
	    }
	    if(state.equals(Subscriber.STATE_NORMAL+"") || state.equals(Subscriber.STATE_CARD_ISSUED+"") ){
	    	result = Ajax.AppJsonResult(Constants.APP_STATE_NORMAL, Constants.APP_STATE_NORMAL_MSG);
			return result;
	    }
		    
		subscriber.setName(name);
		subscriber.setState(Subscriber.STATE_WAIT_CONFIRMED);
		subscriber.setIdCardImg(idCardImg);
		subscriber.setDrivingLicenseImg(drivingImg);
		subscriber.setIDCard(IDCard);
		appregLH(subscriber,true);
		
		Subscriber sub = querySubscriberById(id);
		//更新用户缓存信息
		RedisClient.setobjdata(md5id, sub,DataBaseUtil.LOGIN_COMMON);
		String mobile = subscriber.getPhoneNo();
//		String content = "【电动侠租车】您的证件已提交审核，请耐心等待。通过后，您将可以使用车辆！";
		String content = MsgContent.getSmsMsg(MsgContent.USER_SUBMIT_CARD_MSG);
		if(sendMSg.send(mobile, content)){
			//记录短信信息
			SMSRecord record = new SMSRecord();
			record.setContent(content);
			record.setPhoneNo(mobile);
			record.setType(2);
			record.setUserId(subscriber.getId());
			record.setUserName(subscriber.getName());
			record.setResult(mobile+":Success");
			addSmsRecord(record);
		}
		//短信发送内容，发送给客服人员
//		String sendContent = "【电动侠租车】用户:"+subscriber.getName()+"已提交资料审核，手机号:"+subscriber.getPhoneNo()+"，请及时审核资料！";
		String sendContent = MsgContent.getSmsMsg(MsgContent.SEND_SERVICE_MSG,subscriber.getName(),subscriber.getPhoneNo());
		
		String phoneNos = PropertiesUtil.getBundleValue("kf_mobile_phone");//获取客服人员手机号
		
		sendMSg.send(phoneNos, sendContent);
			
		result = Ajax.AppJsonResult(Constants.APP_SUBMIT_SUCCESS, Constants.APP_SUBMIT_SUCCESS_MSG);
		return result;
	}
	
	
	/**
	 * 新实名认证1.5.1
	 * @param request
	 * @param scriberService
	 * @return
	 */
	public String realNameNew(String data){
		String result = null;
		//获取参数
		Map<String, String> map = JsonTools.desjsonForMap(data);
		if(map.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		String name = map.get("name");
		String idCardImg=map.get("idCardImg");//身份证
		String drivingImg=map.get("drivingImg");//驾驶证
		String handHeldIdCard = map.get("handHeldIdCard");//手持身份证
		String IDCard = map.get("IDCard");
			
		//获取用户信息
		Subscriber subscriber = TokenUtils.getSubscriber(data);
		String id = subscriber.getId();
		String md5id = Md5Util.MD5Encode(id);
		subscriber = querySubscriberById(id);
		String state = subscriber.getState().toString();
		//验证参数
		if(StringUtils.isEmpty(name)){
			result = Ajax.AppJsonResult(Constants.APP_NAME_NULL, Constants.APP_NAME_NULL_MSG);
			return result;
		}
		
		Subscriber subscriberNew =  querySubscriberByIdCard(IDCard);
		
		//判断身份证号是否已认证
		if(subscriberNew != null){
			result = Ajax.AppJsonResult(Constants.APP_IDCARD_CERTIFICATION, Constants.APP_IDCARD_CERTIFICATION_MSG);
			return result;
		}
		
		if(StringUtils.isEmpty(idCardImg)){
			result = Ajax.AppJsonResult(Constants.APP_IDCARD_NULL, Constants.APP_IDCARD_NULL_MSG);
			return result;
		}
		if(StringUtils.isEmpty(drivingImg)){
			result = Ajax.AppJsonResult(Constants.APP_DRIVING_NULL, Constants.APP_DRIVING_NULL_MSG);
			return result;
		}
		//判断是否上传手持身份证
		if(StringUtils.isEmpty(handHeldIdCard)){
			result = Ajax.AppJsonResult(Constants.APP_HANDHELDIDCARD_NULL, Constants.APP_HANDHELDIDCARD_NULL_MSG);
			return result;
		}
	    if(state.equals(Subscriber.STATE_WAIT_CONFIRMED+"")){
	    	result = Ajax.AppJsonResult(Constants.APP_STATE_WAIT_CONFIRMED, Constants.APP_STATE_WAIT_CONFIRMED_MSG);
			return result;
	    }
	    if(StringHelper.isNotEmpty(subscriber.getHandHeldIdCard()) && state.equals(Subscriber.STATE_NORMAL+"")){
	    	result = Ajax.AppJsonResult(Constants.APP_STATE_NORMAL, Constants.APP_STATE_NORMAL_MSG);
			return result;
	    }
		    
		subscriber.setName(name);
		subscriber.setState(Subscriber.STATE_WAIT_CONFIRMED);
		subscriber.setIdCardImg(idCardImg);
		subscriber.setDrivingLicenseImg(drivingImg);
		subscriber.setIDCard(IDCard);
		subscriber.setHandHeldIdCard(handHeldIdCard);
		appregLH(subscriber,true);
		
		Subscriber sub = querySubscriberById(id);
		//更新用户缓存信息
		RedisClient.setobjdata(md5id, sub,DataBaseUtil.LOGIN_COMMON);
		String mobile = subscriber.getPhoneNo();
//		String content = "【电动侠租车】您的证件已提交审核，请耐心等待。通过后，您将可以使用车辆！";
		String content = MsgContent.getSmsMsg(MsgContent.USER_SUBMIT_CARD_MSG);
		if(sendMSg.send(mobile, content)){
			//记录短信信息
			SMSRecord record = new SMSRecord();
			record.setContent(content);
			record.setPhoneNo(mobile);
			record.setType(2);
			record.setUserId(subscriber.getId());
			record.setUserName(subscriber.getName());
			record.setResult(mobile+":Success");
			addSmsRecord(record);
		}
		//短信发送内容，发送给客服人员
//		String sendContent = "【电动侠租车】用户:"+subscriber.getName()+"已提交资料审核，手机号:"+subscriber.getPhoneNo()+"，请及时审核资料！";
		String sendContent = MsgContent.getSmsMsg(MsgContent.SEND_SERVICE_MSG,subscriber.getName(),subscriber.getPhoneNo());
		
		String phoneNos = PropertiesUtil.getBundleValue("kf_mobile_phone");//获取客服人员手机号
		
		sendMSg.send(phoneNos, sendContent);
			
		result = Ajax.AppJsonResult(Constants.APP_SUBMIT_SUCCESS, Constants.APP_SUBMIT_SUCCESS_MSG);
		return result;
	}
	
	
	/**
	 * 修改个人信息
	 * @param request
	 * @param scriberService
	 * @return
	 */
	public String upSubsinfo(String data){
		String result = null;
		//获取参数
		Subscriber s = TokenUtils.getSubscriber(data);
		Map<String, String> map = JsonTools.desjsonForMap(data);
		if(map.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		String nickname = map.get("nickname");
		String nickstatus = map.get("nickstatus");
		if(nickname==null||nickstatus==null){
			result = Ajax.AppJsonResult(Constants.APP_NICK_NULL, Constants.APP_NICK_NULL_MSG);
			return result;
		}
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("id", s.getId());
		param.put("nickname", nickname);
		param.put("nickstatus", nickstatus);
		upSubsinfo(param);
		
		String md5id = Md5Util.MD5Encode(s.getId());
		
		Subscriber subscriber = querySubscriberById(s.getId());
		//更新用户缓存信息
		RedisClient.setobjdata(md5id, subscriber,DataBaseUtil.LOGIN_COMMON);
		
		result=Ajax.AppResult(Constants.APP_SUCCESS,Constants.APP_SUCCESS_MSG,null);
		
		return result;
	}
	
	/**
	 * 安全退出
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public String exit(String data) throws Exception{
		
		String result = null;
			
		int database = DataBaseUtil.LOGIN_COMMON;
		//解密
		CryptoTools des = new CryptoTools("HeGeA8G3".getBytes(),"6LA2EyQm".getBytes());//自定义密钥
		data = des.decode(data);
		JSONObject jsonObject = JSONObject.fromObject(data);
			
		String apptoken = jsonObject.getString("token");//map.get("token");
		Map<String, String> toktenmap = TokenUtils.getTokenDecoder(apptoken);
		String key = toktenmap.get("md5Code")+"token";
		//token缓存中移除
		RedisClient.deleteData(key, database);
		result=Ajax.AppResult(Constants.EXIT_SUCCESS,Constants.EXIT_SUCCESS_MSG,null);
		
		return result;
	}

	@Override
	public Subscriber querySubscriberByIdCard(String idCard) {
		return subScriberMapper.querySubscriberByIdCard(idCard);
	}

	@Override
	public GroundNotice querySubscriberByIdAndOrdernoTake(String id, String orderNo) {
		// TODO Auto-generated method stub
		return subScriberMapper.querySubscriberByIdAndOrdernoTake(id,orderNo);
	}
	@Override
	public GroundNotice querySubscriberByIdAndOrdernoBack(String id, String orderNo,String backDotId ) {
		// TODO Auto-generated method stub
		return subScriberMapper.querySubscriberByIdAndOrdernoBack(id,orderNo,backDotId);
	}
}
