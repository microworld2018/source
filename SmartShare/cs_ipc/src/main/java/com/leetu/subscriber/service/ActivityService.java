package com.leetu.subscriber.service;

import org.springframework.stereotype.Service;

import com.leetu.subscriber.entity.Activity;


public interface ActivityService {

	int addUserInformation(Activity activity);

	int getActivityIdByUUID(String activityId);

}
