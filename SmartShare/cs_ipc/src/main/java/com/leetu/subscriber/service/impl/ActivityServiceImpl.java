package com.leetu.subscriber.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.subscriber.entity.Activity;
import com.leetu.subscriber.mapper.ActivityMapper;
import com.leetu.subscriber.service.ActivityService;
@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    private ActivityMapper activityMapper;
    
	@Override
	public int addUserInformation(Activity activity) {
		int result=activityMapper.addUserInformation(activity);
		return result;
	}
	@Override
	public int getActivityIdByUUID(String activityId) {
		int result = 0 ;
		result=activityMapper.getActivityIdByUUID(activityId);
		return result;
	}

}
