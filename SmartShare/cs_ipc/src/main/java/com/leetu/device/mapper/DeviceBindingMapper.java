package com.leetu.device.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.device.entity.DeviceBinding;

public interface DeviceBindingMapper {

	/**
	 * 根据车辆ID查询
	 * @param carId
	 * @return
	 */
	Map<String, String> getDeviceByCarId(@Param("carId")String carId);
}
