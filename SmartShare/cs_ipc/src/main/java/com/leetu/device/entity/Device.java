package com.leetu.device.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;


public class Device implements Serializable {

		private static final long serialVersionUID = 1L;
		
		/**主键ID*/
		private String id;
		/**内部编码*/
		//车机编号")
		private String setNo;
		/**车机厂家*/
		private String setName;
		//厂家名称")
		private String setNameTitle;
		/**车机型号*/
		private String setType;
		//车机型号")
		private String setTypeName;
		/**手机卡号*/
		//SIM卡号")
		private String simId;
		/**是否刷过固件*/
		private Integer updateFirmware;
		//是否刷过固件")
		private String updateFirmwareMark;
		/**当前固件版本*/
		//当前版本")
		private String firmwareVersion;
		/**车机状态*/
		private String setState;
		/**车机生产日期*/
		//生产日期")
		private Date productDate;
		/**车机首次启用日期*/
		//首次启用日期")
		private Date onlineDate;
		/**创建人Id*/
		private String creatorId;
		/**创建时间*/
		private Date createDate;
		/**时间戳*/
		private Date ts;
		//备注")
		private String remark;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getSetNo() {
			return setNo;
		}
		public void setSetNo(String setNo) {
			this.setNo = setNo;
		}
		public String getSetName() {
			return setName;
		}
		public void setSetName(String setName) {
			this.setName = setName;
		}
		public String getSetNameTitle() {
			return setNameTitle;
		}
		public void setSetNameTitle(String setNameTitle) {
			this.setNameTitle = setNameTitle;
		}
		public String getSetType() {
			return setType;
		}
		public void setSetType(String setType) {
			this.setType = setType;
		}
		public String getSetTypeName() {
			return setTypeName;
		}
		public void setSetTypeName(String setTypeName) {
			this.setTypeName = setTypeName;
		}
		public String getSimId() {
			return simId;
		}
		public void setSimId(String simId) {
			this.simId = simId;
		}
		public Integer getUpdateFirmware() {
			return updateFirmware;
		}
		public void setUpdateFirmware(Integer updateFirmware) {
			this.updateFirmware = updateFirmware;
		}
		public String getUpdateFirmwareMark() {
			return updateFirmwareMark;
		}
		public void setUpdateFirmwareMark(String updateFirmwareMark) {
			this.updateFirmwareMark = updateFirmwareMark;
		}
		public String getFirmwareVersion() {
			return firmwareVersion;
		}
		public void setFirmwareVersion(String firmwareVersion) {
			this.firmwareVersion = firmwareVersion;
		}
		public String getSetState() {
			return setState;
		}
		public void setSetState(String setState) {
			this.setState = setState;
		}
		public Date getProductDate() {
			return productDate;
		}
		public void setProductDate(Date productDate) {
			this.productDate = productDate;
		}
		public Date getOnlineDate() {
			return onlineDate;
		}
		public void setOnlineDate(Date onlineDate) {
			this.onlineDate = onlineDate;
		}
		public String getCreatorId() {
			return creatorId;
		}
		public void setCreatorId(String creatorId) {
			this.creatorId = creatorId;
		}
		public Date getCreateDate() {
			return createDate;
		}
		public void setCreateDate(Date createDate) {
			this.createDate = createDate;
		}
		public Date getTs() {
			return ts;
		}
		public void setTs(Date ts) {
			this.ts = ts;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		 
}
