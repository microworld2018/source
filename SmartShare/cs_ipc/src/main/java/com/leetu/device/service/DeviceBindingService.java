package com.leetu.device.service;

import java.util.Map;

public interface DeviceBindingService {
	/**
	 * 根据车辆ID查询
	 * @param carId
	 * @return
	 */
	Map<String, String> getDeviceByCarId(String carId);
}
