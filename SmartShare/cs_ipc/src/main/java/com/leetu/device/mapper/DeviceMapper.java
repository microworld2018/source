package com.leetu.device.mapper;

import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;

public interface DeviceMapper {

	/**
	 * 查询 车辆对应车机编号
	 * @param t
	 * @return
	 */
	List<Map<String, String>> carAndDevice(@Param("t")String t);
	
}
