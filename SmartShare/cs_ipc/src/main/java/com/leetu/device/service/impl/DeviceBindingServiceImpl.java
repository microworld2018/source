package com.leetu.device.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.device.entity.DeviceBinding;
import com.leetu.device.mapper.DeviceBindingMapper;
import com.leetu.device.service.DeviceBindingService;

@Service
public class DeviceBindingServiceImpl implements DeviceBindingService{

	@Autowired
	private DeviceBindingMapper deviceBindingMapper;
	/**
	 * 根据车辆ID查询
	 * @param carId
	 * @return
	 */
	@Override
	public Map<String, String> getDeviceByCarId(String carId){
		
		return deviceBindingMapper.getDeviceByCarId(carId);
	}
}
