package com.leetu.device.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.device.mapper.DeviceMapper;
import com.leetu.device.service.DeviceService;
import com.leetu.sys.mapper.DictMapper;

@Service
public class DeviceServiceImpl implements DeviceService{
	@Autowired
	private DeviceMapper deviceMapper;
	@Autowired
	private DictMapper dictMapper;
	
	@Override
	public List<Map<String, String>> carAndDevice(String t) {
		return deviceMapper.carAndDevice(t);
	}
	
	
}
