package com.leetu.device.service;

import java.util.List;
import java.util.Map;

public interface DeviceService {

	/**
	 * 查询 车辆对应车机编号
	 * @param t
	 * @return
	 */
	List<Map<String, String>> carAndDevice(String t);
}
