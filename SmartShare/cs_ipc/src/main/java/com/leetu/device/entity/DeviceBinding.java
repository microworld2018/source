package com.leetu.device.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.leetu.car.entity.Car;


public class DeviceBinding implements Serializable {
	
	private static final long serialVersionUID = 1L;
	/**主键*/
	private String id;
	/**车辆ID*/
	private String carId;
	/**车机Id*/
	private String deviceId;
	/**车机编码*/
	//车机编号") 
	private String deviceNo;
	/**绑定类型*/
	private Integer bindType;
	
	//绑定状态") 
	private String bindTypeName;
	
	/**绑定时间*/
	private Date bindDate;
	/**绑定人*/
	private String bindUserId;
	
	private Car car;
	
	private Device device;
	
	//车牌号")
	private String carPlateNo;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceNo() {
		return deviceNo;
	}

	public void setDeviceNo(String deviceNo) {
		this.deviceNo = deviceNo;
	}

	public Integer getBindType() {
		return bindType;
	}

	public void setBindType(Integer bindType) {
		this.bindType = bindType;
	}

	public String getBindTypeName() {
		return bindTypeName;
	}

	public void setBindTypeName(String bindTypeName) {
		this.bindTypeName = bindTypeName;
	}

	public Date getBindDate() {
		return bindDate;
	}

	public void setBindDate(Date bindDate) {
		this.bindDate = bindDate;
	}

	public String getBindUserId() {
		return bindUserId;
	}

	public void setBindUserId(String bindUserId) {
		this.bindUserId = bindUserId;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public Device getDevice() {
		return device;
	}

	public void setDevice(Device device) {
		this.device = device;
	}

	public String getCarPlateNo() {
		return carPlateNo;
	}

	public void setCarPlateNo(String carPlateNo) {
		this.carPlateNo = carPlateNo;
	}
	
 

}
