package com.leetu.car.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.car.entity.RealtimeState;
import com.leetu.car.mapper.RealTimeStateMapper;
import com.leetu.car.service.RealTimeStateService;

@Service
public class RealTimeStateServiceImpl implements RealTimeStateService{

	@Autowired
	private RealTimeStateMapper timeStateMapper;
	
	@Override
	public RealtimeState getRealTimeState(String carId) {
		return timeStateMapper.getRealTimeState(carId);
	}

	@Override
	public void addRealTimeState(Map<String, String> param) {
		timeStateMapper.addRealTimeState(param);
	}

	@Override
	public void updateRealTimeState(Map<String, String> param) {
		timeStateMapper.updateRealTimeState(param);
		
	}

}
