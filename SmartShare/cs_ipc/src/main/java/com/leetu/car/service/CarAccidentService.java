package com.leetu.car.service;

import com.leetu.car.entity.CarAccident;

/**
 * @author jyt
 * @since 2016年7月27日 下午5:50:02
 */
public interface CarAccidentService {

	/**
	 * 通过条件查询车辆事故信息
	 * @param ca
	 * @return
	 * @version 1.0.1
	 */
	CarAccident queryCarAccidentByConditions(CarAccident ca);

	/**
	 * 添加车辆事故信息
	 * @param ca
	 * @version 1.0.1
	 */
	void addCarAccident(CarAccident ca);

	/**
	 * 更新车辆事故信息
	 * @param ca
	 * @version 1.0.1
	 */
	void updateCarAccidentById(CarAccident ca);

}
