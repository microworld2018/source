package com.leetu.car.service.impl;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.ground.order.entity.GroundOrder;
import com.ground.site.entity.SubDot;
import com.leetu.car.entity.Car;
import com.leetu.car.entity.CarView;
import com.leetu.car.entity.RealtimeState;
import com.leetu.car.mapper.CarMapper;
import com.leetu.car.service.CarService;
import com.leetu.device.mapper.DeviceBindingMapper;
import com.leetu.device.mapper.DeviceMapper;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.feestrategy.mapper.StrategyBaseMapper;
import com.leetu.place.mapper.BranchDotMapper;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.mapper.DictMapper;
import com.util.Ajax;
import com.util.Constants;
import com.util.PropertiesUtil;
import com.util.car.CurrentCarInfo;

@Service
public class CarServiceImpl  implements CarService{
	
	@Autowired
	private CarMapper carMapper;
	@Autowired
	private DictMapper dictMapper;
	@Autowired
	private StrategyBaseMapper  strategyBaseMapper;
	@Autowired
	private BranchDotMapper branchDotMapper;
	@Autowired
	private DeviceMapper deviceMapper;
	@Autowired
	private DeviceBindingMapper deviceBindingMapper;
	
	@Override
	public List<Car> getAvailableCarsByDotid(String dotId,String bizState) {
		
		return carMapper.getAvailableCars(dotId,bizState );
	}

	@Override
	public List<CarView> getAvailableCarsViewByDotid(String dotId,String bizState) {
		
		List<CarView> list = carMapper.getAvailableCarViews(dotId,bizState);
			if(null!=list && list.size()>0){
				
				Iterator<CarView> listIter = list.iterator(); 
				
				while(listIter.hasNext()){
					CarView v = listIter.next();
					//默认时租
					if(v.getIsTimelyHire()){
						String timeStrategyId =v.getShizuStrategy();//时租计费ID
						StrategyBase ts= strategyBaseMapper.getStrategyById(timeStrategyId);
						
						if(ts != null && !"".equals(ts)){//计费策略必须存在
							v.setBasePrice(ts.getBasePrice());//计费价格
							v.setKmPrice(ts.getKmPrice());//每公里单价
							v.setTimelyFeeUnit( ts.getTimelyFeeUnitName());//时租计价单位
						}else{
							listIter.remove();
						}
					}else{
						String timeStrategyId =v.getRizuStrategy();//日租计费ID
						StrategyBase ts= strategyBaseMapper.getStrategyById(timeStrategyId);
						v.setBasePrice(ts.getBasePrice());//计费价格
						v.setKmPrice(ts.getKmPrice());//每公里单价
						v.setTimelyFeeUnit(ts.getTimelyFeeUnitName());//时租计价单位
					}
					//可续航里程
					RealtimeState state = carMapper.getCarRealtimeStateById(v.getId());
					
					if(state != null && !"".equals(state)){
						
						v.setLifeMileage(state.getLifeMileage());
					}
					
				}
				
			}
		return list;
	}

	 
	public void updateCarState(String carId, String t) {
		Dict d = dictMapper.getDictByCodes("carBizState", t);
		String state = d.getId();
		carMapper.updateCarState(carId,state);
	}

	@Override
	public void updateDotCar(String carId, String beginFotId, int i) {
		branchDotMapper.updateDotCar(carId, beginFotId, i);
	}

	@Override
	public Map<String, Object> dotInfo(String backdotId) {
		return branchDotMapper.dotInfo(backdotId);
	}

	@Override
	public Map<String, Object> carinfo(String carId) {
		return branchDotMapper.carinfo(carId, null);
	}

	@Override
	public void updateCarStatus(String carId, String t, String version) {
		Dict d = dictMapper.getDictByCodes("carBizState", "6");
		Dict d1 = dictMapper.getDictByCodes("carBizState", "1");
		if(t.equals(d.getId())){
			d = dictMapper.getDictByCodes("carBizState", "3");
		}else if(t.equals(d1.getId())){
			d = dictMapper.getDictByCodes("carBizState", "0");
		}
		String state = d.getId();
		branchDotMapper.updateCarStatus(carId, state, version);
	}

	@Override
	public String addCarInfoRedis() {
		//汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> ActiveIDs = new Hashtable<String,String>();
		//汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> ActiveSNs = new Hashtable<String,String>();
		//获取 已绑定设备的车辆信息
		List<Map<String,String>> cartilist = deviceMapper.carAndDevice(null);
		
		if(cartilist != null && cartilist.size() > 0){
			for (Map<String, String> map : cartilist) {
				String carId = map.get("car_id");
				String deviceNo = map.get("device_no");
				ActiveIDs.put(carId, deviceNo);
				ActiveSNs.put(deviceNo, carId);
			}
		}
		
		RedisClient.setobjdata("ActiveIDs", ActiveIDs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("ActiveSNs", ActiveSNs, DataBaseUtil.CS_CAR_MSG);
			
		return Ajax.AppJsonResult(Constants.APP_TYPE_NULL, Constants.APP_TYPE_NULL_MSG);
	}

	@Override
	public Map<String, String> getCarDotByCarId(String carId) {
		return carMapper.getCarDotByCarId(carId);
	}

	@Override
	public List<Car> getGroundCarList(String userId, String dotName,String vehiclePlateId) {
		List<Car> carList = carMapper.getGroundCarList(userId, dotName,vehiclePlateId);
		List<GroundOrder> groundList=carMapper.getGroundCarId();
		if(carList != null && carList.size() > 0){
			for(int i=0;i<carList.size();i++){
				//获取车辆电量信息
				Map<String, Object> carreal =CurrentCarInfo.getCarmp(carList.get(i).getId());
				if(carreal==null){
					System.out.println("1");
				}
				carList.get(i).setCarSn(carreal.get("sn").toString());
				carList.get(i).setElectricity(carreal.get("electricity") == null?"车辆数据异常":carreal.get("electricity").toString());
				Object chargeStatus = carreal.get("chargeStatus");
				if(chargeStatus != null){
					int chargeStatusI=(int)chargeStatus;
					if(chargeStatusI == 0){
						carList.get(i).setChargeStatus("未充电");
					}else if(chargeStatusI == 1){
						carList.get(i).setChargeStatus("充电中");
					}
				}
				String carId=carList.get(i).getId();
				if(carId!=null && carId.trim()!=""){
					for (GroundOrder groundOrder : groundList) {
						if(carId.equals(groundOrder.getCarId())){
							carList.get(i).setIsGround(1);
							SubDot subDot = carList.get(i).getSubDot();
							subDot.setSubId(groundOrder.getSubId());
							carList.get(i).setSubDot(subDot);
						}
					}
				}
			}
		}
		return carList;
	}

	@Override
	public List<Car> getGroundCarList(String userId, String dotName) {
		// TODO Auto-generated method stub
		return null;
	}

}
