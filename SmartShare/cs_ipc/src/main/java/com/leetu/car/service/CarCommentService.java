package com.leetu.car.service;

import com.leetu.car.entity.CarComment;

/**
 * @author jyt
 * @since 2016年7月29日 上午9:55:16
 */
public interface CarCommentService {

	/**
	 * 添加一条车辆评价
	 * @param cc
	 * @version 1.0.1
	 */
	void addCarComment(CarComment cc);

	/**
	 * 根据条件查询车评
	 * @param cc
	 * @version 1.0.1
	 * @return 
	 */
	CarComment queryCarCommentByConditions(CarComment cc);

	/**
	 * 通过ID更新车辆评价
	 * @param cc
	 * @version 1.0.1
	 */
	void updateCarCommentById(CarComment cc);
}
