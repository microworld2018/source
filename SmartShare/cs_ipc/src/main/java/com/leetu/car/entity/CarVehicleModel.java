package com.leetu.car.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;


public class CarVehicleModel implements Serializable {

	private static final long serialVersionUID = 1L;
	/**主键Id*/
	private String id;
	
	/**车型名称*/
	//车型名称") 
	private String name;
	
	/**动力类型*/
	private String engineType;
	
	//动力类型") 
	private String engineTypeName;
	
	/**车辆品牌*/
	private String brand;
	
	//车辆品牌") 
	private String brandName;
	
	/**车辆等级*/
	private String level;
	
	//车辆等级") 
	private String levelName;
	
	/**座位数*/
	//座位数") 
	private Integer seatingNum;
	
	/**行李数*/
	//行李数") 
	private Double luggageNum;
	
	/**车辆厢数*/
	//车辆厢数") 
	private Integer casesNum;
	
	/**档类别*/
	private String gearboxType;
	
	//档类别") 
	private String gearboxTypeName;
	
	/**是否有娱乐系统*/
	private Integer entertainmentSystem;
	
	/**标准续航里程*/
	//标准续航里程") 
	private Integer standardMileage;
	
	/**创建人Id*/
	private String creatorId;
	
	/**创建时间*/
	private Date createDate;
	
	/**时间戳*/
	private Date ts;
	
	private String webModelPhoto;
	private String androidModelPhoto;
	private String IOSModelPhoto;
	private String microWebModelPhoto;
	
	//20%续航里程") 
	private Double mileage20;
	//30%续航里程") 
	private Double mileage30;
	//40%续航里程") 
	private Double mileage40;
	//50%续航里程") 
	private Double mileage50;
	//60%续航里程") 
	private Double mileage60;
	//70%续航里程") 
	private Double mileage70;
	//80%续航里程") 
	private Double mileage80;
	//90%续航里程") 
	private Double mileage90;
	//100%续航里程") 
	private Double mileage100;
	
	private Integer hasgps;
	
	//导航仪") 
	private String hasgpsDesc;
	
	private String shizuStrategy;
	private String rizuStrategy;
	
	//即时租策略") 
	private String shizuStrategyName;
	
	//日租策略") 
	private String rizuStrategyName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEngineType() {
		return engineType;
	}

	public void setEngineType(String engineType) {
		this.engineType = engineType;
	}

	public String getEngineTypeName() {
		return engineTypeName;
	}

	public void setEngineTypeName(String engineTypeName) {
		this.engineTypeName = engineTypeName;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getLevelName() {
		return levelName;
	}

	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}

	public Integer getSeatingNum() {
		return seatingNum;
	}

	public void setSeatingNum(Integer seatingNum) {
		this.seatingNum = seatingNum;
	}

	public Double getLuggageNum() {
		return luggageNum;
	}

	public void setLuggageNum(Double luggageNum) {
		this.luggageNum = luggageNum;
	}

	public Integer getCasesNum() {
		return casesNum;
	}

	public void setCasesNum(Integer casesNum) {
		this.casesNum = casesNum;
	}

	public String getGearboxType() {
		return gearboxType;
	}

	public void setGearboxType(String gearboxType) {
		this.gearboxType = gearboxType;
	}

	public String getGearboxTypeName() {
		return gearboxTypeName;
	}

	public void setGearboxTypeName(String gearboxTypeName) {
		this.gearboxTypeName = gearboxTypeName;
	}

	public Integer getEntertainmentSystem() {
		return entertainmentSystem;
	}

	public void setEntertainmentSystem(Integer entertainmentSystem) {
		this.entertainmentSystem = entertainmentSystem;
	}

	public Integer getStandardMileage() {
		return standardMileage;
	}

	public void setStandardMileage(Integer standardMileage) {
		this.standardMileage = standardMileage;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public String getWebModelPhoto() {
		return webModelPhoto;
	}

	public void setWebModelPhoto(String webModelPhoto) {
		this.webModelPhoto = webModelPhoto;
	}

	public String getAndroidModelPhoto() {
		return androidModelPhoto;
	}

	public void setAndroidModelPhoto(String androidModelPhoto) {
		this.androidModelPhoto = androidModelPhoto;
	}

	public String getIOSModelPhoto() {
		return IOSModelPhoto;
	}

	public void setIOSModelPhoto(String iOSModelPhoto) {
		IOSModelPhoto = iOSModelPhoto;
	}

	public String getMicroWebModelPhoto() {
		return microWebModelPhoto;
	}

	public void setMicroWebModelPhoto(String microWebModelPhoto) {
		this.microWebModelPhoto = microWebModelPhoto;
	}

	public Double getMileage20() {
		return mileage20;
	}

	public void setMileage20(Double mileage20) {
		this.mileage20 = mileage20;
	}

	public Double getMileage30() {
		return mileage30;
	}

	public void setMileage30(Double mileage30) {
		this.mileage30 = mileage30;
	}

	public Double getMileage40() {
		return mileage40;
	}

	public void setMileage40(Double mileage40) {
		this.mileage40 = mileage40;
	}

	public Double getMileage50() {
		return mileage50;
	}

	public void setMileage50(Double mileage50) {
		this.mileage50 = mileage50;
	}

	public Double getMileage60() {
		return mileage60;
	}

	public void setMileage60(Double mileage60) {
		this.mileage60 = mileage60;
	}

	public Double getMileage70() {
		return mileage70;
	}

	public void setMileage70(Double mileage70) {
		this.mileage70 = mileage70;
	}

	public Double getMileage80() {
		return mileage80;
	}

	public void setMileage80(Double mileage80) {
		this.mileage80 = mileage80;
	}

	public Double getMileage90() {
		return mileage90;
	}

	public void setMileage90(Double mileage90) {
		this.mileage90 = mileage90;
	}

	public Double getMileage100() {
		return mileage100;
	}

	public void setMileage100(Double mileage100) {
		this.mileage100 = mileage100;
	}

	public Integer getHasgps() {
		return hasgps;
	}

	public void setHasgps(Integer hasgps) {
		this.hasgps = hasgps;
	}

	public String getHasgpsDesc() {
		return hasgpsDesc;
	}

	public void setHasgpsDesc(String hasgpsDesc) {
		this.hasgpsDesc = hasgpsDesc;
	}

	public String getShizuStrategy() {
		return shizuStrategy;
	}

	public void setShizuStrategy(String shizuStrategy) {
		this.shizuStrategy = shizuStrategy;
	}

	public String getRizuStrategy() {
		return rizuStrategy;
	}

	public void setRizuStrategy(String rizuStrategy) {
		this.rizuStrategy = rizuStrategy;
	}

	public String getShizuStrategyName() {
		return shizuStrategyName;
	}

	public void setShizuStrategyName(String shizuStrategyName) {
		this.shizuStrategyName = shizuStrategyName;
	}

	public String getRizuStrategyName() {
		return rizuStrategyName;
	}

	public void setRizuStrategyName(String rizuStrategyName) {
		this.rizuStrategyName = rizuStrategyName;
	}

}
