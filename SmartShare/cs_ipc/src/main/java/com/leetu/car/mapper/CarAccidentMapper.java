package com.leetu.car.mapper;

import com.leetu.car.entity.CarAccident;

/**
 * @author jyt
 * @since 2016年7月27日 下午5:56:24
 */
public interface CarAccidentMapper {

	/**
	 * 生成一条事故记录
	 * @param ca
	 * @version 1.0.1
	 */
	void insertCarAccident(CarAccident ca);

	/**
	 * 条件查询事故记录单条
	 * @param ca
	 * @return
	 * @version 1.0.1
	 */
	CarAccident queryCarAccidentByConditions(CarAccident ca);

	/**
	 * 更新事故信息
	 * @param ca
	 * @version 1.0.1
	 */
	void updateCarAccidentById(CarAccident ca);

	
	
	
	
}
