package com.leetu.car.entity;

import java.io.Serializable;
import java.util.Date;

import com.ground.site.entity.SubDot;
import com.leetu.place.entity.CarDotBinding;
import com.leetu.sys.entity.Dict;

public class Car implements Serializable {

	public static final Integer CAR_STATE_OPEN = 1;//开车门
	public static final Integer CAR_STATE_CLOSE = 2;//关车门
	public static final Integer CAR_STATE_WHISTLE = 3;//鸣笛
	
	public static final String CAR_STATE_CLOSE_BLACKOUT = "4";
	
	public static final Integer CAR_OPEN_DOOR_ERROR = 0;//打开车门失败
	public static final Integer CAR_OPEN_DOOR_SUCCESS = 1;//打开车门成功
	
	public static final Integer CAR_CLOSE_DOOR_ERROR = 2;//关闭车门失败
	public static final Integer CAR_CLOSE_DOOR_SUCCESS = 3;//关闭车门成功
	
	public static final Integer CAR_STATE_WHISTLE_ERROR = 4;//鸣笛失败
	public static final Integer CAR_STATE_WHISTLE_SUCCESS = 5;//鸣笛成功
	
	/**电动车接口调用返回参数说明**/
	public static final Integer CONTROL_CAR_PROCESSING_IDENTIFICATION_ERROR = -2;//处理标识错误
	
	public static final Integer CONTROL_CAR_CALL_ERROR = -1;//调用错误
	
	public static final Integer CONTROL_CAR_NOT_ONLINE = 1308;//设备不在线
	
	public static final Integer CONTROL_CAR_SUCCESS = 1;//调用成功(或重启执行成功返回结果)
	
	public static final Integer CONTROL_CAR_NOT_SUPPORTED = 2;//不支持此操作
	
	public static final Integer CONTROL_CAR_TIME_OUT = 3;//调用超时
	
	public static final Integer CONTROL_CAR_BUSY = 4;//远程控制总线忙
	
	public static final Integer CONTROL_CAR_NOT_CLOSE_DOOR = 5;//车门或发动机未关闭
	
	private static final long serialVersionUID = 1L;
	/**主键*/
	private String id;
	/**车牌号*/
	private String vehiclePlateId;
	/**车架号*/
	private String vin;
	/**昵称*/
	private String nickName;
	/**运营城市*/
	private String operationCity;
	/**颜色*/
	private String color;
	/**车型Id*/
	private String modelId;
	private String modelName;
	/**是否是广告车*/
	private Integer isAd;
	private String isAdDesc;
	/**创建人Id*/
	private String creatorId;
	/**创建时间*/
	private Date createDate;
	/**时间戳*/
	private Date ts;
	/**
	 * 车辆业务状态
	 */
	private String bizState;
	
	/**
	 * 业务状态code
	 */
	private String bizStateCode;
	
	private CarVehicleModel carVehicleModel;
	
	private String engineNumber;
	private String drivingLicenseNumber;
	private Date buyTime;
	private String buyPlace;
	
	
	private SubDot subDot;//地勤网点关联实体类
	
	private CarDotBinding carDotBinging;//车辆网点关联实体类
	
	private Dict dict;//字典实体类
	
	private String electricity;//当前电量
	
	private String carSn;//车辆SN
	
	private Integer isGround=0;//是否是地勤下的订单-gjm
	
	private String chargeStatus;//车辆充电状态  ge  jiaming 
	
	public String getChargeStatus() {
		return chargeStatus;
	}

	public void setChargeStatus(String chargeStatus) {
		this.chargeStatus = chargeStatus;
	}

	public Integer getIsGround() {
		return isGround;
	}

	public void setIsGround(Integer isGround) {
		this.isGround = isGround;
	}

	public CarVehicleModel getCarVehicleModel() {
		return carVehicleModel;
	}

	public void setCarVehicleModel(CarVehicleModel carVehicleModel) {
		this.carVehicleModel = carVehicleModel;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVehiclePlateId() {
		return vehiclePlateId;
	}

	public void setVehiclePlateId(String vehiclePlateId) {
		this.vehiclePlateId = vehiclePlateId;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getOperationCity() {
		return operationCity;
	}

	public void setOperationCity(String operationCity) {
		this.operationCity = operationCity;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public Integer getIsAd() {
		return isAd;
	}

	public void setIsAd(Integer isAd) {
		this.isAd = isAd;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", vehiclePlateId=" + vehiclePlateId
				+ ", vin=" + vin + ", nickName=" + nickName
				+ ", operationCity=" + operationCity + ", color=" + color
				+ ", modelId=" + modelId + ", isAd=" + isAd + ", creatorId="
				+ creatorId + ", createDate=" + createDate + ", ts=" + ts + "]";
	}

	public Car(String id, String vehiclePlateId, String vin, String nickName,
			String operationCity, String color, String modelId, int isAd,
			String creatorId, Date createDate, Date ts) {
		super();
		this.id = id;
		this.vehiclePlateId = vehiclePlateId;
		this.vin = vin;
		this.nickName = nickName;
		this.operationCity = operationCity;
		this.color = color;
		this.modelId = modelId;
		this.isAd = isAd;
		this.creatorId = creatorId;
		this.createDate = createDate;
		this.ts = ts;
	}

	public Car() {
		super();
	}
	public String getBizState() {
		return bizState;
	}
	public void setBizState(String bizState) {
		this.bizState = bizState;
	}
	public String getBizStateCode() {
		return bizStateCode;
	}

	public void setBizStateCode(String bizStateCode) {
		this.bizStateCode = bizStateCode;
	}

	public void setModelName(String modelName) {
		this.modelName = modelName;
	}

	public String getIsAdDesc() {
		if(isAd==0){
			return "否";
		}else{
			return "是";
		}
	}

	public void setIsAdDesc(String isAdDesc) {
		this.isAdDesc = isAdDesc;
	}

	public String getEngineNumber() {
		return engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getDrivingLicenseNumber() {
		return drivingLicenseNumber;
	}

	public void setDrivingLicenseNumber(String drivingLicenseNumber) {
		this.drivingLicenseNumber = drivingLicenseNumber;
	}

	public Date getBuyTime() {
		return buyTime;
	}

	public void setBuyTime(Date buyTime) {
		this.buyTime = buyTime;
	}

	public String getBuyPlace() {
		return buyPlace;
	}

	public void setBuyPlace(String buyPlace) {
		this.buyPlace = buyPlace;
	}

	public SubDot getSubDot() {
		return subDot;
	}

	public void setSubDot(SubDot subDot) {
		this.subDot = subDot;
	}

	public CarDotBinding getCarDotBinging() {
		return carDotBinging;
	}

	public void setCarDotBinging(CarDotBinding carDotBinging) {
		this.carDotBinging = carDotBinging;
	}

	public Dict getDict() {
		return dict;
	}

	public void setDict(Dict dict) {
		this.dict = dict;
	}

	public String getModelName() {
		return modelName;
	}

	public String getElectricity() {
		return electricity;
	}

	public void setElectricity(String electricity) {
		this.electricity = electricity;
	}

	public String getCarSn() {
		return carSn;
	}

	public void setCarSn(String carSn) {
		this.carSn = carSn;
	}

	
}
