package com.leetu.car.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.car.entity.CarComment;
import com.leetu.car.mapper.CarCommentMapper;
import com.leetu.car.service.CarCommentService;

/**
 * @author jyt
 * @since 2016年7月29日 上午9:56:10
 */
@Service
public class CarCommentServiceImp implements CarCommentService {

	@Autowired
	private CarCommentMapper carCommentMapper;
	
	
	@Override
	public void addCarComment(CarComment cc) {
		// TODO Auto-generated method stub
		carCommentMapper.insertCarComment(cc);
	}

	@Override
	public CarComment queryCarCommentByConditions(CarComment cc) {
		// TODO Auto-generated method stub
		return carCommentMapper.queryCarCommentByConditions(cc);
	}

	@Override
	public void updateCarCommentById(CarComment cc) {
		 carCommentMapper.updateCarCommentById(cc);
	}
}
