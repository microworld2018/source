package com.leetu.car.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.car.entity.RealtimeState;

public interface RealTimeStateMapper {

	/**
	 * 获取车辆初始续航值
	 * @param carId
	 * @return
	 */
	public RealtimeState getRealTimeState(@Param("carId")String carId);
	
	/**
	 * 车辆初始续航值添加
	 * @param param
	 */
	public int addRealTimeState(Map<String, String> param);
	
	/**
	 * 车辆初始续航值修改
	 * @param param
	 */
	public int updateRealTimeState(Map<String, String> param);

}
