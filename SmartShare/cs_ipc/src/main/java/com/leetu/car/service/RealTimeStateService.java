package com.leetu.car.service;

import java.util.Map;

import com.leetu.car.entity.RealtimeState;

public interface RealTimeStateService {

	/**
	 * 获取车辆初始续航值
	 * @param carId
	 * @return
	 */
	RealtimeState getRealTimeState(String carId);
	
	/**
	 * 车辆初始续航值添加
	 * @param param
	 */
	void addRealTimeState(Map<String, String> param);
	
	/**
	 * 车辆初始续航值修改
	 * @param param
	 */
	void updateRealTimeState(Map<String, String> param);

}
