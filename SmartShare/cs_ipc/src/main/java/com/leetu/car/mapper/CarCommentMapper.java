package com.leetu.car.mapper;

import com.leetu.car.entity.CarComment;

/**
 * @author jyt
 * @since 2016年7月29日 上午9:57:41
 */
public interface CarCommentMapper {

	
	/**
	 * 添加车辆评价
	 * @param cc
	 * @version 1.0.1
	 */
	void insertCarComment(CarComment cc);
	
	/**
	 * 查询车辆评价
	 * @param cc
	 * @return
	 * @version 1.0.1
	 */
	CarComment queryCarCommentByConditions(CarComment cc);

	/**
	 * 更细车辆评价
	 * @param cc
	 * @version 1.0.1
	 */
	void updateCarCommentById(CarComment cc);
}
