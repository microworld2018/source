package com.leetu.car.controller;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.car.entity.CarAccident;
import com.leetu.car.service.CarAccidentService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.Utils;

/**
 * @author zhangsg
 * @since 2016年7月27日 下午5:47:50
 */
@Controller
@RequestMapping("accident")
public class CarAccidentController  extends BaseController<CarAccidentController>  {

	@Autowired
	private CarAccidentService carAccidentService;
	
	@Autowired
	private DictService dictService;
	
	/**
	 * 
	 * 生成车辆事故上报
	 * token、订单ID、汽车ID
	 * @version 1.0.1
	 */
	@RequestMapping("/makeCarAccident")
	public void makeCarAccident()
	{

		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			if(null == s){
				result =  Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN, Constants.APP_LOGIN_TOKEN_MSG);
				 Utils.outJSONObject(result,response);
			}
			CarAccident ca = new CarAccident();
			pmap.remove("token");
			pmap.remove("param");
			ca =Utils.map2Bean(pmap, ca);
			String imgUrls = ca.getImgUrls();
			if(StringHelper.isNotEmpty(imgUrls) && imgUrls.contains("|"))
			{
				CarAccident conditions = new CarAccident ();
				
				conditions.setCarId(ca.getCarId());
				conditions.setOrderId(ca.getOrderId());
				conditions.setMemberId(ca.getMemberId());
				
				conditions=this.carAccidentService.queryCarAccidentByConditions(conditions);
				String[] picArr = imgUrls.split("\\|");
				Class<? extends CarAccident> clazz = ca.getClass();
				for(int i=0;i<picArr.length && i<10;i++){
					Method m2 = clazz.getDeclaredMethod("setPhoto"+i, String.class);
					m2.invoke(ca, picArr[i]);
				}
			}
			ca.setMemberId(s.getId());
			ca.setMemberName(s.getName());
			
			
			
			this.carAccidentService.addCarAccident(ca);
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG );
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
			
	}
	
	/**
	 * 
	 * 修改车辆事故上报
	 * token、订单ID、汽车ID
	 * @version 1.0.1
	 */
	@RequestMapping("/updateCarAccident")
	public void updateCarAccident(){
		
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			if(null == s){
				result =  Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN, Constants.APP_LOGIN_TOKEN_MSG);
				 Utils.outJSONObject(result,response);
			}
			CarAccident ca = new CarAccident();
			pmap.remove("token");
			pmap.remove("param");
			ca =Utils.map2Bean(pmap, ca);
			String imgUrls = ca.getImgUrls();
			if(StringHelper.isNotEmpty(imgUrls) && imgUrls.contains("|"))
			{
				String[] picArr = imgUrls.split("\\|");
				Class<? extends CarAccident> clazz = ca.getClass();
				for(int i=0;i<picArr.length && i<10;i++){
					Method m2 = clazz.getDeclaredMethod("setPhoto"+i, String.class);
					m2.invoke(ca, picArr[i]);
				}
			}
			ca.setMemberId(s.getId());
			ca.setMemberName(s.getName());
			
			
			
			this.carAccidentService.updateCarAccidentById(ca);
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG );
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 查询事故上报
	 * param
	 * token、订单ID、汽车ID
	 * @version 1.0.1
	 */
	@RequestMapping("/queryCarAccident")
	public void queryCarAccident()
	{
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			CarAccident ca = new CarAccident();
			pmap.remove("token");
			pmap.remove("param");
			ca =Utils.map2Bean(pmap, ca);
			if(null == s || StringHelper.isEmpty(ca.getCarId())  || StringHelper.isEmpty(ca.getOrderId()) ){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				 Utils.outJSONObject(result,response);
			}
			ca.setMemberId(s.getId());
			
			ca=this.carAccidentService.queryCarAccidentByConditions(ca);
			ca.setMemberName(s.getName());
			
			String handleStatus = ca.getHandleStatus();
			if(StringHelper.isNotEmpty(handleStatus))
			{
				Dict dict = dictService.getDictById(handleStatus);
				ca.setHandleStatusName(dict.getCnName());
			}
			String acceptFlag = ca.getAcceptFlag();
			if(StringHelper.isNotEmpty(acceptFlag))
			{
				Dict dict = dictService.getDictById(acceptFlag);
				ca.setAcceptFlagName(dict.getCnName());
			}
			
			Map<String,Object> rm = new HashMap<String, Object>();
			
			Map<String,Object> m = new HashMap<String, Object>();
			m.put("id", ca.getId());
			m.put("imgUrls", ca.getImgUrls());
			m.put("memberId", ca.getMemberId());
			m.put("orderId", ca.getOrderId());
			m.put("carLose", ca.getCarLose());
			m.put("ts", ca.getTs());
			/*m.put("handleStatus", ca.getHandleStatus());
			m.put("handleStatusName", ca.getHandleStatusName());*/
			
			rm.put("carAccident", m);
			
			result = Ajax.AppbzJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG ,rm);
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
}
