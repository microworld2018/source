package com.leetu.car.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.car.entity.Car;
import com.leetu.car.entity.CarView;

public interface CarService {

	/**
	 * 返回网点未租用车辆信息
	 * @param dotId
	 * @return
	 */
	List<Car> getAvailableCarsByDotid(String dotId,String bizState);

	/**
	 * 查询网点未租用车辆信息
	 * @param dotId
	 * @return
	 */
	List<CarView> getAvailableCarsViewByDotid(String dotId,String bizState);

	/**
	 * 更新取车网点信息
	 * @param carId
	 * @param beginFotId
	 * @param i
	 */
	void updateDotCar(String carId, String beginFotId, int i);

	/**
	 * 获取网点信息
	 * @param backdotId
	 * @return
	 */
	Map<String, Object> dotInfo(String backdotId);

	/**
	 * 获取车辆信息
	 * @param carId
	 * @return
	 */
	Map<String, Object> carinfo(String carId);

	/**
	 * 更新车辆状态
	 * @param carId
	 * @param string
	 * @param version
	 */
	void updateCarStatus(String carId, String string, String version);

	/**
	 * 车机绑定redis存储数据
	 */
	String addCarInfoRedis();
	
	/**
	 * 根据车辆ID获取车牌号车辆所属网点信息
	 * @param carId
	 * @return
	 */
	Map<String,String> getCarDotByCarId(String carId);
	
	/**获取地勤人员所管理网点下的列表
	 * @param userId
	 * @param dotName
	 * @return
	 */
	List<Car> getGroundCarList(String userId,String dotName);

	List<Car> getGroundCarList(String id, String dotName, String vehiclePlateId);
}
