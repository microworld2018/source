package com.leetu.car.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ground.order.entity.GroundOrder;
import com.leetu.car.entity.Car;
import com.leetu.car.entity.CarView;
import com.leetu.car.entity.RealtimeState;

public interface CarMapper {

	List<Car> getAvailableCars(@Param("id")String id, @Param("bizState")String bizState);

	/**
	 * 获取车辆信息 
	 */
	List<CarView> getAvailableCarViews(@Param("id")String id, @Param("bizState")String bizState);
	

	/**
	 * 查询车辆TBoX数据
	 * @param id
	 * @param bizState
	 * @return
	 */
	RealtimeState getCarRealtimeStateById(@Param("id")String id);

	/**
	 * 更新车辆状态
	 * @param carId
	 * @param state
	 */
	void updateCarState(@Param("carId")String carId, @Param("state")String state);
	
	/**
	 * 根据车辆ID获取车牌号车辆所属网点信息
	 * @param carId
	 * @return
	 */
	Map<String,String> getCarDotByCarId(@Param("carId") String carId);
	
	/**
	 * 获取地勤人员所管理网点下的列表
	 * @param userId
	 * @param dotName
	 * @param vehiclePlateId
	 * @return
	 */
	List<Car> getGroundCarList(@Param("userId")String userId,@Param("dotName")String dotName,@Param("vehiclePlateId")String vehiclePlateId);

	List<GroundOrder> getGroundCarId();
}
