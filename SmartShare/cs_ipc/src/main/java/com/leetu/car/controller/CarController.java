package com.leetu.car.controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.core.controller.BaseController;
import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.ground.site.entity.SubDot;
import com.ground.site.service.SubDotService;
import com.ground.user.entity.User;
import com.leetu.car.entity.Car;
import com.leetu.car.entity.CarView;
import com.leetu.car.param.CarParam;
import com.leetu.car.service.CarService;
import com.leetu.car.service.RealTimeStateService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.device.service.DeviceService;
import com.leetu.orders.param.OrderParam;
import com.leetu.orders.service.OrderService;
import com.leetu.place.entity.BranchDot;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.service.DictService;
import com.leetu.sys.service.RunLogService;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.HttpRequestUtil;
import com.util.JsonTools;
import com.util.TokenUtils;
import com.util.Utils;
import com.util.car.CurrentCarInfo;
/**
 * 
 * @author zhangsg
 * @since 2016年8月3日 上午10:55:23
 */
@Controller
@RequestMapping("car")
public class CarController extends BaseController<CarController> {

	public static final Log logger = LogFactory.getLog(CarController.class);
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private CarService  carService;
	
	@Autowired
	private DictService dictService; 
	
	@Autowired
	private BranchDotService branchDotService;
	
	@Autowired
	private RealTimeStateService realTimeStateService;
	
	@Autowired
	private DeviceBindingService deviceBindingService;
	
	@Autowired
	private DeviceService deviceService;
	
	@Autowired
	private RunLogService runLogService;
	
	@Autowired
	private SubDotService subDotService;//地勤网点关联关系
	
	/**
	 * 7.获取当前网点中可用汽车信息(1.0.1)
	 * @author jyt
	 * @problems 1.未解决车辆mini图片信息路径处理，2.全局要考虑的问题：检查会员是不是已经租车 ，已经...等
	 */
	@RequestMapping("/getAvailablecarsByDotid")
	public void getAvailablecarsByDotid(){
		//业务处理
		String result = null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			
			//根据会员ID查询会员租车信息
			Subscriber s = TokenUtils.getSubscriber(data);
			if(null ==s ){
				Utils.outJSONObject(Ajax.AppJsonResult(Constants.APP_PARAM_REQUIRED_ERROR, Constants.APP_PARAM_REQUIRED_ERROR_MSG),response);
			}
			
			//获取字典表中车辆未租借状态
			Dict d = dictService.getDictByCodes("carBizState", "0");
			System.out.println("***************************************1"+d);
			String dotId= pmap.get("dotId");
			List<CarView> carViews =   carService.getAvailableCarsViewByDotid(dotId,d.getId());
			Map<String,Object> rm = new HashMap<String,Object>();
			System.out.println("**************************************2"+carViews);
			//初始化还车网点默认为取车网点
			BranchDot defaultReturn= branchDotService.getDotById(dotId);
			HashMap<String, Object> backDot = new HashMap<String, Object>();
			backDot.put("id", defaultReturn.getId());
			backDot.put("name", defaultReturn.getName());
			backDot.put("address", defaultReturn.getAddress());
			rm.put("returnDot", backDot);
			System.out.println("************************************3"+rm);
			List<Map<String,Object>> list =new ArrayList<Map<String, Object>>();
			
			for( CarView v: carViews){
				
				Map<String,Object>  m = new HashMap<String, Object>();
				
				m.put("id", v.getId());
				m.put("vehiclePlateId", v.getVehiclePlateId());
				m.put("name", v.getName());
				m.put("brand", v.getBrand());
				m.put("seatingNum", v.getSeatingNum());
				m.put("casesNum", v.getCasesNum());
				//获取车辆可持续里航
				Map<String, Object> carreal =CurrentCarInfo.getCarmp(v.getId());
				System.out.println("************************************4"+carreal);
				
				// ADD BY Jinguangyu;2017-7-13 16:25;
				if(carreal == null){
					//redis中没有这辆车的信息
					System.out.println("************************************4.5carreal为null");
					System.out.println("************************************4.6carId = " + v.getId());
					continue;
				}
				// ADD BY Jinguangyu;2017-7-13 16:25;
				
				String lifeMileage = "";
//				if(carreal != null && !"".equals(carreal)){
					
					 lifeMileage = String.valueOf(carreal.get("mileage"));
//				}
				m.put("lifeMileage", lifeMileage);
				
				//先不测试
				String electricity = carreal.get("electricity").toString();
				
				if(electricity != null && !"".equals(electricity)){
					electricity = electricity.split("/")[0];
				}else{
					electricity = "0";
				}
				System.out.println("************************************5"+electricity);
				double dd= 100;
				double SOC = ArithUtil.div(Double.parseDouble(electricity),dd);
				if("0".equals(carreal.get("mileage")) && SOC > 0){
					//标准里程
					double standardMileage = Double.valueOf(v.getStandardMileage());
					m.put("lifeMileage", String.valueOf(ArithUtil.mul(standardMileage, SOC)));//续航里程
//					paramMap.put("lifeMileage", String.valueOf(ArithUtil.mul(standardMileage, SOC)));//续航里程
				}else{
					m.put("lifeMileage", String.valueOf(carreal.get("mileage")));//续航里程
				}
				System.out.println("************************************6"+m);
				//m.put("lifeMileage", v.getLifeMileage());//可续航里程
				m.put("basePrice", v.getBasePrice());
				m.put("timelyFeeUnit", v.getTimelyFeeUnit());
				m.put("kmPrice", v.getKmPrice());
				m.put("androidModelPhoto", v.getAndroidModelPhoto());//安卓手机端车型图片网络路径
				m.put("iosModelPhoto", v.getIosModelPhoto());//IOS手机端车型图片网络路径
				//iosModelPhoto  对应字段为图片网络路径
				list.add(m);
			}
			rm.put("cars", list);
			System.out.println("************************************7");
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, rm);
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	
	/**
	 * 12开关车门(1.0.1)
	 * @author hzw
	 */
	@RequestMapping("/opOrClDoor")
	public void opOrClDoor(){
		String result = CarParam.opOrClDoor(request, orderService,branchDotService,realTimeStateService,deviceBindingService,runLogService);
		System.out.println("=========================================锁车门  ==后台==  返回结果: "+result+"===========================================================");
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 车辆绑定车机接口（后台系统调用）
	 */
	@RequestMapping("/addCarInfoRedis")
	public void addCarInfoRedis(){
		String result = null;
		try {
			result = CarParam.addCarInfoRedis(request, deviceService);
			
		} catch (Exception e) {
			logger.error("redis添加车辆信息异常:"+e.getMessage());
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		
		Utils.outJSONObject(result,response);
	}
	
	
	/**通电或者断电
	 * @return
	 */
	@RequestMapping("/powerUpsORProwerOff")
	public @ResponseBody String powerUpsORProwerOff(String sn,String state){
		Map<String, String> paramMap = new HashMap<String, String>();
		String contents = "";
		paramMap.put("sn", sn);
		if(state.equals("0")){
			//调用开始用车远程恢复供电
			contents = HttpRequestUtil.getPostMethod(CurrentCarInfo.RESTORE_POWER_URL,paramMap);
		}
		if(state.equals("1")){
			//调用断电接口
			contents = HttpRequestUtil.getPostMethod(CurrentCarInfo.LOCK_POWER_OFF_URL,paramMap);
		}
		return contents;
	}
	
	
	/**跳转车辆操控页面
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping("/openCarControl")
	public ModelAndView openCarControl(HttpServletRequest request,String carId,String carSn,String carNumber) throws UnsupportedEncodingException{
		if(carNumber!=null||carNumber.trim()!=""){
			carNumber=java.net.URLDecoder.decode(carNumber, "UTF-8");
		}
		ModelAndView modelAndView = new ModelAndView();
		User u = (User) request.getSession().getAttribute("user");
		if(u == null){
			modelAndView.setViewName("/manage_car/login");
			return modelAndView;
		}
		modelAndView.addObject("carId", carId); 
		modelAndView.addObject("carSn", carSn);
		modelAndView.addObject("carNumber", carNumber);
		modelAndView.setViewName("/manage_car/control");
		return modelAndView;
	}
	
	/**
	 * 后台上线车辆后，调此接口，初始化车辆信息到redis中
	 */
	@RequestMapping("/initCarInfoWhenCarOnline")
	public void initCarInfoWhenCarOnline(){
		//汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> ActiveIDs = new Hashtable<String,String>();
		//汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> ActiveSNs = new Hashtable<String,String>();
		//获取 已绑定设备的车辆信息
		List<Map<String,String>> cartilist = deviceService.carAndDevice(null);
		
		if(cartilist != null && cartilist.size() > 0){
			for (Map<String, String> map : cartilist) {
				String carId = map.get("car_id");
				String deviceNo = map.get("device_no");
				ActiveIDs.put(carId, deviceNo);
				ActiveSNs.put(deviceNo, carId);
			}
		}
		
		RedisClient.setobjdata("ActiveIDs", ActiveIDs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("ActiveSNs", ActiveSNs, DataBaseUtil.CS_CAR_MSG);
		
		String result = Ajax.AppResult(0, "车辆上线，初始化成功",null);
		Utils.outJSONObject(result,response);
	}
	
}











