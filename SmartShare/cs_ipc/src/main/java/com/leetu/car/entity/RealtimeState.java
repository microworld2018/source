package com.leetu.car.entity;

import java.io.Serializable;

public class RealtimeState implements Serializable {

	private static final long serialVersionUID = 3299663158136243522L;

		//'车辆ID'
		private String id;
		//'车速'
		private String speed;
		////'经度'
		private String lat;
		////'纬度'
		private String lng;
		////'运行时间'
		private String trackTime;
		////'总电压'
		private String totalVoltage;
		////'总电流'
		private String totalCurrent;
		////'总电量'
		private String soc;
		//'当前里程'
		private String currentMileage;
		//'当前挡位'
		private String currentGear;
		//'油门行程'
		private String throttleTrip;
		//'制动里程值'
		private String brakeTrip;
		//'电动机转速'
		private String motorSpeed;
		//'电动机温度'
		private String motorTemperature;
		// '当前电动机'
		private String motorCurrent;
		// '全方位距离导航系统 时间'
		private String obdTime;
		//' 1正在充电 2行驶中 3熄火'
		private String state;
		//'当前 状态 时间'
		private String stateTime;
		//'运营业务状态    1租赁使用中    2未租赁   3预定中   4下线'
		private String bizDtate;
		// '运营业务状态更改时间'
		private String bizStateTime;
		//'操作时间'
		private String ts;
		//'蓄电池电量'
		private String batteryPower;
		//'续航里程'
		private Integer lifeMileage;
		//'环境温度'
		private String outTimperature;
		//'转向灯信号'
		private String corneringLampFlag;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getSpeed() {
			return speed;
		}
		public void setSpeed(String speed) {
			this.speed = speed;
		}
		public String getLat() {
			return lat;
		}
		public void setLat(String lat) {
			this.lat = lat;
		}
		public String getLng() {
			return lng;
		}
		public void setLng(String lng) {
			this.lng = lng;
		}
		public String getTrackTime() {
			return trackTime;
		}
		public void setTrackTime(String trackTime) {
			this.trackTime = trackTime;
		}
		public String getTotalVoltage() {
			return totalVoltage;
		}
		public void setTotalVoltage(String totalVoltage) {
			this.totalVoltage = totalVoltage;
		}
		public String getTotalCurrent() {
			return totalCurrent;
		}
		public void setTotalCurrent(String totalCurrent) {
			this.totalCurrent = totalCurrent;
		}
		public String getSoc() {
			return soc;
		}
		public void setSoc(String soc) {
			this.soc = soc;
		}
		public String getCurrentMileage() {
			return currentMileage;
		}
		public void setCurrentMileage(String currentMileage) {
			this.currentMileage = currentMileage;
		}
		public String getCurrentGear() {
			return currentGear;
		}
		public void setCurrentGear(String currentGear) {
			this.currentGear = currentGear;
		}
		public String getThrottleTrip() {
			return throttleTrip;
		}
		public void setThrottleTrip(String throttleTrip) {
			this.throttleTrip = throttleTrip;
		}
		public String getBrakeTrip() {
			return brakeTrip;
		}
		public void setBrakeTrip(String brakeTrip) {
			this.brakeTrip = brakeTrip;
		}
		public String getMotorSpeed() {
			return motorSpeed;
		}
		public void setMotorSpeed(String motorSpeed) {
			this.motorSpeed = motorSpeed;
		}
		public String getMotorTemperature() {
			return motorTemperature;
		}
		public void setMotorTemperature(String motorTemperature) {
			this.motorTemperature = motorTemperature;
		}
		public String getMotorCurrent() {
			return motorCurrent;
		}
		public void setMotorCurrent(String motorCurrent) {
			this.motorCurrent = motorCurrent;
		}
		public String getObdTime() {
			return obdTime;
		}
		public void setObdTime(String obdTime) {
			this.obdTime = obdTime;
		}
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getStateTime() {
			return stateTime;
		}
		public void setStateTime(String stateTime) {
			this.stateTime = stateTime;
		}
		public String getBizDtate() {
			return bizDtate;
		}
		public void setBizDtate(String bizDtate) {
			this.bizDtate = bizDtate;
		}
		public String getBizStateTime() {
			return bizStateTime;
		}
		public void setBizStateTime(String bizStateTime) {
			this.bizStateTime = bizStateTime;
		}
		public String getTs() {
			return ts;
		}
		public void setTs(String ts) {
			this.ts = ts;
		}
		public String getBatteryPower() {
			return batteryPower;
		}
		public void setBatteryPower(String batteryPower) {
			this.batteryPower = batteryPower;
		}
		public Integer getLifeMileage() {
			return lifeMileage;
		}
		public void setLifeMileage(Integer lifeMileage) {
			this.lifeMileage = lifeMileage;
		}
		public String getOutTimperature() {
			return outTimperature;
		}
		public void setOutTimperature(String outTimperature) {
			this.outTimperature = outTimperature;
		}
		public String getCorneringLampFlag() {
			return corneringLampFlag;
		}
		public void setCorneringLampFlag(String corneringLampFlag) {
			this.corneringLampFlag = corneringLampFlag;
		}
		
		
		@Override
		public String toString() {
			return "RealtimeState [id=" + id + ", speed=" + speed + ", lat="
					+ lat + ", lng=" + lng + ", trackTime=" + trackTime
					+ ", totalVoltage=" + totalVoltage + ", totalCurrent="
					+ totalCurrent + ", soc=" + soc + ", currentMileage="
					+ currentMileage + ", currentGear=" + currentGear
					+ ", throttleTrip=" + throttleTrip + ", brakeTrip="
					+ brakeTrip + ", motorSpeed=" + motorSpeed
					+ ", motorTemperature=" + motorTemperature
					+ ", motorCurrent=" + motorCurrent + ", obdTime=" + obdTime
					+ ", state=" + state + ", stateTime=" + stateTime
					+ ", bizDtate=" + bizDtate + ", bizStateTime="
					+ bizStateTime + ", ts=" + ts + ", batteryPower="
					+ batteryPower + ", lifeMileage=" + lifeMileage
					+ ", outTimperature=" + outTimperature
					+ ", corneringLampFlag=" + corneringLampFlag + "]";
		}
		
}
