package com.leetu.car.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.car.entity.CarAccident;
import com.leetu.car.mapper.CarAccidentMapper;
import com.leetu.car.service.CarAccidentService;

/**
 * @author jyt
 * @since 2016年7月27日 下午5:54:44
 */
@Service
public class CarAccidentServiceImpl implements CarAccidentService {

	@Autowired
	private CarAccidentMapper carAccidentMapper;

	@Override
	public CarAccident queryCarAccidentByConditions(CarAccident ca) {
		// TODO Auto-generated method stub
		return carAccidentMapper.queryCarAccidentByConditions(ca);
	}

	@Override
	public void addCarAccident(CarAccident ca) {
		// TODO Auto-generated method stub
		carAccidentMapper.insertCarAccident(ca);
	}

	@Override
	public void updateCarAccidentById(CarAccident ca) {
		// TODO Auto-generated method stub
		carAccidentMapper.updateCarAccidentById(ca);
	}
}
