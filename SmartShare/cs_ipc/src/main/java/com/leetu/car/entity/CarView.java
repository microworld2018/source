package com.leetu.car.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class CarView  implements Serializable{

	private static final long serialVersionUID = -8817789335974556024L;
	
	private String id;
	private String vehiclePlateId;//车牌号
	private String vin;//车架号
	private String color;//颜色
	private String bizState;//车辆状态402881e55235155f01523529f1070000 预下线 402881e6515734a601515757b4f70000 调度中402881ea4ef6740d014ef6782d390001 未租借 402881ea4ef6740d014ef6785ea70002 租借中402881ea4ef6740d014ef678a3360003 修理中 402881ea4ef6740d014ef678ded60004 停止使用402881ea4ef776e3014ef805ced4000\
	
	private String modelId;
	private String name;//车型名称
	private String brand;//车辆品牌
	private Integer seatingNum;//座位数
	private Integer casesNum;//车辆厢数
	private Integer standardMileage;//标准续航里程
	private String shizuStrategy;//时租 策略
	private String rizuStrategy;//日租 策略
	private String androidModelPhoto;//android图片
	private String iosModelPhoto;//IOS模型图片
	
	private String dotId;//换车点
	
	private String returnDotId;//默认换车点为取车网点
	private String returnDotName;//换车点名称
	//private String isUsed;//是有效关联 0表示关联失效
	
	private BigDecimal basePrice ;//基础价格
	private BigDecimal kmPrice;//里程价格
	private String timelyFeeUnit;//计费单位
	
	private Integer lifeMileage;//可续航里程
	
	private Boolean isTimelyHire =true ;//默认时租

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVehiclePlateId() {
		return vehiclePlateId;
	}

	public void setVehiclePlateId(String vehiclePlateId) {
		this.vehiclePlateId = vehiclePlateId;
	}

	public String getVin() {
		return vin;
	}

	public void setVin(String vin) {
		this.vin = vin;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getBizState() {
		return bizState;
	}

	public void setBizState(String bizState) {
		this.bizState = bizState;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Integer getSeatingNum() {
		return seatingNum;
	}

	public void setSeatingNum(Integer seatingNum) {
		this.seatingNum = seatingNum;
	}

	public Integer getCasesNum() {
		return casesNum;
	}

	public void setCasesNum(Integer casesNum) {
		this.casesNum = casesNum;
	}

	public Integer getStandardMileage() {
		return standardMileage;
	}

	public void setStandardMileage(Integer standardMileage) {
		this.standardMileage = standardMileage;
	}

	public String getShizuStrategy() {
		return shizuStrategy;
	}

	public void setShizuStrategy(String shizuStrategy) {
		this.shizuStrategy = shizuStrategy;
	}

	public String getRizuStrategy() {
		return rizuStrategy;
	}

	public void setRizuStrategy(String rizuStrategy) {
		this.rizuStrategy = rizuStrategy;
	}

	public String getAndroidModelPhoto() {
		return androidModelPhoto;
	}

	public void setAndroidModelPhoto(String androidModelPhoto) {
		this.androidModelPhoto = androidModelPhoto;
	}

	public String getIosModelPhoto() {
		return iosModelPhoto;
	}

	public void setIosModelPhoto(String iosModelPhoto) {
		this.iosModelPhoto = iosModelPhoto;
	}

	public String getDotId() {
		return dotId;
	}

	public void setDotId(String dotId) {
		this.dotId = dotId;
	}

	public String getReturnDotId() {
		return returnDotId;
	}

	public void setReturnDotId(String returnDotId) {
		this.returnDotId = returnDotId;
	}

	public String getReturnDotName() {
		return returnDotName;
	}

	public void setReturnDotName(String returnDotName) {
		this.returnDotName = returnDotName;
	}

	public BigDecimal getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(BigDecimal basePrice) {
		this.basePrice = basePrice;
	}

	public BigDecimal getKmPrice() {
		return kmPrice;
	}

	public void setKmPrice(BigDecimal kmPrice) {
		this.kmPrice = kmPrice;
	}

	public String getTimelyFeeUnit() {
		return timelyFeeUnit;
	}

	public void setTimelyFeeUnit(String timelyFeeUnit) {
		this.timelyFeeUnit = timelyFeeUnit;
	}

	public Integer getLifeMileage() {
		return lifeMileage;
	}

	public void setLifeMileage(Integer lifeMileage) {
		this.lifeMileage = lifeMileage;
	}

	public Boolean getIsTimelyHire() {
		return isTimelyHire;
	}

	public void setIsTimelyHire(Boolean isTimelyHire) {
		this.isTimelyHire = isTimelyHire;
	}

}
