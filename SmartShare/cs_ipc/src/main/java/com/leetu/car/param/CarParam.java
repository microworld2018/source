package com.leetu.car.param;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;

import net.sf.json.JSONObject;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.ground.user.entity.User;
import com.leetu.car.entity.Car;
import com.leetu.car.service.RealTimeStateService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.device.service.DeviceService;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.service.OrderService;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.sys.entity.RunLog;
import com.leetu.sys.service.RunLogService;
import com.util.Ajax;
import com.util.Constants;
import com.util.HttpRequestUtil;
import com.util.IpUtil;
import com.util.JsonTools;
import com.util.MapDistance;
import com.util.TokenUtils;
import com.util.car.CurrentCarInfo;

public class CarParam {

	
	/**
	 * 开关车门（1.0.1）-v2
	 * @param request
	 * @param orderService
	 * @param branchDotService
	 * @param realTimeStateService 
	 * @return
	 */
	public static String opOrClDoor(HttpServletRequest request,
			OrderService orderService,
			BranchDotService branchDotService, 
			RealTimeStateService realTimeStateService,
			DeviceBindingService deviceBindingService,
			RunLogService runLogService) {
		String result = null;
		Integer isOpenDoor = 0;//1打开车门成功 0打开车门失败 3关闭车门成功 2关闭车门失败 4鸣笛失败5鸣笛成功
		try{
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String subId = s.getId();
			//获取参数
			Map<String, String> map = JsonTools.desjsonForMap(data);
			if(map.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			String orderId = map.get("orderId");
			String state = map.get("state"); 
			String carId = map.get("carId");
			String lng = map.get("lng");//经度
			String lat = map.get("lat");//纬度
			
			String ip = IpUtil.getIpAddr(request);
			
			if(orderId==null){
				result = Ajax.AppJsonResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG);
				return result;
			}
			if(state==null){
				result = Ajax.AppJsonResult(Constants.APP_STATE_NULL, Constants.APP_STATE_NULL_MSG);
				return result;
			}
			if(carId==null){
				result = Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
				return result;
			}
			if(lng==null){
				result = Ajax.AppJsonResult(Constants.APP_LNG_NULL, Constants.APP_LNG_NULL_MSG);
				return result;
			}
			if(lat==null){
				result = Ajax.AppJsonResult(Constants.APP_LAT_NULL, Constants.APP_LAT_NULL_MSG);
				return result;
			}
			Orders order = orderService.orderInfoById(orderId);
			if(order==null){
				result = Ajax.AppJsonResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG);
				return result;
			}
			
			//根据车辆ID获取车辆设备绑定，车机编号SN
			Map<String, String> deviceBindingMap = deviceBindingService.getDeviceByCarId(carId);
			
			if(deviceBindingMap == null || StringUtils.isEmpty(deviceBindingMap)){
				return Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
			}
			
			String dotId = order.getBeginSiteId();
			//获取网点信息
			Map<String, Object> dot = branchDotService.dotInfo(dotId);
			String dotLat = dot.get("lat").toString();
			String dotLng = dot.get("lng").toString();
			//范围判断
			if(state.equals(Car.CAR_STATE_OPEN+"")){//开车门
				boolean start = false;
					if(order!=null&&!"".equals(order)){
						if(order.getState().equals(Orders.STATE_ORDER_START)){
//							String msgUrl = PropertiesUtil.getStringValue("msg_url")+"/car/openDoor";
							
							result = getDeviceByCarIdOpDoor(deviceBindingMap,state,CurrentCarInfo.OPEN_DOOR_URL);

							runLogService.addRunAppOpenDoor(order.getOrdersNo(), s.getName(), ip, deviceBindingMap.get("car_plate_no").toString(), RunLog.APP_OPEN_DOOR_MSG+result);
							
							return result;
						}
					}else{
						//当前会员没有订单 不可以 开车门
						start = false;
						return Ajax.AppJsonResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG);
					}
//				}
			}else if(state.equals(Car.CAR_STATE_CLOSE+"")){//关车门
				//关车门  
//				String msgUrl = PropertiesUtil.getStringValue("msg_url")+"/car/closeDoor";
				//添加操作日志
				result = getDeviceByCarIdOpDoor(deviceBindingMap,state,CurrentCarInfo.CLOSE_DOOR_URL);
				runLogService.addRunAppCloseDoor(order.getOrdersNo(), s.getName(), ip, deviceBindingMap.get("car_plate_no").toString(), RunLog.APP_CLOSE_DOOR_MSG+result);
				return result;
			}else if(state.equals(Car.CAR_STATE_WHISTLE+"")){//鸣笛
//				String msgUrl = PropertiesUtil.getStringValue("msg_url")+"/car/findCar";
				result = getDeviceByCarIdOpDoor(deviceBindingMap,state,CurrentCarInfo.FIND_CAR_URL);
				runLogService.addRunAppFindCar(order.getOrdersNo(), s.getName(), ip, deviceBindingMap.get("car_plate_no").toString(), RunLog.APP_FIND_CAR_MSG+result);
				return result;
			}else if(state.equals(Car.CAR_STATE_CLOSE_BLACKOUT+"")){//关中控锁并断电
//				String msgUrl = PropertiesUtil.getStringValue("msg_url")+"/car/lockPowerOff";
				result = getDeviceByCarIdOpDoor(deviceBindingMap,state,CurrentCarInfo.LOCK_POWER_OFF_URL);
				runLogService.addRunAppFindCar(order.getOrdersNo(), s.getName(), ip, deviceBindingMap.get("car_plate_no").toString(), "关中控锁并断电"+result);
				return result;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	
	public static String getDeviceByCarIdOpDoor(Map<String, String> deviceBindingMap,String state,String msgUrl){
		
		String result = null;
	
		Map<String,Object> res = new Hashtable<String,Object>();
	
		Map<String,String> paramMap = new HashMap<String, String>();
		
		paramMap.put("sn", deviceBindingMap.get("device_no"));//设备编号

//		String content = HttpClientUtils.simplePostInvoke(msgUrl, paramMap);
		
		System.out.println("====="+paramMap);
		
		String content = HttpRequestUtil.getPostMethod(msgUrl, paramMap);
		
		System.out.println("返回内容====content==="+content);
		
		if(content != null && !"".equals(content)){
			
			JSONObject obj = JSONObject.fromObject(content);
			
			if(obj.containsKey("result")){
				
				if(obj.getString("result") != null && !"".equals(obj.getString("result"))){
					
					if("false".equals(obj.getString("result"))){
						res.put("msgStatus", "error");
						
						if(state.equals(Car.CAR_STATE_OPEN+"")){
							//打开车门失败
							result = Ajax.AppResult(Constants.CAR_OPEN_DOOR_ERROR, Constants.CAR_OPEN_DOOR_ERROR_MSG,res);
//							return Car.CAR_OPEN_DOOR_ERROR;
						}else if(state.equals(Car.CAR_STATE_CLOSE+"")){
							//关闭车门失败
							result = Ajax.AppResult(Constants.CAR_OPEN_DOOR_ERROR, Constants.CAR_CLOSE_DOOR_ERROR_MSG,res);
//							return Car.CAR_CLOSE_DOOR_ERROR;
						}else if(state.equals(Car.CAR_STATE_WHISTLE+"")){
							//鸣笛失败
							result = Ajax.AppResult(Constants.CAR_STATE_WHISTLE_ERROR, Constants.CAR_STATE_WHISTLE_ERROR_MSG,res);
//							return Car.CAR_STATE_WHISTLE_ERROR;
						}else if("钥匙未关闭".equals(obj.getString("message"))){
							result = Ajax.AppResult(Constants.CAR_NOT_SHUT_DOWN, Constants.CAR_NOT_SHUT_DOWN_MSG,res);
						}
						
					}else{
						JSONObject resultObjct = JSONObject.fromObject(obj.getString("result"));
						
						int abc = resultObjct.getInt("result");
						System.out.println(abc);
						
						//返回码 成功（设备不在线为了测试使用，上线时去除该条件判断）
						if(resultObjct.getInt("result")==Car.CONTROL_CAR_SUCCESS ){
							
							res.put("msgStatus", "ok");
							
							if(state.equals(Car.CAR_STATE_CLOSE+"")){
								//关闭车门成功
								result = Ajax.AppResult(Constants.CAR_CLOSE_DOOR_SUCCESS, Constants.CAR_CLOSE_DOOR_SUCCESS_MSG,res);
							}else if(state.equals(Car.CAR_STATE_OPEN+"")){
								//打开车门成功
								result = Ajax.AppResult(Constants.CAR_OPEN_DOOR_SUCCESS,Constants.CAR_OPEN_DOOR_SUCCESS_MSG,res);
							}else if(state.equals(Car.CAR_STATE_WHISTLE+"")){
								//鸣笛成功
								result = Ajax.AppResult(Constants.CAR_STATE_WHISTLE_SUCCESS, Constants.CAR_STATE_WHISTLE_SUCCESS_MSG,res);
							}else if(state.equals(Car.CAR_STATE_CLOSE_BLACKOUT+"")){
								result = Ajax.AppResult(Constants.CAR_CLOSE_DOOR_SUCCESS, Constants.CAR_CLOSE_DOOR_SUCCESS_MSG,res);
							}
						}else if(resultObjct.getInt("result")==Car.CONTROL_CAR_NOT_ONLINE ){
								result = Ajax.AppResult(Constants.CAR_NOT_LINE, Constants.CAR_NOT_LINE_MSG,res);
						}else if(resultObjct.getInt("result")== 1105){
							result = Ajax.AppResult(Constants.CAR_NOT_SHUT_DOWN, Constants.CAR_NOT_SHUT_DOWN_MSG,res);
						}else if(resultObjct.getInt("result")== 1025){
							result = Ajax.AppResult(Constants.CAR_NOT_SHUT_DOWN, Constants.CAR_NOT_SHUT_DOWN_MSG,res);
						}else if(resultObjct.getInt("result")== 1027){
							result = Ajax.AppResult(Constants.CAR_CLOSE, Constants.CAR_CLOSE_MSG,res);
						}else if("false".equals(resultObjct.getString("result"))){
							result = Ajax.AppResult(Constants.CAR_NOT_SHUT_DOWN, Constants.CAR_NOT_SHUT_DOWN_MSG,res);
						}
					}
				}
			}
		}
		
		return result;
	
	}

	
	/**
	 * 车辆绑定车机接口（后台系统调用）
	 * @param request
	 * @param deviceService
	 * @return
	 */
	public static String addCarInfoRedis(HttpServletRequest request,DeviceService deviceService){
		//汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> ActiveIDs = new Hashtable<String,String>();
		//汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> ActiveSNs = new Hashtable<String,String>();
		//获取 已绑定设备的车辆信息
		List<Map<String,String>> cartilist = deviceService.carAndDevice(null);
		
		System.out.println("=====车机绑定====");
		
		if(cartilist != null && cartilist.size() > 0){
			for (Map<String, String> map : cartilist) {
				String carId = map.get("car_id");
				String deviceNo = map.get("device_no");
				ActiveIDs.put(carId, deviceNo);
				ActiveSNs.put(deviceNo, carId);
			}
		}
		
		RedisClient.setobjdata("ActiveIDs", ActiveIDs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("ActiveSNs", ActiveSNs, DataBaseUtil.CS_CAR_MSG);
		
		return  Ajax.AppJsonResult(Constants.APP_SUCCESS,Constants.APP_SUCCESS_MSG);
	}
	
	/**
	 * 判断用户当前位置与用户之间的距离是否在误差范围内
	 * @param lat
	 * @param lng
	 * @param dotlat
	 * @param dotlng
	 * @return
	 */
	public static boolean userDotDistance(String lat, String lng, String dotlat, String dotlng){
		boolean st = false;
		//计算当前用户与网点之间的距离
		double distance = MapDistance.Distance(Double.parseDouble(lat), Double.parseDouble(lng),Double.parseDouble(dotlat), Double.parseDouble(dotlng));
		double carDot = 500;//误差值
		if(carDot>distance){
			 st = true;
		}
		return st;
	}
	
	/**地勤端开关车门
	 * @param request
	 * @param orderService
	 * @param branchDotService
	 * @param realTimeStateService
	 * @param deviceBindingService
	 * @param runLogService
	 * @return
	 */
	public static String groundOpOrClDoor(HttpServletRequest request,
			DeviceBindingService deviceBindingService,
			RunLogService runLogService) {
		String result = null;
		try{
			//获取当前用户信息
			User s = (User) request.getSession().getAttribute("user");
			String ordersNo = request.getParameter("orderNo");
			String state = request.getParameter("state"); 
			String carId = request.getParameter("carId");
			
			String ip = IpUtil.getIpAddr(request);
			
			if(state==null){
				result = Ajax.AppJsonResult(Constants.APP_STATE_NULL, Constants.APP_STATE_NULL_MSG);
				return result;
			}
			if(carId==null){
				result = Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
				return result;
			}
			
			//根据车辆ID获取车辆设备绑定，车机编号SN
			Map<String, String> deviceBindingMap = deviceBindingService.getDeviceByCarId(carId);
			
			if(deviceBindingMap == null || StringUtils.isEmpty(deviceBindingMap)){
				return Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
			}
			//范围判断
			if(state.equals(Car.CAR_STATE_OPEN+"")){//开车门
					result = getDeviceByCarIdOpDoor(deviceBindingMap,state,CurrentCarInfo.OPEN_DOOR_URL);
					runLogService.addRunAppOpenDoor(ordersNo, s.getName(), ip, deviceBindingMap.get("car_plate_no").toString(), RunLog.APP_OPEN_DOOR_MSG);
					return result;
			}else if(state.equals(Car.CAR_STATE_CLOSE+"")){//关车门
				//关车门  
				result = getDeviceByCarIdOpDoor(deviceBindingMap,state,CurrentCarInfo.CLOSE_DOOR_URL);
				//添加操作日志
				runLogService.addRunAppCloseDoor(ordersNo, s.getName(), ip, deviceBindingMap.get("car_plate_no").toString(), RunLog.APP_CLOSE_DOOR_MSG);
				return result;
			}else if(state.equals(Car.CAR_STATE_WHISTLE+"")){//鸣笛
				runLogService.addRunAppFindCar(ordersNo, s.getName(), ip, deviceBindingMap.get("car_plate_no").toString(), RunLog.APP_FIND_CAR_MSG);
				result = getDeviceByCarIdOpDoor(deviceBindingMap,state,CurrentCarInfo.FIND_CAR_URL);
				return result;
			}else if(state.equals(Car.CAR_STATE_CLOSE_BLACKOUT+"")){//关中控锁并断电
				result = getDeviceByCarIdOpDoor(deviceBindingMap,state,CurrentCarInfo.LOCK_POWER_OFF_URL);
				return result;
			}
		}catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
}
