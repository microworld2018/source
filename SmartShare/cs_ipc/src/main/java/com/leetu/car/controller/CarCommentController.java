package com.leetu.car.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.car.entity.CarComment;
import com.leetu.car.service.CarCommentService;
import com.leetu.subscriber.entity.Subscriber;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.Utils;

/**
 * @author zhangsg
 * @since 2016年7月29日 上午9:50:56
 */
@Controller
@RequestMapping("comment")
public class CarCommentController extends BaseController<CarCommentController>   {

	@Autowired
	private CarCommentService carCommentService;
	
	
	/**
	 * 车辆评价表
	 * @version 1.0.1
	 */
	@RequestMapping("/makeCarComment")
	public void makeCarComment()
	{
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			if(null == s){
				result =  Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN, Constants.APP_LOGIN_TOKEN_MSG);
				 Utils.outJSONObject(result,response);
			}
			CarComment cc = new CarComment();
			pmap.remove("token");
			pmap.remove("param");
			cc =Utils.map2Bean(pmap, cc);
			cc.setSubscriberId(s.getId());
//			cc.setSubscriberName(s.getName());
			this.carCommentService.addCarComment(cc);
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG );
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 查询车辆评价  
	 * param : (orderId 、 carId) or id or 会员id
	 * 返回一条车辆评价记录
	 * @version 1.0.1
	 */
	@RequestMapping("/queryCarComment")
	public void queryCarComment(){
		
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			CarComment cc = new CarComment();
			pmap.remove("token");
			pmap.remove("param");
			cc =Utils.map2Bean(pmap, cc);
			
			if(null == s || StringHelper.isEmpty(cc.getCarId())  || StringHelper.isEmpty(cc.getOrderId()) ){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				 Utils.outJSONObject(result,response);
			}
			
			cc.setSubscriberId(s.getId());
			cc=this.carCommentService.queryCarCommentByConditions(cc);
			Map<Object,Object> rm = new HashMap<Object, Object>();
			rm.put("carComment", cc);
			
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG ,rm);
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 更细车辆评价通过ID
	 * 
	 * @version 1.0.1
	 */
	@RequestMapping("/updateCarComment")
	public void updateCarComment(){
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			CarComment cc = new CarComment();
			pmap.remove("token");
			pmap.remove("param");
			cc =Utils.map2Bean(pmap, cc);
			if(null == s  ||StringHelper.isEmpty(cc.getId()) ){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				 Utils.outJSONObject(result,response);
			}
			cc.setSubscriberId(s.getId());
			/*cc.setSubscriberName(s.getName());*/
			this.carCommentService.updateCarCommentById(cc);
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG );
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
}
