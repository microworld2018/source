package com.leetu.account.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.entity.RechargeCard;
import com.leetu.account.service.AccountService;
import com.leetu.account.service.AccountTradeRecordService;
import com.leetu.account.service.RechargeCardService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.TokenUtils;
import com.util.Utils;

/**
 * @author jyt
 * @since 2016年7月22日 下午5:24:30
 */
@Controller
@RequestMapping("account")
public class AccountController extends BaseController<AccountController> {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private AccountTradeRecordService accountTradeRecordService; 
	
	@Autowired
	private RechargeCardService rechargeCardService;
	
	@Autowired
	private DictService dictService;
	
	private SimpleDateFormat  sdf =new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	
	@RequestMapping("/getAccountByToken")
	public void getAccountByToken(){
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);
			if(null != s){
				//1.查询会员账号余额
				String id = s.getId();
				Account acc =this.accountService.queryAccountBySubscriberId(id);
				HashMap<String, Object> rm = new HashMap<String, Object>();
				rm.put("amount", acc.getAmount());
				rm.put("subscriberId", id);
				//查询会员充值记录
				List<AccountTradeRecord>  list =this.accountTradeRecordService.queryAccountTradeRecordBySubscriberId(id);
				List<HashMap<String, Object>> rList = new ArrayList<HashMap<String, Object>>();
				for(AccountTradeRecord tr :list){
					HashMap<String, Object> m = new HashMap<String, Object>();
					m.put("tradeTime", sdf.format(tr.getTradeTime()));
					String channel = Account.getPayChannel(tr.getPayChannel());
					m.put("payChannel", channel+"充值");
					m.put("amount",tr.getAmount());
					rList.add(m);
				}
				rm.put("charges", rList);
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, rm);
			}else{
				result = Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN, Constants.APP_LOGIN_TOKEN_MSG);
			}
			
		}catch(Exception e){
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	@RequestMapping("/getAvailableRechargeMoney")
	public void getAvailableRechargeMoney()
	{
		String result = ""; 
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);
			if(null ==s ){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_REQUIRED_ERROR, Constants.APP_PARAM_REQUIRED_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			
			List<Dict> dicts = dictService.getDictByDictGroupCode("11");//充值金额
			HashMap<String, Object> rm = new HashMap<String, Object>();
			List<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
			rm.put("rechargeMoney", list);
			for(Dict rc :dicts)
			{
				Map<String,Object> m =new HashMap<String, Object>();
				m.put("id", rc.getId());
				m.put("showName", rc.getCnName());
				m.put("amount", rc.getCode());
				list.add(m);
			}
			result =Ajax.AppbzJsonResult(Constants.APP_RESULT_CODE_SUCCESS, "成功", rm);
		 } catch (Exception e) {
			 result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
		Utils.outJSONObject(result,response);
	}
	
	//@RequestMapping("/getAvailableRechargeCards")
	public void getAvailableRechargeCards(){
		String result = ""; 
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);
			if(null ==s ){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_REQUIRED_ERROR, Constants.APP_PARAM_REQUIRED_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			List<RechargeCard> cards = rechargeCardService.getAvailableRechargeCards();
			HashMap<String, Object> rm = new HashMap<String, Object>();
			List<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
			rm.put("rechargeCards", list);
			for(RechargeCard rc :cards)
			{
				list.add(Utils.bean2Map(rc));
			}
			result =Ajax.AppbzJsonResult(Constants.APP_RESULT_CODE_SUCCESS, "成功", rm);
		 } catch (Exception e) {
			 result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
}
