package com.leetu.account.service;

import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.entity.PayResponseMessage;

/**
 * 充值业务接口
 * 
 * @author jyt
 * @since 2016年8月4日 上午10:12:23
 */
public interface RechargeService {
	
	/**
	 * 创建充值交易记录
	 * 自助充值
	 * @return
	 * @version 1.0.1
	 */
	public AccountTradeRecord addRechargeRecord(String subscriberId,String rechargeId, Double customAmount,Integer payChannel,Integer payType) throws Exception;
	
	/**
	 * 充值成功回调函数
	 * 
	 * @version 1.0.1
	 */
	public void updateRechargeTrade(PayResponseMessage payResponseMessage)throws Exception;

}
