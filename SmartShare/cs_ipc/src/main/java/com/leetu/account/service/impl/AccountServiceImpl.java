package com.leetu.account.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.mapper.AccountMapper;
import com.leetu.account.service.AccountService;

/**
 * @author jyt
 * @since 2016年7月22日 下午5:49:41
 */
@Service
public class AccountServiceImpl  implements AccountService{

	@Autowired
	private AccountMapper accountMapper;
	
	@Override
	public Account queryAccountBySubscriberId(String id) {
		return this.accountMapper.queryAccountBySubscriberId(id);
	}

	@Override
	public void updateAccount(Map<String, Object> param) {
		accountMapper.updateAccount(param);
	}

}
