package com.leetu.account.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.entity.AccountTradeRecordDetail;

public interface AccountTradeRecordMapper {

	/**
	 * 添加交易记录
	 * @param param
	 */
	void addtradeRecord(Map<String, Object> param);

	/**
	 *  支付成功后  交易状态
	 * @param orderNo
	 */
	void upRecordState(@Param("orderNo")String orderNo,@Param("tradeOrderNo")String tradeOrderNo);

	/**
	 * 会员押金充值成功后修改交易状态
	 * @param tradeOrderNo
	 */
	void upRecordStateByTradeOrderNo(@Param("tradeOrderNo")String tradeOrderNo);
	/**
	 * 通过订单交易号查询
	 * @param orderId
	 * @param typeRecharge
	 * @return
	 * @version 1.0.1
	 */
	List<AccountTradeRecord> queryTradeRecordByTradeOrderNo(@Param("orderId")String orderId,
			@Param("typeRecharge")Integer typeRecharge);

	/**
	 * 	更新交易记录
	 * @param accountTradeRecord
	 * @version 1.0.1
	 */
	void updateAccountTradeRecordById(AccountTradeRecord accountTradeRecord);
	
	
	///////////////交易详情操作接口
	
	/**
	 * 添加交易详情记录
	 * @param accountTradeRecordDetail
	 * @version 1.0.1
	 */
	void addAccountTradeRecordDetail(
			AccountTradeRecordDetail accountTradeRecordDetail);

	/**
	 * 更细交易详情记录
	 * @param accountTradeRecordDetail
	 * @version 1.0.1
	 */
	void updateAccountTradeRecordDetail(
			AccountTradeRecordDetail accountTradeRecordDetail);

	/**
	 * 通过交易记录ID查询交易详情集合
	 * @param tradeRecordId
	 * @return
	 * @version 1.0.1
	 */
	List<AccountTradeRecordDetail> getAccountTradeRecordDetailByTradeRecordId(@Param("tradeRecordId")String tradeRecordId);

	/**
	 * 根据会员ID查询会员充值记录
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	List<AccountTradeRecord> queryAccountTradeRecordBySubscriberId(@Param("subscriberId")String id);
	
	/**根据订单编号查询交易记录
	 * @param orderNo
	 * @return
	 */
	List<AccountTradeRecord> queryOrderPayState(@Param("orderNo")String orderNo);
	
	/**
	 * 根据会员ID,交易流水号查询交易成功记录信息
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	AccountTradeRecord getAccountTradeRecordBySubscriberId(@Param("subId")String subId,@Param("tradeOrderNo")String tradeOrderNo);

	/**
	 * 微信查询确认支付成功后，修改交易记录状态为已支付
	 * @param orderNo
	 */
	void updateTradeRecordStateToAlreadyPaid(@Param("orderNo")String orderNo);

	/**
	 * 微信查询失败或金额不对等情况，修改交易记录状态为未支付
	 * @param orderNo
	 */
	void updateOrderStateToUnPaid(@Param("orderNo")String orderNo);

	/**
	 * 根据订单号查询交易记录表中已支付状态的记录
	 * @author Jin Guangyu
	 * @date 2017年7月24日下午3:42:39
	 * @param ordersNo
	 * @return
	 */
	List<AccountTradeRecord> getTradeRecordIsPaid(String ordersNo);

	/**
	 * 修改最新的交易记录状态为已支付
	 * @author Jin Guangyu
	 * @date 2017年7月26日下午3:15:14
	 * @param ordersNo
	 */
	void updateTradeRecordNew(@Param("ordersNo")String ordersNo);

	/**
	 * 根据订单号查交易记录表最新一条
	 * @author Jin Guangyu
	 * @date 2017年7月26日下午3:57:10
	 * @param ordersNo
	 */
	AccountTradeRecord getTradeRecordNew(@Param("ordersNo")String ordersNo);

	/**
	 * 根据对象更新
	 * @author Jin Guangyu
	 * @date 2017年7月28日下午1:34:03
	 * @param accountTradeRecord
	 */
	void updateAccountTradeRecord(AccountTradeRecord accountTradeRecord);

	/**
	 * 添加交易记录并返回主键
	 * @author Jin Guangyu
	 * @date 2017年7月28日下午3:09:40
	 * @param pam
	 * @return
	 */
	String addAccountTradeRecord(Map<String, Object> pam);

	/**
	 * 根据交易记录号获取对象
	 * @author Jin Guangyu
	 * @date 2017年7月29日上午10:39:55
	 * @param outTradeNo
	 * @return
	 */
	AccountTradeRecord getAccountTradeRecordByTradeNo(@Param("tradeOrderNo")String outTradeNo);
	
}
