package com.leetu.account.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.mapper.AccountTradeRecordMapper;
import com.leetu.account.service.AccountTradeRecordService;

@Service
public class AccountTradeRecordServiceImpl implements AccountTradeRecordService{

	@Autowired
	private AccountTradeRecordMapper accountTradeRecordMapper;
	
	@Override
	public void addtradeRecord(Map<String, Object> param) {
		accountTradeRecordMapper.addtradeRecord(param);
	}

	@Override
	public void updateRecordState(String orderNo,String tradeOrderNo) {
		accountTradeRecordMapper.upRecordState(orderNo,tradeOrderNo);
	}
	@Override
	public void updateRecordStateByTradeOrderNo(String tradeOrderNo){
		accountTradeRecordMapper.upRecordStateByTradeOrderNo(tradeOrderNo);
	}

	@Override
	public List<AccountTradeRecord> queryAccountTradeRecordBySubscriberId(
			String id) {
		return accountTradeRecordMapper.queryAccountTradeRecordBySubscriberId(id);
	}

	@Override
	public List<AccountTradeRecord> queryOrderPayState(String orderNo) {
		// TODO Auto-generated method stub
		return accountTradeRecordMapper.queryOrderPayState(orderNo);
	}
	
	/**
	 * 根据会员ID,交易流水号查询交易成功记录信息
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	public AccountTradeRecord getAccountTradeRecordBySubscriberId(String subId,String tradeOrderNo){
		
		return accountTradeRecordMapper.getAccountTradeRecordBySubscriberId(subId, tradeOrderNo);
	}

	/**
	 * 微信查询确认支付成功后，修改交易记录状态为已支付
	 */
	@Override
	public void updateTradeRecordStateToAlreadyPaid(String orderNo) {
		accountTradeRecordMapper.updateTradeRecordStateToAlreadyPaid(orderNo);
	}

	/**
	 * 微信查询失败或金额不对等情况，修改交易记录状态为未支付
	 */
	@Override
	public void updateOrderStateToUnPaid(String orderNo) {
		accountTradeRecordMapper.updateOrderStateToUnPaid(orderNo);
	}

	@Override
	public List<AccountTradeRecord> getTradeRecordIsPaid(String ordersNo) {
		return accountTradeRecordMapper.getTradeRecordIsPaid(ordersNo);
	}

	/**
	 * 修改最新的交易记录状态为已支付
	 */
	@Override
	@Transactional
	public void updateTradeRecordNew(String ordersNo) {
		accountTradeRecordMapper.updateTradeRecordNew(ordersNo);
	}

	/**
	 * 根据订单号查交易记录表最新一条
	 */
	@Override
	public AccountTradeRecord getTradeRecordNew(String ordersNo) {
		return accountTradeRecordMapper.getTradeRecordNew(ordersNo);
	}

	/**
	 * 根据对象更新
	 */
	@Override
	public void updateAccountTradeRecord(AccountTradeRecord accountTradeRecord) {
		accountTradeRecordMapper.updateAccountTradeRecord(accountTradeRecord);
	}

	/**
	 * 添加交易记录并返回主键
	 */
	@Override
	public String addAccountTradeRecord(Map<String, Object> pam) {
		return accountTradeRecordMapper.addAccountTradeRecord(pam);
	}

	/**
	 * 根据交易记录号获取对象
	 */
	@Override
	public AccountTradeRecord getAccountTradeRecordByTradeNo(String outTradeNo) {
		return accountTradeRecordMapper.getAccountTradeRecordByTradeNo(outTradeNo);
	}
	
}
