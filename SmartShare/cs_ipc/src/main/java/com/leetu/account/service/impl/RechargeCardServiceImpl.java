package com.leetu.account.service.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.account.entity.RechargeCard;
import com.leetu.account.mapper.RechargeCardMapper;
import com.leetu.account.service.RechargeCardService;

/**
 * 充值卡
 * @author jyt
 * @since 2016年8月4日 下午3:36:41
 */
@Service
public class RechargeCardServiceImpl implements RechargeCardService {

	@Autowired
	private RechargeCardMapper rechargeCardMapper;
	@Override
	public List<RechargeCard> getAvailableRechargeCards() {
		return rechargeCardMapper.getAvailableRechargeCards();
	}

	@Override
	public RechargeCard getRechargeCardById(@Param("id")String bizId) {
		return rechargeCardMapper.getRechargeCardById(bizId);
	}

}
