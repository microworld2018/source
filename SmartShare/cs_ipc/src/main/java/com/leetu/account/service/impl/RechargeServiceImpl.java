package com.leetu.account.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountPaymentAccount;
import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.entity.AccountTradeRecordDetail;
import com.leetu.account.entity.PayResponseMessage;
import com.leetu.account.mapper.AccountCardInstanceMapper;
import com.leetu.account.mapper.AccountMapper;
import com.leetu.account.mapper.AccountPaymentAccountMapper;
import com.leetu.account.mapper.AccountTradeRecordMapper;
import com.leetu.account.service.RechargeCardService;
import com.leetu.account.service.RechargeService;
import com.leetu.account.util.TradeRecordNo;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.service.DictService;
import com.util.Utils;

/**
 *  充值业务实现
 * @author jyt
 * @since 2016年8月4日 上午10:13:09
 */
@Service
public class RechargeServiceImpl  implements RechargeService{

	@Autowired
	private RechargeCardService rechargeCardService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private AccountMapper accountMapper;
	
	@Autowired
	private AccountTradeRecordMapper accountTradeRecordMapper;
	
	
	@Autowired
	private AccountPaymentAccountMapper accountPaymentAccountMapper;
	
	@Autowired
	private AccountCardInstanceMapper accountCardInstanceMapper;
	
	/**
	 * 创建充值记录--走支付流程
	 * 自助
	 * @return 
	 * @throws Exception 
	 */
	@Override
	public AccountTradeRecord addRechargeRecord(String subscriberId,String rechargeId, Double customAmount,Integer payChannel,Integer payType) throws Exception {
		
		//1.校验     
		//充值卡有效性校验
		Dict dict = dictService.getDictById(rechargeId);
		if(dict==null ||Double.valueOf(dict.getCode()).compareTo(customAmount)!=0 ){
			throw new Exception("根据"+rechargeId+"未查询到可用充值金额，或充值金额与实际不匹配");
		}
		
		//会员校验
		Account account=accountMapper.queryAccountBySubscriberId(subscriberId);
		
		if(account==null){
			throw new Exception("未查询到会员");
		}
		
		//2.交易记录主表，增加记录
		AccountTradeRecord accountTradeRecord= new AccountTradeRecord();
		accountTradeRecord.setSubscriberId(subscriberId);
		accountTradeRecord.setType(Account.TYPE_RECHARGE);
		accountTradeRecord.setPayChannel(payChannel);
		accountTradeRecord.setPayType(payType);
		accountTradeRecord.setResult(AccountTradeRecord.RESULT_CREATE);
		accountTradeRecord.setBizId(rechargeId);
		accountTradeRecord.setSubOrderId(null);//子订单
		accountTradeRecord.setTradeOrderNo(TradeRecordNo.getPayRechargeTradeNo());
		
		
		accountTradeRecord.setIsPresetCard(AccountTradeRecord.IS_PRESET_CARD_FALSE);
		accountTradeRecord.setDescription("自助充值"+customAmount+"元");
		
		accountTradeRecord.setTradeTime(new Date());
		accountTradeRecord.setTs(new Date());
		accountTradeRecord.setAmount(customAmount);
		accountTradeRecord.setResult(AccountTradeRecord.RESULT_CREATE);
		Map<String, Object> map = Utils.bean2Map(accountTradeRecord);
		accountTradeRecordMapper.addtradeRecord(map);
		accountTradeRecord = Utils.map2Bean(map,accountTradeRecord);
		
		//3.交易记录子表，添加记录
		AccountTradeRecordDetail accountTradeRecordDetail= new AccountTradeRecordDetail();
		accountTradeRecordDetail.setTradeRecordId(accountTradeRecord.getId());
		accountTradeRecordDetail.setSubscriberId(subscriberId);
		accountTradeRecordDetail.setAmount(customAmount);
		accountTradeRecordDetail.setDescription(null);
		accountTradeRecordDetail.setPaymentAccountId(null);
		accountTradeRecordDetail.setTs(new Date());
		accountTradeRecordMapper.addAccountTradeRecordDetail(accountTradeRecordDetail);
		
		accountTradeRecord.setAccountTradeRecordDetail(accountTradeRecordDetail);
		return accountTradeRecord;
		
	}

	@Override
	public void updateRechargeTrade(PayResponseMessage payResponseMessage) throws Exception{


		//1.充值记录校验  
				List<AccountTradeRecord> accountTradeRecordList=accountTradeRecordMapper.queryTradeRecordByTradeOrderNo(payResponseMessage.getOrderId(),Account.TYPE_RECHARGE);
				if(accountTradeRecordList==null||accountTradeRecordList.size()==0){
					throw new Exception("充值订单不存在");
				}
				if(accountTradeRecordList.size()>1){
					throw new Exception("根据付款单号"+payResponseMessage.getOrderId()+"查到多笔充值记录");
				}
				AccountTradeRecord accountTradeRecord=accountTradeRecordList.get(0);
				if(!Account.TYPE_RECHARGE.equals(accountTradeRecord.getType())){
					throw new Exception("不为充值订单");
				}
				if(AccountTradeRecord.RESULT_SUCCESS.equals(accountTradeRecord.getResult())){
					return;
				}
				
				
				//3.账户主表变更 
				Account account=accountMapper.queryAccountBySubscriberId(accountTradeRecord.getSubscriberId()); 
				//更新总金额
				BigDecimal accountAmount = new BigDecimal(account.getAmount());
				BigDecimal rechargeAmount = new BigDecimal(accountTradeRecord.getAmount());  
				account.setAmount( new Double(accountAmount.add(rechargeAmount).doubleValue()));
				
				Dict dict = dictService.getDictByCodes("maxFrozenMoney", "maxFrozenMoney");
				
				//规定押金
				BigDecimal maxFrozenMoney = new BigDecimal(Integer.parseInt(dict.getCnName()));
				
				//实际可用金额
				BigDecimal usableAmount = new BigDecimal(account.getUsableAmount()); 
				//实际押金
				BigDecimal frozenAmount = new BigDecimal(account.getFrozenAmount()); 
				//account 押金及可用余额
				if(  frozenAmount.add(rechargeAmount).compareTo(maxFrozenMoney)<=0)
				{
					account.setFrozenAmount(new Double(frozenAmount.add(rechargeAmount).doubleValue()));
					
				}else if(frozenAmount.compareTo(maxFrozenMoney)==0){
					account.setUsableAmount(new Double(usableAmount.add(rechargeAmount).doubleValue()));
				}else{
					BigDecimal decimal = frozenAmount.add(rechargeAmount).subtract(maxFrozenMoney);
					account.setUsableAmount(new Double(usableAmount.add(decimal).doubleValue()));
				}
				accountMapper.updateAccount(Utils.bean2Map(account));
				
				
				AccountPaymentAccount paymentAccount=null;
				if(!StringUtils.isEmpty(payResponseMessage.getAccNo())){
					paymentAccount=accountPaymentAccountMapper.queryPaymentAccountByAccountNoAndSubscriberId(payResponseMessage.getAccNo(), accountTradeRecord.getSubscriberId());
					if(paymentAccount==null){
						paymentAccount=new AccountPaymentAccount();
						paymentAccount.setType(payResponseMessage.getPayType());
						paymentAccount.setAccountNo(payResponseMessage.getAccNo());
						paymentAccount.setSubscriberId(accountTradeRecord.getSubscriberId());
						paymentAccount.setTs(new Date());
						accountPaymentAccountMapper.addAccountPaymentAccount(paymentAccount);
					}
				}
				
				
				//更新交易记录表
				accountTradeRecord.setRemainingAmount(account.getAmount());
				accountTradeRecord.setResult(AccountTradeRecord.RESULT_SUCCESS);
				accountTradeRecordMapper.updateAccountTradeRecordById(accountTradeRecord);
				
				//6.更新交易记录详情表
				List<AccountTradeRecordDetail> list=accountTradeRecordMapper.getAccountTradeRecordDetailByTradeRecordId(accountTradeRecord.getId());
				if(list==null || list.size()==0){
					throw new Exception("未查询到交易记录详情");
				}
				AccountTradeRecordDetail accountTradeRecordDetail=list.get(0);
				
				accountTradeRecordDetail.setTradeNo(payResponseMessage.getQueryId());
				if(paymentAccount!=null){
					accountTradeRecordDetail.setPaymentAccountId(paymentAccount.getId());
				}
				accountTradeRecordMapper.updateAccountTradeRecordDetail(accountTradeRecordDetail);

	}

}
