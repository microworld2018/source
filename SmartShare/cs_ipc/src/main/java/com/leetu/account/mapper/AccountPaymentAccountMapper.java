package com.leetu.account.mapper;

import org.apache.ibatis.annotations.Param;

import com.leetu.account.entity.AccountPaymentAccount;

/**
 *  会员账号，和支付账号支付关联实例
 * @author jyt
 * @since 2016年8月4日 上午11:38:27
 */
public interface AccountPaymentAccountMapper {

	/**
	 * 通过会员ID和会员账户ID查询支付账号关联实例
	 * 根据会员ID和支付账户NO（支付宝ID ,微信ID ,银行卡号等）获取交易记录
	 * @param accNo
	 * @param subscriberId
	 * @return
	 * @version 1.0.1
	 */
	AccountPaymentAccount queryPaymentAccountByAccountNoAndSubscriberId(
			@Param("accNo")String accNo, @Param("subscriberId")String subscriberId);

	/**
	 * 添加会员账户 和第三方支付平台的账户ID交易关系
	 * @param paymentAccount
	 * @version 1.0.1
	 */
	void addAccountPaymentAccount(AccountPaymentAccount paymentAccount);

}
