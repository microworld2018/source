package com.leetu.account.mapper;

import java.util.List;

import com.leetu.account.entity.RechargeCard;

/**
 * @author jyt
 * @since 2016年8月4日 下午3:38:27
 */
public interface RechargeCardMapper {

	/**获取有效充值卡
	 * @return
	 * @version 1.0.1
	 */
	List<RechargeCard> getAvailableRechargeCards();

	/**
	 * 获取有效充值卡
	 * @param bizId
	 * @return
	 * @version 1.0.1
	 */
	RechargeCard getRechargeCardById(String bizId);

}
