package com.leetu.account.entity;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;



/**
 * 充值卡
 * @Author liusong
 * @Description 
 * @Version 2.0,2015-11-16
 */
public class RechargeCard  implements Serializable{
	
	private static final long serialVersionUID = -7747645354169733905L;
	
	public static final Integer IS_VALID_TRUE =1;
	public static final Integer IS_VALID_FALSE=0; 
	
	public static final String CHANNEL_PORTAL="P";
	public static final String CHANNEL_WECHAT="W";
	public static final String CHANNEL_CONSOLE="C";
	
	//ID")
	@JsonProperty("id")
	private String id;
	//名称")
	@JsonIgnore
	private String name;
	//展示名称")
	@JsonProperty("showName")
	private String showName;
	//有效起始时间")
	@JsonIgnore
	private Date startValidTime;
	//有效终止时间")
	@JsonIgnore
	private Date endValidTime;
	//是否有效")
	@JsonIgnore
	private Integer isValid;
	//金额")
	@JsonProperty("amount")
	private Double amount;
	//渠道")
	@JsonIgnore
	private String channel;
	//排序字段")
	@JsonIgnore
	private Integer sortField;
	//创建人")
	@JsonIgnore
	private String creatorId;
	//创建人姓名")
	@JsonIgnore
	private String creatorName;
	//创建时间")
	@JsonIgnore
	private Date createTime;
	@JsonIgnore
	private Date ts;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartValidTime() {
		return startValidTime;
	}
	public void setStartValidTime(Date startValidTime) {
		this.startValidTime = startValidTime;
	}
	public Date getEndValidTime() {
		return endValidTime;
	}
	public void setEndValidTime(Date endValidTime) {
		this.endValidTime = endValidTime;
	}
	public Integer getIsValid() {
		return isValid;
	}
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}
	
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getCreatorId() {
		return creatorId;
	}
	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Date getTs() {
		return ts;
	}
	public void setTs(Date ts) {
		this.ts = ts;
	}
	public Integer getSortField() {
		return sortField;
	}
	public void setSortField(Integer sortField) {
		this.sortField = sortField;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public String getShowName() {
		return showName;
	}
	public void setShowName(String showName) {
		this.showName = showName;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	
	
}
