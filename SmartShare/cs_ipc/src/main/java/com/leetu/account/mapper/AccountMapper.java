package com.leetu.account.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountTradeRecord;

/**
 * @author jyt
 * @since 2016年7月22日 下午5:51:12
 */
public interface AccountMapper {

	/**
	 * 通过会员ID查询会员Account信息
	 * @param id
	 * @return
	 */
	Account queryAccountBySubscriberId(@Param("subscriberId")String id);

	/**
	 * 根据id查询账户信息
	 * @param id
	 * @return
	 */
	Map<String, Object> subAccountInfo(@Param("id")String id);

	/**
	 * 修改账户余额
	 * @param param
	 */
	void updateAccount(Map<String, Object> param);

}
