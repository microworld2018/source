package com.leetu.account.service;

import java.util.List;
import java.util.Map;

import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountTradeRecord;

/**
 * @author jyt
 * @since 2016年7月22日 下午5:27:29
 */
public interface AccountService {

	/**
	 * 根据会员ID获取会员余额
	 * @param id
	 * @return
	 */
	Account queryAccountBySubscriberId(String id);

	/**
	 * 修改账户余额
	 * @param param
	 */
	void updateAccount(Map<String, Object> param);

}
