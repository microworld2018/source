package com.leetu.account.mapper;

import java.util.List;

import com.leetu.account.entity.AccountCardInstance;

/**
 * 充值卡充值，关联实例
 * @author jyt
 * @since 2016年8月4日 上午11:40:34
 */
public interface AccountCardInstanceMapper {

	/**
	 * 添加一个充值卡关系实例
	 * @param instance
	 * @version 1.0.1
	 */
	 void addAccountCardInstance(AccountCardInstance instance); 

	/**
	 * 查询有效充值实例
	 * @param subscriberId
	 * @return
	 * @version 1.0.1
	 */
	 List<AccountCardInstance> queryEnabledCardInstanceByAccountId(
			String subscriberId);

}
