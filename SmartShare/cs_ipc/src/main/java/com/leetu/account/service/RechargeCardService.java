package com.leetu.account.service;

import java.util.List;

import com.leetu.account.entity.RechargeCard;

/**
 * 充值卡接口
 * @author jyt
 * @since 2016年8月4日 上午10:48:17
 */
public interface RechargeCardService {

	
	/**
	 * 获取有效的充值卡集合
	 * @return
	 * @version 1.0.1
	 */
	List<RechargeCard> getAvailableRechargeCards();
	
	/**
	 * 查询充值卡ID查充值卡
	 * @param bizId
	 * @return
	 * @version 1.0.1
	 */
	RechargeCard getRechargeCardById(String bizId);
	
	

}
