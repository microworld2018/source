package com.leetu.account.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.account.entity.AccountTradeRecord;

public interface AccountTradeRecordService {

	/**
	 * 添加交易记录
	 * @param param
	 */
	void addtradeRecord(Map<String, Object> param);

	/**
	 * 支付成功后  交易状态
	 * @param orderNo
	 */
	void updateRecordState(String orderNo,String tradeOrderNo);
	
	/**
	 * 会员押金充值成功后 修改交易状态
	 * @param tradeOrderNo
	 */
	void updateRecordStateByTradeOrderNo(String tradeOrderNo);

	/** 根据会员ID查询会员充值记录
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	List<AccountTradeRecord> queryAccountTradeRecordBySubscriberId(String id);
	
	
	/**根据订单编号查询交易记录
	 * @param orderNo
	 * @return
	 */
	List<AccountTradeRecord> queryOrderPayState(String orderNo);
	
	/**
	 * 根据会员ID,交易流水号查询交易成功记录信息
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	AccountTradeRecord getAccountTradeRecordBySubscriberId(String subId,String tradeOrderNo);

	/**
	 * 微信查询确认支付成功后，修改交易记录状态为已支付
	 * @param orderNo
	 */
	void updateTradeRecordStateToAlreadyPaid(String orderNo);

	/**
	 * 微信查询失败或金额不对等，修改交易记录状态为未支付
	 * @param orderNo
	 */
	void updateOrderStateToUnPaid(String orderNo);

	/**
	 * 根据订单号查询交易记录表中已支付状态的记录
	 * @author Jin Guangyu
	 * @date 2017年7月24日下午3:40:13
	 * @param ordersNo 订单号
	 * @return
	 */
	List<AccountTradeRecord> getTradeRecordIsPaid(String ordersNo);

	/**
	 * 修改最新的交易记录状态为已支付
	 * @author Jin Guangyu
	 * @date 2017年7月26日下午3:13:37
	 * @param ordersNo
	 */
	void updateTradeRecordNew(String ordersNo);

	/**
	 * 根据订单号查交易记录表最新一条
	 * @author Jin Guangyu
	 * @date 2017年7月26日下午3:55:41
	 * @param ordersNo
	 */
	AccountTradeRecord getTradeRecordNew(String ordersNo);

	/**
	 * 根据对象更新
	 * @author Jin Guangyu
	 * @date 2017年7月28日下午1:33:02
	 * @param accountTradeRecord
	 */
	void updateAccountTradeRecord(AccountTradeRecord accountTradeRecord);

	/**
	 * 添加交易记录并返回主键
	 * @author Jin Guangyu
	 * @date 2017年7月28日下午3:08:45
	 * @param pam
	 * @return
	 */
	String addAccountTradeRecord(Map<String, Object> pam);

	/**
	 * 根据交易记录号获取对象
	 * @author Jin Guangyu
	 * @date 2017年7月29日上午10:38:08
	 * @param outTradeNo
	 * @return
	 */
	AccountTradeRecord getAccountTradeRecordByTradeNo(String outTradeNo);
	

}
