package com.leetu.account.controller;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.entity.PayResponseMessage;
import com.leetu.account.service.RechargeCardService;
import com.leetu.account.service.RechargeService;
import com.leetu.orders.param.PayOrderParam;
import com.leetu.subscriber.entity.Subscriber;
import com.util.Ajax;
import com.util.Constants;
import com.util.ContantsTool;
import com.util.JsonTools;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.Utils;
import com.util.alipay.config.AlipayConfig;
import com.util.alipay.pay.Alipay;
import com.util.weixin.Constant;
import com.util.weixin.pay.pay.WeixinPay;
import com.util.weixin.tool.HttpTool;

/**
 * 充值支付控制器
 * @author jyt
 * @since 2016年8月4日 下午1:26:39
 */
@Controller
@RequestMapping("/recharge")
public class RechargePayController  extends BaseController<RechargePayController>{

	
	@Autowired
	private RechargeService rechargeService;
	
	@Autowired
	private RechargeCardService rechargeCardService;
	
	
	
	
	/**
	 * 
	 * 会员自助充值
	 * 请求参数集合（payType，rechargCardId,customAmount）
	 * 返回签名后的订单信息(由移动端请求)
	 * @author jyt
	 * @version 1.0.1
	 * 
	 */
	@RequestMapping("/getRechargeSignedOrder")
	public void getRechargeSignedOrder()
	{
		String result ="";
		try {
			//1.移动端请求必要参数校验
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);
			String payType = pmap.get("payType");//   2信用卡支付   3支付宝   4银联 5微信  
			String rechargeId = pmap.get("rechargeId");//充值卡ID
			String customAmountStr = pmap.get("customAmount");//充值金额，充值卡的面额
			Double customAmount  = StringHelper.isEmpty(customAmountStr)?0.00:Double.valueOf(customAmountStr);
			
			//数据校验
			if( null == s  || StringHelper.isEmpty(payType) ||StringHelper.isEmpty(rechargeId) || StringHelper.isEmpty(customAmountStr) || customAmount.doubleValue()<=0)
			{
				result = Ajax.AppJsonResult(Constants.APP_PARAM_REQUIRED_ERROR, Constants.APP_PARAM_REQUIRED_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			s.setState(4);
			
			if(!Subscriber.isOperated(s)  )// 会员不是审核通过状态，且事务状态为半锁定或全锁定
			{
				result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, "会员的资料未完成审核，或者当前登录处于锁定状态下，无法充值！");
				Utils.outJSONObject(result,response);
			}
			//2.根据请求参数生成预订单和交易
			AccountTradeRecord accountTradeRecord = rechargeService.addRechargeRecord(s.getId(), rechargeId,customAmount , Account.PAY_CHANNEL_APP,Integer.valueOf(payType) );
			
			String tradeOrderNo = accountTradeRecord.getTradeOrderNo();//交易订单号
			pmap.put("out_trade_no", tradeOrderNo);
			
			JSONObject payJSONObject =null;
			
			if(payType.equalsIgnoreCase(""+Account.PAY_TYPE_ALIPAY))//支付宝支付充值
			{
				payJSONObject = Alipay.ydalipay(tradeOrderNo,accountTradeRecord.getAmount()+"",Constants.BODY_RECHARGE,AlipayConfig.notify_url);
				payJSONObject.put("notify_url" ,ContantsTool.DOMAIN_NAME+"/recharge/rechargeByAliCallBack");
			}
			else if(payType.equalsIgnoreCase(""+Account.PAY_TYPE_WECHAT))//微信支付充值
			{
				
				payJSONObject = WeixinPay.wxpay(request.getLocalAddr(), tradeOrderNo, Constants.BODY_RECHARGE, accountTradeRecord.getAmount()+"",Constant.JR_NOTIFY_URL);
				
				payJSONObject.put("notify_url" ,ContantsTool.DOMAIN_NAME+"/recharge/rechargeByWxCallBack");
				
			}
			else if(payType.equalsIgnoreCase(""+Account.PAY_TYPE_CREDIT))//信用卡支付充值
			{
				result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "暂不支持此支付方式");
				System.err.println("payType："+payType);
				Utils.outJSONObject(result,response);
				return;
			}
			
			if(null ==payJSONObject )
			{
				result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "失败");
				Utils.outJSONObject(result,response);
				return;
			}
			else
			{
				Map<String,Object> rm = new HashMap<String, Object>();
				rm.put("alipay", payJSONObject);
				result = Ajax.AppbzJsonResult(Constants.RESULT_CODE_SUCCESS, "成功",payJSONObject);
				Utils.outJSONObject(result,response);
			}
		}catch (Exception e){
			 e.printStackTrace();
			 result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "失败");
			 Utils.outJSONObject(result,response);
		}
	}
	
	
	
	
	/**
	 * 支付宝充值
	 * 异步通知  回调方法
	 * @author jyt
	 * @version 1.0.1
	 */
	@RequestMapping("/rechargeByAliCallBack")
	public  void rechargeByAliCallBack(){
		String result = "fail";
		try {
				Map<String,Object> orderMap = Alipay.ybreturn(request);
				if(orderMap.get("status").toString().equals("ok")){
					result = "success";
					//支付成功后 更新交易状态， 交易详情状态，变更账户余额、 充值记录 ，生成充值卡实例
					PayResponseMessage payResponseMessage =new PayResponseMessage();
					payResponseMessage.setOrderId(orderMap.get("orderNo")+"");
					//payResponseMessage.setOutRefundNo(orderMap.get("out_refunde_no")+"");
					this.rechargeService.updateRechargeTrade(payResponseMessage);
				}
			PrintWriter pw = response.getWriter();
			pw.print(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 微信支付充值
	 * 异步回调
	 * @version 1.0.1
	 */
	@RequestMapping("/rechargeByWxCallBack")
	public  void rechargeByWxCallBack(){
		 Map<String,String> map = new HashMap<String,String>();
			try {
				Map<String,Object> orderMap = WeixinPay.callBackUrl(request);
				if(orderMap.get("state").toString().equals("ok")){
					map.put("return_code", orderMap.get("return_code").toString());
					map.put("return_msg", orderMap.get("return_msg").toString());
					//支付成功后 更新交易状态， 交易详情状态，变更账户余额、 充值记录 ，生成充值卡实例
					PayResponseMessage payResponseMessage =new PayResponseMessage();
					payResponseMessage.setOrderId(orderMap.get("out_trade_no")+"");
					payResponseMessage.setOutRefundNo(orderMap.get("out_refunde_no")+"");
					this.rechargeService.updateRechargeTrade(payResponseMessage);
				}
				String result = HttpTool.mapToXml(map);
				PrintWriter pw = response.getWriter();
				pw.print(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
	}
	
	/**
	 * 同步通知  支付宝充值回调方法
	 * @author hzw
	 */
	@RequestMapping("/rechargBySyncAliCallBack")
	public void tbalipayCallBack(){
		
		String result = "fail";
		try {
			Map<String,Object> orderMap = Alipay.tbreturn(request);
			if(orderMap.get("state").toString().equals("ok")){
				//支付成功后 更新交易状态， 交易详情状态，变更账户余额、 充值记录 ，生成充值卡实例
				PayResponseMessage payResponseMessage =new PayResponseMessage();
				payResponseMessage.setOrderId(orderMap.get("out_trade_no")+"");
				payResponseMessage.setOutRefundNo(orderMap.get("out_refunde_no")+"");
				rechargeService.updateRechargeTrade(payResponseMessage);
			}
			if("success".equals(result)){
				result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
				Utils.outJSONObject(result,response);
			}
			result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
			Utils.outJSONObject(result,response);
		} catch (Exception e) {
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
			Utils.outJSONObject(result,response);
		}
		
	}
	
	
}
