package com.leetu.account.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class TradeRecordNo {

	
	/**
	 * 充值  支付订单编号
	 * @return
	 */
	public static String getPayRechargeTradeNo(){
		
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMddHHmmss");
		StringBuffer buf=new StringBuffer("CZ");
		buf.append(format.format(new Date()));
		Random r=new Random(); 
		for(int i=0;i<6;i++){ 
			buf.append(r.nextInt(10));
		}; 
	
		return buf.toString();
	}
	
	/**
	 * 退款交易编号
	 * @return
	 */
	public static String getRefundTradeNo(){
		
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMddHHmmss");
		StringBuffer buf=new StringBuffer("TK");
		buf.append(format.format(new Date()));
		Random r=new Random(); 
		for(int i=0;i<6;i++){ 
			buf.append(r.nextInt(10));
		}; 
	
		return buf.toString();
	}
	
	
	/**
	 * 扣款 支付订单编号
	 * @return
	 */
	public static String getCutPaymentNo(){
		
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMddHHmmss");
		StringBuffer buf=new StringBuffer("KK");
		buf.append(format.format(new Date()));
		Random r=new Random(); 
		for(int i=0;i<6;i++){ 
			buf.append(r.nextInt(10));
		}; 
	
		return buf.toString();
	}
	

	/**
	 * 订单消费 支付订单编号
	 * @return
	 */
	public static String getPayOrderTradeNo(){
		
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMddHHmmss");
		StringBuffer buf=new StringBuffer("DD");
		buf.append(format.format(new Date()));
		Random r=new Random(); 
		for(int i=0;i<6;i++){ 
			buf.append(r.nextInt(10));
		}; 
	
		return buf.toString();
	}
	
	/**
	 * 订单消费 支付订单编号
	 * @return
	 */
	public static String getMainRefundTradeNo(){
		
		SimpleDateFormat format=new SimpleDateFormat("yyyyMMddHHmmss");
		StringBuffer buf=new StringBuffer("TK");
		buf.append(format.format(new Date()));
		Random r=new Random(); 
		for(int i=0;i<3;i++){ 
			buf.append(r.nextInt(10));
		}; 
	
		return buf.toString();
	}
	
}
