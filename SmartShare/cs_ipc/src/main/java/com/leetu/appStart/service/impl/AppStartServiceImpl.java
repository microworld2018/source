package com.leetu.appStart.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.appStart.mapper.AppStartMapper;
import com.leetu.appStart.service.AppStartService;
import com.util.Ajax;
import com.util.Constants;
import com.util.StringHelper;

@Service
public class AppStartServiceImpl implements AppStartService {
	
	@Autowired
	private AppStartMapper appStartMapper;

	@Override
	public String getAppStart() {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		Map<String, Object> appStart = appStartMapper.getAppStart();
		if(appStart != null){
			String title = appStart.get("title").toString();
			String startImg = appStart.get("start_img").toString();
			String content = appStart.get("content").toString();
			
//		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if(appStart.get("start_time") != null && !"".equals(appStart.get("start_time"))){
				String startTimeStr = appStart.get("start_time").toString().substring(0, 19);
				res.put("startTime", startTimeStr);
			}
			if(appStart.get("end_time") != null && !"".equals(appStart.get("end_time"))){
				String endTimeStr = appStart.get("end_time").toString().substring(0, 19);
				res.put("endTime", endTimeStr);
			}
//		if(StringHelper.isNotEmpty(startTimeStr)){
//			try {
//				Date startTime = sdf.parse(startTimeStr);
//			} catch (ParseException e) {
//				e.printStackTrace();
//				return Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
//			}
//		}
//		if(StringHelper.isNotEmpty(endTimeStr)){
//			try {
//				Date endTime = sdf.parse(endTimeStr);
//				res.put("endTime", endTime);
//			} catch (ParseException e) {
//				e.printStackTrace();
//				return Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
//			}
//		}
			res.put("title", title);
			res.put("imgUrl", startImg);
			res.put("contentUrl", content);
			
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		}else{
			result = Ajax.AppResult(Constants.APP_NULL_APPSTART, Constants.APP_NULL_APPSTART_MSG,res);
		}
		return result;
	}

}
