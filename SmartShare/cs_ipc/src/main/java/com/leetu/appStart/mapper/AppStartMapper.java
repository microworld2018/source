package com.leetu.appStart.mapper;

import java.util.Map;

public interface AppStartMapper {

	Map<String, Object> getAppStart();

}
