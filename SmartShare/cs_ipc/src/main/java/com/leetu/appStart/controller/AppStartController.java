package com.leetu.appStart.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.appStart.service.AppStartService;
import com.util.Ajax;
import com.util.Constants;
import com.util.Utils;

@Controller
@RequestMapping("/appStart")
public class AppStartController extends BaseController<AppStartController> {

	public static final Log logger = LogFactory.getLog(AppStartController.class);
	
	@Autowired
	private AppStartService appStartService;

	/**
	 * 获取开屏信息接口 version 1.0
	 */
	@RequestMapping("/getAppStart")
	public void getAppStart() {
		String result = null;
		try {
			result = appStartService.getAppStart();
		} catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG + ":" + e.getMessage());
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}

		Utils.outJSONObject(result, response);
	}
}
