package com.leetu.insurance.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.insurance.mapper.InsuranceMapper;
import com.leetu.insurance.service.InsuranceService;

@Service
@Transactional
public class InsuranceServiceImpl implements InsuranceService{

	@Autowired
	private InsuranceMapper insuranceMapper;
	
	/**
	 * 根据车辆类型查看保险信息
	 * @param carTypeId
	 * @return
	 */
	@Override
	public List<Map<String, Object>> getCarInsurance(String carId) {
		return insuranceMapper.getCarInsurance(carId);
	}

	/**
	 * 根据保险规则ID获取保险基础内容详情
	 * @param insuranceId
	 * @return
	 */
	@Override
	public List<Map<String, Object>> getInsuranceDesc(String insuranceId) {
		return insuranceMapper.getInsuranceDesc(insuranceId);
	}
	
	/**
	 * 根据保险规则ID获取保险规则信息
	 * @param insuranceId
	 * @return
	 */
	public Map<String, Object> getInsuranceById(String insuranceId,String carId){
		return insuranceMapper.getInsuranceById(insuranceId, carId);
	}

}
