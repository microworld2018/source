package com.leetu.insurance.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface InsuranceMapper {

	/**
	 * 根据车辆类型查看保险信息
	 * @param carTypeId
	 * @return
	 */
	List<Map<String, Object>> getCarInsurance(@Param("carId")String carId);
	
	/**
	 * 根据保险规则ID获取保险基础内容详情
	 * @param insuranceId
	 * @return
	 */
	List<Map<String, Object>> getInsuranceDesc(@Param("insuranceId")String insuranceId);
	
	/**
	 * 根据保险规则ID获取保险规则信息
	 * @param insuranceId
	 * @return
	 */
	Map<String, Object> getInsuranceById(@Param("insuranceId")String insuranceId,@Param("carId")String carId);
}
