package com.leetu.sys.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.sys.entity.SysOperateLogRecord;
import com.leetu.sys.mapper.SysOperateLogRecordMapper;
import com.leetu.sys.service.SysOperateLogService;
@Service
@Transactional
public class SysOperateLogServiceImpl implements SysOperateLogService{

	@Autowired
	private SysOperateLogRecordMapper sysOperateLog;
	
	@Override
	public void addSysOperateLog(Map<String, Object> map) {
		
		sysOperateLog.addSysOperateLog(map);
	}


}
