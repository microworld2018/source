package com.leetu.sys.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.sys.entity.RunLog;
import com.leetu.sys.mapper.RunLogMapper;
import com.leetu.sys.service.RunLogService;

@Service
@Transactional
public class RunLogServiceImpl implements RunLogService{
	
	@Autowired
	private RunLogMapper runLogMapper;
	
	@Override
	public void addRunLog(Map<String, Object> paramMap) {
		runLogMapper.addRunLog(paramMap);
	}
	
	/**
	 * app登录操作日志
	 * @param operatorName
	 * @param operatorContent
	 */
	public void addRunLogAppLogin(String operatorName,String operatorContent,
			String operatorIp){
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("operatorName", operatorName);
		
		paramMap.put("operatorIp", operatorIp);
		
		paramMap.put("operatorContent", operatorContent);
		
		paramMap.put("operatorType", RunLog.APP_LOGIN);
		
		paramMap.put("createDate", new Date());
		
		addRunLog(paramMap);
	}
	
	/**
	 * app用户预约车辆操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	public void addRunAppAdvance(String operatorName,String operatorIp,
			String licensePlateNumber,String operatorContent){
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("operatorName", operatorName);
		
		paramMap.put("operatorIp", operatorIp);
		
		paramMap.put("licensePlateNumber", licensePlateNumber);
		
		paramMap.put("operatorContent", operatorContent);
		
		paramMap.put("operatorType", RunLog.APP_ADVANCE);
		
		paramMap.put("createDate", new Date());
		
		addRunLog(paramMap);
	}
	
	/**
	 * app用户开始用车操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	public void addRunAppStartCar(String ordersNo,String operatorName,
			String operatorIp,String licensePlateNumber,String operatorContent){

		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("ordersNo", ordersNo);
		
		paramMap.put("operatorName", operatorName);
		
		paramMap.put("operatorIp", operatorIp);
		
		paramMap.put("licensePlateNumber", licensePlateNumber);
		
		paramMap.put("operatorContent", operatorContent);
		
		paramMap.put("operatorType", RunLog.APP_START_CAR);
		
		paramMap.put("createDate", new Date());
		
		addRunLog(paramMap);
	}

	
	/**
	 * app用户开车门操作日志
	 * @param ordersNo
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	@Override
	public void addRunAppOpenDoor(String ordersNo, String operatorName,
			String operatorIp, String licensePlateNumber, String operatorContent) {
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("ordersNo", ordersNo);
		
		paramMap.put("operatorName", operatorName);
		
		paramMap.put("operatorIp", operatorIp);
		
		paramMap.put("licensePlateNumber", licensePlateNumber);
		
		paramMap.put("operatorContent", operatorContent);
		
		paramMap.put("operatorType", RunLog.APP_OPEN_DOOR);
		
		paramMap.put("createDate", new Date());
		
		addRunLog(paramMap);
	}
	
	/**
	 * app用户关车门操作日志
	 * @param ordersNo
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	@Override
	public void addRunAppCloseDoor(String ordersNo, String operatorName,
			String operatorIp, String licensePlateNumber, String operatorContent) {
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("ordersNo", ordersNo);
		
		paramMap.put("operatorName", operatorName);
		
		paramMap.put("operatorIp", operatorIp);
		
		paramMap.put("licensePlateNumber", licensePlateNumber);
		
		paramMap.put("operatorContent", operatorContent);
		
		paramMap.put("operatorType", RunLog.APP_CLOSE_DOOR);
		
		paramMap.put("createDate", new Date());
		
		addRunLog(paramMap);
	}
	
	/**
	 * app用户远程寻车操作日志
	 * @param ordersNo
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	@Override
	public void addRunAppFindCar(String ordersNo, String operatorName,
			String operatorIp, String licensePlateNumber, String operatorContent) {
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("ordersNo", ordersNo);
		
		paramMap.put("operatorName", operatorName);
		
		paramMap.put("operatorIp", operatorIp);
		
		paramMap.put("licensePlateNumber", licensePlateNumber);
		
		paramMap.put("operatorContent", operatorContent);
		
		paramMap.put("operatorType", RunLog.APP_FIND_CAR);
		
		paramMap.put("createDate", new Date());
		
		addRunLog(paramMap);
	}
	
	/**
	 * app用户远程寻车操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	public void addRunAppBackCar(String ordersNo,String operatorName,
			String operatorIp,String licensePlateNumber,String operatorContent){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		paramMap.put("ordersNo", ordersNo);
		
		paramMap.put("operatorName", operatorName);
		
		paramMap.put("operatorIp", operatorIp);
		
		paramMap.put("licensePlateNumber", licensePlateNumber);
		
		paramMap.put("operatorContent", operatorContent);
		
		paramMap.put("operatorType", RunLog.APP_BACK_CAR);
		
		paramMap.put("createDate", new Date());
		
		addRunLog(paramMap);
	}

}
