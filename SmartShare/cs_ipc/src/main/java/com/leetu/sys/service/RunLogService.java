package com.leetu.sys.service;

import java.util.Map;

public interface RunLogService {

	/**
	 * 运行日志操作添加
	 * @param paramMap
	 */
	void addRunLog(Map<String, Object> paramMap);
	
	/**
	 * app登录操作日志
	 * @param operatorName
	 * @param operatorContent
	 * @param operatorIp
	 */
	void addRunLogAppLogin(String operatorName,String operatorContent,String operatorIp);
	
	/**
	 * app用户预约车辆操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	void addRunAppAdvance(String operatorName,String operatorIp,String licensePlateNumber,String operatorContent);
	
	/**
	 * app用户开始用车操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	void addRunAppStartCar(String ordersNo,String operatorName,String operatorIp,String licensePlateNumber,String operatorContent);
	
	/**
	 * app用户开车门操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	void addRunAppOpenDoor(String ordersNo,String operatorName,String operatorIp,String licensePlateNumber,String operatorContent);
	
	/**
	 * app用户关车门操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	void addRunAppCloseDoor(String ordersNo,String operatorName,String operatorIp,String licensePlateNumber,String operatorContent);
	
	/**
	 * app用户远程寻车操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	void addRunAppFindCar(String ordersNo,String operatorName,String operatorIp,String licensePlateNumber,String operatorContent);
	
	/**
	 * app用户远程寻车操作日志
	 * @param operatorName
	 * @param operatorIp
	 * @param licensePlateNumber
	 * @param operatorContent
	 */
	void addRunAppBackCar(String ordersNo,String operatorName,String operatorIp,String licensePlateNumber,String operatorContent);
}
