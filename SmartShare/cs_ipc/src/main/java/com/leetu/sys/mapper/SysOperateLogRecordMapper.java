package com.leetu.sys.mapper;

import java.util.Map;

public interface SysOperateLogRecordMapper {

	/**
	 * 操作日志添加
	 * @param log
	 */
	void addSysOperateLog(Map<String, Object> map);
}
