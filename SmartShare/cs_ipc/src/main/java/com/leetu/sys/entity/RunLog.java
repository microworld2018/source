package com.leetu.sys.entity;

import java.io.Serializable;
import java.util.Date;
/**
 * 运行操作日志
 * @author GodZilla
 *
 */
public class RunLog implements Serializable{

	private static final long serialVersionUID = -2760805488884643008L;
	
	public static final String APP_LOGIN= "0";//app登录
	public static final String APP_LOGIN_MSG = "APP-用户登录";
	
	public static final String APP_ADVANCE = "1";//app预约车辆
	public static final String APP_ADVANCE_MSG = "APP-用户预约车辆";
	
	public static final String APP_START_CAR = "2";//app开始用车
	public static final String APP_UPDATE_USER_INFO = "3";//app修改个人信息
	
	public static final String APP_OPEN_DOOR = "4";//app开车门
	public static final String APP_OPEN_DOOR_MSG = "APP-用户开车门";
	
	
	public static final String APP_CLOSE_DOOR = "5";//app关车门
	public static final String APP_CLOSE_DOOR_MSG = "APP-用户关车门";
	
	public static final String APP_FIND_CAR = "6";//app远程寻车
	public static final String APP_FIND_CAR_MSG = "APP-用户远程寻车";
	
	public static final String APP_BACK_CAR = "7";//app确认还车
	public static final String APP_BACK_CAR_MSG = "APP-确认还车";
	
	
	private String id;//id
	
	private String ordersNo;//订单编号
	
	private String operatorName;//操作人
	
	private String operatorIp;//操作IP
	
	private String licensePlateNumber;//车牌号
	
	private String operatorContent;//操作内容
	
	private String operatorType;//操作类型
	
	private Date createDate;//操作时间

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrdersNo() {
		return ordersNo;
	}

	public void setOrdersNo(String ordersNo) {
		this.ordersNo = ordersNo;
	}

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getOperatorIp() {
		return operatorIp;
	}

	public void setOperatorIp(String operatorIp) {
		this.operatorIp = operatorIp;
	}

	public String getLicensePlateNumber() {
		return licensePlateNumber;
	}

	public void setLicensePlateNumber(String licensePlateNumber) {
		this.licensePlateNumber = licensePlateNumber;
	}

	public String getOperatorContent() {
		return operatorContent;
	}

	public void setOperatorContent(String operatorContent) {
		this.operatorContent = operatorContent;
	}

	public String getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(String operatorType) {
		this.operatorType = operatorType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
