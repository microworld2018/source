package com.leetu.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.sys.entity.Dict;
import com.leetu.sys.mapper.DictMapper;
import com.leetu.sys.service.DictService;

@Service
public class DictServiceImpl implements DictService{

	@Autowired
	private DictMapper dictMapper;

	@Override
	public Dict getDictByCodes(String bizstatus, String code) {
		return dictMapper.getDictByCodes(bizstatus,code);
	}

	@Override
	public Dict getDictById(String id) {
		// TODO Auto-generated method stub
		return dictMapper.getDictById(id);
	}

	@Override
	public List<Dict> getDictByDictGroupCode(String groupCode) {
		// TODO Auto-generated method stub
		return dictMapper.getDictByGroupCode(groupCode);
	}
	
	
	

	
}
