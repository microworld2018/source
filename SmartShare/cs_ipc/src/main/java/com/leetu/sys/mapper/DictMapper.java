package com.leetu.sys.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.leetu.sys.entity.Dict;

public interface DictMapper {

	/**
	 * 查询状态码
	 */
	Dict getDictByCodes(@Param("bizstatus")String bizstatus, @Param("code")String code);

	/**
	 * 根据Id查询字典数据
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	Dict getDictById(@Param("id")String id);

	/**
	 * 根据字典组Code查询字典列表
	 * @param groupCode
	 * @return
	 * @version 1.0.1
	 */
	List<Dict> getDictByGroupCode(String groupCode);

}
