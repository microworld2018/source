package com.leetu.sys.mapper;

import java.util.Map;

public interface RunLogMapper {

	/**
	 * 运行日志操作添加
	 * @param paramMap
	 */
	void addRunLog(Map<String, Object> paramMap);
}
