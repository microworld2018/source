package com.leetu.sys.entity;

import java.io.Serializable;
import java.util.Date;

public class Group implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String groupId;
	private String groupName;
	private String groupRemark;
	private String groupCreatorId;
	private Date groupCreateTime;
	private Date ts;
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getGroupRemark() {
		return groupRemark;
	}
	public void setGroupRemark(String groupRemark) {
		this.groupRemark = groupRemark;
	}
	public String getGroupCreatorId() {
		return groupCreatorId;
	}
	public void setGroupCreatorId(String groupCreatorId) {
		this.groupCreatorId = groupCreatorId;
	}
	public Date getGroupCreateTime() {
		return groupCreateTime;
	}
	public void setGroupCreateTime(Date groupCreateTime) {
		this.groupCreateTime = groupCreateTime;
	}
	public Date getTs() {
		return ts;
	}
	public void setTs(Date ts) {
		this.ts = ts;
	}

}
