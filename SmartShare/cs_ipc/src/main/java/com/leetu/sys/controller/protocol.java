package com.leetu.sys.controller;

import java.util.Hashtable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.core.controller.BaseController;

@Controller
@RequestMapping("protocol")
public class protocol extends BaseController<protocol> {

	@RequestMapping("/getprotocol")
	public ModelAndView getprotocol(HttpServletRequest request) {

		ModelAndView modelView = new ModelAndView("share/share");

		return modelView;
	}
}
