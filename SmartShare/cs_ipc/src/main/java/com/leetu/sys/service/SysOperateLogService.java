package com.leetu.sys.service;

import java.util.Map;


public interface SysOperateLogService {

	/**
	 * 日志添加
	 * @param log
	 */
	void addSysOperateLog(Map<String, Object> map);
}
