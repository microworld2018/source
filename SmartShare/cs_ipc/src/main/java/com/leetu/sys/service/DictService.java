package com.leetu.sys.service;

import java.util.List;

import com.leetu.sys.entity.Dict;

public interface DictService {

	/**
	 * 查询车辆状态码
	 */
	Dict getDictByCodes(String bizstatus, String code);
	
	
	List<Dict> getDictByDictGroupCode(String groupCode);
	
	/**
	 * 字典数据ID ,查询字典记录
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	Dict getDictById(String id);


}
