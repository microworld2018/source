package com.leetu.place.service;

import java.util.List;
import java.util.Map;
import com.leetu.place.entity.BranchDot;
import com.leetu.place.entity.HotDot;

public interface BranchDotService {

	/**
	 * 获取一定距离内的网点
	 * @param map
	 * @return
	 */
	List<BranchDot> branchDots(Map map);

	/**
	 * 获取网点下的车辆
	 * @param id
	 * @param bizstatus 
	 * @return
	 */
	Integer getCarCount(String id, String bizstatus);
	
	
	/**
	 * 插入建议网点坐标信息
	 * @param hotDot
	 * @return
	 */
	Integer addHotDot(HotDot hotDot);

	/**
	 * 根据ids车辆网点
	 * @param ids
	 * @return
	 */
	List<BranchDot> getDotByIds(String[] ids);

	/**
	 * 获取所有车辆网点
	 * @return
	 */
	List<BranchDot> getAllDots();

	/**
	 * 根据车辆网点获取网点信息
	 * @param dotId
	 * @return
	 */
	BranchDot getDotById(String dotId);

	/**
	 * 获取车辆信息
	 * @param carId
	 * @param id
	 * @return
	 */
	Map<String, Object> carinfo(String carId, String id);

	/**
	 * 获取车辆电量和可持续里航
	 * @param carId
	 * @param i
	 * @return
	 */
	Map<String, Object> carRealinfo(String carId, int i);

	/**
	 * 获取网点信息
	 * @param dotId
	 * @return
	 */
	Map<String, Object> dotInfo(String dotId);

	/**
	 * 修改车辆状态
	 * @param carId
	 * @param id
	 * @param version 
	 * @return 
	 */
	int updateCarStatus(String carId, String id, String version);

	/**
	 * 更新网点信息
	 * @param carId
	 * @param dotId
	 * @param i
	 */
	void updateDotCar(String carId, String dotId, int isused);

	/**
	 * 添加网点车辆信息
	 * @param carId
	 * @param dotId
	 * @param isused
	 * @param id
	 */
	void addDotCar(Map<String, String> pmap);

	/**
	 * 更新车辆楼层 车位号
	 * @param pmap
	 */
	void updateDotcar(Map<String, String> pmap);

	/**
	 * 查询网点为租借车辆的信息
	 * @param dotId
	 * @return
	 */
	List<Map<String, Object>> dotcars(String dotId);
	
	/**
	 * 确认还车接口
	 * @param data
	 * @return
	 */
	String backCar(String data,String ip);
}
