package com.leetu.place.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.place.entity.BranchDot;
import com.leetu.place.entity.HotDot;

public interface BranchDotMapper {

	/**
	 * 获取一定距离内的网点
	 * @param map
	 * @return
	 */
	List<BranchDot> branchDots(Map map);

	/**
	 * 获取网点下的车辆
	 * @param id
	 * @param bizstatus 
	 * @return
	 */
	Integer getCarCount(@Param("id")String id, @Param("bizstatus")String bizstatus);

	/**
	 * 插入建议网点坐标信息
	 * @param hotDot
	 * @return
	 */
	Integer insertHotDot(HotDot hotDot);
	
	/**
	 * 根据ids查询网点信息
	 * @param id
	 * @return
	 */
	List<BranchDot> getDotByIds(@Param("array")String[] array);

	/**
	 * 获取所有车辆网点
	 * @return
	 */
	List<BranchDot> getAllDots();

	/**
	 * 获取车辆信息
	 * @param carId
	 * @param t
	 * @return
	 */
	Map<String, Object> carinfo(@Param("carId")String carId, @Param("t")String t);

	/**
	 * 获取网点信息
	 * @param dotId
	 * @return
	 */
	Map<String, Object> dotInfo(@Param("dotId")String dotId);

	/**
	 * 更改车辆状态
	 * @param carId
	 * @param ti 车辆预定中状态
	 * @param version 
	 * @return 
	 */
	int updateCarStatus(@Param("carId")String carId, @Param("ti")String ti, @Param("version")String version);

	/**
	 * 获取车辆电量和可持续里航
	 * @param carId
	 * @param t 运营业务状态    1租赁使用中    2未租赁   3预定中   4下线'
	 * @return
	 */
	Map<String, Object> carRealinfo(@Param("carId")String carId, @Param("t")int t);

	/**
	 * 根据Id获取车辆网点信息
	 * @param dotId
	 * @return
	 */
	BranchDot getDotById(String dotId);

	/**
	 * 更新网点信息
	 * @param carId
	 * @param dotId
	 * @param isused
	 */
	void updateDotCar(@Param("carId")String carId, @Param("dotId")String dotId, @Param("isused")int isused);

	/**
	 * 添加网点车辆信息
	 * @param carId
	 * @param dotId
	 * @param isused
	 * @param id 用户id
	 */
	void addDotCar(Map<String, String> pmap);

	/**
	 * 更新车辆楼层 车位号
	 * @param map
	 */
	void updotcar(Map<String, String> map);

	/**
	 * 查询网点为租借车辆的信息
	 * @param dotId
	 * @param state
	 * @return
	 */
	List<Map<String, Object>> dotcars(@Param("dotId")String dotId,@Param("state") String state);

	/**
	 * 删除上上个月数据
	 * @param isused
	 * @param time
	 */
	void deleteDotCar(@Param("isused")Integer isused,@Param("time") String time);

}
