package com.leetu.place.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.ground.notice.entity.GroundNotice;
import com.ground.notice.entity.SubNotice;
import com.ground.notice.service.SubNoticeService;
import com.ground.site.entity.SubDot;
import com.ground.site.service.SubDotService;
import com.leetu.car.service.CarService;
import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.service.CouponService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.service.PayOrderService;
import com.leetu.orders.util.OrderFee;
import com.leetu.orders.util.OrderRedisData;
import com.leetu.place.entity.BranchDot;
import com.leetu.place.entity.HotDot;
import com.leetu.place.mapper.BranchDotMapper;
import com.leetu.place.param.BranchDotParam;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.mapper.DictMapper;
import com.leetu.sys.mapper.SysOperateLogRecordMapper;
import com.leetu.sys.service.DictService;
import com.leetu.sys.service.RunLogService;
import com.leetu.sys.service.SysOperateLogService;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.ChangeCharset;
import com.util.Constants;
import com.util.DateUtil;
import com.util.HttpRequestUtil;
import com.util.JsonTools;
import com.util.MapDistance;
import com.util.PropertiesUtil;
import com.util.TokenUtils;
import com.util.ToolDateTime;
import com.util.car.CurrentCarInfo;
import com.util.charging.Constant.CouponType;
import com.util.charging.Constant.UseCarMode;
import com.util.charging.calculate.FeesAlg;
import com.util.msg.MsgContent;
import com.util.msg.sendMSg;

@Service
@Transactional
public class BranchDotServiceImpl implements BranchDotService {

	@Autowired
	private BranchDotMapper branchDotMapper;
	@Autowired
	private DictMapper dictMapper;
	@Autowired
	private DictService dictService;
	@Autowired
	private OrderService orderService;
	@Autowired
	private OrdersDetailService ordersDetailService;
	@Autowired
	private CarService carService;
	@Autowired
	private DeviceBindingService deviceBindingService;
	@Autowired
	private PayOrderService payorderService;
	@Autowired
	private CouponService couponService;
	@Autowired
	private SubScriberService subScriberService;
	@Autowired
	private SysOperateLogService sysOperateLogService;

	@Autowired
	private RunLogService runLogService;

	@Autowired
	private SubNoticeService subNoticeService;

	@Autowired
	private SubDotService subDotService;

	@Override
	public List<BranchDot> branchDots(Map map) {
		return branchDotMapper.branchDots(map);
	}

	@Override
	public Integer getCarCount(String id, String bizstatus) {
		return branchDotMapper.getCarCount(id, bizstatus);
	}

	@Override
	public Integer addHotDot(HotDot hotDot) {

		return branchDotMapper.insertHotDot(hotDot);

	}

	@Override
	public List<BranchDot> getDotByIds(String[] ids) {
		return branchDotMapper.getDotByIds(ids);
	}

	@Override
	public List<BranchDot> getAllDots() {
		return branchDotMapper.getAllDots();
	}

	@Override
	public BranchDot getDotById(String dotId) {
		return branchDotMapper.getDotById(dotId);
	}

	@Override
	public Map<String, Object> carinfo(String carId, String t) {
		return branchDotMapper.carinfo(carId, t);
	}

	@Override
	public Map<String, Object> carRealinfo(String carId, int t) {
		return branchDotMapper.carRealinfo(carId, t);
	}

	@Override
	public Map<String, Object> dotInfo(String dotId) {
		return branchDotMapper.dotInfo(dotId);
	}

	@Override
	public int updateCarStatus(String carId, String id, String version) {
		return branchDotMapper.updateCarStatus(carId, id, version);
	}

	@Override
	public void updateDotCar(String carId, String dotId, int isused) {
		branchDotMapper.updateDotCar(carId, dotId, isused);
	}

	@Override
	public void addDotCar(Map<String, String> pmap) {
		branchDotMapper.addDotCar(pmap);
	}

	@Override
	public void updateDotcar(Map<String, String> map) {
		branchDotMapper.updotcar(map);
	}

	@Override
	public List<Map<String, Object>> dotcars(String dotId) {
		Dict d = dictMapper.getDictByCodes("carBizState", "1");
		String state = d.getId();
		return branchDotMapper.dotcars(dotId, state);
	}

	@Override
	public synchronized String backCar(String data, String ip) {
		// String result = null;
		// // 获取总里程
		// String mileage = "0";// 默认0
		// String carId = "";
		// SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd
		// HH:mm:ss");//抽离新增
		// try {
		// Map<String, String> pmap = JsonTools.desjsonForMap(data);
		// if (pmap.get("param").equals("error")) {
		// result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR,
		// Constants.APP_PARAM_ERROR_MSG);
		// Map<String, Object> logMapError = new HashMap<String, Object>();
		// logMapError.put("operatorName", "用户");
		// logMapError.put("operateDate", new Date());
		// logMapError.put("operateRemark", "param是error");
		// logMapError.put("operateContent", result);
		// logMapError.put("modelName", "车辆还车");
		// sysOperateLogService.addSysOperateLog(logMapError);
		// return result;
		// }
		// // 请求参数校验
		// String dotId = pmap.get("dotId");
		// carId = pmap.get("carId");
		//
		// Map<String, Object> carMap = carService.carinfo(carId);
		//
		// // 根据车辆ID获取车辆设备绑定，车机编号SN
		// Map<String, String> deviceBindingMap =
		// deviceBindingService.getDeviceByCarId(carId);
		//
		// if (deviceBindingMap == null ||
		// StringUtils.isEmpty(deviceBindingMap)) {
		// Map<String, Object> logMapError = new HashMap<String, Object>();
		// logMapError.put("operatorName", "用户");
		// logMapError.put("operateDate", new Date());
		// logMapError.put("operateRemark", "未绑定设备");
		// logMapError.put("operateContent", "deviceBindingMap为null");
		// logMapError.put("modelName", "车辆还车");
		// sysOperateLogService.addSysOperateLog(logMapError);
		// return Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE,
		// Constants.APP_NO_BINDING_DEVICE_MSG);
		// }
		//
		// Map<String, String> paramMaps = new HashMap<String, String>();
		// paramMaps.put("sn", deviceBindingMap.get("device_no"));// 设备编号
		//
		// if (dotId == null) {
		// result = Ajax.AppJsonResult(Constants.APP_DOTID_NULL,
		// Constants.APP_DOTID_NULL_MSG);
		// Map<String, Object> logMapError = new HashMap<String, Object>();
		// logMapError.put("operatorName", "用户");
		// logMapError.put("operateDate", new Date());
		// logMapError.put("operateRemark", "dotId为null");
		// logMapError.put("operateContent", result);
		// logMapError.put("modelName", "车辆还车");
		// sysOperateLogService.addSysOperateLog(logMapError);
		// return result;
		// }
		// if (carId == null) {
		// result = Ajax.AppJsonResult(Constants.APP_CARID_NULL,
		// Constants.APP_CARID_NULL_MSG);
		// Map<String, Object> logMapError = new HashMap<String, Object>();
		// logMapError.put("operatorName", "用户");
		// logMapError.put("operateDate", new Date());
		// logMapError.put("operateRemark", "carId为null");
		// logMapError.put("operateContent", result);
		// logMapError.put("modelName", "车辆还车");
		// sysOperateLogService.addSysOperateLog(logMapError);
		// return result;
		// }
		//
		// // 获取当前用户信息
		// Subscriber s = TokenUtils.getSubscriber(data);
		// String id = s.getId();
		//
		// Subscriber sub = subScriberService.querySubscriberById(id);
		//
		// // 查询用户是否有租车中的订单
		// Orders ord = orderService.userOrderInfo(id,
		// Orders.STATE_ORDER_START);
		// if (ord == null) {
		// result = Ajax.AppJsonResult(Constants.APP_BACKCAR_STATE_NO,
		// "当前用户没有租车中的订单");
		// Map<String, Object> logMapError = new HashMap<String, Object>();
		// logMapError.put("operatorName", "用户");
		// logMapError.put("operateDate", new Date());
		// logMapError.put("operateRemark", "ord为null");
		// logMapError.put("operateContent", result);
		// logMapError.put("modelName", "当前用户没有租车中的订单");
		// sysOperateLogService.addSysOperateLog(logMapError);
		// return result;
		// }
		//
		// // 实际还车地点
		// // String backdotId = ord.getEndSiteId();
		// Map<String, Object> dotInfo = dotInfo(dotId);
		// String dotLat = dotInfo.get("lat").toString();
		// String dotLng = dotInfo.get("lng").toString();
		//
		// // 根据车辆ID获取车辆信息
		// Map<String, String> carInfo = carService.getCarDotByCarId(carId);
		// if (carInfo == null) {
		// Map<String, Object> logMapError = new HashMap<String, Object>();
		// logMapError.put("operatorName", "用户");
		// logMapError.put("operateDate", new Date());
		// logMapError.put("operateRemark", "车辆信息为null");
		// logMapError.put("operateContent", result);
		// logMapError.put("modelName", "车辆信息为null");
		// sysOperateLogService.addSysOperateLog(logMapError);
		// }
		//
		// // 判断车辆当前位置与还车网之间的距离是否在误差范围内
		// if (CurrentCarInfo.isDoatCar(deviceBindingMap.get("device_no"),
		// dotLat, dotLng)) {
		// // 更新取车网点信息
		// // updateDotCar(carId, dotId, 0);
		// // 更新还车网点信息
		// pmap.put("carId", carId);
		// pmap.put("dotId", dotId);
		// pmap.put("isused", "1");
		// updateDotcar(pmap);
		// // 获取订单详情
		// OrdersDetail orderDetail =
		// ordersDetailService.getByOrderIdDetail(ord.getId());
		// if (orderDetail == null) {
		// Map<String, Object> logMapError = new HashMap<String, Object>();
		// logMapError.put("operatorName", "用户");
		// logMapError.put("operateDate", new Date());
		// logMapError.put("operateRemark", "订单详情为null");
		// logMapError.put("operateContent", result);
		// logMapError.put("modelName", "订单详情为null");
		// sysOperateLogService.addSysOperateLog(logMapError);
		// }
		//
		// // 订单开始时间
		// String beginTime = ToolDateTime.format(orderDetail.getBeginTime(),
		// ToolDateTime.pattern_ymd_hms);
		// String endTime = ToolDateTime.format(new Date(),
		// ToolDateTime.pattern_ymd_hms);
		// Map<String, String> paramMap = new HashMap<String, String>();
		// String vehiclePlateId = carMap.get("vehiclePlateId").toString();
		// paramMap.put("sn", vehiclePlateId);
		// paramMap.put("beginTime", beginTime);
		// paramMap.put("endTime", endTime);
		// Map<String, Object> logMapError = new HashMap<String, Object>();
		// logMapError.put("operatorName", "用户");
		// logMapError.put("operateDate", new Date());
		// logMapError.put("operateRemark", "获取里程_请求参数_还车");
		// logMapError.put("operateContent",
		// "车牌号:"+vehiclePlateId+";开始时间:"+beginTime+";结束时间:"+endTime+";orederId:"+ord.getId()+";orderDetailId:"+orderDetail.getId());
		// logMapError.put("modelName", "获取里程_请求参数_还车");
		// sysOperateLogService.addSysOperateLog(logMapError);
		// mileage = BranchDotParam.getCarMileageForGPS(paramMap,
		// CurrentCarInfo.CAR_MILEAGE_FOR_GPS_URL,
		// sysOperateLogService);
		// // 订单行驶里程(抽离注释)
		// /*
		// * if (mileage != null && !"".equals(mileage) &&
		// * !"0".equals(mileage)) { // 计算里程、精确到小数点后两位 mileage =
		// * String.valueOf(ArithUtil.div(Double.valueOf(mileage), 1000,
		// * 2)); // 总里程，不满一公里按一公里算 mileage =
		// * String.valueOf(ArithUtil.round(Double.parseDouble(mileage)));
		// * } else { logMapError = new HashMap<String, Object>();
		// * logMapError.put("operatorName", "用户");
		// * logMapError.put("operateDate", new Date());
		// * logMapError.put("operateRemark", "获取里程出错");
		// * logMapError.put("operateContent", result);
		// * logMapError.put("modelName", "获取里程出错");
		// * sysOperateLogService.addSysOperateLog(logMapError); }
		// */
		//
		// System.out.println("还车里程距离=====" + mileage);
		//
		// ordersDetailService.updateByOrdersIdDetil(ord.getId(), mileage,
		// endTime); // 更新订单详情添加结束时间
		//
		// // 获取优惠卷
		// Coupon coupon = couponService.getCouonByOrderId(ord.getId());
		// // 获取订单费用信息
		//// Map<String, Object> orderfee = OrderFee.orderFeeInfo(ord,
		// dictService, orderService,
		//// ordersDetailService, coupon);
		//
		//// double actualFee =
		// Double.valueOf(orderfee.get("actualFee").toString());// 实际消费总金额
		// double payPrice = 0.00;// 第三方实际支付金额
		// double userPrice = 0.00;// 账户支付金额
		//// double allPrice =
		// Double.valueOf(orderfee.get("allPrice").toString());// 消费总金额
		//// double allMilePrice =
		// Double.valueOf(orderfee.get("allMilePrice").toString());// 里程消费
		//// double allTimePrice =
		// Double.valueOf(orderfee.get("allTimePrice").toString());// 时长消费
		//
		// // 加保险费用actualFee、allPrice
		// String[] insuranceFees = null;// 保险名称
		// String[] insuranceNames = null;// 保险金额
		// int insuranceCosts = 0;// 保险费用,抽离新增
		//// int days = 0;// 订单用单时长（单位:天,不足一天按一天计算）
		// // 根据订单开始时间、结束时间，计算天数
		//// if (!StringUtils.isEmpty(ord.getBeginTime()) &&
		// !StringUtils.isEmpty(endTime)) {
		//// Date endTimeDate = DateUtil.parseDate(endTime, "yyyy-MM-dd
		// HH:mm:ss");
		//// days = ToolDateTime.getDateDaysForInsurance(ord.getBeginTime(),
		// endTimeDate);
		//// }
		// if (!StringUtils.isEmpty(orderDetail.getInsuranceName())) {
		// // [基础保险, 附加保险]
		// insuranceNames = orderDetail.getInsuranceName().split("/");
		//
		// if (!StringUtils.isEmpty(orderDetail.getInsuranceFee())) {
		// insuranceFees = orderDetail.getInsuranceFee().split("/");
		// }
		// if (insuranceFees != null) {//抽离新增
		// for (String string : insuranceFees) {
		// insuranceCosts = insuranceCosts + (int) (Double.valueOf(string) *
		// 100);
		// }
		// }
		// /*
		// * for(int i =0;i<insuranceNames.length;i++){//抽离注释
		// if(insuranceFees != null && insuranceFees.length > 0){
		// //判断订单用单时长，计算保险费用、总费用
		// if(days > 0){
		// allPrice = ArithUtil.add(allPrice,
		// ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()),
		// days));
		// //actualFee = ArithUtil.add(actualFee,
		// ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()),
		// days));
		// }else{
		// allPrice = ArithUtil.add(allPrice,
		// Double.valueOf(insuranceFees[i].toString()));
		// //actualFee = ArithUtil.add(actualFee,
		// Double.valueOf(insuranceFees[i].toString()));
		// }
		// }
		// }
		// */
		// }
		String result = null;
		// 获取总里程
		String mileage = "0";// 默认0
		String carId = "";
		double payPrice = 0.00;// 第三方实际支付金额
		double userPrice = 0.00;// 账户支付金额
		String[] insuranceFees = null;// 保险名称
		String[] insuranceNames = null;// 保险金额
		int insuranceCosts = 0;// 保险费用,抽离新增
		SimpleDateFormat formatter = new SimpleDateFormat("yy-MM-dd HH:mm:ss");// 抽离新增
		try {
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if (pmap.get("param").equals("error")) {
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			// 请求参数校验
			String dotId = pmap.get("dotId");
			carId = pmap.get("carId");

			// 根据车辆ID获取车辆设备绑定，车机编号SN
			Map<String, String> deviceBindingMap = deviceBindingService.getDeviceByCarId(carId);
			if (deviceBindingMap == null || StringUtils.isEmpty(deviceBindingMap)) {
				return Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
			}
			Map<String, String> paramMaps = new HashMap<String, String>();
			paramMaps.put("sn", deviceBindingMap.get("device_no"));// 设备编号
			if (dotId == null) {
				result = Ajax.AppJsonResult(Constants.APP_DOTID_NULL, Constants.APP_DOTID_NULL_MSG);
			}
			if (carId == null) {
				result = Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
				return result;
			}
			// 获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			/**
			 * 抽离新增
			 * @author ge jiaming
			 * @date 2017-07-29
			 * 
			 * @param ord              订单对象
			 * @param sub              用户对象
			 * @param carInfo          车辆集合
			 * @param orderDetail      订单详情对象
			 * @param beginTime        开始时间
			 * @param endTime          结束时间
			 * @param mileage          行驶里程
			 * @param carMap           车辆集合
			 * @param coupon           优惠券
			 * @param insuranceCosts   保险单价
			 * @param kmPrice          里程单价
			 * @param timePrice        时间单价
			 */
			Map<String, Object> backCarData = getData(id, carId);
			Orders ord = (Orders) backCarData.get("ord");
			if (ord == null) {
				result = Ajax.AppJsonResult(Constants.APP_BACKCAR_STATE_NO, "当前用户没有租车中的订单");
				return result;
			}
			Subscriber sub = (Subscriber) backCarData.get("sub");
			Map<String, String> carInfo = (Map<String, String>) backCarData.get("carInfo");
			OrdersDetail orderDetail = (OrdersDetail) backCarData.get("orderDetail");
			String beginTime = (String) backCarData.get("beginTime");
			String endTime = (String) backCarData.get("endTime");
			mileage = (String) backCarData.get("mileage");
			Map<String, Object> carMap = (Map<String, Object>) backCarData.get("carMap");
			Coupon coupon = (Coupon) backCarData.get("coupon");
			insuranceCosts = (int) backCarData.get("insuranceCosts");
			Double kmPrice = (Double) backCarData.get("kmPrice")*100;
			Double timePrice = (Double) backCarData.get("timePrice")*100;
			System.out.println("=ord=" + ord + ";==" + sub + ";==" + carInfo + ";==" + orderDetail + ";==" + beginTime + ";==" + endTime + 
					";==" + mileage + ";==" + carMap + ";==" + coupon + ";==" + insuranceCosts + ";==" + kmPrice + ";==" + timePrice + "==");
			// 实际还车地点
			Map<String, Object> dotInfo = dotInfo(dotId);
			String dotLat = dotInfo.get("lat").toString();
			String dotLng = dotInfo.get("lng").toString();
			// 判断车辆当前位置与还车网之间的距离是否在误差范围内
			if (CurrentCarInfo.isDoatCar(deviceBindingMap.get("device_no"), dotLat, dotLng)) {
				// 更新还车网点信息
				pmap.put("carId", carId);
				pmap.put("dotId", dotId);
				pmap.put("isused", "1");
				updateDotcar(pmap);

				/**
				 * 调用工具类,进行计费计算(抽离新增)
				 * 
				 * @author ge jiaming
				 * @date 2017-07-29
				 */
				Map<String, Integer> calculateFeesResult = FeesAlg.calculateFees(UseCarMode.NORMAL_MODE,
						formatter.parse(beginTime), formatter.parse(endTime), Double.valueOf(mileage) / 1000,
						insuranceCosts, 0, CouponType.COUPON_NORMAL, false, timePrice.intValue(), kmPrice.intValue(), 0, false);
				// 更新订单详情添加结束时间
				ordersDetailService.updateByOrdersIdDetil(ord.getId(), mileage, endTime);
				Map<String, Object> order = new Hashtable<String, Object>();
				order.put("endTime", endTime);// 结束时间
				order.put("orderId", ord.getId());// 订单ID
				order.put("end_site_id", dotId);// 还车ID
				/**
				 * 抽离新增
				 * @author ge jiaming
				 * @date 2017-07-29
				 */
				order.put("orders_duration",calculateFeesResult.get("allMinute"));// 总时间
				order.put("total_fee",ArithUtil.div(Double.valueOf(calculateFeesResult.get("totalPayment") + ""), 100, 2));// 消费总金额
				order.put("actual_fee",ArithUtil.div(Double.valueOf(calculateFeesResult.get("actualFee") + ""), 100, 2));// 实际消费总金额
				order.put("tpos_pay_fee",0.0/*ArithUtil.div(Double.valueOf(calculateFeesResult.get("cashPayment") + ""), 100, 2)*/);// 第三方支付金额(还车时不保存第三方支付金额)
				order.put("use_balance",ArithUtil.div(Double.valueOf(calculateFeesResult.get("accountPayment") + ""), 100, 2));// 账户支付金额

				if (!StringUtils.isEmpty(coupon)) {
					order.put("coupon_fee", coupon.getPrice());
				} else {
					order.put("coupon_fee", 0);
				}
				order.put("state", Orders.STATE_ORDER_END);
				order.put("channel", Orders.PAY_CHANNEL_APP);
				order.put("allMile", calculateFeesResult.get("allMile"));// 总里程
				order.put("allMilePrice",ArithUtil.div(Double.valueOf(calculateFeesResult.get("totalMilePrice") + ""), 100, 2));// 里程消费金额
				order.put("allTimePrice",ArithUtil.div(Double.valueOf(calculateFeesResult.get("totalTimePrice") + ""), 100, 2));// 时长消费金额
				order.put("payType", Orders.PAY_TYPE_ACCOUNT);
				order.put("is_paid", 1);
				order.put("is_over", 1);
				order.put("is_running", 1);
				// 更新订单信息
				payorderService.updateOrderInfo(order);
				// 抽离完成处(2017-07-31)

				// 短信发送内容，发送给运营人员
				// String content ="【电动侠租车】用户:"+sub.getName()+"开始还车，手机号:"+sub.getPhoneNo()+"，车牌号:"+carInfo.get("vehicle_plate_id")+"。还车时间:"+endTime+",还车地点:"+carInfo.get("address")+".";
				String content = MsgContent.getSmsMsg(MsgContent.USER_BACK_CAR_MSG, sub.getName(), sub.getPhoneNo(),
						carInfo.get("vehicle_plate_id"), endTime, dotInfo.get("address").toString());
				
				//给地勤发短信
//				String phoneNos = PropertiesUtil.getBundleValue("mobile_phone");
//				System.out.println("开始发送短信---手机号:"+phoneNos);
//				sendMSg.send(phoneNos, content);//发送短信				
				
				// 添加消息记录
				GroundNotice groundNotice = new GroundNotice();
				groundNotice.setNoticeContent(content);
				groundNotice.setCreateTime(new Date());
				groundNotice.setNoticeType(0);
				String subId = UUID.randomUUID().toString().replace("-", "");
				groundNotice.setId(subId);
				subNoticeService.addGrouneNotice(groundNotice);
				// 获取取车网点所属的地勤人员
				SubDot subDot = new SubDot();
				subDot.setDotId(dotId);
				List<SubDot> subDotList = subDotService.getSubDotList(subDot);
				// 添加消息关联记录
				if (subDotList != null && subDotList.size() > 0) {
					for (int i = 0; i < subDotList.size(); i++) {
						SubNotice subNotice = new SubNotice();
						String subAndDotId = UUID.randomUUID().toString().replace("-", "");
						subNotice.setDotId(dotId);
						subNotice.setUserId(subDotList.get(i).getSubId());
						subNotice.setNoticeId(subId);
						subNotice.setIsRead(0);
						subNotice.setId(subAndDotId);
						subNoticeService.addSubNotice(subNotice);
					}
				}

				Map<String, String> car = CurrentCarInfo.getCarlatlng(deviceBindingMap.get("device_no"));
				String carLat = car.get("lat");
				String carLng = car.get("lng");

				double distance = MapDistance.Distance(Double.parseDouble(carLat), Double.parseDouble(carLng),
						Double.parseDouble(dotLng), Double.parseDouble(dotLat));
				String operateContent = "订单号:" + ord.getOrdersNo() + "车牌号:" + carMap.get("vehiclePlateId") + "还车网点:"
						+ carInfo.get("address") + "还车网点经纬度:" + dotLat + "," + dotLng + "车辆当前经纬度:" + carLat + ","
						+ carLng + "实际距离:" + distance + ".车辆行驶距离:" + mileage + "(/米)";

				// 添加运行操作日志
				runLogService.addRunAppBackCar(ord.getOrdersNo(), s.getName(), ip,
						carMap.get("vehiclePlateId").toString(), operateContent);
				result = Ajax.AppJsonResult(Constants.APP_BACKCAR_STATE_YES, Constants.APP_BACKCAR_STATE_YES_MSG);
				// 从redis中移除租车中的实时计费订单信息
				OrderRedisData.delCurrentOrderById(ord.getId());

				Dict d = dictService.getDictByCodes("carBizState", "0");// 获取字典表未租借的ID
				updateCarStatus(carId, d.getId(), null);// 更新车辆状态为（未租借）

			} else {

				Map<String, String> car = CurrentCarInfo.getCarlatlng(deviceBindingMap.get("device_no"));
				String carLat = car.get("lat");
				String carLng = car.get("lng");

				double distance = MapDistance.Distance(Double.parseDouble(carLat), Double.parseDouble(carLng),
						Double.parseDouble(dotLng), Double.parseDouble(dotLat));
				String operateContent = "订单号:" + ord.getOrdersNo() + "车牌号:" + carMap.get("vehiclePlateId") + "还车网点:"
						+ carInfo.get("address") + "还车网点经纬度:" + dotLat + "," + dotLng + "车辆当前经纬度:" + carLat + ","
						+ carLng + "实际距离:" + distance + "未到达指定区域还车.车辆行驶距离:" + mileage + "(/米)";

				// 如果车辆未到达网点区域，将恢复车辆供电
				HttpRequestUtil.getPostMethod(CurrentCarInfo.OPEN_LOCK_POWER_URL, paramMaps);
				// 添加运行操作日志
				runLogService.addRunAppBackCar(ord.getOrdersNo(), s.getName(), ip,
						carMap.get("vehiclePlateId").toString(), operateContent);
				result = Ajax.AppJsonResult(Constants.APP_BACKCARDOT_DISTANCE, "到达网点区域才可还车哦");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
			Map<String, Object> logMapError = new HashMap<String, Object>();
			logMapError.put("operatorName", "用户");
			logMapError.put("operateDate", new Date());
			logMapError.put("operateRemark", "进入catch里");
			logMapError.put("operateContent", result);
			logMapError.put("modelName", "返回服务器异常");
			sysOperateLogService.addSysOperateLog(logMapError);
		} finally {
			JSONObject obj = JSONObject.fromObject(result);
 			if (obj.containsKey("resultCode")) {
				// 如果返回码为还车成功，修改车辆状态：未租借
				if (obj.getString("resultCode").equals(Constants.APP_BACKCAR_STATE_YES)) {
					Dict d = dictService.getDictByCodes("carBizState", "0");// 获取字典表未租借的ID
					updateCarStatus(carId, d.getId(), null);// 更新车辆状态为（未租借）
				} else {
					Map<String, Object> logMapError = new HashMap<String, Object>();
					logMapError.put("operatorName", "用户");
					logMapError.put("operateDate", new Date());
					logMapError.put("operateRemark", "智行通返回不是261");
					logMapError.put("operateContent", result);
					logMapError.put("modelName", "智行通返回不是261");
					sysOperateLogService.addSysOperateLog(logMapError);
				}
			} else {
				Map<String, Object> logMapError = new HashMap<String, Object>();
				logMapError.put("operatorName", "用户");
				logMapError.put("operateDate", new Date());
				logMapError.put("operateRemark", "finally中出错");
				logMapError.put("operateContent", result);
				logMapError.put("modelName", "finally中出错");
				sysOperateLogService.addSysOperateLog(logMapError);
			}
		}
		return result;
	}

	public static void main(String[] args) throws Exception {

		Map<String, String> paramMap = new HashMap<String, String>();
		// String vehiclePlateId = new String("津ATP070".getBytes("UTF-8"));
		String vehiclePlateId = ChangeCharset.toGB2312("津ATP070");
		System.out.println(vehiclePlateId);

		String msgUrl = "http://114.215.143.226:8081/cs_car_msg/car/getCarMileageForGPS";

		// 车牌号(字符码 gb2312)
		paramMap.put("sn", vehiclePlateId);

		paramMap.put("beginTime", new String("2017-03-06 15:00:00".getBytes("UTF-8"), "GB2312"));

		paramMap.put("endTime", new String("2017-03-06 18:00:00".getBytes("UTF-8"), "GB2312"));
		System.out.println("中文".getBytes());
		// String mileage =
		// BranchDotParam.getCarMileageForGPS(paramMap,msgUrl,sysOperateLogService);
	}

	public Map<String, Object> getData(String id, String carId) {
		String[] insuranceFees = null;// 保险名称
		String[] insuranceNames = null;// 保险金额
		int insuranceCosts = 0;// 保险费用,抽离新增
		Map<String, Object> dataMap = new HashMap<String, Object>();
		// 根据id查询出order
		Orders ord = orderService.userOrderInfo(id, Orders.STATE_ORDER_START);
		dataMap.put("ord", ord);
		
		// 根据id查询出用户信息
		Subscriber sub = subScriberService.querySubscriberById(id);
		dataMap.put("sub", sub);
		
		// 根据车辆ID获取车辆信息
		Map<String, String> carInfo = carService.getCarDotByCarId(carId);
		dataMap.put("carInfo", carInfo);
		
		// 获取订单详情
		OrdersDetail orderDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
		dataMap.put("orderDetail", orderDetail);
		
		// 订单开始时间
		String beginTime = ToolDateTime.format(orderDetail.getBeginTime(), ToolDateTime.pattern_ymd_hms);
		dataMap.put("beginTime", beginTime);
		
		// 订单结束时间
		String endTime = ToolDateTime.format(new Date(), ToolDateTime.pattern_ymd_hms);
		dataMap.put("endTime", endTime);
		
		// 获取车辆集合
		Map<String, Object> carMap = carService.carinfo(carId);
		dataMap.put("carMap", carMap);
		
		// 获取车辆里程
		dataMap.put("endTime", endTime);
		Map<String, String> paramMap = new HashMap<String, String>();
		String vehiclePlateId = carMap.get("vehiclePlateId").toString();
		paramMap.put("sn", vehiclePlateId);
		paramMap.put("beginTime", beginTime);
		paramMap.put("endTime", endTime);
		Map<String, Object> logMapError = new HashMap<String, Object>();
		logMapError.put("operatorName", "用户");
		logMapError.put("operateDate", new Date());
		logMapError.put("operateRemark", "获取里程_请求参数_还车");
		logMapError.put("operateContent", "车牌号:" + vehiclePlateId + ";开始时间:" + beginTime + ";结束时间:" + endTime
				+ ";orederId:" + ord.getId() + ";orderDetailId:" + orderDetail.getId());
		logMapError.put("modelName", "获取里程_请求参数_还车");
		sysOperateLogService.addSysOperateLog(logMapError);
		String mileage = BranchDotParam.getCarMileageForGPS(paramMap, CurrentCarInfo.CAR_MILEAGE_FOR_GPS_URL,
				sysOperateLogService);
		System.out.println("还车里程距离=====" + mileage);
		dataMap.put("mileage", mileage);
		
		// 获取优惠卷
		Coupon coupon = couponService.getCouonByOrderId(ord.getId());
		dataMap.put("coupon", coupon);
		
		//获取保险单价
		if (!StringUtils.isEmpty(orderDetail.getInsuranceName())) {
			// [基础保险, 附加保险]
			insuranceNames = orderDetail.getInsuranceName().split("/");
			if (!StringUtils.isEmpty(orderDetail.getInsuranceFee())) {
				insuranceFees = orderDetail.getInsuranceFee().split("/");
			} 
			if (insuranceFees != null) {
				for (String string : insuranceFees) {
					insuranceCosts = insuranceCosts + (int) (Double.valueOf(string) * 100);
				}
			}
		}
		dataMap.put("insuranceCosts", insuranceCosts);
		
		// 获取里程单价
		StrategyBase carfee = orderService.carfee(ord.getCarId());// 计费规则
		double kmPrice = carfee.getKmPrice().doubleValue();// 里程价(单位：公里)
		dataMap.put("kmPrice", kmPrice);
		
		// 获取时间单价
		double timePrice = carfee.getBasePrice().doubleValue();// 时间基数
		dataMap.put("timePrice", timePrice);
		return dataMap;
	}
}
