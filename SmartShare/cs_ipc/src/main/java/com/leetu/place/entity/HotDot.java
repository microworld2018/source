package com.leetu.place.entity;

import java.io.Serializable;
import java.util.Date;

public class HotDot  implements Serializable{

	private static final long serialVersionUID = -4751779396213720950L;
	
	private String id;
	
	private String subscriberId;
	
	private Double lat;
	
	private Double lng;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public HotDot(String subscriberId, Double lat, Double lng) {
		super();
		this.subscriberId = subscriberId;
		this.lat = lat;
		this.lng = lng;
	}

	public HotDot() {
		super();
		// TODO Auto-generated constructor stub
	}
}
