package com.leetu.place.param;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.util.StringUtils;

import com.ground.notice.service.SubNoticeService;
import com.leetu.car.entity.Car;
import com.leetu.car.service.CarService;
import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.service.CouponService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.service.PayOrderService;
import com.leetu.orders.util.OrderFee;
import com.leetu.place.entity.BranchDot;
import com.leetu.place.entity.HotDot;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.entity.SysOperateLogRecord;
import com.leetu.sys.service.DictService;
import com.leetu.sys.service.SysOperateLogService;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.ContantsTool;
import com.util.HttpRequestUtil;
import com.util.JsonTools;
import com.util.MapDistance;
import com.util.PropertiesUtil;
import com.util.Quick;
import com.util.TokenUtils;
import com.util.ToolDateTime;
import com.util.car.CurrentCarInfo;
import com.util.msg.MsgContent;
import com.util.msg.sendMSg;

public class BranchDotParam {

	/**
	 * 获取当前用户一定距离以内的网点(1.0.1)
	 * @param request
	 * @param branchDotService
	 * @return
	 */
	public static String appbranchdots(HttpServletRequest request,
			DictService dictService,BranchDotService branchDotService) {
		String result = null;
		try{
			String data = request.getParameter("data");
			
			if(StringUtils.isEmpty(data)){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_REQUIRED_ERROR, Constants.APP_PARAM_REQUIRED_ERROR_MSG);
				return result;
			}
			
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			
			System.out.println(data+"=================");
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			
			/** 请求参数校验**/
			String lat= pmap.get("lat");
			String lng= pmap.get("lng");
			String raidus = ContantsTool.GPS_DISTANCE;
			if(lng==null){
				result = Ajax.AppJsonResult(Constants.APP_LNG_NULL, Constants.APP_LNG_NULL_MSG);
				return result;
			}
			if(lat==null){
				result = Ajax.AppJsonResult(Constants.APP_LAT_NULL, Constants.APP_LAT_NULL_MSG);
				return result;
			}
			/**
			 * 查询车辆状态码
			 */
			Dict dict = dictService.getDictByCodes("carBizState", "0");
			String bizstatus = dict.getId();
			
			Map<?, ?> map = MapDistance.getAround(lat, lng, raidus);
//			System.out.println(map);
			List<BranchDot> branchDots = branchDotService.branchDots(map);
			if(branchDots.isEmpty()){
				result = Ajax.AppJsonResult(Constants.APP_NO_AVAILABLE_DOT, Constants.APP_NO_AVAILABLE_DOT_MSG);	
				return result;
			}
			Map<String,Object> rm = new HashMap<String,Object>();
			List<Map<String,Object>> list = new ArrayList<Map<String, Object>>(); //存放网点信息
			Map<String,Object> user = new HashMap<String,Object>();
			if(branchDots!=null&branchDots.size()>0){
				
				for (BranchDot branchDot : branchDots) {
					String id = branchDot.getId();
					Integer carcount = branchDotService.getCarCount(id,bizstatus);
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("dotId", id);
					m.put("carcount", carcount);
					m.put("name", branchDot.getName());
					m.put("latlng", branchDot.getLat()+","+branchDot.getLng());
					m.put("address", branchDot.getAddress());
					list.add(m);
				}
				
				Map<String,String> dot = getNearDot(branchDots, lat, lng);
				Integer carcount = branchDotService.getCarCount(dot.get("dotId"),bizstatus);
				//最近网点与用户位置的信息
				user.put("ulatlng", lat+","+lng);
				user.put("distance",dot.get("distance"));
				user.put("dotId", dot.get("dotId"));
				user.put("dotname", dot.get("dotName"));
				user.put("dlatlng", dot.get("dotLatLng"));
				user.put("carcount", carcount);
			}
			rm.put("dots", list);
			rm.put("user", user);
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, rm);
			
			
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		return result;
	}
	/**
	 * 获取最近网点信息
	 * @param branchDots
	 * @param lat
	 * @param lng
	 * @return
	 */
	public static Map<String,String> getNearDot(List<BranchDot> branchDots,String lat,String lng){
		Map<String,String> res = new Hashtable<String,String>();
		if(branchDots!=null&branchDots.size()>0){
			Map<String,Map<String,String>> disdot = new Hashtable<String, Map<String,String>>();
			double[] distance= new double[branchDots.size()] ;
			int i = 0;
			for (BranchDot branchDot : branchDots) {
				//网点与当前位置的距离
				distance[i]=MapDistance.Distance(Double.parseDouble(lat), Double.parseDouble(lng),
						Double.parseDouble(branchDot.getLat().toString()), Double.parseDouble(branchDot.getLng().toString()));
				Map<String,String> m = new Hashtable<String,String>();
				m.put("dotId", branchDot.getId());
				m.put("dotName", branchDot.getName());
				m.put("dotLatLng", branchDot.getLat()+","+branchDot.getLng());
				disdot.put(distance[i]+"",m);
				i++;
			}
			//快速排序(升序)
			Quick q = new Quick();
			double [] arrays = q.quick_sort(distance,distance.length);
			res = disdot.get(arrays[0]+"");
			res.put("distance", arrays[0]+"");
		}
		return res;
	}
	
	/**
	 * 插入催我建点坐标信息
	 * @param request
	 * @param branchDotService
	 * @return
	 * @version 1.0.1
	 */
	public static String insertSuggestedBranchDot(HttpServletRequest request,
			BranchDotService branchDotService) {
		
		String result = null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			String lat= pmap.get("lat");
			String lng= pmap.get("lng");
			Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
			if(null == subscriber){
				return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
			}
			HotDot hotDot = new HotDot(subscriber.getId(),Double.valueOf(lat),Double.valueOf(lng));
			
			Integer count = branchDotService.addHotDot(hotDot);
			if(count==1)
			{
				result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
			}
			else
			{
				result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
			}
		}catch(Exception e){
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		return result;
		
	}

	/**
	 * <p>根据网点ID查询可换车网点信息</p>
	 * 当前网点（取车点）有配置换车网点（db字段returnback_dot），
	 *  读取换车网点，否则可换车网点为当前网点。<br>
	 *  <b>1.0.1 初始化网点，只需不设置换车网点</b><br>
	 * @param request
	 * @param branchDotService
	 * @return
	 * @version 1.0.1
	 */
	public static String getReturnDotsByDotid(HttpServletRequest request,
			BranchDotService branchDotService) {
		String result =null;
		try{
				String data = request.getParameter("data");
				Map<String, String> pmap = JsonTools.desjsonForMap(data);
				if(pmap.get("param").equals("error")){
					result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				}
				Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
				if(null == subscriber){
					return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
				}
				String dotId= pmap.get("dotId");
				BranchDot dot  = branchDotService.getDotById(dotId);
				
				Map<String,Object> rm = new HashMap<String,Object>();
			
				if(dot !=null){
					
					ArrayList<Map<String,Object>> list = new ArrayList<Map<String, Object>>();
					rm.put("dots", list);
					//1.若没有设置换车网点，将默认为所有网点可换车
					List<BranchDot> returnDots  = branchDotService.getAllDots();
						for(BranchDot rd : returnDots){
							Map<String,Object> m = new HashMap<String,Object>();
							m.put("dotid", rd.getId());
							m.put("name", rd.getName());
							m.put("address", rd.getAddress());
							m.put("latlng", rd.getLat()+","+rd.getLng());
							list.add(m);
						}
				}
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, rm);
		}catch(Exception e){
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		return result;
		
	}
	
	
	
	/**
	 * <p>根据网点ID查询可换车网点信息</p>
	 * 当前网点（取车点）有配置换车网点（db字段returnback_dot），
	 *  读取换车网点，否则可换车网点为当前网点。<br>
	 *  <b>1.0.1 初始化网点，只需不设置换车网点</b><br>
	 * @param request
	 * @param branchDotService
	 * @return
	 * @version 1.0.1
	 */
	public static String getReturnDotsByDotidNew(HttpServletRequest request,
			BranchDotService branchDotService) {
		String result = null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			/** 请求参数校验**/
			String lat= pmap.get("lat");
			String lng= pmap.get("lng");
			String raidus = ContantsTool.GPS_DISTANCE;
			if(lng==null){
				result = Ajax.AppJsonResult(Constants.APP_LNG_NULL, Constants.APP_LNG_NULL_MSG);
				return result;
			}
			if(lat==null){
				result = Ajax.AppJsonResult(Constants.APP_LAT_NULL, Constants.APP_LAT_NULL_MSG);
				return result;
			}
			Map<?, ?> map = MapDistance.getAround(lat, lng, raidus);
			List<BranchDot> branchDots = branchDotService.branchDots(map);
			if(branchDots.isEmpty()){
				result = Ajax.AppJsonResult(Constants.APP_NO_AVAILABLE_DOT, Constants.APP_NO_AVAILABLE_DOT_MSG);	
				return result;
			}
			Map<String,Object> rm = new HashMap<String,Object>();
			List<Map<String,Object>> list = new ArrayList<Map<String, Object>>(); //存放网点信息
			if(branchDots!=null&branchDots.size()>0){
				for (BranchDot branchDot : branchDots) {
					String id = branchDot.getId();
					Map<String,Object> m = new HashMap<String,Object>();
					m.put("dotid", id);
					m.put("name", branchDot.getName());
					m.put("latlng", branchDot.getLat()+","+branchDot.getLng());
					m.put("address", branchDot.getAddress());
					list.add(m);
				}
			}
			rm.put("dots", list);
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, rm);
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		return result;
		
	}
	
	
	/**
	 * 确认还车
	 * @param request
	 * @param dictService
	 * @param branchDotService
	 * @param orderService 
	 * @return
	 */
	public static String backCar(HttpServletRequest request,
			DictService dictService, BranchDotService branchDotService, 
			OrderService orderService,OrdersDetailService 
			ordersDetailService,
			CarService carService,
			DeviceBindingService deviceBindingService,
			PayOrderService payorderService,
			CouponService couponService,
			SubScriberService subScriberService,
			SysOperateLogService sysOperateLogService,
			SubNoticeService subNoticeService) {
		String result = null;
		//获取总里程
		String mileage = "0";//默认0
		
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				//添加操作日志
				Map<String, Object> logMapError = new HashMap<String, Object>();
				logMapError.put("operatorName", "用户");
				logMapError.put("operateDate", new Date());
				logMapError.put("operateRemark","参数为param为error");
				logMapError.put("operateContent", result);
				logMapError.put("modelName", "车辆还车");
				sysOperateLogService.addSysOperateLog(logMapError);
				return result;
			}
			//请求参数校验
			String dotId= pmap.get("dotId");
//			String floorNo= pmap.get("floorNo");
//			String carNo= pmap.get("carNo");
			String carId= pmap.get("carId");
			
			Map<String, Object> carMap = carService.carinfo(carId);
			
			//根据车辆ID获取车辆设备绑定，车机编号SN
			Map<String, String> deviceBindingMap = deviceBindingService.getDeviceByCarId(carId);
			
			if(deviceBindingMap == null || StringUtils.isEmpty(deviceBindingMap)){
				//添加操作日志
				Map<String, Object> logMapError = new HashMap<String, Object>();
				logMapError.put("operatorName", "用户");
				logMapError.put("operateDate", new Date());
				logMapError.put("operateRemark","dotId==null");
				logMapError.put("operateContent", "获取车机编号SN为null");
				logMapError.put("modelName", "车辆还车");
				sysOperateLogService.addSysOperateLog(logMapError);
				return Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
			}
			
			Map<String, String> paramMaps = new HashMap<String, String>();
			paramMaps.put("sn", deviceBindingMap.get("device_no"));//设备编号
			
			
			if(dotId==null){
				result = Ajax.AppJsonResult(Constants.APP_DOTID_NULL, Constants.APP_DOTID_NULL_MSG);
				Map<String, Object> logMapError = new HashMap<String, Object>();
				logMapError.put("operatorName", "用户");
				logMapError.put("operateDate", new Date());
				logMapError.put("operateRemark","dotId==null");
				logMapError.put("operateContent", result);
				logMapError.put("modelName", "车辆还车");
				sysOperateLogService.addSysOperateLog(logMapError);
				return result;
			}
			if(carId==null){
				result = Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
				Map<String, Object> logMapError = new HashMap<String, Object>();
				logMapError.put("operatorName", "用户");
				logMapError.put("operateDate", new Date());
				logMapError.put("operateRemark","carId==null");
				logMapError.put("operateContent", result);
				logMapError.put("modelName", "车辆还车");
				sysOperateLogService.addSysOperateLog(logMapError);
				return result;
			}
			
			
			/*if(floorNo==null){
				result = Ajax.AppJsonResult(Constants.APP_FLOORNO_NULL, "上传参数:floorNo,楼层为空！");
				return result;
			}
			if(carNo==null){
				result = Ajax.AppJsonResult(Constants.APP_CARNO_NULL, "上传参数:carNo,车位号为空！");
				return result;
			}*/
			
//			Map<String,Object> rm = new HashMap<String,Object>();
//			Integer backCarState = 0;//0不能还车 1可以还车
//			String backCarDesc = "不能还车,车辆当前位置与还车网之间的距离有点远";
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			
			Subscriber sub = subScriberService.querySubscriberById(id);
			
			//查询用户是否有租车中的订单
			Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);
			if(ord==null){
				result = Ajax.AppJsonResult(Constants.APP_BACKCAR_STATE_NO, Constants.APP_BACKCAR_STATE_NO_MSG);
				Map<String, Object> logMapError = new HashMap<String, Object>();
				logMapError.put("operatorName", "用户");
				logMapError.put("operateDate", new Date());
				logMapError.put("operateRemark","ord==null");
				logMapError.put("operateContent", result);
				logMapError.put("modelName", "车辆还车");
				sysOperateLogService.addSysOperateLog(logMapError);
				return result;
			}
			
			//实际还车地点
			String backdotId = ord.getEndSiteId();
			//判断当前网点 与 还车网点是否一样 方案：A取A还
//			if(backdotId.equals(dotId)){
				//查询网点为租借车辆的信息
//				List<Map<String, Object>> cars = branchDotService.dotcars(dotId);
//				if(cars.size()>0){
					Map<String, Object> dotInfo = branchDotService.dotInfo(backdotId);
					String dotLat = dotInfo.get("lat").toString();
					String dotLng = dotInfo.get("lng").toString();
					//判断车辆当前位置与还车网之间的距离是否在误差范围内-(测试注销，正式使用时加上误差距离判断)
					if(CurrentCarInfo.isDoatCar(deviceBindingMap.get("device_no"), dotLat, dotLng)){
						//更新取车网点信息
						branchDotService.updateDotCar(carId, ord.getBeginSiteId(), 0);
						//添加还车网点信息
						pmap.put("carId", carId);
						pmap.put("dotId", dotId);
						pmap.put("isused", "1");
//						pmap.put("floorno", floorNo);
//						pmap.put("parkingno", carNo);
//						pmap.put("subid", id);
						branchDotService.updateDotcar(pmap);
						/*backCarState = 1;//0不能还车 1可以还车
						backCarDesc = "可以还车";*/
						//获取订单详情
						OrdersDetail orderDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
						//订单开始时间
						String beginTime = ToolDateTime.format(orderDetail.getBeginTime(),ToolDateTime.pattern_ymd_hms);
						//订单结束时间
						String endTime = ToolDateTime.format(new Date(), ToolDateTime.pattern_ymd_hms);
						
						String msgUrl = PropertiesUtil.getStringValue("msg_url")+"/car/getCarMileageForGPS";
						
						Map<String,String> paramMap = new HashMap<String, String>();
						
//						paramMap.put("sn", deviceBindingMap.get("device_no"));//设备编号
						//车牌号
						paramMap.put("sn", String.valueOf(carMap.get("vehiclePlateId")));
						
						paramMap.put("beginTime", beginTime);
						
						paramMap.put("endTime", endTime);
						
						/*paramMap.put("beginTime", "2016-12-2 19:35:00");
						
						paramMap.put("endTime", "2016-12-2 20:30:00");*/

						mileage = getCarMileageForGPS(paramMap,msgUrl,sysOperateLogService);
						
						if(mileage != null && !"".equals(mileage) && !"0".equals(mileage)){
							//计算里程、精确到小数点后两位
							mileage = String.valueOf(ArithUtil.div(Double.valueOf(mileage), 1000, 2));
						}
						//计算公里数如果带小数则直接加1
						double dmileage = Double.parseDouble(mileage);
						int idmileage = (int)dmileage;
						if ((int)((dmileage * 100)%100) > 0 || idmileage == 0){
							idmileage++;
						}
						ordersDetailService.updateByOrdersIdDetil(ord.getId(),String.valueOf(idmileage),endTime); //更新订单详情添加结束时间
						
						Dict d = dictService.getDictByCodes("carBizState", "0");//获取字典表未租借的ID
						
						String version = carMap.get("version").toString();
						branchDotService.updateCarStatus(carId, d.getId(), version);//更新车辆状态为（未租借）
						
						//获取优惠卷
						Coupon coupon = couponService.getCouonByOrderId(ord.getId());
						//获取订单费用信息
						Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
						
						double actual_fee  = 0.00;//实际消费金额
						double payPrice = 0.00;//第三方实际支付金额
						double userPrice  = 0.00;//账户支付金额
						double coupon_fee  = 0.00;//优惠券支付金额
						
						double remainingAmount = 0.00;//账户剩余余额
						
						double allPrice = Double.valueOf(orderfee.get("allPrice").toString());//总费用
//						double minConsumption = Double.valueOf(orderfee.get("minConsumption").toString());//最低消费金额
						double allMile =  Double.valueOf(orderfee.get("allMile").toString());
						double allMilePrice = Double.valueOf(orderfee.get("allMilePrice").toString());//里程消费
						double allTimePrice = Double.valueOf(orderfee.get("allTimePrice").toString());//时长消费
						
						Map<String,Object> order = new Hashtable<String,Object>();
						order.put("endTime", endTime);
						order.put("orderId", ord.getId());
						order.put("end_site_id", dotId);
						order.put("orders_duration", orderfee.get("allminutes"));
						order.put("total_fee", allPrice);
						order.put("actual_fee", actual_fee);
						order.put("tpos_pay_fee", payPrice);
						order.put("use_balance", userPrice);
						order.put("coupon_fee", 0);
						order.put("end_mileage", orderfee.get("endmile"));
						order.put("state", Orders.STATE_ORDER_END);
						order.put("channel", Orders.PAY_CHANNEL_APP);
						order.put("allMile", allMile);
						order.put("allMilePrice", allMilePrice);
						order.put("allTimePrice", allTimePrice);
						order.put("payType", Orders.PAY_TYPE_ACCOUNT);
						order.put("is_paid", 1);
						order.put("is_over", 1);
						order.put("is_running", 1);
						//更新订单信息
						payorderService.updateOrderInfo(order);
						
						//根据车辆ID获取车辆信息
						Map<String, String> carInfo = carService.getCarDotByCarId(carId);
						
						//短信发送内容，发送给运营人员
//						String content = "【电动侠租车】用户:"+sub.getName()+"开始还车，手机号:"+sub.getPhoneNo()+"，车牌号:"+carInfo.get("vehicle_plate_id")+"。还车时间:"+endTime+",还车地点:"+carInfo.get("address")+".";
						
						String content = MsgContent.getSmsMsg(MsgContent.USER_BACK_CAR_MSG, sub.getName(), sub.getPhoneNo(), carInfo.get("vehicle_plate_id"), endTime, carInfo.get("address"));
						
						String phoneNos = PropertiesUtil.getBundleValue("mobile_phone");
						
						System.out.println("开始发送短信---手机号:"+phoneNos);
//						sendMSg.send(phoneNos, content);//发送短信
						
						Map<String,String> car =  CurrentCarInfo.getCarlatlng(deviceBindingMap.get("device_no"));
						String carLat = car.get("lat");
						String carLng = car.get("lng");
						
						double distance = MapDistance.Distance(Double.parseDouble(carLat), Double.parseDouble(carLng),Double.parseDouble(dotLng), Double.parseDouble(dotLat));
						String operateContent = "订单号:"+ord.getOrdersNo()+"车牌号:"+carMap.get("vehiclePlateId")+"还车网点经纬度:"+dotLat+","+dotLng+"车辆当前经纬度:"+carLat+","+carLng+"实际距离:"+distance;
					
						
						//还车远程断电
						String msgUrls = PropertiesUtil.getStringValue("msg_url")+"/car/powerFailure";
						
						String contents = HttpRequestUtil.getPostMethod(msgUrls,paramMaps);
						
						System.out.println(contents);
						
						result = Ajax.AppJsonResult(Constants.APP_BACKCAR_STATE_YES, Constants.APP_BACKCAR_STATE_YES_MSG);
						//添加操作日志
						Map<String, Object> logMap = new HashMap<String, Object>();
						logMap.put("operatorName", "用户");
						logMap.put("operateDate", new Date());
						logMap.put("operateRemark","远程断电");
						logMap.put("operateContent", contents);
						logMap.put("modelName", "车辆还车");
						sysOperateLogService.addSysOperateLog(logMap);
						return result;
					}else{
						
						Map<String,String> car =  CurrentCarInfo.getCarlatlng(deviceBindingMap.get("device_no"));
						String carLat = car.get("lat");
						String carLng = car.get("lng");
						
						double distance = MapDistance.Distance(Double.parseDouble(carLat), Double.parseDouble(carLng),Double.parseDouble(dotLng), Double.parseDouble(dotLat));
						String operateContent = "订单号:"+ord.getOrdersNo()+"车牌号:"+carMap.get("vehiclePlateId")+"还车网点经纬度:"+dotLat+","+dotLng+"车辆当前经纬度:"+carLat+","+carLng+"实际距离:"+distance;
						//添加操作日志
						Map<String, Object> logMap = new HashMap<String, Object>();
						logMap.put("operatorName", "用户");
						logMap.put("operateDate", new Date());
						logMap.put("operateRemark","add");
						logMap.put("operateContent", operateContent);
						logMap.put("modelName", "车辆还车");
						sysOperateLogService.addSysOperateLog(logMap);
						
						result = Ajax.AppJsonResult(Constants.APP_BACKCARDOT_DISTANCE, "到达网点区域才可还车哦");
						return result;
					}
			
		}catch(Exception e){
			
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		return result;
	}
	
	
	public static String getCarMileageForGPS(Map<String, String> map,String msgUrl,SysOperateLogService sysOperateLogService){
		
		String mileage = "";
		Map<String, Object> logMapError = new HashMap<String, Object>();
		logMapError.put("operatorName", "用户_智行通_请求参数=="+map.get("sn"));
		logMapError.put("operateDate", new Date());
		logMapError.put("operateRemark","获取里程_智行通_请求参数");
		logMapError.put("operateContent", "http://wsm1.wiselink.net.cn:8008/GetCarMileageForGPS.ashx?carNumber="+map.get("sn")+"&startTime="+map.get("beginTime")+"&endTime="+map.get("endTime"));
		logMapError.put("modelName", "获取里程_智行通_请求参数");
		sysOperateLogService.addSysOperateLog(logMapError);	
		String content = HttpRequestUtil.getPostMethod(msgUrl, map);
		JSONObject obj = JSONObject.fromObject(content);
		Object zxt_result = obj.get("zxtResult");
		logMapError.put("operatorName", "用户_智行通=="+map.get("sn"));
		logMapError.put("operateDate", new Date());
		logMapError.put("operateRemark","获取里程_智行通");
		logMapError.put("operateContent", zxt_result.toString());
		logMapError.put("modelName", "获取里程_智行通");
		sysOperateLogService.addSysOperateLog(logMapError);
	
		
		if(content != null && !"".equals(content)){
			
			
			if(obj.containsKey("result")){
				
				if(obj.getString("result") != null && !"".equals(obj.getString("result"))){
				
					JSONObject objs = JSONObject.fromObject(obj.getString("result"));
					
					if(objs.getString("result").equals("1")){//接口返回数据成功
						
						mileage = objs.getString("value");
					}else{
						mileage = "error";
					}
				}
			}
		}
		
		System.out.println("里程接口调用返回数据:"+mileage);
		
		return mileage;
	
}
	public static Object getCarMileageForGPSResult(Map<String, String> map,String msgUrl){
		
//		Object mileage_reuslt=null;
		
		String content = HttpRequestUtil.getPostMethod(msgUrl, map);
		
//		if(content != null && !"".equals(content)){
//			
//			JSONObject obj = JSONObject.fromObject(content);
//			mileage_reuslt = obj.get("result");
//		}
		return content;
		
	}

	/*public static void main(String[] args){
		double ss = 0.001;
		if ((int)((ss * 100)%100) > 0 || aa == 0)
			aa++;
		System.out.println(aa);
	}*/
}
