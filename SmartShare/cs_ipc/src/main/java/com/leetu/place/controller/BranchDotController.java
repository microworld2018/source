package com.leetu.place.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.ground.notice.service.SubNoticeService;
import com.leetu.car.service.CarService;
import com.leetu.coupon.service.CouponService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.service.PayOrderService;
import com.leetu.place.param.BranchDotParam;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.service.DictService;
import com.leetu.sys.service.SysOperateLogService;
import com.util.Ajax;
import com.util.Constants;
import com.util.IpUtil;
import com.util.Utils;

@Controller
@RequestMapping("place")
public class BranchDotController extends BaseController<BranchDotController> {

	public static final Log logger = LogFactory.getLog(BranchDotController.class);
	
	@Autowired
	private BranchDotService branchDotService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private OrdersDetailService ordersDetailService;
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private DeviceBindingService deviceBindingService;
	
	@Autowired
	private PayOrderService payorderService;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private SubScriberService subScriberService;
	
	@Autowired
	private SysOperateLogService sysOperateLogService;
	
	@Autowired
	private SubNoticeService subNoticeService; 
	
	/**
	 * 5.获取当前用户一定距离以内的网点(1.0.1)
	 * @author hzw
	 * @return
	 */
	@RequestMapping("/appbranchdots")
	public void appbranchdots(){
		String result = null;
		try {
			result = BranchDotParam.appbranchdots(request, dictService, branchDotService);
		} catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
//		System.out.println("获取当前用户一定距离以内的网点信息=========="+result);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 6.催我建点 (1.0.1)
	 * @author jyt
	 */
	@RequestMapping("/makeSuggestedDot")
	public void makeSuggestedDot(){
		String result = null;
		//业务处理
		result = BranchDotParam.insertSuggestedBranchDot(request, branchDotService);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 8.获取当前网点可还车的网点
	 * @author jyt
	 */
	@RequestMapping("/getReturnDotsByDotid")
	public void getReturnDotsByDotid(){
		String result = null;
		//业务处理
		String lat = request.getParameter("lat");
		String lng = request.getParameter("lng");
		if(lat != null && lng != null){
			result = BranchDotParam.getReturnDotsByDotidNew(request, branchDotService);
			Utils.outJSONObject(result,response);
		}
		result = BranchDotParam.getReturnDotsByDotid(request, branchDotService);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 确认还车(1.0.1)
	 */
	@RequestMapping("/backCar")
	public void backCar(){
		
		String data = request.getParameter("data");
		String result = null;
		
		try {
			
			String ip = IpUtil.getIpAddr(request);

			result = branchDotService.backCar(data,ip);
		} catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
}
