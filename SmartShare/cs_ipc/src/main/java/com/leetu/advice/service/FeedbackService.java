package com.leetu.advice.service;

import java.util.List;

import com.leetu.advice.entity.Feedback;

/**
 * @author jyt
 * @since 2016年7月27日 下午12:38:46
 */
public interface FeedbackService {

	/**
	 * 插入一条投诉信息
	 * @param fb
	 * @version 1.0.1
	 */
	void addFeedback(Feedback fb);

	/**
	 * 根据会员Id 查看投诉记录
	 * @param subscriberId
	 * @return
	 * @version 1.0.1
	 */
	List<Feedback> getFeedbackBySubscriberId(String subscriberId);
	
	/**
	 * 根据Id查询投诉建议
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	Feedback getFeedbackById(String id);

}
