package com.leetu.advice.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.core.controller.BaseController;
import com.leetu.advice.entity.Inform;
import com.leetu.advice.service.InformService;
import com.leetu.subscriber.entity.Subscriber;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.TokenUtils;
import com.util.Utils;

/**
 * 通知
 * @author zhangsg
 * @since 2016年7月27日 下午12:36:36
 */
@Controller
@RequestMapping("inform")
public class InformController  extends BaseController<InformController> {
	
	@Autowired
	private InformService informService;

	/**
	 * 查询未读通知
	 * 参数明细
	 * @version 1.0.1
	 */
	@RequestMapping("/queryInformByToken")
	public void queryInformByToken(){
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			if(null == s){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Inform info = new Inform();
			info.setSubscriberId(s.getId());
			info.setIsRead(0);//未读
			info.setInformType(0);//通知
			info.setSubscriberId(s.getId());
			//查询会员未读通知
			List<Inform> list = this.informService.getInformByConditions(info);
			
			Map<String ,Object> rm = 	new HashMap<String, Object>();
			List<Map<String,Object>> dataList =new ArrayList<Map<String, Object>>();
			rm.put("informs" ,dataList);
			for(Inform inf : list)
			{
				Map<String,Object> m =new HashMap<String, Object>();
				dataList.add(m);
				m.put("informContent",inf.getInformContent());
				m.put("id",inf.getId());
				m.put("isRead",0);
				m.put("creatTime", inf.getTs());
			}
			
			//更新查询结果为已读
			updateInformRecord(list,s.getId());
			
			result = Ajax.AppbzJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG ,rm);
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}

	/**
	 * 更细消息记录表
	 * @param list
	 * @version 1.0.1
	 */
	private void updateInformRecord(List<Inform> list,String subId) {

		List <Map<String,Object>> irList  =new ArrayList<Map<String, Object>>();
		//存放单发通知ID
		List <String> infoIds = new ArrayList<String>();
		for( Inform r :list){
			 Integer sendType = r.getInformSendType() ;
			if(null != sendType && sendType==Inform.INFO_SEND_GROUP)//群发
			{
				Map<String,Object> m = new HashMap<String, Object>();
				m.put("id",UUID.randomUUID().toString().replaceAll("-", ""));
				m.put("informId", r.getId());
				m.put("subscriberId", subId);
				m.put("isRead", Inform.INFO_READED);
				irList.add(m);
				//irList.add(new InformRecord(r.getId(),subId,Inform.INFO_READED));//表示已读
			}else{
				if( sendType == Inform.INFO_SEND_SINGDLE)
					infoIds.add(r.getId());
			}
		}
		//批量添加群发通知记录
		if(irList.size()>0)
		{
			Integer  num = informService.addBatchInformRecord(irList);
	
		}
		//更新当前会员个人通知
		if(infoIds.size()>0)
		{
			informService.updateInformRecordStatus(Inform.INFO_READED,subId,infoIds);
		}
	}
	
	
	
	/**
	 * 通过会员ID获取消息列表
	 */
	@RequestMapping("/getSubscriberMgs")
	public void getSubscriberMgs(){
		String data = request.getParameter("data");
		String result;
		try {
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			//通过会员ID获取消息列表
			List<Map<String, Object>> subscriberMgsList = informService.getSubscriberMgs(s.getId());
			if(subscriberMgsList.isEmpty()){
				result = Ajax.AppJsonResult(Constants.APP_NO_CONTENT, Constants.APP_NO_CONTENT_MSG);
			}else{
				result = Ajax.AppbzJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG ,subscriberMgsList);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		Utils.outJSONObject(result,response);
	}
}
