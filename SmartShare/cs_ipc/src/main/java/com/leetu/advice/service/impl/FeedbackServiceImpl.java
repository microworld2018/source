package com.leetu.advice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.advice.entity.Feedback;
import com.leetu.advice.mapper.FeedbackMapper;
import com.leetu.advice.service.FeedbackService;

/**
 * @author zhangsg
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {

	@Autowired
	private FeedbackMapper feedbackMapper;

	@Override
	public void addFeedback(Feedback fb) {
		this.feedbackMapper.insertFeedback(fb);
	}

	@Override
	public List<Feedback> getFeedbackBySubscriberId(String subscriberId) {
		
		return this.feedbackMapper.getFeedbackBySubscriberId(subscriberId);
	}

	@Override
	public Feedback getFeedbackById(String id) {
		return this.feedbackMapper.getFeedbackById(id);
	}

}
