package com.leetu.advice.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.advice.entity.Inform;

/**
 * @author jyt
 * @since 2016年7月27日 下午12:38:46
 */
public interface InformService {

	/**
	 * 插入一条投诉信息
	 * @param fb
	 * @version 1.0.1
	 */
	void addInform(Inform fb);

	/**
	 * 根据会员Id 查看投诉记录
	 * @param subscriberId
	 * @return
	 * @version 1.0.1
	 */
	List<Inform> getInformBySubscriberId(String subscriberId);
	
	/**
	 * 根据Id查询投诉建议
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	Inform getInformById(String id);

	/**
	 * 根据会员Id 查看未读通知消息投诉记录
	 * @param info
	 * @version 1.0.1
	 */
	List<Inform> getInformByConditions(Inform info);

	/**
	 * 通过条件更新通知记录状态
	 * @param isRead
	 * @param subscriberId
	 * @param infoIds
	 * @version 1.0.1
	 */
	void updateInformRecordStatus(Integer isRead, String subscriberId,
			List<String> infoIds);

	/**
	 * 批量添加 已读通知记录
	 * @param irList
	 * @version 1.0.1
	 * @return 
	 */
	Integer addBatchInformRecord(List<Map<String,Object>> irList);
	
	
	/**根据用户ID获取消息列表
	 * @param subscriberId
	 * @param currentPage
	 * @return
	 */
	List<Map<String, Object>> getSubscriberMgs(@Param("subscriberId")String subscriberId);

}
