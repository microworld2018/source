package com.leetu.advice.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.advice.entity.Inform;

/**
 * @author jyt
 * @since 2016年7月27日 下午12:40:58
 */
public interface InformMapper {

	/**
	 * @param fb
	 * @version 1.0.1
	 */
	void insertInform(Inform fb);

	/**
	 * @param subscriberId
	 * @return
	 * @version 1.0.1
	 */
	List<Inform> getInformBySubscriberId(@Param("subscriberId")String subscriberId);

	/**
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	Inform getInformById(String id);

	/**
	 * 根据条件查询通知消息
	 * @param info
	 * @return
	 * @version 1.0.1
	 */
	List<Inform> queryInformBySubscriberId(Inform info);

	/**通过通知类型 会员ID 通知Ids更新记录为已读
	 * @param infoType
	 * @param subscriberId
	 * @param infoIds
	 * @version 1.0.1
	 */
	void updateInformRecordStatusByConditions(@Param("isRead")Integer isRead, @Param("subscriberId")String subscriberId,
			@Param("infoIds")List<String> infoIds);

	/**
	 * 批量添加已读消息记录
	 * @param irList
	 * @return
	 * @version 1.0.1
	 */
	Integer insertBatchInformRecord(List<Map<String,Object>>  list);
	
	
	/**根据用户ID获取消息列表
	 * @param subscriberId
	 * @param currentPage
	 * @return
	 */
	List<Map<String, Object>> getSubscriberMgs(@Param("subscriberId")String subscriberId);

}
