package com.leetu.advice.controller;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.core.controller.BaseController;
import com.leetu.advice.entity.Feedback;
import com.leetu.advice.service.FeedbackService;
import com.leetu.subscriber.entity.Subscriber;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.Utils;

/**
 * 一键投诉Controller
 * @author zhangsg
 */
@Controller
@RequestMapping("advice")
public class FeedbackController  extends BaseController<FeedbackController> {
	
	@Autowired
	private FeedbackService feedbackService;

	/**
	 * 23.一键投诉
	 * 参数明细
	 * token、反馈意见、联系方式、图片路径用|分割
	 * @version 1.0.1
	 */
	@RequestMapping("/makeFeedback")
	public void makeFeedback(){
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			Feedback fb = new Feedback();
			pmap.remove("token");
			pmap.remove("param");
			fb =Utils.map2Bean(pmap, fb);
			/*if(null == s || StringHelper.isEmpty(fb.getContactType()) ||
					StringHelper.isEmpty(fb.getFeedbackDesc()) ||StringHelper.isEmpty(fb.getFeedbackImg())
					){
				result =  Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN, Constants.APP_LOGIN_TOKEN_MSG);
				 Utils.outJSONObject(result,response);
			}*/
			fb.setSubscriberId(s.getId());
			fb.setName(s.getName());
			this.feedbackService.addFeedback(fb);
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		Utils.outJSONObject(result,response);
	}
	
}
