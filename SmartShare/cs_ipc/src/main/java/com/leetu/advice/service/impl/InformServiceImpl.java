package com.leetu.advice.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.advice.entity.Inform;
import com.leetu.advice.mapper.InformMapper;
import com.leetu.advice.service.InformService;

/**
 * @author jyt
 * @since 2016年7月27日 下午12:39:23
 */
@Service
public class InformServiceImpl implements InformService {

	@Autowired
	private InformMapper informMapper;

	@Override
	public void addInform(Inform fb) {
		
		this.informMapper.insertInform(fb);
	}

	@Override
	public List<Inform> getInformBySubscriberId(String subscriberId) {
		
		return this.informMapper.getInformBySubscriberId(subscriberId);
	}

	@Override
	public Inform getInformById(String id) {
		return this.informMapper.getInformById(id);
	}

	@Override
	public List<Inform> getInformByConditions(Inform info) {
		// TODO Auto-generated method stub
		return this.informMapper.queryInformBySubscriberId(info);
	}

	@Override
	public void updateInformRecordStatus(Integer isRead, String subscriberId,
			List<String> infoIds) {
		this.informMapper.updateInformRecordStatusByConditions(isRead, subscriberId, infoIds);
		
	}

	@Override
	public Integer addBatchInformRecord(List<Map<String,Object>> irList) {
		// TODO Auto-generated method stub
		return this.informMapper. insertBatchInformRecord(irList);
	}

	@Override
	public List<Map<String, Object>> getSubscriberMgs(String subscriberId) {
		return informMapper.getSubscriberMgs(subscriberId);
	}

}
