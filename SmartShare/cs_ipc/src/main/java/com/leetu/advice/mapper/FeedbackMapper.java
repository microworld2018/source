package com.leetu.advice.mapper;

import java.util.List;

import com.leetu.advice.entity.Feedback;

/**
 * @author jyt
 * @since 2016年7月27日 下午12:40:58
 */
public interface FeedbackMapper {

	/**
	 * @param fb
	 * @version 1.0.1
	 */
	void insertFeedback(Feedback fb);

	/**
	 * @param subscriberId
	 * @return
	 * @version 1.0.1
	 */
	List<Feedback> getFeedbackBySubscriberId(String subscriberId);

	/**
	 * @param id
	 * @return
	 * @version 1.0.1
	 */
	Feedback getFeedbackById(String id);

}
