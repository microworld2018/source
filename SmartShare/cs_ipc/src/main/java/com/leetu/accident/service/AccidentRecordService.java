package com.leetu.accident.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface AccidentRecordService {
	
	List<Map<?, ?>> getAccidentRecordBySubId(Map<String, Object> paramMap);
	
	Integer getAccidentRecordCount(String subId);
	
	Map<?, ?> getAccidentRecordById(String accidentId);
	
	void updateAccidentById(Map<String, Object> paramMap);
	
	void updateAccidentByTradeOrderNo(String tradeOrderNo);
	
	void updateVehicleByOrderId(String orderId);
	
	void updateAccidentByOrderId(String orderId,String handleStatus);
	
	Map<String, Object> getAccidentByTradeOrderNo(String tradeOrderNo);
}
