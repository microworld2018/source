package com.leetu.accident.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.accident.mapper.AccidentRecordMapper;
import com.leetu.accident.service.AccidentRecordService;

@Service
@Transactional
public class AccidentRecordServiceImpl implements AccidentRecordService{

	@Autowired
	private AccidentRecordMapper accidentRecordMapper;

	/**
	 * 获取会员违章事故记录信息
	 * @param subId
	 * @return
	 */
	@Override
	public List<Map<?, ?>> getAccidentRecordBySubId(Map<String, Object> paramMap) {
		return accidentRecordMapper.getAccidentRecordBySubId(paramMap);
	}

	/**
	 * 获取会员违章事故记录信息个数-分页使用
	 * @param subId
	 * @return
	 */
	@Override
	public Integer getAccidentRecordCount(String subId) {
		return accidentRecordMapper.getAccidentRecordCount(subId);
	}
	
	/**
	 * 根据违章记录ID获取违章记录信息
	 * @param accidentId
	 * @return
	 */
	@Override
	public Map<?, ?> getAccidentRecordById(String accidentId){
		return accidentRecordMapper.getAccidentRecordById(accidentId);
	}
	
	/**
	 * 根据违章记录ID修改状态
	 * @param paramMap
	 */
	@Override
	public void updateAccidentById(Map<String, Object> paramMap){
		accidentRecordMapper.updateAccidentById(paramMap);
	}

	/**
	 * 根据交易编号修改车辆违章记录信息支付状态
	 * @param tradeOrderNo
	 */
	@Override
	public void updateAccidentByTradeOrderNo(String tradeOrderNo) {
		accidentRecordMapper.updateAccidentByTradeOrderNo(tradeOrderNo);
	}
	
	/**
	 * 根据订单ID修改车辆违章业务状态
	 * @param orderId
	 */
	@Override
	public void updateVehicleByOrderId(String orderId){
		accidentRecordMapper.updateVehicleByOrderId(orderId);
	}
	
	/**
	 * 根据订单ID修改车辆事故业务状态
	 * @param orderId
	 */
	@Override
	public void updateAccidentByOrderId(String orderId,String handleStatus){
		accidentRecordMapper.updateAccidentByOrderId(orderId,handleStatus);
	}
	
	/**
	 * 根据交易编号查看车辆违章记录信息
	 * @param tradeOrderNo
	 */
	@Override
	public Map<String, Object> getAccidentByTradeOrderNo(String tradeOrderNo){
		return accidentRecordMapper.getAccidentByTradeOrderNo(tradeOrderNo);
	}

}
