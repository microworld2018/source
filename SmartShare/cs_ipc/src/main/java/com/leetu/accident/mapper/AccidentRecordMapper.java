package com.leetu.accident.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface AccidentRecordMapper {

	List<Map<?, ?>> getAccidentRecordBySubId(Map<String, Object> paramMap);
	
	Integer getAccidentRecordCount(@Param("subId")String subId);
	
	Map<?, ?> getAccidentRecordById(@Param("id") String accidentId);
	
	void updateAccidentById(Map<String, Object> paramMap);
	
	void updateAccidentByTradeOrderNo(@Param("tradeOrderNo")String tradeOrderNo);
	
	void updateVehicleByOrderId(@Param("orderId")String orderId);
	
	void updateAccidentByOrderId(@Param("orderId")String orderId,@Param("handleStatus")String handleStatus); 
	
	Map<String, Object> getAccidentByTradeOrderNo(@Param("tradeOrderNo")String tradeOrderNo);
}
