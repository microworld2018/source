package com.leetu.messagePush.service;

import java.util.List;
import java.util.Map;

import com.leetu.messagePush.entity.SPushNotification;

public interface MessagePushService {

	SPushNotification getMessageTitleById(String id);
	List<Map<String,Object>> getAllMessage(Map<String, Object> paramMap);

}
