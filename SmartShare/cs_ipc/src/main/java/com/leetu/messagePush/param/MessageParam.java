package com.leetu.messagePush.param;

import java.awt.print.Paper;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.util.StringUtils;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.leetu.car.entity.CarComment;
import com.leetu.car.service.CarCommentService;
import com.leetu.car.service.CarService;
import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.service.CouponService;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.messagePush.service.MessagePushService;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.util.OrderFee;
import com.leetu.orders.util.OrderRedisData;
import com.leetu.place.entity.BranchDot;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.entity.SMSRecord;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.ContantsTool;
import com.util.IpUtil;
import com.util.JsonTools;
import com.util.MapDistance;
import com.util.Quick;
import com.util.TokenUtils;
import com.util.ToolDateTime;
import com.util.Utils;
import com.util.car.CurrentCarInfo;
import com.util.msg.MsgContent;
import com.util.msg.sendMSg;
import com.util.page.Pager;

public class MessageParam {
	
	/**
	 * 用户订单记录
	 * @param request
	 * @param orderService
	 * @return
	 */
	public static String subMessageRecords(HttpServletRequest request,
			MessagePushService messagePushService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		try {
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);//正常使用时打开
			String id = s.getId();//正常使用时打开
//			String id="sklfjkasljfksd";//正常使用时关闭
			
			Map<String, String> pmap = JsonTools.desjsonForMap(data);//正常使用时打开
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			paramMap.put("id", id);
			
			Pager pager = new Pager();
			pager.setCurPage(Integer.parseInt(pmap.get("currentPage")));//当前页
//			pager.setCurPage(Integer.parseInt("1"));//当前页--测试专用
			pager.setPageSize(10);//每页显示数量
			
			pager.setTotalRow(23); //共条数据
			pager.setStart(pager.getStart()); //开始行
			paramMap.put("pager", pager);
			
			List<Map<String,Object>> ord = messagePushService.getAllMessage(paramMap);
			res.put("orders", ord);
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		}catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}

		return result;
	}

	
}
