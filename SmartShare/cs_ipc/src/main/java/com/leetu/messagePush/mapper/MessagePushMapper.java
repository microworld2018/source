package com.leetu.messagePush.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.messagePush.entity.SPushNotification;
import com.leetu.sys.entity.SysOperateLogRecord;

public interface MessagePushMapper {
    
	SPushNotification getMessageById(String id);

	List<Map<String,Object>> getAllMessage(Map<String, Object> paramMap);

	
}
