package com.leetu.messagePush.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.messagePush.entity.SPushNotification;
import com.leetu.messagePush.param.MessageParam;
import com.leetu.messagePush.service.impl.MessagePushServiceImpl;
import com.leetu.messagePush.util.MessagePushUtil;
import com.util.IpUtil;
import com.util.Utils;

import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.PushPayload;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("message")
public class MessagePushController extends BaseController<MessagePushController>{
	@Autowired
	private MessagePushServiceImpl messagePushService;

	/**
	 * 向所有设备.所有用户推送活动消息
	 * @version 1.0.1
	 * @throws IOException 
	 */
	@RequestMapping("/pushAll")
	public void pushMessage(HttpServletRequest request) throws IOException{
		String sid = request.getParameter("data");
		String data = request.getParameter("data");
		JSONObject obj = JSONObject.fromObject(data);
		String Id = (String) obj.get("pushId");
		String result ="";
		String ip = "";
		PushPayload  payload=null;
		PushResult pushResult=new PushResult();
		try {
			SPushNotification sPushNotification=messagePushService.getMessageTitleById(Id);
			String title=sPushNotification.getTitle();
			String context=sPushNotification.getContent();
			String type=sPushNotification.getType().toString();
		    for(int i=1;i<=2;i++){
		    	if(i==1){
		    		payload=MessagePushUtil.buildPushObject_ios_tagAnd_alertWithExtrasAndMessage(context,title);//推送所有,附加字段(类型)
//		    		payload=MessagePushUtil.buildPushObject_ios_tagAnd_alertWithExtrasAndMessage(context,type,"17004951028");//推送单人,附加字段(类型)
		    		pushResult=MessagePushUtil.messagePush(payload);
		    	}
		    	if(i==2){
		    		payload=MessagePushUtil.buildPushObject_all_alias_alert(context,title);//推送所有,附加字段(类型)
//		    		payload=MessagePushUtil.buildPushObject_all_alias_alert(context,type,"电动侠推送消息标题","13522294244");//推送单人,附加字段(类型)
		    		pushResult=MessagePushUtil.messagePush(payload);
		    	}
		    }
			if(pushResult!=null){
				result=pushResult.toString();
			}
			
			ip = IpUtil.getIpAddr(request);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 向所有设备.所有用户展示具体的内容
	 * @version 1.0.1
	 * @throws IOException 
	 */
	@RequestMapping("/getMessage")
	public void getMessage(HttpServletRequest request,HttpServletResponse response) throws IOException{
		String result = MessageParam.subMessageRecords(request, messagePushService);
		Utils.outJSONObject(result,response);
	}
}
