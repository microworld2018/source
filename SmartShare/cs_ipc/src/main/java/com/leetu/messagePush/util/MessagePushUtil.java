package com.leetu.messagePush.util;

import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.audience.AudienceTarget;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;

public class MessagePushUtil {
	private static  final String appKey ="630f0e893316d8e5a29bea16";
	private static  final String masterSecret = "ddf110777efd895862ac0b21";

	/**
	 * 推送消息
	 * @param payload
	 * @return
	 */
	public static PushResult messagePush(PushPayload payload){
		JPushClient jpushClient = new JPushClient(masterSecret, appKey, null, ClientConfig.getInstance());
	    PushResult result =null;
	    try {
	        result = jpushClient.sendPush(payload);

	    } catch (APIConnectionException e) {
           e.printStackTrace();
	    } catch (APIRequestException e) {
	    	e.printStackTrace();
	    }
	    
	    return result;
	}
	
	/**
	 * 快捷地构建推送对象：所有平台，所有设备，内容为 <测试信息> 的通知。
	 * @return
	 */
	 public static PushPayload buildPushObject_all_all_alert(String message) {
	        return PushPayload.alertAll(message);
	    }
	 
	 /**
	  * 快捷地构建推送对象：所有平台，推送目标是别名为 "alias1"，通知内容为 ALERT,类型为type。
	  * @return
	  */
//	 public static PushPayload buildPushObject_all_alias_alert(String content,String user) {
//	        return PushPayload.newBuilder()
//	                .setPlatform(Platform.all())
//	                .setAudience(Audience.alias(user))
//	                .setNotification(Notification.alert(content))
//	                .build();
//	    }

	 
	 
	 /**
	  * 构建推送对象：平台是 Android，目标是 tag 为 "tag1" 的设备，内容是 Android 通知 ALERT，并且标题为 TITLE。
	  * @return
	  */
	 public static PushPayload buildPushObject_android_tag_alertWithTitle(String title,String context) {
	        return PushPayload.newBuilder()
	                .setPlatform(Platform.android())
	                .setAudience(Audience.all())
	                .setNotification(Notification.android(context, title, null))
	                .build();
	    }
	 /**
	  * 构建推送对象：平台是 iOS，推送目标是 "tag1", "tag_all" 的交集，推送内容同时包括通知与消息 - 通知信息是 ALERT，
	  * 角标数字为 5，通知声音为 "happy"，并且附加字段 from = "JPush"；消息内容是 MSG_CONTENT。通知是 APNs 推送通道的，
	  * 消息是 JPush 应用内消息通道的。APNs 的推送环境是“生产”（如果不显式设置的话，Library 会默认指定为开发）
	  * @return
	  */
	 public static PushPayload buildPushObject_ios_tagAnd_alertWithExtrasAndMessage(String context,String title) {
	        return PushPayload.newBuilder()
	                .setPlatform(Platform.ios())
	                .setAudience(Audience.all())
	                .setNotification(Notification.newBuilder()
	                        .addPlatformNotification(IosNotification.newBuilder()
	                                .setAlert(title)
	                                .setBadge(5)
	                                .setSound("happy")
	                                .addExtra("url", context)
	                                .build())
	                        .build())
	                 .setMessage(Message.content(context))
	                 .setOptions(Options.newBuilder()
//	                         .setApnsProduction(true)//选择true为生产通道
	                         .setApnsProduction(false)
	                         .build())
	                 .build();
	    }
	 
	 /**
	  * 构建推送对象：平台是 iOS，推送目标是 "tag1", "tag_all" 的交集，推送内容同时包括通知与消息 - 通知信息是 ALERT，
	  * 角标数字为 5，通知声音为 "happy"，并且附加字段 from = "JPush"；消息内容是 MSG_CONTENT。通知是 APNs 推送通道的，
	  * 消息是 JPush 应用内消息通道的。APNs 的推送环境是“生产”（如果不显式设置的话，Library 会默认指定为开发）
	  * @return
	  * 个推,附加字段
	  */
	 public static PushPayload buildPushObject_ios_tagAnd_alertWithExtrasAndMessage(String context,String type,String user) {
		 return PushPayload.newBuilder()
				 .setPlatform(Platform.ios())
				 .setAudience(Audience.alias(user))
				 .setNotification(Notification.newBuilder()
						 .addPlatformNotification(IosNotification.newBuilder()
								 .setAlert("电动侠")
								 .setBadge(5)
								 .setSound("happy")
								 .addExtra("type", type)
								 .build())
						 .build())
				 .setMessage(Message.content(context))
				 .setOptions(Options.newBuilder()
//	                         .setApnsProduction(true)//选择true为生产通道
						 .setApnsProduction(false)
						 .build())
				 .build();
	 }
	 /**
	  * 推送所有
	  * @param context
	  * @param type
	  * @param title
	  * @return
	  */
	 public static PushPayload buildPushObject_all_alias_alert(String context,String title) {
		 return PushPayload.newBuilder()
				 .setPlatform(Platform.all())
				 .setAudience(Audience.all())
				 .setNotification(Notification.newBuilder()
						 .addPlatformNotification(AndroidNotification.newBuilder()
								 .setAlert(title)
								 .setTitle("电动侠出行")
								 .addExtra("url", context)
								 .build())
						 .build())
				 .setMessage(Message.content(context))
				 .setOptions(Options.newBuilder()
						 .setApnsProduction(false)
						 .build())
				 
				 .build();
	 }  
	 
	 /**
	  * 推送单人
	  * @param context
	  * @param type
	  * @param title
	  * @return
	  */
	 public static PushPayload buildPushObject_all_alias_alert(String context,String type,String title,String user) {
		 return PushPayload.newBuilder()
				 .setPlatform(Platform.all())
				 .setAudience(Audience.alias(user))
				 .setNotification(Notification.newBuilder()
						 .addPlatformNotification(AndroidNotification.newBuilder()
								 .setAlert("电动侠")
								 .setTitle(title)
								 .addExtra("type", type)
								 .build())
						 .build())
				 .setMessage(Message.content(context))
				 .setOptions(Options.newBuilder()
						 .setApnsProduction(false)
						 .build())
				 
				 .build();
	 }  
	 
	

	 

}
