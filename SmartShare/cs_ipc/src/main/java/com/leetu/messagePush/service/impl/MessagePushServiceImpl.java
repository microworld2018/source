package com.leetu.messagePush.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.messagePush.entity.SPushNotification;
import com.leetu.messagePush.mapper.MessagePushMapper;
import com.leetu.messagePush.service.MessagePushService;
import com.leetu.messagePush.util.HtmlUtil;
@Service
@Transactional
public  class MessagePushServiceImpl implements MessagePushService {
	@Autowired
	private MessagePushMapper messagePushMapper;
	@Override
	public SPushNotification getMessageTitleById(String id) {
		SPushNotification sPushNotification=null;
		try {
			sPushNotification=messagePushMapper.getMessageById(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sPushNotification;
	}
	@Override

	public List<Map<String,Object>> getAllMessage(Map<String, Object> paramMap) {
		List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
		try {
			list=messagePushMapper.getAllMessage(paramMap);
//          将获得的内容转换为html字符串
//			for (Map<String, Object> map : list) {
//				for (String key : map.keySet()) {
//					String htmlContent=HtmlUtil.getHtml(map.get(key).toString(), key, "");
//					map.put(key, htmlContent);
//				}
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

}
