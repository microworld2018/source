package com.leetu.feestrategy.mapper;

import org.apache.ibatis.annotations.Param;

import com.leetu.feestrategy.entity.StrategyBase;

public interface StrategyBaseMapper {

	/**
	 * 根据id查询计费策略
	 * @param strategyId
	 * @return
	 */
	StrategyBase getStrategyById(String strategyId);

	/**
	 * 查询该车的计费规则
	 * @param carId
	 * @return
	 */
	StrategyBase carfee(@Param("carId")String carId);

}
