package com.leetu.feestrategy.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.feestrategy.mapper.StrategyBaseMapper;
import com.leetu.feestrategy.service.StrategyBaseService;

@Service
@Transactional
public class StrategyBaseServiceImpl implements StrategyBaseService{

	@Autowired
	private StrategyBaseMapper strategyBaseMapper;
	
	@Override
	public StrategyBase getStrategyById(String strategyId) {
		return strategyBaseMapper.getStrategyById(strategyId);
	}

	@Override
	public StrategyBase carfee(String carId) {
		return strategyBaseMapper.carfee(carId);
	}

}
