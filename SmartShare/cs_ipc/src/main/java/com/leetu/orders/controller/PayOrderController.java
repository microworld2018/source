package com.leetu.orders.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.service.AccountService;
import com.leetu.account.service.AccountTradeRecordService;
import com.leetu.car.service.CarService;
import com.leetu.coupon.service.CouponService;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.param.PayOrderParam;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.service.PayOrderService;
import com.leetu.orders.service.impl.PayOrderServiceImpl;
import com.leetu.subscriber.controller.SubScriberController;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.Utils;

@Controller
@RequestMapping("pay")
public class PayOrderController extends BaseController<PayOrderController> {
	
	public static final Log logger = LogFactory.getLog(PayOrderController.class);

	@Autowired
	private PayOrderService payorderService;
	
	@Autowired
	private PayOrderServiceImpl payOrderServiceImpl;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private SubScriberService scriberService;
	
	@Autowired
	private AccountTradeRecordService tradeRecordService;
	
	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private OrdersDetailService ordersDetailService;//订单详情Service接口
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private SubCouponService subCouponService;
	
	@Autowired
	private AccountTradeRecordService accountTradeRecordService;
	
	/**
	 * 支付订单
	 * @author hzw
	 */
	@RequestMapping("/payorder")
	public void payorder(){
		String result = payOrderServiceImpl.payorder(request,scriberService, payorderService,
				orderService,dictService,tradeRecordService,accountService,carService,ordersDetailService,couponService,subCouponService);
		
//		result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,result);
		
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 支付订单（新）
	 * @author Jin Guangyu
	 * @date 2017年7月24日下午1:55:53
	 */
	@RequestMapping("/payOrder")
	public void payOrder(){
		String result = payorderService.payOrder(request, orderService, tradeRecordService, ordersDetailService, 
													couponService, subCouponService);
		Utils.outJSONObject(result, response);
	}
	
	/**
	 * 同步通知  支付宝回调方法
	 * @author hzw
	 */
	@RequestMapping("/tbalipayCallBack")
	public void tbalipayCallBack(){
		String result = payOrderServiceImpl.tbalipayCallBack(request, payorderService,tradeRecordService,carService);
		if("success".equals(result)){
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 支付宝异步回调函数
	 */
	@RequestMapping("/alipayCallBack")
	public void alipayCallBack(){
		System.out.println("进入支付宝异步回调函数地址===http://114.215.143.226:8081/cs_ipc/pay/alipayCallBack");
		String result = payOrderServiceImpl.alipayCallBack(request, payorderService,tradeRecordService,carService);
		if("success".equals(result)){
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 支付宝异步回调函数
	 */
	@RequestMapping("/alipayCallBackNew")
	public void alipayCallBackNew(){
		System.out.println("进入支付宝异步回调函数地址===http://114.215.143.226:8081/cs_ipc/pay/alipayCallBack");
		String result = payOrderServiceImpl.alipayCallBackNew(request, payorderService,tradeRecordService,carService,subCouponService);
		if("success".equals(result)){
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 重复支付处理
	 */
	@RequestMapping("/verificationPayment")
	public void verificationPayment(){
		String result = request.getParameter("code");
		if(StringHelper.isNotEmpty(result) && result.equals("6001") || result == "6001"){
			Orders orders = orderService.orderInfoById(request.getParameter("orderId"));
			if(orders == null){
				result = Ajax.AppJsonResult(Constants.APP_ERROR, "没有此订单信息");
			}else{
				List<AccountTradeRecord> list = accountTradeRecordService.queryOrderPayState(orders.getOrdersNo());
				int number = 0;
				for(int i=0;i<list.size();i++){
					if(list.get(i).getResult() == 1){
						number = number+1;
					}
				}
				if(number>0){
					Map<String, Object> orderMap = new HashMap<String, Object>();
					orderMap.put("orderNo", orders.getOrdersNo());
					payOrderServiceImpl.orderOver(orderMap, payorderService, tradeRecordService, carService);
					result = Ajax.AppJsonResult(Constants.APP_SUCCESS,"此订单已支付请勿重新支付！");
				}else{
					result = Ajax.AppJsonResult(Constants.APP_ERROR, "支付失败请重新支付！");
				}
			}
		}else{
			result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
		}
		Utils.outJSONObject(result,response);
	}
	
	
	/**
	 * 异步通知  微信回调方法
	 * @author hzw
	 */
	@RequestMapping("/wxCallBack")
	public void wxCallBack(){
		System.out.println("进入微信回调函数");
		String result = payOrderServiceImpl.wxCallBack(request, payorderService,tradeRecordService,carService);
		try {
			PrintWriter pw = response.getWriter();
			pw.print(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 异步通知  微信回调方法(新)
	 * @author Jin Guangyu
	 * @date 2017年7月29日下午5:04:45
	 */
	@RequestMapping("/wxCallBackNew")
	public void wxCallBackNew(){
		System.out.println("进入微信回调函数");
		String result = payOrderServiceImpl.wxCallBackNew(request, payorderService,tradeRecordService,carService);
		try {
			PrintWriter pw = response.getWriter();
			pw.print(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 调微信查询接口，确认支付是否成功
	 */
	@RequestMapping("/tradeResults")
	public void tradeResults(){
		System.out.println("-------->调微信查询接口，确认支付是否成功");
		String result = "";
		String data = request.getParameter("data");
		Map<String, String> map = JsonTools.desjsonForMap(data);
		if(map.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		try {
			String orderNo = map.get("orderNo");
			int code = payorderService.confirmTradeResults(orderNo, payorderService, tradeRecordService);
			switch (code) {
			case 1:
				payorderService.updateOrderStateToAlreadyPaid(tradeRecordService, orderNo);
				result = Ajax.AppJsonResult(Constants.APP_SUCCESS, "成功并且金额正确");
				break;
			case 2:
				payorderService.updateOrderStateToUnPaid(tradeRecordService, orderNo);
				result = Ajax.AppJsonResult(Constants.TOTAL_FEE_ERROR, "查询成功但金额错误");
				break;
			case 3:
				payorderService.updateOrderStateToUnPaid(tradeRecordService, orderNo);
				result = Ajax.AppJsonResult(Constants.QUERY_WX_FAILD, "查询微信接口失败");
				break;
			case 4:
				payorderService.updateOrderStateToUnPaid(tradeRecordService, orderNo);
				result = Ajax.AppJsonResult(Constants.ORDER_NOT_EXIST, "根据订单号未查询到订单信息");
				break;
			default:
				break;
			}
		} catch (Exception e) {
			logger.error(e);
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 调微信查询接口，确认支付是否成功（新）
	 */
	@RequestMapping("/tradeResultsNew")
	public void tradeResultsNew(){
		System.out.println("-------->调微信查询接口，确认支付是否成功(新)");
		String result = "";
		String data = request.getParameter("data");
		Map<String, String> map = JsonTools.desjsonForMap(data);
		if(map.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		try {
			String orderNo = map.get("orderNo");
			int code = payorderService.confirmTradeResultsNew(orderNo, payorderService, tradeRecordService, subCouponService);
			switch (code) {
			case 1:
				result = Ajax.AppJsonResult(Constants.APP_SUCCESS, "成功并且金额正确");
				logger.info("订单号" + orderNo + "微信查询结果：" + result );
				break;
			case 2:
//				payorderService.updateOrderStateToUnPaid(tradeRecordService, orderNo);
				result = Ajax.AppJsonResult(Constants.TOTAL_FEE_ERROR, "查询成功但金额错误");
				logger.info("订单号" + orderNo + "微信查询结果：" + result );
				break;
			case 3:
//				payorderService.updateOrderStateToUnPaid(tradeRecordService, orderNo);
				result = Ajax.AppJsonResult(Constants.QUERY_WX_FAILD, "查询微信接口失败");
				logger.info("订单号" + orderNo + "微信查询结果：" + result );
				break;
			case 4:
//				payorderService.updateOrderStateToUnPaid(tradeRecordService, orderNo);
				result = Ajax.AppJsonResult(Constants.ORDER_NOT_EXIST, "根据订单号未查询到订单信息");
				logger.info("订单号" + orderNo + "微信查询结果：" + result );
				break;
			default:
				break;
			}
		} catch (Exception e) {
			logger.error(e);
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 订单结束
	 */
	@RequestMapping("/orderOver")
	public void orderOver(){
		String result = "";
		String data = request.getParameter("data");
		Map<String, String> map = JsonTools.desjsonForMap(data);
		//获取当前用户信息
		Subscriber s = TokenUtils.getSubscriber(data);
		if(map.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		try {
			Map<String,Object> orderMap = new HashMap<String, Object>();
			String orderId = (String) map.get("orderId");
			orderMap.put("orderId", orderId);
			orderMap.put("userId", s.getId());
			boolean flag = payorderService.orderOver(orderMap);
			if(flag){
				result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
			}else{
				result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
			}
			Utils.outJSONObject(result,response);
	    } catch (Exception e) {
			logger.error(e);
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
	    }
	}
	
	/**
	 * 支付宝账单下载接口，提供后台系统使用
	 */
	@RequestMapping("/downLoadUrl")
	public void downLoadUrl(){
		
		String result = null;
		try {
			String data = request.getParameter("dateTime");
			
			result = payOrderServiceImpl.gateWay(data);
		} catch (Exception e) {
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage());
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response);
	}
}
