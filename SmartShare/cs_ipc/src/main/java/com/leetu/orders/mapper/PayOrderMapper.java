package com.leetu.orders.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.orders.entity.Orders;

public interface PayOrderMapper {

	/**
	 * 更新订单信息
	 * @param map
	 */
	void updateOrderInfo(Map<String, Object> map);

	/**
	 * 更新订单详情信息
	 * @param map
	 */
	void updateOrderDetailInfo(Map<String, Object> map);
	
	/**
	 * 定时更新订单信息
	 * @param map
	 */
	void updateOrderInfoQuartz(Map<String, Object> map);
	
	/**
	 * 定时更新订单详情信息
	 * @param map
	 */
	void updateOrderDetailInfoQuartz(Map<String, Object> map);
	/**
	 * 更新订单信息优惠卷金额、实际支付金额
	 * @param couponPrice
	 * @param actualFee
	 * @param orderId
	 */
	void updateOrderInfoCoupon(Map<String, Object> map);

	/**
	 * 更新订单详情信息优惠卷金额、实际支付金额
	 * @param couponPrice
	 * @param actualFee
	 * @param orderId
	 */
	void updateOrderDetailInfoCoupon(Map<String, Object> map);
	
	/**
	 * 支付成功后 更新订单状态
	 * @param orderNo
	 * @param tradeRecordNo
	 */
	void updateOrderstate(@Param("orderNo")String orderNo);
	
	/**
	 * 支付成功后 根据订单id更新订单状态
	 * @param orderId
	 */
	void updateOrderstateById(@Param("orderId")String orderId);

//	/**
//	 * 支付成功后 更新订单状态
//	 * @param orderNo
//	 * @param tradeRecordNo
//	 */
//	void updateOrderDeatailstate(@Param("orderNo")String orderNo, @Param("tradeRecordNo")String tradeRecordNo);
	
	/**
	 * 支付成功后 更新订单状态
	 * @param orderNo
	 * @param tradeRecordNo
	 */
	void updateOrderDeatailstate(@Param("orderNo")String orderNo);
	
	/**
	 * 支付成功后 更新订单状态
	 * @param orderId
	 */
	void updateOrderDeatailstateById(@Param("orderId")String orderId);

	/**
	 * 根据订单号查询订单信息
	 * @param orderNo
	 * @return
	 */
	Orders orderByOrderNo(String orderNo);

	/**
	 * 根据订单号更新插入支付平台订单号
	 * @param orderNo
	 * @param orderNo 
	 */
	void updateOrderTransactionId(@Param("transactionId")String transactionId, @Param("orderNo")String orderNo);

	/**
	 * 根据订单号查询订单信息
	 * @param orderNo
	 * @return
	 */
	Orders queryOrderByOrderNo(@Param("orderNo")String orderNo);

	/**
	 * 微信查询确认支付后，修改订单状态为已支付
	 * @param orderNo
	 */
	void updateOrderStateToAlreadyPaid(@Param("orderNo")String orderNo);

	/**
	 * 微信查询失败或金额不对等情况，修改订单状态为未支付
	 * @param orderNo
	 */
	void updateOrderStateToUnPaid(@Param("orderNo")String orderNo);

	/**
	 * 根据对象更新 三方支付金额，三方交易状态，三方订单号，交易状态
	 * @author Jin Guangyu
	 * @date 2017年7月29日上午11:09:14
	 * @param order
	 */
	void updateOrder(Orders order);

	/**
	 * 根据商户订单号查询订单信息
	 * @author Jin Guangyu
	 * @date 2017年7月31日上午10:21:01
	 * @param orderNo
	 * @return
	 */
	Orders queryOrderByTradeNo(@Param("tradeOrderNo")String tradeOrderNo);

	/**
	 * 客户端调用支付宝回调接口，只更新三方支付金额，三方交易状态，交易状态
	 * @author Jin Guangyu
	 * @date 2017年8月1日下午5:17:02
	 * @param order
	 */
	void updateOrderResultAndTposPayFee(Orders order);

}
