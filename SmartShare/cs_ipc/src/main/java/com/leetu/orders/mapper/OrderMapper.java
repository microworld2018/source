package com.leetu.orders.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.orders.entity.Orders;
import com.util.page.Pager;


public interface OrderMapper {

	
	/**
	 * 查询车辆的上笔订单
	 * @param carId
	 * @return
	 */
	Orders preorder(@Param("carId")String carId);

	/**
	 * 添加预约主订单
	 * @param param
	 */
	void addyyorder(Map<String, Object> param);

	/**
	 * 添加预约订单子订单
	 * @param param
	 */
	void addyyorderDetail(Map<String, Object> param);

	/**
	 * 查询用户当天时间内的取消订单的次数
	 * @param id 
	 * @param ostatus
	 * @return
	 */
	Integer orderMapper(@Param("id")String id, @Param("ostatus")Integer ostatus);

	/**
	 * 更新订单状态
	 * @param id
	 * @param ostatus
	 */
	void updateOrderStatus(@Param("id")String id, @Param("state")Integer state);

	/**
	 * 更改主订单信息
	 * @param param
	 */
	void upOrderInfo(Map<String, Object> map);

	/**
	 * 更新详细订单信息
	 * @param param
	 */
	void upOrderDetailInfo(Map<String, Object> param);	
	
	/**
	 * 根据id查询订单信息
	 * @param orderId
	 * @return
	 */
	Orders orderInfoById(@Param("id")String id);

	/**
	 * 查询用户是否已有订单
	 * @param id
	 * @param state
	 * @return
	 */
	Orders userOrderInfo(@Param("id")String id, @Param("state")String state);

	/**
	 * 查询用户是否有未支付订单
	 * @param id
	 * @param payState
	 * @return
	 */
	Orders userOrderInfoNoPay(@Param("id")String id,@Param("ostatus")String ostatus,@Param("payState")String payState);
	
	/**
	 * 查询用户未支付订单list，针对有多个未支付订单，不能支付的异常
	 * @param id
	 * @param ostatus
	 * @param payState
	 * @return
	 */
	List<Orders> userOrderInfoNoPayList(@Param("id")String id,@Param("ostatus")String ostatus,@Param("payState")String payState);
	
	/**
	 * 用户订单记录
	 * @param paramMap
	 * @return
	 */
	List<Map<String, Object>> subOrderRecords(Map<String, Object> paramMap);
	/**
	 * 用户订单数
	 * @param paramMap
	 * @return
	 */
	Integer subOrderRecordsCount(Map<String, Object> paramMap);
	/**恢复还车网点为取车网点
	 * @param orderId
	 * @version 1.0.1
	 */
	void restoreOderDefaultDot(@Param("orderId")String orderId);

	/**保存还车网点的修改信息
	 * @param orderId
	 * @param dotId
	 * @version 1.0.1
	 */
	void updateOderReturnDot(@Param("orderId")String orderId, @Param("dotId")String dotId);

	/**
	 * 更新订单详情
	 * @param orderId
	 */
	void updateOrderDetailStatus(@Param("orderId")String orderId);	
	
	/**
	 * 获取租车中的订单信息
	 * @return
	 */
	List<Orders> getCurrentOrders();
	
	/**
	 * 根据订单编号获取订单信息
	 * @param ordersNo
	 * @return
	 */
	Orders getOrderInfoByOrdersNo(@Param("ordersNo")String ordersNo);
	
	/**
	 * 根据订单编号修改订单异常状态
	 * @param ordersNo
	 */
	void updateOrderAbnormityByOrderNo(@Param("ordersNo")String ordersNo);
	
	
	/**根据车辆ID获取该车辆所有订单
	 * @param carId
	 * @return
	 */
	List<Map<String, Object>> getCarByIdOrderList(@Param("carId")String carId);
	
	/**
	 * 根据对象修改支付状态
	 * @author Jin Guangyu
	 * @date 2017年7月28日下午1:53:42
	 * @param ord
	 */
	void updateOrder(Orders ord);

	/**
	 * 更新订单表中优惠券金额
	 * @author Jin Guangyu
	 * @date 2017年7月29日下午3:22:58
	 * @param orderId
	 * @param couponFee
	 */
	void updateOrderMapperCoupon(@Param("id")String orderId, @Param("coupon_fee")int couponFee);

	/**
	 * 更新订单表优惠券金额,交易记录号
	 * @author Jin Guangyu
	 * @date 2017年7月31日上午10:08:36
	 * @param orderId
	 * @param couponFee
	 * @param tradeOrderNoNew
	 */
	void updateOrderCouponAndTradeNo(@Param("id")String orderId, @Param("coupon_fee")Float couponFee, 
									 @Param("tradeOrderNo")String tradeOrderNoNew, @Param("tposPayFee")Float tposPayFee);
	

}
