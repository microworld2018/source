package com.leetu.orders.service;

import java.math.BigDecimal;
import java.util.List;

import com.leetu.orders.entity.OrdersBill;

/**
 * @author jyt
 * @since 2016年7月25日 上午9:34:50
 */
public interface OrdersBillService {

	/**
	 * 生成发票
	 * @param bill
	 */
	 void addOrderBill(OrdersBill bill);
	 
	 /**
	  * 根据会员ID查询会员发票历史记录
	  * @param subscriberId
	  * @return
	  */
	 List<OrdersBill> getOrdersBillsBySubscriberId(String subscriberId);
	 
	 /**
	  * 根据会员ID查询可开票总金额
	  * @param subscriberId
	  * @return
	  */
	 BigDecimal  getAvailableOrdersFee2Bill(String subscriberId);
}
