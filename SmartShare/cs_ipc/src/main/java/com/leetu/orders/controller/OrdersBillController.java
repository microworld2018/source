package com.leetu.orders.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.orders.entity.OrdersBill;
import com.leetu.orders.service.OrdersBillService;
import com.leetu.subscriber.entity.Subscriber;
import com.sun.xml.internal.ws.message.StringHeader;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.Utils;

/**
 * @author jyt
 * @since 2016年7月27日 下午1:32:49
 */
@Controller
@RequestMapping("bill")
public class OrdersBillController  extends BaseController<OrdersBillController> {

	@Autowired
	private OrdersBillService ordersBillService;
	
	/**
	 * 33.（生成发票）
	 * <p><b>参数明细</b></p>
	 * <p>token、发票金额、发票抬头、发票类型、收件人、联系方式、邮寄地址、邮编</p>
	 * 
	 *	<p><b>开发票条件</b></p>
	 * <p>开发票条件:开票金额>=100元</p>
	 *  获取所有订单实际消费金额- 已开发票总额 = 未开发票金额
	 */
	@RequestMapping("/makeOutBill")
	public void makeOutBill(){
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			OrdersBill bill = new OrdersBill();
			pmap.remove("token");
			pmap.remove("param");
			bill =Utils.map2Bean(pmap, bill);
			bill.setSubscriberId(s.getId());
			BigDecimal fee = bill.getTotalFee();
			if(null == s ||null == fee  ||
					 StringHelper.isEmpty(bill.getTitle()) 				|| 
					 StringHelper.isEmpty(bill.getType()) 				|| 
					 StringHelper.isEmpty(bill.getRecipients()) 	||
					 StringHelper.isEmpty(bill.getTelphone())		||
					 StringHelper.isEmpty(bill.getAddress())			||
					 StringHelper.isEmpty(bill.getPostcode())
					){
				
				result = Ajax.AppJsonResult(Constants.APP_PARAM_REQUIRED_ERROR, Constants.APP_PARAM_REQUIRED_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			if(   fee.subtract(new BigDecimal("100") ).doubleValue() <0)
			{
				result = Ajax.AppJsonResult(Constants.APP_ORDER_BILL_BAD, "当前金额不足100.00￥,暂不能开票!" );
				Utils.outJSONObject(result,response);
			}
			
			//开发票订单Ids 修改已经生成发票的订单开票状态为已开,
			//测试时注意事务是否OK
			this.ordersBillService.addOrderBill(bill);
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG );
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 34.获取发票未开金额（发票）
	 * 所有订单实际消费金额-已开发票总额 
	 * 新加字段 actual_fee 为实际支付金额
	 */
	@RequestMapping("/getAvailableOrdersFee2Bill")
	public void getAvailableOrdersFee2Bill(){
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			if(null == s){
				result =  Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
				Utils.outJSONObject(result,response);
			}
			BigDecimal availableMoney = this.ordersBillService.getAvailableOrdersFee2Bill(s.getId());
			 Map<String,Object> rm = new HashMap<String, Object>();
			 rm.put("amount",null==availableMoney?0.00:availableMoney);
			 rm.put("subscriberId",s.getId());
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, rm);
		}catch(Exception e){
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	
	/**
	 *35.获取发票历史list（发票）
	 */
	@RequestMapping("/queryOrderBills")
	public void queryOrderBills(){
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			if(null == s){
				result =  Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
				Utils.outJSONObject(result,response);
			}
			List<OrdersBill> list = this.ordersBillService.getOrdersBillsBySubscriberId(s.getId());
			
			Map<String,Object> rm = new HashMap<String, Object>();
			List<Map<String,Object>> dataList = new ArrayList<Map<String, Object>>();
			rm.put("hisBills", list);
			for(OrdersBill ob:list){
				dataList.add(Utils.bean2Map(ob));
			}
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, rm);
		}catch(Exception e){
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
}
