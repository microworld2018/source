package com.leetu.orders.mapper;

import java.math.BigDecimal;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.leetu.orders.entity.OrdersBill;

/**
 * 
 * @author jyt
 * @since 2016年7月25日 上午9:55:10
 */
public interface OrdersBillMapper {

	/**
	 * 添加发票记录
	 * @param bill
	 */
	void insertBill(OrdersBill bill);

	/**
	 * 通过会员ID查询发票记录
	 * @param subscriberId
	 * @return
	 */
	List<OrdersBill> queryBillsBySubscriberId(@Param("subscriberId")String subscriberId);

	/**
	 * 通过会员ID查询会员可票总计金额
	 * @param subscriberId
	 * @return
	 */
	BigDecimal getAvailableMoney2Bill(@Param("subscriberId")String subscriberId);
	
	

}
