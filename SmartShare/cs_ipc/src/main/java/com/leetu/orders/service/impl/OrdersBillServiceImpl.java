package com.leetu.orders.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.orders.entity.OrdersBill;
import com.leetu.orders.mapper.OrdersBillMapper;
import com.leetu.orders.service.OrdersBillService;

/**
 * @author jyt
 * @since 2016年7月25日 上午9:35:22
 */
@Service
@Transactional
public class OrdersBillServiceImpl implements OrdersBillService {

	@Autowired
	private OrdersBillMapper  ordersBillMapper;
	
	@Override
	public void addOrderBill(OrdersBill bill) {
		this.ordersBillMapper.insertBill(bill);
	}

	@Override
	public List<OrdersBill> getOrdersBillsBySubscriberId(String subscriberId) {
		return this.ordersBillMapper.queryBillsBySubscriberId(subscriberId);
	}

	@Override
	public BigDecimal getAvailableOrdersFee2Bill(String subscriberId) {
		return this.ordersBillMapper.getAvailableMoney2Bill(subscriberId);
	}

	
}
