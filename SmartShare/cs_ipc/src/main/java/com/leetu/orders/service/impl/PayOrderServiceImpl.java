package com.leetu.orders.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayDataDataserviceBillDownloadurlQueryRequest;
import com.alipay.api.response.AlipayDataDataserviceBillDownloadurlQueryResponse;
import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.mapper.AccountTradeRecordMapper;
import com.leetu.account.service.AccountService;
import com.leetu.account.service.AccountTradeRecordService;
import com.leetu.account.util.TradeRecordNo;
import com.leetu.car.service.CarService;
import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.entity.SubCoupon;
import com.leetu.coupon.mapper.CouponMapper;
import com.leetu.coupon.mapper.SubCouponMapper;
import com.leetu.coupon.service.CouponService;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.mapper.OrderMapper;
import com.leetu.orders.mapper.OrdersDetailMapper;
import com.leetu.orders.mapper.PayOrderMapper;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.service.PayOrderService;
import com.leetu.orders.util.OrderFee;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.JsonTools;
import com.util.TokenUtils;
import com.util.alipay.config.AlipayConfig;
import com.util.alipay.pay.Alipay;
import com.util.charging.Constant.CouponType;
import com.util.charging.calculate.FeesAlg;
import com.util.charging.calculate.FeesAlgWithCoupon;
import com.util.charging.util.FeeUtils;
import com.util.weixin.Constant;
import com.util.weixin.pay.pay.WeixinPay;
import com.util.weixin.tool.HttpTool;

import net.sf.json.JSONObject;

@Service
@Transactional
public class PayOrderServiceImpl implements PayOrderService{

	@Autowired
	private PayOrderMapper payOrderMapper;
	
	@Autowired
	private AccountTradeRecordMapper accountTradeRecordMapper;
	
	@Autowired
	private SubCouponMapper subCouponMapper;
	
	@Autowired
	private CouponMapper couponMapper;
	
	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
	private OrdersDetailMapper ordersDetailMapper;
	
//	@Autowired
//	private PayOrderService payorderService;
//	
//	@Autowired
//	private AccountTradeRecordService tradeRecordService;
	

	@Override
	public void updateOrderInfo(Map<String, Object> map) {
		payOrderMapper.updateOrderInfo(map);
		payOrderMapper.updateOrderDetailInfo(map);
	}
	@Override
	public void updateOrderInfoQuartz(Map<String, Object> map){
		payOrderMapper.updateOrderInfoQuartz(map);
		payOrderMapper.updateOrderDetailInfoQuartz(map);
	}

//	@Override
//	public void updateOrderstate(String orderNo, String tradeRecordNo) {
//		payOrderMapper.updateOrderstate(orderNo);
//		payOrderMapper.updateOrderDeatailstate(orderNo,tradeRecordNo);
//	}
	
	@Override
	public void updateOrderstate(String orderNo) {
		payOrderMapper.updateOrderstate(orderNo);
		payOrderMapper.updateOrderDeatailstate(orderNo);
	}

	@Override
	public Orders orderByOrderNo(String orderNo) {
		return payOrderMapper.orderByOrderNo(orderNo);
	}
	
	/**
	 * 根据对象更新 三方支付金额，三方交易状态，三方订单号，交易状态
	 * @author Jin Guangyu
	 * @date 2017年7月29日上午11:07:21
	 * @param order
	 */
	@Override
	public void updateOrder(Orders order) {
		payOrderMapper.updateOrder(order);
	}
	
	/**
	 * 客户端调用支付宝回调接口，只更新三方支付金额，三方交易状态，交易状态
	 */
	@Override
	public void updateOrderResultAndTposPayFee(Orders order) {
		payOrderMapper.updateOrderResultAndTposPayFee(order); 
	}
	
	/**
	 * 支付完成结束订单
	 * 支付成功后 更新订单状态、 交易状态
	 * @param orderMap
	 * @return
	 */
	public boolean orderOver(Map<String,Object> orderMap){
		boolean  flag = false;
		try {
			//订单号
			String orderId = orderMap.get("orderId").toString();
			String userId = orderMap.get("userId").toString();
			//查询当前订单
			Orders ord = payOrderMapper.orderByOrderNo(orderId);
			if(null == ord){
				return flag;
			}else{
				//租车是否结束
				if(ord.getState().equals(Orders.STATE_ORDER_END)){
					double actual_fee = 0;
					//更新订单状态
					payOrderMapper.updateOrderstateById(orderId);
					payOrderMapper.updateOrderDeatailstateById(orderId);
					//更新交易状态
//					accountTradeRecordMapper.upRecordState(orderId);
					//妈的逼！有的用订单id 关联，有的用订单号关联
					accountTradeRecordMapper.upRecordState(ord.getOrdersNo(),ord.getTradeOrderNo());
					//更新用户优惠卷状态
					SubCoupon subCoupon =  subCouponMapper.getSubCouponByOrderId(orderId, userId);
					Coupon coupon = couponMapper.getCouonByOrderId(orderId);
					
					Map<String,Object> order = new Hashtable<String,Object>();
					
					if(!StringUtils.isEmpty(coupon)){
						//如果优惠卷金额大于支付金额，实际支付金额就为0
						if(coupon.getPrice().doubleValue() > ord.getTotalFee().doubleValue()){
							actual_fee = 0;
						}else{
							actual_fee = ArithUtil.sub(ord.getTotalFee().doubleValue(),coupon.getPrice().doubleValue());
						}
						order.put("coupon_fee", coupon.getPrice());
					}
					if(!StringUtils.isEmpty(subCoupon)){
						subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
						subCouponMapper.updateSubCoupon(subCoupon);
						
						order.put("orderId", orderId);
						order.put("actual_fee", actual_fee);//实际消费总金额
						order.put("trade_order_no", TradeRecordNo.getPayOrderTradeNo());//交易订单号
						order.put("pay_style", Orders.PAY_TYPE_COUPON);//支付类型:优惠卷支付
						
						//更新订单信息优惠卷金额、实际支付金额
						payOrderMapper.updateOrderInfoCoupon(order);
						//更新订单信息优惠卷金额、实际支付金额
						payOrderMapper.updateOrderDetailInfoCoupon(order);
						
						Orders orders = payOrderMapper.orderByOrderNo(ord.getOrdersNo());
						
						//订单交易记录数据
						Map<String,Object> param = new Hashtable<String,Object>();
						param.put("subscriber_id",userId);
						param.put("type",Account.TYPE_ORDER);
						param.put("pay_type",Orders.PAY_TYPE_COUPON);//支付类型 优惠卷支付
						param.put("pay_channel",Account.PAY_CHANNEL_APP);//渠道
						param.put("amount",actual_fee);//交易金额
						param.put("remaining_amount",0);//账户余额（写死，目前没有账户余额充值）
						param.put("trade_order_no",orders.getTradeOrderNo());//交易单号
						param.put("biz_id",ord.getOrdersNo());//订单号
						param.put("sub_orderId",ord.getRunningDetailNo());//字订单
						param.put("is_preset_card",0);
						param.put("description","优惠卷支付订单付款"+coupon.getPrice()+"元");
						param.put("trade_time","t");
						param.put("is_auto_clear",0);
						param.put("result",1);//支付状态成功
						param.put("order_index",1);
						
						//添加交易记录
						accountTradeRecordMapper.addtradeRecord(param);
					}
					flag = true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = false;
		}
		return flag;
	}
	
	
	/**
	 * 支付订单
	 * @param request
	 * @param scriberService 
	 * @param payorderService
	 * @param orderService
	 * @param dictService 
	 * @param accountService 
	 * @param tradeRecordService 
	 * @param carService 
	 * @return
	 */
	public String payorder(HttpServletRequest request,SubScriberService scriberService,
			PayOrderService payorderService, OrderService orderService, 
			DictService dictService,AccountTradeRecordService tradeRecordService, 
			AccountService accountService, CarService carService,
			OrdersDetailService ordersDetailService,CouponService couponService,SubCouponService subCouponService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();	
		try {
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			String orderId = pmap.get("orderId");
			String payType = pmap.get("payType");//1账户支付   2信用卡支付   3支付宝   4银联 5微信   
			
			if(orderId==""){
				result = Ajax.AppResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG,res);
				return result;
			}
			
			if(payType==null){
				result = Ajax.AppResult(Constants.APP_PAYTYPE_NULL, Constants.APP_PAYTYPE_NULL_MSG,res);
				return result;
			}
			
			Orders ord =orderService.orderInfoById(orderId);
			
			//订单不存在
			if(ord==null){
				result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
				return result;
			}
			//未还车
			if(!ord.getState().equals(Orders.STATE_ORDER_END+"")){
				result = Ajax.AppResult(Constants.APP_NO_BACK_CAR, Constants.APP_NO_BACK_CAR_MSG,res);
				return result;
			}
			//订单已支付
			if(ord.getPayState().equals(Orders.STATE_ORDER_PAY+"")){
				result = Ajax.AppResult(Constants.APP_ORDER_PAY, Constants.APP_ORDER_PAY_MSG,res);
				return result;
			}
			
			//用户订单匹配错误
			if(!ord.getMemberId().equals(id)){
				result = Ajax.AppResult(Constants.APP_ORDER_USER_E, Constants.APP_ORDER_USER_E_MSG,res);
				return result;
			}
			
			List<AccountTradeRecord> accountTradeRecord = tradeRecordService.queryOrderPayState(ord.getOrdersNo());
			
			if(accountTradeRecord != null && accountTradeRecord.size() > 0){
				
				for(AccountTradeRecord account :accountTradeRecord){
					if(account.getResult()==1){//已支付
						//支付成功后 更新订单状态
						payorderService.updateOrderstate(ord.getOrdersNo());
						//支付成功后  更新交易状态
						tradeRecordService.updateRecordState(ord.getOrdersNo(),ord.getTradeOrderNo());
						result = Ajax.AppResult(Constants.APP_ORDER_PAY, Constants.APP_ORDER_PAY_MSG,res);
						return result;
					}
				}
				
				
			}
			
			String dotId = ord.getEndSiteId();//还车网点id
//			double actual_fee  = 0.00;//实际消费金额
			double payPrice = 0.00;//第三方实际支付金额
			double userPrice  = 0.00;//账户支付金额
			double coupon_fee  = 0.00;//优惠券支付金额
			
			double remainingAmount = 0.00;//账户剩余余额
			
			//获取优惠卷
			Coupon coupon = couponService.getCouonByOrderId(ord.getId());
			
			//获取订单费用信息
			Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
			
			//获取订单详情信息
			OrdersDetail ordersDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
			
			Date endTime = ordersDetail.getEndTime();//订单结束时间
			double allPrice = ordersDetail.getTotalFee().doubleValue();//总费用
			double actual_fee = ordersDetail.getActualFee().doubleValue();//实际消费金额
			double allMile = ordersDetail.getMileage().doubleValue();//总里程
			double allMilePrice = ordersDetail.getMileFee().doubleValue();//里程消费
			double allTimePrice = ordersDetail.getTimeFee().doubleValue();//时长消费
			
			//获取订单详情
			OrdersDetail orderDetail = ordersDetailService.getByOrderIdDetail(orderId);
			
			if(!StringUtils.isEmpty(coupon)){
				coupon_fee = coupon.getPrice().doubleValue();//优惠卷支付金额
				if(coupon_fee > allPrice){
					actual_fee = 0;
				}else{
					actual_fee = ArithUtil.sub(allPrice,coupon_fee);
				}
			}
			orderfee.put("actualFee", actual_fee);
			
			//调用支付 方法
			JSONObject json = payType(payType,orderfee,accountService,tradeRecordService,request);
			
			System.out.println("调用支付方法返回json："+json);
			
			@SuppressWarnings("unchecked")
			Map<?, Map<?, ?>> wxJson = (Map<?, Map<?, ?>>)json.get("wx");
			
			payPrice = Double.valueOf(json.get("payp").toString());
			userPrice = Double.valueOf(json.get("userPrice").toString());
			remainingAmount = Double.valueOf(json.get("amount").toString());
//			coupon_fee = payPrice+userPrice;
			
			//判断第三方实际支付金额是否为0 是：用账户支付 否：用微信或支付宝支付
			if(payPrice==0){
				 
				payType = Orders.PAY_TYPE_ACCOUNT.toString();//账户支付
				boolean resState = json.getBoolean("state");
				if(resState){
					Map<String,Object> order = new Hashtable<String,Object>();
					order.put("endTime", endTime);
					order.put("orderId", ord.getId());
					order.put("end_site_id", dotId);
					order.put("orders_duration", orderfee.get("allminutes"));
					order.put("total_fee", allPrice);
					order.put("actual_fee", actual_fee);
					order.put("tpos_pay_fee", payPrice);
					order.put("use_balance", userPrice);
					order.put("coupon_fee", coupon_fee);
					order.put("end_mileage", orderfee.get("endmile"));
					order.put("state", Orders.STATE_ORDER_END);
					order.put("channel", Orders.PAY_CHANNEL_APP);
					order.put("allMile", allMile);
					order.put("allMilePrice", allMilePrice);
					order.put("allTimePrice", allTimePrice);
					order.put("payType", Orders.PAY_TYPE_ACCOUNT);
					order.put("is_paid", 1);
					order.put("is_over", 1);
					order.put("is_running", 1);
					order.put("trade_order_no", TradeRecordNo.getPayOrderTradeNo());//交易单号
					//更新订单信息
					payorderService.updateOrderInfo(order);
					
					//更新用户优惠卷状态
					SubCoupon subCoupon = subCouponService.getSubCouponByOrderId(orderId, s.getId());
					
					if(!StringUtils.isEmpty(subCoupon)){
						subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
						
						subCouponService.updateSubCoupon(subCoupon);
					}
					
				}  
				
			}else{ 
				
				boolean resState = json.getBoolean("state");
				if(resState){
					//预支付成功后   1  添加交易信息 2更新订单信息 
					String description = "订单付款"+payPrice+"元";
					if(String.valueOf(Orders.PAY_TYPE_ALIPAY).equals(payType+"")){
						description = "支付宝订单付款"+payPrice+"元";
					}else if(String.valueOf(Orders.PAY_TYPE_WECHAT).equals(payType+"")){
						description = "微信订单付款"+payPrice+"元";
					}
					
					//订单交易记录数据
					Map<String,Object> param = new Hashtable<String,Object>();
					param.put("subscriber_id",id);
					param.put("type",Account.TYPE_ORDER);
					param.put("pay_type",payType);//支付类型
					param.put("pay_channel",Account.PAY_CHANNEL_APP);//渠道
					param.put("amount",payPrice);//交易金额
					param.put("remaining_amount",remainingAmount);//账户余额
//					param.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());//交易单号
					param.put("biz_id",ord.getOrdersNo());//订单号
					param.put("sub_orderId",ord.getRunningDetailNo());//字订单
					param.put("is_preset_card",0);
					param.put("description",description);
					param.put("trade_time","t");
					param.put("is_auto_clear",0);
					param.put("result",0);
					param.put("order_index",1);
					
					//订单数据
					Map<String,Object> order = new Hashtable<String,Object>();
					order.put("endTime", endTime);
					order.put("orderId", ord.getId());//订单id
					order.put("end_site_id", dotId);//实际还车网点
					order.put("orders_duration", orderfee.get("allminutes"));//总时长 单位：分钟 
					order.put("total_fee", allPrice);//总费用
					order.put("actual_fee", actual_fee);//实际费用
					order.put("tpos_pay_fee", payPrice);//第三方实际支付费用
					order.put("use_balance", userPrice);//账户支付费用
					order.put("coupon_fee", coupon_fee);//优惠券使用金额
					order.put("end_mileage", orderfee.get("endmile"));//结束里程
					order.put("channel", Orders.PAY_CHANNEL_APP);
					order.put("allMile", allMile);
					order.put("allMilePrice", allMilePrice);
					order.put("allTimePrice", allTimePrice);
					order.put("payType", payType);
					order.put("is_paid", 0);
					order.put("is_over", 0);
					order.put("is_running", 1);
					order.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());//交易单号
					
					
					if(wxJson !=null && !"".equals(wxJson)){
						//判断微信统一下单是否订单号重复，如果订单号重复，视为已经支付，修改订单状态
						if(String.valueOf(wxJson.get("order").get("result_code")).equals("FAIL")){
							if (String.valueOf(wxJson.get("order").get("err_code")).equals("INVALID_REQUEST")) {
								//商户订单号重复，但两次金额不同，关闭订单
								JSONObject jObject = WeixinPay.wxCloseOrder(ord.getOrdersNo(),Constant.CLOSE_ORDER_URL);
								System.out.println("微信关闭接口返回数据：--->" + jObject);
								JSONObject jsonObject = JSONObject.fromObject(jObject.get("result"));
								boolean closeState = "SUCCESS".equals(jsonObject.get("result_code")) && "SUCCESS".equals(jsonObject.get("return_code"));
								boolean closedState = "ORDERCLOSED".equals(jsonObject.get("err_code")) && "SUCCESS".equals(jsonObject.get("return_code"));
								if(closeState || closedState){
									System.out.println("关闭订单成功！");
									String rePayResult = rePayAfterOrderClosed(request, scriberService, payorderService, orderService, dictService, tradeRecordService, 
															accountService, carService, ordersDetailService, couponService, subCouponService);
									return rePayResult;
								}
							}
							
							if (String.valueOf(wxJson.get("order").get("err_code")).equals("ORDERPAID")) {
								//支付成功后 更新订单状态
								payorderService.updateOrderstate(ord.getOrdersNo());
								Orders ordInfo =orderService.orderInfoById(orderId);
								param.put("trade_order_no",ordInfo.getTradeOrderNo());//交易单号
								//支付成功后  更新交易状态
								tradeRecordService.updateRecordState(ord.getOrdersNo(),ordInfo.getTradeOrderNo());
							}
						}else{
							
							//更新订单信息
							payorderService.updateOrderInfo(order);
							
							Orders ordInfo =orderService.orderInfoById(orderId);
							
							param.put("trade_order_no",ordInfo.getTradeOrderNo());//交易单号
							
							//添加交易记录
							tradeRecordService.addtradeRecord(param);
							
							
						}
					}else{
						//更新订单信息
						payorderService.updateOrderInfo(order);
						
						Orders ordInfo =orderService.orderInfoById(orderId);
						
						param.put("trade_order_no",ordInfo.getTradeOrderNo());//交易单号
						
						tradeRecordService.addtradeRecord(param);
					}
					
					//更新用户优惠卷状态
					SubCoupon subCoupon = subCouponService.getSubCouponByOrderId(orderId, s.getId());
					
					if(!StringUtils.isEmpty(subCoupon)){
						subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
						
						subCouponService.updateSubCoupon(subCoupon);
					}
				}
			}
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,json);
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	
	/**
	 * 商户订单号重复时，关闭订单后重新支付
	 * @param request
	 * @param scriberService
	 * @param payorderService
	 * @param orderService
	 * @param dictService
	 * @param tradeRecordService
	 * @param accountService
	 * @param carService
	 * @param ordersDetailService
	 * @param couponService
	 * @param subCouponService
	 * @return
	 */
	public String rePayAfterOrderClosed(HttpServletRequest request,SubScriberService scriberService,
										PayOrderService payorderService, OrderService orderService, 
										DictService dictService,AccountTradeRecordService tradeRecordService, 
										AccountService accountService, CarService carService,
										OrdersDetailService ordersDetailService,CouponService couponService,SubCouponService subCouponService){
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();	
		try {
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			String orderId = pmap.get("orderId");
			String payType = pmap.get("payType");//1账户支付   2信用卡支付   3支付宝   4银联 5微信   
			if(orderId==""){
				result = Ajax.AppResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG,res);
				return result;
			}
			if(payType==null){
				result = Ajax.AppResult(Constants.APP_PAYTYPE_NULL, Constants.APP_PAYTYPE_NULL_MSG,res);
				return result;
			}
			
			Orders ord =orderService.orderInfoById(orderId);
			//订单不存在
			if(ord==null){
				result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
				return result;
			}
			//未还车
			if(!ord.getState().equals(Orders.STATE_ORDER_END+"")){
				result = Ajax.AppResult(Constants.APP_NO_BACK_CAR, Constants.APP_NO_BACK_CAR_MSG,res);
				return result;
			}
			//订单已支付
			if(ord.getPayState().equals(Orders.STATE_ORDER_PAY+"")){
				result = Ajax.AppResult(Constants.APP_ORDER_PAY, Constants.APP_ORDER_PAY_MSG,res);
				return result;
			}
			//用户订单匹配错误
			if(!ord.getMemberId().equals(id)){
				result = Ajax.AppResult(Constants.APP_ORDER_USER_E, Constants.APP_ORDER_USER_E_MSG,res);
				return result;
			}
			
			List<AccountTradeRecord> accountTradeRecord = tradeRecordService.queryOrderPayState(ord.getOrdersNo());
			if(accountTradeRecord != null && accountTradeRecord.size() > 0){
				for(AccountTradeRecord account :accountTradeRecord){
					if(account.getResult()==1){//已支付
						//支付成功后 更新订单状态
						payorderService.updateOrderstate(ord.getOrdersNo());
						//支付成功后  更新交易状态
						tradeRecordService.updateRecordState(ord.getOrdersNo(),ord.getTradeOrderNo());
						result = Ajax.AppResult(Constants.APP_ORDER_PAY, Constants.APP_ORDER_PAY_MSG,res);
						return result;
					}
				}
			}
			
			String dotId = ord.getEndSiteId();//还车网点id
			double payPrice = 0.00;//第三方实际支付金额
			double userPrice  = 0.00;//账户支付金额
			double coupon_fee  = 0.00;//优惠券支付金额
			double remainingAmount = 0.00;//账户剩余余额
			
			//获取优惠卷
			Coupon coupon = couponService.getCouonByOrderId(ord.getId());
			
			//获取订单费用信息
			Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
			
			//获取订单详情信息
			OrdersDetail ordersDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
			Date endTime = ordersDetail.getEndTime();//订单结束时间
			double allPrice = ordersDetail.getTotalFee().doubleValue();//总费用
			double actual_fee = ordersDetail.getActualFee().doubleValue();//实际消费金额
			double allMile = ordersDetail.getMileage().doubleValue();//总里程
			double allMilePrice = ordersDetail.getMileFee().doubleValue();//里程消费
			double allTimePrice = ordersDetail.getTimeFee().doubleValue();//时长消费
			
			if(!StringUtils.isEmpty(coupon)){
				coupon_fee = coupon.getPrice().doubleValue();//优惠卷支付金额
				if(coupon_fee > allPrice){
					actual_fee = 0;
				}else{
					actual_fee = ArithUtil.sub(allPrice,coupon_fee);
				}
			}
			
			//调用支付 方法
			JSONObject json = payType(payType,orderfee,accountService,tradeRecordService,request);
			System.out.println("调用支付方法返回json："+json);
			
			@SuppressWarnings("unchecked")
			Map<?, Map<?, ?>> wxJson = (Map<?, Map<?, ?>>)json.get("wx");
			
			payPrice = Double.valueOf(json.get("payp").toString());
			userPrice = Double.valueOf(json.get("userPrice").toString());
			remainingAmount = Double.valueOf(json.get("amount").toString());
			
			//判断第三方实际支付金额是否为0 是：用账户支付 否：用微信或支付宝支付
			if(payPrice==0){
				payType = Orders.PAY_TYPE_ACCOUNT.toString();//账户支付
				boolean resState = json.getBoolean("state");
				if(resState){
					Map<String,Object> order = new Hashtable<String,Object>();
					order.put("endTime", endTime);
					order.put("orderId", ord.getId());
					order.put("end_site_id", dotId);
					order.put("orders_duration", orderfee.get("allminutes"));
					order.put("total_fee", allPrice);
					order.put("actual_fee", actual_fee);
					order.put("tpos_pay_fee", payPrice);
					order.put("use_balance", userPrice);
					order.put("coupon_fee", coupon_fee);
					order.put("end_mileage", orderfee.get("endmile"));
					order.put("state", Orders.STATE_ORDER_END);
					order.put("channel", Orders.PAY_CHANNEL_APP);
					order.put("allMile", allMile);
					order.put("allMilePrice", allMilePrice);
					order.put("allTimePrice", allTimePrice);
					order.put("payType", Orders.PAY_TYPE_ACCOUNT);
					order.put("is_paid", 1);
					order.put("is_over", 1);
					order.put("is_running", 1);
					order.put("trade_order_no", TradeRecordNo.getPayOrderTradeNo());//交易单号
					//更新订单信息
					payorderService.updateOrderInfo(order);
					//更新用户优惠卷状态
					SubCoupon subCoupon = subCouponService.getSubCouponByOrderId(orderId, s.getId());
					if(!StringUtils.isEmpty(subCoupon)){
						subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
						subCouponService.updateSubCoupon(subCoupon);
					}
				}  
			}else{ 
				boolean resState = json.getBoolean("state");
				if(resState){
					//预支付成功后   1  添加交易信息 2更新订单信息 
					String description = "订单付款"+payPrice+"元";
					if(String.valueOf(Orders.PAY_TYPE_ALIPAY).equals(payType+"")){
						description = "支付宝订单付款"+payPrice+"元";
					}else if(String.valueOf(Orders.PAY_TYPE_WECHAT).equals(payType+"")){
						description = "微信订单付款"+payPrice+"元";
					}
					//订单交易记录数据
					Map<String,Object> param = new Hashtable<String,Object>();
					param.put("subscriber_id",id);
					param.put("type",Account.TYPE_ORDER);
					param.put("pay_type",payType);//支付类型
					param.put("pay_channel",Account.PAY_CHANNEL_APP);//渠道
					param.put("amount",payPrice);//交易金额
					param.put("remaining_amount",remainingAmount);//账户余额
					param.put("biz_id",ord.getOrdersNo());//订单号
					param.put("sub_orderId",ord.getRunningDetailNo());//字订单
					param.put("is_preset_card",0);
					param.put("description",description);
					param.put("trade_time","t");
					param.put("is_auto_clear",0);
					param.put("result",0);
					param.put("order_index",1);
					
					//订单数据
					Map<String,Object> order = new Hashtable<String,Object>();
					order.put("endTime", endTime);
					order.put("orderId", ord.getId());//订单id
					order.put("end_site_id", dotId);//实际还车网点
					order.put("orders_duration", orderfee.get("allminutes"));//总时长 单位：分钟 
					order.put("total_fee", allPrice);//总费用
					order.put("actual_fee", actual_fee);//实际费用
					order.put("tpos_pay_fee", payPrice);//第三方实际支付费用
					order.put("use_balance", userPrice);//账户支付费用
					order.put("coupon_fee", coupon_fee);//优惠券使用金额
					order.put("end_mileage", orderfee.get("endmile"));//结束里程
					order.put("channel", Orders.PAY_CHANNEL_APP);
					order.put("allMile", allMile);
					order.put("allMilePrice", allMilePrice);
					order.put("allTimePrice", allTimePrice);
					order.put("payType", payType);
					order.put("is_paid", 0);
					order.put("is_over", 0);
					order.put("is_running", 1);
					order.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());//交易单号
					
					if(wxJson !=null && !"".equals(wxJson)){
						//判断微信统一下单是否订单号重复，如果订单号重复，视为已经支付，修改订单状态
						if(String.valueOf(wxJson.get("order").get("result_code")).equals("FAIL")){
							if (String.valueOf(wxJson.get("order").get("err_code")).equals("ORDERPAID")) {
								//支付成功后 更新订单状态
								payorderService.updateOrderstate(ord.getOrdersNo());
								Orders ordInfo =orderService.orderInfoById(orderId);
								param.put("trade_order_no",ordInfo.getTradeOrderNo());//交易单号
								//支付成功后  更新交易状态
								tradeRecordService.updateRecordState(ord.getOrdersNo(),ordInfo.getTradeOrderNo());
							}
						}else{
							//更新订单信息
							payorderService.updateOrderInfo(order);
							Orders ordInfo =orderService.orderInfoById(orderId);
							param.put("trade_order_no",ordInfo.getTradeOrderNo());//交易单号
							//添加交易记录
							tradeRecordService.addtradeRecord(param);
						}
					}else{
						//更新订单信息
						payorderService.updateOrderInfo(order);
						Orders ordInfo =orderService.orderInfoById(orderId);
						param.put("trade_order_no",ordInfo.getTradeOrderNo());//交易单号
						tradeRecordService.addtradeRecord(param);
					}
					//更新用户优惠卷状态
					SubCoupon subCoupon = subCouponService.getSubCouponByOrderId(orderId, s.getId());
					if(!StringUtils.isEmpty(subCoupon)){
						subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
						subCouponService.updateSubCoupon(subCoupon);
					}
				}
			}
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,json);
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	
	/**
	 * 支付方式
	 * @param payType
	 * @param ord
	 * @param tradeRecordService 
	 * @param request 
	 * @return
	 * @throws Exception 
	 */
	public JSONObject payType(String payType,Map<String,Object> ord,AccountService accountService,
			AccountTradeRecordService tradeRecordService, HttpServletRequest request) throws Exception{
		
		Map<String,Object> account = new Hashtable<String,Object>();
		
		String payp = "0";//第三方实际支付金额
		String userPrice = "0";//账户支付金额
		String amount = "0";//账户余额
		JSONObject json = new JSONObject();
		boolean state = false;//预下单状态 true成功 false失败
		
		String orderNum = ord.get("orderNo").toString();
		
		if(payType.equals(Orders.PAY_TYPE_ALIPAY+"")){
			//支付宝支付  更新账户余额信息
			account = accountPayOrder(ord, accountService,tradeRecordService);
			payp = account.get("payPrice").toString();//第三方实际支付金额
			userPrice = account.get("userPrice").toString();//账户支付金额
			amount = account.get("amount").toString();//账户余额
			
			if(Integer.parseInt(account.get("state").toString())==1&&Double.valueOf(payp)>0){
				 
				String body = Constants.BODY_ORDER;
				JSONObject alipay = Alipay.ydalipay(orderNum, payp, body,AlipayConfig.notify_url);
				json.element("alipay", alipay);
				state = true;
			}
		}else if(payType.equals(Orders.PAY_TYPE_WECHAT+"")){
			//微信支付  更新账户余额信息
			account = accountPayOrder(ord, accountService,tradeRecordService);
			payp = account.get("payPrice").toString();//第三方实际支付金额 单位:元
			
			userPrice = account.get("userPrice").toString();//账户支付金额
			amount = account.get("amount").toString();//账户余额
			
			if(Integer.parseInt(account.get("state").toString())==1&&Double.valueOf(payp)>0){
				
				String remoteAddress = request.getRemoteAddr();
				String body = Constants.BODY_ORDER;
				JSONObject wx = WeixinPay.wxpay(remoteAddress, orderNum, body, payp,Constant.JR_NOTIFY_URL);
				System.out.println("微信统一下单返回数据:"+wx);
				json.element("wx", wx);
				state = true;
			}
		} 
		json.element("state", state);
		json.element("amount", amount);
		json.element("userPrice", userPrice);//账户支付金额
		json.element("payp", payp);//第三方实际支付金额
		json.element("orderNo", orderNum);
		return json;
	}
	
	/**
	 * 账户 支付
	 * @param ord
	 * @param accountService
	 * @param tradeRecordService 
	 * @return
	 */
	public Map<String,Object> accountPayOrder(Map<String,Object> ord, AccountService accountService,
			AccountTradeRecordService tradeRecordService){
		Map<String,Object> param = new Hashtable<String,Object>();
		String id = ord.get("subId").toString();
		//实际支付总金额
		double actualFee = Double.valueOf(ord.get("actualFee").toString());
//		double allPrice = ordersDetail.getActualFee().doubleValue();//实际支付金额
		//第三方实际支付金额
		double payPrice = 0.00;
		//账户支付金额
		double userPrice = 0.00;
		//账户余额
		Account account = accountService.queryAccountBySubscriberId(id);
		Double amount = account.getAmount(); //账户总余额    amount=frozenAmount+usableAmount
		Double frozenAmount = account.getFrozenAmount();//账户冻结金额
		Double usableAmount = account.getUsableAmount();//账户可使用余额
		
		if(usableAmount >= actualFee){
			usableAmount = usableAmount - actualFee;
			amount = frozenAmount+usableAmount;
			payPrice = 0.00;
			userPrice = actualFee;
		}else{
			payPrice =  actualFee-usableAmount;
			userPrice = usableAmount;
			actualFee = usableAmount;
			usableAmount = 0.00;
			amount = frozenAmount+usableAmount;
		}
		param.put("payPrice", payPrice);//第三方实际支付金额
		param.put("userPrice", userPrice);//账户支付金额
		param.put("amount", amount);
		param.put("frozen_amount", frozenAmount);
		param.put("usable_amount", usableAmount);
		param.put("is_refund", 1);
		param.put("last_operate_amount", actualFee);
		param.put("last_operate_type", Account.TYPE_ORDER);
		param.put("subscriber_id", id);
		try {
			if(userPrice>0){
				//更新账户余额
				accountService.updateAccount(param);
				//  添加交易记录
				String description = "账户订单付款"+userPrice+"元";
				Map<String,Object> pam = new Hashtable<String,Object>();
				pam.put("subscriber_id",id);
				pam.put("type",Account.TYPE_ORDER);
				pam.put("pay_type",Account.PAY_TYPE_ACCOUNT);
				pam.put("pay_channel",Account.PAY_CHANNEL_APP);
				pam.put("amount",userPrice);
				pam.put("remaining_amount",amount);
				pam.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());
				pam.put("biz_id",ord.get("orderNo"));
				pam.put("sub_orderId",ord.get("runningDetailNo"));
				pam.put("is_preset_card",0);
				pam.put("description",description);
				pam.put("trade_time",id);
				pam.put("is_auto_clear",0);
				pam.put("result",1);
				pam.put("order_index",1);
				//添加交易记录
				tradeRecordService.addtradeRecord(pam);
			}
			param.put("state", 1);
		} catch (Exception e) {
			param.put("state", 0);
		}
		
		return param;
	}
	
	/**
	 * 同步通知 支付宝回调方法
	 * @param request
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @param dictService 
	 * @return
	 */
	public String tbalipayCallBack(HttpServletRequest request,
			PayOrderService payorderService, AccountTradeRecordService tradeRecordService,
			CarService carService) {
		String result = "fail";
		try {
			Map<String,Object> orderMap = Alipay.tbreturn(request);
			
			/*if(orderMap.get("state").toString().equals("ok")){
				result = "success";
				//支付成功后 更新订单状态 交易状态 车辆状态
				orderOver(orderMap, payorderService, tradeRecordService, carService);
			}*/
			if(orderMap.get("status").toString().equals("ok")){
				result = "success";
				//支付成功后 更新订单状态 交易状态 车辆状态
				orderOver(orderMap, payorderService, tradeRecordService, carService);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 支付成功后 更新订单状态 交易状态 车辆状态
	 * @param orderMap
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @return
	 */
	public boolean orderOver(Map<String,Object> orderMap,PayOrderService payorderService, 
			AccountTradeRecordService tradeRecordService,CarService carService){
		
		boolean t = false;
		
		try {
			String orderNo = orderMap.get("orderNo").toString();//订单号
			
			Orders ord =payorderService.orderByOrderNo(orderNo);
			if(ord==null){
				return t;
			}else{
				if(ord.getState().equals(Orders.STATE_ORDER_END)){
						//支付成功后 更新订单状态
						payorderService.updateOrderstate(orderNo);
						//支付成功后  更新交易状态
						tradeRecordService.updateRecordState(orderNo,ord.getTradeOrderNo());
						System.out.println("-------------------更改了订单状态");
					
					t = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			t = false;
		}
		return t;
	}
	
	/**
	 * 支付成功后 更新订单状态 交易状态 (新)
	 * @param orderMap
	 * @param payorderService
	 * @param tradeRecordService
	 */
	private void orderOverNew(Map<String,Object> orderMap,PayOrderService payorderService, 
			AccountTradeRecordService tradeRecordService, SubCouponService subCouponService){
		try {
			if((boolean) orderMap.get("alipay")){
				//支付宝调
				String outTradeNo = orderMap.get("outTradeNo").toString();//商户订单号
				AccountTradeRecord record = tradeRecordService.getAccountTradeRecordByTradeNo(outTradeNo);//交易记录
				Orders order = payorderService.orderByOrderNo(record.getBizId());//订单
				record.setResult(1);
				order.setTposPayFee(new BigDecimal(orderMap.get("totalAmount").toString()));
				order.setPayState(1);
				order.setPlatformTransactionId(orderMap.get("tradeNo").toString());
				order.setPlatformPayState(1);
				
				//更新表状态
				payorderService.updateOrder(order);
				tradeRecordService.updateAccountTradeRecord(record);
				
				//更新订单详情表的第三方支付金额
				ordersDetailMapper.updateOrderDetailTposPayFee(new BigDecimal(orderMap.get("totalAmount").toString()), order.getOrdersNo());
				
				//更新用户优惠卷状态
				SubCoupon subCoupon = subCouponService.getSubCouponByOrderId(order.getId(), order.getMemberId());
				if(!StringUtils.isEmpty(subCoupon)){
					subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
					subCouponService.updateSubCoupon(subCoupon);
				}
				System.out.println("-------------------更改了订单状态(新)");
			}else{
				//客户端调
				String outTradeNo = orderMap.get("outTradeNo").toString();//商户订单号
				AccountTradeRecord record = tradeRecordService.getAccountTradeRecordByTradeNo(outTradeNo);//交易记录
				Orders order = payorderService.orderByOrderNo(record.getBizId());//订单
				record.setResult(1);
				order.setTposPayFee(new BigDecimal(orderMap.get("totalAmount").toString()));
				order.setPayState(1);
				order.setPlatformPayState(1);
				
				payorderService.updateOrderResultAndTposPayFee(order);
				tradeRecordService.updateAccountTradeRecord(record);
				//更新订单详情表的第三方支付金额
				ordersDetailMapper.updateOrderDetailTposPayFee(new BigDecimal(orderMap.get("totalAmount").toString()), order.getOrdersNo());
				//更新用户优惠卷状态
				SubCoupon subCoupon = subCouponService.getSubCouponByOrderId(order.getId(), order.getMemberId());
				if(!StringUtils.isEmpty(subCoupon)){
					subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
					subCouponService.updateSubCoupon(subCoupon);
				}
				System.out.println("-------------------更改了订单状态(新)");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 异步通知 支付宝回调方法
	 * @param request
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @param dictService 
	 * @return
	 */
	public String alipayCallBack(HttpServletRequest request,
			PayOrderService payorderService, AccountTradeRecordService tradeRecordService,
			CarService carService) {
		String result = "fail";
		try {
			//获取支付宝POST过来反馈信息
			Map<String,String> params = new HashMap<String,String>();
			Map requestParams = request.getParameterMap();
			
			System.out.println("支付宝回调函数返回结果====="+requestParams.toString());
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
					: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
				params.put(name, valueStr);
			}
			
			System.out.println("params====="+params.toString());
//			String tradeStatus = params.get("trade_status");//获取支付宝返回结果状态码
			String outTradeNo = params.get("out_trade_no");//交易号
			
			
			String resultStatus = "";//app返回结果状态码
			
			if(params.containsKey("resultStatus")){
				 resultStatus = params.get("resultStatus");
			}
			
			String tradeStatus = "";//获取支付宝返回结果状态码
			
			if(params.containsKey("trade_status")){
				tradeStatus = params.get("trade_status");
				System.out.println("result====="+tradeStatus);
			}
			
			if(!StringUtils.isEmpty(tradeStatus)){
				
				if(tradeStatus.equals("TRADE_SUCCESS")){
					
					Map<String,Object> orderMap = new HashMap<String, Object>();
					orderMap.put("orderNo", outTradeNo);
					orderOver(orderMap, payorderService, tradeRecordService, carService);
					return "success";
				}
			}
			
			if(!StringUtils.isEmpty(resultStatus)){
				if(resultStatus.equals("9000")){
					Map<String,Object> orderMap = new HashMap<String, Object>();
					//客户端调此接口时，取值result
					String s = params.get("result");
					s = s.replace('&', ',').replace("=", ":");
					StringBuffer sb = new StringBuffer(s);
					sb.insert(0, "{").insert(sb.length(), "}");
					JSONObject json = JSONObject.fromObject(sb.toString());
					
					outTradeNo = json.get("out_trade_no").toString();
					orderMap.put("orderNo", outTradeNo);
					orderOver(orderMap, payorderService, tradeRecordService, carService);
					return "success";
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}
	
	/**
	 * 异步通知 支付宝回调方法（新）
	 * @param request
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @param dictService 
	 * @return
	 */
	public String alipayCallBackNew(HttpServletRequest request,
			PayOrderService payorderService, AccountTradeRecordService tradeRecordService,
			CarService carService, SubCouponService subCouponService) {
		String result = "fail";
		try {
			//获取支付宝POST过来反馈信息
			Map<String,String> params = new HashMap<String,String>();
			Map requestParams = request.getParameterMap();
			
			System.out.println("支付宝回调函数返回结果====="+requestParams.toString());
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
					: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
				params.put(name, valueStr);
			}
			
			System.out.println("params====="+params.toString());
			String outTradeNo = params.get("out_trade_no");//商户订单号
			String tradeNo = params.get("trade_no");//支付宝交易号
			String totalAmount = params.get("total_fee");//订单金额
			
			String resultStatus = "";//app返回结果状态码
			
			if(params.containsKey("resultStatus")){
				 resultStatus = params.get("resultStatus");
			}
			
			String tradeStatus = "";//获取支付宝返回结果状态码
			
			if(params.containsKey("trade_status")){
				tradeStatus = params.get("trade_status");
				System.out.println("result====="+tradeStatus);
			}
			
			if(!StringUtils.isEmpty(tradeStatus)){
				if(tradeStatus.equals("TRADE_SUCCESS")){
					Map<String,Object> orderMap = new HashMap<String, Object>();
					orderMap.put("outTradeNo", outTradeNo);
					orderMap.put("tradeNo", tradeNo);
					orderMap.put("totalAmount", Double.valueOf(totalAmount));
					orderMap.put("alipay", true);
					orderOverNew(orderMap, payorderService, tradeRecordService, subCouponService);
					return "success";
				}
			}
			
			if(!StringUtils.isEmpty(resultStatus)){
				if(resultStatus.equals("9000")){
					Map<String,Object> orderMap = new HashMap<String, Object>();
					orderMap.put("tradeNo", tradeNo);
					
					//客户端调此接口时，取值result
					String s = params.get("result");
					s = s.replace('&', ',').replace("=", ":");
					StringBuffer sb = new StringBuffer(s);
					sb.insert(0, "{").insert(sb.length(), "}");
					JSONObject json = JSONObject.fromObject(sb.toString());
					
					outTradeNo = json.get("out_trade_no").toString();
					totalAmount = json.get("total_fee").toString();
					orderMap.put("outTradeNo", outTradeNo);
					orderMap.put("totalAmount", Double.valueOf(totalAmount));
					orderMap.put("alipay", false);
					orderOverNew(orderMap, payorderService, tradeRecordService, subCouponService);
					return "success";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = "fail";
		}
		return result;
	}
	
	/**
	 * 异步通知 微信回调方法
	 * @param request
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @return
	 */
	public String wxCallBack(HttpServletRequest request,
			PayOrderService payorderService, AccountTradeRecordService tradeRecordService,
			CarService carService) {
		 Map<String,String> map = new HashMap<String,String>();
		try {
			Map<String,Object> orderMap = WeixinPay.callBackUrl(request);
			if(orderMap.get("status").toString().equals("ok")){
				map.put("return_code", orderMap.get("return_code").toString());
				map.put("return_msg", orderMap.get("return_msg").toString());
				//将微信订单号存入数据库
				String transactionId = orderMap.get("tradeRecordNo").toString();
				String orderNo = orderMap.get("orderNo").toString();
				payOrderMapper.updateOrderTransactionId(transactionId, orderNo);
				
				//支付成功后 更新订单状态 交易状态 车辆状态
				orderOver(orderMap, payorderService, tradeRecordService, carService);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String result = HttpTool.mapToXml(map);
		System.out.println("微信异步校验支付成功后返回结果==="+result);
		return result;
	}
	
	/**
	 * 异步通知 微信回调方法(新)
	 * @param request
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @return
	 */
	public String wxCallBackNew(HttpServletRequest request,
			PayOrderService payorderService, AccountTradeRecordService tradeRecordService,
			CarService carService) {
		Map<String,String> map = new HashMap<String,String>();
		try {
			Map<String,Object> orderMap = WeixinPay.callBackUrlNew(request);
			System.out.println("微信回调通知参数：" + orderMap.toString());
			if(orderMap.get("status").toString().equals("ok")){
				map.put("return_code", orderMap.get("return_code").toString());
				map.put("return_msg", orderMap.get("return_msg").toString());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return HttpTool.mapToXml(map);
	}
	
	/**
	 * 调取微信查询接口，确认支付结果
	 * @param orderNo
	 * @return 1:成功并且金额正确 2:查询成功但金额错误 3:查询失败 4:根据订单号未查询到订单信息
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	@Override
	public int confirmTradeResults(String orderNo, PayOrderService payorderService, 
								   AccountTradeRecordService accountTradeRecordService) throws JDOMException, IOException {
		Orders order = payOrderMapper.queryOrderByOrderNo(orderNo);
		if(order == null){
			return 4;
		}
		BigDecimal tposPayFee = order.getTposPayFee();
		String transactionId = order.getPlatformTransactionId();
		JSONObject wx = WeixinPay.confirmTradeResults(transactionId);
		if(wx.get("result") == null || "code_error".equals(wx.get("result"))){
			return 3;
		} else {
			JSONObject result = (JSONObject) wx.get("result");
			Double fee = ArithUtil.mul(Double.valueOf(tposPayFee.toString()),100); 
			Double totalFee = Double.valueOf(result.get("totalFee").toString());
			if("SUCCESS".equals(result.get("tradeState")) && fee.equals(totalFee)){
				return 1;
			}
			if("SUCCESS".equals(result.get("tradeState")) && !fee.equals(totalFee)){
				return 2;
			}
		}
		return 3;
	}
	
	/**
	 * 调取微信查询接口(根据商户订单号查询)，确认支付结果（新）
	 * @param orderNo
	 * @return 1:成功并且金额正确 2:查询成功但金额错误 3:查询失败 4:根据订单号未查询到订单信息
	 * @throws IOException 
	 * @throws JDOMException 
	 */
	@Override
	public int confirmTradeResultsNew(String orderNo, PayOrderService payorderService, 
								   AccountTradeRecordService accountTradeRecordService, 
								   SubCouponService subCouponService) throws Exception {
		try {
			Orders order = payOrderMapper.queryOrderByTradeNo(orderNo);
			if(order == null){
				return 4;
			}
			BigDecimal tposPayFee = order.getTposPayFee();
			JSONObject wx = WeixinPay.orderQuery(orderNo);
			if(wx.get("result") == null || "code_error".equals(wx.get("result"))){
				return 3;
			} else {
				JSONObject result = (JSONObject) wx.get("result");
				Double fee = Double.valueOf(tposPayFee.toString()); 
				Double totalFee = Double.valueOf(result.get("totalFee").toString());
				totalFee = Double.valueOf(new DecimalFormat("###.00").format(totalFee / 100));
				if("SUCCESS".equals(result.get("tradeState")) && fee.equals(totalFee)){
					//更新表状态
					Map<String, Object> orderMap = new HashMap<>();
					orderMap.put("outTradeNo", result.get("outTradeNo"));
					orderMap.put("tradeNo", result.get("transactionId"));
//					double totalAmount = FeeUtils.fenToYuan(Integer.parseInt(result.get("totalFee").toString()));
					orderMap.put("totalAmount", totalFee);
					orderMap.put("alipay", true);//配合公共方法的参数,没有实际意义
					orderOverNew(orderMap, payorderService, accountTradeRecordService, subCouponService);
					return 1;
				}
				if("SUCCESS".equals(result.get("tradeState")) && !fee.equals(totalFee)){
					return 2;
				}
			}
			return 3;
		} catch (Exception e) {
			return 3;
		}
	}
	
	/**
	 * 支付宝统一收单线下交易查询
	 * @param orderNum
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public String gateWay(String dateTime) throws Exception{
		
		String result ="";
		AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",AlipayConfig.GATEWAY_APP_ID,AlipayConfig.private_key,"json","utf-8",AlipayConfig.ali_public_key,AlipayConfig.sign_type);
		AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
		request.setBizContent("{" +
				"\"bill_type\":\"trade\"," +
				"\"bill_date\":\""+dateTime+"\"}");
		
		AlipayDataDataserviceBillDownloadurlQueryResponse response = alipayClient.execute(request);
		System.out.println(response.getBillDownloadUrl());
		
		Map<String,Object> rm = new HashMap<String,Object>();
		
		if(response.isSuccess()){
			rm.put("bill_download_url", response.getBillDownloadUrl());
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,rm);
		}else{
			rm.put("msg", response.getMsg());
			result = Ajax.AppResult(Constants.APP_SUCCESS, response.getErrorCode(),rm);
		} 
		return result;
	}
	
	/**
	 * 微信查询确认支付成功后，更改订单状态为已支付
	 * @param tradeRecordService
	 */
	@Override
	public void updateOrderStateToAlreadyPaid(AccountTradeRecordService tradeRecordService, String orderNo) {
		payOrderMapper.updateOrderStateToAlreadyPaid(orderNo);
		tradeRecordService.updateTradeRecordStateToAlreadyPaid(orderNo);
	}
	
	/**
	 * 微信查询失败或金额不对，修改订单状态为未支付
	 */
	@Override
	public void updateOrderStateToUnPaid(AccountTradeRecordService tradeRecordService, String orderNo) {
		payOrderMapper.updateOrderStateToUnPaid(orderNo);
		tradeRecordService.updateOrderStateToUnPaid(orderNo);
	}
	
	/**
	 * 订单支付（新）
	 */
	@Override
	public String payOrder(HttpServletRequest request, OrderService orderService, AccountTradeRecordService tradeRecordService,
							OrdersDetailService ordersDetailService, CouponService couponService, SubCouponService subCouponService) {
		String result = null;//方法返回结果
		Map<String, Object> res = new Hashtable<String, Object>();
		try {
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String subId = s.getId();//"8c183c5c486811e798ec00163e000d59";
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			String orderId = pmap.get("orderId");//"52e15bd075c011e795d100163e0007f1";
			String payType = pmap.get("payType");//"5";//1账户支付   2信用卡支付   3支付宝   4银联 5微信   
			if("error".equals(pmap.get("param"))){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			if("".equals(orderId) || orderId == null){
				result = Ajax.AppResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG,res);
				return result;
			}
			if("".equals(payType) || payType == null){
				result = Ajax.AppResult(Constants.APP_PAYTYPE_NULL, Constants.APP_PAYTYPE_NULL_MSG,res);
				return result;
			}
			
			//获取订单信息
			Orders ord =orderService.orderInfoById(orderId);
			
			//订单不存在
			if(ord == null){
				result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
				return result;
			}
			//未还车
			if(!ord.getState().equals(Orders.STATE_ORDER_END+"")){
				result = Ajax.AppResult(Constants.APP_NO_BACK_CAR, Constants.APP_NO_BACK_CAR_MSG,res);
				return result;
			}
			//用户订单匹配错误
			if(!ord.getMemberId().equals(subId)){
				result = Ajax.AppResult(Constants.APP_ORDER_USER_E, Constants.APP_ORDER_USER_E_MSG,res);
				return result;
			}
			
			//如果订单表状态为已支付
			if(ord.getPayState().toString().equals(Orders.STATE_ORDER_PAY)){
				//根据订单号修改最新交易记录状态为已支付
				tradeRecordService.updateTradeRecordNew(ord.getOrdersNo());
				return Ajax.AppResult(Constants.APP_ORDER_PAY, Constants.APP_ORDER_PAY_MSG,res);
			}
			
			//获取订单详情
			OrdersDetail orderDetail = ordersDetailService.getByOrderIdDetail(orderId);
			
			int totalTimePrice = FeeUtils.yuanToFen(orderDetail.getTimeFee().floatValue());//总时长费
			int totalMilePrice = FeeUtils.yuanToFen(orderDetail.getMileFee().floatValue());//总里程费
			
			//获取优惠卷
			Coupon coupon = couponService.getCouonByOrderId(orderId);
			int couponFee = 0;
			if(coupon != null){
				couponFee = FeeUtils.yuanToFen(coupon.getPrice().floatValue());//优惠券金额
			}
			float couponPrice = FeeUtils.fenToYuan(couponFee);
			//查询该订单保险单价
			int insuranceFee = ordersDetailService.getInsuranceFee(orderId);
			
			//保险总金额
			int totalInsurancePrice = FeesAlg.insurancePrice(ord.getBeginTime(), ord.getEndTime(), insuranceFee, false);
			
			//计算第三方应付金额
			Map<String, Integer> calculateFeesWithCoupon = FeesAlgWithCoupon.calculateFeesWithCoupon(totalTimePrice, totalMilePrice, totalInsurancePrice, CouponType.COUPON_NORMAL, couponFee, false, 0, true);
			Integer actualFee = calculateFeesWithCoupon.get("actualFee");//实际应付
			Integer cashPayment = calculateFeesWithCoupon.get("cashPayment");//第三方应付
			
			//如果订单状态为未支付，根据订单号查交易记录表最新一条
			AccountTradeRecord accountTradeRecord = tradeRecordService.getTradeRecordNew(ord.getOrdersNo());
			Integer type = Integer.parseInt(payType);//客户端传来的支付方式
			if(accountTradeRecord != null && accountTradeRecord.getTradeOrderNo() != null){
				//记录存在，根据商户订单号去第三方校验
				String tradeOrderNo = accountTradeRecord.getTradeOrderNo();//商户订单号
				JSONObject orderQuery = orderQuery(accountTradeRecord.getPayType(), tradeOrderNo);
				int localAmount = FeeUtils.yuanToFen(accountTradeRecord.getAmount().floatValue());
				if("SUCCESS".equals(orderQuery.get("tradeState"))
					&& localAmount == Integer.parseInt(orderQuery.get("totalFee").toString())){
					//如果校验结果为已支付，修改表状态
					accountTradeRecord.setResult(1);
					tradeRecordService.updateAccountTradeRecord(accountTradeRecord);
					ord.setPayState(1);
					orderService.updateOrder(ord);
					return Ajax.AppResult(Constants.APP_ORDER_PAY, Constants.APP_ORDER_PAY_MSG,res);
				}else{
					//校验结果为未支付，创建新的交易记录，拿生成的商户订单号去支付
					String tradeOrderNoNew = createAccountTradeRecord(actualFee, ord, type, tradeRecordService);
					String remoteAddress = request.getRemoteAddr();//IP地址
					JSONObject orderPayJson = orderPay(cashPayment, tradeOrderNoNew, type, remoteAddress);
					
					float tposPayFee = FeeUtils.fenToYuan(cashPayment);
					//更新订单表优惠券金额,交易记录号,三方支付金额
					orderMapper.updateOrderCouponAndTradeNo(orderId, couponPrice, tradeOrderNoNew, tposPayFee);
					//更新订单详情表优惠券金额,三方支付金额
					ordersDetailMapper.updateOrderDetailCoupon(orderDetail.getId(), couponPrice, tposPayFee);
					result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,orderPayJson);
				}
			}else{
				//记录不存在，创建新的交易记录，拿生成的商户订单号去支付
				String tradeOrderNoNew = createAccountTradeRecord(actualFee, ord, type, tradeRecordService);
				String remoteAddress = request.getRemoteAddr();//IP地址
				JSONObject orderPayJson = orderPay(cashPayment, tradeOrderNoNew, type, remoteAddress);
				
				float tposPayFee = FeeUtils.fenToYuan(cashPayment);
				//更新订单表优惠券金额,交易记录号,三方支付金额
				orderMapper.updateOrderCouponAndTradeNo(orderId, couponPrice, tradeOrderNoNew, tposPayFee);
				//更新订单详情表优惠券金额,三方支付金额
				ordersDetailMapper.updateOrderDetailCoupon(orderDetail.getId(), couponPrice, tposPayFee);
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,orderPayJson);
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	
	/**
	 * 去第三方平台查询支付结果
	 * @author Jin Guangyu
	 * @date 2017年7月26日下午4:56:16
	 * @param payType
	 * @param tradeOrderNo
	 * @return
	 */
	public JSONObject orderQuery(Integer payType, String tradeOrderNo){
		JSONObject jsonObject = new JSONObject();
		try {
			if(Orders.PAY_TYPE_ALIPAY.equals(payType)){
				//支付宝,暂时不查
			}
			if(Orders.PAY_TYPE_WECHAT.equals(payType)){
				//微信
				JSONObject orderQueryJson = WeixinPay.orderQuery(tradeOrderNo);
				JSONObject result = orderQueryJson.fromObject(orderQueryJson.get("result"));
				if("SUCCESS".equals(result.get("tradeState"))){
					jsonObject.put("tradeState", "SUCCESS");
				}
				jsonObject.put("totalFee", result.get("totalFee"));
				jsonObject.put("tradeNo", result.get("transactionId"));//微信订单号
				jsonObject.put("outTradeNo", result.get("outTradeNo"));//商户订单号
				jsonObject.put("queryState", "SUCCESS");
			}
		} catch (Exception e) {
			jsonObject.put("queryState", "FAIL");
		}
		return jsonObject;
	}
	
	/**
	 * 创建新的交易记录
	 * @author Jin Guangyu
	 * @date 2017年7月28日下午2:57:55
	 * @param actualFee
	 * @param order
	 * @param payType
	 * @param accountTradeRecordService
	 * @return
	 */
	private String createAccountTradeRecord(Integer actualFee, Orders order, Integer payType, AccountTradeRecordService accountTradeRecordService){
		float userPrice = FeeUtils.fenToYuan(actualFee);
		String description = "";
		if(Orders.PAY_TYPE_ALIPAY.equals(payType)){
			description = "支付宝订单付款" + userPrice + "元";
		}else if(Orders.PAY_TYPE_WECHAT.equals(payType)){
			description = "微信订单付款" + userPrice + "元";
		}
		Map<String,Object> pam = new Hashtable<String,Object>();
		pam.put("subscriber_id",order.getMemberId());
		pam.put("type",Account.TYPE_ORDER);
		pam.put("pay_type",payType);
		pam.put("pay_channel",Account.PAY_CHANNEL_APP);
		pam.put("amount",userPrice);
		pam.put("remaining_amount",0);//剩下的账户余额
		pam.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());
		pam.put("biz_id",order.getOrdersNo());
		pam.put("sub_orderId",order.getRunningDetailNo());
		pam.put("is_preset_card",0);
		pam.put("description",description);
		pam.put("trade_time","");//sql中会插入NOW()
		pam.put("is_auto_clear",0);
		pam.put("result",0);
		pam.put("order_index",1);
		//添加交易记录
		accountTradeRecordService.addtradeRecord(pam);
		
		return (String) pam.get("trade_order_no");
	}
	
	/**
	 * 去第三方平台支付
	 * @author Jin Guangyu
	 * @date 2017年7月28日下午4:08:31
	 * @param cashPayment 第三方应付金额
	 * @param tradeOrderNoNew 商户订单号
	 * @param type 平台类型
	 */
	private JSONObject orderPay(Integer cashPayment, String tradeOrderNoNew, Integer payType, String remoteAddress) throws Exception {
		JSONObject json = new JSONObject();
		Float totalAmount = FeeUtils.fenToYuan(cashPayment);
		if (Orders.PAY_TYPE_ALIPAY.equals(payType)) {
			//支付宝支付
			JSONObject alipayJson = Alipay.ydalipayNew(tradeOrderNoNew, totalAmount.toString(), Constants.BODY_ORDER, AlipayConfig.notify_url_new);
			json.element("alipay", alipayJson);
			json.element("payp", totalAmount);//第三方支付金额
			return json;
		} else if (Orders.PAY_TYPE_WECHAT.equals(payType)) {
			//微信支付
			JSONObject wxJsonObject = WeixinPay.wxpayNew(remoteAddress, tradeOrderNoNew, Constants.BODY_ORDER, cashPayment.toString(),Constant.JR_NOTIFY_URL_NEW);
			json.element("orderNo", tradeOrderNoNew);//把商户订单号传给客户端，以便后续查询接口使用
			json.element("payp", totalAmount);//第三方支付金额
			json.element("wx", wxJsonObject);
			return json;
		}
		return json;
	}
	
	public static void main(String[] args) {
		String s = "partner=\"0.01\"&seller_id=\"letuweixin@163.com\"";
		s = s.replace('&', ',').replace("=", ":");
		StringBuffer sb = new StringBuffer(s);
		sb.insert(0, "{").insert(sb.length(), "}");
		JSONObject json = JSONObject.fromObject(sb.toString());
		Double object = Double.valueOf(json.get("partner").toString());
		
		System.out.println(object);
	}
	
}






















