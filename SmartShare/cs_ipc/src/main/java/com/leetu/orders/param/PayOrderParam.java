package com.leetu.orders.param;

import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.StyledEditorKit.BoldAction;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.service.AccountService;
import com.leetu.account.service.AccountTradeRecordService;
import com.leetu.account.util.TradeRecordNo;
import com.leetu.car.service.CarService;
import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.entity.SubCoupon;
import com.leetu.coupon.service.CouponService;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.service.PayOrderService;
import com.leetu.orders.util.OrderFee;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.Constants;
import com.util.DateUtil;
import com.util.JsonTools;
import com.util.TokenUtils;
import com.util.ToolDateTime;
import com.util.alipay.config.AlipayConfig;
import com.util.alipay.pay.Alipay;
import com.util.car.RedisCar;
import com.util.weixin.Constant;
import com.util.weixin.pay.pay.WeixinPay;
import com.util.weixin.tool.HttpTool;

public class PayOrderParam {
	
	/**
	 * 支付订单
	 * @param request
	 * @param scriberService 
	 * @param payorderService
	 * @param orderService
	 * @param dictService 
	 * @param accountService 
	 * @param tradeRecordService 
	 * @param carService 
	 * @return
	 */
	public static String payorder(HttpServletRequest request,SubScriberService scriberService,
			PayOrderService payorderService, OrderService orderService, 
			DictService dictService,AccountTradeRecordService tradeRecordService, 
			AccountService accountService, CarService carService,
			OrdersDetailService ordersDetailService,CouponService couponService,SubCouponService subCouponService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();	
		try {
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			String orderId = pmap.get("orderId");
//			String orderPrice = pmap.get("orderPrice");
//			String backdotId = pmap.get("dotId");
			String payType = pmap.get("payType");//1账户支付   2信用卡支付   3支付宝   4银联 5微信      
			/*if(StringUtils.isBlank(payType)){
				
			}*/
			if(orderId==""){
				result = Ajax.AppResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG,res);
				return result;
			}
			if(payType==null){
				result = Ajax.AppResult(Constants.APP_PAYTYPE_NULL, Constants.APP_PAYTYPE_NULL_MSG,res);
				return result;
			}
			Orders ord =orderService.orderInfoById(orderId);
			if(ord==null){
				result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
				return result;
			}
			
			if(!ord.getState().equals(Orders.STATE_ORDER_END+"")){
				result = Ajax.AppResult(Constants.APP_NO_BACK_CAR, Constants.APP_NO_BACK_CAR_MSG,res);
				return result;
			}
			
			if(ord.getPayState().equals(Orders.STATE_ORDER_PAY+"")){
				result = Ajax.AppResult(Constants.APP_ORDER_PAY, Constants.APP_ORDER_PAY_MSG,res);
				return result;
			}
			if(!ord.getMemberId().equals(id)){
				result = Ajax.AppResult(Constants.APP_ORDER_USER_E, Constants.APP_ORDER_USER_E_MSG,res);
				return result;
			}
			
			List<AccountTradeRecord> accountTradeRecord = tradeRecordService.queryOrderPayState(ord.getOrdersNo());
			
			if(accountTradeRecord != null && accountTradeRecord.size() > 0){
				result = Ajax.AppResult(Constants.APP_ORDER_PAY, Constants.APP_ORDER_PAY_MSG,res);
				return result;
			}
			
			String dotId = ord.getEndSiteId();//还车网点id
			
			double actual_fee  = 0.00;//实际消费金额
			double payPrice = 0.00;//第三方实际支付金额
			double userPrice  = 0.00;//账户支付金额
			double coupon_fee  = 0.00;//优惠券支付金额
			
			double remainingAmount = 0.00;//账户剩余余额
			
			//获取优惠卷
			Coupon coupon = couponService.getCouonByOrderId(ord.getId());
			
			//获取订单费用信息
			Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
			Date endTime = (Date) orderfee.get("endTime");//订单结束时间
//			double allPrice = Double.valueOf(orderfee.get("allPrice").toString());//总费用
			double allPrice =  (Double) orderfee.get("allPrice");//总费用
//			double minConsumption = Double.valueOf(orderfee.get("minConsumption").toString());//最低消费金额
			double allMile =  Double.valueOf(orderfee.get("allMile").toString());
			double allMilePrice = Double.valueOf(orderfee.get("allMilePrice").toString());//里程消费
			double allTimePrice = Double.valueOf(orderfee.get("allTimePrice").toString());//时长消费
			//判断当前消费金额是否小于最低消费金额
//			if(allPrice<minConsumption){
//				allPrice=minConsumption;
//			}
			
			//调用支付 方法
			JSONObject json = payType(payType,orderfee,accountService,tradeRecordService,request);
			
			System.out.println("调用支付方法返回json："+json);
			
			Map<?, Map<?, ?>> wxJson = (Map<?, Map<?, ?>>)json.get("wx");
			
			payPrice = Double.valueOf(json.get("payp").toString());
			userPrice = Double.valueOf(json.get("userPrice").toString());
			remainingAmount = Double.valueOf(json.get("amount").toString());
			coupon_fee = payPrice+userPrice;
			
//			System.out.println(json.get("result"));
			//判断第三方实际支付金额是否为0 是：用账户支付 否：用微信或支付宝支付
			if(payPrice==0){
				 
				payType = Orders.PAY_TYPE_ACCOUNT.toString();//账户支付
				boolean resState = json.getBoolean("state");
				if(resState){
					Map<String,Object> order = new Hashtable<String,Object>();
//					order.put("endTime", ToolDateTime.parse(endTime,ToolDateTime.pattern_ymd_hms));
					order.put("endTime", endTime);
					order.put("orderId", ord.getId());
					order.put("end_site_id", dotId);
					order.put("orders_duration", orderfee.get("allminutes"));
					order.put("total_fee", allPrice);
					order.put("actual_fee", actual_fee);
					order.put("tpos_pay_fee", payPrice);
					order.put("use_balance", userPrice);
					order.put("coupon_fee", 0);
					order.put("end_mileage", orderfee.get("endmile"));
					order.put("state", Orders.STATE_ORDER_END);
					order.put("channel", Orders.PAY_CHANNEL_APP);
					order.put("allMile", allMile);
					order.put("allMilePrice", allMilePrice);
					order.put("allTimePrice", allTimePrice);
					order.put("payType", Orders.PAY_TYPE_ACCOUNT);
					order.put("is_paid", 1);
					order.put("is_over", 1);
					order.put("is_running", 1);
					order.put("trade_order_no", TradeRecordNo.getPayOrderTradeNo());//交易单号
					//更新订单信息
					payorderService.updateOrderInfo(order);
					
					//更新用户优惠卷状态
					SubCoupon subCoupon = subCouponService.getSubCouponByOrderId(orderId, s.getId());
					subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_YES);
					
					subCouponService.updateSubCoupon(subCoupon);
				}  
				
			}else{ 
				
				boolean resState = json.getBoolean("state");
				if(resState){
					//预支付成功后   1  添加交易信息 2更新订单信息 
					String description = "订单付款"+payPrice+"元";
					if(String.valueOf(Orders.PAY_TYPE_ALIPAY).equals(payType+"")){
						description = "支付宝订单付款"+payPrice+"元";
					}else if(String.valueOf(Orders.PAY_TYPE_WECHAT).equals(payType+"")){
						description = "微信订单付款"+payPrice+"元";
					}
					
					//订单交易记录数据
					Map<String,Object> param = new Hashtable<String,Object>();
					param.put("subscriber_id",id);
					param.put("type",Account.TYPE_ORDER);
					param.put("pay_type",payType);//支付类型
					param.put("pay_channel",Account.PAY_CHANNEL_APP);//渠道
					param.put("amount",payPrice);//交易金额
					param.put("remaining_amount",remainingAmount);//账户余额
//					param.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());//交易单号
					param.put("biz_id",ord.getOrdersNo());//订单号
					param.put("sub_orderId",ord.getRunningDetailNo());//字订单
					param.put("is_preset_card",0);
					param.put("description",description);
					param.put("trade_time","t");
					param.put("is_auto_clear",0);
					param.put("result",0);
					param.put("order_index",1);
					
					//订单数据
					Map<String,Object> order = new Hashtable<String,Object>();
//					order.put("endTime", ToolDateTime.parse(endTime,ToolDateTime.pattern_ymd_hms));
					order.put("endTime", endTime);
					order.put("orderId", ord.getId());//订单id
					order.put("end_site_id", dotId);//实际还车网点
					order.put("orders_duration", orderfee.get("allminutes"));//总时长 单位：分钟 
					order.put("total_fee", allPrice);//总费用
					order.put("actual_fee", actual_fee);//实际费用
					order.put("tpos_pay_fee", payPrice);//第三方实际支付费用
					order.put("use_balance", userPrice);//账户支付费用
					order.put("coupon_fee", 0);//优惠券使用金额
					order.put("end_mileage", orderfee.get("endmile"));//结束里程
//					order.put("state", Orders.STATE_ORDER_START);//订单状态
					order.put("channel", Orders.PAY_CHANNEL_APP);
					order.put("allMile", allMile);
					order.put("allMilePrice", allMilePrice);
					order.put("allTimePrice", allTimePrice);
					order.put("payType", payType);
					order.put("is_paid", 0);
					order.put("is_over", 0);
					order.put("is_running", 1);
					order.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());//交易单号
					if(wxJson !=null && !"".equals(wxJson)){
						//判断微信统一下单是否订单号重复，如果订单号重复，视为已经支付，修改订单状态
						if(String.valueOf(wxJson.get("order").get("result_code")).equals("FAIL") || String.valueOf(wxJson.get("order").get("err_code")).equals("INVALID_REQUEST") || String.valueOf(wxJson.get("order").get("err_code")).equals("ORDERPAID")){
							//支付成功后 更新订单状态
							payorderService.updateOrderstate(ord.getOrdersNo());
							
							/*Orders ordInfo =orderService.orderInfoById(ord.getId());
							param.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());*/
							//支付成功后  更新交易状态
							tradeRecordService.updateRecordState(ord.getOrdersNo(),ord.getTradeOrderNo());
						}else{
							//更新订单信息
							payorderService.updateOrderInfo(order);
							Orders ordInfo =orderService.orderInfoById(ord.getId());
							param.put("trade_order_no",ordInfo.getTradeOrderNo());
							//添加交易记录
							tradeRecordService.addtradeRecord(param);
							
							
						}
					}else{
						//更新订单信息
						payorderService.updateOrderInfo(order);
						
						Orders ordInfo =orderService.orderInfoById(ord.getId());
						param.put("trade_order_no",ordInfo.getTradeOrderNo());
						
						tradeRecordService.addtradeRecord(param);
						
					}
				}
			}
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,json);
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	/**
	 * 支付方式
	 * @param payType
	 * @param ord
	 * @param tradeRecordService 
	 * @param request 
	 * @return
	 * @throws Exception 
	 */
	public static JSONObject payType(String payType,Map<String,Object> ord,AccountService accountService,
			AccountTradeRecordService tradeRecordService, HttpServletRequest request) throws Exception{
		
		Map<String,Object> account = new Hashtable<String,Object>();
		
		String payp = "0";//第三方实际支付金额
		String userPrice = "0";//账户支付金额
		String amount = "0";//账户余额
		JSONObject json = new JSONObject();
		boolean state = false;//预下单状态 true成功 false失败
		
		String orderNum = ord.get("orderNo").toString();
		
		if(payType.equals(Orders.PAY_TYPE_ALIPAY+"")){
			//支付宝支付  更新账户余额信息
			account = accountPayOrder(ord, accountService,tradeRecordService);
			payp = account.get("payPrice").toString();//第三方实际支付金额
			userPrice = account.get("userPrice").toString();//账户支付金额
			amount = account.get("amount").toString();//账户余额
			
			if(Integer.parseInt(account.get("state").toString())==1&&Double.valueOf(payp)>0){
				 
				String body = Constants.BODY_ORDER;
				JSONObject alipay = Alipay.ydalipay(orderNum, payp, body,AlipayConfig.notify_url);
				json.element("alipay", alipay);
				state = true;
			}
		}else if(payType.equals(Orders.PAY_TYPE_WECHAT+"")){
			//微信支付  更新账户余额信息
			account = accountPayOrder(ord, accountService,tradeRecordService);
			payp = account.get("payPrice").toString();//第三方实际支付金额 单位:元
			
			userPrice = account.get("userPrice").toString();//账户支付金额
			amount = account.get("amount").toString();//账户余额
			
			if(Integer.parseInt(account.get("state").toString())==1&&Double.valueOf(payp)>0){
				
				String remoteAddress = request.getRemoteAddr();
				String body = Constants.BODY_ORDER;
				JSONObject wx = WeixinPay.wxpay(remoteAddress, orderNum, body, payp,Constant.JR_NOTIFY_URL);
				System.out.println("微信统一下单返回数据:"+wx);
				json.element("wx", wx);
				state = true;
			}
		} 
		json.element("state", state);
		json.element("amount", amount);
		json.element("userPrice", userPrice);//账户支付金额
		json.element("payp", payp);//第三方实际支付金额
		return json;
	}

 
	/**
	 * 账户 支付
	 * @param ord
	 * @param accountService
	 * @param tradeRecordService 
	 * @return
	 */
	public static Map<String,Object> accountPayOrder(Map<String,Object> ord, AccountService accountService,
			AccountTradeRecordService tradeRecordService){
		Map<String,Object> param = new Hashtable<String,Object>();
		String id = ord.get("subId").toString();
		//支付总金额
		double allPrice = Double.valueOf(ord.get("allPrice").toString());
		//第三方实际支付金额
		double payPrice = 0.00;
		//账户支付金额
		double userPrice = 0.00;
		//账户余额
		Account account = accountService.queryAccountBySubscriberId(id);
		Double amount = account.getAmount(); //账户总余额    amount=frozenAmount+usableAmount
		Double frozenAmount = account.getFrozenAmount();//账户冻结金额
		Double usableAmount = account.getUsableAmount();//账户可使用余额
		
		if(usableAmount >= allPrice){
			usableAmount = usableAmount - allPrice;
			amount = frozenAmount+usableAmount;
			payPrice = 0.00;
			userPrice = allPrice;
		}else{
			payPrice =  allPrice-usableAmount;
			userPrice = usableAmount;
			allPrice = usableAmount;
			usableAmount = 0.00;
			amount = frozenAmount+usableAmount;
		}
		param.put("payPrice", payPrice);//第三方实际支付金额
		param.put("userPrice", userPrice);//账户支付金额
		param.put("amount", amount);
		param.put("frozen_amount", frozenAmount);
		param.put("usable_amount", usableAmount);
		param.put("is_refund", 1);
		param.put("last_operate_amount", allPrice);
		param.put("last_operate_type", Account.TYPE_ORDER);
		param.put("subscriber_id", id);
		try {
			if(userPrice>0){
				//更新账户余额
				accountService.updateAccount(param);
				//  添加交易记录
				String description = "账户订单付款"+userPrice+"元";
				Map<String,Object> pam = new Hashtable<String,Object>();
				pam.put("subscriber_id",id);
				pam.put("type",Account.TYPE_ORDER);
				pam.put("pay_type",Account.PAY_TYPE_ACCOUNT);
				pam.put("pay_channel",Account.PAY_CHANNEL_APP);
				pam.put("amount",userPrice);
				pam.put("remaining_amount",amount);
				pam.put("trade_order_no",TradeRecordNo.getPayOrderTradeNo());
				pam.put("biz_id",ord.get("orderNo"));
				pam.put("sub_orderId",ord.get("runningDetailNo"));
				pam.put("is_preset_card",0);
				pam.put("description",description);
				pam.put("trade_time",id);
				pam.put("is_auto_clear",0);
				pam.put("result",1);
				pam.put("order_index",1);
				//添加交易记录
				tradeRecordService.addtradeRecord(pam);
			}
			param.put("state", 1);
		} catch (Exception e) {
			param.put("state", 0);
		}
		
		return param;
	}
	
	
	/**
	 * 同步通知 支付宝回调方法
	 * @param request
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @param dictService 
	 * @return
	 */
	public static String tbalipayCallBack(HttpServletRequest request,
			PayOrderService payorderService, AccountTradeRecordService tradeRecordService,
			CarService carService) {
		String result = "fail";
		try {
			Map<String,Object> orderMap = Alipay.tbreturn(request);
			
			/*if(orderMap.get("state").toString().equals("ok")){
				result = "success";
				//支付成功后 更新订单状态 交易状态 车辆状态
				orderOver(orderMap, payorderService, tradeRecordService, carService);
			}*/
			if(orderMap.get("status").toString().equals("ok")){
				result = "success";
				//支付成功后 更新订单状态 交易状态 车辆状态
				orderOver(orderMap, payorderService, tradeRecordService, carService);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	/**
	 * 异步通知 支付宝回调方法
	 * @param request
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @param dictService 
	 * @return
	 */
	public static String alipayCallBack(HttpServletRequest request,
			PayOrderService payorderService, AccountTradeRecordService tradeRecordService,
			CarService carService) {
		String result = "fail";
		try {
			//获取支付宝POST过来反馈信息
			Map<String,String> params = new HashMap<String,String>();
			Map requestParams = request.getParameterMap();
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
					: valueStr + values[i] + ",";
				}
				//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
				//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
				params.put(name, valueStr);
			}
			String result1 = params.get("result");
			System.out.println(result1);
			Map<String, Object> maps = new HashMap<String, Object>();
			String[] str = result1.split("&");
			for(int i=0;i<str.length;i++){
				String[] str1 = str[i].split("=");
				maps.put(str1[0], str1[1]);
			}
			String resultStatus = params.get("resultStatus");
			String outTradeNo = (String) maps.get("out_trade_no");
			outTradeNo.replaceAll("\"","");
			
			if(resultStatus.equals("9000")){
				Map<String,Object> orderMap = new HashMap<String, Object>();
				String orderNo = (String) maps.get("out_trade_no");
				orderMap.put("orderNo", orderNo.replaceAll("\"",""));
				orderOver(orderMap, payorderService, tradeRecordService, carService);
				result = "success";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	//result = "success";
	/**
	 * 异步通知 微信回调方法
	 * @param request
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @return
	 */
	public static String wxCallBack(HttpServletRequest request,
			PayOrderService payorderService, AccountTradeRecordService tradeRecordService,
			CarService carService) {
		 Map<String,String> map = new HashMap<String,String>();
		try {
			Map<String,Object> orderMap = WeixinPay.callBackUrl(request);
			if(orderMap.get("status").toString().equals("ok")){
				map.put("return_code", orderMap.get("return_code").toString());
				map.put("return_msg", orderMap.get("return_msg").toString());
				//支付成功后 更新订单状态 交易状态 车辆状态
				orderOver(orderMap, payorderService, tradeRecordService, carService);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		String result = HttpTool.mapToXml(map);
		System.out.println("微信异步校验支付成功后返回结果==="+result);
		return result;
	}
	/**
	 * 支付成功后 更新订单状态 交易状态 车辆状态
	 * @param orderMap
	 * @param payorderService
	 * @param tradeRecordService
	 * @param carService
	 * @return
	 */
	public static boolean orderOver(Map<String,Object> orderMap,PayOrderService payorderService, 
			AccountTradeRecordService tradeRecordService,CarService carService){
		
		boolean t = false;
		
		try {
			String orderNo = orderMap.get("orderNo").toString();//订单号
//			String tradeRecordNo = orderMap.get("tradeRecordNo").toString();//交易单号
//			String total_fee = orderMap.get("total_fee").toString();//总费用
//			String time_end = orderMap.get("time_end").toString();//交易结束时间
			
			Orders ord =payorderService.orderByOrderNo(orderNo);
			if(ord==null){
				return t;
			}else{
				if(ord.getState().equals(Orders.STATE_ORDER_END)){
					//支付成功后 更新订单状态
					payorderService.updateOrderstate(orderNo);
					//支付成功后  更新交易状态
					tradeRecordService.updateRecordState(orderNo,ord.getTradeOrderNo());
					//根据订单号查询订单信息
					/*Orders order = payorderService.orderByOrderNo(orderNo);
					String carId = order.getCarId();*/
					
					//获取 车辆信息
//					Map<String, Object> car = carService.carinfo(carId);
//					String version = car.get("version").toString();
//					String biz_state = car.get("biz_state").toString();
					
//					carService.updateCarStatus(carId,biz_state,version);
					//redis 存去租车的id和车机id
//					RedisCar.endRedisCarAndDeice(carId);
					t = true;
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			t = false;
		}
		return t;
	}
	
}
