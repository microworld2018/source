package com.leetu.orders.service.impl;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.ground.notice.entity.GroundNotice;
import com.ground.notice.entity.SubNotice;
import com.ground.notice.service.SubNoticeService;
import com.ground.site.entity.SubDot;
import com.ground.site.service.SubDotService;
import com.leetu.car.entity.RealtimeState;
import com.leetu.car.mapper.RealTimeStateMapper;
import com.leetu.car.service.CarService;
import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.service.CouponService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.feestrategy.mapper.StrategyBaseMapper;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.mapper.OrderMapper;
import com.leetu.orders.mapper.OrdersDetailMapper;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.util.OrderFee;
import com.leetu.orders.util.OrderRedisData;
import com.leetu.place.mapper.BranchDotMapper;
import com.leetu.place.param.BranchDotParam;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.mapper.SubScriberMapper;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.entity.RunLog;
import com.leetu.sys.entity.SMSRecord;
import com.leetu.sys.mapper.DictMapper;
import com.leetu.sys.service.DictService;
import com.leetu.sys.service.RunLogService;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.HttpRequestUtil;
import com.util.JsonTools;
import com.util.Md5Util;
import com.util.PropertiesUtil;
import com.util.TokenUtils;
import com.util.ToolDateTime;
import com.util.car.CurrentCarInfo;
import com.util.car.RedisCar;
import com.util.msg.MsgContent;
import com.util.msg.sendMSg;
import com.util.orders.OrderNoUtil;
import com.util.page.Pager;

@Service
@Transactional
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderMapper orderMapper;
	
	@Autowired
	private BranchDotMapper branchDotMapper;

	@Autowired
	private StrategyBaseMapper strategyBaseMapper;
	
	@Autowired
	private SubScriberMapper subScriberMapper;
	
	@Autowired
	private DictMapper dictMapper;
	
	@Autowired
	private RealTimeStateMapper timeStateMapper;
	
	@Autowired
	private OrdersDetailMapper ordersDetailMapper;
	
	@Autowired
	private RunLogService runLogService;
	
	@Autowired
	private CarService carService;
	
	@Autowired
	private SubNoticeService subNoticeService;
	
	@Autowired
	private SubDotService subDotService;
	
	

	@Override
	public StrategyBase carfee(String carId) {
		return strategyBaseMapper.carfee(carId);
	}

	@Override
	public Orders preorder(String carId) {
		return orderMapper.preorder(carId);
	}

	@Override
	public void addYuYUeOrder(Map<String, Object> param) {
		orderMapper.addyyorder(param);
		param.put("orderid", param.get("id"));
		orderMapper.addyyorderDetail(param);
	}

	@Override
	public Integer orderStatus(String id,Integer ostatus) {
		Integer count = orderMapper.orderMapper(id,ostatus);
		if(count==null){
			count = 0;
		}
		return count;
	}

	@Override
	public void upOrderInfo(Map<String, Object> param) {
		orderMapper.upOrderInfo(param);
		orderMapper.upOrderDetailInfo(param);
	}

	@Override
	public Orders orderInfoById(String orderId) {
		return orderMapper.orderInfoById(orderId);
	}

	@Override
	public Orders userOrderInfo(String id, String state) {
		return orderMapper.userOrderInfo(id,state);
	}
	
	@Override
	public Orders userOrderInfoNoPay(String id,String ostatus,String payState){
//		List<Orders> list = orderMapper.userOrderInfoNoPayList(id, ostatus, payState);
//		if (list != null) {
//			return list.get(0);
//		} else {
//			return null;
//		}
		return orderMapper.userOrderInfoNoPay(id,ostatus, payState);
	}

	@Override
	public List<Map<String, Object>> subOrderRecords(Map<String, Object> paramMap) {
		return orderMapper.subOrderRecords(paramMap);
	}

	@Override
	public void updateReturnDot2Default(String orderId) {
		
		orderMapper.restoreOderDefaultDot(orderId);
	}

	@Override
	public void updateOrderReturnDot(String orderId, String dotId) {
		orderMapper.updateOderReturnDot(orderId,dotId);
	}
	
	@Override
	public void updateOrderStatus(String id, Integer state){
		orderMapper.updateOrderStatus(id, state);
	}
	
	@Override
	public void updateOrderDetailStatus(String orderId){
		orderMapper.updateOrderDetailStatus(orderId);
	}
	
	public void updateOrderInfo(Map<String, Object> orderMap){
		orderMapper.upOrderInfo(orderMap);
	}
	
	public void updateOrderDetailInfo(Map<String, Object> orderMap){
		orderMapper.upOrderDetailInfo(orderMap);
	}
	
	public void addyyorder(Map<String, Object> param){
		orderMapper.addyyorder(param);
	}
	
	public void addyyorderDetail(Map<String, Object> param){
		orderMapper.addyyorderDetail(param);
	}
	
	/**
	 * 根据订单编号获取订单信息
	 * @param ordersNo
	 * @return
	 */
	@Override
	public Orders getOrderInfoByOrdersNo(String ordersNo){
		return orderMapper.getOrderInfoByOrdersNo(ordersNo);
	}

	/**
	 * 根据订单编号修改订单异常状态
	 * @param ordersNo
	 */
	@Override
	public void updateOrderAbnormityByOrderNo(String ordersNo){
		orderMapper.updateOrderAbnormityByOrderNo(ordersNo);
	}
	
	@Override
	public synchronized String advance(Subscriber s, String carId, String dotId,
			String backDotId,String ip) {
		String result = null;
		//获取车辆电量和可持续里航
		Map<String, Object> carreal =CurrentCarInfo.getCarmp(carId);
		
		//判断车辆是否绑定车机
		if(carreal == null || "".equals(carreal) ){
			result = Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
			return result;
		}
		//计费规则
		StrategyBase carfee = strategyBaseMapper.carfee(carId);
		Integer timecount = carfee.getTimeBeforeGet();//预约订单定时时间 (单位：分钟)
		if(timecount==null||timecount==0){
			timecount = 20;
		}
		Date date = new Date();
		//定时开始时间
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timing = sdf.format(new Date(date.getTime() + timecount*60*1000));
		//订单创建时间
		SimpleDateFormat tdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = tdf.format(date);
		//会员id
		String id = s.getId();
		String md5id = Md5Util.MD5Encode(id);
		Subscriber sub = subScriberMapper.querySubscriberById(id);
		
		if(sub.getEventState() != null && !"".equals(sub.getEventState())){
			//半锁状态不能下单
			if(sub.getEventState().equals(Subscriber.EVENT_STATE_HALF)){
				result = Ajax.AppJsonResult(Constants.APP_EVENT_STATE_HALF,Constants.APP_EVENT_STATE_HALF_MSG);
				return result;
			}
		}
		
		if(sub == null || "".equals(sub)){
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
			return result;
		}
		   //全锁
		if (sub.EVENT_STATE_FUll.equals(sub.getEventState())) {
			result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "此账号已冻结，不能租车。 如有疑问，请联系客服。");
			return result;
		}
		//判断当前用户资料是否审核通过
		if(sub.getState()!=Subscriber.STATE_NORMAL){
			result = Ajax.AppJsonResult(Constants.APP_STATE_NO_CONFIRMED, Constants.APP_STATE_NO_CONFIRMED_MSG);
			return result;
		}
		
		//判断用户当前时间内是否有预约订单
		if(OrderRedisData.isOrderexist(id)){
			result = Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OK, Constants.APP_ADVANCE_ORDER_OK_MSG);
			return result;
		}
		//查询用户当天时间内预约超时订单的次数
		Integer ostatus = 2;//订单状态 0预约取消 1预约成功 2预约超时 3租车中 4租车结束
		Integer count = orderMapper.orderMapper(id,ostatus);
		if(count==null){
			count = 0;
		}
		if(count>3){
			result = Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OVER, Constants.APP_ADVANCE_ORDER_OVER_MSG);
			return result;
		}
		//查询用户是否有租车中的订单
		Orders ord = orderMapper.userOrderInfo(id,Orders.STATE_ORDER_START);
		if(ord!=null&&(ord.getState().equals("1")||ord.getState().equals("3"))){
			result = Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OK, Constants.APP_ADVANCE_ORDER_OK_MSG);
			return result;
		}
		
		//查询用户是否有租车结束后未支付的订单信息
		Orders noPayOrders = orderMapper.userOrderInfoNoPay(id, Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
		if(noPayOrders != null && noPayOrders.getState().equals(Orders.STATE_ORDER_END) && String.valueOf(noPayOrders.getPayState()).equals(Orders.STATE_ORDER_NO_PAY)){
			result = Ajax.AppJsonResult(Constants.APP_ORDER_NO_PAY, Constants.APP_ORDER_NO_PAY_MSG);
			return result;
		}
		
		//更新缓存
		boolean redis = RedisClient.setobjdata(md5id, sub,DataBaseUtil.LOGIN_COMMON);
		if(!redis){
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			return result;
		}
		//获取字典表中车辆未租借状态
		Dict d = dictMapper.getDictByCodes("carBizState", "0");
		//获取字典表中车辆租借中状态
		Dict di = dictMapper.getDictByCodes("carBizState", "1");
		//获取字典表中车辆下线状态
		Dict dx = dictMapper.getDictByCodes("carBizState", "3");
		
		Map<String, Object> res = new Hashtable<String, Object>();
		
		//获取 车辆信息
		Map<String, Object> car = branchDotMapper.carinfo(carId,null);
		String version = car.get("version").toString();
		//车牌号
		String vehiclePlateId = car.get("vehiclePlateId").toString();
		//*******************************************
		//判断当前车辆是否已经预约 
		if(car.get("bizState").toString().equals(di.getId())){
			result = Ajax.AppJsonResult(Constants.APP_CAR_YUYUE_OK, Constants.APP_CAR_YUYUE_OK_MSG);
			return result;
		}else if(car.get("bizState").toString().equals(dx.getId())){
			result = Ajax.AppJsonResult(Constants.APP_CAR_YUYUE_NO, "当前车辆已经下线了");
			return result;
		}else if(car.get("bizState").toString().equals(d.getId())){
			
			//更新车辆状态为租界中的状态
			 int st = branchDotMapper.updateCarStatus(carId,di.getId(),version);
			 if(st<1){
				 result = Ajax.AppJsonResult(Constants.APP_CAR_YUYUE_OK, Constants.APP_CAR_YUYUE_OK_MSG);
				 return result;
			 }
			
			car.remove("bizState");
			car.put("percent", carreal.get("electricity"));//剩余电量
			//根据车辆ID获取车辆初始续航值
			RealtimeState r = timeStateMapper.getRealTimeState(carId);
			
		
			Map<String,String> paramMap = new HashMap<String, String>();
			paramMap.put("id", carId);//车辆ID
			
//			paramMap.put("lifeMileage", String.valueOf(carreal.get("mileage")));//续航里程
			
			//先不测试
			String electricity = carreal.get("electricity").toString();
			
			if(electricity != null && !"".equals(electricity)){
				electricity = electricity.split("/")[0];
			}else{
				electricity = "0";
			}
			
			double dd= 100;
			
			double SOC = ArithUtil.div(Double.parseDouble(electricity),dd);
			if("0".equals(carreal.get("mileage")) && SOC > 0){
				//标准里程
				double standardMileage = Double.valueOf(car.get("standardMileage").toString());
					
				car.put("lifeMileage", String.valueOf(ArithUtil.mul(standardMileage, SOC)));//续航里程
			}else{
				car.put("lifeMileage", String.valueOf(carreal.get("mileage")));//续航里程
			}
			
			paramMap.put("totalMileage", String.valueOf(carreal.get("totalMileage")));//当前里程
			paramMap.put("time", time);//创建时间
			if(r != null && !"".equals(r)){//修改
				timeStateMapper.updateRealTimeState(paramMap);
			}else{//添加
			 timeStateMapper.addRealTimeState(paramMap);
			}
			
			//获取网点信息
			Map<String, Object> dot = branchDotMapper.dotInfo(dotId);
			res.put("car", car);
			res.put("dot", dot);
			
			//租赁类型名字
			String type = carfee.getIsPrepaidPay()==1?"日租":"时租";
			//该辆车的上笔订单
			Orders order = orderMapper.preorder(carId);
			String preOrders = order==null?"":order.getId();
			//预约订单
			Map<String, Object> param = new Hashtable<String, Object>();
			DecimalFormat df = new DecimalFormat("000");
			String orderNo = OrderNoUtil.getOrderNo();
			param.put("id", "");//订单主键
			param.put("orderid", "");//订单主键
			param.put("orders_no", orderNo);//订单编号
			param.put("orders_detail_no", orderNo+df.format(1));//订单详情编号
			param.put("dotId", dotId);//网点id
			param.put("carId", carId);//车辆id
			param.put("subId", id);//会员id
			param.put("source", Orders.PAY_CHANNEL_APP);//会员id
			param.put("state", "1");//订单状态 0预约取消 1预约成功 2预约超时 3租车开始 4租车结束
			param.put("isAbnormity", "0");//订单是否异常 0 为正常 1为异常
			param.put("isAppraise", "0");//订单是否评价 0 为未评价 1为评价
			param.put("backDotId", backDotId);//还车网点id
			
			param.put("strategyId", carfee.getId());//计费规则ID
			param.put("strategyTitle", carfee.getName());//计费规则名称
			param.put("isTimeout", "0");//是否超时
			param.put("timeoutStrategyid", carfee.getId());//对应超时策略ID
			param.put("isRunning", "0");//是否在进行中
			param.put("preOrders", preOrders);//上笔订单ID
			param.put("isPrePay", "0");//是否预付费
			param.put("isPaid", "0");//是否已经付款
			param.put("typeId", carfee.getType());//租赁类型ID
			param.put("typeName",type);//租赁类型名字
			param.put("isOver", "0");//标记订单是否完结
			param.put("isException", "0");//标记订单异常
			param.put("isOpenBill", "0");//是否开了发票
			param.put("isbill", "1");//是否开了发票
			param.put("time", time);//创建时间
			param.put("paystatus", Orders.STATE_ORDER_NO_PAY);//默认未支付
			//保存预约订单
			orderMapper.addyyorder(param);
			param.put("orderid", param.get("id"));
			orderMapper.addyyorderDetail(param);
			
//		String content = " 尊敬的"+sub.getName()+":您的"+type+"订单已经预定成功，请您在"+timing+	"前去指定网点取车，否则本次订单将自动取消。【共享租车】";
//			String content = "【电动侠租车】您已下单成功，请于"+timecount+"分钟内赶往网点取车，超时后订单自动关闭。";
			String content = MsgContent.getSmsMsg(MsgContent.ORDER_CREATE_SUCCESS_MSG, String.valueOf(timecount));
			String mobile = sub.getPhoneNo();
			if(sendMSg.send(mobile, content)){
				//记录短信信息
				SMSRecord record = new SMSRecord();
				record.setContent(content);
				record.setPhoneNo(mobile);
				record.setType(3);
				record.setUserId(sub.getId());
				record.setUserName(sub.getName());
				record.setResult(mobile+":Success");
				subScriberMapper.addSmsRecord(record);
			}
			//redis 临时存放用户预约订单信息
			OrderRedisData.addTmpOrders(dotId,param.get("orderid").toString(), carId, id, timing,mobile,sub.getName(),orderNo);
			
			//redis 存租车的id和车机id
//			RedisCar.startRedisCarAndDeice(carId);
			
			
			runLogService.addRunAppAdvance(s.getName(),ip,vehiclePlateId, RunLog.APP_ADVANCE_MSG);
			res.put("timing", carfee.getTimeBeforeGet());
			res.put("startTimingTime", time);
			res.put("orderId", param.get("orderid"));
		}
		result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		return result;
	}

	public String lookNowOrder(HttpServletRequest request,
			OrderService orderService, SubScriberService scriberService,
			DictService dictService,
			OrdersDetailService ordersDetailService,
			CouponService couponService){
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		res.put("isexist", 0);//当前订单是否存在 0不存在 1存在
//		try{
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			//获取预约订单 是否存在
			Map<String,Object> yyorder = OrderRedisData.yyorder(id);
			
			if(yyorder!=null&&!yyorder.isEmpty()){
				//预约订单信息
				String orderId = yyorder.get("orderId").toString();
				String carId = yyorder.get("carId").toString();
				String dotId = yyorder.get("dotId").toString();
				String createTime = yyorder.get("createTime").toString();
				
				System.out.println(createTime);
				
				StrategyBase carfee = orderService.carfee(carId);
				Integer timecount = carfee.getTimeBeforeGet();//预约订单定时时间 (单位：分钟)
				
				String orderNo = yyorder.get("orderNo").toString();//订单号
				if(timecount==null||timecount==0){
					timecount = 20;
				}
				//获取 车辆信息
				Map<String, Object> car = branchDotMapper.carinfo(carId,null);
				car.remove("biz_state");
				//获取车辆电量和可持续里航
				Map<String, Object> carreal = CurrentCarInfo.getCarmp(carId);
				
				if(carreal != null && !"".equals(carreal)){
					car.put("lifeMileage", carreal.get("mileage"));//续航里程
					car.put("percent", carreal.get("electricity"));//剩余电量
				}else{
					car.put("lifeMileage", "");//续航里程
					car.put("percent", "");//剩余电量
				}
				
				//获取网点信息
				Map<String, Object> dot = branchDotMapper.dotInfo(dotId);
				res.put("car", car);
				res.put("dot", dot);
//				res.put("timing", carfee.getTimeBeforeGet());
				
				res.put("startTimingTime", createTime);
				res.put("orderId", orderId);
				res.put("orderType", 0);//订单类型 0 预约订单 1租车开始后的订单
				res.put("isexist", 1);
				
				
//				String currentDate = ToolDateTime.format(new Date(),"yyyy-MM-dd HH:mm:ss");
				
//				int secound = ToolDateTime.getDateSecond(ToolDateTime.parse(currentDate+":00",ToolDateTime.pattern_ymd_hms), ToolDateTime.parse(createTime+":00",ToolDateTime.pattern_ymd_hms));
				int secound = ToolDateTime.getDateSecond(new Date(), ToolDateTime.parse(createTime,ToolDateTime.pattern_ymd_hms));
				
				//超时时间为0的时候取消订单
				if(secound <= 0){
					
					secound = 0;
					
					Orders order = orderService.orderInfoById(orderId);
					
					String cancelStr =cancelOrderCommon(s.getId(),order,scriberService,dictService);
					
					res.put("isexist", 0);//修改订单状态 为不存在
					
					System.out.println(cancelStr);
				}
				
				res.put("residualTime",secound);//预约超时剩余时间（秒）
				res.put("timing", carfee.getTimeBeforeGet());
				
				
				System.out.println("预约超时剩余时间===="+secound+"====超时时间==="+carfee.getTimeBeforeGet());
				res.put("orderNo", orderNo);//订单号
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
			}else{
				//查询用户是否有租车结束后未支付的订单信息
				Orders noPayOrders = orderService.userOrderInfoNoPay(id, Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
				
				if(noPayOrders != null && !"".equals(noPayOrders)){
					//获取订单详情
					OrdersDetail ordersDetails = ordersDetailService.getByOrderIdDetail(noPayOrders.getId());
					//获取优惠卷
					Coupon coupon = couponService.getCouonByOrderId(noPayOrders.getId());
					
					res.put("orderType", 2);//订单类型 0 预约订单 1租车开始后的订单2租车结束后的订单
					
					String dotId = noPayOrders.getOrdersBackSiteId();//网点id
					String carId = noPayOrders.getCarId();//车辆id
					
					//订单费用信息
					Map<String,Object> orderfee = OrderFee.orderFeeInfo(noPayOrders, dictService, orderService,ordersDetailService,coupon);
					//获取网点信息
					Map<String, Object> dot = branchDotMapper.dotInfo(dotId);
					//获取 车辆信息
					Map<String, Object> car = branchDotMapper.carinfo(carId,null);
					car.remove("biz_state");
					res.put("dot", dot);
					res.put("car", car);
					res.put("allMile", ordersDetails.getMileage());
					res.put("allMilePrice", ordersDetails.getMileFee());
					res.put("allTimePrice", ordersDetails.getTimeFee());
					res.put("alltime", orderfee.get("alltime"));
					res.put("allminutes", orderfee.get("allminutes"));
					res.put("allPrice", orderfee.get("actualFee"));
					res.put("orderId", noPayOrders.getId());
					res.put("orderNo", noPayOrders.getOrdersNo());
					res.put("isexist", 1);
					res.put("timePrice", orderfee.get("timePrice"));
					res.put("milePrice", orderfee.get("milePrice"));
					res.put("timeType", orderfee.get("timeType"));
					res.put("payStatus", Orders.STATE_ORDER_NO_PAY);//未支付
					if(coupon != null && !"".equals(coupon)){
						res.put("couponFee", coupon.getPrice());//优惠卷
						
					}else{
						res.put("couponFee", "");//优惠卷
					}
					
					result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
					
				}else{
					res.put("orderType", 1);//订单类型 0 预约订单 1租车开始后的订单2租车结束后的订单
					//查询用户是否有租车中的订单
					Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);
					
					
					if(ord==null){
						result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
						return result;
					}
					
					//获取订单详情
					OrdersDetail ordersDetails = ordersDetailService.getByOrderIdDetail(ord.getId());
					
					//获取优惠卷
					Coupon coupon = couponService.getCouonByOrderId(ord.getId());
					
					String dotId = ord.getOrdersBackSiteId();//网点id
					String carId = ord.getCarId();//车辆id
					//订单费用信息
					Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
					//获取网点信息
					Map<String, Object> dot = branchDotMapper.dotInfo(dotId);
					//获取 车辆信息
					Map<String, Object> car = branchDotMapper.carinfo(carId,null);
					car.remove("biz_state");
					res.put("dot", dot);
					res.put("car", car);
					res.put("allMile", !StringUtils.isEmpty(ordersDetails.getMileage())?ordersDetails.getMileage():0);
					res.put("allMilePrice", !StringUtils.isEmpty(ordersDetails.getMileFee())?ordersDetails.getMileFee():0);
					res.put("allTimePrice", !StringUtils.isEmpty(ordersDetails.getTimeFee())?ordersDetails.getTimeFee():0);
					res.put("alltime", orderfee.get("alltime"));
					res.put("allminutes", orderfee.get("allminutes"));
					res.put("allPrice", orderfee.get("allPrice"));
					res.put("orderId", ord.getId());
					res.put("orderNo", ord.getOrdersNo());
					res.put("isexist", 1);
					res.put("timePrice", orderfee.get("timePrice"));
					res.put("milePrice", orderfee.get("milePrice"));
					res.put("timeType", orderfee.get("timeType"));
					res.put("payStatus", ord.getPayState());//未支付
					
					if(!StringUtils.isEmpty(coupon)){
						
						res.put("couponFee", coupon.getPrice().toString());//优惠卷
					}else{
						res.put("couponFee", "0");//优惠卷
					}
					result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
				}
				
			}
			
		return result;
	}
	
	
	/**
	 * 支付订单信息
	 * @param request
	 * @param orderService
	 * @param scriberService
	 * @param dictService
	 * @return
	 */
	public String lookpayOrder(HttpServletRequest request,
			OrderService orderService, SubScriberService scriberService,
			DictService dictService,OrdersDetailService ordersDetailService,
			CouponService couponService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		try {
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			//查询用户是否有租车中的订单
//			Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);
//			Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_END);
			/*if(ord ==null){
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
				return result;
			}*/
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			//String repayFlag = pmap.get("repayFlag");
			//重新支付标记repayFlag=1
			//String repayFlag = "1";
			Orders ord;
			if(pmap.get("repayFlag").equals("1")){
				ord = orderService.userOrderInfoNoPay(id, Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
			}else{
				ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);	
			}
			if(ord ==null){
				result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
				return result;
			}
			
			OrdersDetail ordersDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
			//获取优惠卷
			Coupon coupon = couponService.getCouonByOrderId(ord.getId());
			//订单费用信息
			Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
//			double allPrice = Double.valueOf(orderfee.get("allPrice").toString());
			double allPrice = ordersDetail.getTotalFee().doubleValue();
			
			if(coupon != null && !"".equals(coupon)){
				res.put("couponPrice", coupon.getPrice());//优惠券金额
				if(coupon.getPrice().doubleValue() > allPrice){//如果优惠卷金额大于支付金额，默认支付金额为0
					allPrice = 0;
				}else{
					
					allPrice = ArithUtil.sub(allPrice,coupon.getPrice().doubleValue());
				}
			}else{
				res.put("couponPrice", "");//优惠券金额
			}
			double payPrice = 0.00;
//			double minConsumption = Double.valueOf(orderfee.get("minConsumption").toString());
			//判断当前消费金额是否小于最低消费金额
			/*if(allPrice<minConsumption){
				allPrice=minConsumption;
			}*/
			//获取账户信息
			Map<String,Object> subInfo = scriberService.subAccountInfo(id);
			Double amount = 0.0;
			if(subInfo != null){
				amount = Double.valueOf(subInfo.get("usable_amount").toString());//账户余额
				//判断当前用户余额是否大于支付金额 是 用余额支付 否用微信或支付宝支付剩余的 
				if(amount>allPrice){
					amount = allPrice;
					payPrice = 0;
				}else{
					payPrice = ArithUtil.sub(allPrice,amount);
				}
			}
//			res.put("allMilePrice", orderfee.get("allMilePrice"));//总里程价格 单位：元
//			res.put("allTimePrice", orderfee.get("allTimePrice"));//总时长价格
			
			res.put("allMilePrice", !StringUtils.isEmpty(ordersDetail.getMileFee())?ordersDetail.getMileFee():0);//总里程价格 单位：元
			res.put("allTimePrice", !StringUtils.isEmpty(ordersDetail.getTimeFee())?ordersDetail.getTimeFee():0);//总时长价格
			
			String insuranceName = "";//保险名称
			
			String insuranceFee = "";//保险金额
			
			String [] insuranceFees = null;
			
			String [] insuranceNames = null;
			
			int days = 0;//订单用单时长（单位:天）
			
			//根据订单开始时间、结束时间，计算天数
			if(!StringUtils.isEmpty(ord.getBeginTime())&&!StringUtils.isEmpty(ord.getEndTime())){
				days = ToolDateTime.getDateDaySpace(ord.getBeginTime(), ord.getEndTime());
			}
			
			if(!StringUtils.isEmpty(ordersDetail.getInsuranceName())){
				
				insuranceNames = ordersDetail.getInsuranceName().split("/");
				
				if(!StringUtils.isEmpty(ordersDetail.getInsuranceFee())){
					
					insuranceFees = ordersDetail.getInsuranceFee().split("/");
				}
				
				for(int i =0;i<insuranceNames.length;i++){
					
					if(insuranceFees != null && insuranceFees.length > 0){
						insuranceName += insuranceNames[i]+"("+insuranceFees[i]+"元/单)"+"/";
						insuranceFee  += ArithUtil.mul(Double.valueOf(insuranceNames[i].toString()), days)+"/";
						//总费用+保险费用
//						allPrice = ArithUtil.add(allPrice, Double.valueOf(insuranceNames[i].toString()));
					}
					
				}
			}
			
			
			res.put("allPrice", allPrice);//总费用
			res.put("payPrice", payPrice);//实际支付金额
			res.put("amount", amount);//账户余额
			
			res.put("insuranceName", insuranceName);//保险名称
			res.put("insuranceFee", insuranceFee);//保险金额
			
			res.put("allMile",  orderfee.get("allMile"));//总里程 （公里）
			res.put("alltime",  orderfee.get("alltime"));//总时长
			
			res.put("orderId", ord.getId());
			
			orderService.updateOrderStatus(ord.getId(), Integer.parseInt(Orders.STATE_ORDER_END)); //更新订单状态为租车结束
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	
	
	@Override
	public String startCar(Subscriber s,String carId,String orderId,DeviceBindingService deviceBindingService,CarService carService,String ip) {
		String result = null;
		
		//根据车辆ID获取车辆设备绑定，车机编号SN
		Map<String, String> deviceBindingMap = deviceBindingService.getDeviceByCarId(carId);
		
		if(deviceBindingMap == null || StringUtils.isEmpty(deviceBindingMap)){
			return Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
		}
		
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("sn", deviceBindingMap.get("device_no"));//设备编号
		
		//开始用车远程恢复供电
//		String msgUrl = PropertiesUtil.getStringValue("msg_url")+"/car/restorePower";
		//调用开始用车远程恢复供电
		String contents = HttpRequestUtil.getPostMethod(CurrentCarInfo.RESTORE_POWER_URL,paramMap);
		
		System.out.println(contents);
		Date date = new Date();
		//开始用车时间
		SimpleDateFormat tdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = tdf.format(date);
		//会员id
		String id = s.getId();
		Subscriber sub = subScriberMapper.querySubscriberById(id);
		
		if(sub == null || "".equals(sub)){
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
			return result;
		}
		
		if (sub.EVENT_STATE_HALF.equals(sub.getEventState())) {
			result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "此账号已冻结，不能租车。 如有疑问，请联系客服。");
			return result;
		}
		
		Map<String, Object> orderMap = new HashMap<String, Object>();
		orderMap.put("orderId", orderId);
		orderMap.put("begintime", time);//用车实际开始时间
		orderMap.put("state", Orders.STATE_ORDER_START);//订单状态改为租车中
		orderMap.put("paystatus", Orders.STATE_ORDER_NO_PAY);//状态未支付
		
		//更新订单信息
//		orderMapper.upOrderInfo(orderMap);
		updateOrderInfo(orderMap);
		//更新订单详情信息
//		orderMapper.upOrderDetailInfo(orderMap);
		updateOrderDetailInfo(orderMap);
		
		Orders ord = orderInfoById(orderId);
		
		//根据车辆ID获取车辆信息
		Map<String, String> carInfo = carService.getCarDotByCarId(carId);
		
		//短信发送内容，发送给运营人员（暂不发送）
		String content = MsgContent.getSmsMsg(MsgContent.USER_USE_CAR_MSG,sub.getName(),sub.getPhoneNo(),carInfo.get("vehicle_plate_id").toString(),time,carInfo.get("address").toString());
		
		//添加消息记录表
		GroundNotice groundNotice = new GroundNotice();
		groundNotice.setNoticeContent(content);
		groundNotice.setCreateTime(new Date());
		groundNotice.setNoticeType(0);
		String subId="skdfj"+(new Date()).getTime()+"asjkdfj";
		groundNotice.setId(subId);
		subNoticeService.addGrouneNotice(groundNotice);
		//获取取车网点所属的地勤人员
		SubDot subDot = new SubDot();
		subDot.setDotId(ord.getBeginSiteId());
		List<SubDot> subDotList = subDotService.getSubDotList(subDot);
		//添加消息关联记录
		if(subDotList != null && subDotList.size()>0){
			for(int i=0;i<subDotList.size();i++){
				SubNotice subNotice = new SubNotice();
//				String subAndDotId="skdfj"+(new Date()).getTime()+"asjkdfj";
				String subAndDotId = UUID.randomUUID().toString().replace("-", "");
				subNotice.setDotId(ord.getBeginSiteId());
				subNotice.setUserId(subDotList.get(i).getSubId());
				subNotice.setNoticeId(subId);
				subNotice.setIsRead(0);
				subNotice.setId(subAndDotId);
				subNoticeService.addSubNotice(subNotice);
			}
		}
		
		
//		GroundNotice groundNotice = new GroundNotice();
//		groundNotice.setCreateTime(new Date());
//		groundNotice.setNoticeContent(content);
//		groundNotice.setNoticeType(0);
//		subNoticeService.addGrouneNotice(groundNotice);
//		//获取取车网点所属的地勤人员
//		SubDot subDot = new SubDot();
//		subDot.setDotId(ord.getBeginSiteId());
//		List<SubDot> subDotList = subDotService.getSubDotList(subDot);
//		//添加消息关联记录
//		if(subDotList != null && subDotList.size()>0){
//			for(int i=0;i<subDotList.size();i++){
//				SubNotice subNotice = new SubNotice();
//				subNotice.setDotId(ord.getBeginSiteId());
//				subNotice.setUserId(subDotList.get(i).getSubId());
//				subNotice.setNoticeId(groundNotice.getId());
//				subNotice.setIsRead(0);
//				subNoticeService.addSubNotice(subNotice);
//			}
//		}
		
		
		
		
//		runLogService.addRunAppStartCar(ord.getOrdersNo(),sub.getName(), ip, carInfo.get("vehicle_plate_id").toString(), content);
		
		result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,null);
		return result;
	}

	@Override
	public Integer subOrderRecordsCount(
			Map<String, Object> paramMap) {
		return orderMapper.subOrderRecordsCount(paramMap);
	}

	/**
	 * 订单详情
	 */
	@Override
	public String orderBilling(String data) {
		
		String result = null;
		
		//获取当前用户信息
		Subscriber s = TokenUtils.getSubscriber(data);
		
		//查询用户是否有租车中的订单
		Orders ord = userOrderInfo(s.getId(),Orders.STATE_ORDER_START);
		
		if(StringUtils.isEmpty(s)){
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN, Constants.APP_LOGIN_TOKEN_MSG);
			return result;
		}
		
		if(StringUtils.isEmpty(ord)){
			result = Ajax.AppJsonResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG);
			return result;
		}
		
		Map<String, Object> res = new Hashtable<String, Object>();
		//从redis中获取订单里程、费用、时长数据
		Map<String, Object> redisMap = OrderRedisData.getCurrentOrder(ord.getId());
		
		if(!StringUtils.isEmpty(redisMap)){
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,redisMap);
			
			return result;
		}
		
		Map<String, Object> carMap = carService.carinfo(ord.getCarId());
		
		//订单开始时间
		Date beginTime = ord.getBeginTime();
		//当前时间
		Date endTime = new Date();
		
		String allTime = "";//总时长
		
		Integer allminutes = 0;//花费时间 单位：分钟
		
		if(beginTime != null && !"".equals(beginTime) && endTime != null && !"".equals(endTime)){
			allTime = ToolDateTime.getBetweenDate(beginTime, endTime);//总时长
			 allminutes = ToolDateTime.getDateMinuteSpace(beginTime, endTime);
			 if(allminutes < 1){
				 allminutes = 1;
			 }
		}
		
//		总里程
//		String mileage = "0.00";
		//总里程费用
		double allMilePrice = 0.00;
		//总消费
		double allPrice = 0.00;
		//时长总价格
		double allTimePrice = 0.00;
		
		String vehiclePlateId = carMap.get("vehiclePlateId").toString();
		
		Map<String,String> paramMap = new HashMap<String, String>();
		
		paramMap.put("sn", vehiclePlateId);
		
		paramMap.put("beginTime", ToolDateTime.format(beginTime,ToolDateTime.pattern_ymd_hms));
		
		paramMap.put("endTime", ToolDateTime.format(endTime,ToolDateTime.pattern_ymd_hms));
		
//		String mileage = BranchDotParam.getCarMileageForGPS(paramMap,CurrentCarInfo.CAR_MILEAGE_FOR_GPS_URL);
		
		String mileage = "0";
		
		/*if(!StringUtils.isEmpty(mileage) && !mileage.equals("error")){
			if(!StringUtils.isEmpty(mileage) && !mileage.equals("0")){
				//计算里程、精确到小数点后两位
				mileage = String.valueOf(ArithUtil.div(Double.parseDouble(mileage), 1000, 2));
				//总里程，不满一公里按一公里算
				mileage = String.valueOf(ArithUtil.round(Double.parseDouble(mileage)));
			}
		}else{
			mileage = "0";
		}*/
		
		String timeType = "fenzhong";
		if(ord!=null){
			Dict m = dictMapper.getDictByCodes("timeUnit", "fenzhong");
			Dict h = dictMapper.getDictByCodes("timeUnit", "xiaoshi");
			StrategyBase carfee = carfee(ord.getCarId());//计费规则
			
			BigDecimal  kmPrice = carfee.getKmPrice();//里程价(单位：公里)
			
			allMilePrice = ArithUtil.mul(kmPrice.doubleValue(),Double.parseDouble(mileage));
			
			String  timelyFeeUnit = carfee.getTimelyFeeUnit();//计时单位
			
			//计算时长费用
			if(timelyFeeUnit.equals(m.getId())){
				timeType="fenzhong";
				if(beginTime != null && !"".equals(beginTime) && endTime != null && !"".equals(endTime)){
					allTimePrice = OrderFee.conTimeFee(carfee, beginTime, endTime, timeType);
				}
			}else if(timelyFeeUnit.equals(h.getId())){
				timeType = "xiaoshi";
				if(beginTime != null && !"".equals(beginTime) && endTime != null && !"".equals(endTime)){
					allTimePrice = OrderFee.conTimeFee(carfee, beginTime, endTime, timeType);
				}
			}
			
			//总费用=公里费用+分时费用
			allPrice = ArithUtil.add(allMilePrice, allTimePrice);
			//费用保留两位小数
//			allPrice = ArithUtil.round(allPrice,2);
		}
		
		res.put("alltime", allTime);//总时长
		res.put("allPrice", allPrice);//总费用
		res.put("mileage", mileage);//总里程
		
		result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
	
		return result;
	}
	
	
	/**
	 * 取消订单公用方法
	 * @param sub
	 * @param orders
	 */
	public String cancelOrderCommon(String subId,
			Orders orders,
			SubScriberService scriberService,
			DictService dictService){
		
		String result = null;
		//获取用户预约订单
		Map<String, Object> torders = OrderRedisData.yyorder(subId);
		Subscriber sub = scriberService.querySubscriberById(subId);
		Integer status = 0;//0预约取消 1预约成功 2预约超时 3租车开始 4租车结束
		
//		String content = "【电动侠租车】亲，您的订单已取消！";
		
//		String content="尊敬的"+sub.getName()+"，您的订单"+orders.getOrdersNo()+"已取消。【电动侠租车】";
		String content = MsgContent.getSmsMsg(MsgContent.ORDER_CANCLE_MSG, sub.getName(), orders.getOrdersNo());
		if(null!=torders&&!torders.isEmpty()){
			//获取字典表中车辆未租借状态
			Dict di = dictService.getDictByCodes("carBizState", "0");
			
			String carId = torders.get("carId").toString();
			
			//获取 车辆信息
			Map<String, Object> car = branchDotMapper.carinfo(carId,null);
			String version = car.get("version").toString();
			String mobile = torders.get("mobile").toString();
			String userName = torders.get("userName").toString();
			
//			String createTime = torders.get("createTime").toString();
			//更新车辆状态
			int st = branchDotMapper.updateCarStatus(carId,di.getId(),version);
			
			if(st==1){
				//更新订单状态
				updateOrderStatus(orders.getId(), status);
				updateOrderDetailStatus(orders.getId());
				if(sendMSg.send(mobile, content)){
					//记录短信信息
					SMSRecord record = new SMSRecord();
					record.setContent(content);
					record.setPhoneNo(mobile);
					record.setType(3);
					record.setUserId(subId);
					record.setUserName(userName);
					record.setResult(mobile+":Success");
					scriberService.addSmsRecord(record);
				}
				//从redis中删除会员 预约订单信息
				OrderRedisData.delOrder(sub.getId());
				
				return Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
			}
			
		}
		
		return result;
	}

	@Override
	public List<Map<String, Object>> getCarByIdOrderList(String carId) {
		return orderMapper.getCarByIdOrderList(carId);
	}

	/**
	 * 根据对象修改支付状态
	 */
	@Override
	public void updateOrder(Orders ord) {
		orderMapper.updateOrder(ord);
	}
	
}
