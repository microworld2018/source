package com.leetu.orders.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.util.StringUtils;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;

public class OrderRedisData {
	
	/**
	 * 获取所有预约订单
	 * @return
	 */
	public static Hashtable<String,Map<String,Map<String,Object>>> oldordertimes(){
		//获取redis所有时间段内的订单信息集合 (时间：订单信息集合)预约订单 key:时间,value:map==><key:subId,value:map==>order订单信息>
		Hashtable<String,Map<String,Map<String,Object>>> oldordertime = (Hashtable<String, Map<String, Map<String, Object>>>) RedisClient.getobjkeys("tmporders",DataBaseUtil.CS_IPC);
		if(oldordertime==null){
			oldordertime = new Hashtable<String,Map<String,Map<String,Object>>>();
		}
		return oldordertime;
	}
	
	
	
	/**
	 * 用于临时存放用户预约订单信息
	 * @param orderId 订单id
	 * @param carId   汽车id
	 * @param dotId   网点id
	 * @param mobile  手机号
	 * @param userName用户名
	 * @param createTime 创建时间
	 * @param orderNo 订单号
	 */
	public static void addTmpOrders(String dotId,String orderId,String carId,String subId,String createTime,String mobile,String userName,String orderNo){
		//订单信息
		Map<String,Object> orderinfo = new Hashtable<String, Object>();
		orderinfo.put("orderId", orderId);
		orderinfo.put("carId", carId);
		orderinfo.put("dotId", dotId);
		orderinfo.put("mobile", mobile);
		orderinfo.put("userName", userName);
		orderinfo.put("createTime", createTime);
		orderinfo.put("orderNo", orderNo);
	 
		Hashtable<String,Map<String,Map<String,Object>>> oldordertime = oldordertimes();
		//获取redis时间段内的 订单信息集合(用户id:订单信息 )
		Map<String,Map<String,Object>> oldorderno = oldordertime.get(createTime);
		if(oldorderno==null||oldorderno.isEmpty()){
			oldorderno = new Hashtable<String,Map<String,Object>>();
		}
		oldorderno.put(subId, orderinfo);
		oldordertime.put(createTime, oldorderno);
		RedisClient.setobjdata("tmporders", oldordertime,DataBaseUtil.CS_IPC);
	}
	
	
	
	/**
	 * 获取超时订单
	 * @param createTime
	 * @return
	 * @throws ParseException 
	 */
	public static Map<String,Map<String,Object>> tlist(String createTime) throws ParseException{
		/*//获取redis时间段内的 订单信息集合{时间:{用户ID:订单信息}}
		Hashtable<String,Map<String,Map<String,Object>>> oldordertime = oldordertimes();
		//存储超时订单信息{时间:{用户ID:订单信息}};
		Map<String,Map<String,Map<String,Object>>> oldorderMap = new HashMap<String,Map<String,Map<String,Object>>>();
		//遍历所有预约订单
		for(Entry<String, Map<String, Map<String, Object>>> entry:oldordertime.entrySet()){
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
				Date stardate = sdf.parse(createTime);
				Date enddata = sdf.parse(entry.getKey());
				long diff = (stardate.getTime() - enddata.getTime())/60/1000;
				判断订单是否超时
				if(diff > 0){
					oldorderMap.put(entry.getKey(), entry.getValue());
				} 
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		}
		return oldorderMap;*/
		Hashtable<String,Map<String,Map<String,Object>>> oldordertime = oldordertimes();
		////获取redis时间段内的 订单信息集合{时间:{用户ID:订单信息}}
		Map<String,Map<String,Object>> oldorderno = oldordertime.get(createTime);
		for(Entry<String, Map<String, Map<String, Object>>> entry:oldordertime.entrySet()){
			entry.getKey();
			entry.getValue();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
			Date enddata = sdf.parse(entry.getKey());
			long diff = (new Date().getTime() - enddata.getTime())/60/1000;
			if(diff >= 1){
				if(entry.getValue().size()>0){
					oldorderno = entry.getValue();
					delredisTimeOrder(entry.getKey());
				}else{
					delredisTimeOrder(entry.getKey());
				}
			}
		}
		if(oldorderno==null||oldorderno.isEmpty()){
			oldorderno = new Hashtable<String,Map<String,Object>>();
		}
		return oldorderno;  
	}
	
	/**
	 * 获取某一用户 预约订单
	 * @param subId
	 * @return
	 */
	public static Map<String, Object> yyorder(String subId) {
		Hashtable<String,Map<String,Map<String,Object>>> oldordertime = oldordertimes();
		Map<String, Object> order = new Hashtable<String, Object>();
		if (oldordertime != null && !oldordertime.isEmpty()) {
			for (Entry<String, Map<String, Map<String, Object>>> m : oldordertime.entrySet()) {
				if (m.getValue() != null && m.getValue().containsKey(subId)) {
					order = m.getValue().get(subId);
				}
			}
		}
		return order;
	}
	
	
	/**
	 * 判断用户当前时间内是否有预约订单
	 * @param orderId 订单id
	 * @param createTime 创建时间
	 * @return
	 */
	public static boolean isOrderexist(String subId){
		Hashtable<String,Map<String,Map<String,Object>>> oldordertime = oldordertimes();
		boolean status = false;
		if(oldordertime!=null&&!oldordertime.isEmpty()){
			for (Entry<String, Map<String, Map<String, Object>>> m : oldordertime.entrySet()) {
				if(m.getValue()!=null&&m.getValue().containsKey(subId)){
					status = true;
				}
			}
		}
		return status;
	}
	
	/**
	 * 删除某一时间的 订单信息集合 (时间：订单信息集合)
	 * @param createTime
	 */
	public static void delredisTimeOrder(String createTime){
		Hashtable<String,Map<String,Map<String,Object>>> oldordertime = oldordertimes();
		if(oldordertime!=null&&!oldordertime.isEmpty()){
			oldordertime.remove(createTime);
			RedisClient.setobjdata("tmporders", oldordertime,DataBaseUtil.CS_IPC);
		}
	} 
	
	/**
	 * redis中删除会员 预约订单信息
	 * @param subId
	 */
	public static void delOrder(String subId){
		Hashtable<String,Map<String,Map<String,Object>>> oldordertime = oldordertimes();
		if (oldordertime != null && !oldordertime.isEmpty()) {
			for (Entry<String, Map<String, Map<String, Object>>> m : oldordertime.entrySet()) {
				if (m.getValue() != null && m.getValue().containsKey(subId)) {
					m.getValue().remove(subId);
				}
			}
		}
		RedisClient.setobjdata("tmporders", oldordertime,DataBaseUtil.CS_IPC);
	}
	
	/**
	 * 从redis中获取车辆状态信息
	 * @param carId
	 * @return
	 */
	public static Map<String,Object> getCarStatus(String carId){
		Map<String, Map<String, Object>> carInfo =(Map<String, Map<String, Object>>)RedisClient.getobjkeys("CarInfo",DataBaseUtil.CS_CAR_MSG);
		return (Map<String, Object>)carInfo.get(carId);
	}
	
	/**
	 * 用于存放租车中的订单里程、费用、时长信息（时时计费）
	 * @param orderId 订单ID
	 * @param allTime 总时长
	 * @param allPrice 总费用
	 * @param mileage 总里程
	 */
	public static void addCurrentOrder(String orderId,String allTime,double allPrice,String mileage){
		//订单信息
		Map<String,Object> orderinfo = new Hashtable<String, Object>();
		orderinfo.put("alltime", allTime);
		orderinfo.put("allPrice", allPrice);
		orderinfo.put("mileage", mileage.equals("0")?Double.valueOf("0.0"):mileage);
	 
		Hashtable<String,Map<String,Object>> currentOrders = getCurrentOrders();
		
		currentOrders.put(orderId, orderinfo);
		
		RedisClient.setobjdata("currentOrders", currentOrders,DataBaseUtil.CS_IPC);
	} 
	
	/**
	 * 获取实时计费中所有订单信息
	 * @return
	 */
	public static Hashtable<String,Map<String,Object>> getCurrentOrders(){
		
		Hashtable<String,Map<String,Object>> currentOrders = (Hashtable<String,Map<String,Object>>)RedisClient.getobjkeys("currentOrders", DataBaseUtil.CS_IPC);
		
		if(StringUtils.isEmpty(currentOrders)){
			currentOrders = new Hashtable<String, Map<String,Object>>();
		}
		
		return currentOrders;
	}
	
	/**
	 * 根据ORDERID获取租车中的订单实时计费信息
	 * @param orderId
	 * @return
	 */
	public static Map<String, Object> getCurrentOrder(String orderId){
		
		Hashtable<String,Map<String,Object>> currentOrders = getCurrentOrders();
		
		Map<String, Object> currentOrderMap = new HashMap<String, Object>();
		
		if(!StringUtils.isEmpty(currentOrders)){
			currentOrderMap = currentOrders.get(orderId);
		}
		
		return currentOrderMap;
	} 
	
	/**
	 * 根据订单ID删除租车中的订单实时计费信息
	 * @param orderId
	 */
	public static void delCurrentOrderById(String orderId){
		
		Hashtable<String,Map<String,Object>> currentOrders = (Hashtable<String,Map<String,Object>>)RedisClient.getobjkeys("currentOrders", DataBaseUtil.CS_IPC);
		
		if(!StringUtils.isEmpty(orderId)){
			currentOrders.remove(orderId);
		}
		
		RedisClient.setobjdata("currentOrders", currentOrders,DataBaseUtil.CS_IPC);
	}
	
	public static void main(String[] args) {
		
//		Map<String, Object> carInfo = getCarStatus("8a2331335a3a970b015a592c51a601e9");
//		System.out.println(carInfo);
		
//		addCurrentOrder("0000000", "20分钟", 30.5, "30");
//		delCurrentOrderById("0000000");
		System.out.println(getCurrentOrders());
//		System.out.println(isOrderexist("833a091c99691034aee7cc5b64e1409b"));
//		delOrder("833a091c99691034aee7cc5b64e1409b");
		
//		addTmpOrders("402881ef533b6da201533b8683ac0005","402881ef533b6da201533b8683ac0005","402881f44d6507c1014d650897ed0000","402881e94f6cb86c014f6cbfd1e50000","2016-07-20 16:32","18810289689","hzw");
//				
////		System.out.println(tlist("2016-07-20 16:32"));
//		System.out.println(isOrderexist("402881e94f6cb86c014f6cbfd1e50000"));
////		delredisTimeOrder("2016-07-20 16:32");
//
//		RedisClient.deleteData("tmporders",1);
//		delOrder("6cc0248a54a711e699d920474798e617");
//		System.out.println(yyorder("6cc0248a54a711e699d920474798e617"));
	}
	
}
