package com.leetu.orders.service.impl;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.mapper.OrdersDetailMapper;
import com.leetu.orders.service.OrdersDetailService;
import com.util.StringHelper;
import com.util.charging.util.FeeUtils;

@Service
@Transactional
public class OrdersDetailServiceImpl implements OrdersDetailService {

	@Autowired
	private OrdersDetailMapper ordersDetailMapper;

	@Override
	public OrdersDetail getByOrderIdDetail(String ordersId) {
		return ordersDetailMapper.getByOrderIdDetail(ordersId);
	}

	@Override
	public void updateByOrdersIdDetil(String orderId, String mileage, String endTime) {
		ordersDetailMapper.updateByOrdersIdDetil(orderId, mileage, endTime);
	}

	@Override
	public int getInsuranceFee(String orderId) {
		OrdersDetail ordersDetail = ordersDetailMapper.getByOrderIdDetail(orderId);
		String insuranceFeeStr = ordersDetail.getInsuranceFee();
		float insFee = 0l;
		if(StringHelper.isNotEmpty(insuranceFeeStr)){
			String insuranceFee[] = insuranceFeeStr.split("/");
			for(int i = 0;i < insuranceFee.length;i++){
				insFee += Float.valueOf(insuranceFee[i]);
			}
		}
		return FeeUtils.yuanToFen(insFee);
	}

}
