package com.leetu.orders.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.leetu.car.service.CarService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.subscriber.entity.Subscriber;
import com.util.page.Pager;

public interface OrderService {

	/**
	 * 查询该车的计费规则
	 * @param carId
	 * @return
	 */
	StrategyBase carfee(String carId);

	/**
	 * 该辆车的上笔订单
	 * @param carId
	 * @return
	 */
	Orders preorder(String carId);

	/**
	 * 添加预约订单
	 * @param param
	 */
	void addYuYUeOrder(Map<String, Object> param);

	/**
	 * 查询用户当天时间内的取消订单的次数
	 * @param id 
	 * @param ostatus
	 * @return
	 */
	Integer orderStatus(String id, Integer ostatus);

	/**
	 * 更改订单状态
	 * @param param
	 */
	void upOrderInfo(Map<String, Object> param);

	/**
	 * 查询订单信息
	 * @param orderId
	 * @return
	 */
	Orders orderInfoById(String orderId);

	/**
	 * 查询用户是否已有订单
	 * @param id
	 * @param ostatus
	 * @return
	 */
	Orders userOrderInfo(String id, String ostatus);
	
	/**
	 * 查询用户是否有未支付订单
	 * @param id
	 * @param payState
	 * @return
	 */
	Orders userOrderInfoNoPay(String id,String ostatus,String payState);
	
    /**
     *  用户订单记录
     * @param id
     * @param ostate
     * @return
     */
	List<Map<String, Object>> subOrderRecords(Map<String, Object> paramMap);
	/**
	 * 用户订单记录数 
	 * @param paramMap
	 * @return
	 */
	Integer subOrderRecordsCount(Map<String, Object> paramMap);

	/**
	 * 恢复默认换车网点 为取车网点
	 * @param orderId
	 * @version 1.0.1
	 */
	void updateReturnDot2Default(String orderId);

	/**
	 * 更新预换车网点
	 * @param orderId
	 * @param dotId
	 * @version 1.0.1
	 */
	void updateOrderReturnDot(String orderId, String dotId);

	/**
	 * 取车出发
	 * @param s
	 * @param carId
	 * @param dotId
	 * @param backDotId
	 * @return
	 */
	String advance(Subscriber s, String carId, String dotId, String backDotId,String ip);

	/**
	 * 开始用车
	 * @return
	 */
	String startCar(Subscriber s, String carId,String orderId,DeviceBindingService deviceBindingService,CarService carService,String ip);

	/**
	 * 更新订单状态
	 * @param id
	 * @param ostatus
	 */
	void updateOrderStatus(String id, Integer state);
	
	/**
	 * 更新订单详情
	 * @param orderId
	 */
	void updateOrderDetailStatus(String orderId);
	
	/**
	 * 订单计费详情
	 */
	String orderBilling(String data);
	
	/**
	 * 预约车辆订单添加
	 * @param param
	 */
	void addyyorder(Map<String, Object> param);
	/**
	 * 预约车辆订单详情添加
	 * @param param
	 */
	void addyyorderDetail(Map<String, Object> param);
	
	/**
	 * 根据订单编号获取订单信息
	 * @param ordersNo
	 * @return
	 */
	Orders getOrderInfoByOrdersNo(String ordersNo);
	
	/**
	 * 根据订单编号修改订单异常状态
	 * @param ordersNo
	 */
	void updateOrderAbnormityByOrderNo(String ordersNo);
	
	/**根据车辆ID获取该车辆所有订单
	 * @param carId
	 * @return
	 */
	List<Map<String, Object>> getCarByIdOrderList(@Param("carId")String carId);

	/**
	 * 根据对象修改支付状态
	 * @author Jin Guangyu
	 * @date 2017年7月28日下午1:52:37
	 * @param ord
	 */
	void updateOrder(Orders ord);
	
}
