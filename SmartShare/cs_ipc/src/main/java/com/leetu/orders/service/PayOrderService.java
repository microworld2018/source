package com.leetu.orders.service;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jdom.JDOMException;

import com.leetu.account.service.AccountTradeRecordService;
import com.leetu.coupon.service.CouponService;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.orders.entity.Orders;

public interface PayOrderService {

	/**
	 * 更新订单信息
	 * @param order
	 */
	void updateOrderInfo(Map<String, Object> order);

	/**
	 * 定时更新订单信息
	 */
	void updateOrderInfoQuartz(Map<String, Object> order);
	/**
	 * 支付成功后 更新订单状态
	 * @param orderNo
	 * @param tradeRecordNo
	 */
//	void updateOrderstate(String orderNo, String tradeRecordNo);
	void updateOrderstate(String orderNo);

	/**
	 * 根据订单号查询订单信息
	 * @param orderNo
	 * @return
	 */
	Orders orderByOrderNo(String orderNo);
	
	/**
	 * 支付完成结束订单
	 * 支付成功后 更新订单状态、 交易状态、 车辆状态
	 * @param orderMap
	 * @return
	 */
	public boolean orderOver(Map<String,Object> orderMap);

	/**
	 * 调取微信查询接口，确认支付结果
	 * @param orderNo
	 * @return 1:成功并且金额正确 2:查询成功但金额错误 3:查询微信接口失败 4:根据订单号未查询到订单信息
	 */
	int confirmTradeResults(String orderNo, PayOrderService payOrderService, AccountTradeRecordService accountTradeRecordService) throws JDOMException, IOException;

	/**
	 * 调取微信查询接口，确认支付结果(新)
	 * @param orderNo
	 * @return 1:成功并且金额正确 2:查询成功但金额错误 3:查询微信接口失败 4:根据订单号未查询到订单信息
	 * @throws Exception 
	 */
	int confirmTradeResultsNew(String orderNo, PayOrderService payOrderService, AccountTradeRecordService accountTradeRecordService, SubCouponService subCouponService) throws Exception;
	
	/**
	 * 微信查询确认支付成功后，更改订单状态为已支付
	 * @param tradeRecordService
	 */
	void updateOrderStateToAlreadyPaid(AccountTradeRecordService tradeRecordService, String orderNo);

	/**
	 * 微信查询失败或金额不对，修改订单状态为未支付
	 * @param tradeRecordService
	 * @param string
	 */
	void updateOrderStateToUnPaid(AccountTradeRecordService tradeRecordService, String orderNo);

	/**
	 * 订单支付（新）
	 * @author Jin Guangyu
	 * @date 2017年7月24日下午2:30:02
	 * @param request
	 * @param orderService 
	 * @return
	 */
	String payOrder(HttpServletRequest request, OrderService orderService, AccountTradeRecordService tradeRecordService, 
					OrdersDetailService ordersDetailService, CouponService couponService, SubCouponService subCouponService);

	/**
	 * 根据对象更新 三方支付金额，三方交易状态，三方订单号，交易状态
	 * @author Jin Guangyu
	 * @date 2017年7月29日上午11:04:07
	 * @param order
	 */
	void updateOrder(Orders order);

	/**
	 * 客户端调用支付宝回调接口，只更新三方支付金额，三方交易状态，交易状态
	 * @author Jin Guangyu
	 * @date 2017年8月1日下午5:08:02
	 * @param order
	 */
	void updateOrderResultAndTposPayFee(Orders order);
		
		

}
