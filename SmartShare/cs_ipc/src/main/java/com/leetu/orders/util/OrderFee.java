package com.leetu.orders.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.service.CouponService;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.service.DictService;
import com.util.ArithUtil;
import com.util.DateUtil;
import com.util.ToolDateTime;

public class OrderFee {

	/**
	 * 订单计费公共方法
	 * @param ord
	 * @param dictService
	 * @param orderService
	 * @return
	 */
	public static Map<String,Object> orderFeeInfo(Orders ord,DictService dictService,
			OrderService orderService,
			OrdersDetailService ordersDetailService,
			Coupon coupon){
		Map<String, Object> res = new Hashtable<String, Object>();
		//订单结束时间
		OrdersDetail ordrDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
		Date endTime = new Date();
		if(ordrDetail.getEndTime() != null && !"".equals(ordrDetail.getEndTime())){
			endTime = ordrDetail.getEndTime();
		}
		
		//订单初始里程
		BigDecimal startmile = ord.getBeginMileage()==null ?new BigDecimal(0):ord.getBeginMileage();
		BigDecimal endmile = ord.getEndMileage()==null?new BigDecimal(0):ord.getEndMileage();//获取当前里程
		
		if(startmile.compareTo(endmile) == 1 || startmile.compareTo(endmile) == 0){
			endmile = startmile;
		}
		double allMile = 0.00;
		
		if(ordrDetail.getMileage() != null &&!"".equals(ordrDetail.getMileage())){
			allMile = ordrDetail.getMileage().doubleValue();//总里程
		}
		
		
		double allMilePrice = 0.00;//里程消费
		double allTimePrice = 0.00;//时长消费
		double allPrice = 0.00;//总消费
		double milePrice = 0.00;//里程单价
		double timePrice = 0.00;//时长单价
		double actualFee = 0.00;//实际消费总金额
		Date beginTime = ord.getBeginTime();
		
		String alltime = "";//总时长
		
		Integer allminutes = 0;//花费时间 单位：分钟
		
		if(beginTime != null && !"".equals(beginTime) && endTime != null && !"".equals(endTime)){
			 alltime = ToolDateTime.getBetweenDate(beginTime, endTime);//总时长
			 allminutes = ToolDateTime.getDateMinuteSpace(beginTime, endTime);
			 if(allminutes < 1){
				 allminutes = 1;
			 }
		}
		
		String timeType = "fenzhong";
		if(ord!=null){
			Dict m = dictService.getDictByCodes("timeUnit", "fenzhong");
			Dict h = dictService.getDictByCodes("timeUnit", "xiaoshi");
			StrategyBase carfee = orderService.carfee(ord.getCarId());//计费规则
			
			BigDecimal  kmPrice = carfee.getKmPrice();//里程价(单位：公里)
			milePrice = kmPrice.doubleValue();
			if(ordrDetail.getMileage() == null){
				allMilePrice = kmPrice.doubleValue()*0;
			}else {
				allMilePrice = ArithUtil.mul(kmPrice.doubleValue(), ordrDetail.getMileage().doubleValue());
			}
			
			Integer timeJs = carfee.getTimelyFeeLong();//时间基数 
			timeJs = timeJs==0?1:timeJs;
			BigDecimal  timeprice = carfee.getBasePrice();//时长计价
			timePrice = timeprice.doubleValue();
			String  timelyFeeUnit = carfee.getTimelyFeeUnit();//计时单位
			
			//计算时长费用
			if(timelyFeeUnit.equals(m.getId())){
				timeType="fenzhong";
				if(beginTime != null && !"".equals(beginTime) && endTime != null && !"".equals(endTime)){
					allTimePrice = conTimeFee(carfee, beginTime, endTime, timeType);
					
				}
			}else if(timelyFeeUnit.equals(h.getId())){
				timeType = "xiaoshi";
				if(beginTime != null && !"".equals(beginTime) && endTime != null && !"".equals(endTime)){
					allTimePrice = conTimeFee(carfee, beginTime, endTime, timeType);
				}
			}
			
			
			//加保险费用actualFee、allPrice
			double insuranceFee = 0.0;//保险金额(用户实际的)
			String [] insuranceFees = null;//保险金额(数据库里的)
			String [] insuranceNames = null;//保险名称
			int days = 0;//订单用单时长（单位:天,不足一天按一天计算）
			//获取订单详情
			OrdersDetail orderDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
			//根据订单开始时间、结束时间，计算天数
			if(!StringUtils.isEmpty(ord.getBeginTime())&&!StringUtils.isEmpty(endTime)){
				Date endTimeDate = endTime;
				days = ToolDateTime.getDateDaysForInsurance(ord.getBeginTime(), endTimeDate);
			}
			if(!StringUtils.isEmpty(orderDetail.getInsuranceName())){
				insuranceNames = orderDetail.getInsuranceName().split("/");
				
				if(!StringUtils.isEmpty(orderDetail.getInsuranceFee())){
					insuranceFees = orderDetail.getInsuranceFee().split("/");
				}
				
				for(int i =0;i<insuranceNames.length;i++){
					if(insuranceFees != null && insuranceFees.length > 0){
						//判断订单用单时长，计算保险费用、总费用
						if(days > 0){
//							allPrice = ArithUtil.add(allPrice, ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
//							actualFee = ArithUtil.add(actualFee, ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
							insuranceFee += ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days); 
						}else{
//							allPrice = ArithUtil.add(allPrice, Double.valueOf(insuranceFees[i].toString()));
//							actualFee = ArithUtil.add(actualFee, Double.valueOf(insuranceFees[i].toString()));
							insuranceFee += ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days); 
						}
					}
				}
			}
			
			
			if(coupon != null && !"".equals(coupon)){//加上保险费用
				//实际消费总金额=公里费用+分时费用-优惠卷
				actualFee = ArithUtil.sub(ArithUtil.add(allMilePrice, allTimePrice),coupon.getPrice().doubleValue());
				actualFee = ArithUtil.add(actualFee, insuranceFee);
			}else{
				//实际消费总金额=公里费用+分时费用
				actualFee = ArithUtil.add(allMilePrice, allTimePrice);
				actualFee = ArithUtil.add(actualFee, insuranceFee);
			}
			
			if(actualFee < 0){
				actualFee = 0;
			}
			
			//消费总费用=公里费用+分时费用
			allPrice = ArithUtil.add(allMilePrice, allTimePrice);
			
			res.put("endTime", endTime);
			res.put("allMile", allMile);
			res.put("allMilePrice",allMilePrice);
			res.put("allTimePrice", allTimePrice);
			res.put("alltime", alltime);
			res.put("allminutes", allminutes);
			res.put("allPrice", allPrice);
			res.put("orderId", ord.getId());
			res.put("timePrice", timePrice);
			res.put("milePrice", milePrice);
			res.put("timeType", timeType);
			res.put("startmile", startmile);
			res.put("endmile", endmile);
			res.put("orderNo", ord.getOrdersNo());
			res.put("subId", ord.getMemberId());
			res.put("runningDetailNo", ord.getRunningDetailNo());
			res.put("actualFee", actualFee);//实际消费总金额
			
		}
		return res;
	}
	
	
	/**
	 * 计算时长费用
	 * @param strategyBase
	 * @param startTime
	 * @param endTime
	 * @param timeType
	 * @return
	 */
	public static double conTimeFee(StrategyBase strategyBase, Date startTime, Date endTime,
			String timeType) {
		//即时租
		if(startTime.compareTo(endTime) > 0){
			return new BigDecimal(0).doubleValue();
		}
		//总时长 = (结束时间 - 开始时间) 
//		BigDecimal allTime = new BigDecimal(endTime.getTime()).subtract(new BigDecimal(startTime.getTime()));
		//基本计价时长
//		Integer timelyFeeLong = strategyBase.getTimelyFeeLong();
		//时间基数
//		BigDecimal timeJs = getTimeLongByUnit(new BigDecimal(timelyFeeLong), timeType);
		double allPrice = 0.0;
//		double allTime1 = 0.0;
		
		Long allTime = Long.parseLong(ToolDateTime.getDateMinuteSpace(startTime, endTime)+"");
		System.out.println(allTime);
		/*Long ms = endTime.getTime()-startTime.getTime();
		Integer ss = 1000;  
	    Integer mi = ss * 60;  
	    Integer hh = mi * 60;  
	    Integer dd = hh * 24;  
	    
	    Long day = ms / dd;  
	    Long hour = (ms - day * dd) / hh;  
	    Long minute = (ms - day * dd - hour * hh) / mi;  */
		
	    if(allTime < 1){
	    	allTime = 1L;
	    }
		
//		allTime1 = allTime.doubleValue();
//		//计费基数个数 = (总时长 / 时间基数 )
//		BigDecimal gs = allTime.divide(timeJs,0);
		//计时总费用
//		allPrice = allPrice.add(strategyBase.getBasePrice().multiply(gs));
		double basePrice = 0.0;
		basePrice = strategyBase.getBasePrice().doubleValue();
//		allPrice = minute * basePrice;
		//计时总费用
		allPrice = ArithUtil.mul(allTime, basePrice);
		
		return allPrice;
	}
	
   /**
    * 时长基数
    * @param time
    * @param timeType
    * @return
    */
	public static BigDecimal getTimeLongByUnit(BigDecimal time, String timeType) {
		BigDecimal timeLong = new BigDecimal(0);
		if("fenzhong".equals(timeType)){
			timeLong = new BigDecimal(time.doubleValue() * 60 * 1000);
		}
		else if("xiaoshi".equals(timeType)){
			timeLong = new BigDecimal(time.doubleValue() *60 * 60 * 1000);
		}
		else if("tian".equals(timeType)){
			timeLong = new BigDecimal(time.doubleValue() * 24 * 60 * 60 * 1000);
		}
		else if("yue".equals(timeType)){
			Calendar nowCal = Calendar.getInstance();
			nowCal.add(Calendar.MONTH,time.intValue());
			timeLong = new BigDecimal(nowCal.getTime().getTime() - new Date().getTime());		
		}
		
		return timeLong;
	}
	
}
