package com.leetu.orders.param;

import java.awt.print.Paper;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.util.StringUtils;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.leetu.car.entity.CarComment;
import com.leetu.car.service.CarCommentService;
import com.leetu.car.service.CarService;
import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.service.CouponService;
import com.leetu.coupon.service.SubCouponService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.util.OrderFee;
import com.leetu.orders.util.OrderRedisData;
import com.leetu.place.entity.BranchDot;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.entity.SMSRecord;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.ContantsTool;
import com.util.IpUtil;
import com.util.JsonTools;
import com.util.MapDistance;
import com.util.Quick;
import com.util.TokenUtils;
import com.util.ToolDateTime;
import com.util.Utils;
import com.util.car.CurrentCarInfo;
import com.util.msg.MsgContent;
import com.util.msg.sendMSg;
import com.util.page.Pager;

public class OrderParam {

	/**
	 * 预约订单(取车地点)
	 * @param request
	 * @param orderService
	 * @param scriberService
	 * @param dictService
	 * @return
	 */
	public static String advance(HttpServletRequest request,OrderService orderService,
			SubScriberService scriberService,DictService dictService,BranchDotService branchDotService){
		String result = null;
		try{
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			//获取参数
			Map<String, String> map = JsonTools.desjsonForMap(data);
			if(map.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			String dotId = map.get("dotId");
			String backDotId = map.get("backDotId");
			String carId = map.get("carId");
			 
			if(carId==null){
				result = Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
				return result;
			}
			if(dotId==null){
				result = Ajax.AppJsonResult(Constants.APP_DOTID_NULL, Constants.APP_DOTID_NULL_MSG);
				return result;
			}
			if(backDotId==null){
				result = Ajax.AppJsonResult(Constants.APP_BACKDOTID_NULL, Constants.APP_BACKDOTID_NULL_MSG);
				return result;
			}
			
			String ip = IpUtil.getIpAddr(request);
			//预约车辆 并下预约订单(接口封版，停止使用)
//			result = orderService.advance(s,carId,dotId,backDotId,ip);
			//提示更新最新版本
			result = Ajax.AppJsonResult(Constants.SYS_VERSION_OLD, Constants.SYS_VERSION_OLD_MSG);
			
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	
	/**
	 * 查看当前订单
	 * @param request
	 * @param orderService
	 * @param scriberService
	 * @param dictService
	 * @param branchDotService
	 * @return
	 */
	public static String lookNowOrder(HttpServletRequest request,
			OrderService orderService, SubScriberService scriberService,
			DictService dictService, BranchDotService branchDotService,
			OrdersDetailService ordersDetailService,
			CouponService couponService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		res.put("isexist", 0);//当前订单是否存在 0不存在 1存在
//		try{
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			//获取预约订单 是否存在
			Map<String,Object> yyorder = OrderRedisData.yyorder(id);
			
			if(yyorder!=null&&!yyorder.isEmpty()){
				//预约订单信息
				String orderId = yyorder.get("orderId").toString();
				String carId = yyorder.get("carId").toString();
				String dotId = yyorder.get("dotId").toString();
				String createTime = yyorder.get("createTime").toString();
				
				StrategyBase carfee = orderService.carfee(carId);
				Integer timecount = carfee.getTimeBeforeGet();//预约订单定时时间 (单位：分钟)
				
				String orderNo = yyorder.get("orderNo").toString();//订单号
				if(timecount==null||timecount==0){
					timecount = 20;
				}
				//获取 车辆信息
				Map<String, Object> car = branchDotService.carinfo(carId,null);
				car.remove("biz_state");
				//获取车辆电量和可持续里航
				Map<String, Object> carreal = CurrentCarInfo.getCarmp(carId);
				
				if(carreal != null && !"".equals(carreal)){
					car.put("lifeMileage", carreal.get("mileage"));//续航里程
					car.put("percent", carreal.get("electricity"));//剩余电量
				}else{
					car.put("lifeMileage", "");//续航里程
					car.put("percent", "");//剩余电量
				}
				
				//获取网点信息
				Map<String, Object> dot = branchDotService.dotInfo(dotId);
				res.put("car", car);
				res.put("dot", dot);
				res.put("timing", carfee.getTimeBeforeGet());
				res.put("startTimingTime", createTime);
				res.put("orderId", orderId);
				res.put("orderType", 0);//订单类型 0 预约订单 1租车开始后的订单
				res.put("isexist", 1);
				
				
				int secound = ToolDateTime.getDateSecond(new Date(), ToolDateTime.parse(createTime+":00",ToolDateTime.pattern_ymd_hms));
				
				//超时时间为0的时候取消订单
				if(secound <= 0){
					
					secound = 0;
					
					Orders order = orderService.orderInfoById(orderId);
					
					String cancelStr =cancelOrderCommon(s.getId(),order,orderService,scriberService,dictService,branchDotService);
					
					res.put("isexist", 0);//修改订单状态 为不存在
					
					System.out.println(cancelStr);
				}
				
				res.put("residualTime",secound);//预约超时剩余时间（秒）
				res.put("orderNo", orderNo);//订单号
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
			}else{
				//查询用户是否有租车结束后未支付的订单信息
				Orders noPayOrders = orderService.userOrderInfoNoPay(id, Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
				
				if(noPayOrders != null && !"".equals(noPayOrders)){
					//获取订单详情
					OrdersDetail ordersDetails = ordersDetailService.getByOrderIdDetail(noPayOrders.getId());
					//获取优惠卷
					Coupon coupon = couponService.getCouonByOrderId(noPayOrders.getId());
					
					res.put("orderType", 2);//订单类型 0 预约订单 1租车开始后的订单2租车结束后的订单
					
					String dotId = noPayOrders.getOrdersBackSiteId();//网点id
					String carId = noPayOrders.getCarId();//车辆id
					
					//订单费用信息
					Map<String,Object> orderfee = OrderFee.orderFeeInfo(noPayOrders, dictService, orderService,ordersDetailService,coupon);
					//获取网点信息
					Map<String, Object> dot = branchDotService.dotInfo(dotId);
					//获取 车辆信息
					Map<String, Object> car = branchDotService.carinfo(carId,null);
					car.remove("biz_state");
					res.put("dot", dot);
					res.put("car", car);
//					res.put("allMile", orderfee.get("allMile"));
					res.put("allMile", ordersDetails.getMileage());
//					res.put("allMilePrice", orderfee.get("allMilePrice"));
					res.put("allMilePrice", ordersDetails.getMileFee());
//					res.put("allTimePrice", orderfee.get("allTimePrice"));
					res.put("allTimePrice", ordersDetails.getTimeFee());
					res.put("alltime", orderfee.get("alltime"));
					res.put("allminutes", orderfee.get("allminutes"));
					res.put("allPrice", orderfee.get("actualFee"));
//					res.put("allPrice", ordersDetails.getTotalFee());
					res.put("orderId", noPayOrders.getId());
					res.put("orderNo", noPayOrders.getOrdersNo());
					res.put("isexist", 1);
					res.put("timePrice", orderfee.get("timePrice"));
					res.put("milePrice", orderfee.get("milePrice"));
					res.put("timeType", orderfee.get("timeType"));
					res.put("payStatus", Orders.STATE_ORDER_NO_PAY);//未支付
					if(coupon != null && !"".equals(coupon)){
						res.put("couponFee", coupon.getPrice());//优惠卷
						
					}else{
						res.put("couponFee", "");//优惠卷
					}
					
					result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
					
				}else{
					res.put("orderType", 1);//订单类型 0 预约订单 1租车开始后的订单2租车结束后的订单
					//查询用户是否有租车中的订单
					Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);
					
					
					if(ord==null){
						result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
						return result;
					}
					
					//获取订单详情
					OrdersDetail ordersDetails = ordersDetailService.getByOrderIdDetail(ord.getId());
					
					//获取优惠卷
					Coupon coupon = couponService.getCouonByOrderId(ord.getId());
					
					String dotId = ord.getOrdersBackSiteId();//网点id
					String carId = ord.getCarId();//车辆id
					//订单费用信息
					Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
					//获取网点信息
					Map<String, Object> dot = branchDotService.dotInfo(dotId);
					//获取 车辆信息
					Map<String, Object> car = branchDotService.carinfo(carId,null);
					car.remove("biz_state");
					res.put("dot", dot);
					res.put("car", car);
//					res.put("allMile", orderfee.get("allMile"));
//					res.put("allMilePrice", orderfee.get("allMilePrice"));
//					res.put("allTimePrice", orderfee.get("allTimePrice"));
					res.put("allMile", !StringUtils.isEmpty(ordersDetails.getMileage())?ordersDetails.getMileage():0);
					res.put("allMilePrice", !StringUtils.isEmpty(ordersDetails.getMileFee())?ordersDetails.getMileFee():0);
					res.put("allTimePrice", !StringUtils.isEmpty(ordersDetails.getTimeFee())?ordersDetails.getTimeFee():0);
					res.put("alltime", orderfee.get("alltime"));
					res.put("allminutes", orderfee.get("allminutes"));
					res.put("allPrice", orderfee.get("allPrice"));
//					res.put("allPrice", ordersDetails.getTotalFee());
					res.put("orderId", ord.getId());
					res.put("orderNo", ord.getOrdersNo());
					res.put("isexist", 1);
					res.put("timePrice", orderfee.get("timePrice"));
					res.put("milePrice", orderfee.get("milePrice"));
					res.put("timeType", orderfee.get("timeType"));
					res.put("payStatus", ord.getPayState());//未支付
					
					if(!StringUtils.isEmpty(coupon)){
						
						res.put("couponFee", coupon.getPrice().toString());//优惠卷
					}else{
						res.put("couponFee", "0");//优惠卷
					}
					result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
				}
				
			}
			
//		} catch (Exception e) {
//			e.printStackTrace();
//			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
//		}
		return result;
	}


	/**
	 * 支付订单信息
	 * @param request
	 * @param orderService
	 * @param scriberService
	 * @param dictService
	 * @return
	 */
	public static String lookpayOrder(HttpServletRequest request,
			OrderService orderService, SubScriberService scriberService,
			DictService dictService,OrdersDetailService ordersDetailService,
			CouponService couponService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		try {
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			//查询用户是否有租车中的订单
//			Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);
//			Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_END);
			/*if(ord ==null){
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
				return result;
			}*/
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			//String repayFlag = pmap.get("repayFlag");
			//重新支付标记repayFlag=1
			//String repayFlag = "1";
			Orders ord;
			if(pmap.get("repayFlag").equals("1")){
				ord = orderService.userOrderInfoNoPay(id, Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
			}else{
				ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);	
			}
			if(ord ==null){
				result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
				return result;
			}
			
			OrdersDetail ordersDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
			//获取优惠卷
			Coupon coupon = couponService.getCouonByOrderId(ord.getId());
			//订单费用信息
			Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
//			double allPrice = Double.valueOf(orderfee.get("allPrice").toString());
			double allPrice = ordersDetail.getTotalFee().doubleValue();
			
			if(coupon != null && !"".equals(coupon)){
				res.put("couponPrice", coupon.getPrice());//优惠券金额
				if(coupon.getPrice().doubleValue() > allPrice){//如果优惠卷金额大于支付金额，默认支付金额为0
					allPrice = 0;
				}else{
					
					allPrice = ArithUtil.sub(allPrice,coupon.getPrice().doubleValue());
				}
			}else{
				res.put("couponPrice", "");//优惠券金额
			}
			double payPrice = 0.00;
//			double minConsumption = Double.valueOf(orderfee.get("minConsumption").toString());
			//判断当前消费金额是否小于最低消费金额
			/*if(allPrice<minConsumption){
				allPrice=minConsumption;
			}*/
			//获取账户信息
			Map<String,Object> subInfo = scriberService.subAccountInfo(id);
			Double amount = 0.0;
			if(subInfo != null){
				amount = Double.valueOf(subInfo.get("usable_amount").toString());//账户余额
				//判断当前用户余额是否大于支付金额 是 用余额支付 否用微信或支付宝支付剩余的 
				if(amount>allPrice){
					amount = allPrice;
					payPrice = 0;
				}else{
					payPrice = ArithUtil.sub(allPrice,amount);
				}
			}
//			res.put("allMilePrice", orderfee.get("allMilePrice"));//总里程价格 单位：元
//			res.put("allTimePrice", orderfee.get("allTimePrice"));//总时长价格
			
			res.put("allMilePrice", !StringUtils.isEmpty(ordersDetail.getMileFee())?ordersDetail.getMileFee():0);//总里程价格 单位：元
			res.put("allTimePrice", !StringUtils.isEmpty(ordersDetail.getTimeFee())?ordersDetail.getTimeFee():0);//总时长价格
			
//			if(allPrice < 0){
//				res.put("allPrice", 0);//总费用
//			}else{
				res.put("allPrice", allPrice);//总费用
//			}
//			res.put("payPrice", payPrice);//实际支付金额
			res.put("payPrice", payPrice);//实际支付金额
			res.put("amount", amount);//账户余额
			
			
			res.put("allMile",  orderfee.get("allMile"));//总里程 （公里）
			res.put("alltime",  orderfee.get("alltime"));//总时长
			
			res.put("orderId", ord.getId());
			
			orderService.updateOrderStatus(ord.getId(), Integer.parseInt(Orders.STATE_ORDER_END)); //更新订单状态为租车结束
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}


	/**
	 * 即将还车时的订单信息
	 * @param request
	 * @param orderService
	 * @param scriberService
	 * @param dictService
	 * @param branchDotService
	 * @return
	 */
	public static String backCarOrder(HttpServletRequest request,
			OrderService orderService, SubScriberService scriberService,
			DictService dictService, BranchDotService branchDotService,
			OrdersDetailService ordersDetailService,
			CouponService couponService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();	
		try {
			String data = request.getParameter("data1");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			
			/** 请求参数**/
			String lat= pmap.get("lat");
			String lng= pmap.get("lng");
			String raidus = "40";
			if(lng==null){
				result = Ajax.AppJsonResult(Constants.APP_LNG_NULL, Constants.APP_LNG_NULL_MSG);
				return result;
			}
			if(lat==null){
				result = Ajax.AppJsonResult(Constants.APP_LAT_NULL, Constants.APP_LAT_NULL_MSG);
				return result;
			}
			//获取raidus米范围内的经纬度
			Map map = MapDistance.getAround(lat, lng, raidus);
			//获取raidus米范围内的网点
			List<BranchDot> branchDots = branchDotService.branchDots(map);
			//获取最近的网点
			Map<String,String> dot = getNearDot(branchDots,lat,lng);
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			//查询用户是否有租车中的订单
			Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);
			if(ord==null){
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
				return result;
			}
			
			//查询优惠卷
			Coupon coupon = couponService.getCouonByOrderId(ord.getId());
			Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
			String carId = ord.getCarId();//车辆id

			double allPrice = Double.valueOf(orderfee.get("allPrice").toString());
//			double minConsumption = Double.valueOf(orderfee.get("minConsumption").toString());
			//判断当前消费金额是否小于最低消费金额
			/*if(allPrice<minConsumption){
				allPrice=minConsumption;
			}*/
			
			Map<String,Object> order = new Hashtable<String,Object>();
			order.put("carId", carId);
			order.put("allMile",  orderfee.get("allMile"));
			order.put("allMilePrice",  orderfee.get("allMilePrice"));
			order.put("allTimePrice",  orderfee.get("allTimePrice"));
			order.put("alltime",  orderfee.get("alltime"));
			order.put("allminutes",  orderfee.get("allminutes"));
			order.put("allPrice",  allPrice);
			order.put("orderId", ord.getId());
			
			res.put("dot", dot);
			res.put("order", order);
			
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
			} catch (Exception e) {
				e.printStackTrace();
				result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			}
		return result;
	}

	/**
	 * 获取最近网点信息
	 * @param branchDots
	 * @param lat
	 * @param lng
	 * @return
	 */
	public static Map<String,String> getNearDot(List<BranchDot> branchDots,String lat,String lng){
		Map<String,String> res = new Hashtable<String,String>();
		if(branchDots!=null&branchDots.size()>0){
			Map<String,Map<String,String>> disdot = new Hashtable<String, Map<String,String>>();
			double[] distance= new double[branchDots.size()] ;
			int i = 0;
			for (BranchDot branchDot : branchDots) {
				//网点与当前位置的距离
				distance[i]=MapDistance.Distance(Double.parseDouble(lat), Double.parseDouble(lng),
						Double.parseDouble(branchDot.getLat().toString()), Double.parseDouble(branchDot.getLng().toString()));
				Map<String,String> m = new Hashtable<String,String>();
				m.put("dotId", branchDot.getId());
				m.put("dotName", branchDot.getName());
				m.put("dotLatLng", branchDot.getLat()+","+branchDot.getLng());
				disdot.put(distance[i]+"",m);
				i++;
			}
			//快速排序(升序)
			Quick q = new Quick();
			double [] arrays = q.quick_sort(distance,distance.length);
			res = disdot.get(arrays[0]+"");
			res.put("distance", arrays[0]+"");
		}
		return res;
	}
	
	/**
	 * 用户订单记录
	 * @param request
	 * @param orderService
	 * @return
	 */
	public static String subOrderRecords(HttpServletRequest request,
			OrderService orderService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		try {
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			String id = s.getId();
			
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			paramMap.put("id", id);
			
			Pager pager = new Pager();
			
			pager.setCurPage(Integer.parseInt(pmap.get("currentPage")));//当前页
			pager.setPageSize(10);//每页显示数量
			
			pager.setTotalRow(orderService.subOrderRecordsCount(paramMap)); //共条数据
			pager.setStart(pager.getStart()); //开始行
			paramMap.put("pager", pager);
			
			List<Map<String,Object>> ord = orderService.subOrderRecords(paramMap);
			res.put("orders", ord);
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		}catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}

		return result;
	}
	 
	/**
	 * 微信分享 订单信息
	 * @param request
	 * @param orderService
	 * @return
	 */
	public static String shareOrder(HttpServletRequest request,
			OrderService orderService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		try {
			String orderId = request.getParameter("orderId");
			if(orderId==null||"".equals(orderId)){
				String data = request.getParameter("data");
				Map<String, String> pmap = JsonTools.desjsonForMap(data);
				if(pmap.get("param").equals("error")){
					result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
					return result;
				}
				orderId = pmap.get("orderId");
			}
			Orders order = orderService.orderInfoById(orderId);
			BigDecimal totalFee = order.getTotalFee();
			totalFee = totalFee==null?new BigDecimal(0):totalFee;
			
			Date beginTime = order.getBeginTime();
			Date endTime = order.getEndTime();
			endTime = endTime==null?new Date():endTime;
			String ordersTime = ToolDateTime.getBetweenDate(beginTime, endTime);//总时长
			
			double allMile = sub(order.getEndMileage(), order.getBeginMileage());
			res.put("totalFee", totalFee);
			res.put("ordersTime", ordersTime);
			res.put("allMile", allMile);
			res.put("sahreUrl", ContantsTool.DOMAIN_NAME+"cs_ipc/order/share?orderId="+order.getId());
			request.setAttribute("res", res);
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		}catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	
	/**
	 * 取消预约订单
	 * @param request
	 * @param orderService
	 * @return
	 */
	public static String cancelOrder(HttpServletRequest request,
			OrderService orderService,
			DictService dictService,
			BranchDotService branchDotService,
			SubScriberService scriberService){
		
		String result = null;
		
		try {
			
			String data = request.getParameter("data");
			
			Subscriber s = TokenUtils.getSubscriber(data);
			
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			}
			
			String orderId = pmap.get("orderId");
			
			if(orderId == null || "".equals(orderId)){
				return Ajax.AppJsonResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG);
			}
			
			Orders order = orderService.orderInfoById(orderId);
			
			if(order == null || "".equals(order)){
				return Ajax.AppJsonResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG);
			}
			
			result = cancelOrderCommon(s.getId(),order,orderService,scriberService,dictService,branchDotService);
			
		}catch (Exception e) {
			e.printStackTrace();
			return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		
		return result;
	}
	
	/**
	 * 取消订单公用方法
	 * @param sub
	 * @param orders
	 */
	public static String cancelOrderCommon(String subId,
			Orders orders,
			OrderService orderService, 
			SubScriberService scriberService,
			DictService dictService, 
			BranchDotService branchDotService){
		
		String result = null;
		//获取用户预约订单
		Map<String, Object> torders = OrderRedisData.yyorder(subId);
		Subscriber sub = scriberService.querySubscriberById(subId);
		Integer status = 0;//0预约取消 1预约成功 2预约超时 3租车开始 4租车结束
		
//		String content = "【电动侠租车】亲，您的订单已取消！";
		
//		String content="尊敬的"+sub.getName()+"，您的订单"+orders.getOrdersNo()+"已取消。【电动侠租车】";
		String content = MsgContent.getSmsMsg(MsgContent.ORDER_CANCLE_MSG, sub.getName(), orders.getOrdersNo());
		System.out.println("数据======="+torders);
		
		if(null!=torders&&!torders.isEmpty()&&torders.size()>0){
			//获取字典表中车辆未租借状态
			Dict di = dictService.getDictByCodes("carBizState", "0");
			
			String carId = torders.get("carId").toString();
			
			//获取 车辆信息
			Map<String, Object> car = branchDotService.carinfo(carId,null);
			
			
			String version = car.get("version").toString();
			String mobile = torders.get("mobile").toString();
			String userName = torders.get("userName").toString();
			
//			String createTime = torders.get("createTime").toString();
			//更新车辆状态
			int st = branchDotService.updateCarStatus(carId,di.getId(),version);
			if(st==1){
				//更新订单状态
				orderService.updateOrderStatus(orders.getId(), status);
				orderService.updateOrderDetailStatus(orders.getId());
				if(sendMSg.send(mobile, content)){
					//记录短信信息
					SMSRecord record = new SMSRecord();
					record.setContent(content);
					record.setPhoneNo(mobile);
					record.setType(3);
					record.setUserId(subId);
					record.setUserName(userName);
					record.setResult(mobile+":Success");
					scriberService.addSmsRecord(record);
				}
				//从redis中删除会员 预约订单信息
//				OrderRedisData.delOrder(sub.getId());
				//获取所有预约中的订单信息
				Hashtable<String,Map<String,Map<String,Object>>> oldordertimes = OrderRedisData.oldordertimes();
				
				for(Entry<String, Map<String, Map<String, Object>>> entry:oldordertimes.entrySet()){
					String createTime = entry.getKey();
					
					Map<String, Map<String, Object>> maps = entry.getValue();
					
					for(Entry<String, Map<String, Object>> en:maps.entrySet()){
						
						String sb = en.getKey();
						
						if(sub.getId().equals(sb)){
							OrderRedisData.delredisTimeOrder(createTime);
						}
					}
				}
				
				result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
			}
			
		}else{
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}
		
		return result;
	}
	
	// 进行减法运算
	public static double sub( BigDecimal b1, BigDecimal b2){       
			b1 = b1==null?new BigDecimal(0):b1;
			b2 = b2==null?new BigDecimal(0):b2;
	        return b1.subtract(b2).doubleValue();
	     }
	
	
	/**
	 * 查看订单详情
	 * @param request
	 * @param orderService
	 * @param scriberService
	 * @param dictService
	 * @param branchDotService
	 * @return
	 */
	public static String viewOrderDetails(HttpServletRequest request,
			OrderService orderService,DictService dictService, 
			BranchDotService branchDotService,CarCommentService carCommentService,
			OrdersDetailService ordersDetailService,
			CouponService couponService) {
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		//当前订单是否存在 0不存在 1存在
		try{
			String data = request.getParameter("data");
			if(data == null || data == ""){
				result = Ajax.AppResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG,res.put("isexist", 0));
				return result;
			}
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			//查询订单是否存在
			Orders ord = orderService.orderInfoById(pmap.get("orderId"));
			if(ord==null){
				result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
				return result;
			}
			
			//获取优惠卷
			Coupon coupon = couponService.getCouonByOrderId(ord.getId());
			
			//获取取车网点
			Map<String, Object> takedot = branchDotService.dotInfo(ord.getBeginSiteId());
			
			//获取还车网点
			Map<String, Object> returndot = branchDotService.dotInfo(ord.getEndSiteId());
			
			//获取订单详情
			OrdersDetail orderDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
			
			//获取 车辆信息
			Map<String, Object> car = branchDotService.carinfo(ord.getCarId() == null?"":ord.getCarId(),null);
			
			//获取车辆评价
			Map<String, Object> carCommentMap = new HashMap<String, Object>();
			CarComment cc = new CarComment();
			cc.setOrderId(ord.getId());
 			CarComment carComment = carCommentService.queryCarCommentByConditions(cc);
 			if(carComment != null){
 				carCommentMap.put("isOk", carComment.getIsOk());
 				carCommentMap.put("carFace", carComment.getCarFace());
 				carCommentMap.put("carClean", carComment.getCarClean());
 				carCommentMap.put("carImg", carComment.getCarImg());
 			}
 			//订单基本信息
 			SimpleDateFormat tdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 			res.put("createOrderTime", tdf.format(ord.getCreateDate()));//下单时间
 			res.put("carName", car.get("cnName").toString()+car.get("name"));//车型名称加车辆品牌
 			res.put("carNumber", car.get("vehiclePlateId") == null?"":car.get("vehiclePlateId"));//车牌号
			res.put("takedot", takedot == null?"":takedot.get("name"));//取车地点
			if(ord.getEndTime() != null && !"".equals(ord.getEndTime())){
				res.put("returnCarTime", tdf.format(ord.getEndTime()));
			}else{
				
				if(ord.getOrdersBackTime() != null && !"".equals(ord.getOrdersBackTime())){
					res.put("returnCarTime", tdf.format(ord.getOrdersBackTime()));
				}else{
					res.put("returnCarTime", "");
				}
				
			}
			res.put("returndot", returndot == null?"":returndot.get("name"));//还车地点
			//订单费用信息
			Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
			//获取 车辆信息
			/*res.put("allMile", orderfee.get("allMile"));
			res.put("allMilePrice", orderfee.get("allMilePrice"));
			res.put("allTimePrice", orderfee.get("allTimePrice"));
			res.put("alltime", orderfee.get("alltime"));
			res.put("allminutes", orderfee.get("allminutes"));*/
			res.put("allMile", !StringUtils.isEmpty(orderDetail.getMileage())?orderDetail.getMileage():0);
			res.put("allMilePrice", !StringUtils.isEmpty(orderDetail.getMileFee())?orderDetail.getMileFee():0);
			res.put("allTimePrice", !StringUtils.isEmpty(orderDetail.getTimeFee())?orderDetail.getTimeFee():0);
			res.put("alltime", orderfee.get("alltime"));
			res.put("allminutes", orderfee.get("allminutes"));
//			res.put("allPrice", orderDetail.getActualFee().doubleValue());
			res.put("allPrice", orderfee.get("actualFee"));
			
			
			/*if(Double.valueOf(orderfee.get("actualFee").toString()) < 0){
				res.put("allPrice", 0);
			}else{
				res.put("allPrice", orderfee.get("actualFee"));
			}*/
			res.put("timePrice", orderfee.get("timePrice"));
			res.put("milePrice", orderfee.get("milePrice"));
			res.put("timeType", orderfee.get("timeType"));
			
			
			//订单费用信息
			/*res.put("allMile", orderDetailMap.getMileage() == null?"":orderDetailMap.getMileage());//总里程
//			res.put("alltime", orderDetailMap.getUseTimeStr() == null?"":orderDetailMap.getUseTimeStr());//总用时
			res.put("alltime", orderDetailMap.getUseTimeStr() == null?"":orderDetailMap.getUseTimeStr());//总用时
			res.put("milePrice", orderDetailMap.getMileFee() == null?"":orderDetailMap.getMileFee());//里程消费
			res.put("timePrice", orderDetailMap.getTicketsFee() == null?"":orderDetailMap.getTicketsFee());//用时消费
			res.put("totalFee", orderDetailMap.getTotalFee() == null?"":orderDetailMap.getTotalFee());//总费用
*///			res.put("totalFee", orderDetailMap.getTposPayFee() == null?"":orderDetailMap.getTposPayFee());//实际支付金额
			if(!StringUtils.isEmpty(coupon)){
				
				res.put("coupon", coupon.getPrice());//优惠券
				
			}
			res.put("orderId", ord.getId());//订单ID
			res.put("orderNo", ord.getOrdersNo());//订单编号
			//车辆评价
			res.put("carCommentMap", carCommentMap);
			res.put("isexist", 1);
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
			
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		return result;
	}
	
	/**
	 * 预约订单(取车地点)
	 * @param request
	 * @param orderService
	 * @param scriberService
	 * @param dictService
	 * @return
	 */
	public static String startCar(HttpServletRequest request,OrderService orderService,
			SubScriberService scriberService,DictService dictService,
			BranchDotService branchDotService,
			DeviceBindingService deviceBindingService,
			CarService carService){
		String result = null;
		try{
			String data = request.getParameter("data");
			//获取当前用户信息
			Subscriber s = TokenUtils.getSubscriber(data);
			//获取参数
			Map<String, String> map = JsonTools.desjsonForMap(data);
			if(map.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				return result;
			}
			String carId = map.get("carId");
			String orderId = map.get("orderId");
			 
			if(carId==null){
				result = Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
				return result;
			}
			if(orderId==null){
				result = Ajax.AppJsonResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG);
				return result;
			}
			Orders order = orderService.orderInfoById(orderId);
			if(!order.getState().equals("1")){
				result = Ajax.AppJsonResult(Constants.APP_ORDER_STATE, Constants.APP_ORDER_STATE_MSG);
				return result;
			}
			
			String ip = IpUtil.getIpAddr(request);//获取IP地址
			
			//开始用车
			result = orderService.startCar(s, carId,orderId,deviceBindingService,carService,ip);
			
			JSONObject jsonObject =JSONObject.fromObject(result);
			
			if(jsonObject.containsKey("resultCode")){
				
				if(jsonObject.getString("resultCode").equals("200")){
					//移除redis中存放的预约订单信息
					OrderRedisData.delOrder(s.getId());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
		}
		return result;
	}
	
	
}
