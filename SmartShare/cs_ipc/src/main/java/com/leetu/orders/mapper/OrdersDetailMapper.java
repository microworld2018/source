package com.leetu.orders.mapper;


import java.math.BigDecimal;

import org.apache.ibatis.annotations.Param;

import com.leetu.orders.entity.OrdersDetail;


public interface OrdersDetailMapper {

	/**根据主订单ID获取订单详情
	 * @param ordersId 主订单ID
	 * @return
	 */
	public OrdersDetail getByOrderIdDetail(@Param("ordersId")String ordersId);
	
	
	/**根据主订单ID更新订单详情
	 * @param orderId
	 */
	public void updateByOrdersIdDetil(@Param("orderId")String orderId,@Param("mileage")String mileage,@Param("endTime")String endTime);

	/**
	 * 根据订单号更新详情表的第三方支付金额
	 * @author Jin Guangyu
	 * @date 2017年7月29日下午2:57:23
	 * @param bigDecimal
	 * @param ordersNo
	 */
	public void updateOrderDetailTposPayFee(@Param("tposPayFee")BigDecimal tposPayFee, @Param("ordersNo")String ordersNo);

	/**
	 * 更新详情表的优惠券金额
	 * @author Jin Guangyu
	 * @date 2017年7月29日下午3:25:49
	 * @param id
	 * @param couponFee
	 */
	public void updateOrderDetailCoupon(@Param("id")String id, @Param("coupon_fee")Float couponFee, @Param("tposPayFee")Float tposPayFee);
}
