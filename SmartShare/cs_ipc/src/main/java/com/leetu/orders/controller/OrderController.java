package com.leetu.orders.controller;

import java.util.Hashtable;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.car.service.CarCommentService;
import com.leetu.car.service.CarService;
import com.leetu.coupon.service.CouponService;
import com.leetu.device.service.DeviceBindingService;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.param.OrderParam;
import com.leetu.orders.service.OrderService;
import com.leetu.orders.service.OrdersDetailService;
import com.leetu.orders.service.impl.OrderServiceImpl;
import com.leetu.place.entity.BranchDot;
import com.leetu.place.service.BranchDotService;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.mapper.SubScriberMapper;
import com.leetu.subscriber.service.SubScriberService;
import com.leetu.sys.service.DictService;
import com.util.Ajax;
import com.util.Constants;
import com.util.IpUtil;
import com.util.JsonTools;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.Utils;

/**
 * 订单
 * @author hzw
 *
 */
@Controller
@RequestMapping("order")
public class OrderController extends BaseController<OrderController> {

	@Autowired
	private OrderServiceImpl orderService;
	
	@Autowired
	private SubScriberService scriberService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private BranchDotService branchDotService;
	
	@Autowired
	private CouponService couponService;
	
	@Autowired
	private SubScriberMapper subScriberMapper;
	
	@Autowired
	private CarCommentService carCommentService;//车辆评价Service接口
	
	@Autowired
	private OrdersDetailService ordersDetailService;//订单详情Service接口
	
	@Autowired
	private DeviceBindingService deviceBindingService;
	
	@Autowired
	private CarService carService;
	
	/**
	 * 9 预约订单(取车地点)(1.0.1)
	 * @author hzw
	 */
	@RequestMapping("/advance")
	public void advance(){
		String result = OrderParam.advance(request, orderService, scriberService, dictService,branchDotService);
		Utils.outJSONObject(result,response);
	}
	
	/**后台下订单接口
	 * @param request
	 * @param s 会员实体类
	 * @date 2016-10-10
	 * @author Gaopl
	 */
	@RequestMapping("/backAddOrder")
	public void backAddOrder(HttpServletRequest request){
		String data = request.getParameter("data");
		JSONObject obj = JSONObject.fromObject(data);
		String dotId = (String) obj.get("branchDotId");
		String backDotId = (String) obj.get("branchDotId");
		String carId = (String) obj.get("carId");
		String subId = obj.getString("memberId");
		Subscriber sub = subScriberMapper.querySubscriberById(subId);
		String ip = "";
		try {
			ip = IpUtil.getIpAddr(request);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String result = orderService.advance(sub,carId,dotId,backDotId,ip);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 10查看当前订单(1.0.1)
	 */
	@RequestMapping("/lookNowOrder")
	public void lookNowOrder(){
		
		String result = null;
		 
		 try {
			
			result = orderService.lookNowOrder(request, orderService, scriberService, dictService,ordersDetailService,couponService);
			
		 } catch (Exception e) {
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
		
//		String result = OrderParam.lookNowOrder(request, orderService, scriberService, dictService,branchDotService,ordersDetailService,couponService);
//		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 12支付订单 信息(1.0.1)
	 */
	@RequestMapping("/lookpayOrder")
	public void lookpayOrder(){
		String result = orderService.lookpayOrder(request, orderService, scriberService, dictService,ordersDetailService,couponService);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 即将还车时的订单信息(1.0.1)
	 */
	@RequestMapping("/backCarOrder")
	public void backCarOrder(){
		String result = OrderParam.backCarOrder(request, orderService, scriberService, dictService,branchDotService,ordersDetailService,couponService);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 用户订单记录(1.0.1)
	 */
	@RequestMapping("/subOrderRecords")
	public void subOrderRecords(){
		String result = OrderParam.subOrderRecords(request, orderService);
		Utils.outJSONObject(result,response);
	}
	
	
	/**
	 * 11.保存编辑还车网点
	 * 
	 * @version 1.0.1
	 */
	@RequestMapping("/updateReturnDot")
	public  void updateReturnDot(){
		
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			String dotId = pmap.get("dotId");
			String orderId =pmap.get("orderId");
			if(null == s ||StringHelper.isEmpty(dotId) || StringHelper.isEmpty(orderId)){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				 Utils.outJSONObject(result,response);
			}
			this.orderService.updateOrderReturnDot(orderId,dotId);
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG );
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 恢复默认还车网点 --（取车网点）
	 * 
	 * @version 1.0.1
	 */
	@RequestMapping("/restoreDefaultDot")
	public void restoreDefaultDot(){
		String result =null;
		try{
			String data = request.getParameter("data");
			Map<String, String> pmap = JsonTools.desjsonForMap(data);
			if(pmap.get("param").equals("error")){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				Utils.outJSONObject(result,response);
			}
			Subscriber s = TokenUtils.getSubscriber(data);//获取会员信息
			String orderId =pmap.get("orderId");
			if(null == s || StringHelper.isEmpty(orderId)){
				result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
				 Utils.outJSONObject(result,response);
			}
			this.orderService.updateReturnDot2Default(orderId);
			Orders order = this.orderService.orderInfoById(orderId);
			
			BranchDot dot = this.branchDotService.getDotById(order.getEndSiteId());
			Map<String, Object> map = new Hashtable<String, Object>();
			map.put("dotId", dot.getId());
			map.put("name", dot.getName());
			map.put("latlng", dot.getLat()+","+dot.getLng());
			map.put("address", dot.getAddress());
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,map );
		}catch(Exception e){
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.RESULT_CODE_FAILED, "系统异常！");
		}
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 订单分享信息(1.0.1)
	 */
	@RequestMapping("/orderShare")
	public void orderShare() {
		String result = OrderParam.shareOrder(request, orderService);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 微信分享页面(1.0.1)
	 * @return
	 */
	@RequestMapping("/share")
	public String share() {
		OrderParam.shareOrder(request, orderService);
		String returnUrl = "/share/share";
		return returnUrl;
	}
	
	/**
	 * 取消订单
	 */
	@RequestMapping("/cancelOrder")
	public void cancelOrder(){
		String result = OrderParam.cancelOrder(request, orderService,dictService,branchDotService,scriberService);
		Utils.outJSONObject(result,response);
	}
	
	
	/**
	 * 查看订单详情
	 */
	@RequestMapping("/viewOrderDetails")
	public void viewOrderDetails(){
		String result = OrderParam.viewOrderDetails(request, orderService, dictService,branchDotService,carCommentService,ordersDetailService,couponService);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 开始用车
	 */
	@RequestMapping("/startCar")
	public void startCar(){
		String result = OrderParam.startCar(request, orderService, scriberService, dictService,branchDotService,deviceBindingService,carService);
		Utils.outJSONObject(result,response);
	}
	
	/**
	 * 订单详情时时计费
	 */
	@RequestMapping("/orderBilling")
	public void orderBilling(){
		
		String result = null;
		try {
			String data = request.getParameter("data");
			result = orderService.orderBilling(data);
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Utils.outJSONObject(result,response);
	}
	
}
