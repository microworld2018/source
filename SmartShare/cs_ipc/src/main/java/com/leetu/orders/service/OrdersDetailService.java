package com.leetu.orders.service;


import com.leetu.orders.entity.OrdersDetail;


/**订单详情Service接口
 * @author Gaopl
 * @date 2016-11-10
 */
public interface OrdersDetailService {

	/**根据主订单ID获取订单详情
	 * @param ordersId 主订单ID
	 * @return
	 */
	public OrdersDetail getByOrderIdDetail(String ordersId);
	
	/**根据主订单ID更新订单详情
	 * @param orderId
	 */
	public void updateByOrdersIdDetil(String orderId,String mileage,String endTime);
	
	/**
	 * 根据订单id查询订单详情表，计算用户购买保险单价
	 * @author Jin Guangyu
	 * @date 2017年7月25日上午11:23:39
	 * @param orderId
	 * @return
	 */
	public int getInsuranceFee(String orderId);
}
