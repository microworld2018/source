package com.leetu.app.entity;

import java.io.Serializable;
import java.util.Date;

public class App implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	//app名称
	private String name;
	//appurl
	private String url;
	//app版本号
	private String version;
	//是否更新
	private Integer isUpdate;
	//创建时间
	private Date createDate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public Integer getIsUpdate() {
		return isUpdate;
	}
	public void setIsUpdate(Integer isUpdate) {
		this.isUpdate = isUpdate;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
