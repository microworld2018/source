package com.leetu.app.mapper;

import com.leetu.app.entity.App;

public interface AppMapper {

	App getApp();
}
