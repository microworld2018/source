package com.leetu.app.service;

import java.util.Map;

public interface AppService {

	/**
	 * 获取app版本信息
	 * @return
	 */
	Map<String,Object> getApp();
}
