package com.leetu.app.controller;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.app.service.AppService;
import com.leetu.car.controller.CarController;
import com.leetu.place.controller.BranchDotController;
import com.util.Ajax;
import com.util.Constants;
import com.util.Utils;

@Controller
@RequestMapping("app")
public class AppController extends BaseController<CarController>{

	public static final Log logger = LogFactory.getLog(AppController.class);
	
	@Autowired
	private AppService appService;
	
	/**
	 * 获取app版本信息接口
	 */
	@RequestMapping("/getApp")
	public void getApp(){
		
		String result = "";
		try {
			Map<String, Object> resultMap = appService.getApp();
			
			if(resultMap != null && !"".equals(resultMap)){
				
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, resultMap);
			}else{
				result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
			}
			
		} catch (Exception e) {
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		Utils.outJSONObject(result,response); 
	}
}
