package com.leetu.app.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.app.entity.App;
import com.leetu.app.mapper.AppMapper;
import com.leetu.app.service.AppService;

@Service
public class AppServiceImpl implements AppService{

	@Autowired
	private AppMapper appMapper;
	
	@Override
	public Map<String,Object> getApp() {
		
		App app = appMapper.getApp();
		
		Map<String,Object> rm = new HashMap<String,Object>();
		
		if(app != null && !"".equals(app)){
			
			rm.put("name", app.getName());
			rm.put("version", app.getVersion());
			rm.put("url", app.getUrl());
			rm.put("isUpdate", app.getIsUpdate());
		}
		
		return rm;
	}

	
}
