package com.leetu.connector.service.impl;

import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.leetu.coupon.service.impl.SubCouponServiceImpl;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.service.impl.OrderServiceImpl;
import com.leetu.orders.util.OrderRedisData;
import com.leetu.place.service.impl.BranchDotServiceImpl;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.impl.SubscriberServiceImpl;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.entity.SMSRecord;
import com.leetu.sys.service.impl.DictServiceImpl;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.TokenUtils;
import com.util.msg.MsgContent;
import com.util.msg.sendMSg;

@Service
@Transactional
public class SystemInterfaceService {

	@Autowired
	private SubscriberServiceImpl subscriberService;
	
	@Autowired
	private OrderServiceImpl orderService;
	
	@Autowired
	private SubCouponServiceImpl subCouponService;
	
	@Autowired
	private DictServiceImpl dictService;
	
	@Autowired
	private BranchDotServiceImpl branchDotService;
	
	/**
	 * 短信发送接口-后台使用
	 * @param data
	 * @return
	 */
	public String smsSend(String data){
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		
		String mobile = "";//手机号
		
		String content = "";//发送内容
		
		String subId = "";//会员ID
		
		String name = "";//会员名称
		
		if(pmap.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		
		if(pmap.containsKey("moblie")){
			
			if(StringUtils.isEmpty(pmap.get("moblie"))){
				return Ajax.AppJsonResult(Constants.APP_CURRENT_PAGE_NULL, Constants.APP_CURRENT_PAGE_NULL_MSG);
			}
			
			mobile = pmap.get("moblie");
			
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(pmap.containsKey("content")){
			if(StringUtils.isEmpty(pmap.get("content"))){
				return Ajax.AppJsonResult(Constants.SYS_CONTENT_NULL, Constants.SYS_CONTENT_NULL_MSG);
			}
			content = pmap.get("content");
		}else{
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(pmap.containsKey("subId")){
			
			if(pmap.get("subId") == null || "".equals(pmap.get("subId"))){
				return Ajax.AppJsonResult(Constants.SYS_SUB_ID_NULL, Constants.SYS_SUB_ID_NULL_MSG);
			}
			subId = pmap.get("subId");
		}else{
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(pmap.containsKey("name")){
			
			if(pmap.get("name") == null || "".equals(pmap.get("name"))){
				return Ajax.AppJsonResult(Constants.SYS_NAME_NULL, Constants.SYS_NAME_NULL_MSG);
			}
			name = pmap.get("name");
		}else{
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(sendMSg.send(mobile,content)){
			//记录短信信息
			SMSRecord record = new SMSRecord();
			record.setContent(MsgContent.ORDER_CANCEL_COUNT_MSG);
			record.setPhoneNo(pmap.get("mobile"));
			record.setType(3);
			record.setUserId(pmap.get("subId"));
			record.setUserName(pmap.get("name"));
			record.setResult(pmap.get("mobile")+":Success");
			subscriberService.addSmsRecord(record);
			
			System.out.println("======开始发送短信=====");
			
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}else{
			result = Ajax.AppJsonResult(Constants.APP_ERROR, Constants.APP_ERROR_MSG);
		}
		
		return result;
	}
	
	/**
	 * 取消订单
	 * @param data
	 * @return
	 */
	public String cancelOrder(String data){
		
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		if(pmap.get("param").equals("error")){
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		String orderId = pmap.get("orderId");
		
		if(orderId == null || "".equals(orderId)){
			return Ajax.AppJsonResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG);
		}
		
		Orders order = orderService.orderInfoById(orderId);
		
		if(order == null || "".equals(order)){
			return Ajax.AppJsonResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG);
		}
		
		//获取用户预约订单
		Map<String, Object> torders = OrderRedisData.yyorder(order.getMemberId());
		Subscriber sub = subscriberService.querySubscriberById(order.getMemberId());
		
		
		Integer status = 0;//0预约取消 1预约成功 2预约超时 3租车开始 4租车结束
		
		String content = MsgContent.getSmsMsg(MsgContent.ORDER_CANCLE_MSG, sub.getName(), order.getOrdersNo());
		
		
		System.out.println("数据======="+torders);
		
		if(null!=torders&&!torders.isEmpty()&&torders.size()>0){
			//获取字典表中车辆未租借状态
			Dict di = dictService.getDictByCodes("carBizState", "0");
			
			String carId = torders.get("carId").toString();
			
			//获取 车辆信息
			Map<String, Object> car = branchDotService.carinfo(carId,null);
			
			
			String version = car.get("version").toString();
			String mobile = torders.get("mobile").toString();
			String userName = torders.get("userName").toString();
			
//			String createTime = torders.get("createTime").toString();
			//更新车辆状态
			int st = branchDotService.updateCarStatus(carId,di.getId(),version);
			if(st==1){
				//更新订单状态
				orderService.updateOrderStatus(order.getId(), status);
				orderService.updateOrderDetailStatus(order.getId());
				if(sendMSg.send(mobile, content)){
					//记录短信信息
					SMSRecord record = new SMSRecord();
					record.setContent(content);
					record.setPhoneNo(mobile);
					record.setType(3);
					record.setUserId(sub.getId());
					record.setUserName(userName);
					record.setResult(mobile+":Success");
					subscriberService.addSmsRecord(record);
				}
				//从redis中删除会员 预约订单信息
//				OrderRedisData.delOrder(sub.getId());
				//获取所有预约中的订单信息
				Hashtable<String,Map<String,Map<String,Object>>> oldordertimes = OrderRedisData.oldordertimes();
				
				for(Entry<String, Map<String, Map<String, Object>>> entry:oldordertimes.entrySet()){
					String createTime = entry.getKey();
					
					Map<String, Map<String, Object>> maps = entry.getValue();
					
					for(Entry<String, Map<String, Object>> en:maps.entrySet()){
						
						String sb = en.getKey();
						
						if(sub.getId().equals(sb)){
							OrderRedisData.delredisTimeOrder(createTime);
						}
					}
				}
				
				result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
			}
			
		}else{
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}
		
		return result;
	}
}
