package com.leetu.connector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.connector.service.impl.SystemInterfaceService;
import com.util.Ajax;
import com.util.Constants;
import com.util.Utils;

/**
 * 后台系统统一接口
 * @author GodZilla
 *
 */
@Controller
@RequestMapping("/system")
public class SystemInterfaceController extends BaseController<SystemInterfaceController>{

	public static final Log logger = LogFactory.getLog(SystemInterfaceController.class);
	
	@Autowired
	private SystemInterfaceService systemInterfaceService;
	
	/**
	 * 短信发送接口-后台使用
	 */
	@RequestMapping("/smsSend")
	public void smsSend(){
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = systemInterfaceService.smsSend(data);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 取消订单接口-后台使用
	 */
	@RequestMapping("/cancelOrder")
	public void cancelOrder(){
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = systemInterfaceService.cancelOrder(data);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
}
