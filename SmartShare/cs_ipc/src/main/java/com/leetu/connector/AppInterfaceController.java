package com.leetu.connector;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.core.controller.BaseController;
import com.leetu.connector.service.impl.AppInterfaceService;
import com.util.Ajax;
import com.util.Constants;
import com.util.Utils;

/**
 * app统一接口
 * @author GodZilla
 *
 */
@Controller
@RequestMapping("/app")
public class AppInterfaceController extends BaseController<AppInterfaceController>{
	public static final Log logger = LogFactory.getLog(AppInterfaceController.class);
	
	@Autowired
	private AppInterfaceService appInterfaceService;
	
	
	
	/**
	 * 我的押金-app接口
	 * version 1.0.1
	 */
	@RequestMapping("/getDeposit")
	public void getDeposit(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.getSubDeposit(data);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 押金充值金额
	 * version 1.0.1 
	 */
	@RequestMapping("/depositAmount")
	public void depositAmount(){
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.depositAmount(data);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 押金充值
	 * version 1.0.1
	 */
	@RequestMapping("/topUpDeposit")
	public void topUpDeposit(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.topUpDeposit(data,request);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 会员押金充值-异步通知 微信回调
	 * version 1.0.1
	 */
	@RequestMapping("/wxCallBack")
	public void wxCallBack(){
		System.out.println("进入微信回调函数");
		String result = appInterfaceService.wxCallBack(request);
		try {
			PrintWriter pw = response.getWriter();
			pw.print(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 会员押金充值-异步通知 支付宝回调
	 */
	@RequestMapping("/alipayCallBack")
	public void alipayCallBack(){
		
		System.out.println("进入支付宝回调函数地址==http://114.215.143.226:8081/cs_ipc/app/alipayCallBack");
		
		String result = null;
		
		 try {
			
			result = appInterfaceService.alipayCallBack(request);
			
			PrintWriter pw = response.getWriter();
			pw.print(result);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
	}
	
	/**
	 * 我的钱包-app接口
	 * version 1.0.1
	 */
	@RequestMapping("/getMyWallet")
	public void getMyWallet(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.getMyWallet(data);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			e.printStackTrace();
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 会员违章事故处理-app接口
	 * version 1.0.1
	 */
	@RequestMapping("/getAccident")
	public void getAccident(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.getAccident(data);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 会员违章事故支付-app接口
	 * version 1.0.1
	 */
	@RequestMapping("/payAccident")
	public void payAccident(){
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.payAccident(data,request);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 会员违章事故支付-异步通知 支付宝回调
	 * version 1.0.1
	 */
	@RequestMapping("/alipayCallBackAccident")
	public void alipayCallBackAccident(){
		System.out.println("进入支付宝回调函数地址==http://114.215.143.226:8081/cs_ipc/app/alipayCallBackAccident");
		
		String result = null;
		
		 try {
			
			result = appInterfaceService.alipayCallBackAccident(request);
			
			PrintWriter pw = response.getWriter();
			pw.print(result);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
	}
	
	/**
	 * 会员违章事故支付-异步通知 微信回调
	 * version 1.0.1
	 */
	@RequestMapping("/wxCallBackAccident")
	public void wxCallBackAccident(){
		System.out.println("进入微信回调函数===wxCallBackAccident");
		String result = appInterfaceService.wxCallBackAccident(request);
		try {
			PrintWriter pw = response.getWriter();
			pw.print(result);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 押金退还-app接口
	 * version 1.0.1
	 */
	@RequestMapping("/depositRefund")
	public void depositRefund(){
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.depositRefund(data);
			
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 预约取车-app接口
	 * version 1.0.1
	 */
	@RequestMapping("/advance")
	public void advance(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
//			result = appInterfaceService.advance(data,request);
			result = Ajax.AppJsonResult(Constants.SYS_VERSION_OLD, Constants.SYS_VERSION_OLD_MSG);
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 预约取车-app接口增加保险功能
	 * version 1.0.1
	 */
	@RequestMapping("/advanceNew")
	public void advanceNew(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
//			result = appInterfaceService.advance(data,request);
			result = Ajax.AppJsonResult(Constants.SYS_VERSION_OLD, Constants.SYS_VERSION_OLD_MSG);
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 预约取车-app接口，封优惠券抵扣保险
	 * version 1.6.0
	 * @author Jin Guangyu
	 * @date 2017年8月2日下午4:45:47
	 */
	@RequestMapping("/reserve")
	public void reserve(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.advance(data,request);
		 } catch (Exception e) {
			
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 确认用车-app接口
	 * version 1.0.2
	 */
	@RequestMapping("/confirmCar")
	public void confirmCar(){
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.confirmCar(data);
//			result = Ajax.AppJsonResult(Constants.SYS_VERSION_OLD, Constants.SYS_VERSION_OLD_MSG);
			
		 } catch (Exception e) {
			e.printStackTrace(); 
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 Utils.outJSONObject(result,response);
	}
	
	
	/**
	 * 确认用车-app接口(增加手持照片认证)
	 * version 1.5.1
	 */
	@RequestMapping("/confirmCar1.5.1")
	public void confirmCarNew(){
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.confirmCarNew(data);
			
		 } catch (Exception e) {
			
			e.printStackTrace(); 
			 
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 支付订单信息 -app接口
	 * version 1.0.2
	 */
	@RequestMapping("/lookpayOrder")
	public void lookpayOrder(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.lookpayOrder(data);
			
		 } catch (Exception e) {
			
			e.printStackTrace(); 
			 
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 查看当前订单 -app接口
	 * version 1.0.2
	 */
	@RequestMapping("/lookNowOrder")
	public void lookNowOrder(){
		
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.lookNowOrder(data);
			
		 } catch (Exception e) {
			
			e.printStackTrace(); 
			 
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
	/**
	 * 查看订单详情-app接口
	 * version 1.0.2
	 */
	@RequestMapping("/viewOrderDetails")
	public void viewOrderDetails(){
		String result = null;
		 
		 try {
			
			String data = request.getParameter("data");
			
			result = appInterfaceService.viewOrderDetails(data);
			
		 } catch (Exception e) {
			
			e.printStackTrace(); 
			 
			logger.error(Constants.APP_SERVER_EXCEPTION_MSG+":"+e.getMessage()); 
			
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		 }
		 
		 Utils.outJSONObject(result,response);
	}
	
}
