package com.leetu.connector.service.impl;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.jdom.JDOMException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.leetu.accident.service.impl.AccidentRecordServiceImpl;
import com.leetu.account.entity.Account;
import com.leetu.account.entity.AccountTradeRecord;
import com.leetu.account.service.impl.AccountServiceImpl;
import com.leetu.account.service.impl.AccountTradeRecordServiceImpl;
import com.leetu.account.util.TradeRecordNo;
import com.leetu.car.entity.CarComment;
import com.leetu.car.entity.RealtimeState;
import com.leetu.car.service.impl.CarCommentServiceImp;
import com.leetu.car.service.impl.RealTimeStateServiceImpl;
import com.leetu.coupon.entity.Coupon;
import com.leetu.coupon.entity.SubCoupon;
import com.leetu.coupon.mapper.SubCouponMapper;
import com.leetu.coupon.service.impl.CouponServiceImpl;
import com.leetu.coupon.service.impl.SubCouponServiceImpl;
import com.leetu.deposit.entity.DepositStrategy;
import com.leetu.deposit.entity.RefundRecord;
import com.leetu.deposit.service.impl.DepositRecordServiceImpl;
import com.leetu.deposit.service.impl.DepositServiceImpl;
import com.leetu.deposit.service.impl.DepositStrategyServiceImpl;
import com.leetu.deposit.service.impl.RefundRecordServiceImpl;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.feestrategy.service.impl.StrategyBaseServiceImpl;
import com.leetu.insurance.service.impl.InsuranceServiceImpl;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.entity.OrdersDetail;
import com.leetu.orders.service.impl.OrderServiceImpl;
import com.leetu.orders.service.impl.OrdersDetailServiceImpl;
import com.leetu.orders.util.OrderFee;
import com.leetu.orders.util.OrderRedisData;
import com.leetu.place.service.impl.BranchDotServiceImpl;
import com.leetu.subscriber.entity.Subscriber;
import com.leetu.subscriber.service.impl.SubscriberServiceImpl;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.entity.RunLog;
import com.leetu.sys.entity.SMSRecord;
import com.leetu.sys.service.impl.DictServiceImpl;
import com.leetu.sys.service.impl.RunLogServiceImpl;
import com.util.Ajax;
import com.util.ArithUtil;
import com.util.Constants;
import com.util.IpUtil;
import com.util.JsonTools;
import com.util.Md5Util;
import com.util.StringHelper;
import com.util.TokenUtils;
import com.util.ToolDateTime;
import com.util.alipay.config.AlipayConfig;
import com.util.alipay.pay.Alipay;
import com.util.car.CurrentCarInfo;
import com.util.charging.Constant.CouponType;
import com.util.charging.Constant.UseCarMode;
import com.util.charging.calculate.FeesAlg;
import com.util.msg.MsgContent;
import com.util.msg.sendMSg;
import com.util.orders.OrderNoUtil;
import com.util.page.Pager;
import com.util.weixin.Constant;
import com.util.weixin.pay.pay.WeixinPay;
import com.util.weixin.tool.HttpTool;

@Service
@Transactional
public class AppInterfaceService {

	@Autowired
	private DepositRecordServiceImpl depositRecordService;
	
	@Autowired
	private DepositServiceImpl depositService;
	
	@Autowired
	private DepositStrategyServiceImpl depositStrategyService;
	
	@Autowired
	private AccountServiceImpl accountService;
	
	@Autowired
	private SubCouponServiceImpl subCouponService;
	
	@Autowired
	private AccidentRecordServiceImpl accidentRecordService;
	
	@Autowired
	private AccountTradeRecordServiceImpl accountTradeRecordService;
	
	@Autowired
	private OrderServiceImpl orderService;
	
	@Autowired
	private OrdersDetailServiceImpl ordersDetailService;
	
	@Autowired
	private RefundRecordServiceImpl refundRecordService;
	
	@Autowired
	private StrategyBaseServiceImpl strategyBaseService;
	
	@Autowired
	private DictServiceImpl dictService;
	
	@Autowired
	private BranchDotServiceImpl branchDotService;
	
	@Autowired
	private RealTimeStateServiceImpl realTimeStateService;
	
	@Autowired
	private SubscriberServiceImpl subscriberService;
	
	@Autowired
	private RunLogServiceImpl runLogService;
	
	@Autowired
	private InsuranceServiceImpl insuranceService;
	
	@Autowired
	private CouponServiceImpl couponService;
	
	@Autowired
	private CarCommentServiceImp carCommentService;
	
	@Autowired
	private SubCouponMapper subCouponMapper;
	
	/**
	 * 我的押金-app接口
	 * @param data
	 * @return String
	 * @version 1.0.1
	 */
	public String getSubDeposit(String data) {
		
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		if(pmap.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		
		if(pmap.containsKey("currentPage")){
			
			if(pmap.get("currentPage") == null || "".equals(pmap.get("currentPage"))){
				return Ajax.AppJsonResult(Constants.APP_CURRENT_PAGE_NULL, Constants.APP_CURRENT_PAGE_NULL_MSG);
			}
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(StringUtils.isEmpty(subscriber)){
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		Map<String, Object> res = new Hashtable<String, Object>();
		//获取会员押金金额
		Map<String, Object> depositMap = depositService.getDepositBySubId(subscriber.getId());
		
		double deposit = 0;//用户押金金额
		
		if(!StringUtils.isEmpty(depositMap)){
			deposit = Double.valueOf(depositMap.get("usableAmount").toString());
		}
		
		res.put("deposit", deposit);//用户押金金额
		int countNum = depositRecordService.getDepositRecordCount(subscriber.getId());
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		Pager pager = new Pager();
		pager.setCurPage(Integer.parseInt(pmap.get("currentPage")));//当前页
		pager.setPageSize(10);//每页显示数量
		pager.setTotalRow(countNum); //共条数据
		pager.setStart(pager.getStart()); //开始行
		paramMap.put("pager", pager);
		paramMap.put("subId", subscriber.getId());
		
		List<Map<?, ?>> depositRecordList = depositRecordService.getDepositRecordBySubId(paramMap);
		
		res.put("recordList", depositRecordList);
		
		//获取押金规则金额
		Map<String, Object> depositStrategyMap = depositStrategyService.getDepositStrategy("0");
		
		double strategyMoney = 0;//押金规则金额
		
		if(!StringUtils.isEmpty(depositStrategyMap)){
			strategyMoney =Double.valueOf(depositStrategyMap.get("money").toString());
		}
		res.put("strategyMoney", strategyMoney);
		
		result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		
		return result;
	}
	
	/**
	 * 押金充值金额
	 * @param data
	 * @return String
	 * version 1.0.1 
	 */
	public String depositAmount(String data){
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		Map<String, Object> res = new Hashtable<String, Object>();
		
		double amount = 0;//押金充值金额
		
		if(pmap.get("param").equals("error")){

			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(StringUtils.isEmpty(subscriber)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		
		//判断会员是否上传手持照片
		if(StringUtils.isEmpty(subscriber.getHandHeldIdCard())){
			return Ajax.AppJsonResult(Constants.APP_HANDHELDIDCARD_NULL,Constants.APP_HANDHELDIDCARD_NULL_MSG);
		}
		
		//根据会员ID获取会员押金信息
		Map<String, Object> depositMap = depositService.getDepositBySubId(subscriber.getId());
				
		//获取押金规则金额
		Map<String, Object> depositStrategyMap = depositStrategyService.getDepositStrategy(DepositStrategy.TYPE_DEPOSIT);
		//押金规则金额
		double depositMoney = 0;
		
		if(!StringUtils.isEmpty(depositStrategyMap)){
			
			//如果押金规则金额大于0
			if(Double.valueOf(depositStrategyMap.get("money").toString())> 0){
				depositMoney = Double.valueOf(depositStrategyMap.get("money").toString());
			}
		}else{
			System.out.println("押金规则金额为空！！！！");
		}
		
		if(!StringUtils.isEmpty(depositMap)){
			amount = Double.valueOf(depositMap.get("usableAmount").toString());
			if(depositMoney > 0){
				//如果押金金额大于等于押金规则金额，充值金额为0
				if(Double.valueOf(depositMap.get("usableAmount").toString()) >= depositMoney){
					amount = 0;
				}else{
					//如果押金规则金额大于0 。押金充值金额=押金规则金额-会员账户押金金额
					amount = ArithUtil.sub(depositMoney, amount);
				}
				
			}
		}else{//如果用户押金为空，默认充值金额为 押金规则金额
			amount = depositMoney;
		}
		
		res.put("amount", amount);
		
		return Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		
	}
	
	/**
	 * 押金充值
	 * @param data
	 * @param request
	 * @return String
	 * version 1.0.1
	 */
	public String topUpDeposit(String data,HttpServletRequest request){
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		double amount = 0;//押金充值金额
		
		String payType = "";//支付类型
		
		if(pmap.get("param").equals("error")){

			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(pmap.containsKey("payType")){
			
			if(StringUtils.isEmpty(pmap.get("payType"))){
				
				return Ajax.AppJsonResult(Constants.APP_PAYTYPE_NULL, Constants.APP_PAYTYPE_NULL_MSG);
			}else{
				payType = pmap.get("payType");
			}
			
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(StringUtils.isEmpty(subscriber)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		//根据会员ID获取会员押金信息
		Map<String, Object> depositMap = depositService.getDepositBySubId(subscriber.getId());
		
		//获取押金规则金额
		Map<String, Object> depositStrategyMap = depositStrategyService.getDepositStrategy(DepositStrategy.TYPE_DEPOSIT);
		
		//押金规则金额
		double depositMoney = 0;
		
		if(!StringUtils.isEmpty(depositStrategyMap)){
			
			//如果押金规则金额大于0
			if(Double.valueOf(depositStrategyMap.get("money").toString())> 0){
				depositMoney = Double.valueOf(depositStrategyMap.get("money").toString());
			}
		}else{
			System.out.println("押金规则金额为空！！！！");
		}
		
		if(!StringUtils.isEmpty(depositMap)){
			amount = Double.valueOf(depositMap.get("usableAmount").toString());
			if(depositMoney > 0){
				//如果押金金额大于等于押金规则金额，充值金额为0
				if(Double.valueOf(depositMap.get("usableAmount").toString()) >= depositMoney){
					amount = 0;
				}else{
					//如果押金规则金额大于0 。押金充值金额=押金规则金额-会员账户押金金额
					amount = ArithUtil.sub(depositMoney, amount);
				}
				
			}
		}else{//如果用户押金为空，默认充值金额为 押金规则金额
			amount = depositMoney;
		}
		//如果充值金额小于0，不做充值
		if(amount <= 0){
			return Ajax.AppJsonResult(Constants.APP_RECHARGE_AMOUNT_NO_ZERO,Constants.APP_RECHARGE_AMOUNT_NO_ZERO_MSG);
		}
		
		//预支付成功后   1  添加交易信息 2添加押金记录信息 
		String description = "";
		
		//交易编号
		String tradeRecordNo = TradeRecordNo.getPayRechargeTradeNo();
		
		JSONObject json = new JSONObject();
		
		//交易记录数据
		Map<String,Object> accountTradeParam = new Hashtable<String,Object>();
		
		accountTradeParam.put("subscriber_id",subscriber.getId());
		
		accountTradeParam.put("pay_channel",Account.PAY_CHANNEL_APP);//渠道
		
		accountTradeParam.put("amount",amount);//交易金额
		
		accountTradeParam.put("result",0);//交易状态
		
		accountTradeParam.put("order_index",1);
		
		accountTradeParam.put("trade_time","t");
		
		accountTradeParam.put("biz_id","");//订单号
		
		accountTradeParam.put("sub_orderId","");//字订单
		
		accountTradeParam.put("remaining_amount","0");//账户余额
		
		accountTradeParam.put("is_auto_clear",0);
		
		accountTradeParam.put("trade_order_no",tradeRecordNo);//交易编号
		
		
		if(payType.equals(String.valueOf(Orders.PAY_TYPE_ALIPAY))){//支付宝充值
			
			description = "支付宝押金充值"+amount+"元";
			
			String body = Constants.BODY_DEPOSIT;
			
			JSONObject alipay = Alipay.ydalipay(tradeRecordNo, String.valueOf(amount), body,AlipayConfig.deposit_url);
			
			json.element("alipay", alipay);
			
			accountTradeParam.put("type",Account.TYPE_DEPOSIT);//消费类型
			
			accountTradeParam.put("pay_type",payType);//支付类型
			
			accountTradeParam.put("description",description);//交易描述
			
			//添加交易记录
			accountTradeRecordService.addtradeRecord(accountTradeParam);
			
		}else if(payType.equals(String.valueOf(Orders.PAY_TYPE_WECHAT))){//微信充值
			
			description = "微信押金充值"+amount+"元";
			
			String remoteAddress = request.getRemoteAddr();
			
			String body = Constants.BODY_DEPOSIT;
			
			JSONObject wx;
			
			try {
				wx = WeixinPay.wxpay(remoteAddress, tradeRecordNo, body, String.valueOf(amount),Constant.DEPOSIT_URL);
				System.out.println("微信统一下单返回数据:"+wx);
				json.element("wx", wx);
				//微信统一下单异常
				if(wx.getJSONObject("order").getString("return_code").equals("FAIL")){
					return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
				}
				
			} catch (JDOMException e) {
				e.printStackTrace();
				return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			} catch (IOException e) {
				e.printStackTrace();
				return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			}
			
			
			accountTradeParam.put("pay_type",payType);//支付类型
			
			accountTradeParam.put("description",description);//交易描述
			
			accountTradeParam.put("type",Account.TYPE_DEPOSIT);//消费类型
			
			accountTradeParam.put("description",description);//交易描述
			
			//添加交易记录
			accountTradeRecordService.addtradeRecord(accountTradeParam);
			
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("subId", subscriber.getId());//会员ID
		paramMap.put("type", "0");//类型 0:押金充值 1:押金扣款
		paramMap.put("money", amount);//充值金额
		paramMap.put("desc", description);//描述
		paramMap.put("tradeOrderNo", tradeRecordNo);//交易编号
		paramMap.put("payStatus", "0");//交易状态 0:失败 1:成功
		
		//添加会员押金记录
		depositRecordService.addDepositRecord(paramMap);
		
		return Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,json);
	}
	
	/**
	 * 会员押金充值-异步通知 微信回调
	 * version 1.0.1
	 * @param request
	 * @return
	 */
	public String wxCallBack(HttpServletRequest request){
		
		Map<String,String> map = new HashMap<String,String>();
		try {
			Map<String,Object> orderMap = WeixinPay.callBackUrl(request);
			
			System.out.println("微信回调函数返回值======"+orderMap);
			
			if(orderMap.get("status").toString().equals("ok")){
				map.put("return_code", orderMap.get("return_code").toString());
				map.put("return_msg", orderMap.get("return_msg").toString());
				
				String tradeRecordNo = orderMap.get("orderNo").toString();//交易编号
				String total_fee = orderMap.get("total_fee").toString();//实际充值金额 单位：分
				total_fee = String.valueOf(ArithUtil.div(Double.valueOf(total_fee), 100));//转换单位:元
				System.out.println("充值单位转换==="+total_fee+"元");
				//根据交易编号修改交易记录状态
				accountTradeRecordService.updateRecordStateByTradeOrderNo(tradeRecordNo);
				//根据交易编号修改会员押金记录状态
				depositRecordService.updateDepositRecordByTradeOrderNo(tradeRecordNo);
				//获取会员押金金额记录信息
				Map<String, Object> depositRecordMap = depositRecordService.getDepositRecordByRradeOrderNo(tradeRecordNo);
			
				System.out.println("会员押金记录信息===="+depositRecordMap);
				
				if(!StringUtils.isEmpty(depositRecordMap)){
					
					String subId = depositRecordMap.get("subId").toString();
					//根据会员ID获取会员押金信息
					Map<String, Object> depositMap = depositService.getDepositBySubId(subId);
					
					System.out.println("会员押金信息==="+depositMap);
					
					
					if(!StringUtils.isEmpty(depositMap)){
						//押金金额
						double amount = Double.valueOf(depositMap.get("amount").toString());
						System.out.println("押金金额==="+amount);
						//冻结金额
						double frozenAmount = Double.valueOf(depositMap.get("frozenAmount").toString());
						System.out.println("冻结金额==="+frozenAmount);
						//可用金额
						double usableAmount = Double.valueOf(depositMap.get("usableAmount").toString());
						System.out.println("可用金额==="+usableAmount);
						
						//押金金额=原有押金+充值金额
						amount = ArithUtil.add(amount, Double.valueOf(total_fee));
						//可用金额=押金金额-冻结金额
						usableAmount = ArithUtil.sub(amount, frozenAmount);
						
						Map<String, Object> paramMap = new HashMap<String, Object>();
						
						paramMap.put("amount", amount);//押金金额
						paramMap.put("frozenAmount", frozenAmount);//冻结金额
						paramMap.put("usableAmount", usableAmount);//可用金额
						paramMap.put("subId", subId);//会员ID
						
						System.out.println("修改会员押金信息===="+paramMap);
						//根据会员ID修改会员押金金额
						depositService.updateDepositBySubId(paramMap);
					}else{
						
						Map<String, Object> paramMap = new HashMap<String, Object>();
						
						paramMap.put("subId", subId);//用户ID
						paramMap.put("amount", total_fee);//押金金额
						paramMap.put("frozenAmount", 0);//冻结金额
						paramMap.put("usableAmount", total_fee);//可用金额
						paramMap.put("isRefund", "0");//是否退款
						
						System.out.println("添加会员押金==="+paramMap);
						//添加会员押金
						depositService.addDeposit(paramMap);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String result = HttpTool.mapToXml(map);
		System.out.println("微信异步校验支付成功后返回结果==="+result);
		return result;
	}
	
	/**
	 * 会员押金充值-异步通知支付宝回调
	 * version 1.0.1
	 * @param request
	 * @return
	 */
	public String alipayCallBack(HttpServletRequest request){
		
		String result = "fail";
		
		//获取支付宝POST过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		@SuppressWarnings("unchecked")
		Map<String,Object> requestParams = request.getParameterMap();
		
		System.out.println("支付宝回调函数返回结果====="+requestParams.toString());
		
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
				: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
			params.put(name, valueStr);
		}
		
		System.out.println("params====="+params.toString());
		String tradeStatus = params.get("trade_status");//获取支付宝返回结果状态码
		
		System.out.println("result====="+tradeStatus);
		/*Map<String, Object> maps = new HashMap<String, Object>();
		String[] str = result1.split("&");
		for(int i=0;i<str.length;i++){
			String[] str1 = str[i].split("=");
			maps.put(str1[0], str1[1]);
		}*/
//		String resultStatus = params.get("resultStatus");
//		String outTradeNo = (String) maps.get("out_trade_no");
//		outTradeNo.replaceAll("\"","");
		if(tradeStatus.equals("TRADE_SUCCESS")){
//			String tradeRecordNo = maps.get("out_trade_no").toString().replaceAll("\"","");//交易编号
			String outTradeNo = params.get("out_trade_no");//交易号
//			String total_fee = maps.get("total_fee").toString();//实际充值金额
			
			String total_fee = params.get("total_fee");//实际充值金额
			
			//根据交易编号修改交易记录状态
			accountTradeRecordService.updateRecordStateByTradeOrderNo(outTradeNo);
			//根据交易编号修改会员押金记录状态
			depositRecordService.updateDepositRecordByTradeOrderNo(outTradeNo);
			//获取会员押金金额记录信息
			Map<String, Object> depositRecordMap = depositRecordService.getDepositRecordByRradeOrderNo(outTradeNo);
		
			System.out.println("会员押金记录信息===="+depositRecordMap);
			
			if(!StringUtils.isEmpty(depositRecordMap)){
				
				String subId = depositRecordMap.get("subId").toString();
				//根据会员ID获取会员押金信息
				Map<String, Object> depositMap = depositService.getDepositBySubId(subId);
				
				System.out.println("会员押金信息==="+depositMap);
				
				
				if(!StringUtils.isEmpty(depositMap)){
					//押金金额
					double amount = Double.valueOf(depositMap.get("amount").toString());
					System.out.println("押金金额==="+amount);
					//冻结金额
					double frozenAmount = Double.valueOf(depositMap.get("frozenAmount").toString());
					System.out.println("冻结金额==="+frozenAmount);
					//可用金额
					double usableAmount = Double.valueOf(depositMap.get("usableAmount").toString());
					System.out.println("可用金额==="+usableAmount);
					
					//押金金额=原有押金+充值金额
					amount = ArithUtil.add(amount, Double.valueOf(total_fee));
					//可用金额=押金金额-冻结金额
					usableAmount = ArithUtil.sub(amount, frozenAmount);
					
					Map<String, Object> paramMap = new HashMap<String, Object>();
					
					paramMap.put("amount", amount);//押金金额
					paramMap.put("frozenAmount", frozenAmount);//冻结金额
					paramMap.put("usableAmount", usableAmount);//可用金额
					paramMap.put("subId", subId);//会员ID
					
					System.out.println("修改会员押金信息===="+paramMap);
					//根据会员ID修改会员押金金额
					depositService.updateDepositBySubId(paramMap);
				}else{
					
					Map<String, Object> paramMap = new HashMap<String, Object>();
					
					paramMap.put("subId", subId);//用户ID
					paramMap.put("amount", total_fee);//押金金额
					paramMap.put("frozenAmount", 0);//冻结金额
					paramMap.put("usableAmount", total_fee);//可用金额
					paramMap.put("isRefund", "0");//是否退款
					
					System.out.println("添加会员押金==="+paramMap);
					//添加会员押金
					depositService.addDeposit(paramMap);
				}
			}
			
			result = "success";
			
		}
		
		return result;
	}
	
	
	/**
	 * 我的钱包-app接口
	 * @param data
	 * @return String
	 * @version 1.0.1
	 */
	public String getMyWallet(String data){
		
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		if(pmap.get("param").equals("error")){

			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(StringUtils.isEmpty(subscriber)){
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		Map<String, Object> res = new Hashtable<String, Object>();
		//获取会员押金金额
		Map<String, Object> depositMap = depositService.getDepositBySubId(subscriber.getId());
		
		double deposit = 0;//用户押金金额
		
		if(!StringUtils.isEmpty(depositMap)){
			deposit = Double.valueOf(depositMap.get("usableAmount").toString());
		}
		
		res.put("deposit", deposit);//用户押金金额
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		SubCoupon subCoupon = new SubCoupon();
		subCoupon.setSubId(subscriber.getId());
		subCoupon.setIsUsed(SubCoupon.SUB_COUPON_IS_USED_NO);//未使用
		subCoupon.setExpirationTime(new Date());//过期时间
		
		paramMap.put("subCoupon", subCoupon);
		
		//获取会员可用优惠卷数量
		Integer couponCount = subCouponService.getSubCouponUsedCount(paramMap);
		
		res.put("couponCount", couponCount);//用户优惠卷可用数量
		
		//获取押金规则金额信息
		Map<String, Object> depositStrategy = depositStrategyService.getDepositStrategy("0");
		
		if(!StringUtils.isEmpty(depositStrategy)){
			//押金定义规则金额
			res.put("depositMoney", depositStrategy.get("money"));
		}else{
			res.put("depositMoney", "0");
		}
		
		
		double amount = 0;//用户账户金额
		
		//获取会员账户金额
		Account account = accountService.queryAccountBySubscriberId(subscriber.getId());
		
		if(!StringUtils.isEmpty(account)){
			amount = account.getUsableAmount();
		}
		
		res.put("amount", amount);//用户账户金额
		
		result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		
		return result;
	}
	
	/**
	 * 会员违章事故处理-app接口
	 * @param data
	 * @return result
	 * version 1.0.1
	 */
	public String getAccident(String data){
		
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		//校验参数
		if(pmap.get("param").equals("error")){

			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(pmap.containsKey("currentPage")){
			
			if(pmap.get("currentPage") == null || "".equals(pmap.get("currentPage"))){
				
				return Ajax.AppJsonResult(Constants.APP_CURRENT_PAGE_NULL, Constants.APP_CURRENT_PAGE_NULL_MSG);
			}
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(StringUtils.isEmpty(subscriber)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		Map<String, Object> res = new Hashtable<String, Object>();
		
		int countNum = accidentRecordService.getAccidentRecordCount(subscriber.getId());
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		Pager pager = new Pager();
		pager.setCurPage(Integer.parseInt(pmap.get("currentPage")));//当前页
		pager.setPageSize(10);//每页显示数量
		pager.setTotalRow(countNum); //共条数据
		pager.setStart(pager.getStart()); //开始行
		paramMap.put("pager", pager);
		paramMap.put("subId", subscriber.getId());
		
		List<Map<?, ?>> accidentRecordList = accidentRecordService.getAccidentRecordBySubId(paramMap);
		
		res.put("accidentList", accidentRecordList);
		
		result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		
		return result;
	}
	
	/**
	 * 会员违章事故支付-app接口
	 * version 1.0.1
	 */
	public String payAccident(String data,HttpServletRequest request){
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		double amount = 0;//支付金额
		
		String payType = "";//支付类型
		
		String accidentType = "";//违章事故类型 0：车辆违章 1：车辆事故
		
		if(pmap.get("param").equals("error")){

			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(pmap.containsKey("accidentId")){
			
			if(pmap.get("accidentId") == null || "".equals(pmap.get("accidentId"))){
				
				return Ajax.AppJsonResult(Constants.APP_ACCIDENT_ID_NULL, Constants.APP_ACCIDENT_ID_NULL_MSG);
			}
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(pmap.containsKey("payType")){
			
			if(StringUtils.isEmpty(pmap.get("payType"))){
				
				return Ajax.AppJsonResult(Constants.APP_PAYTYPE_NULL, Constants.APP_PAYTYPE_NULL_MSG);
			}else{
				payType = pmap.get("payType");
			}
			
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(StringUtils.isEmpty(subscriber)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		//根据违章记录ID获取违章记录信息
		Map<?, ?> accidentMap = accidentRecordService.getAccidentRecordById(pmap.get("accidentId"));
	
		if(!StringUtils.isEmpty(accidentMap)){
			
			amount = Double.valueOf(accidentMap.get("money").toString());//支付金额
			
			accidentType = accidentMap.get("type").toString();//违章事故类型
		}else{
			return Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		//预支付成功后   1  添加交易信息 2添加押金记录信息 
		String description = "";
		String body = "";		
		//交易编号
		String tradeRecordNo = TradeRecordNo.getCutPaymentNo();
		
		JSONObject json = new JSONObject();
		
		//交易记录数据
		Map<String,Object> accountTradeParam = new Hashtable<String,Object>();
		
		accountTradeParam.put("subscriber_id",subscriber.getId());
		
		accountTradeParam.put("pay_channel",Account.PAY_CHANNEL_APP);//渠道
		
		accountTradeParam.put("amount",amount);//交易金额
		
		accountTradeParam.put("result",0);//交易状态
		
		accountTradeParam.put("order_index",1);
		
		accountTradeParam.put("trade_time","t");
		
		accountTradeParam.put("biz_id","");//订单号
		
		accountTradeParam.put("sub_orderId","");//字订单
		
		accountTradeParam.put("remaining_amount","0");//账户余额
		
		accountTradeParam.put("is_auto_clear",0);
		
		accountTradeParam.put("trade_order_no",tradeRecordNo);//交易编号
		
		if(payType.equals(String.valueOf(Orders.PAY_TYPE_ALIPAY))){//支付宝充值
			
			if(accidentType.equals("0")){//车辆违章
				description = "车辆违章支付宝支付"+amount+"元";
				
				body = Constants.BODY_VIOLATION;
			}else{
				description = "车辆事故支付宝支付"+amount+"元";
				
				body = Constants.BODY_ACCIDENT;
			}
			
			
			JSONObject alipay = Alipay.ydalipay(tradeRecordNo, String.valueOf(amount), body,AlipayConfig.accident_url);
			
			json.element("alipay", alipay);
			
			accountTradeParam.put("type",Account.TYPE_DEPOSIT);//消费类型
			
			accountTradeParam.put("pay_type",payType);//支付类型
			
			accountTradeParam.put("description",description);//交易描述
			
			//添加交易记录
			accountTradeRecordService.addtradeRecord(accountTradeParam);
			
		}else if(payType.equals(String.valueOf(Orders.PAY_TYPE_WECHAT))){//微信充值
			
			if(accidentType.equals("0")){//车辆违章
				description = "车辆违章微信支付"+amount+"元";
				
				body = Constants.BODY_VIOLATION;
			}else{
				description = "车辆事故微信支付"+amount+"元";
				
				body = Constants.BODY_ACCIDENT;
			}
			
			String remoteAddress = request.getRemoteAddr();
			
			JSONObject wx;
			
			try {
				wx = WeixinPay.wxpay(remoteAddress, tradeRecordNo, body, String.valueOf(amount),Constant.ACCIDENT_URL);
				System.out.println("微信统一下单返回数据:"+wx);
				json.element("wx", wx);
				//微信统一下单异常
				if(wx.getJSONObject("order").getString("return_code").equals("FAIL")){
					return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
				}
				
			} catch (JDOMException e) {
				e.printStackTrace();
				return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			} catch (IOException e) {
				e.printStackTrace();
				return Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			}
			
			if(accidentType.equals("0")){//车辆违章
				accountTradeParam.put("type",Account.TYPE_CUT_VIOLATION);//消费类型
			}else{
				accountTradeParam.put("type",Account.TYPE_CUT_ACCIDENT);//消费类型
			}
			
			accountTradeParam.put("description",description);//交易描述
			
			//添加交易记录
			accountTradeRecordService.addtradeRecord(accountTradeParam);
			
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("tradeOrderNo", tradeRecordNo);
		paramMap.put("payStatus", "0");
		paramMap.put("id", accidentMap.get("id"));
		//修改会员车辆违章事故记录信息
		accidentRecordService.updateAccidentById(paramMap);
		
		return Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,json);
		
	}
	
	/**
	 * 会员违章事故支付-异步通知 支付宝回调
	 * version 1.0.1
	 * @param request
	 * @return
	 */
	public String alipayCallBackAccident(HttpServletRequest request){
		String result = "fail";
		
		//获取支付宝POST过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		@SuppressWarnings("unchecked")
		Map<String,Object> requestParams = request.getParameterMap();
		
		System.out.println("支付宝回调函数返回结果====="+requestParams.toString());
		
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
				: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
			params.put(name, valueStr);
		}
		
		System.out.println("params====="+params.toString());
		String tradeStatus = params.get("trade_status");//获取支付宝返回结果状态码
		
		System.out.println("result====="+tradeStatus);
		if(tradeStatus.equals("TRADE_SUCCESS")){
			String outTradeNo = params.get("out_trade_no");//交易号
			
			System.out.println("交易号====="+outTradeNo);
			
			//根据交易编号修改交易记录状态
			accountTradeRecordService.updateRecordStateByTradeOrderNo(outTradeNo);
			//根据交易编号修改会员车辆违章记录状态
			accidentRecordService.updateAccidentByTradeOrderNo(outTradeNo);
			//根据交易编号获取会员车辆违章记录信息
			Map<String, Object> accidentRecordMap = accidentRecordService.getAccidentByTradeOrderNo(outTradeNo);
			
			System.out.println("根据交易编号获取会员车辆违章记录信息======"+accidentRecordMap.toString());
			if(!StringUtils.isEmpty(accidentRecordMap)){
				
				//订单编号
				String ordersNo = accidentRecordMap.get("ordersNo").toString();
				
				System.out.println("订单编号====="+ordersNo);
				
				if(!StringUtils.isEmpty(ordersNo)){
					//获取订单信息
					Orders orders =  orderService.getOrderInfoByOrdersNo(ordersNo);
					
					System.out.println("获取订单信息====="+orders);
					
					String orderId = orders.getId();
					
					//修改订单异常状态
					orderService.updateOrderAbnormityByOrderNo(ordersNo);
					
					System.out.println("修改订单异常状态=====");
					
					if(!StringUtils.isEmpty(orderId)){
						
						if(String.valueOf(accidentRecordMap.get("type")).equals("0")){//修改车辆违章处理状态
							
							System.out.println("修改违章处理状态=====");
							accidentRecordService.updateVehicleByOrderId(orderId);
							
						}else{//修改车辆事故处理状态
							
							//获取字典表中车辆事故处理状态 05 已结案
							Dict d = dictService.getDictByCodes("handleStatus", "05");
							System.out.println("修改车辆事故处理状态=====");
							accidentRecordService.updateAccidentByOrderId(orderId, d.getId());
						}
					}
				}
			
			}
			
			result = "success";
			
		}
		
		return result;
	}
	
	/**
	 * 会员违章事故支付-异步通知 微信回调
	 * version 1.0.1
	 * @return
	 */
	public String wxCallBackAccident(HttpServletRequest request){
		Map<String,String> map = new HashMap<String,String>();
		try {
			Map<String,Object> params = WeixinPay.callBackUrl(request);
			
			System.out.println("微信回调函数返回值======"+params);
			
			if(params.get("status").toString().equals("ok")){
				map.put("return_code", params.get("return_code").toString());
				map.put("return_msg", params.get("return_msg").toString());
				
				String outTradeNo = params.get("orderNo").toString();//交易编号
				String total_fee = params.get("total_fee").toString();//实际充值金额 单位：分
				total_fee = String.valueOf(ArithUtil.div(Double.valueOf(total_fee), 100));//转换单位:元
				System.out.println("充值单位转换==="+total_fee+"元");
				
				System.out.println("交易号====="+outTradeNo);
				
				//根据交易编号修改交易记录状态
				accountTradeRecordService.updateRecordStateByTradeOrderNo(outTradeNo);
				//根据交易编号修改会员车辆违章记录状态
				accidentRecordService.updateAccidentByTradeOrderNo(outTradeNo);
			
				//根据交易编号获取会员车辆违章记录信息
				Map<String, Object> accidentRecordMap = accidentRecordService.getAccidentByTradeOrderNo(outTradeNo);
				
				if(!StringUtils.isEmpty(accidentRecordMap)){
					
					//订单编号
					String ordersNo = accidentRecordMap.get("ordersNo").toString();
					
					if(!StringUtils.isEmpty(ordersNo)){
						//获取订单信息
						Orders orders =  orderService.getOrderInfoByOrdersNo(ordersNo);
						
						String orderId = orders.getId();
						
						//修改订单异常状态
						orderService.updateOrderAbnormityByOrderNo(ordersNo);
						
						if(!StringUtils.isEmpty(orderId)){
							
							if(String.valueOf(accidentRecordMap.get("type")).equals("0")){//修改车辆违章处理状态
								
								accidentRecordService.updateVehicleByOrderId(orderId);
								
							}else{//修改车辆事故处理状态
								
								//获取字典表中车辆事故处理状态 05 已结案
								Dict d = dictService.getDictByCodes("handleStatus", "05");
								
								accidentRecordService.updateAccidentByOrderId(orderId, d.getId());
							}
						}
					}
				
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String result = HttpTool.mapToXml(map);
		System.out.println("微信异步校验支付成功后返回结果==="+result);
		return result;
	}
	
	/**
	 * 押金退还-app接口
	 * @param data
	 * @return
	 * version 1.0.1
	 */
	public String depositRefund(String data){
		
		String result = null;
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		//校验参数
		if(pmap.get("param").equals("error")){

			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		Subscriber subscriber = TokenUtils.getSubscriber(data);//获取会员信息
		
		if(StringUtils.isEmpty(subscriber)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		//判断用户当前时间内是否有预约订单
		if(OrderRedisData.isOrderexist(subscriber.getId())){
			
			return Ajax.AppJsonResult(Constants.APP_BOOKING_ORDER_EXIST,Constants.APP_BOOKING_ORDER_EXIST_MSG);
		}
		
		//查询用户是否有租车中的订单
		Orders ord = orderService.userOrderInfo(subscriber.getId(),Orders.STATE_ORDER_START);
		if(!StringUtils.isEmpty(ord)){
			
			return Ajax.AppJsonResult(Constants.APP_BOOKING_ORDER_EXIST,Constants.APP_BOOKING_ORDER_EXIST_MSG);
		}
		
		
		//获取会员押金金额
		Map<String, Object> depositMap = depositService.getDepositBySubId(subscriber.getId());
			
		Map<String, Object> paramMap = new HashMap<String, Object>();
		
		//退款交易编号
		String tradeRecordNoTk = TradeRecordNo.getRefundTradeNo();
		String tradeRecordNo = "";
		if(!StringUtils.isEmpty(depositMap)){
			//如果退款金额为0
			if(depositMap.get("usableAmount").equals("0")){
				
				return Ajax.AppJsonResult(Constants.APP_DEPOSIT_EXIT,Constants.APP_DEPOSIT_EXIT_MSG);
			}
			//获取会员押金充值记录信息//
			List<Map<String, Object>> listMapToUp = depositRecordService.getDepositBySubId(subscriber.getId());
			
			if(!StringUtils.isEmpty(listMapToUp) && listMapToUp.size() > 0){
				tradeRecordNo = listMapToUp.get(0).get("tradeOrderNo").toString();
			}
			paramMap.put("money", depositMap.get("usableAmount"));//退款金额
			
			paramMap.put("subId", subscriber.getId());//会员ID
			paramMap.put("type", "2");//类型 0:押金充值 1:押金扣款2:押金退款
			paramMap.put("desc", "退款审核中（预计7-15个工作日到账）");//描述
			paramMap.put("tradeOrderNo", tradeRecordNoTk);//交易编号
			paramMap.put("payStatus", "2");//交易状态 0:失败 1:成功2:退款申请3:退款成功
			
			//添加会员押金记录
			depositRecordService.addDepositRecord(paramMap);
			
			//添加会员退款申请记录信息
			Map<String, Object> refundMap = new HashMap<String, Object>();
			refundMap.put("subId", subscriber.getId());//会员ID
			refundMap.put("type", RefundRecord.REFUND_TYPE_DEPOSIT);//退款类型 0:账户余额退款1:押金退款
			refundMap.put("status", RefundRecord.REFUND_STATUS_APPLY);//退款状态是否退款1.申请退款2.退款中3.退款中断4.已退款
			refundMap.put("name", subscriber.getName());//会员名称
			refundMap.put("desc", "用户退款申请");//描述
			refundMap.put("money", depositMap.get("usableAmount"));//退款金额
			refundMap.put("actualMoney", depositMap.get("usableAmount"));//实际退款金额
			refundMap.put("tradeOrderNo", tradeRecordNoTk);//退款流水号
			refundMap.put("rechargeTradeNo", tradeRecordNo);//交易流水号
			
			AccountTradeRecord accountTradeRecord =  accountTradeRecordService.getAccountTradeRecordBySubscriberId(subscriber.getId(), tradeRecordNo);
			
			refundMap.put("payType",accountTradeRecord.getPayType());//支付类型
			refundRecordService.addRefundRecord(refundMap);
			
			//修改会员押金金额
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("subId", subscriber.getId());//会员ID
			
			double frozenAmount = 0;//冻结金额=可用金额+冻结金额
			double usableAmount = Double.parseDouble(depositMap.get("usableAmount").toString());
			frozenAmount = ArithUtil.add(usableAmount, Double.parseDouble(depositMap.get("frozenAmount").toString()));
			param.put("amount", frozenAmount);//押金金额
			param.put("frozenAmount", frozenAmount);//冻结金额
			param.put("usableAmount", 0);//可用金额
			
			depositService.updateDepositBySubId(param);
			
			result = Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
		}
		
		return result;
	}
	
	/**
	 * 预约取车-app接口
	 * version 1.0.1
	 * @param data
	 * @return
	 */
	public synchronized String advance(String data,HttpServletRequest request){
		
		String result = null;
		
		try {
			String ip = IpUtil.getIpAddr(request);
			
			//获取当前用户信息
			Subscriber sb = TokenUtils.getSubscriber(data);
			//判断用户是否更新手持照片
			if(StringHelper.isEmpty(sb.getHandHeldIdCard())){
				return Ajax.AppJsonResult(Constants.APP_HANDHELDIDCARD_NULL, Constants.APP_HANDHELDIDCARD_NULL_MSG);
			}
			
			//获取参数
			Map<String, String> map = JsonTools.desjsonForMap(data);
			if(map.get("param").equals("error")){
				return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			}
			
			
			String dotId = map.get("dotId");
			
			String backDotId = map.get("backDotId");
			
			String carId = map.get("carId");
			
			String insuranceId = map.get("insuranceId");
			
			if(StringUtils.isEmpty(carId)){
				
				return Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
			}
			
			if(StringUtils.isEmpty(dotId)){
				
				return Ajax.AppJsonResult(Constants.APP_DOTID_NULL, Constants.APP_DOTID_NULL_MSG);
			}
			
			if(StringUtils.isEmpty(backDotId)){
				
				return Ajax.AppJsonResult(Constants.APP_BACKDOTID_NULL, Constants.APP_BACKDOTID_NULL_MSG);
			}
			
			//判断账号是否过期
			if(StringUtils.isEmpty(sb)){
				
				return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
			}
			
			
			String md5id = Md5Util.MD5Encode(sb.getId());
			Subscriber sub = subscriberService.querySubscriberById(sb.getId());	
			
			
			if(!StringUtils.isEmpty(sub.getEventState())){
				//半锁状态不能下单
				if(sub.getEventState().equals(Subscriber.EVENT_STATE_HALF)){
					
					return Ajax.AppJsonResult(Constants.APP_EVENT_STATE_HALF,Constants.APP_EVENT_STATE_HALF_MSG);
				}
				//全锁
				if(sub.getEventState().equals(Subscriber.EVENT_STATE_FUll)){
					
					return Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "此账号已冻结，不能租车。 如有疑问，请联系客服。");
				}
			}
			
			//判断当前用户资料是否审核通过
			if(!sub.getState().equals(Subscriber.STATE_NORMAL)){
				result = Ajax.AppJsonResult(Constants.APP_STATE_NO_CONFIRMED, Constants.APP_STATE_NO_CONFIRMED_MSG);
				return result;
			}
			
			//判断用户当前时间内是否有预约订单
			if(OrderRedisData.isOrderexist(sub.getId())){
				result = Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OK, Constants.APP_ADVANCE_ORDER_OK_MSG);
				return result;
			}
			
			//查询用户当天时间内预约超时订单的次数
			Integer ostatus = 2;//订单状态 0预约取消 1预约成功 2预约超时 3租车中 4租车结束
			
			Integer count = orderService.orderStatus(sub.getId(),ostatus);
			
			if(count==null){
				
				count = 0;
			}
			
			//每天预约超时不能超过三次
			if(count>3){
				
				return Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OVER, Constants.APP_ADVANCE_ORDER_OVER_MSG);
			}
			
			
			//查询用户是否有租车中的订单
			Orders ord = orderService.userOrderInfo(sub.getId(),Orders.STATE_ORDER_START);
			
			if(ord!=null&&(ord.getState().equals(Orders.STATE_BOOKING_SUCCESS)||ord.getState().equals(Orders.STATE_ORDER_START))){
				
				return Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OK, Constants.APP_ADVANCE_ORDER_OK_MSG);
			}
			
			//查询用户是否有租车结束后未支付的订单信息
			Orders noPayOrders = orderService.userOrderInfoNoPay(sub.getId(), Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
			
			if(noPayOrders != null && noPayOrders.getState().equals(Orders.STATE_ORDER_END) && String.valueOf(noPayOrders.getPayState()).equals(Orders.STATE_ORDER_NO_PAY)){
				
				return Ajax.AppJsonResult(Constants.APP_ORDER_NO_PAY, Constants.APP_ORDER_NO_PAY_MSG);
			}
			
			
			//获取押金规则金额信息
			Map<String, Object> depositStrategy = depositStrategyService.getDepositStrategy("0");
			
			//如果押金规则不为空，判断会员押金金额是否充足，不充足，不允许租车
			if(!StringUtils.isEmpty(depositStrategy)){
				//获取会员押金金额
				Map<String, Object> depositMap = depositService.getDepositBySubId(sub.getId());
				
				if(StringUtils.isEmpty(depositMap)){
					
					return Ajax.AppJsonResult(Constants.APP_DEPOSIT_NULL, Constants.APP_DEPOSIT_NULL_MSG);
				}else{
					//押金规则金额
					double depositMoney = Double.valueOf(depositStrategy.get("money").toString());
					
					//如果押金规则金额大于0
					if(depositMoney > 0){
						//会员押金可用金额
						double subPositMoney = Double.valueOf(depositMap.get("usableAmount").toString());
						//如果会员押金可用金额不等于押金规则金额，不可以用车
						if(depositMoney>subPositMoney){
							
							return Ajax.AppJsonResult(Constants.APP_DEPOSIT_NULL, Constants.APP_DEPOSIT_NULL_MSG);
						}
					}
				}
			}
			
			
			//获取车辆电量和可持续里航
			Map<String, Object> carreal =CurrentCarInfo.getCarmp(carId);
			
			//判断车辆是否绑定车机
			if(carreal == null || "".equals(carreal) ){
				result = Ajax.AppJsonResult(Constants.APP_NO_BINDING_DEVICE, Constants.APP_NO_BINDING_DEVICE_MSG);
				return result;
			}
			
			//计费规则
			StrategyBase carfee = strategyBaseService.carfee(carId);
			
			Integer timecount = carfee.getTimeBeforeGet();//预约订单定时时间 (单位：分钟)
			
			if(timecount==null||timecount==0){
				timecount = 20;
			}
			
			Date date = new Date();
			//定时开始时间
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			String timing = sdf.format(new Date(date.getTime() + timecount*60*1000));
			//订单创建时间
			SimpleDateFormat tdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			String time = tdf.format(date);
			
			
			RedisClient.setobjdata(md5id, sub,DataBaseUtil.LOGIN_COMMON);
			
			//获取字典表中车辆未租借状态
			Dict d = dictService.getDictByCodes("carBizState", "0");
			//获取字典表中车辆租借中状态
			Dict di = dictService.getDictByCodes("carBizState", "1");
			//获取字典表中车辆下线状态
			Dict dx = dictService.getDictByCodes("carBizState", "3");
			
			Map<String, Object> res = new Hashtable<String, Object>();
			
			//获取 车辆信息
			Map<String, Object> car = branchDotService.carinfo(carId,null);
			String version = car.get("version").toString();
			//车牌号
			String vehiclePlateId = car.get("vehiclePlateId").toString();
			
			//判断当前车辆是否已经预约 
			if(car.get("bizState").toString().equals(di.getId())){
				result = Ajax.AppJsonResult(Constants.APP_CAR_YUYUE_OK, Constants.APP_CAR_YUYUE_OK_MSG);
				return result;
			}else if(car.get("bizState").toString().equals(dx.getId())){
				result = Ajax.AppJsonResult(Constants.APP_CAR_YUYUE_NO, Constants.APP_CAR_YUYUE_NO_MSG);
				return result;
			}else if(car.get("bizState").toString().equals(d.getId())){
				
				//更新缓存
				boolean redis = RedisClient.setobjdata(md5id, sub,DataBaseUtil.LOGIN_COMMON);
				if(!redis){
					result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
					return result;
				}
				
				//更新车辆状态为租界中的状态
				branchDotService.updateCarStatus(carId,di.getId(),version);
				
				car.remove("bizState");
				car.put("percent", carreal.get("electricity"));//剩余电量
				
				//根据车辆ID获取车辆初始续航值
				RealtimeState r = realTimeStateService.getRealTimeState(carId);
				
				Map<String,String> paramMap = new HashMap<String, String>();
				paramMap.put("id", carId);//车辆ID
				
				String electricity = carreal.get("electricity").toString();
				
				if(electricity != null && !"".equals(electricity)){
					electricity = electricity.split("/")[0];
				}else{
					electricity = "0";
				}
				
				double dd= 100;
				
				double SOC = ArithUtil.div(Double.parseDouble(electricity),dd);
				if("0".equals(carreal.get("mileage")) && SOC > 0){
					//标准里程
					double standardMileage = Double.valueOf(car.get("standardMileage").toString());
						
					car.put("lifeMileage", String.valueOf(ArithUtil.mul(standardMileage, SOC)));//续航里程
				}else{
					car.put("lifeMileage", String.valueOf(carreal.get("mileage")));//续航里程
				}
				
				paramMap.put("totalMileage", String.valueOf(carreal.get("totalMileage")));//当前里程
				paramMap.put("time", time);//创建时间
				if(r != null && !"".equals(r)){//修改
					realTimeStateService.updateRealTimeState(paramMap);
				}else{//添加
					realTimeStateService.addRealTimeState(paramMap);
				}
				
				//获取网点信息
				Map<String, Object> dot = branchDotService.dotInfo(dotId);
				res.put("car", car);
				res.put("dot", dot);
				
				//租赁类型名字
				String type = carfee.getIsPrepaidPay()==1?"日租":"时租";
				//该辆车的上笔订单
				Orders order = orderService.preorder(carId);
				String preOrders = order==null?"":order.getId();
				//预约订单
				Map<String, Object> param = new Hashtable<String, Object>();
				DecimalFormat df = new DecimalFormat("000");
				String orderNo = OrderNoUtil.getOrderNo();
				param.put("id", "");//订单主键
				param.put("orderid", "");//订单主键
				param.put("orders_no", orderNo);//订单编号
				param.put("orders_detail_no", orderNo+df.format(1));//订单详情编号
				param.put("dotId", dotId);//网点id
				param.put("carId", carId);//车辆id
				param.put("subId", sub.getId());//会员id
				param.put("source", Orders.PAY_CHANNEL_APP);//支付渠道
				param.put("state", "1");//订单状态 0预约取消 1预约成功 2预约超时 3租车开始 4租车结束
				param.put("isAbnormity", "0");//订单是否异常 0 为正常 1为异常
				param.put("isAppraise", "0");//订单是否评价 0 为未评价 1为评价
				param.put("backDotId", backDotId);//还车网点id
				
				param.put("strategyId", carfee.getId());//计费规则ID
				param.put("strategyTitle", carfee.getName());//计费规则名称
				param.put("isTimeout", "0");//是否超时
				param.put("timeoutStrategyid", carfee.getId());//对应超时策略ID
				param.put("isRunning", "0");//是否在进行中
				param.put("preOrders", preOrders);//上笔订单ID
				param.put("isPrePay", "0");//是否预付费
				param.put("isPaid", "0");//是否已经付款
				param.put("typeId", carfee.getType());//租赁类型ID
				param.put("typeName",type);//租赁类型名字
				param.put("isOver", "0");//标记订单是否完结
				param.put("isException", "0");//标记订单异常
				param.put("isOpenBill", "0");//是否开了发票
				param.put("isbill", "1");//是否开了发票
				param.put("time", time);//创建时间
				param.put("paystatus", Orders.STATE_ORDER_NO_PAY);//默认未支付
				//param.put("isGround", 1);//是否是地勤订单（0：否1：是）
				//保存预约订单
				orderService.addyyorder(param);
				param.put("orderid", param.get("id"));
				
				
				if(!StringUtils.isEmpty(insuranceId)){
					
					param.put("insuranceId", insuranceId);//保险ID
					
					String [] insuranceIds = insuranceId.split("/");
					
					String insuranceName = "";//保险名称
					
					String insuranceFee = "";//保险金额
					
					if(insuranceIds != null && insuranceIds.length > 0){
						
						for(int i = 0;i < insuranceIds.length;i++){
							
							if(!StringUtils.isEmpty(insuranceIds[i])){
								
							  	Map<String, Object> insuranceMap = insuranceService.getInsuranceById(insuranceIds[i], carId);
							
							  	if(!StringUtils.isEmpty(insuranceMap)){
							  		
							  		insuranceName += insuranceMap.get("name").toString()+"/"; 
							  		
							  		insuranceFee += insuranceMap.get("amount").toString()+"/";
							  	}
							}
							
						}
					}
					
					if(!StringUtils.isEmpty(insuranceName)){
						param.put("insuranceName", insuranceName.substring(0, insuranceName.length()-1));//保险名称
					}else{
						param.put("insuranceName", "");//保险名称
					}
					
					
					if(!StringUtils.isEmpty(insuranceFee)){
						param.put("insuranceFee", insuranceFee.substring(0, insuranceFee.length()-1));//保险金额
					}else{
						param.put("insuranceFee", "");//保险金额
					}
					
				}
				
//				param.put("");
				
				orderService.addyyorderDetail(param);
				
				String content = MsgContent.getSmsMsg(MsgContent.ORDER_CREATE_SUCCESS_MSG, String.valueOf(timecount));
				String mobile = sub.getPhoneNo();
				if(sendMSg.send(mobile, content)){
					//记录短信信息
					SMSRecord record = new SMSRecord();
					record.setContent(content);
					record.setPhoneNo(mobile);
					record.setType(3);
					record.setUserId(sub.getId());
					record.setUserName(sub.getName());
					record.setResult(mobile+":Success");
					subscriberService.addSmsRecord(record);
				}
				
				//redis 临时存放用户预约订单信息
				OrderRedisData.addTmpOrders(dotId,param.get("orderid").toString(), carId, sub.getId(), timing,mobile,sub.getName(),orderNo);
				
				runLogService.addRunAppAdvance(sub.getName(),ip,vehiclePlateId, RunLog.APP_ADVANCE_MSG);
				res.put("timing", carfee.getTimeBeforeGet());
				res.put("startTimingTime", time);
				res.put("orderId", param.get("orderid"));
				
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
				return result;
			}
			
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			return Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION_MSG);
		}
		
		return result;
	}
	
	
	public String confirmCar(String data){
		
		String result = null;
		
		//获取当前用户信息
		Subscriber sb = TokenUtils.getSubscriber(data);
		//获取参数
		Map<String, String> map = JsonTools.desjsonForMap(data);
		
		
		String carId = map.get("carId");
		
		if(map.get("param").equals("error")){
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(StringUtils.isEmpty(carId)){
			
			return Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
		}
		
		//判断账号是否过期
		if(StringUtils.isEmpty(sb)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		
		String md5id = Md5Util.MD5Encode(sb.getId());
		
		Subscriber sub = subscriberService.querySubscriberById(sb.getId());	
		
		//判断当前用户资料审核未通过 状态码 4
		if(sub.getState().equals(Subscriber.STATE_NO_CONFIRMED)){
			result = Ajax.AppJsonResult(Constants.APP_STATE_NO_CONFIRMED, Constants.APP_STATE_NO_CONFIRMED_MSG);
			return result;
		}
		//用户资料未认证 状态 1提示未提交资料
		if(sub.getState().equals(Subscriber.STATE_UNCONFIRMED)){
			result = Ajax.AppJsonResult(Constants.APP_STATE_NO_UNAUTHORIZED, Constants.APP_STATE_NO_UNAUTHORIZED_MSG);
			return result;
		}
		
		//用户资料审核中状态码 2
		if(sub.getState().equals(Subscriber.STATE_WAIT_CONFIRMED)){
			result = Ajax.AppJsonResult(Constants.APP_STATE_NO_AUDIT, Constants.APP_STATE_NO_AUDIT_MSG);
			return result;
		}
		
		if(!StringUtils.isEmpty(sub.getEventState())){
			//半锁状态不能下单
			if(sub.getEventState().equals(Subscriber.EVENT_STATE_HALF)){
				
				return Ajax.AppJsonResult(Constants.APP_EVENT_STATE_HALF,Constants.APP_EVENT_STATE_HALF_MSG);
			}
			//全锁
			if(sub.getEventState().equals(Subscriber.EVENT_STATE_FUll)){
				
				return Ajax.JSONResult(Constants.APP_EVENT_STATE_FUll, Constants.APP_EVENT_STATE_FUll_MSG);
			}
		}
		
		
		//判断用户当前时间内是否有预约订单
		if(OrderRedisData.isOrderexist(sub.getId())){
			result = Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OK, Constants.APP_ADVANCE_ORDER_OK_MSG);
			return result;
		}
		
		//查询用户当天时间内预约超时订单的次数
		Integer ostatus = 2;//订单状态 0预约取消 1预约成功 2预约超时 3租车中 4租车结束
		
		Integer count = orderService.orderStatus(sub.getId(),ostatus);
		
		if(count==null){
			
			count = 0;
		}
		
		//每天预约超时不能超过三次
		if(count>3){
			
			return Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OVER, Constants.APP_ADVANCE_ORDER_OVER_MSG);
		}
		
		//查询用户是否有租车中的订单
		Orders ord = orderService.userOrderInfo(sub.getId(),Orders.STATE_ORDER_START);
		
		if(ord!=null&&(ord.getState().equals(Orders.STATE_BOOKING_SUCCESS)||ord.getState().equals(Orders.STATE_ORDER_START))){
			
			return Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OK, Constants.APP_ADVANCE_ORDER_OK_MSG);
		}
		
		//查询用户是否有租车结束后未支付的订单信息
		Orders noPayOrders = orderService.userOrderInfoNoPay(sub.getId(), Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
		
		if(noPayOrders != null && noPayOrders.getState().equals(Orders.STATE_ORDER_END) && String.valueOf(noPayOrders.getPayState()).equals(Orders.STATE_ORDER_NO_PAY)){
			
			return Ajax.AppJsonResult(Constants.APP_ORDER_NO_PAY, Constants.APP_ORDER_NO_PAY_MSG);
		}
		
		//获取押金规则金额信息
		Map<String, Object> depositStrategy = depositStrategyService.getDepositStrategy("0");
		
		//如果押金规则不为空，判断会员押金金额是否充足，不充足，不允许租车
		if(!StringUtils.isEmpty(depositStrategy)){
			//获取会员押金金额
			Map<String, Object> depositMap = depositService.getDepositBySubId(sub.getId());
			
			if(StringUtils.isEmpty(depositMap)){
				
				return Ajax.AppJsonResult(Constants.APP_DEPOSIT_NULL, Constants.APP_DEPOSIT_NULL_MSG);
			}else{
				//押金规则金额
				double depositMoney = Double.valueOf(depositStrategy.get("money").toString());
				
				//如果押金规则金额大于0
				if(depositMoney > 0){
					//会员押金可用金额
					double subPositMoney = Double.valueOf(depositMap.get("usableAmount").toString());
					//如果会员押金可用金额不等于押金规则金额，不可以用车
					if(depositMoney>subPositMoney){
						
						return Ajax.AppJsonResult(Constants.APP_DEPOSIT_NULL, Constants.APP_DEPOSIT_NULL_MSG);
					}
				}
			}
		}
		
		//更新缓存
		boolean redis = RedisClient.setobjdata(md5id, sub,DataBaseUtil.LOGIN_COMMON);
		if(!redis){
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			return result;
		}
		
		
		
		List<Map<String, Object>> paramList = new ArrayList<Map<String, Object>>();
		
		//根据车辆ID获取汽车类型保险信息
		List<Map<String, Object>> carInsuranceMaps = insuranceService.getCarInsurance(carId);
		
		if(!StringUtils.isEmpty(carInsuranceMaps)){
			
			for(Map<String, Object> carInsuranceMap :carInsuranceMaps){
				
				Map<String, Object> res = new HashMap<String, Object>();
				
				if(carInsuranceMap.containsKey("name")){
					res.put("title", carInsuranceMap.get("name").toString());	
				}
				
				if(carInsuranceMap.containsKey("amount")){
					res.put("subtitle", carInsuranceMap.get("amount").toString()+"元/单");
				}
				
				
				if(carInsuranceMap.containsKey("isUsed")){
					res.put("isUsed", carInsuranceMap.get("isUsed").toString());
				}
				
				if(carInsuranceMap.containsKey("insuranceDesc")){
					res.put("insuranceDesc", StringUtils.isEmpty(carInsuranceMap.get("insuranceDesc").toString())?"":carInsuranceMap.get("insuranceDesc").toString());
				}
				
				if(carInsuranceMap.containsKey("insuranceId")){
					res.put("insuranceId", carInsuranceMap.get("insuranceId").toString());
				}
				
				if(!StringUtils.isEmpty(carInsuranceMap.get("insuranceId").toString())){
					
					List<Map<String, Object>> getInsuranceDescMaps =  insuranceService.getInsuranceDesc(carInsuranceMap.get("insuranceId").toString());
				
					res.put("tipsList", getInsuranceDescMaps);
				}
				paramList.add(res);
			}
			
		}else{
			//车辆未关联保险
		}
		
//		Map<String, Object> paramMap = new HashMap<String, Object>();
//		paramMap.put("data", paramList);
		
		result = Ajax.AppJsonArrayResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,paramList);
		
		
		return result;
		
	}
	
	
	
	/**确认用车新接口1.5.1
	 * @param data
	 * @return
	 */
	public String confirmCarNew(String data){
		
		String result = null;
		
		//获取当前用户信息
		Subscriber sb = TokenUtils.getSubscriber(data);
		//获取参数
		Map<String, String> map = JsonTools.desjsonForMap(data);
		
		
		String carId = map.get("carId");
		
		if(map.get("param").equals("error")){
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		if(StringUtils.isEmpty(carId)){
			
			return Ajax.AppJsonResult(Constants.APP_CARID_NULL, Constants.APP_CARID_NULL_MSG);
		}
		
		//判断账号是否过期
		if(StringUtils.isEmpty(sb)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		
		String md5id = Md5Util.MD5Encode(sb.getId());
		
		Subscriber sub = subscriberService.querySubscriberById(sb.getId());	
		
		//判断当前用户资料审核未通过 状态码 4
		if(sub.getState().equals(Subscriber.STATE_NO_CONFIRMED)){
			result = Ajax.AppJsonResult(Constants.APP_STATE_NO_CONFIRMED, Constants.APP_STATE_NO_CONFIRMED_MSG);
			return result;
		}
		//用户资料未认证 状态 1提示未提交资料
		if(sub.getState().equals(Subscriber.STATE_UNCONFIRMED)){
			result = Ajax.AppJsonResult(Constants.APP_STATE_NO_UNAUTHORIZED, Constants.APP_STATE_NO_UNAUTHORIZED_MSG);
			return result;
		}
		
		//用户资料审核中状态码 2
		if(sub.getState().equals(Subscriber.STATE_WAIT_CONFIRMED)){
			result = Ajax.AppJsonResult(Constants.APP_STATE_NO_AUDIT, Constants.APP_STATE_NO_AUDIT_MSG);
			return result;
		}
		
		//用户是否上传手持照片
		if(StringHelper.isEmpty(sub.getHandHeldIdCard())){
			result = Ajax.AppJsonResult(Constants.APP_HANDHELDIDCARD_NULL, Constants.APP_HANDHELDIDCARD_NULL_MSG);
			return result;
		}		
		
		
		if(!StringUtils.isEmpty(sub.getEventState())){
			//半锁状态不能下单
			if(sub.getEventState().equals(Subscriber.EVENT_STATE_HALF)){
				
				return Ajax.AppJsonResult(Constants.APP_EVENT_STATE_HALF,Constants.APP_EVENT_STATE_HALF_MSG);
			}
			//全锁
			if(sub.getEventState().equals(Subscriber.EVENT_STATE_FUll)){
				
				return Ajax.JSONResult(Constants.APP_EVENT_STATE_FUll, Constants.APP_EVENT_STATE_FUll_MSG);
			}
		}
		
		
		//判断用户当前时间内是否有预约订单
		if(OrderRedisData.isOrderexist(sub.getId())){
			result = Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OK, Constants.APP_ADVANCE_ORDER_OK_MSG);
			return result;
		}
		
		//查询用户当天时间内预约超时订单的次数
		Integer ostatus = 2;//订单状态 0预约取消 1预约成功 2预约超时 3租车中 4租车结束
		
		Integer count = orderService.orderStatus(sub.getId(),ostatus);
		
		if(count==null){
			
			count = 0;
		}
		
		//每天预约超时不能超过三次
		if(count>3){
			
			return Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OVER, Constants.APP_ADVANCE_ORDER_OVER_MSG);
		}
		
		//查询用户是否有租车中的订单
		Orders ord = orderService.userOrderInfo(sub.getId(),Orders.STATE_ORDER_START);
		
		if(ord!=null&&(ord.getState().equals(Orders.STATE_BOOKING_SUCCESS)||ord.getState().equals(Orders.STATE_ORDER_START))){
			
			return Ajax.AppJsonResult(Constants.APP_ADVANCE_ORDER_OK, Constants.APP_ADVANCE_ORDER_OK_MSG);
		}
		
		//查询用户是否有租车结束后未支付的订单信息
		Orders noPayOrders = orderService.userOrderInfoNoPay(sub.getId(), Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
		
		if(noPayOrders != null && noPayOrders.getState().equals(Orders.STATE_ORDER_END) && String.valueOf(noPayOrders.getPayState()).equals(Orders.STATE_ORDER_NO_PAY)){
			
			return Ajax.AppJsonResult(Constants.APP_ORDER_NO_PAY, Constants.APP_ORDER_NO_PAY_MSG);
		}
		
		//获取押金规则金额信息
		Map<String, Object> depositStrategy = depositStrategyService.getDepositStrategy("0");
		
		//如果押金规则不为空，判断会员押金金额是否充足，不充足，不允许租车
		if(!StringUtils.isEmpty(depositStrategy)){
			//获取会员押金金额
			Map<String, Object> depositMap = depositService.getDepositBySubId(sub.getId());
			
			if(StringUtils.isEmpty(depositMap)){
				
				return Ajax.AppJsonResult(Constants.APP_DEPOSIT_NULL, Constants.APP_DEPOSIT_NULL_MSG);
			}else{
				//押金规则金额
				double depositMoney = Double.valueOf(depositStrategy.get("money").toString());
				
				//如果押金规则金额大于0
				if(depositMoney > 0){
					//会员押金可用金额
					double subPositMoney = Double.valueOf(depositMap.get("usableAmount").toString());
					//如果会员押金可用金额不等于押金规则金额，不可以用车
					if(depositMoney>subPositMoney){
						
						return Ajax.AppJsonResult(Constants.APP_DEPOSIT_NULL, Constants.APP_DEPOSIT_NULL_MSG);
					}
				}
			}
		}
		
		//更新缓存
		boolean redis = RedisClient.setobjdata(md5id, sub,DataBaseUtil.LOGIN_COMMON);
		if(!redis){
			result = Ajax.AppJsonResult(Constants.APP_LOGIN_FAIL, Constants.APP_LOGIN_FAIL_MSG);
			return result;
		}
		
		
		
		List<Map<String, Object>> paramList = new ArrayList<Map<String, Object>>();
		
		//根据车辆ID获取汽车类型保险信息
		List<Map<String, Object>> carInsuranceMaps = insuranceService.getCarInsurance(carId);
		
		if(!StringUtils.isEmpty(carInsuranceMaps)){
			
			for(Map<String, Object> carInsuranceMap :carInsuranceMaps){
				
				Map<String, Object> res = new HashMap<String, Object>();
				
				if(carInsuranceMap.containsKey("name")){
					res.put("title", carInsuranceMap.get("name").toString());	
				}
				
				if(carInsuranceMap.containsKey("amount")){
					res.put("subtitle", carInsuranceMap.get("amount").toString()+"元/单");
				}
				
				
				if(carInsuranceMap.containsKey("isUsed")){
					res.put("isUsed", carInsuranceMap.get("isUsed").toString());
				}
				
				if(carInsuranceMap.containsKey("insuranceDesc")){
					res.put("insuranceDesc", StringUtils.isEmpty(carInsuranceMap.get("insuranceDesc").toString())?"":carInsuranceMap.get("insuranceDesc").toString());
				}
				
				if(carInsuranceMap.containsKey("insuranceId")){
					res.put("insuranceId", carInsuranceMap.get("insuranceId").toString());
				}
				
				if(!StringUtils.isEmpty(carInsuranceMap.get("insuranceId").toString())){
					
					List<Map<String, Object>> getInsuranceDescMaps =  insuranceService.getInsuranceDesc(carInsuranceMap.get("insuranceId").toString());
				
					res.put("tipsList", getInsuranceDescMaps);
				}
				paramList.add(res);
			}
			
		}else{
			//车辆未关联保险
		}
		
//		Map<String, Object> paramMap = new HashMap<String, Object>();
//		paramMap.put("data", paramList);
		
		result = Ajax.AppJsonArrayResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,paramList);
		
		
		return result;
		
	}
	
	
	
	/**
	 * 支付订单信息接口
	 * @param data
	 * @return
	 */
	public String lookpayOrder(String data){
		
		String result = null;
		
		Map<String, Object> res = new Hashtable<String, Object>();
		
		//获取当前用户信息
		Subscriber s = TokenUtils.getSubscriber(data);
		
		//判断账号是否过期
		if(StringUtils.isEmpty(s)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		String id = s.getId();
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		if(pmap.get("param").equals("error")){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
			return result;
		}
		
		Orders ord;
		if(pmap.get("repayFlag").equals("1")){
			ord = orderService.userOrderInfoNoPay(id, Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
		}else{
			ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);	
		}
		if(ord ==null){
			result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
			return result;
		}
		
		OrdersDetail ordersDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
		
		//获取优惠卷
		Coupon coupon = couponService.getCouonByOrderId(ord.getId());
		String selectCoupon = pmap.get("selectCoupon");
		boolean selectCoupon1 = pmap.get("selectCoupon") != null;
		System.out.println(selectCoupon);
		//获取优惠券金额
		Integer couponPrice =null;
		if(("1".equals(selectCoupon)&&coupon != null)||("true".equals(selectCoupon)&&coupon != null)){
			couponPrice = coupon.getPrice().intValue()*100;
			res.put("couponPrice", couponPrice/100);//优惠券金额
		}else if(selectCoupon == null && coupon != null){
			couponPrice = coupon.getPrice().intValue()*100;
			res.put("couponPrice", couponPrice/100);//优惠券金额
		}else {
			SubCoupon subCoupon = new SubCoupon();
			subCoupon.setOrderId(ord.getId());
			//更新优惠卷关联表
			subCouponMapper.updateSubCouponByOrderId(subCoupon);
			couponPrice = 0;
			res.put("couponPrice", "");//优惠券金额
		}
		String isUserCoupon = pmap.get("isUserCoupon");
	/*	if(coupon==null){
			couponPrice = 0;
		}else {
			couponPrice = coupon.getPrice().intValue()*100;
		}*/
		//订单费用信息
//		Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
		
		/**
		 * 调用工具类,进行计费计算(抽离新增)
		 * 
		 * @author ge jiaming
		 * @date 2017-07-29
		 */
		//获取参数
		String[] insuranceFees = null;// 保险名称
		String[] insuranceNames = null;// 保险金额
		List<Map<String, Object>> paramList = new ArrayList<Map<String,Object>>();
		int insuranceCosts = 0;// 保险费用
		//获取保险单价
		if (!StringUtils.isEmpty(ordersDetail.getInsuranceName())) {
			// [基础保险, 附加保险]
			insuranceNames = ordersDetail.getInsuranceName().split("/");
			if (!StringUtils.isEmpty(ordersDetail.getInsuranceFee())) {
				insuranceFees = ordersDetail.getInsuranceFee().split("/");
			} 
			if (insuranceFees != null) {
				for (String string : insuranceFees) {
					insuranceCosts = insuranceCosts + (int) (Double.valueOf(string) * 100);
				}
			}
		}
		// 获取里程单价
		StrategyBase carfee = orderService.carfee(ord.getCarId());// 计费规则
		Double kmPriceD = carfee.getKmPrice().doubleValue();// 里程价(单位：公里)
		float kmPriceF=(float)(kmPriceD*100);
		int kmPrice=(int) kmPriceF;
		// 获取时间单价
		Double timePriceD = carfee.getBasePrice().doubleValue();// 时间基数
		float timePriceF=(float)(timePriceD*100);
		int timePrice=(int) timePriceF;
		//获取账户余额
		Map<String,Object> subInfo = subscriberService.subAccountInfo(id);
		Double amount = 0.0;
		if(subInfo != null){
			amount = Double.valueOf(subInfo.get("usable_amount").toString());//账户余额
		}
		
		Map<String, Integer> calculateFeesResult = FeesAlg.calculateFees(UseCarMode.NORMAL_MODE,
				ord.getBeginTime(), ord.getEndTime(), ordersDetail.getMileage().doubleValue(),
				insuranceCosts, couponPrice, CouponType.COUPON_NORMAL, false, timePrice, kmPrice, amount.intValue(), true);
		
		double allPrice = ArithUtil.div(Double.valueOf(calculateFeesResult.get("cashPayment") + ""), 100, 2);
		//用车时间
		Integer useTime = calculateFeesResult.get("allMinute");
		int days=useTime % 1440 == 0?useTime / 1440:(useTime / 1440 + 1);
		if(!StringUtils.isEmpty(ordersDetail.getInsuranceName())){
			insuranceNames = ordersDetail.getInsuranceName().split("/");
			if(!StringUtils.isEmpty(ordersDetail.getInsuranceFee())){
				insuranceFees = ordersDetail.getInsuranceFee().split("/");
			}
			for(int i =0;i<insuranceNames.length;i++){
				Map<String, Object> paramMap = new HashMap<String, Object>();
				if(insuranceFees != null && insuranceFees.length > 0){
					paramMap.put("insuranceName", insuranceNames[i]+"("+insuranceFees[i]+"元/单)");
					//判断订单用单时长，计算保险费用、总费用
					if(days > 0){
						paramMap.put("insuranceFee", ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
					}else{
						paramMap.put("insuranceFee", insuranceFees[i].toString());
					}
					paramList.add(paramMap);
				}
			}
		}
		
	    /*	
     	double allPrice = Double.parseDouble(orderfee.get("allPrice").toString());
		String [] insuranceFees = null;//保险名称
		String [] insuranceNames = null;//保险金额
		
		int days = 0;//订单用单时长（单位:天,不足一天按一天计算）
		
		List<Map<String, Object>> paramList = new ArrayList<Map<String,Object>>();
		
		//根据订单开始时间、结束时间，计算天数
		if(!StringUtils.isEmpty(ord.getBeginTime())&&!StringUtils.isEmpty(ord.getEndTime())){
			days = ToolDateTime.getDateDaysForInsurance(ord.getBeginTime(), ord.getEndTime());
		}
		
		if(!StringUtils.isEmpty(ordersDetail.getInsuranceName())){
					
					insuranceNames = ordersDetail.getInsuranceName().split("/");
					
					if(!StringUtils.isEmpty(ordersDetail.getInsuranceFee())){
						
						insuranceFees = ordersDetail.getInsuranceFee().split("/");
					}
					for(int i =0;i<insuranceNames.length;i++){
						
						Map<String, Object> paramMap = new HashMap<String, Object>();
						
						if(insuranceFees != null && insuranceFees.length > 0){
							paramMap.put("insuranceName", insuranceNames[i]+"("+insuranceFees[i]+"元/单)");
							//判断订单用单时长，计算保险费用、总费用
							if(days > 0){
								paramMap.put("insuranceFee", ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
								allPrice = ArithUtil.add(allPrice, ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
							
							}else{
								paramMap.put("insuranceFee", insuranceFees[i].toString());
								allPrice = ArithUtil.add(allPrice, Double.valueOf(insuranceFees[i].toString()));
							}
							
							paramList.add(paramMap);
						}
						
					}
				}
		
		if(coupon != null && !"".equals(coupon)){
			res.put("couponPrice", coupon.getPrice());//优惠券金额
			if(coupon.getPrice().doubleValue() > allPrice){//如果优惠卷金额大于支付金额，默认支付金额为0
				allPrice = 0;
			}else{
				
				allPrice = ArithUtil.sub(allPrice,coupon.getPrice().doubleValue());
			}
		}else{
			res.put("couponPrice", "");//优惠券金额
		}
		
		
		double payPrice = 0.00;
		//获取账户信息
		Map<String,Object> subInfo = subscriberService.subAccountInfo(id);
		Double amount = 0.0;
		if(subInfo != null){
			amount = Double.valueOf(subInfo.get("usable_amount").toString());//账户余额
			//判断当前用户余额是否大于支付金额 是 用余额支付 否用微信或支付宝支付剩余的 
			if(amount>allPrice){
				amount = allPrice;
				payPrice = 0;
			}else{
				payPrice = ArithUtil.sub(allPrice,amount);
			}
		}
		res.put("allMilePrice", !StringUtils.isEmpty(ordersDetail.getMileFee())?ordersDetail.getMileFee():0);//总里程价格 单位：元
		res.put("allTimePrice", !StringUtils.isEmpty(ordersDetail.getTimeFee())?ordersDetail.getTimeFee():0);//总时长价格
		res.put("allMilePrice", orderfee.get("allMilePrice"));
		res.put("allTimePrice", orderfee.get("allTimePrice"));
		res.put("allMile",  orderfee.get("allMile"));//总里程 （公里）
		res.put("alltime",  orderfee.get("alltime"));//总时长
        res.put("payPrice", payPrice);//实际支付金额
        **/
		res.put("allMilePrice", (double)calculateFeesResult.get("totalMilePrice")/100);
		res.put("allTimePrice", (double)calculateFeesResult.get("totalTimePrice")/100);
		res.put("allPrice", allPrice);//总费用
		res.put("payPrice", calculateFeesResult.get("cashPayment"));//实际支付金额
		res.put("amount", 0.0);//账户余额 
		res.put("insurance", paramList);//保险名称
		res.put("allMile",  calculateFeesResult.get("allMile"));//总里程 （公里）
		res.put("alltime",  useTime);//总时长
		//抽离结束(2017-08-01)
		
		res.put("orderId", ord.getId());
		
		orderService.updateOrderStatus(ord.getId(), Integer.parseInt(Orders.STATE_ORDER_END)); //更新订单状态为租车结束
		
		return Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		
	}
	
	/**
	 * 查看当前订单 -app接口
	 * @param data
	 * @return
	 */
	public String lookNowOrder(String data){
		
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		res.put("isexist", 0);//当前订单是否存在 0不存在 1存在
		
		//获取当前用户信息
		Subscriber s = TokenUtils.getSubscriber(data);
		
		//判断账号是否过期
		if(StringUtils.isEmpty(s)){
			
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		String id = s.getId();
		//获取预约订单 是否存在
		Map<String,Object> yyorder = OrderRedisData.yyorder(id); 
		
		if(yyorder!=null&&!yyorder.isEmpty()){
			//预约订单信息
			String orderId = yyorder.get("orderId").toString();
			String carId = yyorder.get("carId").toString();
			String dotId = yyorder.get("dotId").toString();
			String createTime = yyorder.get("createTime").toString();
			
			System.out.println(createTime);
			
			StrategyBase carfee = orderService.carfee(carId);
			Integer timecount = carfee.getTimeBeforeGet();//预约订单定时时间 (单位：分钟)
			
			String orderNo = yyorder.get("orderNo").toString();//订单号
			if(timecount==null||timecount==0){
				timecount = 20;
			}
			//获取 车辆信息
			Map<String, Object> car = branchDotService.carinfo(carId,null);
			car.remove("biz_state");
			//获取车辆电量和可持续里航
			Map<String, Object> carreal = CurrentCarInfo.getCarmp(carId);
			
			if(carreal != null && !"".equals(carreal)){
				car.put("lifeMileage", carreal.get("mileage"));//续航里程
				car.put("percent", carreal.get("electricity"));//剩余电量
			}else{
				car.put("lifeMileage", "");//续航里程
				car.put("percent", "");//剩余电量
			}
			
			//获取网点信息
			Map<String, Object> dot = branchDotService.dotInfo(dotId);
			res.put("car", car);
			res.put("dot", dot);
			
			res.put("startTimingTime", createTime);
			res.put("orderId", orderId);
			res.put("orderType", 0);//订单类型 0 预约订单 1租车开始后的订单
			res.put("isexist", 1);
			
			int secound = ToolDateTime.getDateSecond(new Date(), ToolDateTime.parse(createTime,ToolDateTime.pattern_ymd_hms));
			
			//超时时间为0的时候取消订单
			if(secound <= 0){
				
				secound = 0;
				
				Orders order = orderService.orderInfoById(orderId);
				
				
				Map<String, Object> torders = OrderRedisData.yyorder(s.getId());
				Subscriber sub = subscriberService.querySubscriberById(s.getId());
				Integer status = 0;//0预约取消 1预约成功 2预约超时 3租车开始 4租车结束
				
				String content = MsgContent.getSmsMsg(MsgContent.ORDER_CANCLE_MSG, s.getName(), order.getOrdersNo());
				
				
				if(null!=torders&&!torders.isEmpty()){
					//获取字典表中车辆未租借状态
					Dict di = dictService.getDictByCodes("carBizState", "0");
					
					//获取 车辆信息
					Map<String, Object> cars = branchDotService.carinfo(carId,null);
					String version = cars.get("version").toString();
					String mobile = torders.get("mobile").toString();
					String userName = torders.get("userName").toString();
					
					//更新车辆状态
					int st = branchDotService.updateCarStatus(carId,di.getId(),version);
					
					if(st==1){
						//更新订单状态
						orderService.updateOrderStatus(order.getId(), status);
						orderService.updateOrderDetailStatus(order.getId());
						if(sendMSg.send(mobile, content)){
							//记录短信信息
							SMSRecord record = new SMSRecord();
							record.setContent(content);
							record.setPhoneNo(mobile);
							record.setType(3);
							record.setUserId(s.getId());
							record.setUserName(userName);
							record.setResult(mobile+":Success");
							subscriberService.addSmsRecord(record);
						}
						//从redis中删除会员 预约订单信息
						OrderRedisData.delOrder(s.getId());
						
						return Ajax.AppJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG);
					}
				}
				
				res.put("isexist", 0);//修改订单状态 为不存在
				
			}
			
			res.put("residualTime",secound);//预约超时剩余时间（秒）
			res.put("timing", carfee.getTimeBeforeGet());
			res.put("orderNo", orderNo);//订单号
			result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		}else{
			//查询用户是否有租车结束后未支付的订单信息
			Orders noPayOrders = orderService.userOrderInfoNoPay(id, Orders.STATE_ORDER_END, Orders.STATE_ORDER_NO_PAY);
			
			if(noPayOrders != null && !"".equals(noPayOrders)){
				//获取订单详情
				OrdersDetail ordersDetails = ordersDetailService.getByOrderIdDetail(noPayOrders.getId());
				//获取优惠卷
				Coupon coupon = couponService.getCouonByOrderId(noPayOrders.getId());
				
				res.put("orderType", 2);//订单类型 0 预约订单 1租车开始后的订单2租车结束后的订单
				
				String dotId = noPayOrders.getOrdersBackSiteId();//网点id
				String carId = noPayOrders.getCarId();//车辆id
				
				//订单费用信息
				Map<String,Object> orderfee = OrderFee.orderFeeInfo(noPayOrders, dictService, orderService,ordersDetailService,coupon);
				
				double allPrice = Double.valueOf(orderfee.get("actualFee").toString());
				
				//获取网点信息
				Map<String, Object> dot = branchDotService.dotInfo(dotId);
				//获取 车辆信息
				Map<String, Object> car = branchDotService.carinfo(carId,null);
				car.remove("biz_state");
				res.put("dot", dot);
				res.put("car", car);
				res.put("allMile", ordersDetails.getMileage());
				res.put("allMilePrice", ordersDetails.getMileFee());
				res.put("allTimePrice", ordersDetails.getTimeFee());
				res.put("alltime", orderfee.get("alltime"));
				res.put("allminutes", orderfee.get("allminutes"));
				res.put("orderId", noPayOrders.getId());
				res.put("orderNo", noPayOrders.getOrdersNo());
				res.put("isexist", 1);
				res.put("timePrice", orderfee.get("timePrice"));
				res.put("milePrice", orderfee.get("milePrice"));
				res.put("timeType", orderfee.get("timeType"));
				res.put("payStatus", Orders.STATE_ORDER_NO_PAY);//未支付
				
				

				String [] insuranceFees = null;//保险名称
				
				String [] insuranceNames = null;//保险金额
				
				int days = 0;//订单用单时长（单位:天）
				
				//根据订单开始时间、结束时间，计算天数
				if(!StringUtils.isEmpty(noPayOrders.getBeginTime())&&!StringUtils.isEmpty(noPayOrders.getEndTime())){
					days = ToolDateTime.getDateDaysForInsurance(noPayOrders.getBeginTime(), noPayOrders.getEndTime());
				}
				
				List<Map<String, Object>> paramList = new ArrayList<Map<String,Object>>();
				
				if(!StringUtils.isEmpty(ordersDetails.getInsuranceName())){
					
					insuranceNames = ordersDetails.getInsuranceName().split("/");
					
					if(!StringUtils.isEmpty(ordersDetails.getInsuranceFee())){
						
						insuranceFees = ordersDetails.getInsuranceFee().split("/");
					}
					
					for(int i =0;i<insuranceNames.length;i++){
						
						Map<String, Object> paramMap = new HashMap<String, Object>();
						
						if(insuranceFees != null && insuranceFees.length > 0){
							paramMap.put("insuranceName", insuranceNames[i]+"("+insuranceFees[i]+"元/单)");
							//判断订单用单时长，计算保险费用、总费用
							if(days > 0){
								paramMap.put("insuranceFee", ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
								allPrice = ArithUtil.add(allPrice, ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
							
							}else{
								paramMap.put("insuranceFee", insuranceFees[i].toString());
								allPrice = ArithUtil.add(allPrice, Double.valueOf(insuranceFees[i].toString()));
							}
							
							paramList.add(paramMap);
						}
						
					}
				}
				
				res.put("allPrice", allPrice);//总金额
				res.put("insurance", paramList);//保险名称
				
				if(coupon != null && !"".equals(coupon)){
					res.put("couponFee", coupon.getPrice());//优惠卷
					
				}else{
					res.put("couponFee", "");//优惠卷
				}
				
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
				
			}else{
				res.put("orderType", 1);//订单类型 0 预约订单 1租车开始后的订单2租车结束后的订单
				//查询用户是否有租车中的订单
				Orders ord = orderService.userOrderInfo(id,Orders.STATE_ORDER_START);
				
				if(ord==null){
					result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
					return result;
				}
				
				//获取订单详情
				OrdersDetail ordersDetails = ordersDetailService.getByOrderIdDetail(ord.getId());
				
				//获取优惠卷
				Coupon coupon = couponService.getCouonByOrderId(ord.getId());
				
				String dotId = ord.getOrdersBackSiteId();//网点id
				String carId = ord.getCarId();//车辆id
				//订单费用信息
				Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
				
				double allPrice = Double.valueOf(orderfee.get("actualFee").toString());
				
				//获取网点信息
				Map<String, Object> dot = branchDotService.dotInfo(dotId);
				//获取 车辆信息
				Map<String, Object> car = branchDotService.carinfo(carId,null);
				car.remove("biz_state");
				res.put("dot", dot);
				res.put("car", car);
				res.put("allMile", !StringUtils.isEmpty(ordersDetails.getMileage())?ordersDetails.getMileage():0);
				res.put("allMilePrice", !StringUtils.isEmpty(ordersDetails.getMileFee())?ordersDetails.getMileFee():0);
				res.put("allTimePrice", !StringUtils.isEmpty(ordersDetails.getTimeFee())?ordersDetails.getTimeFee():0);
				res.put("alltime", orderfee.get("alltime"));
				res.put("allminutes", orderfee.get("allminutes"));
//				res.put("allPrice", orderfee.get("allPrice"));
				res.put("orderId", ord.getId());
				res.put("orderNo", ord.getOrdersNo());
				res.put("isexist", 1);
				res.put("timePrice", orderfee.get("timePrice"));
				res.put("milePrice", orderfee.get("milePrice"));
				res.put("timeType", orderfee.get("timeType"));
				res.put("payStatus", ord.getPayState());//未支付
				
				
				String [] insuranceFees = null;//保险名称
				
				String [] insuranceNames = null;//保险金额
				
				int days = 0;//订单用单时长（单位:天）
				
				//根据订单开始时间、结束时间，计算天数
				if(!StringUtils.isEmpty(noPayOrders.getBeginTime())&&!StringUtils.isEmpty(noPayOrders.getEndTime())){
					days = ToolDateTime.getDateDaysForInsurance(noPayOrders.getBeginTime(), noPayOrders.getEndTime());
				}
				
				List<Map<String, Object>> paramList = new ArrayList<Map<String,Object>>();
				
				if(!StringUtils.isEmpty(ordersDetails.getInsuranceName())){
					
					insuranceNames = ordersDetails.getInsuranceName().split("/");
					
					if(!StringUtils.isEmpty(ordersDetails.getInsuranceFee())){
						
						insuranceFees = ordersDetails.getInsuranceFee().split("/");
					}
					
					for(int i =0;i<insuranceNames.length;i++){
						
						Map<String, Object> paramMap = new HashMap<String, Object>();
						
						if(insuranceFees != null && insuranceFees.length > 0){
							paramMap.put("insuranceName", insuranceNames[i]+"("+insuranceFees[i]+"元/单)");
							//判断订单用单时长，计算保险费用、总费用
							if(days > 0){
								paramMap.put("insuranceFee", ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
								allPrice = ArithUtil.add(allPrice, ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
							
							}else{
								paramMap.put("insuranceFee", insuranceFees[i].toString());
								allPrice = ArithUtil.add(allPrice, Double.valueOf(insuranceFees[i].toString()));
							}
							
							paramList.add(paramMap);
						}
						
					}
				}
				
				res.put("allPrice", allPrice);//总金额
				res.put("insurance", paramList);//保险名称
				
				if(!StringUtils.isEmpty(coupon)){
					
					res.put("couponFee", coupon.getPrice().toString());//优惠卷
				}else{
					res.put("couponFee", "0");//优惠卷
				}
				result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
			}
			
		}
		
		return result;
	}
	
	/**
	 * 查看订单详情
	 * @param data
	 * @return
	 */
	public String viewOrderDetails(String data){
		
		String result = null;
		Map<String, Object> res = new Hashtable<String, Object>();
		
		Map<String, String> pmap = JsonTools.desjsonForMap(data);
		
		//获取当前用户信息
		Subscriber sb = TokenUtils.getSubscriber(data);
		
		//判断账号是否过期
		if(StringUtils.isEmpty(sb)){
				
			return Ajax.AppJsonResult(Constants.APP_LOGIN_TOKEN,Constants.APP_LOGIN_TOKEN_MSG);
		}
		
		if(pmap.get("param").equals("error")){
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		String orderId = "";
		
		if(pmap.containsKey("orderId")){
			
			if(StringUtils.isEmpty(pmap.get("orderId"))){
				
				return Ajax.AppJsonResult(Constants.APP_ORDERID_NULL, Constants.APP_ORDERID_NULL_MSG);
			}else{
				orderId = pmap.get("orderId");
			}
			
		}else{
			
			return Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		
		//查询订单是否存在
		Orders ord = orderService.orderInfoById(orderId);
		if(ord==null){
			result = Ajax.AppResult(Constants.APP_ORDER_NO, Constants.APP_ORDER_NO_MSG,res);
			return result;
		}
		
		
		// 获取优惠卷
		Coupon coupon = couponService.getCouonByOrderId(ord.getId());
		String selectCoupon = pmap.get("selectCoupon");
		// 获取优惠券金额
		Integer couponPrice = null;
		if ("1".equals(selectCoupon) && coupon != null) {
			couponPrice = coupon.getPrice().intValue() * 100;
		} else if ("0".equals(selectCoupon) || coupon == null) {
			couponPrice = 0;
		} else {
			couponPrice = 0;
		}
		
		//获取取车网点
		Map<String, Object> takedot = branchDotService.dotInfo(ord.getBeginSiteId());
		
		//获取还车网点
		Map<String, Object> returndot = branchDotService.dotInfo(ord.getEndSiteId());
		
		//获取订单详情
		OrdersDetail orderDetail = ordersDetailService.getByOrderIdDetail(ord.getId());
		
		//获取 车辆信息
		Map<String, Object> car = branchDotService.carinfo(ord.getCarId() == null?"":ord.getCarId(),null);
		
		//获取车辆评价
		Map<String, Object> carCommentMap = new HashMap<String, Object>();
		
		CarComment cc = new CarComment();
		
		cc.setOrderId(ord.getId());
		
		CarComment carComment = carCommentService.queryCarCommentByConditions(cc);
		
		if(carComment != null){
			
			carCommentMap.put("isOk", carComment.getIsOk());
			
			carCommentMap.put("carFace", carComment.getCarFace());
			
			carCommentMap.put("carClean", carComment.getCarClean());
			
			carCommentMap.put("carImg", carComment.getCarImg());
		}
		//订单基本信息
		SimpleDateFormat tdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		res.put("createOrderTime", tdf.format(ord.getCreateDate()));//下单时间
		
		res.put("carName", car.get("cnName").toString()+car.get("name"));//车型名称加车辆品牌
		
		res.put("carNumber", car.get("vehiclePlateId") == null?"":car.get("vehiclePlateId"));//车牌号
		
		res.put("takedot", takedot == null?"":takedot.get("name"));//取车地点
		
		if(ord.getEndTime() != null && !"".equals(ord.getEndTime())){
			
			res.put("returnCarTime", tdf.format(ord.getEndTime()));
		}else{
			
			if(ord.getOrdersBackTime() != null && !"".equals(ord.getOrdersBackTime())){
				
				res.put("returnCarTime", tdf.format(ord.getOrdersBackTime()));
			}else{
				
				res.put("returnCarTime", "");
			}
			
		}
		res.put("returndot", returndot == null?"":returndot.get("name"));//还车地点
		
		/**
		 * 调用工具类,进行计费计算(抽离新增)
		 * 
		 * @author ge jiaming
		 * @date 2017-07-29
		 */
		//获取参数
		String[] insuranceFees = null;// 保险名称
		String[] insuranceNames = null;// 保险金额
		List<Map<String, Object>> paramList = new ArrayList<Map<String,Object>>();
		int insuranceCosts = 0;// 保险费用
		//获取保险单价
		if (!StringUtils.isEmpty(orderDetail.getInsuranceName())) {
			// [基础保险, 附加保险]
			insuranceNames = orderDetail.getInsuranceName().split("/");
			if (!StringUtils.isEmpty(orderDetail.getInsuranceFee())) {
				insuranceFees = orderDetail.getInsuranceFee().split("/");
			} 
			if (insuranceFees != null) {
				for (String string : insuranceFees) {
					insuranceCosts = insuranceCosts + (int) (Double.valueOf(string) * 100);
				}
			}
		}
		// 获取里程单价
		StrategyBase carfee = orderService.carfee(ord.getCarId());// 计费规则
		Double kmPriceD = carfee.getKmPrice().doubleValue();// 里程价(单位：公里)
		float kmPriceF=(float)(kmPriceD*100);
		int kmPrice=(int) kmPriceF;
		// 获取时间单价
		Double timePriceD = carfee.getBasePrice().doubleValue();// 时间基数
		float timePriceF=(float)(timePriceD*100);
		int timePrice=(int) timePriceF;
		//获取账户余额
		Map<String,Object> subInfo = subscriberService.subAccountInfo(sb.getId());
		Double amount = 0.0;
		if(subInfo != null){
			amount = Double.valueOf(subInfo.get("usable_amount").toString());//账户余额
		}
		
		Map<String, Integer> calculateFeesResult = FeesAlg.calculateFees(UseCarMode.NORMAL_MODE,
				ord.getBeginTime(), ord.getEndTime(), orderDetail.getMileage().doubleValue(),
				insuranceCosts, couponPrice, CouponType.COUPON_NORMAL, false, timePrice, kmPrice, amount.intValue(), true);
		
		double allPrice = ArithUtil.div(Double.valueOf(calculateFeesResult.get("totalPayment") + ""), 100, 2);
		//用车时间
		Integer useTime = calculateFeesResult.get("allMinute");
		int days=useTime % 1440 == 0?useTime / 1440:(useTime / 1440 + 1);
		if(!StringUtils.isEmpty(orderDetail.getInsuranceName())){
			insuranceNames = orderDetail.getInsuranceName().split("/");
			if(!StringUtils.isEmpty(orderDetail.getInsuranceFee())){
				insuranceFees = orderDetail.getInsuranceFee().split("/");
			}
			for(int i =0;i<insuranceNames.length;i++){
				Map<String, Object> paramMap = new HashMap<String, Object>();
				if(insuranceFees != null && insuranceFees.length > 0){
					paramMap.put("insuranceName", insuranceNames[i]+"("+insuranceFees[i]+"元/单)");
					//判断订单用单时长，计算保险费用、总费用
					if(days > 0){
						paramMap.put("insuranceFee", ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
					}else{
						paramMap.put("insuranceFee", insuranceFees[i].toString());
					}
					paramList.add(paramMap);
				}
			}
		}
		res.put("allMilePrice", ((double)calculateFeesResult.get("totalMilePrice"))/100);
		res.put("allTimePrice", ((double)calculateFeesResult.get("totalTimePrice"))/100);
		res.put("allPrice", allPrice);//总费用
	    res.put("payPrice", calculateFeesResult.get("cashPayment"));//实际支付金额
	    res.put("amount", 0.0);//账户余额
		res.put("insurance", paramList);//保险名称
		res.put("allMile",  calculateFeesResult.get("allMile"));//总里程 （公里）
		res.put("alltime",  useTime);//总时长
		
		
		
		/*//订单费用信息
		Map<String,Object> orderfee = OrderFee.orderFeeInfo(ord, dictService, orderService,ordersDetailService,coupon);
		
		double allPrice = Double.valueOf(orderfee.get("actualFee").toString());
		
		res.put("allMile", !StringUtils.isEmpty(orderDetail.getMileage())?orderDetail.getMileage():0);
		
//		res.put("allMilePrice", !StringUtils.isEmpty(orderDetail.getMileFee())?orderDetail.getMileFee():0);
//		
//		res.put("allTimePrice", !StringUtils.isEmpty(orderDetail.getTimeFee())?orderDetail.getTimeFee():0);
		res.put("allMilePrice", orderfee.get("allMilePrice"));
		res.put("allTimePrice", orderfee.get("allTimePrice"));
		
		res.put("alltime", orderfee.get("alltime"));
		
		res.put("allminutes", orderfee.get("allminutes"));
		
		res.put("allPrice", orderfee.get("actualFee"));
		
		res.put("timePrice", orderfee.get("timePrice"));
		
		res.put("milePrice", orderfee.get("milePrice"));
		
		res.put("timeType", orderfee.get("timeType"));
		
		if(!StringUtils.isEmpty(coupon)){
			
			res.put("coupon", coupon.getPrice());//优惠券
			
		}
		
		String [] insuranceFees = null;//保险名称
		
		String [] insuranceNames = null;//保险金额
		
		int days = 0;//订单用单时长（单位:天）
		
		//根据订单开始时间、结束时间，计算天数
		if(!StringUtils.isEmpty(ord.getBeginTime())&&!StringUtils.isEmpty(ord.getEndTime())){
			days = ToolDateTime.getDateDaysForInsurance(ord.getBeginTime(), ord.getEndTime());
		}
		
		List<Map<String, Object>> paramList = new ArrayList<Map<String,Object>>();
		
		if(!StringUtils.isEmpty(orderDetail.getInsuranceName())){
			
			insuranceNames = orderDetail.getInsuranceName().split("/");
			
			if(!StringUtils.isEmpty(orderDetail.getInsuranceFee())){
				
				insuranceFees = orderDetail.getInsuranceFee().split("/");
			}
			
			
			
			for(int i =0;i<insuranceNames.length;i++){
				
				Map<String, Object> paramMap = new HashMap<String, Object>();
				
				if(insuranceFees != null && insuranceFees.length > 0){
					paramMap.put("insuranceName", insuranceNames[i]+"("+insuranceFees[i]+"元/单)");
					//判断订单用单时长，计算保险费用、总费用
					if(days > 0){
						paramMap.put("insuranceFee", ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
//						allPrice = ArithUtil.add(allPrice, ArithUtil.mul(Double.valueOf(insuranceFees[i].toString()), days));
					
					}else{
						paramMap.put("insuranceFee", insuranceFees[i].toString());
//						allPrice = ArithUtil.add(allPrice, Double.valueOf(insuranceFees[i].toString()));
					}
					
					paramList.add(paramMap);
				}
				
			}
		}
		
		res.put("allPrice", allPrice);//总费用
		res.put("insurance", paramList);//保险名称
*/		
		res.put("orderId", ord.getId());//订单ID
		
		res.put("orderNo", ord.getOrdersNo());//订单编号
		//车辆评价
		res.put("carCommentMap", carCommentMap);
		
		res.put("isexist", 1);
		
		result = Ajax.AppResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,res);
		
		return result;
		
	}
	
}