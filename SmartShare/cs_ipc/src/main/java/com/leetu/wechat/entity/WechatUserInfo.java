package com.leetu.wechat.entity;

import java.util.Date;
import java.util.List;

import org.springframework.util.StringUtils;

/**
 * 通过网页授权获取的微信用户信息
 * 
 * @author wangjing
 * 
 */
public class WechatUserInfo {

	private String id;
	private String openId;
	private String nickname;
	private int sex;
	private String country;
	private String province;
	private String city;
	private String headImgUrl;
	private Date createTime;
	private List<String> privilegeList;
	private String subscriberId;
	
	
	private String unionId;
	
	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getHeadImgUrl() {
		return headImgUrl;
	}

	public void setHeadImgUrl(String headImgUrl) {
		this.headImgUrl = headImgUrl;
	}

	public List<String> getPrivilegeList() {
		return privilegeList;
	}

	public void setPrivilegeList(List<String> privilegeList) {
		this.privilegeList = privilegeList;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getSubscriberId() {
		return subscriberId;
	}

	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}


	public String getUnionId() {
		if(StringUtils.isEmpty(unionId)){
			return openId;
		}else{
			return unionId;
		}
		
	}

	public void setUnionId(String unionId) {
		this.unionId = unionId;
	}
	
	
}
