package com.leetu.wechat.entity;

import java.util.Date;


/**
 * @author wangjing
 *
 */
public class WechatTokenInfo {

	private String id;
	private String wxKey;
	private String wxValue;
	private Date createTime;
	private Date deadTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getWxKey() {
		return wxKey;
	}

	public void setWxKey(String wxKey) {
		this.wxKey = wxKey;
	}

	public String getWxValue() {
		return wxValue;
	}

	public void setWxValue(String wxValue) {
		this.wxValue = wxValue;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getDeadTime() {
		return deadTime;
	}

	public void setDeadTime(Date deadTime) {
		this.deadTime = deadTime;
	}


}
