package com.leetu.carservice.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.leetu.carservice.entity.CarBreak;
import com.leetu.carservice.mapper.CarBreakMapper;
import com.leetu.carservice.service.CarBreakService;
import com.util.DateUtil;
import com.util.StringHelper;

@Service
public class CarBreakServiceImpl implements CarBreakService{

	@Autowired
	private CarBreakMapper carBreakMapper;

	@Override
	public String add(Map<String, String> param) {
		param.put("ts", DateUtil.getChar19DateString());
		carBreakMapper.saveCarBreak(param);
		return param.get("id");
	}

	@Override
	public void update(Map<String, String> param) {
		carBreakMapper.updateCarBreak(param);
	}

	@Override
	public CarBreak get(String id) {
		
		if(StringHelper.isEmpty(id)){
			return null;
		}
		Map<String,String> p = new HashMap<String,String>();
		p.put("id", id);
		CarBreak car = carBreakMapper.getCarBreak(p);
		car.setTsStr(DateUtil.getChar19DateString(car.getTs()));
		return car;
	}

	@Override
	public List<CarBreak> getCarBreakByOrederId(String orderId) {
		
		if(StringHelper.isEmpty(orderId)){
			return null;
		}
		Map<String,String> p = new HashMap<String,String>();
		p.put("orderId", orderId);
		
		return carBreakMapper.getCarBreakList(p);
		
	}
}
