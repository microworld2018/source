
package com.leetu.carservice.service;

import java.util.List;
import java.util.Map;

import com.leetu.carservice.entity.CarBreak;

/**
 *
 * @package:com.leetu.carservice.service
 * @fileName:CarBreakService.java
 * @author:赵振
 * @createTime:2016年7月25日上午9:59:26
 */
public interface CarBreakService {
	
	
	public String  add(Map<String, String> param);
	
	public void update(Map<String, String> param);
	
	public CarBreak get(String  id);
	 
	public List<CarBreak> getCarBreakByOrederId(String orderId);

}
