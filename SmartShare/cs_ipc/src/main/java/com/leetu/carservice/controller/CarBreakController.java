package com.leetu.carservice.controller;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.core.controller.BaseController;
import com.leetu.carservice.entity.CarBreak;
import com.leetu.carservice.service.CarBreakService;
import com.util.Ajax;
import com.util.Constants;
import com.util.JsonTools;
import com.util.StringHelper;
import com.util.Utils;

@Controller
@RequestMapping("carBreak")
public class CarBreakController extends BaseController<CarBreakController> {

	public static final Log log = LogFactory.getLog(CarBreakController.class);
	
	@Autowired
	private CarBreakService carBreakService;

	@RequestMapping("/add")
	public void save(){
		
		String result = null;
		String data = request.getParameter("data");
		Map<String, Object> pmap = JsonTools.desjsonForMapData(data);
		if(StringHelper.isEmpty(data)){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		try {
			@SuppressWarnings("unchecked")
			Map<String, String> pdata = new HashMap<String, String>();
			for (Map.Entry<String, Object> entry : pmap.entrySet()) {  
			    System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());  
			    pdata.put(entry.getKey(), entry.getValue().toString());
			}  
			
			
			String cbid = carBreakService.add(pdata);
			Map<String,Object> rm = new HashMap<String,Object>();
			rm.put("id", cbid);
			result = Ajax.AppbzJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, rm);
		} catch (Exception e) {
			log.error("插入数据错误",e);
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION);
		}
		
		Utils.outJSONObject(result,response);
	}
	
	
	
	@RequestMapping("/get")
	public void get(){
		
		String result = null;
		String data = request.getParameter("data");
		Map<String, Object> pmap = JsonTools.desjsonForMapData(data);
		if(StringHelper.isEmpty(data)){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		try {
			@SuppressWarnings("unchecked")
			Map<String, String> pdata = (Map<String, String>) pmap.get("data");
			result = Ajax.AppbzJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG, carBreakService.get(pdata.get("id")));
			
		} catch (Exception e) {
			log.error("插入数据错误",e);
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION);
		}
		Utils.outJSONObject(result,response);
	}
	
	
	@RequestMapping("/update")
	public void update(){
		String result = null;
		String data = request.getParameter("data");
		Map<String, Object> pmap = JsonTools.desjsonForMapData(data);
		if(StringHelper.isEmpty(data)){
			result = Ajax.AppJsonResult(Constants.APP_PARAM_ERROR, Constants.APP_PARAM_ERROR_MSG);
		}
		try {
			@SuppressWarnings("unchecked")
			Map<String, String> pdata = (Map<String, String>) pmap.get("data");
			
			carBreakService.update(pdata);
			result = Ajax.AppbzJsonResult(Constants.APP_SUCCESS, Constants.APP_SUCCESS_MSG,null);
		} catch (Exception e) {
			log.error("插入数据错误",e);
			e.printStackTrace();
			result = Ajax.AppJsonResult(Constants.APP_SERVER_EXCEPTION, Constants.APP_SERVER_EXCEPTION);
		}
		Utils.outJSONObject(result,response);
	}
	
}
