
package com.leetu.carservice.mapper;

import java.util.List;
import java.util.Map;




import com.leetu.carservice.entity.CarBreak;



public interface CarBreakMapper{
	
	public CarBreak getCarBreak(Map<String, String> param);
	
	public List<CarBreak> getCarBreakList(Map<String, String> param);
	
	public void saveCarBreak(Map<String, String> param);
	
	public void updateCarBreak(Map<String, String> param);
}
