package com.util.socket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MultiThreadServer extends Thread{
	
	private int port = 8888;
	
    private ServerSocket serverSocket;
    
    private ExecutorService executorService;
    
    private final int POOL_SIZE = 10;
    
    public MultiThreadServer(ServerSocket serverScoket){  
        try {  
            if(null == serverSocket){  
                this.serverSocket = new ServerSocket(port);  
                executorService = Executors.newFixedThreadPool(Runtime.getRuntime()
                        .availableProcessors() * POOL_SIZE);
                System.out.println("***socket服务器即将启动，等待客户端的连接***"); 
            }  
        } catch (Exception e) {  
            System.out.println("SocketThread创建socket服务出错");  
            e.printStackTrace();  
        }  
  
    } 
    
    public MultiThreadServer() throws IOException {
        serverSocket = new ServerSocket(port);
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime()
                .availableProcessors() * POOL_SIZE);
        System.out.println("***socket服务器即将启动，等待客户端的连接***");
    }

    public void service() {
    	int count = 0;
        while (true) {
            Socket socket = null;
            try {
                socket = serverSocket.accept();
                executorService.execute(new Handler(socket));

                count++;
                System.out.println("服务器端被连接过的次数："+count);
                InetAddress address = socket.getInetAddress();
                System.out.println("当前客户端的IP为："+address.getHostAddress());
                
                
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
   /* public void run(){ 
    	int count = 0;
        while(!this.isInterrupted()){  
            try {  
                Socket socket = serverSocket.accept();  
                  
//                socket.setSoTimeout(30000);
                
                executorService.execute(new Handler(socket));

                count++;
                System.out.println("服务器端被连接过的次数："+count);
                InetAddress address = socket.getInetAddress();
                System.out.println("当前客户端的IP为："+address.getHostAddress());
                  
            }catch (Exception e) {  
                e.printStackTrace();  
            }  
        }  
    }*/
    
    public void closeSocketServer(){  
        try {  
             if(null!=serverSocket && !serverSocket.isClosed())  
             {  
              serverSocket.close();  
             }  
        } catch (IOException e) {  
         // TODO Auto-generated catch block  
         e.printStackTrace();  
        }  
      } 

    public static void main(String[] args) throws IOException {
        new MultiThreadServer().service();
    }
}
