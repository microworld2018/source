package com.util.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import net.sf.json.JSONObject;

public class Handler implements Runnable{

	public static final String CHARCODE = "utf-8";
    
    private Socket socket;
    
    public Handler(Socket socket) {
        this.socket = socket;
    }
    
    private PrintWriter getWriter(Socket socket) throws IOException {
        OutputStream socketOut = socket.getOutputStream();
        return new PrintWriter(socketOut, true);
    }
    
    private BufferedReader getReader(Socket socket) throws IOException {
        InputStream socketIn = socket.getInputStream();
        return new BufferedReader(new InputStreamReader(socketIn));
    }
    
    public void run() {
        BufferedReader br = null;
        PrintWriter out = null;
        try {
            br = getReader(socket);

            out = getWriter(socket);
            String msg = null;
            while ((msg = br.readLine()) != null) {
//                System.out.println("s1:" + msg);
                msg = Util.decode(msg,CHARCODE);
                System.out.println("客户端传递的数据:" + msg);

                JSONObject obj = new JSONObject();
                obj.put("orderId", "1111111");
                obj.put("carId", "3232323");
                String res = Util.encode(obj.toString().getBytes(CHARCODE));
                System.out.println("服务器端返回的数据:" + Util.decode(res,CHARCODE));

                out.println(res);
                out.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (socket != null)
                    socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (br != null)
                    br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (out != null) {
                out.close();
            }
        }
    }
    
    /**
     * socket关闭服务
     */
    public void closeSocketServer(){  
        try {  
             if(null!=socket && !socket.isClosed()){  
            	 socket.close();  
             }  
        }catch (IOException e) {  
        	e.printStackTrace();  
        }  
    }
}
