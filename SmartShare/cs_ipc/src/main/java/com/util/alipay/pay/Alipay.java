package com.util.alipay.pay;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeQueryModel;
import com.alipay.api.request.AlipayDataDataserviceBillDownloadurlQueryRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayDataDataserviceBillDownloadurlQueryResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.util.HttpRequestUtil;
import com.util.ToolDateTime;
import com.util.alipay.config.AlipayConfig;
import com.util.alipay.sign.RSA;
import com.util.alipay.util.AlipayNotify;

public class Alipay {

	
	
	/**
	 * 移动端  支付宝支付
	 * @param orderNum
	 * @param filling
	 * @return
	 */
	public static JSONObject ydalipay(String orderNum,String total_fee,String body,String notifyUrl){
		Map<String, String> sParaTemp = new HashMap<String, String>();
	
		sParaTemp.put("service", AlipayConfig.service);//接口名称
        sParaTemp.put("partner", AlipayConfig.partner);//签约用户号。
        sParaTemp.put("_input_charset", AlipayConfig.input_charset);//编码格式
        sParaTemp.put("seller_id", AlipayConfig.seller_id);// 收款支付宝账号，以2088开头由16位纯数字组成的字符串，一般情况下收款账号就是签约账号
		sParaTemp.put("payment_type", AlipayConfig.payment_type);//支付类型 默认为 1
		sParaTemp.put("notify_url", notifyUrl);
		sParaTemp.put("out_trade_no", orderNum);//商户网站唯一订单号
		sParaTemp.put("subject", "order_sn"+orderNum);//商品名称
		sParaTemp.put("total_fee", total_fee);//交易金额
		sParaTemp.put("body", body);//交易内容
		String str = "partner=\""+AlipayConfig.partner+"\"&seller_id=\""+AlipayConfig.seller_id+"\"&out_trade_no=\""+orderNum+"\"&subject=\""+"order_sn"+orderNum+"\"&body=\""+body+"\"&total_fee=\""+total_fee+"\"&notify_url=\""+notifyUrl+"\"&service=\""+AlipayConfig.service+"\"&payment_type=\""+AlipayConfig.payment_type+"\"&_input_charset=\""+AlipayConfig.input_charset+"\"";
		String sign = URLEncoder.encode(RSA.buildRequestMysign(str));//sign进行URLEncoder加密
		sParaTemp.put("sign", sign);//签名
		System.out.println(sign.length());
		sParaTemp.put("sign_type", AlipayConfig.sign_type);//签名方式
		
		str = str +"&sign="+"\""+sign+"\"";
		str = str +"&sign_type="+"\""+AlipayConfig.sign_type+"\"";
		
		sParaTemp.put("aliparameter", str);
		JSONObject json = JSONObject.fromObject(sParaTemp);
		return json;
	}
	
	/**
	 * 移动端  支付宝支付(新)
	 * @param orderNum
	 * @param filling
	 * @return
	 */
	public static JSONObject ydalipayNew(String orderNum,String total_fee,String body,String notifyUrl){
		Map<String, String> sParaTemp = new HashMap<String, String>();
	
		sParaTemp.put("service", AlipayConfig.service);//接口名称
        sParaTemp.put("partner", AlipayConfig.partner);//签约用户号。
        sParaTemp.put("_input_charset", AlipayConfig.input_charset);//编码格式
        sParaTemp.put("seller_id", AlipayConfig.seller_id);// 收款支付宝账号，以2088开头由16位纯数字组成的字符串，一般情况下收款账号就是签约账号
		sParaTemp.put("payment_type", AlipayConfig.payment_type);//支付类型 默认为 1
		sParaTemp.put("notify_url", notifyUrl);
		sParaTemp.put("out_trade_no", orderNum);//商户网站唯一订单号
		sParaTemp.put("subject", "order_sn"+orderNum);//商品名称
		sParaTemp.put("total_fee", total_fee);//交易金额
		sParaTemp.put("body", body);//交易内容
		String str = "partner=\""+AlipayConfig.partner+"\"&seller_id=\""+AlipayConfig.seller_id+"\"&out_trade_no=\""+orderNum+"\"&subject=\""+"order_sn"+orderNum+"\"&body=\""+body+"\"&total_fee=\""+total_fee+"\"&notify_url=\""+notifyUrl+"\"&service=\""+AlipayConfig.service+"\"&payment_type=\""+AlipayConfig.payment_type+"\"&_input_charset=\""+AlipayConfig.input_charset+"\"";
		String sign = URLEncoder.encode(RSA.buildRequestMysign(str));//sign进行URLEncoder加密
		sParaTemp.put("sign", sign);//签名
		System.out.println(sign.length());
		sParaTemp.put("sign_type", AlipayConfig.sign_type);//签名方式
		
		str = str +"&sign="+"\""+sign+"\"";
		str = str +"&sign_type="+"\""+AlipayConfig.sign_type+"\"";
		
		sParaTemp.put("aliparameter", str);
		JSONObject json = JSONObject.fromObject(sParaTemp);
		return json;
	}
	
	/**
	 * 同步通知 签名验证是否正确
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static Map<String,Object> tbreturn(HttpServletRequest request) throws UnsupportedEncodingException{
		String status = "no";
		//获取支付宝GET过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
						: valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
			params.put(name, valueStr);
		}
		
		//商户订单号
		String out_trade_no = new String(request.getParameter("out_trade_no").getBytes("ISO-8859-1"),"UTF-8");
		//支付宝交易号
		String trade_no = new String(request.getParameter("trade_no").getBytes("ISO-8859-1"),"UTF-8");
		//交易状态
		String trade_status = new String(request.getParameter("trade_status").getBytes("ISO-8859-1"),"UTF-8");
		String total_fee = new String(request.getParameter("total_fee").getBytes("ISO-8859-1"),"UTF-8");
		String is_success = new String(request.getParameter("is_success").getBytes("ISO-8859-1"),"UTF-8");
		String notify_time = new String(request.getParameter("notify_time").getBytes("ISO-8859-1"),"UTF-8");
		//计算得出通知验证结果
		boolean verify_result = AlipayNotify.verify(params);
		
		Map<String,Object> ordermap = new HashMap<String, Object>();
		if(verify_result){//验证成功
			status = "ok";
			ordermap.put("out_trade_no", out_trade_no);
			ordermap.put("trade_no", trade_no);
			ordermap.put("trade_status", trade_status);
			ordermap.put("is_success", is_success);
			ordermap.put("total_fee", total_fee);
			ordermap.put("status", status);
			ordermap.put("notify_time", notify_time);
		} 
		return ordermap;
	}
	/**
	 * 异步通知 签名验证是否正确
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static Map<String,Object> ybreturn(HttpServletRequest request) throws UnsupportedEncodingException{
		
		Map<String,Object> ordermap = new HashMap<String, Object>();
		
		String status = "no";
		//获取支付宝POST过来反馈信息
		Map<String,String> params = new HashMap<String,String>();
		Map requestParams = request.getParameterMap();
		for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
			String name = (String) iter.next();
			String[] values = (String[]) requestParams.get(name);
			String valueStr = "";
			for (int i = 0; i < values.length; i++) {
				valueStr = (i == values.length - 1) ? valueStr + values[i]
				                                                        : valueStr + values[i] + ",";
			}
			//乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
			//valueStr = new String(valueStr.getBytes("ISO-8859-1"), "gbk");
			params.put(name, valueStr);
		}
		
		String resultStatus = params.get("resultStatus");
		System.out.println(resultStatus);
		String result = params.get("result");
		System.out.println(result);
		Map<String, Object> maps = new HashMap<String, Object>();
		String[] str = result.split("&");
		for(int i=0;i<str.length;i++){
			String[] str1 = str[i].split("=");
			maps.put(str1[0], str1[1]);
		}
		System.out.println(maps.get("out_trade_no"));
		System.out.println(maps.get("total_fee"));
		
		//商户订单号
		String out_trade_no = params.get("out_trade_no");
		//支付宝交易号
		String trade_no = params.get("trade_no");
		//交易状态
		String trade_status = params.get("trade_status");
		String total_fee = params.get("total_fee");
		//交易付款时间
		String gmt_payment = params.get("gmt_payment");
		
		//获取支付宝的通知返回参数
		boolean abc = AlipayNotify.verify(params);
		
		System.out.println(abc+"====abc");
		
		if(AlipayNotify.verify(params)){//验证成功
			if(trade_status.equals("TRADE_SUCCESS")){
				status = "ok";
				ordermap.put("status", status);
				ordermap.put("orderNo", out_trade_no);
				ordermap.put("tradeRecordNo", trade_no);
				ordermap.put("trade_status", trade_status);
				ordermap.put("total_fee", total_fee);
				ordermap.put("time_end", gmt_payment);
			}else{
				ordermap.put("status", trade_status);
			}
		}else{
			ordermap.put("status", status);
		}
		return ordermap;
	}
	
	/**
	 * 支付宝查询订单状态
	 * @author Jin Guangyu
	 * @date 2017年7月27日下午2:07:14
	 * @return
	 * @throws Exception 
	 *//*
	public static JSONObject queryOrder(String tradeOrderNo) throws Exception{
		AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",
															"2016072901682603",
															"MIICdQIBADANBgkqhkiG9w0BAQEFAASCAl8wggJbAgEAAoGBAMfpzhW1He+Lka1i4yR3G+ewyak2sVzAv0jAd4RCetfg5Ez+pS0QQtjeiOdB22p3w9ojad4fsCPzU4ZYQqynTVncqM5JvyLC3sscCjbjpuLi9B1bVsCpWhVN6OO/nQf1ceb+6O7110vRYsZdg1BI40MX2rvgAJjQUfVXzONSGExPAgMBAAECgYAracKvRB20H4MLPCZHjPYqYmHotU/e7Jydl+5ccnNq3hO2BLaqCpX5spnYXpzfihKmFHqxGgCOThEdY51XER3iu1siIQwPuQJ0c/x5iTZx8Y7l89hUySf9nvT5r7YO2Q8MGG87tp7c6Pc31fggVA8x7J3E/M2RiQ0VpsTVfF32AQJBAPN785huVZn4vL/ClZDHQnXKkk51eZAVDFSu9vGq7f+uqQkFlZ5W1uFWJ2LnDycG/4y7LiKn0dovzyJa6MxgAeECQQDSMH89ggBw9DQ8B36vZvsVguoenFZ4KJCMGkFVgU8TTNzWK4NBcEa/n9FAQljM96QtKU9iWvcxcgmlpLL7XnQvAkAvQ9aLto9jbkelqe5Cxot+Ni6kkGVitNNJ9RT24MmGoq8pky3yKMVZxNGsf1IPr2oYNZXFIHK6OiGpd6BNXC3BAkAtKrS8/+meXCu5Vpb3lDaSWc57g3blXToqkR3HBQF8EUFHWJRdBIZ/eRVbLKaubOy/s6fWZ9vEqU+/Y5P+hj6LAkBC6NgwiOHSJBzcJb+A646bX1yxHtkyB830RfLKHeQg0nM0YFck4wPpvU1HTkRh6Y1AAgAnYNeM4JGV0KBl2WTl",
															"json",
															"utf-8",
															"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCnxj/9qwVfgoUh/y2W89L6BkRAFljhNhgPdyPuBV64bfQNN1PjbCzkIM6qRdKBoLPXmKKMiFYnkd6rAoprih3/PrQEB/VsW8OoM8fxn67UDYuyBTqA23MML9q1+ilIZwBC2AQ2UBVOrFXfFl75p6/B5KsiNG9zpgmLCUYuLkxpLQIDAQAB",
															"RSA");
		AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
		request.setBizContent("{" +
				"\"out_trade_no\":\"DD20170728090028011409\"" +
				"  }");
		AlipayTradeQueryResponse response = alipayClient.execute(request);
		if(response.isSuccess()){
			System.out.println("调用成功");
			String body = response.getBody();
			JSONObject json = JSONObject.fromObject(body);
			return json;
		} else {
			System.out.println("调用失败");
		}
		return null;
	}*/
	
	
	public static void main(String[] args) throws Exception{
		AlipayClient alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do",AlipayConfig.GATEWAY_APP_ID,AlipayConfig.private_key,"json","utf-8",AlipayConfig.ali_public_key,AlipayConfig.sign_type);
		AlipayDataDataserviceBillDownloadurlQueryRequest request = new AlipayDataDataserviceBillDownloadurlQueryRequest();
		request.setBizContent("{" +
				"\"bill_type\":\"trade\"," +
				"\"bill_date\":\"2017-02\"" +
				"}");
		AlipayDataDataserviceBillDownloadurlQueryResponse response = alipayClient.execute(request);
		System.out.println(response.getBillDownloadUrl());
		if(response.isSuccess()){
			System.out.println("调用成功");
		} else {
			System.out.println("调用失败");
		}
//		JSONObject jsonObject = queryOrder("2017041215540001");
//		System.out.println(jsonObject);
	}
	
}
