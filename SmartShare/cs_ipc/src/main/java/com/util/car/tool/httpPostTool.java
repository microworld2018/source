package com.util.car.tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.util.car.token.token;

public class httpPostTool {

	
	/**
	 * 发送请求
	 * @param url
	 * @param param
	 * @return
	 */
	public static Map<String, Object> httpPost(String url,Map<String, Object> param){
		Map<String, Object> result = new HashMap<String, Object>();
		//加密请求参数
		String[] str = token.createToken();
		param.put("ms", str[0]);
		param.put("token", str[1]);
		String data = SubmitPost(url, param.toString());
		result = parseJSON2Map(data);
		return result;
	}
	/**
	 * json转map
	 * @param jsonStr
	 * @return
	 */
	public static Map<String, Object> parseJSON2Map(String jsonStr){  
        Map<String, Object> map = new HashMap<String, Object>();  
        //最外层解析  
        JSONObject json = JSONObject.fromObject(jsonStr);  
        for(Object k : json.keySet()){  
            Object v = json.get(k);   
            //如果内层还是数组的话，继续解析  
            if(v instanceof JSONArray){  
                List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();  
                Iterator<JSONObject> it = ((JSONArray)v).iterator();  
                while(it.hasNext()){  
                    JSONObject json2 = it.next();  
                    list.add(parseJSON2Map(json2.toString()));  
                }  
                map.put(k.toString(), list);  
            } else {  
                map.put(k.toString(), v);  
            }  
        }  
        return map;  
    }  
	
	/**
	 * httpPost发送post请求
	 * @param url
	 * @param data
	 * @return
	 */
	public static String SubmitPost(String url, String data) {
		String result = null;
		HttpClient httpclient = new DefaultHttpClient();
		try {
			HttpPost httppost = new HttpPost(url);
			
			StringBody scdata = new StringBody(data);

			MultipartEntity reqEntity = new MultipartEntity();
			reqEntity.addPart("data", scdata);// 普通参数
			httppost.setEntity(reqEntity);

			HttpResponse response = httpclient.execute(httppost);

			int statusCode = response.getStatusLine().getStatusCode();

			if (statusCode == HttpStatus.SC_OK) {
				 
				HttpEntity resEntity = response.getEntity();
				result = EntityUtils.toString(resEntity);
				
				EntityUtils.consume(resEntity);
			}

		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				httpclient.getConnectionManager().shutdown();
			} catch (Exception ignore) {

			}
		}
		return result;
	}
	
}
