package com.util.car;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import org.springframework.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.leetu.orders.util.OrderRedisData;
import com.util.ContantsTool;
import com.util.HttpRequestUtil;
import com.util.MapDistance;
import com.util.PropertiesUtil;

public class CurrentCarInfo {

	private static final String MSG_URL = PropertiesUtil.getStringValue("msg_url");
	
	public static final String RESTORE_POWER_URL = MSG_URL+"/car/restorePower";
	
	public static final String OPEN_DOOR_URL = MSG_URL+"/car/openDoor";
	
	public static final String CLOSE_DOOR_URL = MSG_URL+"/car/closeDoor";
	
	public static final String FIND_CAR_URL = MSG_URL+"/car/findCar";
	
	public static final String GPS_INFO_URL = MSG_URL+"/car/getGPSInfo";
	
	public static final String LOCK_POWER_OFF_URL = MSG_URL+"/car/lockPowerOff";
	
	public static final String OPEN_LOCK_POWER_URL = MSG_URL+"/car/openLockPower";
	
	public static final String CAR_MILEAGE_FOR_GPS_URL = MSG_URL+"/car/getCarMileageForGPS";
	
	public static Map<String,Object> getCarmp(String carId){
		/*Map<String,String> res = new Hashtable<String,String>();
		res.put("lifeMileage", "200");
		res.put("percent", "80%");*/
		return OrderRedisData.getCarStatus(carId);
	}
	
	/**
	 * 获取当前车辆经纬度
	 * @param carId
	 * @return
	 */
	public static Map<String,String> getCarlatlng(String sn){
		Map<String,String> res = new Hashtable<String,String>();
		
		//还车远程断电
//		String msgUrls = PropertiesUtil.getStringValue("msg_url")+"/car/getGPSInfo";
		
//		String msgUrls = "http://114.215.143.226:8081/cs_car_msg/car/getGPSInfo";
		
		Map<String, String> paramMaps = new HashMap<String, String>();
		paramMaps.put("sn", sn);
		
		String longitude = "";
		String latitude = "";
		
		String contents = HttpRequestUtil.getPostMethod(GPS_INFO_URL,paramMaps);
		
		if(contents != null && !"".equals(contents)){
			JSONObject obj = JSONObject.fromObject(contents);
			
			JSONArray array = JSONArray.fromObject(obj.getString("data"));
			
			if(array != null && array.size() > 0){
				for(int i = 0; i <array.size();i++){
					
					JSONObject jsonObject = (JSONObject)array.get(i);
					
					longitude = jsonObject.getString("longitude");
					latitude =jsonObject.getString("latitude");
					
					String lngLat = longitude+","+latitude;
					
					String apiContents = HttpRequestUtil.getPostMethod("http://api.map.baidu.com/geoconv/v1/?coords="+lngLat+"&from=1&to=5&output=json&ak=BtxULUWSmvG50D5GKe0ka9Yk",null);
//					{"status":0,"result":[{"x":116.41743363693,"y":40.061229924549}]}
//					40.061229889283}]}
					/*String []abc = apiContents.split(":");
					String xx = abc[3].split(",")[0];
					String yy = abc[4].split("}")[0];
					longitude = xx;
					latitude = yy;
					System.out.println(xx+"==="+yy);*/
					JSONObject appObj = JSONObject.fromObject(apiContents);
					
//					JSONObject appObj = JSONObject.fromObject(AppSendUtils.connectURL("http://api.map.baidu.com/geoconv/v1/?coords="+lngLat+"&from=1&to=5&output=json&ak=BtxULUWSmvG50D5GKe0ka9Yk", ""));
					JSONArray ary =JSONArray.fromObject(appObj.getString("result"));
					
					if(ary != null && ary.size() > 0){
						for(int j  = 0; j<ary.size();j++){
							JSONObject o = (JSONObject)ary.get(j);
							
							longitude = o.getString("x");
							latitude =o.getString("y");
						}
					}
					
				}
			}
			
		}else{
			System.out.println("获取车辆当前经纬度返回数据为空！！");
		}
		
		System.out.println(latitude+"==="+longitude);
		
		res.put("lat", longitude);
		res.put("lng", latitude);
		return res;
	}
	
	public static void main(String[] args) {
		getCarlatlng("250004363");
	}
	
	/**
	 * 判断当前网点 与 车辆的位置 是否在 误差范围内
	 * @param carId
	 * @param raidus
	 * @param lat
	 * @param lng
	 * @return
	 */
	public static boolean isDoatCar(String sn,String lat,String lng){
		Integer raidus = Integer.parseInt(ContantsTool.GPS_RAIDUS);
		boolean state = false;
		
		Map<String,String> car =  getCarlatlng(sn);
		
		if(!StringUtils.isEmpty(car)){
			String carLat = car.get("lat");
			String carLng = car.get("lng");
			
			double distance = MapDistance.Distance(Double.parseDouble(carLat), Double.parseDouble(carLng),Double.parseDouble(lng), Double.parseDouble(lat));
			System.out.println("还车距离:"+raidus+"米====实际相差距离"+distance);
			
			//判断车辆当前位置与还车网之间的距离是否在误差范围内
			if(Double.valueOf(raidus)>=distance){
				state = true;
			}
		}
		
		return state;
	}
	
	
}
