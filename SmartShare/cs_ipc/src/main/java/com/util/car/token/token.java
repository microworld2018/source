package com.util.car.token;

	/**
	 * 生成令牌，用于双方服务器验证
	 * @package:com.util
	 * @fileName:token.java
	 * @createTime:2016年7月27日下午4:21:01
	 */
	public class token {
		
		/**
		 * 扰码
		 */
		private static final String scrambleCode = "ABCDEFGHIGKLMN123~!@#$%^";
		/**
		 * 失效时间
		 */
		private static final int failureSecond = 300; //令牌有效时间 秒 五分钟  注意双方服务器的时间是否一致
		
		/**
		 * TODO:createToken(生成令牌)
		 * @createTime:2016年7月27日下午5:08:07
		 * @return:String
		 * @return
		 */
		public static String[] createToken(){
			
			String[] resultCode = new String[2];
			
			String ms = String.valueOf(System.currentTimeMillis());
			resultCode[0] = ms;
			ms = ms.substring(ms.length()-4, ms.length());
			
			String md5ScrambleCode = MD5.ToMD5(scrambleCode+ms);
			
			md5ScrambleCode = md5ScrambleCode.substring(md5ScrambleCode.length()-10, md5ScrambleCode.length());
			
			resultCode[1] = MD5.ToMD5(md5ScrambleCode+scrambleCode);
			
			return resultCode;
		}
		/**
		 * TODO:createToken(根据时间戳 生成令牌)
		 * @createTime:2016年7月27日下午5:07:54
		 * @return:String
		 * @param ms
		 * @return
		 */
		public static String createToken(String ms){
			
			String resultCode = "";
			
			ms = ms.substring(ms.length()-4, ms.length());
			
			String md5ScrambleCode = MD5.ToMD5(scrambleCode+ms);
			
			md5ScrambleCode = md5ScrambleCode.substring(md5ScrambleCode.length()-10, md5ScrambleCode.length());
			
			resultCode = MD5.ToMD5(md5ScrambleCode+scrambleCode);
			
			return resultCode;
		}
		
		/**
		 * TODO:verifyToken(验证令牌)
		 * @createTime:2016年7月27日下午5:07:42
		 * @return:boolean
		 * @param ms
		 * @param token
		 * @return
		 */
		public static boolean verifyToken(String ms,String token){
			
			if(null==ms||"".equals(ms)||null==token||"".equals(token)){
				
				return false;
			}
			
			long localMS = System.currentTimeMillis();
			
			long remoteMS = Long.valueOf(ms);
			
			if(((localMS - remoteMS) / 1000) > failureSecond ) {
				
				return false;
				
			}else{
				
				String localToken = createToken(ms);
				
				if(localToken.equals(token)){
					
					return true;
				}
			}
			
			return false;
		}
		
		

		public static void main(String[] args) {
			String[] str = createToken();			 
			System.out.println(verifyToken(str[0],str[1]));
		}
}
