package com.util.car;

import java.util.Hashtable;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;

/***
 * redis 数据信息  （更新车辆租借和为租借的车辆信息） 
 * @author user
 *
 */
public class RedisCar {

	public static void main(String[] args) {
		
		String carId = "ff808081574f8d9401574fe33743001e";
		endRedisCarAndDeice(carId);
		//租借中得汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> ActiveIDs = (Hashtable<String, String>) RedisClient.getobjkeys("ActiveIDs",  DataBaseUtil.CS_CAR_MSG);
		//租借中得汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> ActiveSNs = (Hashtable<String, String>) RedisClient.getobjkeys("ActiveSNs", DataBaseUtil.CS_CAR_MSG);
		
		//未租借中得汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> InactiveIDs = (Hashtable<String, String>) RedisClient.getobjkeys("InactiveIDs", DataBaseUtil.CS_CAR_MSG);
		//未借中得汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> InactiveSNs = (Hashtable<String, String>) RedisClient.getobjkeys("InactiveSNs",DataBaseUtil.CS_CAR_MSG);
		
		System.out.println(ActiveIDs);
		System.out.println(ActiveSNs);
		System.out.println(InactiveIDs);
		System.out.println(InactiveSNs);
		
	}
	
	/**
	 * 租车结束
	 * @param carId
	 */
	public static void endRedisCarAndDeice(String carId){
		
		//租借中得汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> ActiveIDs = (Hashtable<String, String>) RedisClient.getobjkeys("ActiveIDs",  DataBaseUtil.CS_CAR_MSG);
		//租借中得汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> ActiveSNs = (Hashtable<String, String>) RedisClient.getobjkeys("ActiveSNs", DataBaseUtil.CS_CAR_MSG);
		
		//未租借中得汽车列表 //key = 汽车ID， value = 汽车SN 
		Hashtable<String,String> InactiveIDs = (Hashtable<String, String>) RedisClient.getobjkeys("InactiveIDs", DataBaseUtil.CS_CAR_MSG);
		//未借中得汽车列表 //key = 汽车SN， value = 汽车ID
		Hashtable<String,String> InactiveSNs = (Hashtable<String, String>) RedisClient.getobjkeys("InactiveSNs",DataBaseUtil.CS_CAR_MSG);
		
		String deviceNo = ActiveIDs.get(carId);
		ActiveIDs.remove(carId);
		ActiveSNs.remove(deviceNo);
		
		InactiveIDs.put(carId, deviceNo);
		InactiveSNs.put(deviceNo, carId);
		
		RedisClient.setobjdata("ActiveIDs", ActiveIDs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("ActiveSNs", ActiveSNs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("InactiveIDs", InactiveIDs, DataBaseUtil.CS_CAR_MSG);
		RedisClient.setobjdata("InactiveSNs", InactiveSNs, DataBaseUtil.CS_CAR_MSG);
	}
	
	/**
	 * 开始租车
	 * @param carId
	 */
	public static void startRedisCarAndDeice(String carId){
			
			//租借中得汽车列表 //key = 汽车ID， value = 汽车SN 
			Hashtable<String,String> ActiveIDs = (Hashtable<String, String>) RedisClient.getobjkeys("ActiveIDs",  DataBaseUtil.CS_CAR_MSG);
			//租借中得汽车列表 //key = 汽车SN， value = 汽车ID
			Hashtable<String,String> ActiveSNs = (Hashtable<String, String>) RedisClient.getobjkeys("ActiveSNs", DataBaseUtil.CS_CAR_MSG);
			
			//未租借中得汽车列表 //key = 汽车ID， value = 汽车SN 
			Hashtable<String,String> InactiveIDs = (Hashtable<String, String>) RedisClient.getobjkeys("InactiveIDs", DataBaseUtil.CS_CAR_MSG);
			//未借中得汽车列表 //key = 汽车SN， value = 汽车ID
			Hashtable<String,String> InactiveSNs = (Hashtable<String, String>) RedisClient.getobjkeys("InactiveSNs",DataBaseUtil.CS_CAR_MSG);
			
			String deviceNo = InactiveIDs.get(carId);
			if(deviceNo!=null){
				ActiveIDs.put(carId, deviceNo);
				ActiveSNs.put(deviceNo, carId); 
				
				InactiveIDs.remove(carId);   
				InactiveSNs.remove(deviceNo);
			}
			
			RedisClient.setobjdata("ActiveIDs", ActiveIDs, DataBaseUtil.CS_CAR_MSG);
			RedisClient.setobjdata("ActiveSNs", ActiveSNs, DataBaseUtil.CS_CAR_MSG);
			RedisClient.setobjdata("InactiveIDs", InactiveIDs, DataBaseUtil.CS_CAR_MSG);
			RedisClient.setobjdata("InactiveSNs", InactiveSNs, DataBaseUtil.CS_CAR_MSG);
			
	}
}
