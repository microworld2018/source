package com.util;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;


public class Utils {

	protected static final Logger tagInfo = Logger.getLogger("tagInfo");
	
	public static JsonConfig config = new JsonConfig();

    /**
     * 判断字符是否为空
     * @param str 待判断的字符
     * @return
     */
	public static boolean isBland(String str){
		return null != str && !"".equals(str) ;
	}
	
	/**
     * 判断数字是否为空
     * @param str 待判断的数字
     * @return
     */
	public static boolean isBland(Integer id){
		return null != id && 0 < id;
	}
	
	public static String requestGetParameter(HttpServletRequest request,String paramteter){
		return (request.getParameter(paramteter)==null||"".equals(request.getParameter(paramteter).trim()))?null:request.getParameter(paramteter);
	}
	
	/**
	 * @desc 将List对象转换为JSONArray对象并输出
	 * @param list
	 * @param response
	 */
	public static void outJSONArray(List<?> list, HttpServletResponse response){
		JSONArray jsonArray = null;
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if(null != list && 0<list.size()){
				jsonArray=JSONArray.fromObject(list);
			}
			out.print(jsonArray);
//			System.err.println("jsonArray is :" + jsonArray);
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
	
	/**
	 * @desc 将一个对象转换成JSONObject对象并输出
	 * @param object
	 * @param callback
	 * @param response
	 */
	public static void outJSONObject(Object object, String callback, HttpServletResponse response){
		JSONObject json = null;
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if(null != object){
				json = JSONObject.fromObject(object);
				if(callback != null){
					out.write(callback +"(" + json + ")");
				}else{
					out.print(json);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
	
	/**
	 * @desc 输出JSONObject对象
	 * @param object
	 * @param callback
	 * @param response
	 */
	public static void outJSONObject(JSONObject json, String callback, HttpServletResponse response){
		
		//config.addIgnoreFieldAnnotation(org.codehaus.jackson.annotate.JsonIgnore.class);
		//json=JSONObject.fromObject(json, config);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if(null != json){
				if(callback != null){
//					out.write(callback +"(" + json + ")");
					out.write(json.toString());
				}else{
					out.print(json);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
	
	public static void outJSONObject(String data, HttpServletResponse response){
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if(data != null){
				out.write(data);
			}else{
				out.print(data);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
	
	/**
	 * 
	 * @param object
	 * @param response
	 * @param key
	 */
	public static void outputJSONObject(Object object,HttpServletResponse response,String key){
		 JSONObject json = new JSONObject();
			response.setContentType("text/html;charset=utf-8");
			PrintWriter out = null;
			json.element(key, object);
			try{
				out = response.getWriter();
				if (null != json) {
					out.write(json.toString());
				} else {
					out.print(json);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (null != out) {
					out.flush();
					out.close();
				}
			}
	}
	 /**
	 
	
	 /**
	  * 存放手机验证码
	  * @param request
	  * @param code
	  */
	 public static void setmobilecode(HttpServletRequest request,String code){
		 HttpSession session = request.getSession();
		 String mobile = request.getParameter("mobile");
		 session.setAttribute(mobile, code);
		 session.setMaxInactiveInterval(60*5);//失效时间5分钟
	 }
	 
	 /**
	  * 获取手机验证码
	  * @param request
	  * @return
	  */
	 public static String getmobilecode(HttpServletRequest request){
		 HttpSession session = request.getSession();
		 String mobile = request.getParameter("mobile");
		 String code = null;
		 try {
			 code = session.getAttribute(mobile).toString();
		} catch (Exception e) {
			code = null;
		}
		 return code;
	 }
	
	 @SuppressWarnings("unchecked")
	public static final <T> T  map2Bean(Map  m ,T t){
		 ObjectMapper objectMapper = new ObjectMapper();
		return (T) objectMapper.convertValue(m,t.getClass());
	 }
	 
	 public static final Map<String, Object> bean2Map(Object bean){
		 ObjectMapper objectMapper = new ObjectMapper();
		 Map<String, Object> objectAsMap = objectMapper.convertValue(bean, Map.class);
		 return objectAsMap;
	 }
	 
   /* public static final Map<String, Object>  beant2Map(Object bean) throws IntrospectionException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{  
        Map<String, Object> objectAsMap = new HashMap<String, Object>();
        BeanInfo info = Introspector.getBeanInfo(bean.getClass());
        for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
            Method reader = pd.getReadMethod();
            if (reader != null)
                objectAsMap.put(pd.getName(),reader.invoke(bean));
        }
        
        return objectAsMap;  
    }  */
    
}
