package com.util;

/**
 * Java 快速排序法
 * @author hzw
 *
 */
public class Quick {
    public double[] quick_sort(double[] arrays, int lenght) {
        if (null == arrays || lenght < 1) {
            System.out.println("input error!");
            return arrays;
        }
       return _quick_sort(arrays, 0, lenght - 1);
    }

    public double[] _quick_sort(double[] arrays, int start, int end) {
        if(start>=end){
            return arrays;
        }
        
        int i = start;
        int j = end;
        double value = arrays[i];
        boolean flag = true;
        while (i != j) {
            if (flag) {
                if (value > arrays[j]) {
                    swap(arrays, i, j);
                    flag=false;

                } else {
                    j--;
                }
            }else{
                if(value<arrays[i]){
                    swap(arrays, i, j);
                    flag=true;
                }else{
                    i++;
                }
            }
        }
//        snp(arrays);
        _quick_sort(arrays, start, j-1);
        _quick_sort(arrays, i+1, end);
        return arrays;
    }

    public void snp(double[] arrays) {
        for (int i = 0; i < arrays.length; i++) {
            System.out.print(arrays[i] + " ");
        }
        System.out.println();
    }

    private void swap(double[] arrays, int i, int j) {
    	double temp;
        temp = arrays[i];
        arrays[i] = arrays[j];
        arrays[j] = temp;
    }

    public static void main(String args[]) {
        Quick q = new Quick();
        double[] a = {49.9,38.00987,65.7867868976,12,45,5,1,6,8};
        double [] arrays = q.quick_sort(a,a.length);
        for (int i = 0; i < arrays.length; i++) {
            System.out.print(arrays[i] + " \t");
        }
        System.out.println();
    } 

}
