package com.util;

import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

public class StringUtil {
	private static Locale locale = new Locale("zh", "CN");
	public static ResourceBundle bundle = ResourceBundle.getBundle("config", locale);
	/**
	 * 产生随机字符串
	 * */
	private static Random randGen = null;
	private static char[] numbersAndLetters = null;
	public static final String REG_MOBILE = "^((13[0-9])|(14[0-9])|(15[^4,\\D])|(17[0-9])|(18[0-9]))\\d{8}$";
	public static final String REG_INT = "^[0-9]{1,}$";

	public static boolean isMobile(Object mobile) {
		if (mobile == null || mobile.toString().trim().equals("")) {
			return false;
		}
		if (!mobile.toString().trim().matches(REG_MOBILE)) {
			return false;
		}
		return true;
	}

	public static boolean isNullOrTooLong(Object str, int maxLength) {
		if (str == null || str.toString().trim().equals("")) {
			return true;
		}
		if (str.toString().trim().length() > maxLength) {
			return true;
		}
		return false;
	}

	public static boolean isTooLong(Object str, int maxLength) {
		if (str == null || str.toString().trim().equals("")) {
			return false;
		}
		if (str.toString().trim().length() > maxLength) {
			return true;
		}
		return false;
	}

	public static boolean isInt(Object str) {
		if (str == null || str.toString().trim().equals("")) {
			return false;
		}
		if (!str.toString().trim().matches(REG_INT)) {
			return false;
		}
		return true;
	}

	public static boolean isDouble(Object str) {
		if (str == null || str.toString().trim().equals("")) {
			return false;
		}
		try {
			Double.parseDouble(str.toString());
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean getCheckParamAttr(Map<String, Object> param, String[] params) {
		for (int i = 0; i < params.length; i++) {
			if (param.get(params[i]) == null || StringUtils.isBlank(param.get(params[i]).toString())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @desc获取一个随机字符串
	 * @param length
	 *            随机字符串的长度
	 * @return
	 */
	public static final String randomString(int length) {
		if (length < 1) {
			return null;
		}
		if (randGen == null) {
			randGen = new Random();
			//numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz" + "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
			//获取随机字符只能为数字
			numbersAndLetters = ("0123456789").toCharArray();
			// numbersAndLetters =
			// ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
		}
		char[] randBuffer = new char[length];
		for (int i = 0; i < randBuffer.length; i++) {
			randBuffer[i] = numbersAndLetters[randGen.nextInt(9)];
		}
		return new String(randBuffer);
	}

	/**
	 * @desc根据字符串数组放回该数组对用json格式的数据
	 * @param stringArray
	 * @return
	 */
	public static String returnString(String[] stringArray) {
		String str = "";
		if (null != stringArray && 0 < stringArray.length) {
			str += "[";
			for (String s : stringArray) {
				str = str + "\"" + s + "\", ";
			}
			str = str.substring(0, str.length() - 2);
			str += "]";
		}
		return str;
	}

	public static String getOperationNum(String createTime, Integer id) {
		String str = createTime.replace("-", "").replace(" ", "").replace(":", "");
		for (int i = 0; i < 10 - id.toString().length(); i++) {
			str += "0";
		}
		str += id;
		return str;
	}

	public static void main(String[] args) {
		System.out.println("15523652362".matches(REG_MOBILE));
	}
}
