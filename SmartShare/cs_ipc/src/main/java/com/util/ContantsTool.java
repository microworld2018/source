package com.util;

public class ContantsTool {

	public static final String DOMAIN_NAME = PropertiesUtil.getStringValue("domain.name");
	public static final String GPS_DISTANCE = PropertiesUtil.getStringValue("gps_distance");
	public static final String GPS_RAIDUS = PropertiesUtil.getStringValue("gps_raidus");
}
