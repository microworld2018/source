package com.util;

/**
 * @Author wangyx
 * @Version 1.0, 2015-4-21
 */
public class Constants {
	
	//app名字
	public final static String BODY = "电动侠";
	public final static String BODY_ORDER = "电动侠-订单消费";
	public final static String BODY_RECHARGE = "电动侠-会员充值";
	public final static String BODY_DEPOSIT = "电动侠-会员押金充值";
	public final static String BODY_VIOLATION = "电动侠-会员车辆违章扣款";
	public final static String BODY_ACCIDENT = "电动侠-会员车辆事故扣款";
	
	
	/** APP使用的常量 **/
	public final static int APP_RESULT_CODE_SUCCESS = 0;
	public final static int APP_RESULT_CODE_FAIL = 1;
	public final static int APP_RESULT_CODE_LOGIN = -1;

	
	public final static String SESSION_USER = "user";
	public final static String SESSION_USER_PERMISSION = "user_permission";
	public final static String SESSION_USER_MENU = "user_menu";
	
	
	public final static int RESULT_CODE_SUCCESS = 0;
	public final static int RESULT_CODE_FAILED = 1;
	
	public final static String SESSION_SUBSCRIBER = "subscriber";//前台会员登录session
	
	
	public final static String SUBSCIRBER_REGISTER_PHONE_CODE="registerPhoneCode";///注册短信验证码信息
	public final static String SUBSCIRBER_CHANGE_OLD_PHONE_CODE="oldChangePhoneCode";//修改绑定手机短信验证码
	public final static String SUBSCIRBER_CHANGE_NEW_PHONE_CODE="newChangePhoneCode";//修改绑定手机短信验证码
	public final static String SUBSCRIBER_PHONE_FIND_PWD_CODE="findPwdCode";
	public final static String SUBSCRIBER_LOGIN_CODE="loginCode";
	
	
	public final static Integer REGISTER_SMS_VALID_MINUTE= 5;//注册短信验证码有效时间 单位分 5
	public final static Integer REGISTER_SMS_VALID_RETRY_MINUTE= 2;
	
	
	public final static String 	BASE_URL="http://www.dearho.com";
	
	public final static Integer PAY_TIMEOUT_MINUTE=5;//支付超时分钟数
	public final static Integer ORDER_PAY_TIMEOUT_MINUTE=5;//支付超时分钟数
	
	
	
	public final static Integer REFUND_TRANSIT_DAY=5;//	退款中转时间
	
	/*退款 最近订单 完成时间*/
	public final static Integer REFUND_LATELY_COMPLETION_DAY=15;
	
	public final static Integer ALIPAY_REFUND_VALID_MONTH=3;//阿里 交易可走退款流程期限
	public final static Integer UNIONPAY_REFUND_VALID_MONTH=11;//银联 交易可走退款期限
	public final static Integer WXPAY_REFUND_VALID_MONTH=12;//微信交易可走退款期限
	
	public final static String RETURN_BACK_CAR_PAY_ORDERS = "RETURN_BACK_CAR_PAY_ORDERS";//还车时最后需要支付的订单
	
	
//	public final static Integer maxFrozenMoney=500;
	
	public final static String DEFAULT_PASSWORDS = "123456";
	
	public static final String APP_DES_IV = "6LA2EyQm";
	public static final String APP_DES_KEY = "HeGeA8G3";
	
	/*app返回状态码*/
	public static final Integer APP_PARAM_REQUIRED_ERROR= 198; 
	public static final String  APP_PARAM_REQUIRED_ERROR_MSG = "上传参数缺失必要参数！";
	
	public static final Integer APP_PARAM_ERROR	= 199;
	public static final String  APP_PARAM_ERROR_MSG	= "上传参数格式有误";
	
	public static final Integer APP_SUCCESS = 200;
    public static final String  APP_SUCCESS_MSG	= "成功";
    public static final Integer APP_ERROR	= 201;
    public static final String  APP_ERROR_MSG = "失败";
	
    public static final Integer APP_LOGIN_SUCCESS = 200;
    public static final String  APP_LOGIN_SUCCESS_MSG = "登录成功";
    public static final Integer APP_LOGIN_FAIL 	= 201; 
    public static final String  APP_LOGIN_FAIL_MSG 	= "服务异常";
    
    public static final Integer APP_SUBMIT_SUCCESS	= 200;
    public static final String  APP_SUBMIT_SUCCESS_MSG	= "数据提交成功，请等待审核";
    
    public static final Integer APP_LOGIN_NULL	= 202;
    public static final String  APP_LOGIN_NULL_MSG	= "手机号和密码不能为空"; 
    
    public static final Integer APP_LOGIN_MOBILE_STYLE	= 203;
    public static final String  APP_LOGIN_MOBILE_STYLE_MSG  = "手机格式错误";
    
    public static final Integer APP_LOGIN_NO_MOBILE	= 204; 
    public static final String  APP_LOGIN_NO_MOBILE_MSG	= "该手机号没有注册"; 
    
    public static final Integer APP_LOGIN_ERROR_PWD 	= 205;
    public static final String  APP_LOGIN_ERROR_PWD_MSG = "登录密码错误"; 
    
    public static final Integer APP_LOGIN_TOKEN = 206; 
    public static final String  APP_LOGIN_TOKEN_MSG = "账号已过期，请重新登录";
    
    public static final Integer APP_MOBILE_NULL = 207;
    public static final String  APP_MOBILE_NULL_MSG = "手机号为空";
    
    public static final Integer APP_MOBILE_EXIST = 208;
    public static final String  APP_MOBILE_EXIST_MSG = "手机号已注册";
    
    public static final Integer APP_MOBILE_CODE_NULL 	= 209;
    public static final String  APP_MOBILE_CODE_NULL_MSG = "手机号验证码为空";
    
    public static final Integer APP_MOBILE_CODE_ERROR 	= 210;
    public static final String  APP_MOBILE_CODE_ERROR_MSG = "手机号验证码错误";
    
    public static final Integer APP_MOBILE_CODE_NO = 211;
    public static final String  APP_MOBILE_CODE_MSG = "手机号验证码失效";	
    
    public static final Integer APP_MOBILE_OLD_ERROR = 212;
    public static final String  APP_MOBILE_OLD_ERROR_MSG = "当前用户手机号输入有误";
    
    
    public static final Integer APP_EVENT_STATE_HALF = 213;
    public static final String  APP_EVENT_STATE_HALF_MSG = "半锁  不能下单/不能操作账户";
    
	public static final Integer APP_EVENT_STATE_FUll	= 214;
	public static final String  APP_EVENT_STATE_FUll_MSG = "此账号已被锁定，如有疑问，请联系客服";
	
	public static final Integer APP_NAME_NULL = 215;
	public static final String  APP_NAME_NULL_MSG = "姓名为空";
	
	public static final Integer APP_STATE_WAIT_CONFIRMED = 216;
	public static final String  APP_STATE_WAIT_CONFIRMED_MSG = "资料待审核";
	
	public static final Integer APP_STATE_NORMAL = 217; 
	public static final String  APP_STATE_NORMAL_MSG = "资料已审核通过";
	
	public static final Integer APP_IDCARD_NULL	= 218;
	public static final String  APP_IDCARD_NULL_MSG = "身份证不能为空";
	
	public static final Integer APP_DRIVING_NULL = 219;
	public static final String  APP_DRIVING_NULL_MSG = "驾驶证不能为空";
	
	public static final Integer APP_SERVER_EXCEPTION = 220;
	public static final String  APP_SERVER_EXCEPTION_MSG = "服务器异常";
	
	public static final Integer APP_TYPE_NULL = 221; 
	public static final String  APP_TYPE_NULL_MSG = "发送短信类型为空";
	
	public static final Integer APP_STATE_NO_CONFIRMED	= 222;
	public static final String  APP_STATE_NO_CONFIRMED_MSG = "资料未审核通过,请先认证身份！";
	
	public static final Integer APP_ADVANCE_ORDER_OK = 223;
	public static final String  APP_ADVANCE_ORDER_OK_MSG = "用户当前时间内已有预约订单";
	
	public static final Integer APP_ADVANCE_ORDER_OVER	= 224;
	public static final String  APP_ADVANCE_ORDER_OVER_MSG = "当天内预约超时订单已超过3次，不可租车";
	
	public static final Integer APP_CAR_YUYUE_OK = 225;
	public static final String  APP_CAR_YUYUE_OK_MSG = "当前车辆已经被预约了";
	
	public static final Integer APP_NICK_NULL = 226;
	public static final String  APP_NICK_NULL_MSG = "参数昵称或称谓为空";
	
	public static final Integer APP_ORDERID_NULL = 227;
	public static final String  APP_ORDERID_NULL_MSG = "上传参数:orderId,订单id为空";
	
	public static final Integer APP_STATE_NULL	= 228;
	public static final String  APP_STATE_NULL_MSG = "上传参数:state,打开车门状态为空";
	
	public static final Integer APP_CARID_NULL	= 229;
	public static final String  APP_CARID_NULL_MSG = "上传参数：carId,汽车id为空";
	
	public static final Integer APP_LNG_NULL = 230;
	public static final String  APP_LNG_NULL_MSG = "上传参数:lng,经度为空";
	
	public static final Integer APP_LAT_NULL = 231;
	public static final String  APP_LAT_NULL_MSG = "上传参数:lat,纬度为空";
	
	public static final Integer APP_DOTID_NULL			= 232; 
	public static final String  APP_DOTID_NULL_MSG 		= "上传参数:dotId,网点为空！";
	
	public static final Integer APP_BACKDOTID_NULL 		= 232;
	public static final String  APP_BACKDOTID_NULL_MSG	= "上传参数:backDotId,还车网点为空！";
	
	public static final Integer APP_FLOORNO_NULL		= 233; //上传参数:floorNum,楼层为空！
	public static final Integer APP_CARNO_NULL			= 234; //上传参数:carNum,车位号为空！
	
	public static final Integer APP_ORDERPRICE_NULL		= 235;
	public static final String  APP_ORDERPRICE_NULL_MSG = "上传参数:orderPrice,订单总消费金额为空";
	
	public static final Integer APP_PAYTYPE_NULL		= 236;
	public static final String  APP_PAYTYPE_NULL_MSG	= "上传参数:payType,支付类型为空！";
	
	public static final Integer APP_BACKCARDOT_NO		= 237;
	public static final String  APP_BACKCARDOT_NO_MSG	= "还车网点与取车网点不匹配,订单不可以支付";
	
	public static final Integer APP_ORDER_NO			= 238;
	public static final String  APP_ORDER_NO_MSG 		= "订单不存在"; 
	
	public static final Integer APP_ORDER_STATE			= 239;
	public static final String  APP_ORDER_STATE_MSG		= "请联系客服检查订单状态！";
	
	public static final Integer APP_ORDER_USER_E		= 240; //用户订单匹配错误！
	public static final String  APP_ORDER_USER_E_MSG    = "用户订单匹配错误！";
	
	public static final Integer APP_ORDER_BILL_BAD		= 241; //当前金额不足100.00￥,暂不能开票!
	public static final Integer APP_BACKCARDOT_DISTANCE	= 242; //车辆当前位置与还车网之间的距离没有在误差范围内
	
	public static final Integer APP_CAR_YUYUE_NO		= 243; //当前车辆已经下线了
	public static final String  APP_CAR_YUYUE_NO_MSG    ="当前车辆已经下线了！";
	
	public static final Integer APP_SMS_TIMER_OUT 		= 244; //120秒内只能获取一次验证码，请稍后重试
	public static final String  APP_SMS_TIMER_OUT_MSG = "120秒内只能获取一次验证码，请稍后重试！";
	
	
	public static final Integer APP_NOT_REGISTERED    	= 245;
	public static final String  APP_NOT_REGISTERED_MSG  = "用户未注册！";
	
	public static final Integer APP_SENT_FAILED			= 246; //发送失败
	public static final String  APP_SENT_FAILED_MSG = "发送失败！";
	
	public static final Integer APP_NO_BINDING_DEVICE   = 247;
	public static final String  APP_NO_BINDING_DEVICE_MSG = "未绑定设备！";
	
	public static final Integer CAR_OPEN_DOOR_ERROR 	= 248;
	public static final String  CAR_OPEN_DOOR_ERROR_MSG = "打开车门失败！";
	
	public static final Integer CAR_CLOSE_DOOR_ERROR 	 = 249;
	public static final String  CAR_CLOSE_DOOR_ERROR_MSG = "关闭车门失败！";
	
	public static final Integer CAR_STATE_WHISTLE_ERROR  = 250;
	public static final String  CAR_STATE_WHISTLE_ERROR_MSG = "鸣笛失败！";
	
	public static final Integer CAR_CLOSE_DOOR_SUCCESS  = 251;
	public static final String  CAR_CLOSE_DOOR_SUCCESS_MSG = "锁车门成功！";
	
	public static final Integer CAR_OPEN_DOOR_SUCCESS 	= 252;
	public static final String  CAR_OPEN_DOOR_SUCCESS_MSG = "开车门成功！";
	
	public static final Integer CAR_STATE_WHISTLE_SUCCESS = 253;
	public static final String  CAR_STATE_WHISTLE_SUCCESS_MSG = "鸣笛成功！";
	
	public static final Integer CAR_BRANCH_DOT_FULL 	= 254;
	public static final String  CAR_BRANCH_DOT_FULL_MSG = "网点车位已满！";
	
	public static final Integer CAR_NOT_LINE 			= 255;
	public static final String  CAR_NOT_LINE_MSG		= "设备不在线！";
	
	public static final Integer CAR_NOT_SHUT_DOWN		= 1025;
	public static final String  CAR_NOT_SHUT_DOWN_MSG	= "发动机尚未关闭,操作失败！";
	
	public static final Integer EXIT_SUCCESS 			= 256;
	public static final String  EXIT_SUCCESS_MSG 		= "退出成功！";
	
	public static final Integer EXIT_ERROR				= 257;
	public static final String  EXIT_ERROR_MSG			= "退出失败！";
	
	public static final String CAR_CLOSE_MSG           ="车门未关,请关车门!";
	public static final Integer CAR_CLOSE				= 1027;
	
	public static final Integer APP_NO_CONTENT			= 258;
	public static final String  APP_NO_CONTENT_MSG		= "暂无消息！";
	
	public static final Integer APP_ORDER_NO_PAY		= 259;
	public static final String  APP_ORDER_NO_PAY_MSG	= "您有未支付的订单信息！";
	
	public static final Integer APP_BACKCAR_STATE_NO	= 260;
	public static final String  APP_BACKCAR_STATE_NO_MSG = "不能还车！";
	
	public static final Integer APP_BACKCAR_STATE_YES	= 261;
	public static final String  APP_BACKCAR_STATE_YES_MSG = "还车成功！";
	
	
	public static final Integer APP_PAY_STATE_YES	= 262;
	public static final String  APP_PAY_STATE_YES_MSG = "支付成功！";
	
	public static final Integer APP_PAY_STATE_NO	= 263;
	public static final String  APP_PAY_STATE_NO_MSG= "支付失败！";
	
	
	public static final Integer APP_NO_AVAILABLE_DOT	 = 264;
	public static final String  APP_NO_AVAILABLE_DOT_MSG = "附近没有可用网点！";
	
	public static final Integer APP_REQUEST_TIME = 265;
	public static final String  APP_REQUEST_TIME_MSG = "请求超时！";
	
	public static final Integer APP_COUPON_LOGIN_NULL = 266;
	public static final String  APP_COUPON_LOGIN_NULL_MSG = "暂无优惠卷！";   
	
	public static final Integer APP_CURRENT_PAGE_NULL = 267;
	public static final String  APP_CURRENT_PAGE_NULL_MSG = "上传参数:currentPage,当前页为空！";
	
	public static final Integer APP_COUPON_ID_NULL = 268;
	public static final String  APP_COUPON_ID_NULL_MSG = "上传参数:couponId,优惠卷ID为空！";
    
	public static final Integer APP_NO_BACK_CAR = 269;//未还车
	public static final String  APP_NO_BACK_CAR_MSG = "未还车！";
	
	public static final Integer APP_ORDER_PAY = 270;//订单已支付
	public static final String  APP_ORDER_PAY_MSG = "订单已支付！";
	
	public static final Integer APP_RECHARGE_AMOUNT_NULL = 271;
	public static final String  APP_RECHARGE_AMOUNT_NULL_MSG = "充值金额不能为空！";
	
	public static final Integer APP_RECHARGE_AMOUNT_NO_ZERO = 272;
	public static final String  APP_RECHARGE_AMOUNT_NO_ZERO_MSG = "充值金额不能为0！";
	
	public static final Integer APP_ACCIDENT_ID_NULL = 273;
	public static final String  APP_ACCIDENT_ID_NULL_MSG = "上传参数:accidentId,违章事故ID为空！";
	
	public static final Integer APP_BOOKING_ORDER_EXIST = 274;
	public static final String  APP_BOOKING_ORDER_EXIST_MSG = "您有未完成订单，暂不能退押金！";
	
	public static final Integer APP_DEPOSIT_NULL = 275;
	public static final String  APP_DEPOSIT_NULL_MSG = "押金不足，请充值！";
	
	public static final Integer APP_DEPOSIT_EXIT = 276;
	public static final String  APP_DEPOSIT_EXIT_MSG = "押金已经提交退款！";
	
	public static final Integer APP_STATE_NO_UNAUTHORIZED	= 278;
	public static final String  APP_STATE_NO_UNAUTHORIZED_MSG = "资料未提交！";
	
	public static final Integer APP_STATE_NO_AUDIT	= 279;
	public static final String  APP_STATE_NO_AUDIT_MSG = "资料待审核！";
	
	public static final Integer APP_HANDHELDIDCARD_NULL = 280;
	public static final String  APP_HANDHELDIDCARD_NULL_MSG = "身份认证升级，请更新到新版本后继续用车。";
	
	public static final Integer APP_IDCARD_CERTIFICATION = 281;
	public static final String  APP_IDCARD_CERTIFICATION_MSG = "您的身份证已认证其他手机，如有问题请联系客服！400-888-1212";
	
	public static final Integer APP_NOTIN_ACTIVITIES = 281;
    public static final String  APP_NOTIN_ACTIVITIES_MSG = "非活动时间！";
    
    public static final Integer APP_HAS_SAMECOUPON = 282;
    public static final String  APP_HAS_SAMECOUPON_MSG = "您已获取过该优惠券！";
    
    public static final Integer APP_NULL_APPSTART = 283;
    public static final String  APP_NULL_APPSTART_MSG = "没有正在使用的开屏！";
	
	public static final Integer SYS_CONTENT_NULL = 1000;
	public static final String  SYS_CONTENT_NULL_MSG = "上传参数:content,短信发送内容为空！";
	
	public static final Integer SYS_MOBILE_NULL = 1001;
	public static final String  SYS_MOBILE_NULL_MSG = "上传参数:mobile,短信发送手机号为空！";
	
	public static final Integer SYS_SUB_ID_NULL = 1002;
	public static final String  SYS_SUB_ID_NULL_MSG = "上传参数:subId,短信发送人subId为空！";
	
	public static final Integer SYS_NAME_NULL = 1003;
	public static final String  SYS_NAME_NULL_MSG = "上传参数:name,短信发送人姓名为空！";
	
	public static final Integer SYS_VERSION_OLD = 1004;
	public static final String  SYS_VERSION_OLD_MSG = "当前版本过低，请升级新版后继续用车！";
	
	public static final Integer TOTAL_FEE_ERROR = 1005;
	public static final String  TOTAL_FEE_ERROR_MSG = "查询成功但金额错误！";
	
	public static final Integer QUERY_WX_FAILD = 1006;
	public static final String  QUERY_WX_FAILD_MSG = "查询微信接口失败！";
	
	public static final Integer ORDER_NOT_EXIST = 1007;
	public static final String  ORDER_NOT_EXIST_MSG = "根据订单号未查询到订单信息！";
	
}
