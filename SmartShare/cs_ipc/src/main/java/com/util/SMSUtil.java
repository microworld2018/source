/**
 * Copyrigh  (c) dearho Team
 * All rights reserved.
 *
 *This file SMSUtil.java creation date:[2015-5-25 下午03:16:28] by liusong
 *http://www.dearho.com
 */
package com.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.Random;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;


/**
 * @Author liusong
 * @Description 
 * @Version 1.0,2015-5-25
 *
 */
public class SMSUtil {
	private static final String apikey="d61c5a73de13f97279ef71127dd55049";
	private static final String username="bjfs";
	private static final String password="ml150513";

	public  static final String RESULT_SUCCESS="success";
	public  static final String RESULT_ERROR="error";
	
	public static final Log log = LogFactory.getLog(SMSUtil.class);
	

	public static void sendSMS(String phoneNo,String content,Integer type){
		if(StringUtils.isEmpty(phoneNo)){
			new Exception("下发短信手机号不能为空!");
		}
		if(StringUtils.isEmpty(content)){
			new Exception("下发短信短信内容不能为空!");
		}
		if(content.length()>500){
			new Exception("下发短信短信内容过长!");
		}
		try {
			// 创建StringBuffer对象用来操作字符串
			StringBuffer sb = new StringBuffer("http://m.5c.com.cn/api/send/?");
			// APIKEY
			sb.append("apikey="+apikey);
			//用户名
			sb.append("&username="+username);
			// 向StringBuffer追加密码
			sb.append("&password="+password);

			// 向StringBuffer追加手机号码
			sb.append("&mobile="+phoneNo);

			// 向StringBuffer追加消息内容转URL标准码
			sb.append("&content="+URLEncoder.encode(content,"GBK"));
			// 创建url对象
			URL url = new URL(sb.toString());
			// 打开url连接
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			// 设置url请求方式 ‘get’ 或者 ‘post’
			connection.setRequestMethod("POST");
			// 发送
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			// 返回发送结果
			String inputline = in.readLine();

			System.out.println(inputline);
		} catch (Exception e) {
			 
		}
		
	}
	
	
	public static void main(String[] args) throws Exception {
		
		String phoneNo = "18810289689";
		String content = " 电老虎租车用户修改手机号验证码:6666，此验证码5分钟内有效，如非本人操作请忽略。";
		
				// 创建StringBuffer对象用来操作字符串
				StringBuffer sb = new StringBuffer("http://m.5c.com.cn/api/send/?");
				// APIKEY
				sb.append("apikey="+apikey);
				//用户名
				sb.append("&username="+username);
				// 向StringBuffer追加密码
				sb.append("&password="+password);

				// 向StringBuffer追加手机号码
				sb.append("&mobile="+phoneNo);

				// 向StringBuffer追加消息内容转URL标准码
				sb.append("&content="+URLEncoder.encode(content,"GBK"));
				// 创建url对象
				URL url = new URL(sb.toString());
				// 打开url连接
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				// 设置url请求方式 ‘get’ 或者 ‘post’
				connection.setRequestMethod("POST");
				// 发送
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
				// 返回发送结果
				String inputline = in.readLine();

				System.out.println(inputline);
	}
}
