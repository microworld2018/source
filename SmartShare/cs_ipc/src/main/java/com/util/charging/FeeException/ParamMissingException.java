package com.util.charging.FeeException;

/**
 * 缺少参数异常
 * @author zhangsg
 *
 */
public class ParamMissingException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public ParamMissingException(String msg){
		super(msg);
	}

}
