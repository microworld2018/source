package com.util.charging.calculate;

import java.util.HashMap;
import java.util.Map;

import com.util.charging.Constant.CouponType;
import com.util.charging.FeeException.NoSuchCouponTypeException;

/**
 * 结合优惠券相关信息计算费用
 * 
 * @author zhangsg
 * @date 2017年7月24日下午5:14:01
 */
public class FeesAlgWithCoupon {

	/**
	 * 根据优惠券和账户余额计算第三方应付金额
	 */
	public static Map<String, Integer> calculateFeesWithCoupon(int totalTimePrice, int totalMilePrice, int totalInsurancePrice,
												  CouponType couponType, int couponFee, boolean applyInsurance, 
												  int amount, boolean debug){
		if(debug){
			System.out.println("传入参数：----> couponType:" + couponType + ", applyInsurance:" + applyInsurance 
								+ ", amount:" + amount + ", debug:" + debug);
		}
		Map<String, Integer> map = new HashMap<>();
		int accountPayment = 0;
		int cashPayment = 0;
		int actualFee = 0;
		int totalFee = totalTimePrice + totalMilePrice + totalInsurancePrice;
		switch (couponType) {
		case COUPON_NORMAL:
			if(applyInsurance){
				actualFee = totalFee - couponFee;
				actualFee = actualFee < 0 ? 0 : actualFee;
				if(amount < actualFee){
					accountPayment = amount;
					cashPayment = actualFee - amount;
				}else{
					accountPayment = actualFee;
				}
			}else{
				int feeNoInsurance = totalTimePrice + totalMilePrice - couponFee;//不包含保险费
				feeNoInsurance = feeNoInsurance < 0 ? 0 : feeNoInsurance;
				actualFee = feeNoInsurance + totalInsurancePrice;
				if(amount < actualFee){
					accountPayment = amount;
					cashPayment = actualFee - amount;
				}else{
					accountPayment = actualFee;
				}
			}
			break;
		case COUPON_TIME:
			if(applyInsurance){
				int feeNoMile = totalTimePrice + totalInsurancePrice - couponFee;//不包含里程费
				feeNoMile = feeNoMile < 0 ? 0 : feeNoMile;
				actualFee = totalMilePrice + feeNoMile;
				if(amount < actualFee){
					accountPayment = amount;
					cashPayment = actualFee - amount;
				}else{
					accountPayment = actualFee;
				}
			}else{
				int feeNoMileAndInsurance = totalTimePrice - couponFee;//不包含里程费和保险费
				feeNoMileAndInsurance = feeNoMileAndInsurance < 0 ? 0 : feeNoMileAndInsurance;
				actualFee = feeNoMileAndInsurance + totalMilePrice + totalInsurancePrice;
				if(amount < actualFee){
					accountPayment = amount;
					cashPayment = actualFee - amount;
				}else{
					accountPayment = actualFee;
				}
			}
			break;
		case COUPON_MILE:
			if(applyInsurance){
				int feeNoTime = totalMilePrice + totalInsurancePrice - couponFee;//不包含时长费
				feeNoTime = feeNoTime < 0 ? 0 : feeNoTime;
				actualFee = totalTimePrice + feeNoTime;
				if(amount < actualFee){
					accountPayment = amount;
					cashPayment = actualFee - amount;
				}else{
					accountPayment = actualFee;
				}
			}else{
				int feeNoTimeAndInsurance = totalMilePrice - couponFee;//不包含时长费和保险费
				feeNoTimeAndInsurance = feeNoTimeAndInsurance < 0 ? 0 : feeNoTimeAndInsurance;
				actualFee = feeNoTimeAndInsurance + totalTimePrice + totalInsurancePrice;
				if(amount < actualFee){
					accountPayment = amount;
					cashPayment = actualFee - amount;
				}else{
					accountPayment = actualFee;
				}
			}
			break;
		default:
			throw new NoSuchCouponTypeException("无此优惠券类型！");
		}
		map.put("actualFee", actualFee);
		map.put("cashPayment", cashPayment);
		return map;
	}
}
