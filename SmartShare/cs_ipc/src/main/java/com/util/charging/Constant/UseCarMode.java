package com.util.charging.Constant;

/**
 * 用车模式
 * 
 * @author zhangsg
 */
public enum UseCarMode {

	NORMAL_MODE("基础模式", 1), 
	PACKAGE_MODE("套餐模式", 2);

	private UseCarMode(String name, int value) {
		this.name = name;
		this.value = value;
	}

	private String name;
	private int value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString(){
		return name;
	}
}
