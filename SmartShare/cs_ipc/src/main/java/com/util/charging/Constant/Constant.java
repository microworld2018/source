package com.util.charging.Constant;

/**
 * 一些常量
 * @author zhangsg
 * @date 2017年7月20日 上午9:47:12
 */
public class Constant {

	/**
	 * 结束时间比开始时间小
	 */
	public static final Integer DATE_ERROR = -999;
}
