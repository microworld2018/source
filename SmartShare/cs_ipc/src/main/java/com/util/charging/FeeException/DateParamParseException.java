package com.util.charging.FeeException;

/**
 * 日期参数转换异常
 * @author zhangsg
 * @date 2017年7月25日上午9:37:33
 */
public class DateParamParseException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DateParamParseException(String msg){
		super(msg);
	}
}
