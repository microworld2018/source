package com.util.charging.util;

/**
 * 里程工具类
 * @author zhangsg
 * @date 2017年7月24日下午5:51:35
 */
public class MileUtils {

	/**
	 * 不足一公里按一公里计
	 */
	public static int calculateMile(double mileage){
		int allMile;
		//不足一公里按一公里计
		if(mileage % 1.0 != 0){
			allMile = (int) (mileage / 1) + 1;
		}else{
			allMile = (int) mileage;
		}
		return allMile;
	}
	
	
}
