package com.util.charging.FeeException;

/**
 * 无此优惠券类型异常
 * @author zhangsg
 * @date 2017年7月25日上午9:36:41
 */
public class NoSuchCouponTypeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoSuchCouponTypeException(String msg) {
		super(msg);
	}
}
