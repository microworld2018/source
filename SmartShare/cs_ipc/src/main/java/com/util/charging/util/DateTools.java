package com.util.charging.util;

import java.util.Date;

import com.util.charging.Constant.Constant;
import com.util.charging.FeeException.EndTimeEarlierThanStartTimeException;

/**
 * 时间工具类
 * @author zhangsg
 *
 */
public class DateTools {

	/**
	 * 根据两个时间计算间隔的分钟数（不足一分钟按一分钟计）
	 * @param startTime 
	 * @param endTime
	 * @return -999:结束时间小于开始时间
	 */
	public static int getMinutesFromDateInterval(Date startTime, Date endTime){
		long second = getSecondsFromDateInterval(startTime, endTime);
		if(second == Constant.DATE_ERROR){
			throw new EndTimeEarlierThanStartTimeException("结束时间早于开始时间，请检查参数！");
		}
	    if(second % 60 == 0){
	    	return (int)second / 60;
	    }else{
	    	return (int) (second / 60 + 1);
	    }
	}
	
	/**
	 * 计算两个时间间隔的天数，不足一天按一天计
	 * @param startTime
	 * @param endTime
	 * @return -999:结束时间小于开始时间
	 */
	public static int getDaysFromDateInterval(Date startTime, Date endTime){
		long second = getSecondsFromDateInterval(startTime, endTime);
		if(second == Constant.DATE_ERROR){
			throw new EndTimeEarlierThanStartTimeException("结束时间早于开始时间，请检查参数！");
		}
		if(second % 3600 == 0){
			return (int) (second / 3600 / 24);
		}else{
			return (int) (second / 3600 / 24 + 1);
		}
	}
	
	/**
	 * 返回两个日期间隔多少秒
	 * @param startTime
	 * @param endTime
	 * @return Constant.DATE_ERROR:结束时间小于开始时间
	 */
	public static long getSecondsFromDateInterval(Date startTime, Date endTime){
		long diff = endTime.getTime() - startTime.getTime();
		if(diff < 0){
			//如果结束时间小于开始时间
			return Constant.DATE_ERROR;
		}
		return diff / 1000;
	}
	
}










