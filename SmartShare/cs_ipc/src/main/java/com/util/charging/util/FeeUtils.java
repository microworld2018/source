package com.util.charging.util;

/**
 * 关于钱的一些转换
 * 
 * @author zhangsg
 * @date 2017年7月25日上午11:45:07
 */
public class FeeUtils {

	/**
	 * float类型的元转为int类型的分
	 */
	public static int yuanToFen(float yuan) {
		int i = (int) ((yuan + 0.005f) * 100);
		return i;
	}

	/**
	 * int类型的元，转为float类型的分
	 */
	public static float fenToYuan(int fen) {
		float fenL = fen;
		return fenL / 100;
	}
	
	public static void main(String[]args) {
//		BigDecimal b1 = new BigDecimal(Double.toString(0.019));
//		BigDecimal b2 = new BigDecimal(Double.toString(100));
//		int i = b1.multiply(b2).intValue();
//		System.out.println(i);
//		System.out.println(yuanToFen(8.4f));
//		System.out.println(yuanToFen(8.39f));
//		System.out.println(yuanToFen(9.4f));
//		System.out.println(yuanToFen(75.34f));
//		System.out.println(yuanToFen(0.01f));
	}
}









