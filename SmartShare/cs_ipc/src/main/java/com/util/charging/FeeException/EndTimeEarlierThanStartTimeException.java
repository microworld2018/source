package com.util.charging.FeeException;

/**
 * 结束时间早于开始时间异常
 * @author zhangsg
 * @date 2017年7月20日 上午10:29:47
 */
public class EndTimeEarlierThanStartTimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public EndTimeEarlierThanStartTimeException(String msg){
		super(msg);
	}
	
}
