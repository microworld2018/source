package com.util.charging.calculate;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.util.charging.Constant.CouponType;
import com.util.charging.Constant.UseCarMode;
import com.util.charging.FeeException.ParamMissingException;
import com.util.charging.FeeException.NoSuchCouponTypeException;
import com.util.charging.FeeException.NoSuchUseCarModeException;
import com.util.charging.util.DateTools;
import com.util.charging.util.MileUtils;

/**
 * 统一计费
 * @author zhangsg
 * @date 2017年7月19日 下午3:14:17
 */
public class FeesAlg {

	/**
	 * 计算用户应付金额、账户支付金额、现金支付金额
	 */
	public static Map<String, Integer> calculateFees(UseCarMode useCarMode, Date startTime, Date endTime, 
													 double mileage, int insuranceFee, int couponFee, 
													 CouponType couponType, boolean applyInsurance, 
													 int minutePrice, int milePrice, int amount, boolean debug){
		if(debug){
			System.out.println("传入参数----> useCarMode:" + useCarMode + ", startTime:" + startTime + ", endTime:" + endTime 
								+ ", mileage:" + mileage + ", insuranceFee:" + insuranceFee + ", couponFee:" + couponFee 
								+ ", couponType:" + couponType + ", applyInsurance:" + applyInsurance 
								+ ", minutePrice:" + minutePrice + ", milePrice:" + milePrice + ", amount:" + amount 
								+ ", debug:" + debug);
		}
		int totalPayment = 0;
		int accountPayment = 0;
		int cashPayment = 0;
		int actualFee = 0;
		int totalTimePrice = 0;
		int totalMilePrice = 0;
		int allMinute = DateTools.getMinutesFromDateInterval(startTime, endTime);
		int allMile = MileUtils.calculateMile(mileage);
		
		Map<String, Integer> map = new HashMap<>();
		
		switch (useCarMode) {
		case NORMAL_MODE:
			totalTimePrice = timePrice(startTime, endTime, minutePrice, debug);
			totalMilePrice = milePrice(mileage, milePrice, debug);
			int totalInsurancePrice = insurancePrice(startTime, endTime, insuranceFee, debug);
			
			totalPayment = totalTimePrice + totalMilePrice + totalInsurancePrice;
			
			switch (couponType) {
			case COUPON_NORMAL:
				if(applyInsurance){					
					actualFee = totalPayment - couponFee;
					actualFee = actualFee < 0 ? 0 : actualFee;
					if(amount < actualFee){
						accountPayment = amount;
						cashPayment = actualFee - amount;
					}else{
						accountPayment = actualFee;
					}
				}else{
					int feeNoInsurance = totalTimePrice + totalMilePrice - couponFee;//不包含保险费
					feeNoInsurance = feeNoInsurance < 0 ? 0 : feeNoInsurance;
					actualFee = feeNoInsurance + totalInsurancePrice;
					if(amount < actualFee){
						accountPayment = amount;
						cashPayment = actualFee - amount;
					}else{
						accountPayment = actualFee;
					}
				}
				break;
			case COUPON_TIME:
				if(applyInsurance){
					int feeNoMile = totalTimePrice + totalInsurancePrice - couponFee;//不包含里程费
					feeNoMile = feeNoMile < 0 ? 0 : feeNoMile;
					actualFee = totalMilePrice + feeNoMile;
					if(amount < actualFee){
						accountPayment = amount;
						cashPayment = actualFee - amount;
					}else{
						accountPayment = actualFee;
					}
				}else{
					int feeNoMileAndInsurance = totalTimePrice - couponFee;//不包含里程费和保险费
					feeNoMileAndInsurance = feeNoMileAndInsurance < 0 ? 0 : feeNoMileAndInsurance;
					actualFee = feeNoMileAndInsurance + totalMilePrice + totalInsurancePrice;
					if(amount < actualFee){
						accountPayment = amount;
						cashPayment = actualFee - amount;
					}else{
						accountPayment = actualFee;
					}
				}
				break;
			case COUPON_MILE:
				if(applyInsurance){
					int feeNoTime = totalMilePrice + totalInsurancePrice - couponFee;//不包含时长费
					feeNoTime = feeNoTime < 0 ? 0 : feeNoTime;
					actualFee = totalTimePrice + feeNoTime;
					if(amount < actualFee){
						accountPayment = amount;
						cashPayment = actualFee - amount;
					}else{
						accountPayment = actualFee;
					}
				}else{
					int feeNoTimeAndInsurance = totalMilePrice - couponFee;//不包含时长费和保险费
					feeNoTimeAndInsurance = feeNoTimeAndInsurance < 0 ? 0 : feeNoTimeAndInsurance;
					actualFee = feeNoTimeAndInsurance + totalTimePrice + totalInsurancePrice;
					if(amount < actualFee){
						accountPayment = amount;
						cashPayment = actualFee - amount;
					}else{
						accountPayment = actualFee;
					}
				}
				break;
			default:
				throw new NoSuchCouponTypeException("没有此优惠券类型！");
			}
			
			map.put("totalPayment", totalPayment);
			map.put("accountPayment", accountPayment);
			map.put("cashPayment", cashPayment);
			map.put("totalTimePrice", totalTimePrice);
			map.put("totalMilePrice", totalMilePrice);
			map.put("actualFee", actualFee);
			map.put("allMinute", allMinute);
			map.put("allMile", allMile);
			break;

		default:
			throw new NoSuchUseCarModeException("无此用车模式！");
		}
		return map;
	}
	
	/**
	 * 计算用车时长费用
	 * @return
	 */
	private static int timePrice(Date startTime, Date endTime, int minutePrice, boolean debug){
		if(startTime == null || endTime == null){
			throw new ParamMissingException("缺少日期参数，请检查！");
		}
		int timePrice = 0;

		//根据开始时间和结束时间计算用车时间,不足一分钟按一分钟计
		int allMinute = DateTools.getMinutesFromDateInterval(startTime, endTime);
		
		//每分钟收费，与用车时间相乘，得出用车时长费用
		timePrice = allMinute * minutePrice;
		if(debug){
			System.out.println("时长：" + allMinute + "分钟，时长费用：" + timePrice + "分");
		}
		return timePrice;
	}
	
	/**
	 * 计算用车里程费用
	 */
	private static int milePrice(double mileage, int milePrice, boolean debug){
		int allMilePrice = 0;
		
		//不足一公里按一公里计
		int allMile = MileUtils.calculateMile(mileage);
		
		//每公里收费，与用车里程相乘，得出用车里程费用
		allMilePrice = allMile * milePrice;
		
		if(debug){
			System.out.println("里程：" + allMile + "公里，里程费用：" + allMilePrice + "分");
		}
		return allMilePrice;
	}
	
	/**
	 * 计算保险费用
	 */
	public static int insurancePrice(Date startTime, Date endTime, int insuranceFee, boolean debug){
		if(startTime == null || endTime == null){
			throw new ParamMissingException("缺少日期参数，请检查！");
		}
		int insFee = 0;
		
		//计算用车天数，不足一天按一天计
		int days = DateTools.getDaysFromDateInterval(startTime, endTime);
		
		//计算保险总金额，保险金额乘以天数
		insFee = insuranceFee * days;
		
		if(debug){
			System.out.println("共用车" + days + "天，保险费用：" + insFee + "分");
		}
		return insFee;
	}
	
}



















