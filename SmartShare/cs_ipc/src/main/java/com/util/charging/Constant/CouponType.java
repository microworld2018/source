package com.util.charging.Constant;

/**
 * 优惠券类型
 * @author zhangsg
 * @date 2017年7月25日上午10:35:53
 */
public enum CouponType {

	COUPON_NORMAL("普通优惠券", 1), 
	COUPON_TIME("时长优惠券", 2), 
	COUPON_MILE("里程优惠券", 3),
	COUPON_INSURANCE("保险优惠券", 4);

	private String name;
	private int value;

	private CouponType(String name, int value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString(){
		return name;
	}
}
