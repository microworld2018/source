package com.util.charging.FeeException;

/**
 * 无此用车模式异常
 * @author zhangsg
 * @date 2017年7月20日 上午10:34:07
 */
public class NoSuchUseCarModeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NoSuchUseCarModeException(String msg){
		super(msg);
	}
}
