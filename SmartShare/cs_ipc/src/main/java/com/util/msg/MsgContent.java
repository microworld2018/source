package com.util.msg;

import org.springframework.util.StringUtils;

/**
 * 短信发送内容
 * @author GodZilla
 *
 */
public class MsgContent {
	
	//短信内容替换通配符@
	public static final String USER_MSG_REPLACE_CODE_1 = "@";
	//短信内容替换通配符#
	public static final String USER_MSG_REPLACE_CODE_2 = "#";
	//短信内容替换通配符$
	public static final String USER_MSG_REPLACE_CODE_3 = "$";
	//短信内容替换通配符*
	public static final String USER_MSG_REPLACE_CODE_4 = "*";
	//短信内容替换通配符&
	public static final String USER_MSG_REPLACE_CODE_5 = "&";
	
	
	//用户注册验证码-短信提示消息通知
	public static final String USER_REGISTRATION_VERIFICATION_CODE_MSG = "【电动侠租车】用户注册验证码:@，此验证码5分钟内有效，如非本人操作请忽略。";
	//用户修改手机号验证码-短信提示消息通知
	public static final String USER_UPDATE_PHONE_CODE_MSG = "【电动侠租车】用户修改手机号验证码:@，此验证码5分钟内有效，如非本人操作请忽略。";
	//用户登录验证码-短信提示消息通知
	public static final String USER_LOGIN_COCE_MSG = "【电动侠租车】用户登录验证码:@，此验证码5分钟内有效，如非本人操作请忽略。";
	//证件已提交审核-短信提示消息通知
	public static final String USER_SUBMIT_CARD_MSG = "【电动侠租车】您的证件已提交审核，请耐心等待。通过后，您将可以使用车辆！";
	//订单取消次数-短信提示消息通知
	public static final String ORDER_CANCEL_COUNT_MSG = "【电动侠租车】亲，您的订单已超时取消，每天最多3次哦！";
	//订单取消-短信提示消息通知
	public static final String ORDER_CANCLE_MSG = "尊敬的@，您的订单#已取消。【电动侠租车】";
	//订单下单成功-短信提示消息通知
	public static final String ORDER_CREATE_SUCCESS_MSG = "【电动侠租车】您已下单成功，请于@分钟内赶往网点取车，超时后订单自动关闭。";
	//资料审核发送客服人员-短信提示消息通知
	public static final String SEND_SERVICE_MSG = "【电动侠租车】用户:@已提交资料审核，手机号:#，请及时审核资料！";
	//用户开始用车-短信提示消息通知
	public static final String USER_USE_CAR_MSG = "【电动侠租车】用户:@开始用车，手机号:#，车牌号:$。用车时间:*,取车地点:&.";
	//用户开始还车-短信提示消息通知
	public static final String USER_BACK_CAR_MSG = "【电动侠租车】用户:@开始还车，手机号:#，车牌号:$。还车时间:*,还车地点:&.";
	
	/**
	 * 获取短信发送消息内容
	 * @param content
	 * @return
	 */
	public static String getSmsMsg(String content){
		
		return content;
	}
	
	/**
	 * 获取短信发送消息内容
	 * @param msg 发送内容
	 * @param data 替换参数
	 * @return
	 */
	public static String getSmsMsg(String msg,String data){
		
		String content = "";
		
		if(!StringUtils.isEmpty(msg)){
			
			content = msg.replace(USER_MSG_REPLACE_CODE_1, data);
		}
		
		return content;
	}
	
	/**
	 * 获取短信发送消息内容 
	 * @param msg -发送内容
	 * @param data1 -替换参数1
	 * @param data2 -替换参数2
	 * @return
	 */
	public static String getSmsMsg(String msg,String data1,String data2){
		
		String content = "";
		
		if(!StringUtils.isEmpty(msg)){
			
			content = msg.replace(USER_MSG_REPLACE_CODE_1, data1).replace(USER_MSG_REPLACE_CODE_2, data2);
		}
		
		return content;
	}
	
	/**
	 * 获取短信发送内容  
	 * @param msg -发送内容
	 * @param data1 -替换参数1
	 * @param data2 -替换参数2
	 * @param data3 -替换参数3
	 * @param data4 -替换参数4
	 * @param data5 -替换参数5
	 * @return
	 */
	public static String getSmsMsg(String msg,String data1,String data2,String data3,String data4,String data5){
		String content = "";
		
		if(!StringUtils.isEmpty(msg)){
			
			content = msg.replace(USER_MSG_REPLACE_CODE_1, data1);
			content = content.replace(USER_MSG_REPLACE_CODE_2, data2);
			content = content.replace(USER_MSG_REPLACE_CODE_3, data3);
			content = content.replace(USER_MSG_REPLACE_CODE_4, data4);
			content = content.replace(USER_MSG_REPLACE_CODE_5, data5);
		}
		
		return content;
	}
	
	
}
