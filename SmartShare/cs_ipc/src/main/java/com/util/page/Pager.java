package com.util.page;

import java.util.List;

public class Pager {
	private int curPage = 1; //当前页
	private int pageSize = 10;//每页多少行
	private int totalRow;//共多少行
	private int start;// 当前起始行
	private int end;//结束行
	private int totalPage;//共多少页
	
	public int getCurPage() {
		return curPage;
	}
	public void setCurPage(int curPage) {
		this.curPage = curPage;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getTotalRow() {
		return totalRow;
	}
	//计算共多少行
	public void setTotalRow(int totalRow) {
		
       if(curPage < 1){
			
			curPage = 1;
			
		}else{
			
			start = pageSize * (curPage - 1);
		
		}
		
		totalPage = (totalRow + pageSize-1) / pageSize; //总页数  = （总行数  + 一页显示多少个 - 1）*显示个数
	
		this.totalRow = totalRow;
		
		if(totalPage > curPage){
			
			start = pageSize *(curPage - 1);
			end = totalRow;
		}
		
		end = start + pageSize > totalRow ? totalRow : start+pageSize;
		
	}
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getEnd() {
		return end;
	}
	public void setEnd(int end) {
		this.end = end;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	/**  
     * 对list集合进行分页处理  
	 * @param <E>
     * @return  
     */  
    public <E> List<E> listSplit(List<E> list) {  
        return list.subList(this.start, this.end);  
    }  
    
}
