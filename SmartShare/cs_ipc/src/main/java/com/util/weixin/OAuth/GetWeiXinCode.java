package com.util.weixin.OAuth;

import java.util.HashMap;
import java.util.Map;

import com.util.weixin.Constant;
import com.util.weixin.tool.HttpsRequest;
import com.util.weixin.tool.URLEncoderUTF8;

import net.sf.json.JSONObject;


public class GetWeiXinCode {
	/**
	 * Using:用户同意授权，获取code
	 * 
	 */
	public static String getCodeRequest(String redirect_url, String scope) {
		String GetCodeRequest = Constant.GETCODEREQUEST;
		GetCodeRequest = GetCodeRequest.replace("APPID", URLEncoderUTF8
				.urlEnodeUTF8(Constant.APPID));
		GetCodeRequest = GetCodeRequest.replace("REDIRECT_URI", URLEncoderUTF8
				.urlEnodeUTF8(redirect_url));
		GetCodeRequest = GetCodeRequest.replace("SCOPE", scope);
		return GetCodeRequest;
	}

	/**
	 * Using: 通过code换取网页授权access_token
	 * 
	 */
	public static Map<String, Object> GetAccess_tokenRequest(String code,
			String scope) throws Exception {
		String GetAccess_tokenRequest = Constant.GETACCESS_TOKENREQUEST;
		GetAccess_tokenRequest = GetAccess_tokenRequest.replace("APPID",
				URLEncoderUTF8.urlEnodeUTF8(Constant.APPID));
		GetAccess_tokenRequest = GetAccess_tokenRequest.replace("SECRET",
				URLEncoderUTF8.urlEnodeUTF8(Constant.SECRET));
		GetAccess_tokenRequest = GetAccess_tokenRequest.replace("CODE",
				URLEncoderUTF8.urlEnodeUTF8(code));
		String jsonaccess = HttpsRequest.httpRequest(GetAccess_tokenRequest,
				"POST", null);
		JSONObject json = JSONObject.fromObject(jsonaccess);

		Map<String, Object> map = new HashMap<String, Object>();
		if (scope.equals(Constant.SCOPE)) {
			String access_token = json.getString("access_token");
			map.put("access_token", access_token);
		}
		String openid = json.containsKey("openid") == true ? json
				.getString("openid") : null;
		if (openid != null) {
			map.put("openid", openid);
		}
		return map;
	}
	
	/**
	 * 拉取用户信息(需scope为 snsapi_userinfo)
	 * @param param
	 * @return
	 * @throws Exception
	 */
    public static Map<String,Object> GetSnsapi_userinfoRequest(Map<String ,Object> param) throws Exception{
    	String openid = param.get("openid").toString();
    	String access_token = param.get("access_token").toString();
    	String GetSnsapi_userinfoRequest = Constant.GetSnsapi_userinfoRequest;
    	GetSnsapi_userinfoRequest  = GetSnsapi_userinfoRequest.replace("OPENID", URLEncoderUTF8.urlEnodeUTF8(openid));
    	GetSnsapi_userinfoRequest = GetSnsapi_userinfoRequest.replace("ACCESS_TOKEN", URLEncoderUTF8.urlEnodeUTF8(access_token));
    	String weixinUser = HttpsRequest.httpRequest(GetSnsapi_userinfoRequest, "POST", null);
    	JSONObject json = JSONObject.fromObject(weixinUser);
    	String nickname = json.getString("nickname");
    	String headimgurl = json.getString("headimgurl");
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("nickname", nickname);
    	map.put("headimgurl", headimgurl);
    	map.put("openid", openid);
    	return map;
    }

	/**
	 * Using: 获取access token调用凭据 access_token是公众号的全局唯一票据
	 * 
	 * @return
	 * @throws Exception
	 */
	public static String GetAccessTokenRequest() throws Exception {
		String GETJsaccess_tokenRequest = Constant.GETJSACCESS_TOKENREQUEST;
		GETJsaccess_tokenRequest = GETJsaccess_tokenRequest.replace("APPID",
				URLEncoderUTF8.urlEnodeUTF8(Constant.APPID));
		GETJsaccess_tokenRequest = GETJsaccess_tokenRequest.replace(
				"APPSECRET", URLEncoderUTF8.urlEnodeUTF8(Constant.SECRET));
		String jsonaccess = HttpsRequest.httpRequest(GETJsaccess_tokenRequest,
				"GET", null);
		JSONObject json = JSONObject.fromObject(jsonaccess);
		String access_token = json.getString("access_token");
		return access_token;
	}

	/**
	 * 获取 公众账号 唯一 openId
	 * 
	 * @return
	 */
	public static String GetOpenid(String access_token, String openid) {
		String GetOpenid = Constant.GETOPENID;
		GetOpenid = GetOpenid.replace("ACCESS_TOKEN", access_token);
		GetOpenid = GetOpenid.replace("OPENID", openid);
		String jsonaccess = HttpsRequest.httpRequest(GetOpenid, "GET", null);
		JSONObject json = JSONObject.fromObject(jsonaccess);
		return json.getString("openid");
	}

	// 通过access_token换取网页授权api_ticket
	public static String GetApi_ticketRequest(String access_token)
			throws Exception {
		String GETApi_ticketRequest = Constant.GETAPI_TICKETREQUEST;
		GETApi_ticketRequest = GETApi_ticketRequest.replace("ACCESS_TOKEN",
				URLEncoderUTF8.urlEnodeUTF8(access_token));
		String jsonaccess = HttpsRequest.httpRequest(GETApi_ticketRequest,
				"GET", null);
		JSONObject json = JSONObject.fromObject(jsonaccess);
		String api_ticket = json.getString("ticket");
		return api_ticket;
	}

}
