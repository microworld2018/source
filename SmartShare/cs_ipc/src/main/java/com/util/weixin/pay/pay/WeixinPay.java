package com.util.weixin.pay.pay;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import com.util.ArithUtil;
import com.util.weixin.Constant;
import com.util.weixin.pay.sign.PaySign;
import com.util.weixin.tool.HttpTool;
import com.util.weixin.tool.XMLUtil;

import net.sf.json.JSONObject;


public class WeixinPay {
	
	private static final Logger logger = Logger.getLogger(WeixinPay.class);

	 
	/**
	 * 微信支付
	 * @param remoteAddress
	 * @param openid
	 * @param orderCommonNo
	 * @param body格式：APP——需传入应用市场上的APP名字-实际商品名称，天天爱消除-游戏充值。
	 * @param totalFee
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static JSONObject wxpay(String remoteAddress, String orderCommonNo, String body, String totalFee,
			String notifyUrl) throws JDOMException, IOException {
		// 金额转化为分为单位
		// float sessionmoney = Float.parseFloat(totalFee);
		// String finalmoney = String.format("%.2f", sessionmoney);
		// finalmoney = finalmoney.replace(".", "");

		String payprice = String.valueOf(ArithUtil.mul(Double.valueOf(totalFee), 100));
		// 去除小数点
		payprice = payprice.substring(0, payprice.indexOf("."));

		JSONObject json = new JSONObject();
		SortedMap<Object, Object> contentMap = new TreeMap<Object, Object>();
		contentMap.put("appid", Constant.APPID); // 公众账号 ID
		contentMap.put("mch_id", Constant.MCHID); // 商户号
		contentMap.put("nonce_str", PaySign.create_nonce_str()); // 随机字符串
		// contentMap.put("body", URLEncoderUTF8.urlEnodeUTF8(body)); // 商品描述
		contentMap.put("body", body); // 商品描述
//		String orderNoRanDom = orderCommonNo + RanDomNum.createRandomNum();
		contentMap.put("out_trade_no", orderCommonNo); // 商户订单号
		System.out.println("给微信传的商户订单号：" + orderCommonNo);
		contentMap.put("total_fee", payprice); // 订单总金额
		contentMap.put("spbill_create_ip", remoteAddress); // 订单生成的机器IP
		contentMap.put("notify_url", notifyUrl); // 通知地址
		contentMap.put("trade_type", Constant.TRADE_TYPE_APP); // 交易类型
		String sign = PaySign.createSign(contentMap);// 签名
		contentMap.put("sign", sign);

		// 调用统一下单接口返回的值
		String result = HttpTool.sendPost(Constant.PREPAY_ID_URL, PaySign.sortmapTomap(contentMap), "UTF-8");
		// 调用统一接口返回的值XML转换为map格式
		Map<String, String> map = XMLUtil.doXMLParse(result);
		System.out.println(map);
		// System.out.println(map.get("return_code"));
		if (map != null && map.get("return_code").equals("SUCCESS") && map.get("result_code").equals("SUCCESS")) {
			SortedMap<Object, Object> wxPayParamMap = new TreeMap<Object, Object>();
			wxPayParamMap.put("appid", Constant.APPID);
			wxPayParamMap.put("partnerid", Constant.MCHID);
			wxPayParamMap.put("prepayid", map.get("prepay_id"));
			wxPayParamMap.put("package", "Sign=WXPay");
			wxPayParamMap.put("noncestr", PaySign.create_nonce_str());
			wxPayParamMap.put("timestamp", PaySign.create_timestamp());
			String paySign = PaySign.createSign(wxPayParamMap);// 支付得到的签名
			wxPayParamMap.put("sign", paySign);
			wxPayParamMap.put("return_code", map.get("return_code"));
			wxPayParamMap.put("result_code", map.get("result_code"));
			JSONObject pay = JSONObject.fromObject(wxPayParamMap);
			json.put("order", pay);
		} else {

			json.put("order", map);
		}
		return json;
	}
	
	/**
	 * 
	 * @date 2017年7月29日下午4:06:06
	 * @param remoteAddress
	 * @param orderCommonNo 商户订单号
	 * @param body
	 * @param totalFee 三方支付金额
	 * @param notifyUrl 回调地址
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static JSONObject wxpayNew(String remoteAddress, String orderCommonNo, String body, String totalFee,
			String notifyUrl) throws JDOMException, IOException {
		JSONObject json = new JSONObject();
		SortedMap<Object, Object> contentMap = new TreeMap<Object, Object>();
		contentMap.put("appid", Constant.APPID); // 公众账号 ID
		contentMap.put("mch_id", Constant.MCHID); // 商户号
		contentMap.put("nonce_str", PaySign.create_nonce_str()); // 随机字符串
		// contentMap.put("body", URLEncoderUTF8.urlEnodeUTF8(body)); // 商品描述
		contentMap.put("body", body); // 商品描述
		contentMap.put("out_trade_no", orderCommonNo); // 商户订单号
		System.out.println("给微信传的商户订单号：" + orderCommonNo);
		contentMap.put("total_fee", totalFee); // 订单总金额
		contentMap.put("spbill_create_ip", remoteAddress); // 订单生成的机器IP
		contentMap.put("notify_url", notifyUrl); // 通知地址
		contentMap.put("trade_type", Constant.TRADE_TYPE_APP); // 交易类型
		String sign = PaySign.createSign(contentMap);// 签名
		contentMap.put("sign", sign);

		// 调用统一下单接口返回的值
		String result = HttpTool.sendPost(Constant.PREPAY_ID_URL, PaySign.sortmapTomap(contentMap), "UTF-8");
		// 调用统一接口返回的值XML转换为map格式
		Map<String, String> map = XMLUtil.doXMLParse(result);
		System.out.println(map);
		// System.out.println(map.get("return_code"));
		if (map != null && map.get("return_code").equals("SUCCESS") && map.get("result_code").equals("SUCCESS")) {
			SortedMap<Object, Object> wxPayParamMap = new TreeMap<Object, Object>();
			wxPayParamMap.put("appid", Constant.APPID);
			wxPayParamMap.put("partnerid", Constant.MCHID);
			wxPayParamMap.put("prepayid", map.get("prepay_id"));
			wxPayParamMap.put("package", "Sign=WXPay");
			wxPayParamMap.put("noncestr", PaySign.create_nonce_str());
			wxPayParamMap.put("timestamp", PaySign.create_timestamp());
			String paySign = PaySign.createSign(wxPayParamMap);// 支付得到的签名
			wxPayParamMap.put("sign", paySign);
			wxPayParamMap.put("return_code", map.get("return_code"));
			wxPayParamMap.put("result_code", map.get("result_code"));
			JSONObject pay = JSONObject.fromObject(wxPayParamMap);
			json.put("order", pay);
		} else {

			json.put("order", map);
		}
		return json;
	}

	/**
	 * 微信查询接口
	 * 
	 * @param transactionId
	 * @return
	 * @throws IOException
	 * @throws JDOMException
	 */
	public static JSONObject confirmTradeResults(String transactionId) throws JDOMException, IOException {
		SortedMap<Object, Object> contentMap = new TreeMap<Object, Object>();
		contentMap.put("appid", Constant.APPID); // 公众账号 ID
		contentMap.put("mch_id", Constant.MCHID); // 商户号
		contentMap.put("nonce_str", PaySign.create_nonce_str()); // 随机字符串
		contentMap.put("transaction_id", transactionId); // 微信的订单号
		String sign = PaySign.createSign(contentMap);// 签名
		contentMap.put("sign", sign);

		// 调用查询接口返回的值
		String result = HttpTool.sendPost(Constant.QUERY_TRADE_RESULTS_URL, PaySign.sortmapTomap(contentMap), "UTF-8");
		Map<String, String> map = XMLUtil.doXMLParse(result);
		System.out.println(map);
		JSONObject json = new JSONObject();
		if (map == null) {
			json.put("result", null);
		} else if(map.get("return_code").equals("SUCCESS") && map.get("result_code").equals("SUCCESS")) {
			SortedMap<Object, Object> queryResultsMap = new TreeMap<Object, Object>();
			queryResultsMap.put("tradeState", map.get("trade_state"));
			queryResultsMap.put("totalFee", map.get("total_fee"));
			JSONObject results = JSONObject.fromObject(queryResultsMap);
			json.put("result", results);
		} else {
			json.put("result", "code_error");
		}
		return json;
	}

	/**
	 * 微信支付 异步通知
	 * 
	 * @param request
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static Map<String, Object> callBackUrl(HttpServletRequest request) throws JDOMException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String status = "no";
		// 微信支付通知参数
		// 接受微信支付通知post的参数
		Map<String, String> requestMap = XMLUtil.doXMLParse(request);

		String return_msg = "";

		System.out.println("异步回调地址返回参数====" + requestMap);
		// 判断是否财付通签名
		if (PaySign.isValidSign(PaySign.strmapToTreeMap(requestMap))) {
			String return_code = requestMap.get("return_code");
			return_msg = requestMap.get("return_msg");
			map.put("return_code", return_code);

			if ("SUCCESS".equals(return_code)) {
				status = "ok";
				String total_fee = requestMap.get("total_fee");// 订单总金额，单位为分
				String cash_fee = requestMap.get("cash_fee");// 订单现金支付金额
				String time_end = requestMap.get("time_end");// 支付时间
				String transaction_id = requestMap.get("transaction_id");// 微信支付订单号
				String out_trade_no = requestMap.get("out_trade_no");// 用户生成的订单号
//				out_trade_no = out_trade_no.substring(0, out_trade_no.length() - 4);
				System.out.println("异步通知的商户订单号：" + out_trade_no);
				map.put("total_fee", total_fee);
				map.put("cash_fee", cash_fee);
				map.put("time_end", time_end);
				map.put("tradeRecordNo", transaction_id);
				map.put("orderNo", out_trade_no);
				map.put("status", status);
				map.put("return_msg", "支付成功");
			} else {
				map.put("return_msg", return_msg);
				map.put("status", return_code);
			}
		} else {
			map.put("status", status);
		}
		return map;

	}
	
	/**
	 * 微信支付 异步通知(新)
	 * 
	 * @param request
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static Map<String, Object> callBackUrlNew(HttpServletRequest request) throws JDOMException, IOException {
		Map<String, Object> map = new HashMap<String, Object>();
		String status = "no";
		// 微信支付通知参数
		// 接受微信支付通知post的参数
		System.out.println("开始解析微信通知回调参数...");
		Map<String, String> requestMap = XMLUtil.doXMLParse(request);
		String return_msg = "";
		System.out.println("异步回调地址返回参数====" + requestMap);
		// 判断是否财付通签名
		if (PaySign.isValidSign(PaySign.strmapToTreeMap(requestMap))) {
			String return_code = requestMap.get("return_code");
			return_msg = requestMap.get("return_msg");
			map.put("return_code", return_code);
			if ("SUCCESS".equals(return_code)) {
				status = "ok";
				String total_fee = requestMap.get("total_fee");// 订单总金额，单位为分
				String cash_fee = requestMap.get("cash_fee");// 订单现金支付金额
				String time_end = requestMap.get("time_end");// 支付时间
				String transaction_id = requestMap.get("transaction_id");// 微信支付订单号
				String out_trade_no = requestMap.get("out_trade_no");// 商户订单号
//				out_trade_no = out_trade_no.substring(0, out_trade_no.length() - 4);
				System.out.println("异步通知的商户订单号：" + out_trade_no);
				map.put("totalAmount", total_fee);
				map.put("cash_fee", cash_fee);
				map.put("time_end", time_end);
				map.put("tradeRecordNo", transaction_id);
				map.put("outTradeNo", out_trade_no);
				map.put("status", status);
				map.put("return_msg", "支付成功");
			} else {
				map.put("return_msg", return_msg);
				map.put("status", return_code);
			}
		} else {
			map.put("status", status);
		}
		return map;
	}
	
	/**
	 * 微信关闭订单
	 * @param orderNo
	 * @param url
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static JSONObject wxCloseOrder(String orderNo, String url) throws JDOMException, IOException {
		SortedMap<Object, Object> contentMap = new TreeMap<Object, Object>();
		contentMap.put("appid", Constant.APPID); // 公众账号 ID
		contentMap.put("mch_id", Constant.MCHID); // 商户号
		contentMap.put("out_trade_no", orderNo); // 商户订单号
		contentMap.put("nonce_str", PaySign.create_nonce_str()); // 随机字符串
		String sign = PaySign.createSign(contentMap);// 签名
		contentMap.put("sign", sign);
		
		// 调用关闭接口返回的值
		String result = HttpTool.sendPost(url, PaySign.sortmapTomap(contentMap), "UTF-8");
		Map<String, String> map = XMLUtil.doXMLParse(result);
		System.out.println(map);
		
		JSONObject json = new JSONObject();
		if (map == null) {
			json.put("result", null);
		} else if(map.get("return_code").equals("SUCCESS") && map.get("result_code").equals("SUCCESS")) {
//			SortedMap<Object, Object> closeResultsMap = new TreeMap<Object, Object>();
//			closeResultsMap.put("tradeState", map.get("trade_state"));
//			closeResultsMap.put("totalFee", map.get("total_fee"));
			JSONObject results = JSONObject.fromObject(map);
			json.put("result", results);
		} else {
			json.put("result", "code_error");
		}
		return json;
	}
	
	/**
	 * 微信查询接口（根据商户订单号查询）
	 * @date 2017年7月26日下午4:45:25
	 * @param tradeOrderNo
	 * @return
	 * @throws JDOMException
	 * @throws IOException
	 */
	public static JSONObject orderQuery(String tradeOrderNo) throws JDOMException, IOException {
		SortedMap<Object, Object> contentMap = new TreeMap<Object, Object>();
		contentMap.put("appid", Constant.APPID); // 公众账号 ID
		contentMap.put("mch_id", Constant.MCHID); // 商户号
		contentMap.put("nonce_str", PaySign.create_nonce_str()); // 随机字符串
		contentMap.put("out_trade_no", tradeOrderNo); // 商户订单号
		String sign = PaySign.createSign(contentMap);// 签名
		contentMap.put("sign", sign);

		// 调用查询接口返回的值
		String result = HttpTool.sendPost(Constant.QUERY_TRADE_RESULTS_URL, PaySign.sortmapTomap(contentMap), "UTF-8");
		Map<String, String> map = XMLUtil.doXMLParse(result);
		System.out.println("商户订单号调微信查询接口返回数据：----> " + map);
		JSONObject json = new JSONObject();
		if (map == null) {
			json.put("result", null);
		} else if(map.get("return_code").equals("SUCCESS") && map.get("result_code").equals("SUCCESS")) {
			SortedMap<Object, Object> queryResultsMap = new TreeMap<Object, Object>();
			queryResultsMap.put("result_code", map.get("result_code"));
			queryResultsMap.put("tradeState", map.get("trade_state"));//交易状态
			queryResultsMap.put("totalFee", map.get("total_fee"));//总金额（分）
			if(map.get("trade_state").equals("NOTPAY")){
				queryResultsMap.put("totalFee", 0);
			}
			queryResultsMap.put("transactionId", map.get("transaction_id"));//微信订单号
			queryResultsMap.put("outTradeNo", map.get("out_trade_no"));//商户订单号
			
			JSONObject results = JSONObject.fromObject(queryResultsMap);
			json.put("result", results);
		} else {
			SortedMap<Object, Object> queryResultsMap = new TreeMap<Object, Object>();
			queryResultsMap.put("result_code", map.get("result_code"));
			queryResultsMap.put("err_code", map.get("err_code"));
			queryResultsMap.put("err_code_des", map.get("err_code_des"));
			JSONObject results = JSONObject.fromObject(queryResultsMap);
			json.put("result", results);
		}
		return json;
	}

	public static void main(String[] args) {
		double payprice = ArithUtil.mul(Double.valueOf("0.56"), 100);
		String s = String.valueOf(payprice);
		String newD = s.substring(0, s.indexOf("."));
		System.out.println(newD);
	}

}
