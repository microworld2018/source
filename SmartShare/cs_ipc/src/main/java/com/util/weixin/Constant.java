package com.util.weixin;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.util.ContantsTool;
import com.util.PropertiesUtil;

public class Constant {

	// *****************************************微信****************************************************************************************************************************************************************

	/*
	 * 微信appid 
	 */
	public static String APPID = "wxfdde3d5877da4cf8";
	/*
	 * 微信secrect
	 */
	public static String SECRET = "";

	/*
	 * 微信公众号后台 服务器配置处Token
	 */
	public static String TOKEN = "";

	/*
	 * 商户号
	 */
	public static final String MCHID = "";
	/*
	 * 支付密钥 key
	 */
	public static String KEY = "";

	// ****************************************支付*********************************************************************************************************
	/*
	 * 支付IP地址
	 */
	public static final String SPBILL_CREATE_IP = "127.0.0.1";
	/*
	 * 加密方式
	 */
	public static final String[] SIGNTYPE = { "MD5", "SHA1" };
	/*
	 * 设备号
	 */
	public static final String DEVICE_INFO = "";

	/*
	 * 订单支付回调地址
	 */
	public static final String JR_NOTIFY_URL = ContantsTool.DOMAIN_NAME+"cs_ipc/pay/wxCallBack";
	
	/*
	 * 订单支付回调地址(新)
	 */
	public static final String JR_NOTIFY_URL_NEW = ContantsTool.DOMAIN_NAME+"cs_ipc/pay/wxCallBackNew";

	/*
	 * 押金充值回调地址
	 */
	public static final String DEPOSIT_URL = ContantsTool.DOMAIN_NAME+"cs_ipc/app/wxCallBack";
	
	/*
	 * 违章事故扣款回调地址
	 */
	public static final String ACCIDENT_URL = ContantsTool.DOMAIN_NAME+"cs_ipc/app/wxCallBackAccident";
	
	/*
	 * 交易类型(微信网页支付)
	 */
	public static final String TRADE_TYPE_JS = "JSAPI";
	/*
	 * 交易类型(微信app支付)
	 */
	public static final String TRADE_TYPE_APP = "APP";
	/*
	 * 编码
	 */
	public static final String INPUT_CHARSET = "UTF-8";
	/*
	 * 统一支付接口
	 */
	public static final String PREPAY_ID_URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";
	
	/*
	 * 微信查询接口
	 */
	public static final String QUERY_TRADE_RESULTS_URL = "https://api.mch.weixin.qq.com/pay/orderquery";
	
	/*
	 * 微信关闭订单接口
	 */
	public static final String CLOSE_ORDER_URL = "https://api.mch.weixin.qq.com/pay/closeorder";

	// ******************************************推送信息**************************************************************************************************
	/*
	 * 3 发送模板消息
	 */
	public static final String FASONGMOBANURL = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=ACCESS_TOKEN";

	/*
	 * 创建菜单的URL
	 */
	public static final String CREATE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN";
	
	/*
	 * 全局 票据
	 */
	public static String getAccessToken(){
		return RedisClient.getData("wxjssdk", "access_token",DataBaseUtil.CS_IPC);
	}
	
	/*
	 * js临时票据
	 */
	public static String getJsApiTicket(){
		return RedisClient.getData("wxjssdk", "jsapi_ticket",DataBaseUtil.CS_IPC);
	}
	
	// ******************************************第三方授权**************************************************************************************************

	/*
	 * 微信scope(网页授权)
	 */
	public static final String SCOPE = "snsapi_userinfo";
	/*
	 * 微信scope(静默授权)
	 */
	public static final String SCOPEBASE = "snsapi_base";

	/* 微信号获取用户信息的请求地址
    */
    public static String GetSnsapi_userinfoRequest = "https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

	
	/*
	 * 微信推送模版ID
	 */
	public static final String TEMPLETE_ID = "2MZbINQKrZQJIMjCqd8Szzh-wlIJG5k4tKNQ0gzrwTY";

	/*
	 * 网页授权获取用户基本信息==>微信号获取code的请求地址
	 */
	public static final String GETCODEREQUEST = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect";
	/*
	 * 网页授权获取用户基本信息==>微信号获取access_token openid的请求地址
	 */
	public static final String GETACCESS_TOKENREQUEST = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code";
	/*
	 * 获取 调用凭据 接口 access_token(access_token是公众号的全局唯一票据)
	 */
	public static final String GETJSACCESS_TOKENREQUEST = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

	/*
	 * 获取 用户 公众账号 唯一 openid
	 */
	public static final String GETOPENID = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN";

	/*
	 * 获取Api_ticket
	 */
	public static final String GETAPI_TICKETREQUEST = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";


}
