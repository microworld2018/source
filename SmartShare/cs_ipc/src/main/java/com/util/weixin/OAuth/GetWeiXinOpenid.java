package com.util.weixin.OAuth;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.core.controller.ServiceContext;
import com.util.weixin.Constant;

public class GetWeiXinOpenid {

	private static final Logger logger = Logger
			.getLogger(GetWeiXinOpenid.class);
	
	/**
	 * 获取用户信息
	 * @param serviceContext
	 * @param redirect_url
	 * @return
	 */
	public static Map<String,Object> GetWeixinUserInfo(ServiceContext serviceContext,
			String redirect_url){
		Map<String, Object> usermap = new HashMap<String, Object>();
		String code = serviceContext.getRequest().getParameter("code");// 获取code
		if (null == code || "".equals(code)) {
			try {
				String url = GetWeiXinCode.getCodeRequest(redirect_url, Constant.SCOPE);
				HttpServletResponse response = serviceContext.getResponse();
				response.sendRedirect(url);// 重定向
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				Map<String, Object> map = GetWeiXinCode.GetAccess_tokenRequest(
						code, Constant.SCOPE);
				 if(!map.isEmpty()){
					 usermap=GetWeiXinCode.GetSnsapi_userinfoRequest(map);
				 }
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return usermap;
	}

	/**
	 * 获取普通用户 openid
	 * 
	 * @param serviceContext
	 * @param redirect_url
	 *            回调地址
	 * @return
	 */
	public static String GetWeixinOpenid(ServiceContext serviceContext,
			String redirect_url) {
		String openid = null;
		String code = serviceContext.getRequest().getParameter("code");// 获取code
		if (null == code || "".equals(code)) {
			try {
				String url = GetWeiXinCode.getCodeRequest(redirect_url,
						Constant.SCOPEBASE);
				HttpServletResponse response = serviceContext.getResponse();
				response.sendRedirect(url);// 重定向
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				Map<String, Object> map = GetWeiXinCode.GetAccess_tokenRequest(
						code, Constant.SCOPEBASE);
				if (null != map && !"".equals(map) && !map.isEmpty()) {
					openid = map.get("openid").toString();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return openid;
	}

	/**
	 * 获取 用户对于 公众账号 唯一openid
	 * 
	 * @param serviceContext
	 * @param redirect_url
	 *            回调地址
	 * @return
	 */
	public static String GetWXOpenId(ServiceContext serviceContext,
			String redirect_url) {
		String openid = GetWeixinOpenid(serviceContext, redirect_url);// 获取普通用户
		return openid;
	}

	 
}
