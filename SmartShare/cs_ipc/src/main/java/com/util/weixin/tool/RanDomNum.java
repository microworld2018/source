package com.util.weixin.tool;

/**
 * 随机产生4位数
 * @author zhangsg
 *
 */
public class RanDomNum {
	public static int createRandomNum(){
		int i = (int)(Math.random()*(9999-1000+1))+1000;//产生1000-9999的随机数
		return i;
	}
	
	public static void main(String[] args) {
		String s = "DD11112";
		s = s + createRandomNum();
		System.out.println(s);
		System.out.println(s.substring(0, s.length() - 4));
	}
}
