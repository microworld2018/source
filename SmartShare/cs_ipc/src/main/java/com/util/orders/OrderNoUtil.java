package com.util.orders;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OrderNoUtil extends Thread{  
  
    private static long orderNum = 0l;  
    private static String date ;  
      
    public static void main(String[] args) throws InterruptedException {  
        for (int i = 0; i < 10; i++) {  
            System.out.println(getOrderNo());  
            System.out.println(getOrderdetailNo());
            //Thread.sleep(1000);  
        }  
    }  
    /** 
     * 生成订单编号 
     * @return 
     */  
    public static synchronized String getOrderNo() {  
        String str = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());  
        if(date==null||!date.equals(str)){  
            date = str;  
            orderNum  = 0l;  
        }  
        orderNum ++;  
        long orderNo = Long.parseLong((date)) * 10000;  
        orderNo += orderNum;;  
        return orderNo+"";  
    }  
    /** 
     * 生成订单详情编号 
     * @return 
     */  
    public static synchronized String getOrderdetailNo() {  
        String str = new SimpleDateFormat("yyyyMMddHHmm").format(new Date());  
        DecimalFormat df = new DecimalFormat("000");
        if(date==null||!date.equals(str)){  
            date = str;  
            orderNum  = 0l;  
        }  
        orderNum ++;  
        long orderNo = Long.parseLong((date)) * 10000;  
        orderNo += orderNum;  
        return orderNo+df.format(1);  
    }  
    
}
