package com.util;

import java.util.HashMap;
import java.util.Map;




import net.sf.json.JSONObject;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import com.core.redis.RedisClient;
import com.core.redis.util.DataBaseUtil;
import com.leetu.subscriber.entity.Subscriber;
/**
 * 
* @ClassName: TokenUtils 
* @Description: Token 工具类
* @author LH
* @date 2016年3月9日 下午4:24:15 
*
 */
public class TokenUtils {
	
	/**
	 * @Title: getTokenEncode 
	* @Description: 加密 规则:当前时间戳MD5的16加密 加 code的32位加密
	* @param @param time
	* @param @param code
	* @return String
	 */
	@SuppressWarnings("restriction")
	public static String getTokenEncode(String time,String code){
		String md5code = Md5Util.MD5Encode(code);
		String md5time = Md5Util.MD5Encode(time).toString().substring(8, 24);
		BASE64Encoder base64Encoder = new BASE64Encoder();
		return base64Encoder.encode((md5time+md5code).getBytes());
	}
	@SuppressWarnings("restriction")
	public static Map<String, String> getTokenDecoder(String code) {
		Map<String, String> map = new HashMap<String,String>();
		BASE64Decoder decoder = new BASE64Decoder();
		byte[] decodeBuffer;
		try {
			decodeBuffer = decoder.decodeBuffer(code);
			String md5Token = new String(decodeBuffer);
			map.put("md5Time", md5Token.substring(0, 16));
			map.put("md5Code", md5Token.substring(16, md5Token.length()));
		} catch (Exception e) {
			map=null;
		}
		return map;
	}
	
	/**
	 * 获取会员信息
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public static Subscriber getSubscriber(String data){
		Subscriber subscriber = null;
		
		try {
			Map<String, String> map = JsonTools.desjsonForMap(data);
			String token = map.get("token");
			Map<String, String> toktenmap = TokenUtils.getTokenDecoder(token);
			if(toktenmap!=null){
				subscriber = (Subscriber) RedisClient.getobjkeys(toktenmap.get("md5Code"),DataBaseUtil.LOGIN_COMMON);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return subscriber;
	}
	/**
	 * 判断token 是否失效
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public static boolean isToken(String data) throws Exception{
		boolean status = true;
		String token = null;
		int database = DataBaseUtil.LOGIN_COMMON;
		//解密
		CryptoTools des = new CryptoTools("HeGeA8G3".getBytes(),"6LA2EyQm".getBytes());//自定义密钥
		data = des.decode(data);
		JSONObject jsonObject = JSONObject.fromObject(data);
		
		String apptoken = jsonObject.getString("token");//map.get("token");
		Map<String, String> toktenmap = TokenUtils.getTokenDecoder(apptoken);
		String key = toktenmap.get("md5Code")+"token";
		if(toktenmap!=null&&RedisClient.keyexists(key, database)){
			token = (String) RedisClient.getstrvalue(key,database);
		}
		if(apptoken.equals(token)){
			status = false;
		}
		return status;
	}
	
}
