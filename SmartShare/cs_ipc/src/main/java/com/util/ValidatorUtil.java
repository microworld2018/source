package com.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 校验工具类
 * @author GodZilla
 *
 */
public class ValidatorUtil {
	/**
	 * 手机号格式校验
	 * @param phoneNo
	 * @return
	 */
	public static boolean isPhone(String phoneNo){
		Pattern pattern = Pattern.compile("^0?(1)[0-9]{10}$");
		Matcher matcher = pattern.matcher(phoneNo);
		return matcher.matches();
	}
}
