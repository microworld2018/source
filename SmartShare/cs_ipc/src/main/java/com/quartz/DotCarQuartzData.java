package com.quartz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.leetu.place.mapper.BranchDotMapper;
import com.util.ToolDateTime;

@Service
public class DotCarQuartzData {

	@Autowired
	private BranchDotMapper branchDotMapper;
	
	/**
	 * 每月1号凌晨1点 删除c_car_dot表中isused=0的上上个月数据
	 */
//	@Scheduled(cron="0 0 1 1 * ?")   // 每月1号凌晨1点执行一次：0 0 1 1 * ?
    public void dotcar(){
//		String time = ToolDateTime.beforeDate();
//		Integer isused = 0;
//		branchDotMapper.deleteDotCar(isused,time);
	}
	
}
