package com.quartz;

import com.leetu.deposit.mapper.DepositMapper;
import com.leetu.feestrategy.entity.StrategyBase;
import com.leetu.feestrategy.mapper.StrategyBaseMapper;
import com.leetu.orders.entity.Orders;
import com.leetu.orders.mapper.OrderMapper;
import com.leetu.orders.service.PayOrderService;
import com.leetu.orders.util.OrderFee;
import com.leetu.orders.util.OrderRedisData;
import com.leetu.place.mapper.BranchDotMapper;
import com.leetu.place.param.BranchDotParam;
import com.leetu.subscriber.mapper.SubScriberMapper;
import com.leetu.sys.entity.Dict;
import com.leetu.sys.entity.SMSRecord;
import com.leetu.sys.mapper.DictMapper;
import com.leetu.sys.service.SysOperateLogService;
import com.util.ArithUtil;
import com.util.ToolDateTime;
import com.util.car.CurrentCarInfo;
import com.util.msg.sendMSg;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class OrderCancelQuartz
{

  @Autowired
  private BranchDotMapper branchDotMapper;

  @Autowired
  private DictMapper dictMapper;

  @Autowired
  private OrderMapper orderMapper;

  @Autowired
  private SubScriberMapper subScriberMapper;

  @Autowired
  private StrategyBaseMapper strategyBaseMapper;

  @Autowired
  private PayOrderService payorderService;

  @Autowired
  private DepositMapper depositMapper;

  @Autowired
  private SysOperateLogService sysOperateLogService;

  @Scheduled(cron="0 */1 * * * ?")
  public void currentOrder()
  {
    List currentOrders = this.orderMapper.getCurrentOrders();

    if (!(StringUtils.isEmpty(currentOrders)))
    {
      for (Iterator localIterator = currentOrders.iterator(); localIterator.hasNext(); ) { Orders orders = (Orders)localIterator.next();

        String carId = orders.getCarId();

        String orderId = orders.getId();

        Map carMap = this.branchDotMapper.carinfo(carId, null);

        Date beginTime = orders.getBeginTime();

        Date endTime = new Date();

        String allTime = "";

        Integer allminutes = Integer.valueOf(0);

        if ((beginTime != null) && (!("".equals(beginTime))) && (endTime != null) && (!("".equals(endTime)))) {
          allTime = ToolDateTime.getBetweenDate(beginTime, endTime);
          allminutes = Integer.valueOf(ToolDateTime.getDateMinuteSpace(beginTime, endTime));
          if (allminutes.intValue() < 1) {
            allminutes = Integer.valueOf(1);
          }

        }

        double allMilePrice = 0D;

        double allPrice = 0D;

        double allTimePrice = 0D;

        String vehiclePlateId = carMap.get("vehiclePlateId").toString();

        Map paramMap = new HashMap();

        paramMap.put("sn", vehiclePlateId);

        paramMap.put("beginTime", ToolDateTime.format(beginTime, "yyyy-MM-dd HH:mm:ss"));

        paramMap.put("endTime", ToolDateTime.format(endTime, "yyyy-MM-dd HH:mm:ss"));

        String mileage = BranchDotParam.getCarMileageForGPS(paramMap, CurrentCarInfo.CAR_MILEAGE_FOR_GPS_URL, this.sysOperateLogService);

        if ((!(StringUtils.isEmpty(mileage))) && (!(mileage.equals("error")))) {
          if ((!(StringUtils.isEmpty(mileage))) && (!(mileage.equals("0"))))
          {
            mileage = String.valueOf(ArithUtil.div(Double.parseDouble(mileage), 1000.0D, 2));

            mileage = String.valueOf(ArithUtil.round(Double.parseDouble(mileage)));
          }
        } else {
          Map currenteOrder = OrderRedisData.getCurrentOrder(orders.getId());

          if (!(StringUtils.isEmpty(currenteOrder)))
            mileage = currenteOrder.get("mileage").toString();
          else
            mileage = "0";
        }

        System.out.println("定时获取里程数据=====" + mileage + "===时间:" + ToolDateTime.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        String timeType = "fenzhong";
        Dict m = this.dictMapper.getDictByCodes("timeUnit", "fenzhong");
        Dict h = this.dictMapper.getDictByCodes("timeUnit", "xiaoshi");
        StrategyBase carfee = this.strategyBaseMapper.carfee(carId);

        BigDecimal kmPrice = carfee.getKmPrice();

        allMilePrice = ArithUtil.mul(kmPrice.doubleValue(), Double.parseDouble(mileage));

        String timelyFeeUnit = carfee.getTimelyFeeUnit();

        if (timelyFeeUnit.equals(m.getId())) {
          timeType = "fenzhong";
          if ((beginTime != null) && (!("".equals(beginTime))) && (endTime != null) && (!("".equals(endTime))))
            allTimePrice = OrderFee.conTimeFee(carfee, beginTime, endTime, timeType);
        }
        else if (timelyFeeUnit.equals(h.getId())) {
          timeType = "xiaoshi";
          if ((beginTime != null) && (!("".equals(beginTime))) && (endTime != null) && (!("".equals(endTime)))) {
            allTimePrice = OrderFee.conTimeFee(carfee, beginTime, endTime, timeType);
          }

        }

        allPrice = ArithUtil.add(allMilePrice, allTimePrice);

        OrderRedisData.addCurrentOrder(orderId, allTime, allPrice, mileage);

        Map order = new Hashtable();

        order.put("orderId", orderId);
        order.put("orders_duration", allminutes);
        order.put("total_fee", Double.valueOf(allPrice));
        order.put("actual_fee", Double.valueOf(allPrice));
        order.put("allMile", mileage);
        order.put("allMilePrice", Double.valueOf(allMilePrice));
        order.put("allTimePrice", Double.valueOf(allTimePrice));

        this.payorderService.updateOrderInfoQuartz(order);
      }
    }
  }

  @Scheduled(cron="0/1 * * * * ?")
  public void OrderTimingDo()
    throws ParseException
  {
    Date date = new Date();

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String timing = sdf.format(date);

    Map torders = OrderRedisData.tlist(timing);

    if ((torders != null) && (!(torders.isEmpty())))
    {
      Dict di = this.dictMapper.getDictByCodes("carBizState", "0");
      Integer status = Integer.valueOf(2);
      for (Iterator localIterator = torders.entrySet().iterator(); localIterator.hasNext(); ) { Map.Entry m = (Map.Entry)localIterator.next();
        String subId = (String)m.getKey();
        Map orederMap = (Map)m.getValue();
        String carId = orederMap.get("carId").toString();
        String orderId = orederMap.get("orderId").toString();
        String mobile = orederMap.get("mobile").toString();
        String userName = orederMap.get("userName").toString();

        Map car = this.branchDotMapper.carinfo(carId, null);
        String version = car.get("version").toString();

        int st = this.branchDotMapper.updateCarStatus(carId, di.getId(), version);
        if (st == 1)
        {
          this.orderMapper.updateOrderStatus(orderId, status);
          this.orderMapper.updateOrderDetailStatus(orderId);
          if (sendMSg.send(mobile, "【电动侠租车】亲，您的订单已超时取消，每天最多3次哦！"))
          {
            SMSRecord record = new SMSRecord();
            record.setContent("【电动侠租车】亲，您的订单已超时取消，每天最多3次哦！");
            record.setPhoneNo(mobile);
            record.setType(Integer.valueOf(3));
            record.setUserId(subId);
            record.setUserName(userName);
            record.setResult(mobile + ":Success");
            this.subScriberMapper.addSmsRecord(record);
          }
        }

        OrderRedisData.delredisTimeOrder(timing);
      }
    }
  }
}