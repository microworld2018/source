<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'upload.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <script type="text/javascript">  
    i = 1;  
    j = 1;  
    $(document).ready(function(){  
          
        $("#btn_add1").click(function(){  
            document.getElementById("newUpload1").innerHTML+='<div id="div_'+i+'"><input  name="file" type="file"  /><input type="button" value="删除"  onclick="del_1('+i+')"/></div>';  
              i = i + 1;  
        });  
          
        $("#btn_add2").click(function(){  
            document.getElementById("newUpload2").innerHTML+='<div id="div_'+j+'"><input  name="file_'+j+'" type="file"  /><input type="button" value="删除"  onclick="del_2('+j+')"/></div>';  
              j = j + 1;  
        });  
    });  
  
    function del_1(o){  
     document.getElementById("newUpload1").removeChild(document.getElementById("div_"+o));  
    }  
      
    function del_2(o){  
         document.getElementById("newUpload2").removeChild(document.getElementById("div_"+o));  
    }  
  
</script>  
</head>  
<body>  
  
     <h1> 图片服务器 上传 实例</h1>   
      <hr align="left" width="60%" color="#WE0000" size="3">  
    <form name="userForm1" action="upload/imgUpload" enctype="multipart/form-data" method="post">  
        <div id="newUpload1">  
            <input type="file" name="filedata">  
          <br> <br>     
            服务器图片接收大小限制：最大约9MB ，10KB 可事先约定
         <br> <br>    
            
           <br>
            图片保存样本：
           <label> <input type="radio" name="type" value="single" checked="checked">保存一张(服务器只保存一张图片)</label>
           <label> <input type="radio" name="type" value="double">保存多张(服务器保存多张格式的压缩图片，当选择多种保存后，压缩质量选项失效)</label>
           <br>
           
           <br>
           用户上传标识：
           <label> <input type="text" name="userId" value="systemTest">必填，用户上传则填写用户ID,如系统上传则填写系统ID</label>
           <br>
           
           <br>
           压缩质量：
           <label> <input type="radio" name="quality" value="">  原图</label>
           <label> <input type="radio" name="quality" value="low">低质量</label>
           <label> <input type="radio" name="quality" value="medium">中等质量</label>
           <label> <input type="radio" name="quality" value="hight" checked="checked">高质量   推荐 </label>
           PS：除GIF格式外，任意格式图片经压缩后后缀发生变化，压缩后为JPG格式。
           <br>
           图片用途：       
       	   <label> <input type="radio" name="useType" value="1" checked="checked">相册</label>
           <label> <input type="radio" name="useType" value="2">证件</label>
           <label> <input type="radio" name="useType" value="3">证据</label>
           <label> <input type="radio" name="useType" value="4">附件 </label>
           
           PS：图片用户可事先约定，也可由任意字符串构成，建议用阿拉伯数字代替类型。
        </div>  

        <input type="submit" value="上传" >  
    </form>   
    
    
    <div>
    返回实例
    JSON格式多样本保存模式：
    
    
    <br>
    
    {"state":true,"url":
    	{"url":
    	"http://114.215.134.7/test/1/20160707/5C4D5B5C771A18191E05181F05181F08191B121A1E121918.jpg",
    	"low_url":"http://114.215.134.7/test/1/20160707/low_5C4D5B5C771A18191E05181F05181F08191B121A1E121918.jpg",
    	"medium_url":"http://114.215.134.7/test/1/20160707/medium_5C4D5B5C771A18191E05181F05181F08191B121A1E121918.jpg",
    	"hight_url":"http://114.215.134.7/test/1/20160707/hight_5C4D5B5C771A18191E05181F05181F08191B121A1E121918.jpg"
    }}
    
    <br>
    <br>
    <br>
    如出现异常 则state字段 标示为false,如： 
    {"state":false,"error":"文件格式不正确"}
    
    </div>
    <br>  
    <br>  
    <hr align="left" width="60%" color="#WE0000" size="3"> 
     <h1>图片的单一删除	</h1>   
     
      <form name="userForm2" action="upload/imgRemove" enctype="multipart/form-data" method="post"> 
      	<input type="text" name="url"/>
        <input type="submit" value="删除" >  
    </form>   
    
    <br>  
    <br>  
    
        <hr align="left" width="60%" color="#WE0000" size="3"> 
     <h1>图盘的多张删除	</h1>   
     
      <form name="userForm2" action="upload/imgRemoves" enctype="multipart/form-data" method="post"> 
      	<input type="text" name="url"/>
      	<input type="text" name="url"/>
      	<input type="text" name="url"/>
        <input type="submit" value="删除多张图片" >  
    </form>   
    
    <br>  
    <br> 
</body>  
</html>  