package com.img.gif;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;






public class gifUtil {

	/**
	 * 压缩入口
	 * @return
	 * @throws java.lang.Exception 
	 */
	public static String entrance (double scale,String newFileName,String saveFilePath,String quality) throws Exception{
		
		GifDecoder gd = new GifDecoder();
		int status = gd.read(new FileInputStream(new File(saveFilePath+"/"+newFileName)));
		if (status != GifDecoder.STATUS_OK) {
			 throw new java.lang.Exception("验证错误");
		}
		
		AnimatedGifEncoder ge = new AnimatedGifEncoder();
		FileOutputStream fo = new FileOutputStream(new File(saveFilePath+"/"+quality+"_"+newFileName));
		ge.start(fo);
		ge.setRepeat(0);
		//ge.setSize((int)scale*100,(int)scale*100);
		for (int i = 0; i < gd.getFrameCount(); i++) {
			BufferedImage frame = gd.getFrame(i);
			int width = frame.getWidth();
			int height = frame.getHeight();
//			width = (int) (width * 1);
//			height = (int) (height * 1);
			width = (int)(width*scale);
			height = (int)(height*scale);
			BufferedImage rescaled = Scalr.resize(frame, Scalr.Mode.FIT_EXACT, width, height);
			int delay = gd.getDelay(i);
			ge.setDelay(delay);
			ge.addFrame(rescaled);
			rescaled.flush();
		}
		ge.finish();
		fo.flush();
		fo.close();
		gd= null;
		ge = null;
		return saveFilePath+"/"+quality+"_"+newFileName;
	}

}
