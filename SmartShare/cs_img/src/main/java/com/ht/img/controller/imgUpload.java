package com.ht.img.controller;


import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.core.controller.BaseController;
import com.util.PropertiesUtil;
import com.util.StringUtil;
import com.util.Utils;
import com.util.imgUploadUtil;


@Controller
@RequestMapping("/upload")
public class imgUpload extends BaseController<imgUpload> {
	
	Logger  log = Logger.getLogger(imgUpload.class);
	
	@RequestMapping(value="/imgUpload")
	public void imgUpload(@RequestParam MultipartFile filedata){
		String type = request.getParameter("type");
		String userId = request.getParameter("userId"); 
		String fileName = request.getParameter("fileName");
		String quality = request.getParameter("quality");  
		String useType =request.getParameter("useType");
		
		JSONObject ret = new JSONObject();
		ret.put("state", true);
		if(imgUploadUtil.checkImage(filedata)){
			
			if(type==null||type.equals("single")){
				
				try {
					ret.put("url", imgUploadUtil.singleSave(filedata, fileName, userId, quality, useType));
					log.info("[" + request.getRemoteAddr()+"]：" + ret.getString("url"));
				} catch (Exception e) {
					ret.put("state", false);
					ret.put("error", "上传图片出错，原因:"+e.getMessage());
					log.error("[" + request.getRemoteAddr()+"]：" + e);
					e.printStackTrace();
				}
				
			}else if(type.equals("double")){
				 
				try {
					ret.put("url", imgUploadUtil.doubleSave(filedata, fileName, userId, useType).toString());
					log.info("[" + request.getRemoteAddr()+"]：" + ret.getString("url"));
				} catch (Exception e) {
					ret.put("state", false);
					ret.put("error", "上传图片出错，原因:"+e.getMessage());
					log.error("[" + request.getRemoteAddr()+"]：" + e);
					e.printStackTrace();
				}
			}
			
		}else{
			ret.put("state", false);
			ret.put("error", "文件格式不正确");
			log.error("[" + request.getRemoteAddr()+"]：文件格式不正确，请求的文件全称为："+filedata.getOriginalFilename() );
		}
		
		
		Utils.outJSONObject(ret.toString(), null, response);
		
	}
	
	
	@RequestMapping(value="/imgRemove")
	public void remove(String url){
		
		JSONObject ret = new JSONObject();
		
		if(!StringUtil.isEmpty(url)){
			try {
				imgUploadUtil.delImgByUrl(url);
				ret.put("state", true);
				log.info("[" + request.getRemoteAddr()+"]：删除一张图:" +url );
			} catch (Exception e) {
				e.printStackTrace();
				ret.put("state", false);
				ret.put("error", e.getMessage());
				log.error("[" + request.getRemoteAddr()+"]：删除图片出错:" +url,e);
				
			}
			
		}
		Utils.outJSONObject(ret.toString(), null, response);
	}
	
	
	
	
	@RequestMapping(value="/imgRemoves")
	public void removes(String [] url){
		
		JSONObject ret = new JSONObject();
		
		if(null!=url &&url.length>0){
 
				for (String u : url) {
					try {
						imgUploadUtil.delImgByUrl(u);
						ret.put("state", true);
						log.info("[" + request.getRemoteAddr()+"]：删除一张图:" +url );
					} catch (Exception e) {
						e.printStackTrace();
						ret.put("state", false);
						ret.put("error",( ret.get("error")==null?"": ret.get("error")) + "" + u+"删除失败："+  e.getMessage());
						log.error("[" + request.getRemoteAddr()+"]：删除图片出错:" +url,e);
					}
					
				}
				
			 
		}
		Utils.outJSONObject(ret.toString(), null, response);
		
	}
	
	
	
	@RequestMapping(value="/upload")
	public String upload(){
		
		return "/upload/upload";
		
	}
	

}
