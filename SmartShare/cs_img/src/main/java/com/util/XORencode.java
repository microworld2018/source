package com.util;

/**
 * 简单异或加密
 *
 */
public class XORencode {
	
	public static final String key = "huatai";
	
	  public static  String EncryptCode(String res, String key) {
	        byte[] bs = res.getBytes();
	        for (int i = 0; i < bs.length; i++) {
	            bs[i] = (byte) ((bs[i]) ^ key.hashCode());
	        }
	        return parseByte2HexStr(bs);
	    }
	  
	  
	  public static String DeCode(String res, String key) {
	        byte[] bs = parseHexStr2Byte(res);
	        for (int i = 0; i < bs.length; i++) {
	            bs[i] = (byte) ((bs[i]) ^ key.hashCode());
	        }
	        return new String(bs);
	    }
	
    public static String parseByte2HexStr(byte buf[]) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);  
            if (hex.length() == 1) {
                hex = '0' + hex;  
            }
            sb.append(hex.toUpperCase());  
        }
        return sb.toString();  
    }
    public static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1)
            return null;
        byte[] result = new byte[hexStr.length()/2];
        for (int i = 0;i< hexStr.length()/2; i++) {
            int high = Integer.parseInt(hexStr.substring(i*2, i*2+1), 16);  
            int low = Integer.parseInt(hexStr.substring(i*2+1, i*2+2), 16);  
            result[i] = (byte) (high * 16 + low);  
        }
        return result;  
    }
    
    
    public static void main(String[] args) {
		
    	String  m = XORencode.EncryptCode("我爱你",XORencode.key);
    	System.err.println("加密后:"+ m);
    	
    	System.err.println("解密后："+XORencode.DeCode("5C4D5B5C771A18191E05181F05181E08191C121B1F121818",XORencode.key));
	}
    
    

}
