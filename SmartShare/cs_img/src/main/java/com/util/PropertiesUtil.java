package com.util;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

@SuppressWarnings("all")
public class PropertiesUtil {
	private static String configFile = "config.properties";


	public static String getStringValue(String fileName, String key,
			String defaultValue) {
		CompositeConfiguration config = new CompositeConfiguration();
		PropertiesConfiguration pc = null;
		String value = "";
		try {
			pc = new PropertiesConfiguration(fileName);
			config.addConfiguration(pc);
			value = config.getString(key, defaultValue).trim();
		} catch (ConfigurationException e) {
			e.printStackTrace();
		} finally {
			return value;
		}
	}

	public static String getStringValue(String key, String defaultValue) {
		return PropertiesUtil.getStringValue(configFile, key, defaultValue);
	}

	public static String getStringValue(String key) {
		return PropertiesUtil.getStringValue(key, "");
	}

	public static int getIntValue(String fileName, String key, int defaultValue) {
		CompositeConfiguration config = new CompositeConfiguration();
		PropertiesConfiguration pc = null;
		int value = 0;
		try {
			pc = new PropertiesConfiguration(fileName);
			config.addConfiguration(pc);
			value = config.getInt(key, defaultValue);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		} finally {
			return value;
		}
	}

	public static int getIntValue(String key, int defaultValue) {
		return PropertiesUtil.getIntValue(configFile, key, defaultValue);
	}

	public static int getIntValue(String key) {
		return PropertiesUtil.getIntValue(key, 0);
	}

	public static String[] getPropertiesValues(String fileName, String key,
			char delimiter) {
		CompositeConfiguration config = new CompositeConfiguration();
		PropertiesConfiguration pc = null;
		if (!Character.isWhitespace(delimiter)) {
			AbstractConfiguration.setDefaultListDelimiter(delimiter);
		}
		try {
			pc = new PropertiesConfiguration(fileName);
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		config.addConfiguration(pc);

		String[] filevalues = config.getStringArray(key);
		return filevalues;
	}
	
	
    public static Properties loadPropertiesFile(String filePath) {
    	PropertiesUtil.class.getClassLoader().getResourceAsStream("password.properties");
        Properties properties = new Properties();
        InputStream is = null;
         
        try {
            try {
                is = new FileInputStream(filePath);
                properties.load(is);
            } finally {
                if (is != null) {
                    is.close();
                    is = null;
                }
            }
        } catch (Exception e) {
            properties = null;
        }
         
        return properties;
    }
    
    public static Properties loadProperties(String FileName) {
    	
        Properties properties = new Properties();
        InputStream is = null;
         
        try {
            try {
                is = PropertiesUtil.class.getClassLoader().getResourceAsStream(FileName);
                properties.load(is);
            } finally {
                if (is != null) {
                    is.close();
                    is = null;
                }
            }
        } catch (Exception e) {
            properties = null;
        }
         
        return properties;
    }


    public static Map<String, Properties> loadPropertiesFiles(List<String> filePaths) {
        Map<String, Properties> propertiesMap = new HashMap<String, Properties>();
         
        for (String filePath : filePaths) {
            propertiesMap.put(filePath, loadPropertiesFile(filePath));
        }
         
        return propertiesMap;
    }
     
    public static boolean storePropertiesFile(String filePath, Properties properties) {
        return storePropertiesFile(filePath, getProperty(properties));
    }
     
    public static boolean storePropertiesFile(String filePath, Map<String, String> propertyMap) {
        Properties properties = new Properties();
        FileWriter writer = null;
         
        try {
            try {
                writer = new FileWriter(filePath);
                 
                for (Map.Entry<String, String> entry : propertyMap.entrySet()) {
                    properties.put(entry.getKey(), entry.getValue());
                }
                 
                properties.store(writer, null);
            } finally {
                if (writer != null) {
                    writer.close();
                    writer = null;
                }
            }
             
            return true;
        } catch(Exception e) {
            return false;
        }
    }
     
    public static boolean storePropertiesFiles(List<String> filePaths, List<Map<String, String>> propertyMaps) {
        int filePathSize = filePaths.size();
         
        if (filePathSize != propertyMaps.size()) {
            return false;
        }
         
        for (int i = 0; i < filePathSize; i++) {
            if (!storePropertiesFile(filePaths.get(i), propertyMaps.get(i))) {
                return false;
            }
        }
         
        return true;
    }
     
    public static String getString(String key, String defaultValue, Properties properties) {
        return (properties == null) ? null : properties.getProperty(key, defaultValue);
    }
     
    public static Integer getInteger(String key, Integer defaultValue, Properties properties) {
        String stringValue = (properties == null) ? null : properties.getProperty(key, defaultValue.toString());
        Integer value = null;
         
        if (stringValue == null) {
            return null;
        }
         
        try {
            value = Integer.valueOf(stringValue);
        } catch (NumberFormatException e) {
            value = defaultValue;
        }
         
        return value;
    }
     
    public static Boolean getBoolean(String key, Boolean defaultValue, Properties properties) {
        String stringValue = (properties == null) ? null : properties.getProperty(key, defaultValue.toString());
        return (stringValue == null) ? null : Boolean.valueOf(stringValue);
    }
     
    public static Double getDouble(String key, Double defaultValue, Properties properties) {
        String stringValue = (properties == null) ? null : properties.getProperty(key, defaultValue.toString());
        Double value = null;
         
        if (stringValue == null) {
            return null;
        }
         
        try {
            value = Double.valueOf(stringValue);
        } catch (NumberFormatException e) {
            value = defaultValue;
        }
         
        return value;
    }
     
    public static Date getDate(String key, Date defaultValue, Properties properties) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String stringValue = (properties == null) ? null : properties.getProperty(key, sdf.format(defaultValue));
        Date value = null;
         
        if (stringValue == null) {
            return null;
        }
         
        try {
            value = sdf.parse(stringValue);
        } catch (ParseException e) {
            value = defaultValue;
        }
         
        return value;
    }
     
    public static Map<String, String> getProperty(List<String> keys, Properties properties) {
        Map<String, String> propertyMap = new HashMap<String, String>();
         
        for (String key : keys) {
            propertyMap.put(key, properties.getProperty(key));
        }
         
        return propertyMap;
    }
     
    public static Map<String, String> getProperty(List<String> keys, String filePath) {
        return getProperty(keys, loadPropertiesFile(filePath));
    }
     
    public static Map<String, String> getProperty(Properties properties) {
        Map<String, String> propertyMap = new HashMap<String, String>();
         
        for (Iterator<Object> iter = properties.keySet().iterator(); iter.hasNext();) {
            String key = iter.next().toString();
            propertyMap.put(key, properties.getProperty(key));
        }
         
        return propertyMap;
    }
     
    public static Map<String, String> getProperty(String filePath) {
        return getProperty(loadPropertiesFile(filePath));
    }
    
    
    public static void main(String[] args) {
		
    	Properties p = PropertiesUtil.loadPropertiesFile("D:\\workspace\\myeclipse 2014\\cs_img\\src\\main\\resources\\imgUpload.properties");
    	
    	System.err.println(PropertiesUtil.loadProperties("imgUpload.properties"));
    	 System.err.println(PropertiesUtil.getString("img.format", "", p));
	}
    
}