package com.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.lang.StringUtils;



public class SendMsgUtil {
	private static Map<String, Object> statusMap = new HashMap<String, Object>();
	static {
		statusMap.put("-1", "û�и��û��˻�");
		statusMap.put("-2", "�ӿ���Կ����ȷ,�����˻���½����");
		statusMap.put("-21", "MD5�ӿ���Կ���ܲ���ȷ");
		statusMap.put("-3", "������������");
		statusMap.put("-11", "���û�������");
		statusMap.put("-14", "�������ݳ��ַǷ��ַ�");
		statusMap.put("-4", "�ֻ�Ÿ�ʽ����ȷ");
		statusMap.put("-41", "�ֻ����Ϊ��");
		statusMap.put("-42", "��������Ϊ��");
		statusMap.put("-51", "����ǩ���ʽ����ȷ,�ӿ�ǩ���ʽΪ����ǩ�����ݡ�");
		statusMap.put("-6", "IP����");
		// ����0 ���ŷ�������
		// ����״̬Ϊ�������
		statusMap.put("0", "δ֪�����ȡ��֤��ʧ�ܣ�");
	}

	public static String sendMsg(String mobile, String content) throws HttpException, IOException {
        if(StringUtils.isBlank(mobile)){
        	return "0";
        }
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod("http://gbk.sms.webchinese.cn");
		post.addRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=gbk");// ��ͷ�ļ�������ת��
		//NameValuePair[] data = { new NameValuePair("Uid", "haozhaiyou"), new NameValuePair("Key", "6debf947188c745c9772"), new NameValuePair("smsMob", mobile), new NameValuePair("smsText", content) };
		NameValuePair[] data = { new NameValuePair("Uid", "aasas"), new NameValuePair("Key", "9e6ff4010s3845e180aa28"), new NameValuePair("smsMob", mobile), new NameValuePair("smsText", content) };
		post.setRequestBody(data);

		client.executeMethod(post);
		Header[] headers = post.getResponseHeaders();
		int statusCode = post.getStatusCode();
		System.out.println("statusCode:" + statusCode);
		for (Header h : headers) {
			System.out.println(h.toString());
		}
		String result = new String(post.getResponseBodyAsString().getBytes("gbk"));
		System.out.println(result); // ��ӡ������Ϣ״̬
		post.releaseConnection();

		return result;
	}
	
	public static JSONObject sendMessageCheckCode(String mobile) {
		String code = StringUtil.randomString(4);
		String result = "1";
		try {
			result = sendMsg(mobile, "���ȡ����֤���ǣ�" + code+"  ������ʼǡ�");
			System.out.println("code:"+code);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		JSONObject json = new JSONObject();
		if (Integer.parseInt(result) > 0) {
			json.put("status", "ok");
			json.put("msg", "��ȡ��֤��ɹ���");
			json.put("code", code);
		} else {
			json.put("status", "fail");
			json.put("msg", statusMap.get(code));
		}
		return json;
	}
	
}
