package com.util;

import java.text.ParseException;  
import java.text.SimpleDateFormat;  
import java.util.Calendar;  
import java.util.Date;  
import java.util.HashMap;  
import java.util.Map;  

import com.util.imgUploadUtil.EnumQuality;
      
    public class DateUtil {  
        private static String ymdhms = "yyyy-MM-dd HH:mm:ss";    
        private static String ymd = "yyyy-MM-dd";    
        public static SimpleDateFormat ymdSDF = new SimpleDateFormat(ymd);    
        private static String year = "yyyy";    
        private static String month = "MM";    
        private static String day = "dd";    
        public static SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat(ymdhms);    
        public static SimpleDateFormat yearSDF = new SimpleDateFormat(year);    
        public static SimpleDateFormat monthSDF = new SimpleDateFormat(month);    
        public static SimpleDateFormat daySDF = new SimpleDateFormat(day);    
        
        public static SimpleDateFormat yyyyMMddHHmm = new SimpleDateFormat(    
                "yyyy-MM-dd HH:mm");    
        
        public static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-MM-dd");    
        
        public static SimpleDateFormat yyyyMMddHH_NOT_ = new SimpleDateFormat(    
                "yyyyMMdd");    
        
        public static long DATEMM = 86400L;    
        
        public static String getCurrentTime() {    
            return yyyyMMddHHmmss.format(new Date());    
        }    
        
        public static String getYesterdayYYYYMMDD() {    
            Date date = new Date(System.currentTimeMillis() - DATEMM * 1000L);    
            String str = yyyyMMdd.format(date);    
            try {    
                date = yyyyMMddHHmmss.parse(str + " 00:00:00");    
                return yyyyMMdd.format(date);    
            } catch (ParseException e) {    
                e.printStackTrace();    
            }    
            return "";    
        }    
        
        public String getStrDate(String backDay) {  
            Calendar calendar = Calendar.getInstance() ;  
            calendar.add(Calendar.DATE, Integer.parseInt("-" + backDay));  
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd") ;  
            String back = sdf.format(calendar.getTime()) ;  
            return back ;  
        }  
        
        public static String getCurrentYear() {    
            return yearSDF.format(new Date());    
        }   
        public static String getCurrentMonth() {    
            return monthSDF.format(new Date());    
        }   
        public static String getCurrentDay() {    
            return daySDF.format(new Date());    
        }    
        public static String getCurrentymd() {    
            return ymdSDF.format(new Date());    
        }    
        
        public static long getTimeNumberToday() {    
            Date date = new Date();    
            String str = yyyyMMdd.format(date);    
            try {    
                date = yyyyMMdd.parse(str);    
                return date.getTime() / 1000L;    
            } catch (ParseException e) {    
                e.printStackTrace();    
            }    
            return 0L;    
        }    
        
        public static String getTodateString() {    
            String str = yyyyMMddHH_NOT_.format(new Date());    
            return str;    
        }  
        
        public static String getYesterdayString() {    
            Date date = new Date(System.currentTimeMillis() - DATEMM * 1000L);    
            String str = yyyyMMddHH_NOT_.format(date);    
            return str;    
        }    
        
        public static Date getYesterDayZeroHour() {    
            Calendar cal = Calendar.getInstance();    
            cal.add(Calendar.DATE, -1);    
            cal.set(Calendar.SECOND, 0);    
            cal.set(Calendar.MINUTE, 0);    
            cal.set(Calendar.HOUR, 0);    
            return cal.getTime();    
        }  
        
        public static String longToString(long date, String format) {    
            SimpleDateFormat sdf = new SimpleDateFormat(format);    
            // 前面的lSysTime是秒数，先乘1000得到毫秒数，再转为java.util.Date类型    
            java.util.Date dt2 = new Date(date * 1000L);    
            String sDateTime = sdf.format(dt2); // 得到精确到秒的表示：08/31/2006 21:08:00    
            return sDateTime;    
        }    
        
        public static Date getTodayZeroHour() {    
            Calendar cal = Calendar.getInstance();    
            cal.set(Calendar.SECOND, 0);    
            cal.set(Calendar.MINUTE, 0);    
            cal.set(Calendar.HOUR, 0);    
            return cal.getTime();    
        }   
        
        public static Date getYesterDay24Hour() {    
            Calendar cal = Calendar.getInstance();    
            cal.add(Calendar.DATE, -1);    
            cal.set(Calendar.SECOND, 59);    
            cal.set(Calendar.MINUTE, 59);    
            cal.set(Calendar.HOUR, 23);    
            return cal.getTime();    
        }  
        
        public static Date stringToDate(String date, String format) {    
            SimpleDateFormat sdf = new SimpleDateFormat(format);    
            try {    
                return sdf.parse(date);    
            } catch (ParseException e) {    
                return null;    
            }    
        }    
        
        public static Date getStartDayOfWeek(Date date) {    
            Calendar c = Calendar.getInstance();    
            c.setTime(date);    
            c.set(Calendar.DAY_OF_WEEK, 1);    
            date = c.getTime();    
            return date;    
        }    
        
        public static Date getLastDayOfWeek(Date date) {    
            Calendar c = Calendar.getInstance();    
            c.setTime(date);    
            c.set(Calendar.DAY_OF_WEEK, 7);    
            date = c.getTime();    
            return date;    
        }    
        
        public static Date getStartDayOfMonth(Date date) {    
            Calendar c = Calendar.getInstance();    
            c.setTime(date);    
            c.set(Calendar.DAY_OF_MONTH, 1);    
            date = c.getTime();    
            return date;    
        }    
        
        public static Date getLastDayOfMonth(Date date) {    
            Calendar c = Calendar.getInstance();    
            c.setTime(date);    
            c.set(Calendar.DATE, 1);    
            c.add(Calendar.MONTH, 1);    
            c.add(Calendar.DATE, -1);    
            date = c.getTime();    
            return date;    
        }    
        
        public static Date getStartDayOfNextMonth(Date date) {    
            Calendar c = Calendar.getInstance();    
            c.setTime(date);    
            c.add(Calendar.MONTH, 1);    
            c.set(Calendar.DAY_OF_MONTH, 1);    
            date = c.getTime();    
            return date;    
        }    
        
        public static Date getLastDayOfNextMonth(Date date) {    
            Calendar c = Calendar.getInstance();    
            c.setTime(date);    
            c.set(Calendar.DATE, 1);    
            c.add(Calendar.MONTH, 2);    
            c.add(Calendar.DATE, -1);    
            date = c.getTime();    
            return date;    
        }    
        
        public static String givedTimeToBefer(String givedTime, long interval,    
                String format_Date_Sign) {    
            String tomorrow = null;    
            try {    
                SimpleDateFormat sdf = new SimpleDateFormat(format_Date_Sign);    
                Date gDate = sdf.parse(givedTime);    
                long current = gDate.getTime(); // 将Calendar表示的时间转换成毫秒    
                long beforeOrAfter = current - interval * 1000L; // 将Calendar表示的时间转换成毫秒    
                Date date = new Date(beforeOrAfter); // 用timeTwo作参数构造date2    
                tomorrow = new SimpleDateFormat(format_Date_Sign).format(date);    
            } catch (ParseException e) {    
                e.printStackTrace();    
            }    
            return tomorrow;    
        }    
        public static long stringToLong(String date, String format) {    
            SimpleDateFormat sdf = new SimpleDateFormat(format);    
            Date dt2 = null;    
            long lTime = 0;    
            try {    
                dt2 = sdf.parse(date);    
                // 继续转换得到秒数的long型    
                lTime = dt2.getTime() / 1000;    
            } catch (ParseException e) {    
                e.printStackTrace();    
            }    
        
            return lTime;    
        }    
        
        public static Map<String, String> getTwoDay(String endTime,    
                String beginTime, boolean isEndTime) {    
            Map<String, String> result = new HashMap<String, String>();    
            if ((endTime == null || endTime.equals("") || (beginTime == null || beginTime    
                    .equals(""))))    
                return null;    
            try {    
                java.util.Date date = ymdSDF.parse(endTime);    
                endTime = ymdSDF.format(date);    
                java.util.Date mydate = ymdSDF.parse(beginTime);    
                long day = (date.getTime() - mydate.getTime())    
                        / (24 * 60 * 60 * 1000);    
                result = getDate(endTime, Integer.parseInt(day + ""), isEndTime);    
            } catch (Exception e) {    
            }    
            return result;    
        }    
        
        public static Integer getTwoDayInterval(String endTime, String beginTime,    
                boolean isEndTime) {    
            if ((endTime == null || endTime.equals("") || (beginTime == null || beginTime    
                    .equals(""))))    
                return 0;    
            long day = 0l;    
            try {    
                java.util.Date date = ymdSDF.parse(endTime);    
                endTime = ymdSDF.format(date);    
                java.util.Date mydate = ymdSDF.parse(beginTime);    
                day = (date.getTime() - mydate.getTime()) / (24 * 60 * 60 * 1000);    
            } catch (Exception e) {    
                return 0 ;    
            }    
            return Integer.parseInt(day + "");    
        }    
        
        public static Map<String, String> getDate(String endTime, Integer interval,    
                boolean isEndTime) {    
            Map<String, String> result = new HashMap<String, String>();    
            if (interval == 0 || isEndTime) {    
                if (isEndTime)    
                    result.put(endTime, endTime);    
            }    
            if (interval > 0) {    
                int begin = 0;    
                for (int i = begin; i < interval; i++) {    
                    endTime = givedTimeToBefer(endTime, DATEMM, ymd);    
                    result.put(endTime, endTime);    
                }    
            }    
            return result;    
        }   
        
        
        public enum EnumQuality{
            low(0.5,0.5f),medium(0.7,0.9f),hight(1,1f);
            private final double scale;
            private final float  quality;

            EnumQuality(double scale,float quality) {
                this.scale = scale;
                this.quality = quality;
            }
            public double getScale() {
                return scale;
            }
            public double getQuality() {
                return quality;
            }
        }
        
        
        public static void main(String[] args) {
        	EnumQuality q = Enum.valueOf(EnumQuality.class, "22");
    		
        	System.err.println(q.toString());
		}
        
    } 