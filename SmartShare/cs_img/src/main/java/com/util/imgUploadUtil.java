package com.util;

import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import javax.imageio.ImageIO;

import net.sf.json.JSONObject;

import org.springframework.web.multipart.MultipartFile;

import com.img.ZipImg.ImageUtils;
import com.img.gif.gifUtil;

public class imgUploadUtil {
	
	 private static long Maxsize;
	 private static long Minsize;
	 private static String path;
	 private static String[] format ;
	 private static String httpurl;
	 
	static {
		Properties p = PropertiesUtil.loadProperties("imgUpload.properties");
		Maxsize = PropertiesUtil.getInteger("img.MaxSize", 0, p);
        Minsize = PropertiesUtil.getInteger("img.MiniSize", 0, p);
        path = PropertiesUtil.getString("img.path", "", p);
        format = PropertiesUtil.getString("img.format", "", p).split(",");
        httpurl = PropertiesUtil.getString("img.httpUrl", "", p);
        
	}
	
	public enum EnumQuality{
        low(0.5,0.5f),medium(0.7,0.9f),hight(1,1f);
        private final double scale;
        private final float  quality;

        EnumQuality(double scale,float quality) {
            this.scale = scale;
            this.quality = quality;
        }
        public double getScale() {
            return scale;
        }
        public double getQuality() {
            return quality;
        }
    }
	
	
	public static boolean checkImage(MultipartFile filedata){
		long filesize = filedata.getSize();
		String filename = filedata.getOriginalFilename();
		
		if(filesize<=Maxsize&&filesize>=Minsize){
			
			for (String fileformat : format) {
				if(filename.endsWith("."+fileformat)){
					return true;
				}
			} 
			
		}
		return false;
		
	}
	
	public static String singleSave(MultipartFile filedata,String fileName,String userId,String quality,String useType) throws Exception{
		
		String newFileName = imgUploadUtil.createNewFileName(userId, fileName,filedata.getOriginalFilename());
		 
		String saveFilePath = path +"/"+userId+"/"+useType +"/"+DateUtil.getTodateString();
		
		String retUrl = saveFilePath+"/"+newFileName;
		
		imgUploadUtil.saveFile(saveFilePath, filedata,newFileName);
		
		try {
			if(!StringUtil.isEmpty(quality)){
				EnumQuality q = Enum.valueOf(EnumQuality.class, quality);
				retUrl = ZipImg(newFileName,saveFilePath,q);
				delImg(saveFilePath+"/"+newFileName); //删除原版
			}
			
		} catch (Exception e) {
			throw new Exception("压缩图片出错，错误原因："+e.getMessage());
		}
		
		
		return retUrl.replace(path, httpurl);
	}
	
	
	
public static JSONObject doubleSave(MultipartFile filedata,String fileName,String userId,String useType) throws Exception{
		
		String newFileName = imgUploadUtil.createNewFileName(userId, fileName,filedata.getOriginalFilename());
		 
		String saveFilePath = path +"/"+userId+"/"+useType +"/"+DateUtil.getTodateString();
		
		String retUrl = saveFilePath+"/"+newFileName;
		
		JSONObject url = new JSONObject();
		imgUploadUtil.saveFile(saveFilePath, filedata,newFileName);
		url.put("url", retUrl.replace(path, httpurl));
		try {
			 for(EnumQuality e:EnumQuality.values()){//2
				 url.put(e.toString()+"_url", ZipImg(newFileName,saveFilePath,e).replace(path, httpurl));
		        }
			
		} catch (Exception e) {
			throw new Exception("压缩图片出错，错误原因："+e.getMessage());
		}
		return url;
	}
	
	
	
	
	
	public static String ZipImg(String newFileName,String saveFilePath,EnumQuality q) throws Exception{
		
			if(newFileName.endsWith(".gif")||newFileName.endsWith(".GIF")){
				gifUtil.entrance(q.scale, newFileName, saveFilePath, q.toString());
			}else{
				
				System.err.println(saveFilePath+"//"+newFileName);
				ImageUtils.fromFile(new File(saveFilePath+"/"+newFileName))
				
				.scale(q.scale)
				.quality(q.quality)
				.fixedGivenSize(true)
				.keepRatio(true)
				.toFile(new File(saveFilePath+"/"+q.toString()+"_"+newFileName));
			}
		return saveFilePath+"/"+q.toString()+"_"+newFileName;
	}
	
	
	
	
	
	
	public static void saveFile(String wholePath, MultipartFile filedata,String newFileName) throws Exception {

        File fileDir = new File(wholePath);
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(wholePath+"/"+newFileName);
            out.write(filedata.getBytes());
            
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("文件读写错误："+e.getMessage());
        }finally{
        	out.flush();
            out.close();
        }

    }
	
	public static void delImg(String path) throws Exception{
		File file = new File(path);
		if(!file.exists()){
			throw new Exception("文件不存在");
		}
		file.delete();
	}
	
	
	
	public static void delImgByUrl(String url) throws Exception{
		
	String Filepath = url.replace(httpurl, path);
	
	try {
		delImg(Filepath);
	} catch (Exception e) {
		throw new Exception("删除文件出错原因："+e.getMessage());
	}
	
		
	}
	
	
	
	
	
	
	public static  String createNewFileName(String userId,String fileName,String OriginalFilename){
		
//		if(!StringUtil.isEmpty(fileName)){
//			return fileName;
//		}
//		for (String fileformat : format) {
//			if(OriginalFilename.endsWith("."+fileformat)||OriginalFilename.endsWith("."+fileformat.toUpperCase())){
//				return XORencode.EncryptCode(userId+"_"+(fileName==null?"":fileName)+DateUtil.getCurrentTime(), XORencode.key)+"."+fileformat; //使用异或加密文件名
//			}
//		}
//		return XORencode.EncryptCode(userId+"_"+(fileName==null?"":fileName)+DateUtil.getCurrentTime(),XORencode.key)+"."+StringUtil.getLastStr(OriginalFilename, ".");
		return java.util.UUID.randomUUID().toString().replaceAll("-", "")+"."+StringUtil.getLastStr(OriginalFilename, ".");
	}

	public static void main(String[] args) {
		
	}

}
