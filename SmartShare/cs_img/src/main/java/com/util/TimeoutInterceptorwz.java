package com.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

//import com.wz.login.entity.User;


/**
 * @desc 拦截器
 * @author Administrator
 * 
 */
public class TimeoutInterceptorwz implements HandlerInterceptor {

	private final Logger logger = Logger.getLogger(TimeoutInterceptorwz.class);

	public String[] allowUrls;

	public void setAllowUrls(String[] allowUrls) {
		this.allowUrls = allowUrls;
	}

	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}

	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	/**
	 * @desc 拦截除静态文件URL意外的所有请求；其中部分URL还需要做是否是微信浏览器打开验证
	 * @author ygm
	 * @date 2016-03-18
	 */
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {

		String requestURL = request.getRequestURI().replace(
				request.getContextPath(), "");
		logger.debug("请求URL为：" + requestURL);

		String excludeURL = ContantsTool.EXCLUDE_URL;
		String[] url = excludeURL.split(";");
		// excludeURL 中存放了不限制浏览器打开的URL
		boolean flag = true;
		if (null != url && 0 < url.length) {
			for (int i = 0, n = url.length; i < n; i++) {
				// 相等说明不需要 判断登陆状态
				if (requestURL.equals(url[i])) {
					flag = false;
				}
			}
			 
		} 
//		if(flag){
//			if("/".equals(requestURL)){
//				User user = Utils.getSessionUser(request);
//				if(null!=user&!"".equals(user)){
//					request.getSession().setAttribute("username", user.getWz_user_acount());
//				} 
//				String basePath = request.getScheme() + "://" + request.getServerName()
//				+ ":" + request.getServerPort() + request.getContextPath();
//				request.getSession().setAttribute("basePath", basePath);
//				request.getRequestDispatcher("/WEB-INF/jsp/wz/index-article.jsp").forward(request,response);
//				 
//			}else{
//				User user = Utils.getSessionUser(request);
//				if(null!=user&!"".equals(user)){
//					request.setAttribute("username", user.getWz_user_acount());
//					return true;
//				}else{
//					return false;
//				}
//			}
//		}
//		return false;
		
		return true;
	}

}
