package com.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.apache.log4j.Logger;

//import com.wz.login.entity.User;


public class Utils {

	protected static final Logger tagInfo = Logger.getLogger("tagInfo");
	
	public static JsonConfig config = new JsonConfig();

	public static boolean isBland(String str){
		return null != str && !"".equals(str) ;
	}
	
	public static boolean isBland(Integer id){
		return null != id && 0 < id;
	}
	
	public static String requestGetParameter(HttpServletRequest request,String paramteter){
		return (request.getParameter(paramteter)==null||"".equals(request.getParameter(paramteter).trim()))?null:request.getParameter(paramteter);
	}
	
	public static void outJSONArray(List<?> list, HttpServletResponse response){
		JSONArray jsonArray = null;
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if(null != list && 0<list.size()){
				jsonArray=JSONArray.fromObject(list);
			}
			out.print(jsonArray);
//			System.err.println("jsonArray is :" + jsonArray);
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
	
	public static void outJSONObject(Object object, String callback, HttpServletResponse response){
		JSONObject json = null;
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if(null != object){
				json = JSONObject.fromObject(object);
				if(callback != null){
					out.write(callback +"(" + json + ")");
				}else{
					out.print(json);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
	
	public static void outJSONObject(JSONObject json, String callback, HttpServletResponse response){
		
		//config.addIgnoreFieldAnnotation(org.codehaus.jackson.annotate.JsonIgnore.class);
		//json=JSONObject.fromObject(json, config);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			if(null != json){
				if(callback != null){
//					out.write(callback +"(" + json + ")");
					out.write(json.toString());
				}else{
					out.print(json);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(null != out){
				out.flush();
				out.close();
			}
		}
	}
	/**
	 * 
	 * @param object
	 * @param response
	 * @param key
	 */
	public static void outputJSONObject(Object object,HttpServletResponse response,String key){
		 JSONObject json = new JSONObject();
			response.setContentType("text/html;charset=utf-8");
			PrintWriter out = null;
			json.element(key, object);
			try{
				out = response.getWriter();
				if (null != json) {
					out.write(json.toString());
				} else {
					out.print(json);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (null != out) {
					out.flush();
					out.close();
				}
			}
	}
//	 /**
//	  * 
//	  * @param request
//	  * @param user
//	  */
//	 public static void setsessionuser(HttpServletRequest request,User user){
//		 HttpSession session = request.getSession();
//		 session.setAttribute("user", user);
//	 }
//	
//	/**
//	 * 从session中获取当前登录的用户
//	 * @param request 
//	 * @return
//	 */
//	 public static User getSessionUser(HttpServletRequest request){
//		HttpSession session = request.getSession();
//		User user = (User)session.getAttribute("user");
//		return user;
//	} 
	 
//	 /**
//	  * 从session 中获取用户名
//	  * @param request
//	  * @return
//	  */
//	 public static void getusername(HttpServletRequest request){
//		 User user = getSessionUser(request);
//		 if(null!=user&!"".equals(user)){
//			 request.setAttribute("username", user.getWz_user_acount());
//		 }
//	 }
	
	 public static void setmobilecode(HttpServletRequest request,String code){
		 HttpSession session = request.getSession();
		 String mobile = request.getParameter("mobile");
		 session.setAttribute(mobile, code);
		 session.setMaxInactiveInterval(60*5);//失效时间5分钟
	 }
	 
	 public static String getmobilecode(HttpServletRequest request){
		 HttpSession session = request.getSession();
		 String mobile = request.getParameter("mobile");
		 String code = null;
		 try {
			 code = session.getAttribute(mobile).toString();
		} catch (Exception e) {
			code = null;
		}
		 return code;
	 }
	
}
