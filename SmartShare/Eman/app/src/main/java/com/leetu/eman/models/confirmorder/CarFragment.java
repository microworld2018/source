package com.leetu.eman.models.confirmorder;/**
 * Created by neo on 2016/9/23.
 */

import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseFragment;
import com.leetu.eman.models.confirmorder.beans.CarDetailBean;

/**
 * created by neo on 2016/9/23 16:52
 */
public class CarFragment extends BaseFragment {
    private ImageView ivCarIcon;
    private TextView tvCarNumber, tvBattery, tvStructure, tvCarType, tvPrice;

    @Override
    protected int getLayoutId() {
        return R.layout.item_frgament;
    }

    @Override
    protected void initView() {
        CarDetailBean carDetailBean = (CarDetailBean) getArguments().getSerializable("info");
        ivCarIcon = findViewById(R.id.iv_con_car_icon);
        tvCarNumber = findViewById(R.id.tv_con_car_number);
        tvBattery = findViewById(R.id.tv_con_car_battery);
        tvStructure = findViewById(R.id.tv_con_car_structure);
        tvCarType = findViewById(R.id.tv_con_car_type);
        tvPrice = findViewById(R.id.tv_con_car_price);
        saveData(carDetailBean);
    }

    @Override
    protected void loadData() {

    }

    //设置数据
    public void saveData(CarDetailBean carDetailBean) {
        Glide.with(getActivity())
                .load(carDetailBean.getAndroidModelPhoto())
                .thumbnail(0.1f)
                .into(ivCarIcon);
        tvCarNumber.setText(carDetailBean.getVehiclePlateId());
        tvBattery.setText(carDetailBean.getLifeMileage()+"公里");
        tvStructure.setText(carDetailBean.getSeatingNum() + "座 " + carDetailBean.getCasesNum() + "厢");
        tvCarType.setText(carDetailBean.getName());
        tvPrice.setText("¥" + carDetailBean.getBasePrice() + "/分钟+¥" + carDetailBean.getKmPrice() + "/公里");
    }
}
