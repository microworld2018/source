package com.leetu.eman.urls;


import com.leetu.eman.BuildConfig;

/**
 * Created by Administrator on 2016/4/8.
 */
public class URLS {
    /**
     * 主机地址
     */
    private static final boolean isDebug = BuildConfig.IS_DEBUG;
    public static final String HOST = isDebug ? "http://114.215.143.226:8081/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";
    public static final String IMG_HOST = isDebug ? "http://img.lee-tu.com:8888/cs_img/" : "http://img.lee-tu.com:8888/cs_img/";

//        public static final String HOST = isDebug ? "http://192.168.200.9:8080/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";//华西坤
//        public static final String HOST = isDebug ? "http://192.168.200.105:8080/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";
    //    public static final String HOST = isDebug ? "http://192.168.80.96:8080/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";
    //    public static final String HOST = isDebug ? "http://192.168.200.52:8080/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";
//        public static final String HOST = isDebug ? "http://192.168.200.211:8081/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";
//        public static final String HOST = isDebug ? "http://192.168.200.126:8081/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";//胡爱军
//        public static final String HOST = isDebug ? "http://192.168.146.2:8081/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";//胡爱军
//        public static final String HOST = isDebug ? "http://192.168.200.126:8081/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";//葛佳明
//        public static final String HOST = isDebug ? "http://192.168.200.213:8080/cs_ipc/" : "http://www.lee-tu.com:8081/cs_ipc/";//
// http://www.lee-tu.com:8081/cs_ipc/";//葛佳明
//        public static final String HOST = "http://www.lee-tu.com:8081/cs_ipc/";

    public static final String getURL(String api) {
        return HOST + api;
    }

//    public static final String getURL(String api) {
//        if (BuildConfig.IS_DEBUG) {
//            if (LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).getTestSelect() != null) {
//                if (!LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).getTestSelect().equals("")) {
//                    if (LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).getTestSelect().equals("1")) {
//                        return "http://www.lee-tu.com:8081/cs_ipc/" + api;
//                    } else {
//                        return "http://114.215.143.226:8081/cs_ipc/" + api;
//                    }
//                } else {
//                    return "http://114.215.143.226:8081/cs_ipc/" + api;
//                }
//            } else {
//                return "http://114.215.143.226:8081/cs_ipc/" + api;
//            }
//        } else {
//            return HOST + api;
//        }
//    }

    public static final String getImgURL(String api) {
        return IMG_HOST + api;
    }

    public static final String CODE = getURL("sub/sendCode");//获取验证码  0
    public static final String LOAD = getURL("sub/mobileCodeLogin");//登录  0
    public static final String POINT = getURL("place/appbranchdots");//获取网点  0
    public static final String POINT_DETAIL = getURL("car/getAvailablecarsByDotid");//获取网点详情  1
    public static final String RESERVATION_ORDER = getURL("app/reserve");//预约订单  1
    public static final String CANCEL_ORDER = getURL("order/cancelOrder");//取消预约订单   1

    public static final String USER_INFO = getURL("sub/appSubsinfo");//用户信息 1/0
    public static final String USER_EXIT = getURL("sub/exit");//退出 0
    public static final String ORDER_RECORD = getURL("order/subOrderRecords");//订单记录  1
    //    public static final String ORDER_DETAIL = getURL("order/viewOrderDetails");//订单详情  1 不用了
    public static final String ORDER_DETAIL = getURL("app/viewOrderDetails");//订单详情  1

    public static final String BALANCE_RECORD = getURL("account/getAccountByToken");//充值记录  1
    public static final String REBACK_CAR_DOTS = getURL("place/getReturnDotsByDotid");//可还车网点  1
    public static final String CHECK_ORDER = getURL("order/lookNowOrder");//检查当前订单  0/1
    public static final String CHECK_ORDER_POLLING = getURL("order/orderBilling");//实时更新当前订单  0/1


    public static final String OPEN_OR_CLOSE_DOOR = getURL("car/opOrClDoor");//开关车门  1
    public static final String ORDER_START_CAR = getURL("order/startCar");//开始用车  1
    public static final String CAR_COMMENT = getURL("comment/makeCarComment");//车辆评价  0
    public static final String CAR_DESTORY = getURL("carBreak/add");//车辆损坏上报
    public static final String CAR_ACCIDENT = getURL("accident/makeCarAccident");//车辆事故上报  1
    //    public static final String ORDER_DETAIL = getURL("order/backCarOrder");//即将还车
    public static final String UPDATA_RETURN_CAR_ADDRESS = getURL("order/updateReturnDot");//编辑还车网点  1
    public static final String RESET_DEFAULT_RETURN_CAR_ADDRESS = getURL("order/restoreDefaultDot");//恢复默认还车网点  1
    public static final String COMPLAINT = getURL("advice/makeFeedback");//一键投诉  1
    //    public static final String ORDER_OK_DETAIL = getURL("order/lookpayOrder");//生成订单详情  1 不用了
    public static final String ORDER_OK_DETAIL = getURL("app/lookpayOrder");//生成订单详情  1
    public static final String RETURN_CAR = getURL("place/backCar");//确认还车  1
    public static final String PAY = getURL("pay/payOrder");//确认支付  1
    public static final String PAY_SUCCESS = getURL("pay/alipayCallBackNew");//支付成功  1
    public static final String PAY_OVER_ORDER = getURL("pay/orderOver ");//支付成功  1
    public static final String WXPAY_COMFIRM_ORDER = getURL("pay/tradeResultsNew");//微信支付成功  1
    public static final String PAY_Err = getURL("pay/verificationPayment");//重复支付返回6001时   1

//    public static final String APPREG_LH = getURL("sub/appregLH");//实名认证      1.5.0
    public static final String APPREG_LH = getURL("sub/appregLHNew1.5.1");//实名认证      1.5.1
    public static final String USER_AVAILABLE_BILL = getURL("bill/getAvailableOrdersFee2Bill");//获取发票金额    1
    public static final String DRAW_BILL = getURL("bill/makeOutBill");//开发票  1
    public static final String USER_INFO_UPDATE = getURL("sub/upSubsinfo");//修改用户信息  1
    public static final String NOTIFI_MSG = getURL("message/getMessage");//通知消息   1

    public static final String COUPON_ALL = getURL("coupon/getCouponAllList");//优惠券裂表           1
    public static final String COUPON_USE = getURL("coupon/getCouponIsUedList");//可用优惠券裂表      1
    public static final String COUPON_CONFIRM = getURL("coupon/confirmCoupon");// 订单使用优惠券裂表  1
    public static final String REIGNER_COUPON = getURL("coupon/loginSendCoupon");// 注册发送优惠券

    public static final String SHARE_COUPON = getURL("coupon/shareSendCoupon");// 分享的优惠券   0

    public static final String ACTIVITY = getURL("activity/activity.html");// 活动页html
    public static final String ARGUMENT = getURL("agreement/subAgreement.html");// 用户协议页html
    public static final String SHARE = getURL("coupon/registrationCertificateCoupon.html");// 分享的优惠券页html     0


    public static final String IMG_UP = getImgURL("upload/imgUpload");//图片上传
    public static final String APP_VERSION = getURL("app/getApp");//版本升级 0


    //1.3
    public static final String MyWallet = getURL("app/getMyWallet");//我的钱包
    public static final String MyDeposit = getURL("app/getDeposit");//我的押金
    public static final String GET_RECHARG_AMOUNT= getURL("app/depositAmount");//充值押金页面需充值金额接口
    public static final String RECHARG_DEPOSIT = getURL("app/topUpDeposit");//会员押金充值
    public static final String DEPOSIT_ARGUMENT = getURL("deposit/xieyi.html");//会员押金充值协议
    public static final String RETURN_DEPOSIT = getURL("app/depositRefund");//退押金
    public static final String MyPeccancy = getURL("app/getAccident");//违章事故处理
    public static final String ACCIDENT_PAY= getURL("app/payAccident");//事故违章处理支付接口

    public static final String CONFIRM_CAR= getURL("app/confirmCar1.5.1");//确认订单
    //1.5.1
    public static final String STARTUPAD= getURL("appStart/getAppStart");//首页启动页广告图

    public static final String ACTIVITY_SHARE_COUPNE= getURL("coupon/weChatShareSendCoupon");//活动详情分享领券



}
