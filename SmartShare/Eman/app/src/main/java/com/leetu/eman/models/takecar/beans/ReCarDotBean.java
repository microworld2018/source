package com.leetu.eman.models.takecar.beans;/**
 * Created by jyt on 2016/9/27.
 */

import java.io.Serializable;

/**
 * created by neo on 2016/9/27 15:53
 */
public class ReCarDotBean implements Serializable {
    String address;
    String dotId;
    String name;
    double lat;
    double lng;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDotId() {
        return dotId;
    }

    public void setDotId(String dotId) {
        this.dotId = dotId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "ReCarDotBean{" +
                "address='" + address + '\'' +
                ", dotId='" + dotId + '\'' +
                ", name='" + name + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                '}';
    }
}
