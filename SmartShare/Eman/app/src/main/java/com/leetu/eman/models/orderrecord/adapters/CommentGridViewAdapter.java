package com.leetu.eman.models.orderrecord.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.leetu.eman.R;

import java.util.List;

/**
 * Created by neo on 2016/11/10.
 */

public class CommentGridViewAdapter extends BaseAdapter {
    private Context context;
    private List<String> pics;

    public CommentGridViewAdapter(Context context, List<String> pics) {
        this.context = context;
        this.pics = pics;
    }

    @Override
    public int getCount() {
        return pics.size();
    }

    @Override
    public Object getItem(int position) {
        return pics.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_orderdetail_image, null);
            viewHolder.imageView = (ImageView) convertView.findViewById(R.id.iv_orderdetail_image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Glide.with(context)
                .load(pics.get(position))
                .centerCrop()
                .thumbnail(0.1f)
                .placeholder(R.mipmap.ic_add)
                .error(R.mipmap.ic_add)
                .into(viewHolder.imageView);
        return convertView;
    }

    class ViewHolder {
        ImageView imageView;
    }
}
