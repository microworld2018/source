package com.leetu.eman.models.returncar;/**
 * Created by gn on 2016/10/10.
 */

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;


/**
 * created by neo on 2016/10/10 14:36
 */
public class ReturnCarPresenter extends BasePresenter implements ReturnCarCortract.UserAction {
    private Context context;
    private ReturnCarCortract.View returnCarListener;

    public ReturnCarPresenter(Context context, ReturnCarCortract.View returnCarListener) {
        this.context = context;
        this.returnCarListener = returnCarListener;
    }

    @Override
    public void lockCarDoor(final String lat, final String lng, final String carId, String orderId, final String dotId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.OPEN_OR_CLOSE_DOOR)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("lat", lat)
                    .addParam("lng", lng)
                    .addParam("carId", carId)
                    .addParam("orderId", orderId)
                    .addParam("state", "4")
                    .readTimeout(60000)
                    .writeTimeout(60000)
                    .connectTimeout(60000)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), returnCarListener)) {
                                LogUtils.e("gn", "锁车门" + response.getData());
                            } else {
                                if (response.getResultCode() != 206) {
                                    if (response.getResultCode() == 251) {
                                        returnCar(lat, lng, carId, dotId);
                                    } else {
                                        returnCarListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            returnCarListener.timeOutFail();
                        }
                    });
        } else {
            returnCarListener.showNetError("");
        }
    }

    @Override
    public void returnCar(String lat, String lng, String carId, String dotId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.RETURN_CAR)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("lat", lat)
                    .addParam("lng", lng)
                    .addParam("carId", carId)
                    .addParam("dotId", dotId)
                    .readTimeout(60000)
                    .writeTimeout(60000)
                    .connectTimeout(60000)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), returnCarListener)) {
                                LogUtils.e("gn", "确认还车" + response.getData());
                            } else {
                                if (response.getResultCode() != 206) {
                                    if (response.getResultCode() == 261) {
                                        returnCarListener.showReturnCar();
                                    } else {
                                        returnCarListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            returnCarListener.timeOutFail();
                        }
                    });
        } else {
            returnCarListener.showNetError("");
        }
    }
}
