package com.leetu.eman.models.usecar.bean;

/**
 * Created by Administrator on 2016/12/1.
 */

public class CheckUserOrderBean {
    private CheckUserOrderDot dot;
    private CheckUserOrderCar car;
    int isexist;
    double timePrice;
    double allTimePrice;
    String timeType;
    String orderNo;
    String allMile;
    double milePrice;
    double allMilePrice;
    String orderId;
    double allPrice;
    String allminutes;
    String alltime;
    int orderType;
    int payStatus = 2;

    public int getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(int payStatus) {
        this.payStatus = payStatus;
    }

    public CheckUserOrderDot getDot() {
        return dot;
    }

    public void setDot(CheckUserOrderDot dot) {
        this.dot = dot;
    }

    public CheckUserOrderCar getCar() {
        return car;
    }

    public void setCar(CheckUserOrderCar car) {
        this.car = car;
    }

    public int getIsexist() {
        return isexist;
    }

    public void setIsexist(int isexist) {
        this.isexist = isexist;
    }

    public double getTimePrice() {
        return timePrice;
    }

    public void setTimePrice(double timePrice) {
        this.timePrice = timePrice;
    }

    public double getAllTimePrice() {
        return allTimePrice;
    }

    public void setAllTimePrice(double allTimePrice) {
        this.allTimePrice = allTimePrice;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getAllMile() {
        return allMile;
    }

    public void setAllMile(String allMile) {
        this.allMile = allMile;
    }

    public double getMilePrice() {
        return milePrice;
    }

    public void setMilePrice(double milePrice) {
        this.milePrice = milePrice;
    }

    public double getAllMilePrice() {
        return allMilePrice;
    }

    public void setAllMilePrice(double allMilePrice) {
        this.allMilePrice = allMilePrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(double allPrice) {
        this.allPrice = allPrice;
    }

    public String getAllminutes() {
        return allminutes;
    }

    public void setAllminutes(String allminutes) {
        this.allminutes = allminutes;
    }

    public String getAlltime() {
        return alltime;
    }

    public void setAlltime(String alltime) {
        this.alltime = alltime;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    @Override
    public String toString() {
        return "CheckUserOrderBean{" +
                "dot=" + dot +
                ", car=" + car +
                ", isexist=" + isexist +
                ", timePrice=" + timePrice +
                ", allTimePrice=" + allTimePrice +
                ", timeType='" + timeType + '\'' +
                ", orderNo=" + orderNo +
                ", allMile='" + allMile + '\'' +
                ", milePrice=" + milePrice +
                ", allMilePrice=" + allMilePrice +
                ", orderId='" + orderId + '\'' +
                ", allPrice=" + allPrice +
                ", allminutes='" + allminutes + '\'' +
                ", alltime='" + alltime + '\'' +
                ", orderType=" + orderType +
                '}';
    }
}
