package com.leetu.eman.models.coupon;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gn.myanimpulltorefreshlistview.pulltoimpl.PullToRefreshBase;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.coupon.adapters.CouPonAdapter;
import com.leetu.eman.models.coupon.beans.CouponBean;
import com.gn.myanimpulltorefreshlistview.views.PullToRefreshListView;
import com.leetu.eman.models.returncar.PayOrderActivity;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by neo on 2016/12/17.
 */

public class CouponActivity extends BaseActivity implements View.OnClickListener, CouponContract.View {
    private TitleBar titleBar;
    private PullToRefreshListView pullToRefreshListView;
    private ListView listView;
    private CouponPresenter couponPresenter;
    private List<CouponBean> couponBeanList;
    private CouPonAdapter couPonAdapter;
    private View emptyView;
    private TextView tvAdd;
    public static final String MODE = "mode";
    public static final String ORDER_PRICE = "orderPrice";
    public static final String COUPON_BEAN = "couponBean";
    private String mode = "0", orderPrice;
    private LinearLayout ltAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_coupon);
        initView();
        loadData();
    }

    private void initView() {
        if (getIntent().getStringExtra(MODE) != null) {
            mode = getIntent().getStringExtra(MODE);
        }
        if (getIntent().getStringExtra(ORDER_PRICE) != null) {
            orderPrice = getIntent().getStringExtra(ORDER_PRICE);
        }
        couponPresenter = new CouponPresenter(this, this);
        titleBar = (TitleBar) findViewById(R.id.title_coupon);
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.pt_coupon_list);
        listView = pullToRefreshListView.getRefreshableView();
        emptyView = findViewById(R.id.lt_coupon_empty);
        ltAdd = (LinearLayout) findViewById(R.id.lt_coupon_add);
        tvAdd = (TextView) findViewById(R.id.tv_coupon_add);

        couponBeanList = new ArrayList<>();
        couPonAdapter = new CouPonAdapter(this, couponBeanList);

        if (mode != null) {
            if (mode.equals("1")) {
                ltAdd.setVisibility(View.GONE);
            } else {
                ltAdd.setVisibility(View.GONE);//功能未开通，先隐藏
            }

        }
        titleBar.setTitle("优惠券");
        titleBar.setLeftClickListener(this);
        tvAdd.setOnClickListener(this);
        listView.setAdapter(couPonAdapter);
        pullToRefreshListView.setEmptyView(emptyView);
        pullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                couponPresenter.loadData(true, false, mode, orderPrice);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                couponPresenter.loadData(false, false, mode, orderPrice);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mode.equals("1")) {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    position--;
                    bundle.putSerializable(COUPON_BEAN, couponBeanList.get(position));
                    intent.putExtras(bundle);
                    setResult(PayOrderActivity.COUPON_RESULT, intent);
                    CouponActivity.this.finish();
                }
            }
        });
    }

    private void loadData() {
        showLoading();
        couponPresenter.loadData(true, true, mode, orderPrice);
    }

    @Override
    protected void onRetryLoadData() {
        showProgressBar(true);
        couponPresenter.loadData(true, true, mode, orderPrice);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                CouponActivity.this.finish();
                break;
            case R.id.tv_coupon_add:
                showButtomToast("添加");
                break;
        }
    }

    @Override
    public void showLoadData(boolean isUp, List<CouponBean> coupon, String mode) {
        showContent();
        showProgressBar(false);
        pullToRefreshListView.onRefreshComplete();
        if (isUp) {
            couponBeanList.clear();
        }
        couponBeanList.addAll(coupon);
        couPonAdapter.notifyDataSetChanged();
    }

    @Override
    public void showEmptyView() {
        showContent();
        pullToRefreshListView.onRefreshComplete();
        couPonAdapter.notifyDataSetChanged();
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showContent();
        loadData();
    }

    @Override
    public void timeOutFail() {
        super.timeOutFail();
        pullToRefreshListView.onRefreshComplete();
    }
}
