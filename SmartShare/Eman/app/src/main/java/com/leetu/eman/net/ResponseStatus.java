package com.leetu.eman.net;

/**
 * Created by kevin on 16/3/16.
 */
public class ResponseStatus {


    private int resultCode;
    private String reason;
    private String data;
    private String resultMsg;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }


    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    @Override
    public String toString() {
        return "ResponseStatus{" +
                "resultCode='" + resultCode + '\'' +
                ", reason='" + reason + '\'' +
                ", data='" + data + '\'' +
                ", resultMsg='" + resultMsg + '\'' +
                '}';
    }
}
