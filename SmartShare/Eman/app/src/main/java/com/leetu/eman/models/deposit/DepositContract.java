package com.leetu.eman.models.deposit;

import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.deposit.bean.MyDepositBean;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;

/**
 * Created by lvjunfeng on 2017/4/5.
 */

public interface DepositContract {
    interface View extends BaseContract {
        void showLoad(boolean isUp,MyDepositBean myDepositBean);

        void showCheckUserRNA( UserInfoBean userInfoBean);

        void showReturnDeposit();

        void showUnCompleteOrder();
    }

    interface UserAction {
        void load(boolean isUp,boolean isFrist);

        void checkUserRNA();

        void retrunDeposit();
    }
}
