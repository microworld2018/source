package com.leetu.eman.beans;

/**
 * Created by lvjunfeng on 2017/2/7.
 */

public class CityBean {
    String name;
    double lat;
    double lon;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
