package com.leetu.eman.net;//package com.gzg.wuzhaitown.net;
//
//import android.util.Log;
//
//import com.gzg.wuzhaitown.utils.LogUtils;
//import com.zhy.http.okhttp.callback.StringCallback;
//
//
///**
// * 上传图片到服务器
// */
//public class UploadFileRepository{
//
//    private String tag = "UploadFileRepository";
//    private boolean isDebug = false;
//
//    public void loadData(Param param, final StringCallback callback) {
//        Log.i("sabrina", "2222222222222222222222222222222222222222222==" );
//
//        if (param != null) {
//            Log.i("sabrina", "00000000000000000000000000000==" );
//            String url = NetworkHelper.formatUrl(param.getHost(), param.getUri(), param.getAction());
//            Log.i("sabrina", "000000000000url=="+url );
//            if (isDebug) {
//                LogUtils.i(tag, "request URL:" + url);
//            }
//
//            if (callback != null) {
//                callback.onBefore();
//            }
//
//            HttpEngine.uploadFile(url, param.getRequestTag(), param.getFileParams(), param.getTimeout(), param.getParams(), new HttpEngine.ResponseCallback() {
//                @Override
//                public void onResponse(String response) {
//                    Log.i("sabrina", "33333333333333333333333333333333333==" );
//                    if (isDebug) {
//                        LogUtils.i(tag, "response data:" + response);
//                    }
//
//                    ResponseStatus status = JsonParser.getParsedData(response, ResponseStatus.class);
//                    if (callbackListener != null) {
//                        callbackListener.onSuccess(status);
//                        callbackListener.onAfter();
//                    }
//
//                }
//
//                @Override
//                public void onFailure(Exception error) {
//                    Log.i("sabrina", "666666666666666666666666+ error=="+error );
//                    if (isDebug) {
//                        if (error != null) {
//                            LogUtils.e(tag, error.getMessage(), error);
//                        } else {
//                            LogUtils.e(tag, "load data fail", error);
//                        }
//                    }
//
//                    if (callbackListener != null) {
//                        callbackListener.onFailure(error);
//                        callbackListener.onAfter();
//                    }
//                }
//            });
//        } else {
//
//            Exception exception = new RuntimeException("请求参数为空");
//            if (isDebug) {
//                LogUtils.e(tag, exception.getMessage(), exception);
//            }
//
//            if (callbackListener != null) {
//                callbackListener.onFailure(exception);
//                callbackListener.onAfter();
//            }
//        }
//
//    }
//
//
//    public void cancelRequest(Object tag) {
//        HttpEngine.getInstance().cancelRequest(tag);
//    }
//
//
//    public void setTag(String tag) {
//        this.tag = tag;
//        HttpEngine.getInstance().setTag(tag);
//    }
//
//    public void enableDebug(boolean isDebug) {
//        this.isDebug = isDebug;
//        HttpEngine.getInstance().enableDebug(isDebug);
//    }
//}
