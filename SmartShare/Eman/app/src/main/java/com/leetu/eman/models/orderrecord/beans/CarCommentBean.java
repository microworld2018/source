package com.leetu.eman.models.orderrecord.beans;

/**
 * Created by Administrator on 2016/11/10.
 */

public class CarCommentBean {
    String carImg;
    String isOk;
    String carFace;
    String carClean;

    public String getCarFace() {
        return carFace;
    }

    public void setCarFace(String carFace) {
        this.carFace = carFace;
    }

    public String getCarImg() {
        return carImg;
    }

    public void setCarImg(String carImg) {
        this.carImg = carImg;
    }

    public String getCarClean() {
        return carClean;
    }

    public void setCarClean(String carClean) {
        this.carClean = carClean;
    }

    public String getIsOk() {
        return isOk;
    }

    public void setIsOk(String isOk) {
        this.isOk = isOk;
    }
}
