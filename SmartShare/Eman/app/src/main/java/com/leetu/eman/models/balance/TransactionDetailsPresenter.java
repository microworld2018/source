package com.leetu.eman.models.balance;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.balance.beans.RechargeRecordList;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;


/**
 * Created by Administrator on 2016/11/5.
 */

public class TransactionDetailsPresenter extends BasePresenter implements TransactionDetailsContract.UserAction {
    private final TransactionDetailsContract.View banlanceDetailsListener;
    private Context context;

    private int page = 1;

    public TransactionDetailsPresenter(Context context, TransactionDetailsContract.View banlanceDetailsListener) {
        this.context = context;
        this.banlanceDetailsListener = banlanceDetailsListener;
    }


    @Override
    public void loadData(final boolean isUp, final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, banlanceDetailsListener)) {
                if (!isUp) {
                    page = 1;
                }
                HttpEngine.post().url(URLS.BALANCE_RECORD)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .addParam("pageNo", "" + page)
                        .tag(TransactionDetailsActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), banlanceDetailsListener)) {
                                    LogUtils.e("gn", "余额明细" + response.getData());
                                    RechargeRecordList rechargeRecordList = JsonParser.getParsedData(response.getData(), RechargeRecordList.class);
                                    if (rechargeRecordList != null) {
                                        if (rechargeRecordList.getCharges() != null) {
                                            if (rechargeRecordList.getCharges().size() != 0) {
                                                page++;
                                            }
                                            banlanceDetailsListener.showLoadData(isUp, rechargeRecordList.getCharges());
                                        }
                                    }

                                } else {
                                    if (response.getResultCode() != 206) {
                                        if (isFrist) {
                                            banlanceDetailsListener.contentFail();
                                        } else {
                                            banlanceDetailsListener.showFail(response.getResultCode(), response.getResultMsg());
                                        }
                                    }

                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                banlanceDetailsListener.timeOutFail();
                            }
                        });
            }
        } else {
            if (isFrist) {
                banlanceDetailsListener.contentFail();
            } else {
                banlanceDetailsListener.showFail(context.getResources().getString(R.string.net_error));
            }
        }
    }
}
