package com.leetu.eman.application;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Process;

import com.baidu.mapapi.SDKInitializer;
import com.leetu.eman.mygreedao.DaoMaster;
import com.leetu.eman.mygreedao.DaoSession;
import com.leetu.eman.utils.AppUtils;
import com.leetu.eman.utils.SharePreferenceUtils;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;

import java.util.Stack;

import cn.jpush.android.api.JPushInterface;


/**
 * Created by neo on 16/3/8.
 */
public class LeTravelApplication extends Application implements ActivityLifecycleCallbacks {
    public static SharePreferenceUtils sharePreferenceUtils;
    private static Stack<Activity> activityStack = new Stack<Activity>();
    ;
    private static LeTravelApplication singleton;
    private static DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        ///当前进程的名字
        String processName = AppUtils.getProcessName(Process.myPid());
        //判断当前进程为主进程，进行一系列初始化（推送采用的是多进程，涉及到oncreat()被多次调用问题）
        if (getPackageName().equals(processName)) {
            // 在使用 SDK 各组间之前初始化 context 信息，传入 ApplicationContext
            SDKInitializer.initialize(this);
            getSharePreferenceInstance(this);
//        LeakCanary.install(this);
            //异常收集初始化
//        CrashHandler.getInatance().init(this);
            //shareSDK初始化
            Config.REDIRECT_URL = "http://sns.whalecloud.com/sina2/callback";//微博默认
            UMShareAPI.get(this);//6.1.1完整版
            PlatformConfig.setWeixin("wxfdde3d5877da4cf8", "c665b25641e688e15d338d4280ccaed6");
            PlatformConfig.setSinaWeibo("1230382916", "69cdebb41647c6c2f664fff51c17b4dd");
            PlatformConfig.setQQZone("1105817787", "UvCcKoeQGic2jzyT");//已上线
            MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);

            //极光推送
            JPushInterface.setDebugMode(false);
            JPushInterface.init(this);
            setupDatabase();
        }

    }


    public static synchronized SharePreferenceUtils getSharePreferenceInstance(Context context) {
        if (sharePreferenceUtils == null) {
            sharePreferenceUtils = new SharePreferenceUtils(context);
        }
        return sharePreferenceUtils;
    }

    /**
     * 配置数据库
     */
    private void setupDatabase() {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "eman.db", null);
        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
    }

    public static DaoSession getDaoInstant() {
        return daoSession;
    }

    public static LeTravelApplication getInstance() {
        return singleton;
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    /**
     * get current Activity 获取当前Activity（栈中最后一个压入的）
     */
    public Activity currentActivity() {
        if (activityStack.size() > 0) {
            return activityStack.lastElement();
        }
        return null;
    }

    public Stack<Activity> getAllActivity() {
        return activityStack;
    }

    /**
     * add Activity 添加Activity到栈
     */
    public void addActivity(Activity activity) {

        activityStack.add(activity);
        for (int i = 0; i < activityStack.size(); i++) {
        }
    }

    /**
     * 结束所有Activity
     */
    public void finishAllActivity() {
        for (int i = 0, size = activityStack.size(); i < size; i++) {
            if (null != activityStack.get(i)) {
                activityStack.get(i).finish();
            }
        }
        activityStack.clear();
    }

    /**
     * 退出应用程序
     */
    public void AppExit() {
        try {
            finishAllActivity();
        } catch (Exception e) {
        }
    }

    /**
     * 结束指定的Activity
     */
    public void finishActivity(Activity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity.finish();
            activity = null;
        }
    }

    /**
     * 结束指定类名的Activity
     */
    public void finishActivity(Class<?> cls) {
        for (Activity activity : activityStack) {
            if (activity.getClass().equals(cls)) {
                finishActivity(activity);
            }
        }
    }

    /**
     * 指定某一个不结束，其他的都结束
     *
     * @param cls
     */
    public void finishAllExcept(Class<?> cls) {
        int size = activityStack.size();
        for (int i = 0; i < size; i++) {
            if (null != activityStack.get(i)) {
                if (activityStack.get(i).getClass().equals(cls)) {
                } else {
                    activityStack.get(i).finish();
                }
            }
        }
    }
}
