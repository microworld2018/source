package com.leetu.eman.base;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.leetu.eman.R;
import com.leetu.eman.utils.SharePreferenceUtils;
import com.leetu.eman.views.ErrorView;
import com.leetu.eman.views.MessageDialog;


/**
 * Created by kevin on 16/3/13.
 */
public abstract class BaseFragment extends Fragment {

    private ProgressDialog mProgressDialog;
    private Toast mToast;

    private View mContentView;
    private ErrorView mErrorView;
    protected SharePreferenceUtils sharePreferenceUtils;
    private PullToRefreshCompleteListener pullToRefreshCompleteListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mToast = Toast.makeText(getContext(), "", Toast.LENGTH_SHORT);
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(false);
        sharePreferenceUtils = new SharePreferenceUtils(getActivity());
    }

    @Nullable
    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContentView = inflater.inflate(getLayoutId(), container, false);
        mErrorView = (ErrorView) mContentView.findViewById(R.id.empty_view);
        if (mErrorView != null) {
            mErrorView.setOnRetryListener(new ErrorView.RetryListener() {
                @Override
                public void onRetry() {
                    onRetryLoadData();
                }
            });
        }
        initView();
        setListener();
        return mContentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    protected <T> T findViewById(int id) {
        return (T) mContentView.findViewById(id);
    }

    protected abstract int getLayoutId();

    protected abstract void initView();

    protected abstract void loadData();

    protected View getContentView() {
        return null;
    }

    protected void setListener() {
    }

    /**
     * 显示加载进度条
     */
    protected void showLoading() {

        if (mErrorView == null) {
            return;
        }

        if (mErrorView.getVisibility() != View.VISIBLE) {
            mErrorView.setVisibility(View.VISIBLE);
        }

        mErrorView.showLoading();
    }

    /**
     * 显示用户数据为空时的提示信息
     *
     * @param title    提示标题
     * @param subTitle 提示子标题
     */
    protected void showBlankMessage(String title, String subTitle) {

        if (mErrorView == null) {
            return;
        }

        mErrorView.setImageResource(R.mipmap.ic_exception_blank_task);
        mErrorView.showEmptyView();

        if (mErrorView.getVisibility() != View.VISIBLE) {
            mErrorView.setVisibility(View.VISIBLE);
        }

        if (title != null && !"".equals(title.trim())) {
            mErrorView.showTitle(true);
            mErrorView.setTitle(title);
        } else {
            mErrorView.showTitle(false);
        }

        if (subTitle != null && !"".equals(subTitle.trim())) {
            mErrorView.showSubtitle(true);
            mErrorView.setSubtitle(subTitle);
        } else {
            mErrorView.showTitle(false);
        }

        if (getContentView() != null) {
            getContentView().setVisibility(View.GONE);
        }


    }

    /**
     * 重新加载数据，当点击重试按钮时会调用此方法
     */
    protected void onRetryLoadData() {

    }


    /**
     * 显示加载用户数据出错时的提示信息
     *
     * @param title    主标题
     * @param subTitle 子标题
     */
    protected void showErrorMessage(String title, String subTitle) {
        if (mErrorView == null) {
            return;
        }

        mErrorView.setImageResource(R.mipmap.error_view_cloud);
        mErrorView.showEmptyView();

        if (mErrorView.getVisibility() != View.VISIBLE) {
            mErrorView.setVisibility(View.VISIBLE);
        }

        if (title != null && !"".equals(title.trim())) {
            mErrorView.showTitle(true);
            mErrorView.setTitle(title);
        } else {
            mErrorView.showTitle(false);
        }

        if (subTitle != null && !"".equals(subTitle.trim())) {
            mErrorView.showSubtitle(true);
            mErrorView.setSubtitle(subTitle);
        } else {
            mErrorView.showTitle(false);
        }

        if (getContentView() != null) {
            getContentView().setVisibility(View.GONE);
        }

    }


    /**
     * 显示内容视图
     */
    protected void showContent() {
        if (getContentView() != null && getContentView().getVisibility() != View.VISIBLE) {
            getContentView().setVisibility(View.VISIBLE);
        }
        if (mErrorView != null) {
            mErrorView.setVisibility(View.GONE);
        }

    }


    protected void showProgressBar(boolean show) {
        showProgressBar(show, "");
    }

    protected void showProgressBar(boolean show, String message) {
        if (show) {
            mProgressDialog.setMessage(message);
            mProgressDialog.show();
        } else {
            mProgressDialog.hide();
        }
    }


    /**
     * 显示吐司
     *
     * @param msg 吐司中所要显示的提示信息
     */
    protected void showButtomToast(String msg) {
        mToast.setText(msg);
        mToast.setGravity(Gravity.NO_GRAVITY, 0, 0);
        mToast.show();
    }

    /**
     * 显示吐司
     *
     * @param messageId 吐司中所要显示的提示信息的id
     */
    protected void showButtomToast(int messageId) {
        mToast.setText(messageId);
        mToast.setGravity(Gravity.NO_GRAVITY, 0, 0);
        mToast.show();
    }

    /**
     * 居中显示吐司
     *
     * @param msg 吐司中所要显示的提示信息
     */

    protected void showMiddleToast(String msg) {
        mToast.setText(msg);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }


    protected void showDialog(String title, String msg, DialogInterface.OnClickListener clickOk) {

        MessageDialog dialog = new MessageDialog(getActivity());
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setBtnNo("取消");
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.setCancelListener(null);
        dialog.show();
    }


    protected void showDialog(String title, String msg, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel, DialogInterface.OnCancelListener onListener) {
        MessageDialog dialog = new MessageDialog(getActivity());
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setBtnNo("取消");
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.setCancelListener(clickCancel);
        dialog.setOnCancelListener(onListener);
        dialog.show();

    }

    protected void showDialog(String title, String msg, String ok, String cancel, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel, DialogInterface.OnCancelListener onListener) {
        MessageDialog dialog = new MessageDialog(getActivity());
        dialog.setTitle(title);
        dialog.setBtnYes(ok);
        dialog.setBtnNo(cancel);
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.setCancelListener(clickCancel);
        dialog.setOnCancelListener(onListener);
        dialog.show();
    }


    protected void showOkDialog(String title, String msg) {
        MessageDialog dialog = new MessageDialog(getActivity());
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setMessage(msg);
        dialog.setOkListener(null);
        dialog.show();
    }


    protected void showOkDialog(String title, String msg, DialogInterface.OnClickListener clickOk) {

        MessageDialog dialog = new MessageDialog(getActivity());
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.show();
    }

    protected void showNetError() {
        mToast.setText(getString(R.string.net_error));
        mToast.setGravity(Gravity.NO_GRAVITY, 0, 0);
        mToast.show();
    }

    protected void showDataError() {
        mToast.setText(getString(R.string.data_error));
        mToast.setGravity(Gravity.NO_GRAVITY, 0, 0);
        mToast.show();
    }

    protected void showTimeOutError() {
        mToast.setText(getString(R.string.time_out));
        mToast.setGravity(Gravity.NO_GRAVITY, 0, 0);
        mToast.show();
    }

    public void setPullToRefreshCompleteListener(PullToRefreshCompleteListener pullToRefreshCompleteListener) {
        this.pullToRefreshCompleteListener = pullToRefreshCompleteListener;
    }

    public interface PullToRefreshCompleteListener {
        void pullToRefreshComplete();
    }

    /**
     * 停止刷新
     */
    public class GetDataTask extends AsyncTask<Void, Void, String> {

        //后台处理部分
        @Override
        protected String doInBackground(Void... params) {
            // Simulates a background job.
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            String str = "Added after refresh...I add";
            return str;
        }

        //这里是对刷新的响应，可以利用addFirst（）和addLast()函数将新加的内容加到LISTView中
        //根据AsyncTask的原理，onPostExecute里的result的值就是doInBackground()的返回值
        @Override
        protected void onPostExecute(String result) {
            //在头部增加新添内容
//            mListItems.addFirst(result);

            //通知程序数据集已经改变，如果不做通知，那么将不会刷新mListItems的集合
//            adapter.notifyDataSetChanged();
            // Call onRefreshComplete when the list has been refreshed.
            if (pullToRefreshCompleteListener != null) {
                pullToRefreshCompleteListener.pullToRefreshComplete();
            }
            super.onPostExecute(result);
        }
    }
}
