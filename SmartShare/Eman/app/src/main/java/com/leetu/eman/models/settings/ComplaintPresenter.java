package com.leetu.eman.models.settings;/**
 * Created by gn on 2016/10/11.
 */

import android.content.Context;


import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.utils.PicUpUtils;

import java.util.List;

/**
 * created by neo on 2016/10/11 09:19
 */
public class ComplaintPresenter extends BasePresenter implements ComplaintContract.UserAction, PicUpUtils.PicListener {
    private Context context;
    private ComplaintContract.View complaintListener;
    private PicUpUtils picUpUtils;
    private String feedbackDesc, contactType;

    public ComplaintPresenter(Context context, ComplaintContract.View complaintListener) {
        this.context = context;
        this.complaintListener = complaintListener;
        picUpUtils = new PicUpUtils();
    }

    @Override
    public void upFileData(List<String> files, String feedbackDesc, String contactType) {
        this.feedbackDesc = feedbackDesc;
        this.contactType = contactType;

        if (files == null) {
            picOk("");
        } else {
            picUpUtils.setPicListener(this);
            picUpUtils.upPic(context, files);
        }
    }

    @Override
    public void picOk(String picUrls) {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, complaintListener)) {
                HttpEngine.post().url(URLS.COMPLAINT)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .addParam("feedbackImg", picUrls)
                        .addParam("feedbackDesc", feedbackDesc)
                        .addParam("contactType", contactType)
                        .tag(ComplaintActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                LogUtils.e("gn", "一键投诉" + response.getData());
                                if (checkCode(response.getResultCode(), complaintListener)) {
                                    complaintListener.showupFileData();
                                } else {
                                    if (response.getResultCode() != 206) {
                                        complaintListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                complaintListener.timeOutFail();
                            }
                        });
            }
        } else {
            complaintListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }

    @Override
    public void picFail(String msg) {
        LogUtils.e("gn", "失败啦传回数据");
        complaintListener.showFail(msg);
    }

    @Override
    public void picNetError() {
        complaintListener.contentFail();
    }
}
