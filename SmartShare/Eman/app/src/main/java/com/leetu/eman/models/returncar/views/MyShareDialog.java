package com.leetu.eman.models.returncar.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.leetu.eman.R;

import static com.leetu.eman.R.id.iv_share_qq;
import static com.leetu.eman.R.id.iv_share_qqzone;
import static com.leetu.eman.R.id.iv_share_weibo;


/**
 * Created by 郭宁 on 2015/5/28.
 */
public class MyShareDialog extends Dialog implements View.OnClickListener {

    private final Context context;
    private ImageView ivShareWechat, ivShareWechatCircle, ivShareWeibo, ivShareQQ, ivShareQQZone;
    private ClickListenerInterface clickListenerInterface;

    public MyShareDialog(Context context) {

        super(context, R.style.selectorDialog);
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_share, null);
        setContentView(view);

        ivShareWechat = (ImageView) findViewById(R.id.iv_share_wechat);
        ivShareWechatCircle = (ImageView) findViewById(R.id.iv_share_wechat_circle);
        ivShareWeibo = (ImageView) findViewById(iv_share_weibo);
        ivShareQQ = (ImageView) findViewById(iv_share_qq);
        ivShareQQZone = (ImageView) findViewById(iv_share_qqzone);
        //设置按钮监听
        ivShareWechat.setOnClickListener(this);
        ivShareWechatCircle.setOnClickListener(this);
        ivShareWeibo.setOnClickListener(this);
        ivShareQQ.setOnClickListener(this);
        ivShareQQZone.setOnClickListener(this);
        Window dialogWindow = getWindow();

        dialogWindow.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        dialogWindow.setWindowAnimations(R.style.mystyle);  //添加动画
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();
        lp.width = (int) (d.widthPixels * 1);
        dialogWindow.setAttributes(lp);
    }

    public void setClicklistener(ClickListenerInterface clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }

    @Override
    public void onClick(View v) {
        clickListenerInterface.startShare();
        switch (v.getId()) {
            case R.id.iv_share_wechat_circle:
                clickListenerInterface.wechatCircle();
                break;

            case R.id.iv_share_wechat:
                clickListenerInterface.wechat();
                break;

            case iv_share_weibo:
                clickListenerInterface.weibo();
                break;

            case iv_share_qq:
                clickListenerInterface.QQ();
                break;
            case iv_share_qqzone:
                clickListenerInterface.qqZone();
                break;
        }
        MyShareDialog.this.cancel();
    }

    public interface ClickListenerInterface {
        void wechat();

        void wechatCircle();

        void weibo();

        void QQ();

        void qqZone();

        void startShare();
    }

}
