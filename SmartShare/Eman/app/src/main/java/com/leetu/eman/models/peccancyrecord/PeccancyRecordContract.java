package com.leetu.eman.models.peccancyrecord;


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.deposit.bean.DepositWxPayBean;
import com.leetu.eman.models.peccancyrecord.beans.PeccancyRecordList;
import com.leetu.eman.models.returncar.beans.PayBean;

/**
 * Created by jyt on 2016/9/18.
 */

public interface PeccancyRecordContract {
    interface View extends BaseContract {
        void showLoad(PeccancyRecordList recordList, boolean isUp);

        void showWxPay(DepositWxPayBean wxPayBean);

        void showAliPay(PayBean wxPayBean);

    }

    interface UserAction {
        void load(boolean isUp, boolean isFrist);

        void pay(String orderId, String payType);

    }
}
