package com.leetu.eman.models.personalcenter;/**
 * Created by gn on 2016/9/29.
 */

import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;

/**
 * created by neo on 2016/9/29 13:52
 */
public interface PersonalCenterContract {
    interface View extends BaseContract {
        void showLoad(UserInfoBean userInfoBean);

        void showSubmit();
    }

    interface UserAction {
        void load();

        void submit(String nickname, String nickstatus);
    }
}
