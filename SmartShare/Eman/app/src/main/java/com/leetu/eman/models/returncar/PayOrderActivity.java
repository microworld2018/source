package com.leetu.eman.models.returncar;/**
 * Created by jyt on 2016/9/21.
 */

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.coupon.CouponActivity;
import com.leetu.eman.models.coupon.beans.CouponBean;
import com.leetu.eman.models.returncar.beans.BaoXianBean;
import com.leetu.eman.models.returncar.beans.OrederCreateDetailBean;
import com.leetu.eman.models.returncar.beans.PayBean;
import com.leetu.eman.models.returncar.beans.WXPayBean;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.payutils.PayResult;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.TitleBar;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

import java.util.List;

import static com.leetu.eman.models.coupon.CouponActivity.COUPON_BEAN;
import static com.leetu.eman.wxapi.WXPayEntryActivity.ACTION_NAME;


/**
 * created by neo on 2016/9/21 10:23
 * 支付订单
 */
public class PayOrderActivity extends BaseActivity implements View.OnClickListener, PayOrderContract.View {
    private TitleBar titleBar;
    private Button btPay;
    private TextView tvMileage, tvTime, tvAllMileageCost, tvAllTimeCost, tvFactCost, tvCoupon, tvBaoXianName, tvBaoXianCost;
    private PayOrderPresenter payOrderPresenter;
    private OrederCreateDetailBean orederCreateDetailBean;
    private String resultStatus;
    private LinearLayout ltAliPay, ltWechatPay, ltPayCoupon, ltPayBaoXian;
    private CheckBox cbAliPay, cbWechatPay;
    private RelativeLayout rlCheXian, rlBuChongXian;
    private String payFlags = "0";//支付标记
    private static final int SDK_PAY_FLAG = 1;
    // 定义一个变量，来标识是否退出
    private static boolean isExit = false;
    public static final int COUPON_REQUEST = 1010;
    public static final int COUPON_RESULT = 1011;
    private CouponBean couponBean;
    private IWXAPI api;
    private WXPayBean.WxBean.OrderBean wxOrderBean;
    private Intent intent;
    private MyBroadcastReceiver myBroadcastReceiver;
    private String orderNo;

    Handler exitHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };
    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    showProgressBar(true);
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                    if (TextUtils.equals(resultStatus, "9000")) {
                        payOrderPresenter.paySuccess("9000", resultInfo);
                        LogUtils.e("lv", "resultInfo:" + resultInfo);
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            showButtomToast("支付结果确认中");
                        } else if (TextUtils.equals(resultStatus, "6001")) {
                            if (orederCreateDetailBean.getOrderId() != null) {
                                payOrderPresenter.errorPay("6001", orederCreateDetailBean.getOrderId());
                            }
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            showProgressBar(false);
                            showButtomToast("支付失败");
                            payFlags = "1";
                            Intent intent = new Intent(PayOrderActivity.this, PayFailActivity.class);
                            startActivity(intent);
                        }
                    }
                    break;
                }
                default:
                    break;
            }
        }

        ;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_order);
        api = WXAPIFactory.createWXAPI(this, "wxfdde3d5877da4cf8");
        api.registerApp("wxfdde3d5877da4cf8");
        initView();
        loadData();
        setProgressType(false);
        registWxPayBroadcast();
    }

    protected void initView() {
        if (getIntent().getStringExtra(PAY_FLAGS) != null) {
            payFlags = getIntent().getStringExtra(PAY_FLAGS);
        }
        titleBar = (TitleBar) findViewById(R.id.title_payorder);
        btPay = (Button) findViewById(R.id.bt_pay);
        tvMileage = (TextView) findViewById(R.id.tv_pay_allmileage);
        tvTime = (TextView) findViewById(R.id.tv_pay_alltime);
        tvAllMileageCost = (TextView) findViewById(R.id.tv_pay_allmileagecost);
        tvAllTimeCost = (TextView) findViewById(R.id.tv_pay_alltimecost);
        ltPayCoupon = (LinearLayout) findViewById(R.id.lt_payorder_coupon);
        tvCoupon = (TextView) findViewById(R.id.tv_payorder_coupon);
        tvFactCost = (TextView) findViewById(R.id.tv_pay_factcost);
        ltWechatPay = (LinearLayout) findViewById(R.id.lt_pay_wechat);
        cbWechatPay = (CheckBox) findViewById(R.id.cb_pay_wechat);
        ltAliPay = (LinearLayout) findViewById(R.id.lt_pay_ali);
        cbAliPay = (CheckBox) findViewById(R.id.cb_pay_ali);

        ltPayBaoXian = (LinearLayout) findViewById(R.id.ll_pay_baoxian);

        payOrderPresenter = new PayOrderPresenter(this, this);

        titleBar.setTitle("支付订单");
        titleBar.hideLeftLayout();
        btPay.setOnClickListener(this);
        ltWechatPay.setOnClickListener(this);
        cbWechatPay.setOnClickListener(this);
        ltAliPay.setOnClickListener(this);
        cbAliPay.setOnClickListener(this);
        ltPayCoupon.setOnClickListener(this);
    }


    private void loadData() {
        showLoading();
        payOrderPresenter.loadOrder(payFlags,"0");
    }

    //注册微信支付成功失败广播
    private void registWxPayBroadcast() {
        IntentFilter myIntentFilter = new IntentFilter();
        myIntentFilter.addAction(ACTION_NAME);
        myBroadcastReceiver = new MyBroadcastReceiver();
        registerReceiver(myBroadcastReceiver, myIntentFilter);
    }

    @Override
    protected void onRetryLoadData() {
        loadData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                PayOrderActivity.this.finish();
                break;
            case R.id.bt_pay:
                showProgressBar(true);
                if (orederCreateDetailBean.getAllPrice() == 0) {//当总金额为零的时候就不调用第三方支付
                    payOrderPresenter.payOverOrder(orederCreateDetailBean.getOrderId());
                } else {
                    if (cbWechatPay.isChecked()) {
                        if (isInstalled(PayOrderActivity.this, "com.tencent.mm")) {
                            payOrderPresenter.pay(orederCreateDetailBean.getOrderId(), "5");
                        } else {
                            showProgressBar(false);
                            showButtomToast("未安装微信应用，请安装后支付");
                        }
                    } else if(cbAliPay.isChecked()){
                        payOrderPresenter.pay(orederCreateDetailBean.getOrderId(), "3");
                    }
                }
                break;
            case R.id.lt_pay_ali:
            case R.id.cb_pay_ali:
                if (cbAliPay.isChecked()) {
                    cbWechatPay.setChecked(false);
                } else {
                    cbWechatPay.setChecked(false);
                    cbAliPay.setChecked(true);
                }
                break;
            case R.id.lt_pay_wechat:
            case R.id.cb_pay_wechat:
                if (cbWechatPay.isChecked()) {
                    cbAliPay.setChecked(false);
                } else {
                    cbAliPay.setChecked(false);
                    cbWechatPay.setChecked(true);
                }
                break;
            case R.id.lt_payorder_coupon:
                Intent intent = new Intent(PayOrderActivity.this, CouponActivity.class);
                intent.putExtra(CouponActivity.MODE, "1");
                intent.putExtra(CouponActivity.ORDER_PRICE, orederCreateDetailBean.getAllPrice() + "");
                startActivityForResult(intent, COUPON_REQUEST);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == COUPON_REQUEST) {
            if (resultCode == COUPON_RESULT) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    couponBean = (CouponBean) bundle.getSerializable(COUPON_BEAN);
                    if (couponBean != null) {
                        showProgressBar(true);
                        payOrderPresenter.useCoupon(couponBean.getCouponId(), orederCreateDetailBean.getOrderId());
                    }
                }
            }
        }
    }

    @Override
    public void showLoadOrder(OrederCreateDetailBean orederCreateDetailBean) {
        showContent();
        showProgressBar(false);
        if (orederCreateDetailBean != null) {
            this.orederCreateDetailBean = orederCreateDetailBean;

            if (orederCreateDetailBean.getInsurance() != null && orederCreateDetailBean.getInsurance().size() > 0) {
                List<BaoXianBean> baoXianDatas = orederCreateDetailBean.getInsurance();
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 30);
                ltPayBaoXian.setLayoutParams(params);
                ltPayBaoXian.removeAllViews();
                for (int i = 0; i < baoXianDatas.size(); i++) {
                    View viewItem = LayoutInflater.from(this).inflate(R.layout.layout_paybaoxianitem, null);
                    rlCheXian = (RelativeLayout) viewItem.findViewById(R.id.rl_orderpay_chexian);
                    tvBaoXianName = (TextView) viewItem.findViewById(R.id.tv_order_baoxianname);
                    tvBaoXianCost = (TextView) viewItem.findViewById(R.id.tv_pay_chexiancost);
                    tvBaoXianName.setText(baoXianDatas.get(i).getInsuranceName());
                    tvBaoXianCost.setText("￥" + baoXianDatas.get(i).getInsuranceFee());
                    ltPayBaoXian.addView(viewItem);
                }
            }

            if (couponBean != null) {
                tvCoupon.setTextColor(getResources().getColor(R.color.main_red));
                tvCoupon.setText("-¥" + (int) (couponBean.getPrice()));
            } else {
                if (orederCreateDetailBean.getCouponPrice() == 0) {
                } else {
                    tvCoupon.setTextColor(getResources().getColor(R.color.main_red));
                    tvCoupon.setText("-¥" + doubleTrans(orederCreateDetailBean.getCouponPrice()));
                }
            }
            tvMileage.setText("" + orederCreateDetailBean.getAllMile());
            tvTime.setText(orederCreateDetailBean.getAlltime());
            tvAllMileageCost.setText("¥" + orederCreateDetailBean.getAllMilePrice());
            tvAllTimeCost.setText("¥" + orederCreateDetailBean.getAllTimePrice());
            tvFactCost.setText("¥" + orederCreateDetailBean.getAllPrice());
            if (orederCreateDetailBean.getAllPrice() == 0) {
                cbAliPay.setEnabled(false);
                cbAliPay.setChecked(false);
                ltAliPay.setEnabled(false);
                cbWechatPay.setEnabled(false);
                cbWechatPay.setChecked(false);
                ltWechatPay.setEnabled(false);
            } else {
                ltAliPay.setEnabled(true);
                cbAliPay.setEnabled(true);
                cbAliPay.setChecked(true);
                cbWechatPay.setEnabled(true);
                cbWechatPay.setChecked(false);
                ltWechatPay.setEnabled(true);
            }
        }
    }

    //小数点后为0时去除，只显示整数
    public static String doubleTrans(double num) {
        if (num % 1.0 == 0) {
            return String.valueOf((long) num);
        }
        return String.valueOf(num);
    }

    //支付宝支付
    private void aLiPay(final String payString) {
        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask(PayOrderActivity.this);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payString, true);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    @Override
    public void showPay(PayBean payBean) {
        if (payBean != null && payBean.getAlipay() != null) {
            LogUtils.e("gn", "支付宝支付" + payBean.getAlipay().toString());
            aLiPay(payBean.getAlipay().getAliparameter());
        }
    }

    @Override
    public void showWxPay(WXPayBean payBean) {
        if (payBean != null && payBean.getWx() != null) {
            showProgressBar(false);
            wxOrderBean = payBean.getWx().getOrder();
            orderNo = payBean.getOrderNo();
            if (wxOrderBean != null) {
                PayReq req = new PayReq();
                req.appId = wxOrderBean.getAppid();
                req.partnerId = wxOrderBean.getPartnerid();
                req.prepayId = wxOrderBean.getPrepayid();
                req.nonceStr = wxOrderBean.getNoncestr();
                req.timeStamp = wxOrderBean.getTimestamp();
                req.packageValue = "Sign=WXPay";//固定形式，
                req.sign = wxOrderBean.getSign();
                req.extData = "app data"; // optional
                // 调起支付
                api.sendReq(req);
                LogUtils.e("gn", "微信支付接口回调调起支付");
            }
        }
    }

    @Override
    public void showUseCoupn() {
        payFlags = "1";
        tvCoupon.setTextColor(getResources().getColor(R.color.main_red));
        payOrderPresenter.loadOrder(payFlags,"1");
    }

    @Override
    public void showPayOverOrder() {
        showPaySuccess();
    }

    @Override
    public void showPayFail() {
        showProgressBar(false);
        orderNo = null;
        Intent intent2 = new Intent(PayOrderActivity.this, PayFailActivity.class);
        startActivity(intent2);
    }

    @Override
    public void showPaySuccess() {
        showProgressBar(false);
        orderNo = null;
        showButtomToast("支付成功");
        goToReturnSuccessActivity();
    }

    public void goToReturnSuccessActivity() {
        if (intent == null) {
            intent = new Intent(PayOrderActivity.this, ReturnCarSuccessActivity.class);
        }
        startActivity(intent);
        PayOrderActivity.this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
        }
        return true;
    }

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            // 利用handler延迟发送更改状态信息
            exitHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            finish();
            LeTravelApplication.getInstance().AppExit();
        }
    }

    @Override
    public void loginOk() {
        LeTravelApplication.getInstance().finishAllExcept(MainActivity.class);
        super.loginOk();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(orderNo)) {//用于微信支付完返回APP刷新检查订单状态
//            showProgressBar(true);
            payOrderPresenter.confirmWxPay(orderNo);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(PayOrderActivity.class);
        unregisterReceiver(myBroadcastReceiver);
    }

    class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
//            if (action.equals(ACTION_NAME)) {
//                int err_code = intent.getIntExtra("err_code", 2);
//                if (err_code == 0) {//成功,确认支付
//                    showProgressBar(true);
//                    payOrderPresenter.confirmWxPay(orderNo);
//                } else if (err_code == -1) {
////                    Intent intent2 = new Intent(PayOrderActivity.this, PayFailActivity.class);
////                    startActivity(intent2);
//                    showProgressBar(true);
//                    payOrderPresenter.confirmWxPay(orderNo);
//                } else if (err_code == -2) {
//                    showButtomToast("支付已取消！");
//                }
//            }
        }
    }
}
