package com.leetu.eman.models.currentorder.carreport;/**
 * Created by jyt on 2016/9/20.
 */

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.currentorder.CurrentOrderActivity;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.views.PhotoFragment;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * created by neo on 2016/9/20 11:02
 * 车辆评价
 */
public class ReportCarActivity extends BaseActivity implements View.OnClickListener, ReportCarContract.View, RadioGroup.OnCheckedChangeListener {
    private TitleBar titleBar;
    private LinearLayout ltReportContent;
    private ReportCarPresenter reportCarPresenter;
    private Button btUpData;
    private CurrentOrderBean currentOrderBean;
    private RadioGroup rgIsOk, rgIsOkFace, rgIsClear;
    private String isOk, carFace, carClean;
    private RadioButton rbOk, rbNoOk, rbOkFace, rbNoFace, rbBadFace, rbClear, rbSomethings, rbStink;
    private PhotoFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_car);
        initView();
    }

    protected void initView() {

        if (getIntent().getExtras().getSerializable(CurrentOrderActivity.CURRENT_ORDER) != null) {
            currentOrderBean = (CurrentOrderBean) getIntent().getExtras().getSerializable(CurrentOrderActivity.CURRENT_ORDER);
        }
        titleBar = (TitleBar) findViewById(R.id.title_car);
        ltReportContent = (LinearLayout) findViewById(R.id.lt_reportcar_content);
        btUpData = (Button) findViewById(R.id.bt_report_car_up);
        rgIsOk = (RadioGroup) findViewById(R.id.rg_isok);
        rgIsOkFace = (RadioGroup) findViewById(R.id.rg_isok_face);
        rgIsClear = (RadioGroup) findViewById(R.id.rg_isclear);
        rbOk = (RadioButton) findViewById(R.id.rb_report_ok);
        rbNoOk = (RadioButton) findViewById(R.id.rb_report_nook);
        rbOkFace = (RadioButton) findViewById(R.id.rb_report_okface);
        rbNoFace = (RadioButton) findViewById(R.id.rb_report_noface);
        rbBadFace = (RadioButton) findViewById(R.id.rb_report_badface);
        rbClear = (RadioButton) findViewById(R.id.rb_report_clear);
        rbSomethings = (RadioButton) findViewById(R.id.rb_report_somethings);
        rbStink = (RadioButton) findViewById(R.id.rb_report_stink);

        reportCarPresenter = new ReportCarPresenter(this, this);

        savePhotoFragment();

        titleBar.setTitle("车辆评价");
        titleBar.setLeftClickListener(this);
        btUpData.setOnClickListener(this);
        rgIsOk.setOnCheckedChangeListener(this);
    }

    void savePhotoFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.
                beginTransaction();
        fragment = new PhotoFragment();
        //加到Activity中
        fragmentTransaction.add(R.id.lt_reportcar_fragment_content, fragment);
        //加到后台堆栈中，有下一句代码的话，点击返回按钮是退到Activity界面，没有的话，直接退出Activity
        //后面的参数是此Fragment的Tag。相当于id
//        fragmentTransaction.addToBackStack("fragment1");
        //记住提交
        fragmentTransaction.commit();
    }


    protected void loadData() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                ReportCarActivity.this.finish();
                break;
            case R.id.bt_report_car_up:
                if (fragment.factSelected.size() != 0) {
                    List<String> list = new ArrayList<>();
                    for (int i = 0; i < fragment.factSelected.size(); i++) {
                        list.add(fragment.factSelected.get(i).getOriginalPath());
                    }
                    if (currentOrderBean != null) {
                        reportCarPresenter.upData(currentOrderBean.getOrderId(), currentOrderBean.getCar().getCarId(), isOk, carFace, carClean, list);
                    }
                }

                break;
        }
    }

    @Override
    public void tokenOld() {

    }

    @Override
    public void showUpData() {

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.rg_isok:
                if (checkedId == rbOk.getId()) {
                    isOk = "1";
                } else if (checkedId == rbNoOk.getId()) {
                    isOk = "2";
                }
                break;
            case R.id.rg_isok_face:
                if (checkedId == rbOkFace.getId()) {
                    carFace = "1";
                } else if (checkedId == rbNoFace.getId()) {
                    carFace = "2";
                } else if (checkedId == rbBadFace.getId()) {
                    carFace = "3";
                }
                break;
            case R.id.rg_isclear:
                if (checkedId == rbClear.getId()) {
                    carClean = "1";
                } else if (checkedId == rbSomethings.getId()) {
                    carClean = "2";
                } else if (checkedId == rbStink.getId()) {
                    carClean = "3";
                }
                break;
        }
    }
}
