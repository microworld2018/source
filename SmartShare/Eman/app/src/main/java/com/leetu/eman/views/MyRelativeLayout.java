package com.leetu.eman.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

import com.leetu.eman.utils.LogUtils;

/**
 * Created by lvjunfeng on 2017/4/27.
 */

public class MyRelativeLayout extends RelativeLayout implements GestureDetector.OnGestureListener{
    private int verticalMinDistance = 10;

    private int minVelocity = 0;

    private  Context mContext;
    private GestureDetector gestureDetector;
    public MyRelativeLayout(Context context) {
        this(context,null);
    }

    public MyRelativeLayout(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public MyRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        gestureDetector = new GestureDetector(context, this);
        this.mContext = context;
    }


    @Override
    public boolean onDown(MotionEvent e) {
        LogUtils.e("lv", "onDown");
//        if(onTouchListener != null) {
//            onTouchListener.down();
//        }
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        LogUtils.e("lv", "onFling");
        if(onTouchListener != null) {
            onTouchListener.scrollDown();
        }
//        if (e1.getX()
//                - e2.getX() > verticalMinDistance && Math.abs(velocityX) > minVelocity) {
//            LogUtils.e("pingan", "向左手势");
//
////            if(onTouchListener != null) {
////                onTouchListener.scrollLeft();
////            }
//
//        } else if ((e2.getX() - e1.getX() > verticalMinDistance && Math.abs(velocityX) > minVelocity)) {
//
////            LogUtils.e("pingan", "向右手势");
////            if(onTouchListener != null) {
////                onTouchListener.scrollRight();
////            }
//
//        } else if (e1.getY()
//                - e2.getY() > verticalMinDistance && Math.abs(velocityY) > minVelocity) {
//            LogUtils.e("pingan", "向上手势");
////            if(onTouchListener != null) {
////                onTouchListener.scrollUp();
////            }
//
//        } else if ((e2.getY() - e1.getY() > verticalMinDistance && Math.abs(velocityY) > minVelocity)) {
//            float dis = e2.getY() - e1.getY();
//            LogUtils.e("lv", "向下手势"+dis+"--"+velocityY);
//            if(onTouchListener != null) {
//                onTouchListener.scrollDown();
//            }
//
//        }
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        return true;
    }
private OnTouchListener onTouchListener;

    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.onTouchListener = onTouchListener;
    }

    public interface OnTouchListener{
        void scrollDown();

//        void scrollUp();
//
//        void scrollLeft();
//
//        void scrollRight();

        void down();
    }
}
