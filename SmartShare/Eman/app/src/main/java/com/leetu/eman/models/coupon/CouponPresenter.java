package com.leetu.eman.models.coupon;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.coupon.beans.CouponBean;
import com.leetu.eman.models.coupon.beans.CouponListBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2016/12/17.
 */

public class CouponPresenter extends BasePresenter implements CouponContract.UserAction {
    private Context context;
    private CouponContract.View couponListener;
    private int page = 1;

    public CouponPresenter(Context context, CouponContract.View couponListener) {
        this.context = context;
        this.couponListener = couponListener;
    }

    @Override
    public void loadData(final boolean isUp, final boolean isFrist, final String mode, String orderPrice) {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (isUp) {
                page = 1;
            }
            if (mode.equals("0")) {
                normalCoupon(isUp, isFrist, mode);
            } else if (mode.equals("1")) {
                abnormalCoupon(isUp, isFrist, mode, orderPrice);
            }
        } else {
            if (isFrist) {
                couponListener.contentFail();
            } else {
                couponListener.showFail(context.getResources().getString(R.string.net_error));
            }

        }
    }


    private void normalCoupon(final boolean isUp, final boolean isFrist, final String mode) {
        HttpEngine.post().url(URLS.COUPON_ALL)
                .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                .addParam("currentPage", page + "")
                .execute(new HttpEngine.ResponseCallback() {
                    @Override
                    public void onResponse(ResponseStatus response) {
                        if (checkCode(response.getResultCode(), couponListener)) {
                            CouponListBean couponListBean = JsonParser.getParsedData(response.getData(), CouponListBean.class);
                            if (couponListBean != null) {
                                if (couponListBean.getCoupon() != null ) {
                                    page++;
                                    couponListener.showLoadData(isUp, fromatData(couponListBean), mode);
                                }
                            }
                        } else {
                            if (response.getResultCode() != 206) {
                                if (response.getResultCode() == 266) {//暂无优惠券
                                    couponListener.showEmptyView();
                                } else if (isFrist) {
                                    if (response.getResultCode() == 214) {
                                    } else {
                                        couponListener.contentFail();
                                    }
                                } else {
                                    couponListener.showFail(response.getResultCode(), response.getResultMsg());
                                }

                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception error) {
                        if (isFrist) {
                            couponListener.contentFail();
                        } else {
                            couponListener.timeOutFail();
                        }
                    }
                });
    }

    private void abnormalCoupon(final boolean isUp, final boolean isFrist, final String mode, String orderPrice) {
        HttpEngine.post().url(URLS.COUPON_USE)
                .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                .addParam("currentPage", page + "")
                .addParam("orderPrice", orderPrice)
                .execute(new HttpEngine.ResponseCallback() {
                    @Override
                    public void onResponse(ResponseStatus response) {
                        if (checkCode(response.getResultCode(), couponListener)) {
                            CouponListBean couponListBean = JsonParser.getParsedData(response.getData(), CouponListBean.class);
                            if (couponListBean != null) {
                                if (couponListBean.getCoupon() != null ) {
                                    couponListener.showLoadData(isUp, fromatData(couponListBean), mode);
                                    page++;
                                }
                            }
                        } else {
                            if (response.getResultCode() != 206) {
                                if (response.getResultCode() == 266) {//暂无优惠券
                                    couponListener.showEmptyView();
                                } else if (isFrist) {
                                    if (response.getResultCode() == 214) {
                                    } else {
                                        couponListener.contentFail();
                                    }
                                } else {
                                    couponListener.showFail(response.getResultCode(), response.getResultMsg());
                                }

                            }
                        }
                    }

                    @Override
                    public void onFailure(Exception error) {
                        if (isFrist) {
                            couponListener.contentFail();
                        } else {
                            couponListener.timeOutFail();
                        }
                    }
                });
    }

    private List<CouponBean> fromatData(CouponListBean couponListBean) {
        if (couponListBean != null)
            for (int i = 0; i < couponListBean.getCoupon().size(); i++) {
                try {
                    if (stringToLong(couponListBean.getCoupon().get(i).getEndValidityTime()) < stringToLong(couponListBean.getCoupon().get(i).getDateTime())) {
                        couponListBean.getCoupon().get(i).setIsUsed("2");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        return couponListBean.getCoupon();
    }

    // string类型转换为date类型
    // strTime要转换的string类型的时间，formatType要转换的格式yyyy-MM-dd HH:mm:ss//yyyy年MM月dd日
    // HH时mm分ss秒，
    // strTime的时间格式必须要与formatType的时间格式相同
    public static Date stringToDate(String strTime, String formatType)
            throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat(formatType);
        Date date = null;
        date = formatter.parse(strTime);
        return date;
    }

    // string类型转换为long类型
    // strTime要转换的String类型的时间
    // formatType时间格式
    // strTime的时间格式和formatType的时间格式必须相同
    public static long stringToLong(String strTime)
            throws ParseException {
        Date date = stringToDate(strTime, "yyyy-MM-dd"); // String类型转成date类型
        if (date == null) {
            return 0;
        } else {
            long currentTime = dateToLong(date); // date类型转成long类型
            return currentTime;
        }
    }

    // date类型转换为long类型
    // date要转换的date类型的时间
    public static long dateToLong(Date date) {
        return date.getTime();
    }
}
