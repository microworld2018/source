package com.leetu.eman.models.balance;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.views.TitleBar;


/**
 * Created by Administrator on 2016/11/25.
 */

public class ReChargeResaultActivity extends BaseActivity implements View.OnClickListener {
    private ImageView ivResault;
    private TitleBar rechargeTitle;
    private LinearLayout rechargeSuccess;
    private Button btnEquiry, btnContinueRecharges, btnContinueRechargef;
    public static final String RESAULT = "result";
    private boolean resault;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recharge_resault);
        initView();
    }

    protected void initView() {
        resault = getIntent().getBooleanExtra(RESAULT, false);

        rechargeTitle = (TitleBar) findViewById(R.id.recharge_title);
        rechargeSuccess = (LinearLayout) findViewById(R.id.recharge_success);
        ivResault = (ImageView) findViewById(R.id.iv_recharge_resault);
        btnEquiry = (Button) findViewById(R.id.btn_equiry);
        btnContinueRecharges = (Button) findViewById(R.id.btn_continue_recharges);
        btnContinueRechargef = (Button) findViewById(R.id.btn_continue_rechargef);

        if (resault) {
            rechargeTitle.setTitle("充值成功");
            rechargeSuccess.setVisibility(View.VISIBLE);
            btnContinueRechargef.setVisibility(View.GONE);

            Glide.with(ReChargeResaultActivity.this)
                    .load(R.mipmap.iv_recharge_successful)
                    .into(ivResault);
        } else {
            rechargeTitle.setTitle("充值失败");
            rechargeSuccess.setVisibility(View.GONE);
            btnContinueRechargef.setVisibility(View.VISIBLE);

            Glide.with(ReChargeResaultActivity.this)
                    .load(R.mipmap.iv_recharge_fail)
                    .into(ivResault);
        }

        btnEquiry.setOnClickListener(this);
        rechargeTitle.setLeftClickListener(this);
        btnContinueRecharges.setOnClickListener(this);
        btnContinueRechargef.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                ReChargeResaultActivity.this.finish();
                break;
            case R.id.btn_equiry:
            case R.id.btn_continue_recharges:
            case R.id.btn_continue_rechargef:
                ReChargeResaultActivity.this.finish();
                break;
        }
    }
}
