package com.leetu.eman.mygreedao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Unique;

/**
 * Created by handsome on 2016/4/8.
 */
@Entity
public class NavigationRecord {

    public static final int TYPE_CART = 0x01;
    public static final int TYPE_LOVE = 0x02;

    //不能用int
    @Id(autoincrement = true)
    private Long id;
    @Unique
    private String startAddress;
    private String endAddress;
    private String registerTime;
    private String userName;

    @Generated(hash = 1078954143)
    public NavigationRecord(Long id, String startAddress, String endAddress, String registerTime,
                            String userName) {
        this.id = id;
        this.startAddress = startAddress;
        this.endAddress = endAddress;
        this.registerTime = registerTime;
        this.userName = userName;
    }

    @Generated(hash = 1215059859)
    public NavigationRecord() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getResgisterTime() {
        return registerTime;
    }

    public void setResgisterTime(String registerTime) {
        this.registerTime = registerTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "NavigationRecord{" +
                "id=" + id +
                ", startAddress='" + startAddress + '\'' +
                ", endAddress='" + endAddress + '\'' +
                ", resgisterTime='" + registerTime + '\'' +
                ", userName='" + userName + '\'' +
                '}';
    }

    public String getRegisterTime() {
        return this.registerTime;
    }

    public void setRegisterTime(String registerTime) {
        this.registerTime = registerTime;
    }
}
