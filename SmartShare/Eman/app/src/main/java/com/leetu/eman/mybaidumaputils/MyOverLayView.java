package com.leetu.eman.mybaidumaputils;/**
 * Created by jyt on 2016/9/21.
 */

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.leetu.eman.R;

/**
 * created by neo on 2016/9/21 15:53
 */
public class MyOverLayView extends LinearLayout {
    private CheckBox cbCount;
    private CheckBox cbCar;

    public MyOverLayView(Context context) {
        super(context);


    }

    public MyOverLayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public MyOverLayView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    public MyOverLayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attributeSet) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setOrientation(VERTICAL);
        inflater.inflate(R.layout.item_overlay_view, this, true);
        cbCount = (CheckBox) findViewById(R.id.cb_up_count);
        cbCar = (CheckBox) findViewById(R.id.cb_down_car);
    }

    public void setBackgroundColor() {

    }

    public void useOrNoUse(boolean isUse) {
        if (isUse) {
            cbCount.setChecked(true);
            cbCar.setChecked(true);
        } else {
            cbCount.setChecked(false);
            cbCar.setChecked(false);
        }
    }

    public void setCount(String count) {
        cbCount.setText(count);
    }
}
