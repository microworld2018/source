package com.leetu.eman.models.currentorder.carreport;/**
 * Created by gn on 2016/10/10.
 */

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.utils.PicUpUtils;

import java.util.List;

/**
 * created by neo on 2016/10/10 15:43
 */
public class ReportDestoryPresenter extends BasePresenter implements ReportDestoryContract.UserAction, PicUpUtils.PicListener {
    private Context context;
    private ReportDestoryContract.View reportDestoryListener;
    private String subscriberId, subscriberName, orderId, orderNumber, carId, carNumber, breakTypeFacade, breakTypeTyre, breakTypeDecoration, breakTypeGlass, breakTypeOther, breakTypeOdor, breakDesc;
    private List<String> breakImg;
    private PicUpUtils picUpUtils;

    public ReportDestoryPresenter(Context context, ReportDestoryContract.View reportDestoryListener) {
        this.context = context;
        this.reportDestoryListener = reportDestoryListener;
        picUpUtils = new PicUpUtils();
    }

    @Override
    public void upData(String subscriberId, String subscriberName, String orderId, String orderNumber, String carId, String carNumber, String breakTypeFacade, String breakTypeTyre, String breakTypeDecoration, String breakTypeGlass, String breakTypeOther, String breakTypeOdor, String breakDesc, List<String> breakImg) {
        this.subscriberId = subscriberId;//会员ID
        this.subscriberName = subscriberName;//会员名称
        this.orderId = orderId;//订单ID
        this.orderNumber = orderNumber;//订单号
        this.carId = carId;//汽车ID
        this.carNumber = carNumber;//车牌号
        this.breakTypeFacade = breakTypeFacade;//汽车外表损坏
        this.breakTypeTyre = breakTypeTyre;//汽车轮胎损坏
        this.breakTypeDecoration = breakTypeDecoration;//汽车内饰损坏
        this.breakTypeGlass = breakTypeGlass;//汽车玻璃损坏
        this.breakTypeOther = breakTypeOther;//汽车其他损坏
        this.breakTypeOdor = breakTypeOdor;//汽车内幕异味
        this.breakDesc = breakDesc;//损坏描述
        this.breakImg = breakImg;//图片地址，多张已“|”分割
        picUpUtils.setPicListener(this);
        if (breakImg != null) {
            if (breakImg.size() > 0) {
                picUpUtils.upPic(context, breakImg);
            } else {
                picOk("");
            }
        }

    }

    @Override
    public void picOk(String picUrls) {
        if (NetworkHelper.isNetworkConnect(context)) {
            LogUtils.e("gn", "提交");
            HttpEngine.post().url(URLS.CAR_DESTORY)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("subscriberId", subscriberId)
                    .addParam("subscriberName", subscriberName)
                    .addParam("orderId", orderId)
                    .addParam("orderNumber", orderNumber)
                    .addParam("carId", carId)
                    .addParam("carNumber", carNumber)
                    .addParam("breakTypeFacade", breakTypeFacade)
                    .addParam("breakTypeTyre", breakTypeTyre)
                    .addParam("breakTypeDecoration", breakTypeDecoration)
                    .addParam("breakTypeGlass", breakTypeGlass)
                    .addParam("breakTypeOther", breakTypeOther)
                    .addParam("breakTypeOdor", breakTypeOdor)
                    .addParam("breakDesc", breakDesc)
                    .addParam("breakImg", picUrls)
                    .execute(new HttpEngine.ResponseCallback() {
                                 @Override
                                 public void onResponse(ResponseStatus response) {
                                     LogUtils.e("gn", "提交损坏" + response.getData());
                                     if (checkCode(response.getResultCode(), reportDestoryListener)) {
                                         reportDestoryListener.showUpData();
                                     } else {
                                         if (response.getResultCode() != 206) {
                                             reportDestoryListener.showFail(response.getResultMsg());
                                         }
                                     }
                                 }

                                 @Override
                                 public void onFailure(Exception error) {
                                     reportDestoryListener.timeOutFail();
                                 }
                             }
                    );
        } else {
            reportDestoryListener.contentFail();
        }
    }

    @Override
    public void picFail(String msg) {
        reportDestoryListener.showFail(msg);
    }

    @Override
    public void picNetError() {
        reportDestoryListener.contentFail();
    }

}
