package com.leetu.eman.models.takecar;/**
 * Created by jyt on 2016/9/19.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.bumptech.glide.Glide;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.confirmorder.ConfirmOrderActivity;
import com.leetu.eman.models.currentorder.CurrentOrderActivity;
import com.leetu.eman.models.takecar.beans.ReCarDotBean;
import com.leetu.eman.models.takecar.beans.TakeCarBean;
import com.leetu.eman.models.takecar.views.MyOtherDialog;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.TitleBar;

/**
 * created by neo on 2016/9/19 13:34
 * 取车地点
 */
public class TakeCarActivity extends BaseActivity implements View.OnClickListener, TakeCarContract.View, BaseActivity.MyLocationLisenter {
    private TitleBar titleBar;
    private MyOtherDialog myOtherDialog;
    private LinearLayout ltOpenDoor, ltWhistle, ltCloseDoor;
    private TakeCarPresenter takeCarPresenter;
    private String dotId, carId, backDotId;
    private ImageView ivNavigation, ivCar;
    private TextView tvName, tvAddress, tvId, tvType, tvTime;
    private ReCarDotBean reCarDotBean;
    public static final String TAKE_CAR_DOT = "takeCarDot";
    private TakeCarBean takeCarBean;
    private int time = 600;
    private MyCountDownTimer myTimer;
    private Button btCancelOrder;
    public static final int TAKE_CAR_CODE = 9999;
    public static final String TAKE_CAR_CANCLE = "take_car_cancle";
    public static final int AUTO_CANCLE = 1000;
    private int type = 1;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_car);
        initView();
    }

    protected void initView() {
        intent = getIntent();
        titleBar = (TitleBar) findViewById(R.id.title_takecar);
        ltOpenDoor = (LinearLayout) findViewById(R.id.lt_open_cardoor);
        ltWhistle = (LinearLayout) findViewById(R.id.lt_whistle_car);
        ltCloseDoor = (LinearLayout) findViewById(R.id.lt_close_cardoor);
        ivNavigation = (ImageView) findViewById(R.id.iv_le_navigation);
        tvName = (TextView) findViewById(R.id.tv_takecar_name);
        tvAddress = (TextView) findViewById(R.id.tv_takecar_address);
        ivCar = (ImageView) findViewById(R.id.iv_takecar_pic);
        tvId = (TextView) findViewById(R.id.tv_takecar_id);
        tvType = (TextView) findViewById(R.id.tv_takecar_types);
        tvTime = (TextView) findViewById(R.id.tv_takecar_time);
        btCancelOrder = (Button) findViewById(R.id.bt_takecar_cancel);

        takeCarPresenter = new TakeCarPresenter(this, this);

        titleBar.setTitle("取车地点");
        titleBar.setLeftClickListener(this);
        ltOpenDoor.setOnClickListener(this);
        ltWhistle.setOnClickListener(this);
        ltCloseDoor.setOnClickListener(this);
        ivNavigation.setOnClickListener(this);
        btCancelOrder.setOnClickListener(this);
        TakeCarActivity.this.setMyLocationLisenter(this);
        loadData();
    }


    private void loadData() {
        showLoading();
        takeCarPresenter.loadData();
    }

    @Override
    public void showLoadData(TakeCarBean takeCarBean) {
        if (takeCarBean != null) {
            showProgressBar(false);
            showContent();
            this.takeCarBean = takeCarBean;
            saveData();
        }

    }

    @Override
    protected void onRetryLoadData() {
        loadData();
    }

    @Override
    public void getLocation(BDLocation location) {
        if (type == 1) {
            takeCarPresenter.openOrCloseDoor(takeCarBean.getOrderId(), "3", takeCarBean.getCar().getCarId(), "" + location.getLatitude(), "" + location.getLongitude());
        } else if (type == 2) {
            navigationLocationApp(location.getLatitude(), location.getLongitude(), location.getBuildingName(), reCarDotBean.getLat(), reCarDotBean.getLng(), reCarDotBean.getName(), 2);
        }
    }


    @Override
    public void showopenOrCloseDoorOk() {
        showProgressBar(false);
        showButtomToast("鸣笛中...");
    }

    @Override
    public void showStartUseCar() {
        myTimer.cancel();
        showProgressBar(false);
        Intent intent = new Intent(TakeCarActivity.this, CurrentOrderActivity.class);
        startActivity(intent);
    }

    @Override
    public void showCancelOrder() {
        showProgressBar(false);
        Intent intent = new Intent();
        intent.putExtra(TAKE_CAR_CANCLE, 1);
        intent.setClass(TakeCarActivity.this, CancelOrderActivity.class);
        startActivityForResult(intent, TAKE_CAR_CODE);
    }


    //==计时器=====
    class MyCountDownTimer extends CountDownTimer {
        /**
         * @param millisInFuture    表示以毫秒为单位 倒计时的总数
         *                          <p>
         *                          例如 millisInFuture=1000 表示1秒
         * @param countDownInterval 表示 间隔 多少微秒 调用一次 onTick 方法
         *                          <p>
         *                          例如: countDownInterval =1000 ; 表示每1000毫秒调用一次onTick()
         */
        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onFinish() {
            tvTime.setText("0");
            setResult(TakeCarActivity.AUTO_CANCLE, intent);
            showButtomToast("订单已取消");
            TakeCarActivity.this.finish();
            this.cancel();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            time--;
            LogUtils.e("gn", time + "");
            tvTime.setText(formatTime(time));
        }
    }

    String formatTime(int time) {
        String timeString = "";
        if (time > 60) {
            int i = time / 60;
            String second;
            if ((time - i * 60) >= 10) {
                second = "" + (time - i * 60);
            } else {
                second = "0" + (time - i * 60);
            }
            timeString = i + ":" + second;
        } else {
            if (time > 0) {
                timeString = time + "s";
            }
        }
        return timeString;
    }

    //=======================
    //设置数据
    void saveData() {
        if (takeCarBean != null) {
            if (takeCarBean.getDot() != null) {
                reCarDotBean = takeCarBean.getDot();
                tvName.setText(takeCarBean.getDot().getName());
                tvAddress.setText(takeCarBean.getDot().getAddress());
            }
            if (takeCarBean.getCar() != null) {
                Glide.with(this).load(takeCarBean.getCar().getAndroidModelPhoto()).into(ivCar);
                tvId.setText(takeCarBean.getCar().getVehiclePlateId());
                tvType.setText(takeCarBean.getCar().getName());
            }
//            time = takeCarBean.getTiming();//计时规则 分
            time = takeCarBean.getResidualTime();//实际剩余时间 s
            LogUtils.e("gn", "剩余时间" + time + "s");
//            if (time == 0) {
//                time = 30 * 60;
//            } else {
//                time = time * 60;
//            }
//            if(time == 0) {
//                time = 2;
//            }
            myTimer = new MyCountDownTimer(time * 1000, 1000);
            myTimer.start();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                if (myTimer != null) {
                    myTimer.cancel();
                }
                TakeCarActivity.this.finish();
                break;
            case R.id.layout_right:

                break;
            case R.id.lt_open_cardoor:
                showDialog("温馨提示", "打开车门将开始正式计费\n是否确认？", "确定", "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showProgressBar(true);
//                        reGetLocation();
                        takeCarPresenter.startUseCar(takeCarBean.getOrderId(), takeCarBean.getCar().getCarId());
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                break;
            case R.id.lt_whistle_car:
                showProgressBar(true);
                reGetLocation();
                type = 1;
                break;
            case R.id.lt_close_cardoor:
                reGetLocation();
                break;
            case R.id.iv_le_navigation:
                type = 2;
                reGetLocation();
                break;
            case R.id.bt_takecar_cancel:
                showProgressBar(true);
                takeCarPresenter.cancelOrder(takeCarBean.getOrderId());
                break;
        }
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (myTimer != null) {
            myTimer.cancel();
            myTimer = null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TAKE_CAR_CODE) {
            if (resultCode == CancelOrderActivity.AGAIN) {
                Intent intent = new Intent();
                setResult(CancelOrderActivity.AGAIN, intent);
                TakeCarActivity.this.finish();
            } else if (resultCode == CancelOrderActivity.LEFT_BACK) {
                Intent intent = new Intent();
                setResult(ConfirmOrderActivity.RESULT_FINISH, intent);
                TakeCarActivity.this.finish();
            } else if (resultCode == CancelOrderActivity.GO_HOME) {
                Intent intent = new Intent();
                setResult(CancelOrderActivity.GO_HOME, intent);
                TakeCarActivity.this.finish();
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (myTimer != null) {
                myTimer.cancel();
                TakeCarActivity.this.finish();
            }
        }
        return true;
    }
}
