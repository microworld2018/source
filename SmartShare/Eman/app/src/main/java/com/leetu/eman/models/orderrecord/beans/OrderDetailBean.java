package com.leetu.eman.models.orderrecord.beans;

import com.leetu.eman.models.returncar.beans.BaoXianBean;

import java.util.List;

/**
 * Created by Administrator on 2016/11/10.
 */

public class OrderDetailBean {

    CarCommentBean carCommentMap;
    String returndot;
    String takedot;
    String milePrice;
    String timeType;
    String allTimePrice;
    String allminutes;
    String returnCarTime;
    String createOrderTime;
    String orderId;
    String coupon;
    String timePrice;
    String carName;
    String allPrice;
    String carNumber;
    String allMile;
    String isexist;
    String orderNo;
    String alltime;
    String allMilePrice;
    List<BaoXianBean> insurance;


    public String getReturnCarTime() {
        return returnCarTime;
    }

    public void setReturnCarTime(String returnCarTime) {
        this.returnCarTime = returnCarTime;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getAllMile() {
        return allMile;
    }

    public void setAllMile(String allMile) {
        this.allMile = allMile;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getMilePrice() {
        return milePrice;
    }

    public void setMilePrice(String milePrice) {
        this.milePrice = milePrice;
    }

    public String getTakedot() {
        return takedot;
    }

    public void setTakedot(String takedot) {
        this.takedot = takedot;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CarCommentBean getCarCommentMap() {
        return carCommentMap;
    }

    public void setCarCommentMap(CarCommentBean carCommentMap) {
        this.carCommentMap = carCommentMap;
    }

    public String getReturndot() {
        return returndot;
    }

    public void setReturndot(String returndot) {
        this.returndot = returndot;
    }

    public String getAlltime() {
        return alltime;
    }

    public void setAlltime(String alltime) {
        this.alltime = alltime;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getIsexist() {
        return isexist;
    }

    public void setIsexist(String isexist) {
        this.isexist = isexist;
    }

    public String getTimePrice() {
        return timePrice;
    }

    public void setTimePrice(String timePrice) {
        this.timePrice = timePrice;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCreateOrderTime() {
        return createOrderTime;
    }

    public void setCreateOrderTime(String createOrderTime) {
        this.createOrderTime = createOrderTime;
    }


    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public String getAllTimePrice() {
        return allTimePrice;
    }

    public void setAllTimePrice(String allTimePrice) {
        this.allTimePrice = allTimePrice;
    }

    public String getAllminutes() {
        return allminutes;
    }

    public void setAllminutes(String allminutes) {
        this.allminutes = allminutes;
    }

    public String getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(String allPrice) {
        this.allPrice = allPrice;
    }

    public String getAllMilePrice() {
        return allMilePrice;
    }

    public void setAllMilePrice(String allMilePrice) {
        this.allMilePrice = allMilePrice;
    }

    public List<BaoXianBean> getInsurance() {
        return insurance;
    }

    public void setInsurance(List<BaoXianBean> insurance) {
        this.insurance = insurance;
    }
}
