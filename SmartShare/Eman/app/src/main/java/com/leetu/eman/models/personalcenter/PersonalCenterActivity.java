package com.leetu.eman.models.personalcenter;/**
 * Created by jyt on 2016/9/20.
 */

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.models.personalcenter.views.MyPrefixesDialog;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.TitleBar;


/**
 * created by neo on 2016/9/20 09:19
 * 个人信息页面
 */
public class PersonalCenterActivity extends BaseActivity implements View.OnClickListener, PersonalCenterContract.View {
    private TitleBar titleBar;
    private LinearLayout llNickstatus;
    private PersonalCenterPresenter personalCenterPresenter;
    private EditText etUserName;
    private TextView tvUserNo, tvNickstatus;
    private UserInfoBean userInfoBean;
    private int status;//称谓状态
    private MyPrefixesDialog myPrefixesDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_center);
        initView();
        loadData();
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_personal);
        etUserName = (EditText) findViewById(R.id.et_user_name);
        tvUserNo = (TextView) findViewById(R.id.tv_user_number);

        personalCenterPresenter = new PersonalCenterPresenter(this, this);

        titleBar.setTitle("个人信息");
        titleBar.setRightText("保存");
        titleBar.showRightText();
        titleBar.showRightLayout();
        titleBar.setmRightTextColor("#ffffff");
        titleBar.setLeftClickListener(this);
        titleBar.setRightClickListener(this);


        tvNickstatus = (TextView) findViewById(R.id.tv_nickstatus);
        llNickstatus = (LinearLayout) findViewById(R.id.ll_nickstatus);
        llNickstatus.setOnClickListener(this);
    }


    protected void loadData() {
        showLoading();
        personalCenterPresenter.load();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                PersonalCenterActivity.this.finish();
                break;
            case R.id.ll_nickstatus:
                if (myPrefixesDialog == null) {
                    myPrefixesDialog = new MyPrefixesDialog(this);
                    myPrefixesDialog.setClicklistener(new MyPrefixesDialog.ClickListenerInterface() {
                        @Override
                        public void female() {
                            tvNickstatus.setText("女");
                            status = 0;
                        }

                        @Override
                        public void secrecy() {
                            tvNickstatus.setText("保密");
                            status = 1;
                        }

                        @Override
                        public void male() {
                            tvNickstatus.setText("男");
                            status = 2;
                        }
                    });
                }
                myPrefixesDialog.show();
                break;
            case R.id.layout_right:
                if (etUserName.getText().toString().trim().length() > 0) {
                    showProgressBar(true);
                    personalCenterPresenter.submit(etUserName.getText().toString().trim(), "" + status);
                } else {
                    Toast.makeText(PersonalCenterActivity.this, "请输入姓名", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    @Override
    protected void onRetryLoadData() {
        super.onRetryLoadData();
        showProgressBar(true);
        personalCenterPresenter.load();
    }

    @Override
    public void showLoad(UserInfoBean userInfoBean) {
        showContent();
        showProgressBar(false);
        if (userInfoBean != null) {
            etUserName.setText(userInfoBean.getNickname());
            LogUtils.e("lv", "姓名：" + userInfoBean.getNickname());
            tvUserNo.setText(userInfoBean.getPhoneNo());
            this.userInfoBean = userInfoBean;
            if (userInfoBean.getNickstatus() == 0) {
                tvNickstatus.setText("女");
                status = 0;
            } else if (userInfoBean.getNickstatus() == 1) {
                tvNickstatus.setText("保密");
                status = 1;
            } else if (userInfoBean.getNickstatus() == 2) {
                tvNickstatus.setText("男");
                status = 2;
            }

        }

    }

    @Override
    public void showSubmit() {
        showProgressBar(false);
        showButtomToast("保存成功");
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showProgressBar(true);
        personalCenterPresenter.load();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(PersonalCenterActivity.class);
    }
}
