package com.leetu.eman.models.usecar;/**
 * Created by jyt on 2016/9/22.
 */

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.models.usecar.bean.AdBean;
import com.leetu.eman.models.usecar.bean.Dots;
import com.leetu.eman.models.usecar.bean.OverLayBean;
import com.leetu.eman.models.usecar.bean.Points;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * created by neo on 2016/9/22 16:05
 */
public class MainPresenter implements MainContract.UserAction {
    private Context context;
    private MainContract.View mainListener;

    public MainPresenter(Context context, MainContract.View mainListener) {
        this.context = context;
        this.mainListener = mainListener;
    }

    @Override
    public void getPoint(String lat, String lng) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.POINT)
                    .addParam("lat", lat)
                    .addParam("lng", lng)
                    .readTimeout(15000)
                    .tag(MainActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (response.getResultCode() == 200) {
                                Points points = JsonParser.getParsedData(response.getData(), Points.class);
                                if (points != null) {
                                    if (points.getDots() != null) {
                                        if (points.getDots().size() > 0) {
                                            mainListener.showPoint(formatData(points));
                                        } else {
                                            mainListener.showFail("附近暂无网点");
                                        }
                                    }
                                }
                            } else if (response.getResultCode() == 264) {
//                                mainListener.showFail(response.getResultMsg());
                                mainListener.dataFail();
                            } else {
                                mainListener.showFail(response.getResultMsg());
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            mainListener.timeOutFail();
                        }
                    });
        } else {
            mainListener.showNetError(context.getString(R.string.net_error));
        }
    }

    @Override
    public void getUserInfo() {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (LeTravelApplication.getSharePreferenceInstance(context).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(context).getToken().equals("")) {
                HttpEngine.post().url(URLS.USER_INFO)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (response.getResultCode() == 200) {
                                    LogUtils.e("gn", "首页获取个人信息" + response.getData());
                                    UserInfoBean userInfoBean = JsonParser.getParsedData(response.getData(), UserInfoBean.class);
                                    if (userInfoBean != null) {
                                        LeTravelApplication.getSharePreferenceInstance(context).saveUserStatus(userInfoBean.getState(), userInfoBean.getHandHeldIdCard());
                                        mainListener.showUserInfo(userInfoBean);
                                    }
                                } else {
                                    if (response.getResultCode() != 206) {
                                        mainListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
//                                mainListener.showFail("获取用户信息失败");
                            }
                        });
            }
        } else {
            mainListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }

    //获取广告接口
    @Override
    public void getAdInfo() {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post()
                    .url(URLS.STARTUPAD)
                    .readTimeout(5000)
                    .writeTimeout(5000)
                    .connectTimeout(5000)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (response.getResultCode() == 200) {
                                LogUtils.e("lv", "广告获取数据：" + response.getData());
                                AdBean adBean = JsonParser.getParsedData(response.getData(), AdBean.class);
                                if (adBean != null) {
                                    mainListener.showAd(adBean);
                                }

                            } else if (response.getResultCode() == 283) {
                                mainListener.showFail(283, response.getResultMsg());
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            LogUtils.e("lv", "广告数据失败：" + error.getMessage());
                        }
                    });
        }
    }


    //格式化数据
    private List<OverLayBean> formatData(Points points) {
        List<OverLayBean> list = new ArrayList<>();
        if (points != null) {
            if (points.getDots() != null) {
                for (int i = 0; i < points.getDots().size(); i++) {
                    Dots dots = points.getDots().get(i);
                    OverLayBean overLayBean = new OverLayBean();
                    String[] latlng = dots.getLatlng().split(",");
                    overLayBean.setLatitude(Double.parseDouble(latlng[0]));
                    overLayBean.setLongitude(Double.parseDouble(latlng[1]));
                    overLayBean.setCarcount(dots.getCarcount());
                    overLayBean.setDotId(dots.getDotId());
                    overLayBean.setName(dots.getName());
                    overLayBean.setAddress(dots.getAddress());
                    list.add(overLayBean);
                }
            }
//            if (points.getUser() != null) {
//                OverLayBean overLayBean = new OverLayBean();
//                if (points.getUser().getDlatlng() != null) {
//                    String[] latlng = points.getUser().getDlatlng().split(",");
//                    overLayBean.setLatitude(Double.parseDouble(latlng[0]));
//                    overLayBean.setLongitude(Double.parseDouble(latlng[1]));
//                    overLayBean.setDotId(points.getUser().getDotId());
//                    overLayBean.setName(points.getUser().getDotname());
//                    overLayBean.setCarcount(points.getUser().getCarcount() + "");
//                    list.add(overLayBean);
//                }
//            }
        }
        return list;
    }
}
