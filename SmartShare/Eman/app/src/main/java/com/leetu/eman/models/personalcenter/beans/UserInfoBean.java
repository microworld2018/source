package com.leetu.eman.models.personalcenter.beans;/**
 * Created by gn on 2016/9/29.
 */

/**
 * created by neo on 2016/9/29 14:05
 */
public class UserInfoBean {
    int nickstatus;
    String drivingLicenseImg;
    String nickname;
    String idCardImg;
    String handHeldIdCard;
    String stateDesc;
    String name;
    String IDCard;
    int state;
    String phoneNo;

    public int getNickstatus() {
        return nickstatus;
    }

    public void setNickstatus(int nickstatus) {
        this.nickstatus = nickstatus;
    }

    public String getDrivingLicenseImg() {
        return drivingLicenseImg;
    }

    public void setDrivingLicenseImg(String drivingLicenseImg) {
        this.drivingLicenseImg = drivingLicenseImg;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getIdCardImg() {
        return idCardImg;
    }

    public void setIdCardImg(String idCardImg) {
        this.idCardImg = idCardImg;
    }

    public String getStateDesc() {
        return stateDesc;
    }

    public void setStateDesc(String stateDesc) {
        this.stateDesc = stateDesc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getState() {
        return state;
    }

    public String getIDCard() {
        return IDCard;
    }

    public void setIDCard(String IDCard) {
        this.IDCard = IDCard;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getHandHeldIdCard() {
        return handHeldIdCard;
    }

    public void setHandHeldIdCard(String handHeldIdCard) {
        this.handHeldIdCard = handHeldIdCard;
    }
}
