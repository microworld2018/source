package com.leetu.eman.models.settings;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.views.CustomEditText;
import com.leetu.eman.views.PhotoFragment;
import com.leetu.eman.views.TitleBar;


/**
 * 意见反馈页面
 */
public class ComplaintActivity extends BaseActivity implements View.OnClickListener, ComplaintContract.View {

    private TitleBar titleBar;
    private EditText etPhone;
    private CustomEditText customEdit;//发布内容
    private ComplaintPresenter complaintPresenter;
    private Button btComplaintCommit;
    private PhotoFragment fragment;
    private int picSize;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complaint);
        initView();
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_complaint);
        btComplaintCommit = (Button) findViewById(R.id.bt_complaint_commit);
        customEdit = (CustomEditText) findViewById(R.id.custom_edit);
        etPhone = (EditText) findViewById(R.id.et_complaint_phone);

        savePhotoFragment();
        complaintPresenter = new ComplaintPresenter(this, this);

        titleBar.setTitle("意见反馈");
        titleBar.setLeftClickListener(this);
        btComplaintCommit.setOnClickListener(this);

        customEdit.setHintText("请输入您的建议或反馈！");
        customEdit.setMaxLength(200);
        customEdit.setTextNumChangeListener(new CustomEditText.TextNumChangeListener() {
            @Override
            public void onNumChangeListener(boolean isChange) {
                if (isChange) {

                    btComplaintCommit.setEnabled(true);
                } else {
                    if (picSize > 0) {
                        btComplaintCommit.setEnabled(true);
                    } else {
                        btComplaintCommit.setEnabled(false);
                    }
                }
            }
        });

    }

    void savePhotoFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.
                beginTransaction();
        fragment = new PhotoFragment();
        //加到Activity中
        fragmentTransaction.add(R.id.lt_complaint_fragment_content, fragment);
        //加到后台堆栈中，有下一句代码的话，点击返回按钮是退到Activity界面，没有的话，直接退出Activity
        //后面的参数是此Fragment的Tag。相当于id
//        fragmentTransaction.addToBackStack("fragment1");
        //记住提交
        fragmentTransaction.commit();

        fragment.setOnSelectPicListener(new PhotoFragment.OnSelectPicListener() {


            @Override
            public void selectPic(int size) {
                picSize = size;
                if (picSize > 0) {
                    btComplaintCommit.setEnabled(true);
                } else {
                    if (customEdit.getText().length() > 0) {
                        btComplaintCommit.setEnabled(true);
                    } else {
                        btComplaintCommit.setEnabled(false);
                    }
                }
            }
        });
    }


    protected void loadData() {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.layout_left:
                ComplaintActivity.this.finish();
                break;
            case R.id.bt_complaint_commit:
                if (checkEditText()) {
                    showProgressBar(true);
                    complaintPresenter.upFileData(fragment.getPicStringList(), customEdit.getText(), etPhone.getText().toString().trim());
                }
                break;
        }
    }

    boolean checkEditText() {
        if (customEdit.getText().toString().trim().equals("")) {
            showButtomToast("请输入内容");
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void showupFileData() {
        showProgressBar(false);
        showButtomToast("意见提交成功！");
        customEdit.setText(null);

    }

    @Override
    public void loginOk() {
        showContent();
        super.loginOk();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        fragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(ComplaintActivity.class);
    }
}
