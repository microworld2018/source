package com.leetu.eman.payutils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alipay.sdk.app.PayTask;
import com.leetu.eman.models.deposit.bean.DepositWxPayBean;
import com.leetu.eman.utils.LogUtils;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 * Created by lvjunfeng on 2017/4/11.
 */

public class PayUtils {

    private String payFlags = "0";//支付标记
    private static final int SDK_PAY_FLAG = 1;
    private static IWXAPI api;
    public static PayUtils payUtils;
    private DepositWxPayBean.WxBean.OrderBean wxOrderBean;

    public static PayUtils getInstance() {
        if (payUtils == null) {
            payUtils = new PayUtils();
        }
        return payUtils;
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @SuppressWarnings("unused")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((String) msg.obj);
                    /**
                     * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                     * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                     * docType=1) 建议商户依赖异步通知
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
//                    showProgressBar(true);//--------------------------------
                    // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档

                    if (aliPayListener != null) {
                        aliPayListener.onPayListener(resultStatus, resultInfo);
                    }
                    break;
                }
                default:
                    break;
            }
        }

        ;
    };

    public void aliPay(final String payString, final Context context, AliPayListener aliPayListener) {
        this.aliPayListener = aliPayListener;
        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                // 构造PayTask 对象
                PayTask alipay = new PayTask((Activity) context);
                // 调用支付接口，获取支付结果
                String result = alipay.pay(payString, true);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);
            }
        };

        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    public void wxPay(DepositWxPayBean payBean, Context context) {
        api = WXAPIFactory.createWXAPI(context, "wxfdde3d5877da4cf8");
        api.registerApp("wxfdde3d5877da4cf8");
        if (payBean != null && payBean.getWx() != null) {
            wxOrderBean = payBean.getWx().getOrder();
            if (wxOrderBean != null) {
                PayReq req = new PayReq();
                req.appId = wxOrderBean.getAppid();
                req.partnerId = wxOrderBean.getPartnerid();
                req.prepayId = wxOrderBean.getPrepayid();
                req.nonceStr = wxOrderBean.getNoncestr();
                req.timeStamp = wxOrderBean.getTimestamp();
                req.packageValue = "Sign=WXPay";//固定形式，
                req.sign = wxOrderBean.getSign();
                req.extData = "app data"; // optional
                // 调起支付
                api.sendReq(req);
                LogUtils.e("lv", "微信支付接口回调调起支付");
            }
        }
    }

    private AliPayListener aliPayListener;
    private WxPayListener wXpayListener;


    public void setAliPayListener(AliPayListener payListener) {
        this.aliPayListener = payListener;
    }

    public void setWxPayListener(WxPayListener payListener) {
        this.wXpayListener = payListener;
    }

    public interface AliPayListener {
        void onPayListener(String resultStatus, String resultInfo);
    }

    public interface WxPayListener {
        void onPaySuccessListener(String result);
    }
}

