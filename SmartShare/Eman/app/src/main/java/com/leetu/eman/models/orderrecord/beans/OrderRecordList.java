package com.leetu.eman.models.orderrecord.beans;/**
 * Created by gn on 2016/9/28.
 */

import java.util.List;

/**
 * created by neo on 2016/9/28 14:40
 */
public class OrderRecordList {
    List<OrderRecordBean> orders;

    public List<OrderRecordBean> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderRecordBean> orders) {
        this.orders = orders;
    }
}
