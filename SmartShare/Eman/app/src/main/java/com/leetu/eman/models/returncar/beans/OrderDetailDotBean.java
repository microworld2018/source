package com.leetu.eman.models.returncar.beans;/**
 * Created by gn on 2016/10/10.
 */

/**
 * created by neo on 2016/10/10 14:47
 */
public class OrderDetailDotBean {
    String dotName;
    String dotLatLng;
    String dotId;
    String distance;

    public String getDotName() {
        return dotName;
    }

    public void setDotName(String dotName) {
        this.dotName = dotName;
    }

    public String getDotLatLng() {
        return dotLatLng;
    }

    public void setDotLatLng(String dotLatLng) {
        this.dotLatLng = dotLatLng;
    }

    public String getDotId() {
        return dotId;
    }

    public void setDotId(String dotId) {
        this.dotId = dotId;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
