package com.leetu.eman.models.confirmorder;/**
 * Created by jyt on 2016/9/27.
 */


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBean;

import java.util.List;

/**
 * created by neo on 2016/9/27 13:17
 */
public interface ReturnCarAddressContract {
    interface View extends BaseContract {
        void showLoad(List<ReturnCarPointBean> returnCarPoints);
    }

    interface UserAction {
        void load(String dotId, boolean isUP, boolean isFrist);
    }
}
