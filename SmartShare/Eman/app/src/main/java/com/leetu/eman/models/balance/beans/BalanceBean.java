package com.leetu.eman.models.balance.beans;/**
 * Created by gn on 2016/9/29.
 */

import java.io.Serializable;

/**
 * created by neo on 2016/9/29 13:24
 */
public class BalanceBean implements Serializable {
   private double deposit;
   private double amount;
   private String couponCount;

    public double getDeposit() {
        return deposit;
    }

    public void setDeposit(double deposit) {
        this.deposit = deposit;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCouponCount() {
        return couponCount;
    }

    public void setCouponCount(String couponCount) {
        this.couponCount = couponCount;
    }
}
