package com.leetu.eman.models.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.TitleBar;

import static android.webkit.WebSettings.LOAD_NO_CACHE;

public class WebViewActivity extends BaseActivity implements View.OnClickListener {
    private WebView webView;
    private TitleBar titleBar;
    private ProgressBar progressBar;
    private String url;
    private boolean netFlag = true;
    public static final String WEBVIEW_URL = "webview_url";
    public static final String TITLE_FLAG = "title_flag";
    public static final String CONTENT_FLAG = "content_flag";//传的是文本


    private String contentFlag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        webView = (WebView) findViewById(R.id.webview);
        titleBar = (TitleBar) findViewById(R.id.webview_title);
        progressBar = (ProgressBar) findViewById(R.id.progressline_web);

        url = getIntent().getStringExtra(WEBVIEW_URL);
        contentFlag = getIntent().getStringExtra(CONTENT_FLAG);
        String title_name = getIntent().getStringExtra(TITLE_FLAG);
        titleBar.setTitle(title_name);
        titleBar.setLeftClickListener(this);

        WebSettings settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(LOAD_NO_CACHE);
        webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        settings.setBuiltInZoomControls(true);
        //加载需要显示的网页
        if (url != null) {
            webView.loadUrl(url);
        } else if (contentFlag != null) {
            webView.loadData(contentFlag, "text/html; charset=UTF-8", null);
        }
        //设置Web视图
        webView.setWebViewClient(new webViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                if (title != null) {
                    titleBar.setTitle(title);//a textview
                }
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progressBar.setProgress(newProgress);
                super.onProgressChanged(view, newProgress);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                WebViewActivity.this.finish();
                break;
        }
    }


    private class webViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            LogUtils.e("lv", "url" + url);
            Intent intent = new Intent();
            intent.setClass(WebViewActivity.this, WebViewDetailActivity.class);
            intent.putExtra(WebViewDetailActivity.DETAIL_URL, url);
            startActivity(intent);

            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            LogUtils.e("lv", "errorCode:" + errorCode); //-6服务器异常  -2网络异常
            netFlag = true;
            if (errorCode == -2) {
                contentFail();
            } else if (errorCode == -6) {
                showLoading();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            LogUtils.e("lv", "onPageFinished:" + url);
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            if (!netFlag) {
                showContent();
            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
        }
    }


    @Override
    protected void onRetryLoadData() {
        super.onRetryLoadData();
        netFlag = false;
        webView.loadUrl(url);
    }

    @Override
    protected void onDestroy() {
        if (webView != null) {
            webView.clearHistory();
            ((ViewGroup) webView.getParent()).removeView(webView);
            webView.destroy();
            webView = null;
        }
        super.onDestroy();
    }

}
