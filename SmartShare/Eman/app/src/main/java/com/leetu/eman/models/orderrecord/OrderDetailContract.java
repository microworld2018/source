package com.leetu.eman.models.orderrecord;


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.orderrecord.beans.OrderDetailBean;

/**
 * Created by Administrator on 2016/11/5.
 */

public interface OrderDetailContract {
    interface View extends BaseContract {
        void showLoad(OrderDetailBean orderDetailBean);
    }

    interface UserAction {
        void load(String orderId, boolean isFrist);
    }
}
