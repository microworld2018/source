package com.leetu.eman.models.returncar;/**
 * Created by gn on 2016/10/11.
 */

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.returncar.beans.OrederCreateDetailBean;
import com.leetu.eman.models.returncar.beans.PayBean;
import com.leetu.eman.models.returncar.beans.WXPayBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;


/**
 * created by neo on 2016/10/11 15:14
 */
public class PayOrderPresenter extends BasePresenter implements PayOrderContract.UserAction {
    private Context context;
    private PayOrderContract.View payOrderListener;

    public PayOrderPresenter(Context context, PayOrderContract.View payOrderListener) {
        this.context = context;
        this.payOrderListener = payOrderListener;
    }

    @Override
    public void loadOrder(String payFlags,String selectCoupon) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.ORDER_OK_DETAIL)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("repayFlag", payFlags)
                    .addParam("selectCoupon", selectCoupon)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), payOrderListener)) {
                                LogUtils.e("gn", "生成订单详情" + response.getData());
                                OrederCreateDetailBean orederCreateDetailBean = JsonParser.getParsedData(response.getData(), OrederCreateDetailBean.class);
                                payOrderListener.showLoadOrder(orederCreateDetailBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    payOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            payOrderListener.timeOutFail();
                        }
                    });
        } else {
            payOrderListener.contentFail();
        }
    }

    @Override
    public void pay(String orderId, String payType) {
        if ("3".equals(payType)) {
            goToAliPay(orderId, payType);
        } else if ("5".equals(payType)) {
            goToWxPay(orderId, payType);
        }
    }

    private void goToWxPay(String orderId, String payType) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.PAY)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("payType", payType)
                    .addParam("payDetail", "order")
                    .tag(PayOrderActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), payOrderListener)) {
                                LogUtils.e("gn", "支付订单Data:" + response.getData());
                                WXPayBean wxPayBean = JsonParser.getParsedData(response.getData(), WXPayBean.class);
                                payOrderListener.showWxPay(wxPayBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    if (response.getResultCode() == 270) {//订单已重复支付
                                        payOrderListener.showPaySuccess();
                                    } else {
                                        payOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            payOrderListener.timeOutFail();
                        }
                    });
        } else {
            payOrderListener.showNetError("");
        }
    }

    private void goToAliPay(String orderId, String payType) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.PAY)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("payType", payType)
                    .addParam("payDetail", "order")
                    .tag(PayOrderActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), payOrderListener)) {
                                LogUtils.e("gn", "支付订单" + response.getData());
                                PayBean payBean = JsonParser.getParsedData(response.getData(), PayBean.class);
                                payOrderListener.showPay(payBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    if (response.getResultCode() == 270) {//订单已重复支付
                                        payOrderListener.showPaySuccess();
                                    } else {
                                        payOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            payOrderListener.timeOutFail();
                        }
                    });
        } else {
            payOrderListener.showNetError("");
        }

    }

    @Override
    public void paySuccess(String resultStatus, String result) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.PAY_SUCCESS)
                    .addParam("resultStatus", resultStatus)
                    .addParam("result", result)
                    .execute3(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), payOrderListener)) {
                                LogUtils.e("gn", "支付成功" + response.getData());
                                payOrderListener.showPaySuccess();
                            } else {
                                if (response.getResultCode() != 206) {
                                    payOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            payOrderListener.timeOutFail();
                        }
                    });
        } else {
            payOrderListener.showNetError("");
        }
    }

    @Override
    public void useCoupon(String subCouponId, String orderId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.COUPON_CONFIRM)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("subCouponId", subCouponId)
                    .addParam("orderId", orderId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), payOrderListener)) {
                                LogUtils.e("gn", "使用优惠券" + response.getData());
                                payOrderListener.showUseCoupn();
                            } else {
                                if (response.getResultCode() != 206) {
                                    payOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            payOrderListener.timeOutFail();
                        }
                    });
        } else {
            payOrderListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }

    @Override
    public void payOverOrder(String orderId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.PAY_OVER_ORDER)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), payOrderListener)) {
                                LogUtils.e("gn", "直接支付" + response.getData());
                                payOrderListener.showPayOverOrder();
                            } else {
                                if (response.getResultCode() != 206) {
                                    payOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            payOrderListener.timeOutFail();
                        }
                    });
        } else {
            payOrderListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }

    @Override
    public void errorPay(String code, String orderId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.PAY_Err)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("code", code)
                    .addParam("orderId", orderId)
                    .execute4(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {//重复支付
                            if (checkCode(response.getResultCode(), payOrderListener)) {
                                payOrderListener.showPaySuccess();
                            } else {
                                payOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            payOrderListener.timeOutFail();
                        }
                    });
        } else {
            payOrderListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }

    @Override
    public void confirmWxPay(String orderId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.WXPAY_COMFIRM_ORDER)
                    .addParam("orderNo", orderId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), payOrderListener)) {
                                LogUtils.e("lv","微信支付查询成功");
                                payOrderListener.showPaySuccess();
                            } else {
                                payOrderListener.showPayFail();
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            payOrderListener.timeOutFail();
                        }
                    });
        } else {
            payOrderListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }
}
