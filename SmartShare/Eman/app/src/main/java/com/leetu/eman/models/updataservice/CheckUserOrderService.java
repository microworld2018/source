package com.leetu.eman.models.updataservice;/**
 * Created by gn on 2016/9/28.
 */

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;


import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.models.returncar.beans.OrederCreateDetailBean;
import com.leetu.eman.models.takecar.beans.TakeCarBean;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.models.usecar.bean.CheckUserOrderBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

import org.greenrobot.eventbus.EventBus;


/**
 * created by neo on 2016/9/28 09:53
 * 检查用户当前是否有订单
 */
public class CheckUserOrderService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    void init() {
        if (NetworkHelper.isNetworkConnect(this)) {
            if (LeTravelApplication.getSharePreferenceInstance(CheckUserOrderService.this).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(CheckUserOrderService.this).getToken().equals("")) {
                HttpEngine.post().url(URLS.CHECK_ORDER)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(this).getToken())
                        .tag(MainActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                LogUtils.e("gn", "检查用户订单" + response.getResultCode());
                                if (response.getResultCode() == 200) {
                                    LogUtils.e("gn", "检查用户订单" + response.getData());
                                    try {
                                        CheckUserOrderBean checkUserOrderBean =
                                                JsonParser.getParsedData(response.getData(), CheckUserOrderBean.class);
                                        LogUtils.e("gn", checkUserOrderBean.toString());
                                        if (checkUserOrderBean.getIsexist() == 1) {
                                            LogUtils.e("gn", "有订单");
                                            if (checkUserOrderBean.getOrderType() == 0) {
                                                TakeCarBean checkOrderBean = JsonParser.getParsedData(response.getData(), TakeCarBean.class);
                                                EventBus.getDefault().post(checkOrderBean);
                                            } else if (checkUserOrderBean.getOrderType() == 1) {
                                                LogUtils.e("gn", "用车中");
                                                CurrentOrderBean currentOrderBean = JsonParser.getParsedData(response.getData(), CurrentOrderBean.class);
                                                EventBus.getDefault().post(currentOrderBean);
                                            } else if (checkUserOrderBean.getPayStatus() == 0) {
                                                LogUtils.e("gn", "有未支付的订单");
                                                EventBus.getDefault().post(new OrederCreateDetailBean());
                                            }
                                        } else {
                                            EventBus.getDefault().post("");
                                        }
                                    } catch (Exception e) {
                                        LogUtils.e("gn", "失败" + e.toString());
                                        e.printStackTrace();
                                    }
                                } else {
                                    EventBus.getDefault().post(response);
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                EventBus.getDefault().post(getResources().getString(R.string.time_out));
                            }
                        });
            } else {//后台检测是否登录并不做任何处理
//                EventBus.getDefault().post(new TokenBean());
            }
            stopSelf();
        } else {
            EventBus.getDefault().post(getResources().getString(R.string.net_error));
        }
    }
}
