package com.leetu.eman.models.balance.beans;/**
 * Created by gn on 2016/9/29.
 */

/**
 * created by neo on 2016/9/29 13:26
 */
public class RechargeRecordBean {
    int amount;
    String tradeTime;
    String payChannel;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(String payChannel) {
        this.payChannel = payChannel;
    }
}
