package com.leetu.eman.models.peccancyrecord.beans;/**
 * Created by gn on 2016/9/28.
 */

import java.util.List;

/**
 * created by neo on 2016/9/28 14:40
 */
public class PeccancyRecordList {
    List<PeccancyRecordBean> accidentList;

    public List<PeccancyRecordBean> getAccidentList() {
        return accidentList;
    }

    public void setOrders(List<PeccancyRecordBean> records) {
        this.accidentList = records;
    }
}
