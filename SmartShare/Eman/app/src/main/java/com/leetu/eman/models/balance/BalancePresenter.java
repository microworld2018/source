package com.leetu.eman.models.balance;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.balance.beans.BalanceBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;


/**
 * Created by jyt on 2016/9/18.
 */

public class BalancePresenter extends BasePresenter implements BalanceContract.UserAction {
    private Context context;
    private BalanceContract.View balanceListener;
    private int page = 1;

    public BalancePresenter(Context context, BalanceContract.View balanceListener) {
        this.context = context;
        this.balanceListener = balanceListener;
    }

    @Override
    public void load(final boolean isFrist) {

        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, balanceListener)) {
                HttpEngine.post().url(URLS.MyWallet)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .tag(BalanceActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), balanceListener)) {
                                    LogUtils.e("lv", "我的钱包：" + response.getData());
                                    BalanceBean balanceBean = JsonParser.getParsedData(response.getData(), BalanceBean.class);
                                    if (balanceBean != null) {
                                        balanceListener.showLoad(balanceBean);
                                    }
                                } else {
                                    if (response.getResultCode() != 206) {
                                        balanceListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                balanceListener.timeOutFail();
                            }
                        });
            }
        } else {
            if (isFrist) {
                balanceListener.contentFail();
            } else {
                balanceListener.showFail(context.getResources().getString(R.string.net_error));
            }

        }
    }
}
