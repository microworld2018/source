package com.leetu.eman.models.returncar;/**
 * Created by jyt on 2016/9/21.
 */

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.views.TitleBar;

/**
 * created by neo on 2016/9/21 09:25
 * 即将还车
 */
public class ReturnCarActivity extends BaseActivity implements View.OnClickListener, ReturnCarCortract.View, BaseActivity.MyLocationLisenter {
    private TitleBar titleBar;
    private ImageView ivLockCar, ivNav;
    private ReturnCarPresenter returnCarPresenter;
    public static final String ORDER_DETAIL = "orederDetail";
    private CurrentOrderBean currentOrderBean;
    private BDLocation location;
    private int type;
    private TextView tvLocationName, tvName, tvAddress, tvService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_car);
        initView();
    }

    protected void initView() {
        if (getIntent().getExtras().getSerializable(ORDER_DETAIL) != null) {
            currentOrderBean = (CurrentOrderBean) getIntent().getExtras().getSerializable(ORDER_DETAIL);
        }
        titleBar = (TitleBar) findViewById(R.id.title_returncar);
        ivLockCar = (ImageView) findViewById(R.id.iv_will_lockcar);
        ivNav = (ImageView) findViewById(R.id.iv_returncar_nav);
        tvLocationName = (TextView) findViewById(R.id.tv_return_location);
        tvName = (TextView) findViewById(R.id.tv_return_name);
        tvAddress = (TextView) findViewById(R.id.tv_return_adddress);
        tvService = (TextView) findViewById(R.id.tv_return_customer_service);

        returnCarPresenter = new ReturnCarPresenter(this, this);
        setMyLocationLisenter(this);

        if (currentOrderBean != null) {
            if (currentOrderBean.getDot() != null) {
                tvName.setText(currentOrderBean.getDot().getName());
                tvAddress.setText(currentOrderBean.getDot().getAddress());
            }
        }
        setProgressType(false);
        titleBar.setTitle("即将还车");
        titleBar.setLeftClickListener(this);
        ivLockCar.setOnClickListener(this);
        setMyLocationLisenter(this);
        ivNav.setOnClickListener(this);
        tvService.setOnClickListener(this);
        reGetLocation();
    }

    @Override
    protected void onRetryLoadData() {
        reGetLocation();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                ReturnCarActivity.this.finish();
                break;
            case R.id.iv_will_lockcar:
                showDialog("温馨提示", "小主要在车外锁门哦！", "确定", "取消", false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showProgressBar(true);
                        type = 1;
                        reGetLocation();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                break;
            case R.id.iv_returncar_nav:
                type = 2;
                reGetLocation();
                break;
            case R.id.tv_return_customer_service:
                showDialog("联系客服", "400-888-1212", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callPhone("400-888-1212");
                    }
                });
                break;
        }
    }

    /**
     * 拨打电话
     */
    private void callPhone(String number) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + number);
        intent.setData(data);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    @Override
    public void showLockCarDoor() {

    }

    @Override
    public void showReturnCar() {
        showProgressBar(false);
        Intent intent = new Intent(ReturnCarActivity.this, PayOrderActivity.class);
        intent.putExtra(PAY_FLAGS, "0");
        startActivity(intent);
        ReturnCarActivity.this.finish();
    }

    @Override
    public void getLocation(BDLocation location) {
        this.location = location;
        tvLocationName.setText("您目前正位于：" + location.getAddrStr());
        if (type == 1) {
            returnCarPresenter.lockCarDoor("" + location.getLatitude(), "" + location.getLongitude(), currentOrderBean.getCar().getCarId(), currentOrderBean.getOrderId(), currentOrderBean.getDot().getDotId());
        } else if (type == 2) {
            navigationLocationApp(location.getLatitude(), location.getLongitude(), location.getBuildingName(), currentOrderBean.getDot().getLat(), currentOrderBean.getDot().getLng(), currentOrderBean.getDot().getName(), 2);
        }
//        returnCarPresenter.lockCarDoor("" + location.getLatitude(), "" + location.getLongitude(), currentOrderBean.getCar().getCarId(), currentOrderBean.getOrderId());
    }

    @Override
    public void loginOk() {
        LeTravelApplication.getInstance().finishAllExcept(MainActivity.class);
        super.loginOk();
    }
}
