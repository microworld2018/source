package com.leetu.eman.models.confirmorder.adapter;/**
 * Created by jyt on 2016/9/27.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBean;

import java.util.List;


/**
 * created by neo on 2016/9/27 13:42
 * 可还车网点adapter
 */
public class ReturnCarAddressAdapter extends BaseAdapter {
    private List<ReturnCarPointBean> list;
    private Context context;

    public ReturnCarAddressAdapter(List<ReturnCarPointBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHoler viewHoler = null;
        if (convertView == null) {
            viewHoler = new ViewHoler();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_returncar_address, null);
            viewHoler.tvName = (TextView) convertView.findViewById(R.id.tv_return_car_name);
            viewHoler.tvAddress = (TextView) convertView.findViewById(R.id.tv_return_car_address);
            convertView.setTag(viewHoler);
        } else {
            viewHoler = (ViewHoler) convertView.getTag();
        }
        viewHoler.tvName.setText(list.get(position).getName());
        viewHoler.tvAddress.setText(list.get(position).getAddress());
        return convertView;
    }

    class ViewHoler {
        TextView tvName, tvAddress;
    }
}
