package com.leetu.eman.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leetu.eman.R;


/**
 * Created by lvjunfeng on 2017/6/12.
 */

public class AdLoadingTime extends RelativeLayout {
    private TextView tv_jump_time;
    //默认倒计时时间
    private int progress = 4;

    private ProgressListener progressListener;
    public AdLoadingTime(Context context) {
       this(context,null);
    }

    public AdLoadingTime(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public AdLoadingTime(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.adtime_view,this,true);
        tv_jump_time = (TextView) findViewById(R.id.tv_jump_time);
    }

    public void stop(){
        removeCallbacks(task);
    }

    public void start(){
        stop();
        post(task);
    }

    Runnable task = new Runnable() {
        @Override
        public void run() {
                tv_jump_time.setText(progress+"");
                progress--;
                if(progress == 0) {
                    stop();
                    if (progressListener != null) {
                        progressListener.endProgress();
                    }
                }
                postDelayed(task,1000);
        }
    };

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public void setProgressListener(ProgressListener progressListener) {
        this.progressListener = progressListener;
    }

    public interface ProgressListener {
        void endProgress();
    }
}
