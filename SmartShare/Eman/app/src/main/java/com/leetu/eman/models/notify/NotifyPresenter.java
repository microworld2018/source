package com.leetu.eman.models.notify;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.notify.beans.NotifyList;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

/**
 * Created by Administrator on 2016/11/5.
 */

public class NotifyPresenter extends BasePresenter implements NotifyContract.UserAction {
    private Context context;
    private NotifyContract.View notifyListener;
    private int page = 1;

    public NotifyPresenter(Context context, NotifyContract.View notifyListener) {
        this.context = context;
        this.notifyListener = notifyListener;
    }

    @Override
    public void loadData(final boolean isUp, final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, notifyListener)) {
                page = 1;

                HttpEngine.post().url(URLS.NOTIFI_MSG)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .addParam("currentPage",page+"" )
                        .tag(NotifyActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), notifyListener)) {
                                    LogUtils.e("gn", "通知" + response.getData());
                                    NotifyList recordList = JsonParser.getParsedData(response.getData(), NotifyList.class);
                                    if (recordList != null) {
                                        if (recordList.getData() != null) {
                                            notifyListener.showLoadData(isUp, recordList.getData());
                                        }
                                    }

                                } else {
                                    if (response.getResultCode() != 206) {
                                        if (isFrist) {
                                            notifyListener.contentFail();
                                        } else {
                                            notifyListener.showFail(response.getResultCode(), response.getResultMsg());
                                        }

                                    }

                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                notifyListener.timeOutFail();
                            }
                        });
            }
        } else {
            if (isFrist) {
                notifyListener.contentFail();
            } else {
                notifyListener.showFail(context.getResources().getString(R.string.net_error));
            }
        }
    }
}
