package com.leetu.eman.mygreedao.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

/**
 * Created by neo on 2017/2/7.
 */
//调试信息txt
public class DebugMessage {
    public static void put(Context context, final String s, final String name) {
        try {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    FileOutputStream outStream = null;
                    try {
                        outStream = new FileOutputStream("/sdcard/" + name + ".txt", true);
                        OutputStreamWriter writer = new OutputStreamWriter(outStream, "gb2312");
                        writer.write(s);
                        writer.write("/n");
                        writer.flush();
                        writer.close();//记得关闭
                        outStream.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        } catch (Exception e) {
            Log.e("m", "file write error");
        }
    }
}