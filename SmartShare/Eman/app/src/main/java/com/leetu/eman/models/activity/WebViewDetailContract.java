package com.leetu.eman.models.activity;

/**
 * Created by lvjunfeng on 2017/6/13.
 */

public interface WebViewDetailContract {
    interface View{
        void showShareResault(int code);

    }
    interface UserAction{
        void goShare();

    }
}
