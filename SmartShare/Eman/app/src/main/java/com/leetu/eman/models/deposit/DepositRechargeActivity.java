package com.leetu.eman.models.deposit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.activity.WebViewActivity;
import com.leetu.eman.models.deposit.bean.DepositRechargeBean;
import com.leetu.eman.models.deposit.bean.DepositWxPayBean;
import com.leetu.eman.models.returncar.beans.PayBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.payutils.PayUtils;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.TitleBar;

import static com.leetu.eman.models.activity.WebViewActivity.TITLE_FLAG;
import static com.leetu.eman.models.deposit.DepositReChargeResaultActivity.DEPOSIT_RESAULT;
import static com.leetu.eman.wxapi.WXPayEntryActivity.ACTION_NAME;

public class DepositRechargeActivity extends BaseActivity implements DepositRechargeContract.View, View.OnClickListener {
    private TitleBar titleBar;
    private TextView needReCharge, argments;
    private LinearLayout llAliPay;
    private LinearLayout llWxPay;
    private CheckBox cbAali, cbWechat;
    private Button startPay;
    private DepositRechargePresenter depositRechargePresenter;

    private int type = 3;
    private int DEPOSIT_REQUEST = 1;
    private boolean isFrist = true;
    private MyBroadcastReceiver myBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit_recharge);
        initView();
        initData();
        registWxPayBroadcast();
    }

    //注册微信支付成功失败广播
    private void registWxPayBroadcast() {
        IntentFilter myIntentFilter = new IntentFilter();
        myIntentFilter.addAction(ACTION_NAME);
        myBroadcastReceiver = new MyBroadcastReceiver();
        registerReceiver(myBroadcastReceiver, myIntentFilter);
    }


    private void initView() {
        titleBar = (TitleBar) findViewById(R.id.deposit_reCharge_title);
        needReCharge = (TextView) findViewById(R.id.need_reCharge);
        llAliPay = (LinearLayout) findViewById(R.id.lt_pay_ali);
        llWxPay = (LinearLayout) findViewById(R.id.lt_pay_wechat);
        startPay = (Button) findViewById(R.id.bt_pay);
        cbAali = (CheckBox) findViewById(R.id.cb_ali);
        cbWechat = (CheckBox) findViewById(R.id.cb_wechat);
        argments = (TextView) findViewById(R.id.rechange_argments);
        depositRechargePresenter = new DepositRechargePresenter(this, this);

        titleBar.setTitle("押金充值");
        titleBar.setLeftClickListener(this);
        llAliPay.setOnClickListener(this);
        llWxPay.setOnClickListener(this);
        cbAali.setOnClickListener(this);
        cbWechat.setOnClickListener(this);
        startPay.setOnClickListener(this);
        argments.setOnClickListener(this);
    }

    private void initData() {
        showProgressBar(true);
        depositRechargePresenter.loadData(isFrist);
    }

    @Override
    public void showLoad(DepositRechargeBean amountBean) {
        showContent();
        showProgressBar(false);
        isFrist = false;
        //设置押金金额显示
        needReCharge.setText(amountBean.getAmount() + "");
    }

    @Override
    public void showAliPay(PayBean payBean) {
        if (payBean != null && payBean.getAlipay() != null) {
            PayUtils.getInstance().aliPay(payBean.getAlipay().getAliparameter(), DepositRechargeActivity.this, new PayUtils.AliPayListener() {
                @Override
                public void onPayListener(String resultStatus, String resultInfo) {
                    if (TextUtils.equals(resultStatus, "9000")) {
                        showProgressBar(false);
                        Intent intent = new Intent(DepositRechargeActivity.this, DepositReChargeResaultActivity.class);
                        intent.putExtra(DEPOSIT_RESAULT, true);
                        startActivityForResult(intent, DEPOSIT_REQUEST);
                    } else {
                        showProgressBar(false);
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            showButtomToast("支付结果确认中");
                        } else if (TextUtils.equals(resultStatus, "6001")) {//用户中途取消  5000重复请求
                            showButtomToast("取消支付");
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            LogUtils.e("lv","支付宝支付失败resultStatus："+resultStatus);
                            showButtomToast("支付失败");
                            Intent intent = new Intent(DepositRechargeActivity.this, DepositReChargeResaultActivity.class);
                            intent.putExtra(DEPOSIT_RESAULT, false);
                            startActivityForResult(intent, DEPOSIT_REQUEST);
                        }
                    }
                }
            });
        }
    }

    @Override
    public void showWxPay(DepositWxPayBean wxPayBean) {
        showProgressBar(false);
        PayUtils.getInstance().wxPay(wxPayBean, this);
    }


//    @Override
//    public void showPaySuccess() {
//        showProgressBar(false);
//        Intent intent = new Intent(DepositRechargeActivity.this, DepositReChargeResaultActivity.class);
//        intent.putExtra(DEPOSIT_RESAULT, true);
//        startActivityForResult(intent, DEPOSIT_REQUEST);
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                finish();
                break;
            case R.id.lt_pay_ali:
            case R.id.cb_ali:
                type = 3;
                if (cbAali.isChecked()) {
                    cbWechat.setChecked(false);
                } else {
                    cbAali.setChecked(true);
                    cbWechat.setChecked(false);
                }
                break;
            case R.id.lt_pay_wechat:
            case R.id.cb_wechat:
                type = 5;
                if (cbWechat.isChecked()) {
                    cbAali.setChecked(false);
                } else {
                    cbWechat.setChecked(true);
                    cbAali.setChecked(false);
                }
                break;

            case R.id.bt_pay:
                showProgressBar(true);
                depositRechargePresenter.loadPay("" + type);
                break;

            case R.id.rechange_argments:
                Intent intent = new Intent(DepositRechargeActivity.this, WebViewActivity.class);
                intent.putExtra(WebViewActivity.WEBVIEW_URL, URLS.DEPOSIT_ARGUMENT);
                intent.putExtra(TITLE_FLAG, "押金协议");
                startActivity(intent);
                break;
        }
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        depositRechargePresenter.loadData(isFrist);
    }

    @Override
    protected void onRetryLoadData() {
        super.onRetryLoadData();
        depositRechargePresenter.loadData(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(DepositRechargeActivity.class);
        unregisterReceiver(myBroadcastReceiver);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == DEPOSIT_REQUEST) {
            if (resultCode == 1) {
            } else if (resultCode == 2) {
                //查看押金
                Intent intent = getIntent();
                setResult(2, intent);
                DepositRechargeActivity.this.finish();
            }

        }

    }

    class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_NAME)) {
                int err_code = intent.getIntExtra("err_code", 1);
                Intent intent2 = new Intent(DepositRechargeActivity.this, DepositReChargeResaultActivity.class);

                if (err_code == 0) {//成功
                    intent2.putExtra(DEPOSIT_RESAULT, true);
                    startActivityForResult(intent2, DEPOSIT_REQUEST);
                } else if (err_code == -1) {
                    intent2.putExtra(DEPOSIT_RESAULT, false);
                    startActivityForResult(intent2, DEPOSIT_REQUEST);
                } else if (err_code == -2) {
                    showButtomToast("支付已取消！");
                }
            }
        }
    }
}
