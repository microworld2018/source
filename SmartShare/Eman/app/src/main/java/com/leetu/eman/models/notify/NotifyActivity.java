package com.leetu.eman.models.notify;


import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gn.myanimpulltorefreshlistview.pulltoimpl.PullToRefreshBase;
import com.gn.myanimpulltorefreshlistview.views.PullToRefreshListView;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.notify.adapter.NotifyAdapter;
import com.leetu.eman.models.notify.beans.NotifyBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;


/**
 * 通知页面
 */
public class NotifyActivity extends BaseActivity implements View.OnClickListener, NotifyContract.View {
    private TitleBar titleBar;
    private NotifyPresenter notifyPresenter;
    private PullToRefreshListView pullToRefreshListView;
    private List<NotifyBean> messages;
    private ListView mLstView;
    private NotifyAdapter notifyAdapter;
    private boolean isFrist = true;
    private LinearLayout listEmptyView;
    private TextView tvNoneRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);
        initView();
        loadData();
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_notify);
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.listview_cheap_all);
        mLstView = pullToRefreshListView.getRefreshableView();
        listEmptyView = (LinearLayout) findViewById(R.id.list_emptyview);
        tvNoneRecord = (TextView) findViewById(R.id.none_record);
        mLstView.setEmptyView(listEmptyView);

        notifyPresenter = new NotifyPresenter(this, this);
        messages = new ArrayList<>();
        notifyAdapter = new NotifyAdapter(this, messages);

        titleBar.setTitle("通知");
        tvNoneRecord.setText("暂无通知");
        titleBar.setLeftClickListener(this);

        mLstView.setAdapter(notifyAdapter);
        //接口问题暂时屏蔽
        pullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                pullToRefreshListView.onRefreshComplete();
//                notifyPresenter.loadData(false, false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                pullToRefreshListView.onRefreshComplete();
//                notifyPresenter.loadData(true, false);
            }
        });
    }


    protected void loadData() {
//        showLoading();
//        notifyPresenter.loadData(false, isFrist);  //接口问题 ，暂提示无消息

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                NotifyActivity.this.finish();
                break;
        }
    }

    @Override
    public void showLoadData(boolean isUp, List<NotifyBean> datas) {
        showProgressBar(false);
        showContent();
        isFrist = false;

        if (datas != null && datas.size() > 0) {
            if (!isUp) {
                messages.clear();
                messages.addAll(datas);
            } else {
                messages.addAll(datas);
            }
            notifyAdapter.notifyDataSetChanged();
        }
        pullToRefreshListView.onRefreshComplete();
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        notifyPresenter.loadData(false, true);
    }

    @Override
    public void contentFail() {
        super.contentFail();
    }

    @Override
    protected void onRetryLoadData() {
        showProgressBar(true);
        notifyPresenter.loadData(false, isFrist);
        super.onRetryLoadData();
    }

    @Override
    public void timeOutFail() {
        super.timeOutFail();
    }

    @Override
    public void showFail(String message) {
        super.showFail(message);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(NotifyActivity.class);
    }
}
