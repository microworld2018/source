package com.leetu.eman.models.currentorder;/**
 * Created by jyt on 2016/9/19.
 */

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.bumptech.glide.Glide;
import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.balance.BalanceActivity;
import com.leetu.eman.models.confirmorder.ReturnCarAddressActivity;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBean;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.models.currentorder.beans.PollingBean;
import com.leetu.eman.models.currentorder.carreport.ReportAccidentActivity;
import com.leetu.eman.models.currentorder.carreport.ReportCarActivity;
import com.leetu.eman.models.currentorder.carreport.ReportDestoryActivity;
import com.leetu.eman.models.notify.NotifyActivity;
import com.leetu.eman.models.orderrecord.OrdersRecordActivity;
import com.leetu.eman.models.peccancyrecord.PeccancyRecordActivity;
import com.leetu.eman.models.personalcenter.PersonalCenterActivity;
import com.leetu.eman.models.pollingupdataservice.PollingService;
import com.leetu.eman.models.returncar.ReturnCarActivity;
import com.leetu.eman.models.rnauthentication.RNAuthenticationActivity;
import com.leetu.eman.models.settings.SettingActivity;
import com.leetu.eman.models.takecar.views.MyOtherDialog;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.utils.StatusBarUtil;
import com.leetu.eman.views.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


/**
 * created by neo on 2016/9/19 13:31
 * 当前订单
 */
public class CurrentOrderActivity extends BaseActivity implements View.OnClickListener, MyOtherDialog.ClickListenerInterface, CurrentOrderContract.View, BaseActivity.MyLocationLisenter {
    private TitleBar titleBar;
    private MyOtherDialog myOtherDialog;
    private Button btReturnCar, btOpenDoor, btCloseDoor, btWhistle, btContacService;
    public static final String CURRENT_ORDER = "currentOrder";
    private CurrentOrderBean currentOrderBean;
    private TextView tvCarId, tvCarType, tvMileage, tvTime, tvCost, tvReturnName, tvReturnAddress, tvUserNo, tvMenuStatus;
    private CurrentOrderPresenter currentOrderPresenter;
    private int type;
    private LinearLayout ltResetAddress, ltDefaultAddress, ltMenu;
    public static String DOT_ID = "dotId";
    private static final int REQUEST = 1000;
    public static final int RESULT = 1001;
    private static final int SETTING = 1005;
    private View menuView;
    private String returnCarDotId;
    private ReturnCarPointBean returnCarPointBean;
    private RelativeLayout rtMenuBalance, rtMenuAccident, rtMenuNotify, rtMenuOrderRecord, rtAuthentication, rtMenuInvoice, rtMenuSetting,rtMenuService;
    private DrawerLayout drawerLayout;
    private ImageView ivPersonalPic, ivCarPic, ivDefaultAddress, ivNav;
    private boolean isChangeAddress = false;
    // 定义一个变量，来标识是否退出
    private static boolean isExit = false;

    //实时更新
    private Intent intent = null;
    private MyServiceConnection conn;
    private PollingService.MyBind myBinder;

    //客户端执行轮询的时间间隔
    public static int MLOOP_INTERVAL_SECS = 60;


    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_order);
        initView();
    }

    //开启实时更新服务
    private void startUpDataService() {
        if (conn == null) {
            conn = new MyServiceConnection();
        }
        if (intent == null) {
            intent = new Intent(CurrentOrderActivity.this, PollingService.class);
        }

        if (PollingService.isServiceRuning) {//服务是否绑定
        } else {
            bindService(intent, conn, Context.BIND_AUTO_CREATE);
        }
    }

    protected void initView() {

        titleBar = (TitleBar) findViewById(R.id.title_currentorder);
        btReturnCar = (Button) findViewById(R.id.bt_return_car);
        ivCarPic = (ImageView) findViewById(R.id.iv_current_order_pic);
        tvCarId = (TextView) findViewById(R.id.tv_current_order_carid);
        tvCarType = (TextView) findViewById(R.id.tv_current_order_cartype);
        tvMileage = (TextView) findViewById(R.id.tv_current_order_mileage);
        tvTime = (TextView) findViewById(R.id.tv_current_order_time);
        tvCost = (TextView) findViewById(R.id.tv_current_order_cost);
        tvReturnName = (TextView) findViewById(R.id.tv_current_order_rename);
        tvReturnAddress = (TextView) findViewById(R.id.tv_current_order_readdress);
        btOpenDoor = (Button) findViewById(R.id.bt_current_order_opendoor);
        btCloseDoor = (Button) findViewById(R.id.bt_current_order_closedoor);
        btWhistle = (Button) findViewById(R.id.bt_current_order_whistle);
        btContacService = (Button) findViewById(R.id.bt_current_order_contacservice);
        ltResetAddress = (LinearLayout) findViewById(R.id.lt_current_order_reset_readdress);
        ltDefaultAddress = (LinearLayout) findViewById(R.id.lt_current_order_default_readdress);
        ivDefaultAddress = (ImageView) findViewById(R.id.iv_current_order_default_address);
        ivNav = (ImageView) findViewById(R.id.iv_current_order_nav);
        //
        menuView = LayoutInflater.from(this).inflate(R.layout.left_menu, null);
        drawerLayout = (DrawerLayout) findViewById(R.id.dl_order);
        rtMenuBalance = (RelativeLayout) menuView.findViewById(R.id.rt_menu_balance);
        rtMenuAccident = (RelativeLayout) menuView.findViewById(R.id.rt_menu_accident);
        rtAuthentication = (RelativeLayout) menuView.findViewById(R.id.rt_rn_authentication);
        tvMenuStatus = (TextView) menuView.findViewById(R.id.tv_left_status);
        rtMenuOrderRecord = (RelativeLayout) menuView.findViewById(R.id.rt_menu_oredersrecord);
        rtMenuNotify = (RelativeLayout) menuView.findViewById(R.id.rt_menu_notify);
        rtMenuInvoice = (RelativeLayout) menuView.findViewById(R.id.rt_menu_invoice);
        rtMenuSetting = (RelativeLayout) menuView.findViewById(R.id.rt_menu_setting);
        rtMenuService = (RelativeLayout) menuView.findViewById(R.id.rt_menu_service);
        ivPersonalPic = (ImageView) menuView.findViewById(R.id.iv_personal_pic);
        tvUserNo = (TextView) menuView.findViewById(R.id.tv_user);
        ltMenu = (LinearLayout) findViewById(R.id.lt_order_left_menu);

        currentOrderPresenter = new CurrentOrderPresenter(this, this);
        returnCarPointBean = new ReturnCarPointBean();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        menuView.setLayoutParams(layoutParams);
        ltMenu.addView(menuView);

        titleBar.setRightIcon(R.mipmap.ic_take_car_menu);
        titleBar.setLeftIcon(R.mipmap.ic_personal_center);
        titleBar.showRightLayout();
        titleBar.showRightIcon();
        titleBar.setTitle("当前订单");
        titleBar.setRightClickListener(this);
        titleBar.setLeftClickListener(this);
        btReturnCar.setOnClickListener(this);
        btOpenDoor.setOnClickListener(this);
        btCloseDoor.setOnClickListener(this);
        btWhistle.setOnClickListener(this);
        ivNav.setOnClickListener(this);
        btContacService.setOnClickListener(this);
        ltResetAddress.setOnClickListener(this);
        ltDefaultAddress.setOnClickListener(this);
        rtMenuBalance.setOnClickListener(this);
        rtMenuAccident.setOnClickListener(this);
        rtAuthentication.setOnClickListener(this);
        rtMenuOrderRecord.setOnClickListener(this);
        rtMenuNotify.setOnClickListener(this);
        rtMenuInvoice.setOnClickListener(this);
        rtMenuSetting.setOnClickListener(this);
        ivPersonalPic.setOnClickListener(this);
        ltMenu.setOnClickListener(this);
        rtMenuService.setOnClickListener(this);
        setMyLocationLisenter(this);
        loadData();

    }

    @Override
    public void setstatusBar() {
        setStatusBarColor(0);
    }
    private void setStatusBarColor(int statusBarAlpha) {
        StatusBarUtil.setColorForDrawerLayout(this, (DrawerLayout) findViewById(R.id.dl_order), getResources()
                .getColor(R.color.main), statusBarAlpha);
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        if (LeTravelApplication.getSharePreferenceInstance(this).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(this).getToken().equals("")) {
            if (LeTravelApplication.getSharePreferenceInstance(this).getUserNumber() != null) {
                tvUserNo.setText(LeTravelApplication.getSharePreferenceInstance(this).getUserNumber());
            }
            if (LeTravelApplication.getSharePreferenceInstance(this).getUserStatus() != null) {
                try {
                    switch (Integer.parseInt(LeTravelApplication.getSharePreferenceInstance(this).getUserStatus())) {
                        case 1:
                            tvMenuStatus.setText("未认证");
                            tvMenuStatus.setTextColor(getResources().getColor(R.color.small_text));
                            break;
                        case 2:
                            tvMenuStatus.setText("审核中");
                            tvMenuStatus.setTextColor(getResources().getColor(R.color.rnaing));
                            break;
                        case 4:
                            tvMenuStatus.setText("未通过");
                            tvMenuStatus.setTextColor(getResources().getColor(R.color.red));
                            break;
                        case 3:
                            tvMenuStatus.setText("已审核");
                            tvMenuStatus.setTextColor(getResources().getColor(R.color.main));
                            break;
                        default:
                            tvMenuStatus.setText("");
                            break;
                    }
                } catch (Exception e) {

                }
            }
        }

        //重新开启实时计费
        if (myBinder != null) {
            myBinder.start(MLOOP_INTERVAL_SECS);
        }

        super.onResume();
    }


    private void loadData() {
        showLoading();
        currentOrderPresenter.loadData();
    }

    @Override
    public void showLoadData(CurrentOrderBean currentOrderBean) {
        if (currentOrderBean != null) {
            showContent();
            this.currentOrderBean = currentOrderBean;
            saveData();
        }
        startUpDataService();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                if (LeTravelApplication.getSharePreferenceInstance(this).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(this).getToken().equals("")) {
                    if (LeTravelApplication.getSharePreferenceInstance(this).getUserNumber() != null) {
                        tvUserNo.setText(LeTravelApplication.getSharePreferenceInstance(this).getUserNumber());
                    }
                }
                drawerLayout.openDrawer(ltMenu);
                break;
            case R.id.layout_right:
                if (myOtherDialog != null) {
                    myOtherDialog.show();
                } else {
                    myOtherDialog = new MyOtherDialog(this);
                    myOtherDialog.setClicklistener(this);
                    myOtherDialog.show();
                }
                break;
            case R.id.bt_return_car:
                Intent intent = new Intent(CurrentOrderActivity.this, ReturnCarActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ReturnCarActivity.ORDER_DETAIL, currentOrderBean);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.bt_current_order_opendoor:
                type = 1;
                showProgressBar(true);
                reGetLocation();
                break;
            case R.id.bt_current_order_closedoor:
                type = 2;
                showProgressBar(true);
                reGetLocation();
                break;
            case R.id.bt_current_order_whistle:
                type = 3;
                showProgressBar(true);
                reGetLocation();
                break;
            case R.id.bt_current_order_contacservice:
                showDialog("联系客服", "400-888-1212", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callPhone("400-888-1212");
                    }
                });
                break;
            case R.id.lt_current_order_reset_readdress:
                Intent intent2 = new Intent(CurrentOrderActivity
                        .this, ReturnCarAddressActivity.class);
                intent2.putExtra(DOT_ID, returnCarDotId);
                startActivityForResult(intent2, REQUEST);
                break;
            case R.id.lt_current_order_default_readdress:
                showProgressBar(true);
                currentOrderPresenter.defaultReturnDot(currentOrderBean.getCar().getCarId(), currentOrderBean.getOrderId());
                break;
            case R.id.rt_menu_balance:
                Intent intent3 = new Intent();
                intent3.setClass(CurrentOrderActivity.this, BalanceActivity.class);//余额
                startActivity(intent3);
                break;
            case R.id.rt_menu_accident:
                Intent intent9 = new Intent();
                intent9.setClass(CurrentOrderActivity.this, PeccancyRecordActivity.class);//违章事故处理
                startActivity(intent9);
                break;
            case R.id.rt_rn_authentication://认证
                Intent intent1 = new Intent(CurrentOrderActivity.this, RNAuthenticationActivity.class);
                startActivity(intent1);
                break;
            case R.id.rt_menu_oredersrecord:
                Intent intent4 = new Intent();
                intent4.setClass(CurrentOrderActivity.this, OrdersRecordActivity.class);//订单管理
                startActivity(intent4);
                drawerLayout.closeDrawer(ltMenu);
                break;
            case R.id.rt_menu_notify:
                Intent intent5 = new Intent();
                intent5.setClass(CurrentOrderActivity.this, NotifyActivity.class);//通知
                startActivity(intent5);
                break;
            case R.id.rt_menu_invoice:
                showButtomToast("功能开发中");
//                Intent intent6 = new Intent();
//                intent6.setClass(CurrentOrderActivity.this, InvoiceActivity.class);//索取发票
//                startActivity(intent6);
                break;
            case R.id.rt_menu_setting:
                Intent intent7 = new Intent();
                intent7.setClass(CurrentOrderActivity.this, SettingActivity.class);//设置
                startActivityForResult(intent7,SETTING);
                break;

            case R.id.rt_menu_service:
                drawerLayout.closeDrawer(ltMenu);
                showDialog("联系客服", "400-888-1212", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callPhone("400-888-1212");
                    }
                });
                break;
            case R.id.iv_personal_pic:
                Intent intent8 = new Intent();
                intent8.setClass(CurrentOrderActivity.this, PersonalCenterActivity.class);//个人中心
                startActivityForResult(intent8, REQUEST);
                break;

            case R.id.iv_current_order_nav:
                type = 4;
                reGetLocation();
                break;
        }
    }

    /**
     * 拨打电话
     */
    private void callPhone(String number) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + number);
        intent.setData(data);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    //设置数据
    void saveData() {
        if (currentOrderBean != null) {
            if (currentOrderBean.getCar() != null) {
                if (currentOrderBean.getCar().getAndroidModelPhoto() != null) {
                    Glide.with(this).load(currentOrderBean.getCar().getAndroidModelPhoto()).into(ivCarPic);
                    tvCarId.setText(currentOrderBean.getCar().getVehiclePlateId());
                    tvCarType.setText(currentOrderBean.getCar().getName());
                }
            }
            if (currentOrderBean.getDot() != null) {
                returnCarDotId = currentOrderBean.getDot().getDotId();
                tvReturnName.setText(currentOrderBean.getDot().getName());
                tvReturnAddress.setText(currentOrderBean.getDot().getAddress());
                returnCarPointBean.setName(currentOrderBean.getDot().getName());
                returnCarPointBean.setAddress(currentOrderBean.getDot().getAddress());
                returnCarPointBean.setLatlng(currentOrderBean.getDot().getLat() + "," + currentOrderBean.getDot().getLng());
            }
//            tvMileage.setText(currentOrderBean.getAllMile() + "公里");
//            tvTime.setText(currentOrderBean.getAlltime());
//            tvCost.setText("¥" + currentOrderBean.getAllPrice());

            tvMileage.setText("-");
            tvTime.setText("-");
            tvCost.setText("-");
        }
    }

    @Override
    public void carPoinion() {
        Intent intent = new Intent(CurrentOrderActivity.this, ReportCarActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(CURRENT_ORDER, currentOrderBean);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    @Override
    public void destoryReport() {
        Intent intent = new Intent(CurrentOrderActivity.this, ReportDestoryActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(CURRENT_ORDER, currentOrderBean);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void accdientReport() {
        Intent intent = new Intent(CurrentOrderActivity.this, ReportAccidentActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(CURRENT_ORDER, currentOrderBean);
        intent.putExtras(bundle);
        startActivity(intent);
    }


    @Override
    public void showopenOrCloseDoor() {
        showProgressBar(false);
        switch (type) {
            case 1:
                showButtomToast("车门已打开");
                break;
            case 2:
                showButtomToast("已锁车门");
                break;
            case 3:
                showButtomToast("鸣笛中...");
                break;
        }
    }

    @Override
    public void showUpDataReturnDot() {
        isChangeAddress = true;
        ivDefaultAddress.setImageResource(R.mipmap.ic_reset_default_returncar_ok);
        showProgressBar(false);
        tvReturnName.setText(returnCarPointBean.getName());
        tvReturnAddress.setText(returnCarPointBean.getAddress());
    }

    @Override
    public void showDefaultReturnDot(ReturnCarPointBean returnCarPointBean) {
        showProgressBar(false);
        ivDefaultAddress.setImageResource(R.mipmap.ic_reset_default_returncar);
        currentOrderBean.getDot().setDotId(returnCarPointBean.getDotid());
        currentOrderBean.getDot().setAddress(returnCarPointBean.getAddress());
        currentOrderBean.getDot().setName(returnCarPointBean.getName());
        String latlng[] = returnCarPointBean.getLatlng().split(",");
        currentOrderBean.getDot().setLat(Double.parseDouble(latlng[0]));
        currentOrderBean.getDot().setLng(Double.parseDouble(latlng[1]));
        returnCarPointBean.setName(currentOrderBean.getDot().getName());
        returnCarPointBean.setAddress(currentOrderBean.getDot().getAddress());
        returnCarPointBean.setLatlng(currentOrderBean.getDot().getLat() + "," + currentOrderBean.getDot().getLng());
        tvReturnName.setText(returnCarPointBean.getName());
        tvReturnAddress.setText(returnCarPointBean.getAddress());
    }


    @Override
    public void getLocation(BDLocation location) {
        if (type != 0) {
            if (type == 4) {
                if (currentOrderBean != null) {
                    if (currentOrderBean.getDot() != null) {
                        String latlng[] = returnCarPointBean.getLatlng().split(",");
                        navigationLocationApp(location.getLatitude(), location.getLongitude(), location.getBuildingName(), Double.parseDouble(latlng[0]), Double.parseDouble(latlng[1]), returnCarPointBean.getName(), 2);
                    }
                }

            } else {
                showProgressBar(true);
                currentOrderPresenter.openOrCloseDoor(currentOrderBean.getOrderId(), "" + type, currentOrderBean.getCar().getCarId(), location.getLatitude() + "", location.getLongitude() + "");
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST) {
            if (resultCode == RESULT) {
                returnCarPointBean = (ReturnCarPointBean) data.getExtras().getSerializable(ReturnCarAddressActivity.RE_ADDRESS);
                if (returnCarPointBean != null) {
                    showProgressBar(true);
                    currentOrderBean.getDot().setAddress(returnCarPointBean.getAddress());
                    currentOrderBean.getDot().setDotId(returnCarPointBean.getDotid());
                    currentOrderBean.getDot().setName(returnCarPointBean.getName());
                    String latlng[] = returnCarPointBean.getLatlng().split(",");
                    currentOrderBean.getDot().setLat(Double.parseDouble(latlng[0]));
                    currentOrderBean.getDot().setLng(Double.parseDouble(latlng[1]));
                    returnCarDotId = returnCarPointBean.getDotid();
                    currentOrderPresenter.upDataReturnDot(returnCarDotId, currentOrderBean.getOrderId());
                }
            }
        }else if(requestCode ==SETTING && resultCode == SettingActivity.RESULT_EXIT){
            finish();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onStop() {
        if (myBinder != null) {
            myBinder.stop();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        //停止计时器任务
        if (myBinder != null) {
            myBinder.stop();
        }

        EventBus.getDefault().unregister(this);
        if(conn != null) {
            unbindService(conn);
        }

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (drawerLayout.isDrawerOpen(ltMenu)) {
                drawerLayout.closeDrawers();
            } else {
                exit();
            }
        }
        return true;
    }

    @Override
    public void loginOk() {
        LeTravelApplication.getInstance().finishAllExcept(MainActivity.class);
        super.loginOk();
    }

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            // 利用handler延迟发送更改状态信息
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            finish();
            LeTravelApplication.getInstance().AppExit();
        }
    }


    //如果有订单，回调
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void upData(final PollingBean pollingBean) {//在ui线程执行
        if (pollingBean != null) {
            tvMileage.setText(pollingBean.getMileage() + "公里");
            tvTime.setText(pollingBean.getAlltime());
            tvCost.setText("￥" + pollingBean.getAllPrice());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void showToast(String message) {//在ui线程执行
        if (!TextUtils.isEmpty(message)) {
            showButtomToast(message);
        }
    }


    /**
     * 自定义一个类，实现ServiceConnection接口，并重写其两个方法
     *
     * @author Administrator
     */
    public class MyServiceConnection implements ServiceConnection {
        public void onServiceConnected(ComponentName name, IBinder service) {
            //得到PollingService.MyBinder对象，我们通过这个对象来操作服务中的方法
            myBinder = (PollingService.MyBind) service;
        }

        public void onServiceDisconnected(ComponentName name) {
        }
    }
}
