package com.leetu.eman.models.notify;


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.notify.beans.NotifyBean;

import java.util.List;

/**
 * Created by Administrator on 2016/11/5.
 */

public interface NotifyContract {
    interface View extends BaseContract {
        void showLoadData(boolean isUp, List<NotifyBean> datas);
    }

    interface UserAction {
        void loadData(boolean isUp, boolean isFrist);
    }
}
