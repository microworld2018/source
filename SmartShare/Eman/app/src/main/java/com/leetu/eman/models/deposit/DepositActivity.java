package com.leetu.eman.models.deposit;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.gn.myanimpulltorefreshlistview.pulltoimpl.PullToRefreshBase;
import com.gn.myanimpulltorefreshlistview.views.PullToRefreshListView;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.deposit.adapter.DepositRecordAdapter;
import com.leetu.eman.models.deposit.bean.DepositRecordBean;
import com.leetu.eman.models.deposit.bean.MyDepositBean;
import com.leetu.eman.models.orderrecord.OrdersRecordActivity;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.models.rnauthentication.RNAuthenticationActivity;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

public class DepositActivity extends BaseActivity implements DepositContract.View, View.OnClickListener {
    private TitleBar titleBar;
    private TextView tvDepositBlance;
    private Button btnRecharge;
    private PullToRefreshListView pullToRefreshListView;
    private DepositPresenter depositPresenter;
    private DepositRecordAdapter recordAdapter;
    private ListView mListView;
    public static final int REQUEST = 001;
    private List<DepositRecordBean> mList;
    private boolean isRechargeSuccess = false;
    private MyDepositBean mDepositBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);
        initView();
        initData();
    }

    private void initView() {
        titleBar = (TitleBar) findViewById(R.id.deposit_title);
        tvDepositBlance = (TextView) findViewById(R.id.tv_deposit_balance);
        btnRecharge = (Button) findViewById(R.id.btn_deposit_recharge);
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.listview_deposit);
        depositPresenter = new DepositPresenter(this, this);

        titleBar.setTitle("我的押金");
        titleBar.setRightText("退押金");
        titleBar.setmRightTextColor("#ffffff");
        titleBar.setLeftClickListener(this);
        titleBar.setRightClickListener(this);
        btnRecharge.setOnClickListener(this);

        mList = new ArrayList<>();

        recordAdapter = new DepositRecordAdapter(this, mList);
        mListView = pullToRefreshListView.getRefreshableView();
//        mListView.setDivider(getResources().getDrawable(R.color.line));
        mListView.setDividerHeight(0);
        mListView.setAdapter(recordAdapter);

        pullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                depositPresenter.load(false, false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                depositPresenter.load(true, false);
            }
        });
    }

    private void initData() {
        showProgressBar(true);
        depositPresenter.load(false, true);
    }

    @Override
    public void showLoad(boolean isUp, MyDepositBean myDepositBean) {
        showProgressBar(false);
        showContent();
        this.mDepositBean = myDepositBean;
        if (mDepositBean.getDeposit() >= mDepositBean.getStrategyMoney()) {
            btnRecharge.setEnabled(false);
        } else {
            btnRecharge.setEnabled(true);
        }

        tvDepositBlance.setText(mDepositBean.getDeposit() + "");

        List<DepositRecordBean> recordList = mDepositBean.getRecordList();
        if (recordList != null && recordList.size() > 0) {
            if (!isUp) {
                mList.clear();
                mList.addAll(recordList);
                recordAdapter.notifyDataSetChanged();
            } else {
                mList.addAll(recordList);
                recordAdapter.notifyDataSetChanged();
            }

        }
        pullToRefreshListView.onRefreshComplete();
    }

    @Override
    public void showCheckUserRNA(UserInfoBean userInfoBean) {
        showProgressBar(false);
        if (userInfoBean != null) {
            switch (userInfoBean.getState()) {
                case 1:
                    showDialog("", "身份未认证\n您要现在去认证吗？", "去认证", "取消", false, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(DepositActivity.this, RNAuthenticationActivity.class);
                            startActivity(intent);
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
                    break;
                case 2:
                    showMiddleToast("身份审核中");
                    break;
                case 4:
                    showDialog("", "审核未通过\n是否修改个人信息", "是", "否", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(DepositActivity.this, RNAuthenticationActivity.class);
                            startActivity(intent);
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }, new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {

                        }
                    });
                    break;
                case 3:
                    if (TextUtils.isEmpty(userInfoBean.getHandHeldIdCard())) {
                        showDialog("", "补齐身份后才可充值押金", "去补齐", "取消", false, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(DepositActivity.this, RNAuthenticationActivity.class);
                                startActivity(intent);
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }, new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {

                            }
                        });
                    } else {
                        Intent intent = new Intent(DepositActivity.this, DepositRechargeActivity.class);
                        startActivityForResult(intent, REQUEST);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void showReturnDeposit() {
        showButtomToast("提交成功");
        isRechargeSuccess = true;
        depositPresenter.load(false, true);
    }

    @Override
    public void showUnCompleteOrder() {
        showProgressBar(false);
        showDialog("", "您有未完成订单，暂不能退押金！", "查看订单", "取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(DepositActivity.this, OrdersRecordActivity.class);
                startActivity(intent);
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                if (isRechargeSuccess) {
                    Intent intent1 = getIntent();
                    setResult(200, intent1);
                }

                DepositActivity.this.finish();
                break;
            case R.id.layout_right:
                if (mDepositBean.getDeposit() > 0.0) {
                    showDpositDialog("", "您确定要退押金么？\n退押金后将无法用车哦！", "(预计15个工作日到账)", "确定", "取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            showProgressBar(true);
                            depositPresenter.retrunDeposit();
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    });
                } else {
                    showButtomToast("押金余额大于0元才可退款哦！");
                }

                break;
            case R.id.btn_deposit_recharge:
                depositPresenter.checkUserRNA();
                break;
        }
    }

    @Override
    public void loginOk() {
        showLoading();
        depositPresenter.load(false, false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(DepositActivity.class);
    }

    @Override
    public void showFail(String message) {
        showProgressBar(false);
        pullToRefreshListView.onRefreshComplete();
        super.showFail(message);
    }


    @Override
    protected void onRetryLoadData() {
        super.onRetryLoadData();
        showLoading();
        depositPresenter.load(false, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST) {
            if (resultCode == 2) {
                isRechargeSuccess = true;
                depositPresenter.load(false, false);
            }
        }
    }
}
