package com.leetu.eman.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.leetu.eman.R;


/**
 * Created by 郭宁 on 2015/5/28.
 */
public class MyActivityShareDialog extends Dialog implements View.OnClickListener {

    private final Context context;
    private TextView tvShareWechat, tvCancleCircle;
    private ClickListenerInterface clickListenerInterface;

    public MyActivityShareDialog(Context context) {

        super(context, R.style.selectorDialog);
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_activity_share, null);
        setContentView(view);

        tvShareWechat = (TextView) findViewById(R.id.tv_start_wxcircle);
        tvCancleCircle = (TextView) findViewById(R.id.tv_cancle_wx);

        //设置按钮监听
        tvShareWechat.setOnClickListener(this);
        tvCancleCircle.setOnClickListener(this);

        Window dialogWindow = getWindow();

        dialogWindow.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        dialogWindow.setWindowAnimations(R.style.mystyle);  //添加动画
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();
        lp.width = (int) (d.widthPixels * 1);
        dialogWindow.setAttributes(lp);
    }

    public void setClicklistener(ClickListenerInterface clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_start_wxcircle:
                if(clickListenerInterface != null) {
                    clickListenerInterface.wechat();
                }
                break;

            case R.id.tv_cancle_wx:
                break;
        }
        MyActivityShareDialog.this.cancel();
    }

    public interface ClickListenerInterface {
        void wechat();

    }

}
