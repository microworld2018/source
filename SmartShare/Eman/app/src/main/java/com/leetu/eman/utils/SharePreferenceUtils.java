package com.leetu.eman.utils;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by neo on 15/5/28.
 */
public class SharePreferenceUtils {

    private static final String FILE_NAME = "user_info";

    private static final String USER_NUMBER = "userNumber";
    private static final String USER_PASSWORD = "userPassword";
    private static final String WELCOME_VERSION = "welcomeVersion";
    private static final String AREA_ID = "areaId";
    private static final String AREA_NAME = "areaName";
    private static final String TOKEN = "token";
    private static final String FIRST = "first";
    private static final String USER_STATUS = "userStatus";
    private static final String USER_STATUS_HANDIDCAD_IMG = "userStatus_handidcars_img";
    private static final String TEST_SELECT = "testSelect";
    private static final String AD_TIME= "adtime";
    private static final String AD_URL = "adimg_url";
    private static final String AD_DETAIL_URL = "addetailimg_url";


    private SharedPreferences mSharedPreferences;

    public void saveUserStatus(int userStatus,String idUrl) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(USER_STATUS), DataEncrypt.getEncryptStr("" + userStatus)).commit();
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(USER_STATUS_HANDIDCAD_IMG), DataEncrypt.getEncryptStr(idUrl)).commit();
    }

    public String getUserStatus() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(USER_STATUS), null));
    }
    public String getHandIdCarImg() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(USER_STATUS_HANDIDCAD_IMG), null));
    }

    public void saveFirst(String isFirst) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(FIRST), DataEncrypt.getEncryptStr(isFirst)).commit();
    }

    public String getFirst() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(FIRST), null));
    }

    public void saveAdTime(String time) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(AD_TIME), DataEncrypt.getEncryptStr(time)).commit();
    }

    public String getAdTime() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(AD_TIME), null));
    }

    public void saveAdImgUrl(String url) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(AD_URL), DataEncrypt.getEncryptStr(url)).commit();
    }

    public String getAdImgUrl() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(AD_URL), null));
    }


    public void saveAdDetailUrl(String detailUrl) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(AD_DETAIL_URL), DataEncrypt.getEncryptStr(detailUrl)).commit();
    }

    public String getAdDetailUrl() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(AD_DETAIL_URL), null));
    }

    public SharePreferenceUtils(Context context) {
        mSharedPreferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public void saveToken(String token) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(TOKEN), DataEncrypt.getEncryptStr(token)).commit();
    }

    public String getToken() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(TOKEN), null));
    }

    public void saveUserNumber(String userINumber) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(USER_NUMBER), DataEncrypt.getEncryptStr(userINumber)).commit();
    }

    public String getUserNumber() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(USER_NUMBER), null));
    }

    public void saveUserPassword(String userPassword) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(USER_PASSWORD), DataEncrypt.getEncryptStr(userPassword)).commit();
    }

    public String getUserPassword() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(USER_PASSWORD), null));
    }

    public void saveWelcomeVersion(String welcomeVersion) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(WELCOME_VERSION), DataEncrypt.getEncryptStr(welcomeVersion)).commit();
    }

    public String getWelcomeVersion() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(WELCOME_VERSION), null));
    }

    public void saveAreaId(String areaId) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(AREA_ID), DataEncrypt.getEncryptStr(areaId)).commit();
    }

    public String getAreaId() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(AREA_ID), null));
    }

    public void saveAreaName(String areaName) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(AREA_NAME), DataEncrypt.getEncryptStr(areaName)).commit();
    }

    public String getAreaName() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(AREA_NAME), null));
    }

    //测试专用
    public void saveTestSelect(String testSelect) {
        mSharedPreferences.edit().putString(DataEncrypt.getEncryptStr(TEST_SELECT), DataEncrypt.getEncryptStr(testSelect)).commit();
    }

    public String getTestSelect() {
        return DataEncrypt.getDecrypt(mSharedPreferences.getString(DataEncrypt.getEncryptStr(TEST_SELECT), null));
    }
}
