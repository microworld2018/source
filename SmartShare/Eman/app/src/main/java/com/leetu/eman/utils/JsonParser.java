package com.leetu.eman.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.TypeReference;
import com.leetu.eman.BuildConfig;


import java.util.HashMap;
import java.util.List;

/**
 * 解析json的工具类
 */
public class JsonParser {

    /**
     * 将json解析成对象
     *
     * @param json
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> T getParsedData(String json, Class<T> cls) {

        if (json == null || "".equals(json.trim())) {
            return null;
        }

        try {
            return JSON.parseObject(json, cls);
        } catch (JSONException e) {

            if (BuildConfig.IS_DEBUG) {
                throw new JSONException(e.getMessage());
            }

            return null;
        }
    }

    /**
     * 将json解析成列表
     *
     * @param json
     * @param cls
     * @param <T>
     * @return
     */

    public static <T> List<T> getParsedDataToList(String json, Class<T> cls) {

        if (json == null || "".equals(json.trim())) {
            return null;
        }

        try {
            return JSON.parseArray(json, cls);
        } catch (JSONException e) {
            if (BuildConfig.IS_DEBUG) {
                throw new JSONException(e.getMessage());
            }
            return null;
        }

    }

    /**
     * 将json数据解析成map
     *
     * @param json
     * @param cls
     * @param <T>
     * @return
     */
    public static <T> HashMap<String, T> getParsedDataToMap(String json, Class<T> cls) {

        if (json == null || "".equals(json.trim())) {
            return null;
        }

        try {
            return JSON.parseObject(json,
                    new TypeReference<HashMap<String, T>>() {
                    });
        } catch (JSONException e) {

            if (BuildConfig.IS_DEBUG) {
                throw new JSONException(e.getMessage());
            }

            return null;
        }

    }


}
