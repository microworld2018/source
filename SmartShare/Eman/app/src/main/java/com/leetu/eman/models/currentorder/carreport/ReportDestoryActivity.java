package com.leetu.eman.models.currentorder.carreport;/**
 * Created by jyt on 2016/9/20.
 */

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.currentorder.CurrentOrderActivity;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.views.CustomEditText;
import com.leetu.eman.views.PhotoFragment;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * created by neo on 2016/9/20 11:04
 * 车辆损坏上报
 */
public class ReportDestoryActivity extends BaseActivity implements View.OnClickListener, ReportDestoryContract.View, CompoundButton.OnCheckedChangeListener, PhotoFragment.OnSelectPicListener {
    private TitleBar titleBar;
    private CurrentOrderBean currentOrderBean;
    private Button btReportDestory;
    private PhotoFragment fragment;
    private ReportDestoryPresenter reportDestoryPresenter;
    private CheckBox cbFace, cbTyre, cbInterior, cbGlass, cbOthers, cbSmell;
    private String breakTypeFacade = "0", breakTypeTyre = "0", breakTypeDecoration = "0", breakTypeGlass = "0", breakTypeOther = "0", breakTypeOdor = "0", breakDesc;
    private CustomEditText etDesc;
    private TextView desNum;
    private boolean isWrite = false;
    private boolean isSelect = false;
    private boolean isCheck = false;
    private List<CheckBox> ltCheckBoxs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_destory);
        initView();
    }

    protected void initView() {
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getSerializable(CurrentOrderActivity.CURRENT_ORDER) != null) {
                currentOrderBean = (CurrentOrderBean) getIntent().getExtras().getSerializable(CurrentOrderActivity.CURRENT_ORDER);
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_destory);
        btReportDestory = (Button) findViewById(R.id.bt_reportdestory);
        cbFace = (CheckBox) findViewById(R.id.cb_face_destory);
        cbTyre = (CheckBox) findViewById(R.id.cb_tyre_destory);
        cbInterior = (CheckBox) findViewById(R.id.cb_Interior_destory);
        cbGlass = (CheckBox) findViewById(R.id.cb_glass_destory);
        cbOthers = (CheckBox) findViewById(R.id.cb_others_destory);
        cbSmell = (CheckBox) findViewById(R.id.cb_Interior_smell);
        etDesc = (CustomEditText) findViewById(R.id.et_destory_desc);
        desNum = (TextView) findViewById(R.id.text_num);

        savePhotoFragment();
        reportDestoryPresenter = new ReportDestoryPresenter(this, this);
        ltCheckBoxs = new ArrayList<>();

        ltCheckBoxs.add(cbFace);
        ltCheckBoxs.add(cbTyre);
        ltCheckBoxs.add(cbInterior);
        ltCheckBoxs.add(cbGlass);
        ltCheckBoxs.add(cbOthers);
        ltCheckBoxs.add(cbSmell);
        titleBar.setTitle("车辆损坏");
        titleBar.setLeftClickListener(this);
        desNum.setHint("损坏与问题描述");
        btReportDestory.setOnClickListener(this);
        cbFace.setOnCheckedChangeListener(this);
        cbTyre.setOnCheckedChangeListener(this);
        cbInterior.setOnCheckedChangeListener(this);
        cbGlass.setOnCheckedChangeListener(this);
        cbOthers.setOnCheckedChangeListener(this);
        cbSmell.setOnCheckedChangeListener(this);
        etDesc.setTextNumChangeListener(new CustomEditText.TextNumChangeListener() {
            @Override
            public void onNumChangeListener(boolean isChange) {
                if (!etDesc.getText().toString().trim().equals("")) {
                    isWrite = true;
                } else {
                    isWrite = false;
                }
                btReportDestory.setEnabled(checkAllData());
            }
        });
    }

    void savePhotoFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.
                beginTransaction();
        fragment = new PhotoFragment();
        fragment.setOnSelectPicListener(this);
        //加到Activity中
        fragmentTransaction.add(R.id.lt_reportdestory_fragment_content, fragment);
        //加到后台堆栈中，有下一句代码的话，点击返回按钮是退到Activity界面，没有的话，直接退出Activity
        //后面的参数是此Fragment的Tag。相当于id
//        fragmentTransaction.addToBackStack("fragment1");
        //记住提交
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                ReportDestoryActivity.this.finish();
                break;
            case R.id.bt_reportdestory:
                List<String> list = new ArrayList<>();
                if (fragment.factSelected.size() != 0) {
                    for (int i = 0; i < fragment.factSelected.size(); i++) {
                        list.add(fragment.factSelected.get(i).getOriginalPath());
                    }
                }
                if (currentOrderBean != null) {
                    breakDesc = etDesc.getText().toString().trim();
                    showProgressBar(true);
                    reportDestoryPresenter.upData("", "", currentOrderBean.getOrderId(), "", currentOrderBean.getCar().getCarId(), currentOrderBean.getCar().getVehiclePlateId(), breakTypeFacade, breakTypeTyre, breakTypeDecoration, breakTypeGlass, breakTypeOther, breakTypeOdor, breakDesc, list);
                }
                break;
        }
    }

    @Override
    public void showUpData() {
        showButtomToast("提交成功");
        showProgressBar(false);
        ReportDestoryActivity.this.finish();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.cb_face_destory:
                if (isChecked) {
                    breakTypeFacade = "1";
                } else {
                    breakTypeFacade = "0";
                }
                break;
            case R.id.cb_tyre_destory:
                if (isChecked) {
                    breakTypeTyre = "1";
                } else {
                    breakTypeTyre = "0";
                }
                break;
            case R.id.cb_Interior_destory:
                if (isChecked) {
                    breakTypeDecoration = "1";
                } else {
                    breakTypeDecoration = "0";
                }
                break;
            case R.id.cb_glass_destory:
                if (isChecked) {
                    breakTypeGlass = "1";
                } else {
                    breakTypeGlass = "0";
                }
                break;
            case R.id.cb_others_destory:
                if (isChecked) {
                    breakTypeOther = "1";
                } else {
                    breakTypeOther = "0";
                }
                break;
            case R.id.cb_Interior_smell:
                if (isChecked) {
                    breakTypeOdor = "1";
                } else {
                    breakTypeOdor = "0";
                }
                break;
            default:
                break;
        }
        for (int i = 0; i < ltCheckBoxs.size(); i++) {
            if (ltCheckBoxs.get(i).isChecked()) {
                isCheck = true;
                break;
            }
            isCheck = false;
        }
        btReportDestory.setEnabled(checkAllData());
    }

    @Override
    public void selectPic(int size) {
        if (size > 0) {
            isSelect = true;
            btReportDestory.setEnabled(true);
        } else {
            btReportDestory.setEnabled(false);
            isSelect = false;
        }
        btReportDestory.setEnabled(checkAllData());
    }

    boolean checkAllData() {
        if (isWrite || isCheck || isSelect) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showContent();
    }
}
