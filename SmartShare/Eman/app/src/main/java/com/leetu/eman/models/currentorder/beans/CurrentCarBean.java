package com.leetu.eman.models.currentorder.beans;/**
 * Created by gn on 2016/10/9.
 */

import java.io.Serializable;

/**
 * created by neo on 2016/10/9 13:45
 */
public class CurrentCarBean implements Serializable {
    String androidModelPhoto;
    String bizState;
    int version;
    String name;
    String carId;
    String iosModelPhoto;
    String vehiclePlateId;
    String cnName;

    public String getAndroidModelPhoto() {
        return androidModelPhoto;
    }

    public void setAndroidModelPhoto(String androidModelPhoto) {
        this.androidModelPhoto = androidModelPhoto;
    }

    public String getBizState() {
        return bizState;
    }

    public void setBizState(String bizState) {
        this.bizState = bizState;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getIosModelPhoto() {
        return iosModelPhoto;
    }

    public void setIosModelPhoto(String iosModelPhoto) {
        this.iosModelPhoto = iosModelPhoto;
    }

    public String getVehiclePlateId() {
        return vehiclePlateId;
    }

    public void setVehiclePlateId(String vehiclePlateId) {
        this.vehiclePlateId = vehiclePlateId;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }
}
