package com.leetu.eman.models.usecar.bean;/**
 * Created by jyt on 2016/9/22.
 */

/**
 * created by neo on 2016/9/22 16:23
 * 距离用户最近的点
 */
public class NearUserPoint {
    String distance;
    String ulatlng;
    String dotname;
    String dlatlng;
    String dotId;
    int carcount;

    public int getCarcount() {
        return carcount;
    }

    public void setCarcount(int carcount) {
        this.carcount = carcount;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getUlatlng() {
        return ulatlng;
    }

    public void setUlatlng(String ulatlng) {
        this.ulatlng = ulatlng;
    }

    public String getDotname() {
        return dotname;
    }

    public void setDotname(String dotname) {
        this.dotname = dotname;
    }

    public String getDlatlng() {
        return dlatlng;
    }

    public void setDlatlng(String dlatlng) {
        this.dlatlng = dlatlng;
    }

    public String getDotId() {
        return dotId;
    }

    public void setDotId(String dotId) {
        this.dotId = dotId;
    }

    @Override
    public String toString() {
        return "NearUserPoint{" +
                "distance='" + distance + '\'' +
                ", ulatlng='" + ulatlng + '\'' +
                ", dotname='" + dotname + '\'' +
                ", dlatlng='" + dlatlng + '\'' +
                ", dotId='" + dotId + '\'' +
                ", carcount=" + carcount +
                '}';
    }
}
