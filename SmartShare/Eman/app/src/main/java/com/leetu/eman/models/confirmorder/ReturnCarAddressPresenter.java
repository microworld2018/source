package com.leetu.eman.models.confirmorder;/**
 * Created by jyt on 2016/9/27.
 */

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBean;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBeans;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

import java.util.List;


/**
 * created by neo on 2016/9/27 13:17
 */
public class ReturnCarAddressPresenter extends BasePresenter implements ReturnCarAddressContract.UserAction {
    private Context context;
    private ReturnCarAddressContract.View returnCarAddressListener;

    public ReturnCarAddressPresenter(Context context, ReturnCarAddressContract.View returnCarAddressListener) {
        this.context = context;
        this.returnCarAddressListener = returnCarAddressListener;
    }

    @Override
    public void load(String dotId, boolean isUP, final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.REBACK_CAR_DOTS)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("dotId", dotId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), returnCarAddressListener)) {
                                LogUtils.e("gn", "可还车网点" + response.getData());
                                ReturnCarPointBeans returnCarPointBeans = JsonParser.getParsedData(response.getData(), ReturnCarPointBeans.class);
                                List<ReturnCarPointBean> returnCarPoints = returnCarPointBeans.getDots();
                                returnCarAddressListener.showLoad(returnCarPoints);
                            } else {
                                if (response.getResultCode() != 206) {
                                    if (isFrist) {
                                        returnCarAddressListener.contentFail();
                                    } else {
                                        returnCarAddressListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            returnCarAddressListener.timeOutFail();
                        }
                    });
        } else {
            if (isFrist) {
                returnCarAddressListener.contentFail();
            } else {
                returnCarAddressListener.showFail(context.getResources().getString(R.string.net_error));
            }
        }
    }
}
