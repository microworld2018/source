package com.leetu.eman.models.takecar;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.views.TitleBar;

import static com.leetu.eman.models.takecar.TakeCarActivity.TAKE_CAR_CANCLE;


/**
 * Created by Administrator on 2016/11/23.
 */

public class CancelOrderActivity extends BaseActivity implements View.OnClickListener {
    private TitleBar titleBar;
    private Button btHome, btAgain;
    public static final int GO_HOME = 10000;
    public static final int AGAIN = 10001;
    public static final int LEFT_BACK = 10002;
    private int takecar;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitivty_cancel_order);
        initView();
        intent = getIntent();
        if (intent != null) {
            takecar = intent.getIntExtra(TAKE_CAR_CANCLE, 0);
        }
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_cancelorder);
        btHome = (Button) findViewById(R.id.bt_go_home);
        btAgain = (Button) findViewById(R.id.bt_order_again);


        titleBar.setTitle("订单取消");
        titleBar.setLeftClickListener(this);
        btHome.setOnClickListener(this);
        btAgain.setOnClickListener(this);
    }


    protected void loadData() {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.bt_go_home:
                if (intent != null) {
                    setResult(GO_HOME, intent);
                }
                break;
            case R.id.bt_order_again:
                setResult(AGAIN, intent);
                break;
            case R.id.layout_left:
                if (intent != null) {
                    if (takecar == 1) {
                        setResult(LEFT_BACK, intent);
                    }
                }

                break;
        }
        CancelOrderActivity.this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (takecar == 1) {
                setResult(GO_HOME, intent);
            }
            CancelOrderActivity.this.finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void loginOk() {
        super.loginOk();
    }
}
