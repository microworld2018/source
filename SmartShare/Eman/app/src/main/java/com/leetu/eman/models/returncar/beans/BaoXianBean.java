package com.leetu.eman.models.returncar.beans;

/**
 * Created by lvjunfeng on 2017/5/8.
 */

public class BaoXianBean {
    private String insuranceName;
    private String insuranceFee;

    public String getInsuranceName() {
        return insuranceName;
    }

    public void setInsuranceName(String insuranceName) {
        this.insuranceName = insuranceName;
    }

    public String getInsuranceFee() {
        return insuranceFee;
    }

    public void setInsuranceFee(String insuranceFee) {
        this.insuranceFee = insuranceFee;
    }
}
