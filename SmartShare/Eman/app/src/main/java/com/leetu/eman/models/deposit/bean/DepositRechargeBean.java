package com.leetu.eman.models.deposit.bean;

/**
 * Created by lvjunfeng on 2017/4/12.
 */

public class DepositRechargeBean {
    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
