package com.leetu.eman.models.pollingupdataservice;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.models.currentorder.beans.PollingBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.Timer;
import java.util.TimerTask;

import static com.leetu.eman.models.currentorder.CurrentOrderActivity.MLOOP_INTERVAL_SECS;


/**
 * 作者：lvjunfeng on 2017/3/15 20:38
 * 作用：实时更新价格服务
 */
public class PollingService extends Service {
    /**
     * 当前服务是否正在执行
     */
    public static boolean isServiceRuning = false;
    private static Timer timer;
    private IntenterBoradCastReceiver receiver;

    public PollingService() {
        isServiceRuning = false;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        LogUtils.e("lv", "onBind");
        if (isServiceRuning) {
        } else {
            // 启动轮询拉取消息
            startLoop(MLOOP_INTERVAL_SECS);
        }
        return new MyBind();
    }

    /**
     * 启动轮询拉去消息
     */
    public void startLoop(final int interval) {
        LogUtils.e("lv", "启动Timer任务");
        if (timer == null) {
            timer = new Timer();
        }
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                isServiceRuning = true;
                if (NetworkHelper.isNetworkConnect(LeTravelApplication.getInstance())) {
                    HttpEngine.post()
                            .url(URLS.CHECK_ORDER_POLLING)
                            .addParam("token", LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).getToken())
                            .readTimeout(30000)
                            .writeTimeout(30000)
                            .connectTimeout(30000)
                            .tag(PollingService.class)
                            .execute(new HttpEngine.ResponseCallback() {
                                @Override
                                public void onResponse(ResponseStatus response) {
                                    LogUtils.e("lv", "轮询服务中请求接口成功response:" + response.getData());

                                    if (response.getData() != null) {
                                        checkData(response);
                                    }
                                }

                                @Override
                                public void onFailure(Exception error) {
                                    LogUtils.e("lv", "轮询服务中请求接口失败onFailure:" + error.getMessage());
                                    //重启timer
                                    stopLoop();
                                    startLoop(interval);
                                }
                            });
                } else {
                    //停止Timer任务
                    EventBus.getDefault().post("网络异常，请检查网络");
                    stopLoop();
                    registerNetBroadrecevicer();
                }
            }
        }, 0, interval * 1000);
    }

    /**
     * 停止轮询
     */
    public void stopLoop() {
        HttpEngine.cancelRequest(PollingService.class);
        if (timer != null) {
            timer.cancel();
            timer = null;//必须null
            LogUtils.e("lv", "停止Timer任务");
        }
    }

    private void checkData(ResponseStatus response) {
        PollingBean pollingBean = JsonParser.getParsedData(response.getData(), PollingBean.class);
        if (pollingBean != null) {
            EventBus.getDefault().post(pollingBean);
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        LogUtils.e("lv", "onUnbind");
        isServiceRuning = false;
        HttpEngine.cancelRequest(PollingService.class);
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        stopSelf();
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        HttpEngine.cancelRequest(PollingService.class);
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }
        super.onDestroy();
    }

    public class MyBind extends Binder {
        public void start(int interval) {
            startLoop(interval);
        }

        public void stop() {
            stopLoop();
        }
    }


    //注册网络变化的广播
    private void registerNetBroadrecevicer() {
        LogUtils.e("lv", "注册发送网络变化广播");
        //获取广播对象
        if (receiver == null) {
            receiver = new IntenterBoradCastReceiver();
        }
        IntentFilter filter = new IntentFilter();
        //添加动作，监听网络
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver, filter);
    }

    //监听网络状态变化的广播接收器
    public class IntenterBoradCastReceiver extends BroadcastReceiver {

        private ConnectivityManager mConnectivityManager;
        private NetworkInfo netInfo;

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                mConnectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                netInfo = mConnectivityManager.getActiveNetworkInfo();
                if (netInfo != null && netInfo.isAvailable()) {
                    //解除广播
                    if (receiver != null) {
                        unregisterReceiver(receiver);
                        receiver = null;
                        LogUtils.e("lv", "网络已连接检测广播已解除");
                    }
                    startLoop(MLOOP_INTERVAL_SECS);
                } else {
                    stopLoop();
                }

            }

        }
    }
}