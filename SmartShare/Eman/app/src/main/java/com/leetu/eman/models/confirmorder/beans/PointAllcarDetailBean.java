package com.leetu.eman.models.confirmorder.beans;/**
 * Created by jyt on 2016/9/23.
 */

import java.util.List;

/**
 * created by neo on 2016/9/23 16:41
 * 所选网点所有的车辆信息
 */
public class PointAllcarDetailBean {
    List<CarDetailBean> cars;
    DefaultReturnCarPoint returnDot;

    public List<CarDetailBean> getCars() {
        return cars;
    }

    public void setCars(List<CarDetailBean> cars) {
        this.cars = cars;
    }

    public DefaultReturnCarPoint getReturnDot() {
        return returnDot;
    }

    public void setReturnDot(DefaultReturnCarPoint returnDot) {
        this.returnDot = returnDot;
    }
}
