package com.leetu.eman.models.currentorder;/**
 * Created by gn on 2016/10/9.
 */


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBean;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;

/**
 * created by neo on 2016/10/9 14:23
 */
public interface CurrentOrderContract {
    interface View extends BaseContract {
        void showLoadData(CurrentOrderBean currentOrderBean);

        void showopenOrCloseDoor();

        void showUpDataReturnDot();

        void showDefaultReturnDot(ReturnCarPointBean returnCarPointBean);

    }

    interface UserAction {
        void loadData();

        void openOrCloseDoor(String orderId, String state, String carId, String lat, String lng);

        void upDataReturnDot(String dotId, String orderId);

        void defaultReturnDot(String carId, String orderId);

    }
}
