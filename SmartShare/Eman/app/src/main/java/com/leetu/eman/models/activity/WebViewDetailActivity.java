package com.leetu.eman.models.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.activity.bean.WebDetailsBean;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.MyActivityShareDialog;
import com.leetu.eman.views.TitleBar;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.util.Stack;

/**
 * Created by Administrator on 2016/12/28.
 */

public class WebViewDetailActivity extends BaseActivity implements View.OnClickListener, MyActivityShareDialog.ClickListenerInterface, WebViewDetailContract.View {
    private WebView webView;
    private ProgressBar progressBar;
    private TitleBar titleBar;
    public static final String DETAIL_URL = "url";
    private String url;
    private boolean netFlag = true;
    private WebViewDetailPresent webViewDetailPresent;
    private String shareTitle = "";
    private String shareIcon = "";
    public static final String RETURN_MAINACTIVITY = "return_mainActivity_flag";//广告页flag
    private boolean returnMainActivity = false;//从广告页进入的

    public static final String JPUSH_FLAG = "jpush_flag";//推送通知flag
    private boolean jpush = false;//从推送通知进入的

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_detail);
        webView = (WebView) findViewById(R.id.wv_detail);
        progressBar = (ProgressBar) findViewById(R.id.progressline_bar);
        titleBar = (TitleBar) findViewById(R.id.title_webdetail);
        if (getIntent().getStringExtra(DETAIL_URL) != null) {
            url = getIntent().getStringExtra(DETAIL_URL);
        }

        returnMainActivity = getIntent().getBooleanExtra(RETURN_MAINACTIVITY, false);
        jpush = getIntent().getBooleanExtra(JPUSH_FLAG,false);



        WebSettings settings = webView.getSettings();
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        titleBar.setRightIcon(R.mipmap.iv_share);
        titleBar.showRightIcon();
        titleBar.setLeftClickListener(this);
        titleBar.setRightClickListener(this);

        webViewDetailPresent = new WebViewDetailPresent(this, this);
        settings.setJavaScriptEnabled(true);
        webView.addJavascriptInterface(new JavaScriptinterface(this), "jsObj");
        //设置Web视图
        webView.setWebViewClient(new webViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onReceivedTitle(WebView view, String title) {
                if (title != null) {
                    titleBar.setTitle(title);//a textview
                    shareTitle = title;
                }
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progressBar.setProgress(newProgress);
                super.onProgressChanged(view, newProgress);
            }
        });
        webView.loadUrl(url);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                if (returnMainActivity) {//广告页进入
                    Intent intent = new Intent(WebViewDetailActivity.this, MainActivity.class);
                    startActivity(intent);
                    WebViewDetailActivity.this.finish();
                } else if(jpush){//推送进入

                    Stack<Activity> activitys = LeTravelApplication.getInstance().getAllActivity();
                    if(activitys.size()>1) {
//                        Activity activity = activitys.get(activitys.size() - 1);
                        WebViewDetailActivity.this.finish();
                    }else {
                        Intent intent = new Intent(WebViewDetailActivity.this, MainActivity.class);
                        startActivity(intent);
                        WebViewDetailActivity.this.finish();
                    }

                }else {
                    WebViewDetailActivity.this.finish();
                }
                break;
            case R.id.layout_right:
                MyActivityShareDialog shareDialog = new MyActivityShareDialog(this);
                shareDialog.setCanceledOnTouchOutside(true);
                shareDialog.setClicklistener(this);
                shareDialog.show();
                break;
        }
    }

    @Override
    public void wechat() {
        UMImage image;
        if (TextUtils.isEmpty(shareIcon)) {
            image = new UMImage(WebViewDetailActivity.this, R.mipmap.ic_share_cicle);
        } else {
            image = new UMImage(WebViewDetailActivity.this, shareIcon);
        }

        ShareAction shareAction = new ShareAction(WebViewDetailActivity.this).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE);

        shareAction.withTitle(shareTitle)
                .withMedia(image)
                .withTargetUrl(url)
                .setCallback(umShareListener)
                .share();
    }

    @Override
    public void showShareResault(int code) {

    }


    private class webViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains("goLogin")) {
                if (checkUserStatus()) {
                    showMiddleToast("您已是老用户");
                } else {
                    showLogin();
                }
            } else {
                if (url != null) {
                    webView.loadUrl(url);
                }
            }
            return true;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            LogUtils.e("lv", "errorCode:" + errorCode); //-6服务器异常  -2网络异常
            netFlag = true;
            if (errorCode == -2) {
                contentFail();
            } else if (errorCode == -6) {
                showLoading();
            }

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            progressBar.setVisibility(View.GONE);
            if (!netFlag) {
                showContent();
            }
        }


        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void loginOk() {
        super.loginOk();
        if(returnMainActivity) {
            Intent intent = new Intent(WebViewDetailActivity.this, MainActivity.class);
            startActivity(intent);
            WebViewDetailActivity.this.finish();
        }else {
            LeTravelApplication.getInstance().finishAllExcept(MainActivity.class);
        }
    }

    /**
     * 检查用户状态
     */
    private boolean checkUserStatus() {
        if (LeTravelApplication.getSharePreferenceInstance(this).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(this).getToken().equals("")) {
            if (LeTravelApplication.getSharePreferenceInstance(this).getUserNumber() != null) {
                if (LeTravelApplication.getSharePreferenceInstance(this).getUserNumber().equals("")) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public class JavaScriptinterface {
        Context context;

        public JavaScriptinterface(Context c) {
            context = c;
        }

        /**
         * 与js交互时用到的方法，在js里直接调用的获取用户信息
         */
        @JavascriptInterface
        public String getUserInfo() {
            if (checkUserStatus()) {
                String userNumber = LeTravelApplication.getSharePreferenceInstance(context).getUserNumber();
                return userNumber;
            } else {
                showButtomToast("您当前状态未登录");
                return null;
            }
        }

        /**
         * 与js交互时用到的方法，在js里直接调用的获取用户信息
         */
        @JavascriptInterface
        public void Htmlcallactive(String shareInfo) {
            LogUtils.e("lv", "jscalljava了：" + shareInfo);
            if (!TextUtils.isEmpty(shareInfo)) {
                WebDetailsBean parsedData = JsonParser.getParsedData(shareInfo, WebDetailsBean.class);
                if (parsedData != null) {
                    WebViewDetailActivity.this.shareTitle = parsedData.getTitle();
                    WebViewDetailActivity.this.shareIcon = parsedData.getShareIco();
                }

            }

        }
    }

    @Override
    protected void onRetryLoadData() {
        super.onRetryLoadData();
        netFlag = false;
        webView.loadUrl(url);

    }

    @Override
    protected void onDestroy() {
        if (webView != null) {
            webView.clearHistory();
            ((ViewGroup) webView.getParent()).removeView(webView);
            webView.destroy();
            webView = null;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            if (returnMainActivity) {
                Intent intent = new Intent(WebViewDetailActivity.this, MainActivity.class);
                startActivity(intent);
                WebViewDetailActivity.this.finish();
            } else if(jpush){//推送进入

                Stack<Activity> activitys = LeTravelApplication.getInstance().getAllActivity();
                if(activitys.size()>1) {//前端展示时点击消息，返回之前的页面
                    WebViewDetailActivity.this.finish();
                }else {
                    Intent intent = new Intent(WebViewDetailActivity.this, MainActivity.class);
                    startActivity(intent);
                    WebViewDetailActivity.this.finish();
                }

            }else {
                super.onBackPressed();
            }
        }
    }

    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {
            showMiddleToast("分享成功");
            webViewDetailPresent.goShare();
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            showMiddleToast("分享失败，" + t.getMessage());
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            showMiddleToast("分享取消");
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

    }
}
