package com.leetu.eman.mygreedao.utils;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.mygreedao.NavigationRecord;

import java.util.List;

/**
 * Created by neo on 2017/2/7.
 */
//数据库操作（增删查改）
public class DaoOperationUtils {
    /**
     * 添加数据
     *
     * @param navigationRecord
     */
    public static boolean insert(NavigationRecord navigationRecord) {
        long b = LeTravelApplication.getDaoInstant().getNavigationRecordDao().insert(navigationRecord);
        if (b == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 删除数据
     *
     * @param id
     */
    public static void delete(long id) {
        LeTravelApplication.getDaoInstant().getNavigationRecordDao().deleteByKey(id);
    }

    /**
     * 更新数据
     *
     * @param navigationRecord
     */
    public static void update(NavigationRecord navigationRecord) {
        LeTravelApplication.getDaoInstant().getNavigationRecordDao().update(navigationRecord);
    }

    /**
     * 查询全部数据
     *
     * @return
     */
    public static List<NavigationRecord> queryAll() {
        return LeTravelApplication.getDaoInstant().getNavigationRecordDao().queryBuilder().list();
    }
}
