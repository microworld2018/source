package com.leetu.eman.views;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ScrollView;

/**
 * Created by lvjunfeng on 2017/4/28.
 * 自定义弹性scrollview
 */

public class MyScrollView extends ScrollView {
    //记录是否可以下拉
    private boolean canPullDown = false;
    //记录是否可以上拉
    private boolean canPullUp = false;

    //移动因子，用于手势滑动距离与view滚动之间的比例差值
    private static final float MOVE_ACTION = 0.3f;

    //滚动回复原位所需时间
    private static final int TIME = 300;

    //记录正常时布局位置
    private Rect rect = new Rect();

    //Scrollview唯一的一个子view
    private View contentview;

    //手指按下时的Y值, 用于在移动时计算移动距离
    //如果按下时不能上拉和下拉， 会在手指移动时更新为当前手指的Y值
    private float startY;

    //记录移动了view
    private boolean isMove = false;

    public MyScrollView(Context context) {
        this(context,null);
    }

    public MyScrollView(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public MyScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    //当布局绘制完成时调用
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        contentview = getChildAt(0);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if(contentview == null) {
            return;
        }
        rect.set(contentview.getLeft(),contentview.getTop(),contentview.getRight(),contentview.getBottom());
    }


    /**
     * 1，如果只是要不定期取一下这个数值，调用scrollView.getScrollY()，如果是横向的scrollView，换成X。
     2，如果要随时使用这个值，例如正在scroll过程中，其他控件要随着卷动的位置变化，
     需要继承一下并且重载onScrollChanged(int, int, int, int)，这样每个scroll动作，
     都会被调用，告知当前scroll的位置以及上一次的位置。如果是横向，那么参数1，3起作用，纵向为2，4.
     * @return
     */

    private boolean isCanPullDown(){
        return getScrollY() ==0 || contentview.getHeight() < getHeight() + getScrollY();
    }

    private boolean isCanPullUp(){
        return contentview.getHeight() <= getHeight() + getScrollY();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {


        if(contentview == null) {
            return super.dispatchTouchEvent(ev);
        }

        int action = ev.getAction();
        switch (action) {

            case  MotionEvent.ACTION_DOWN:
                startY = ev.getY();//记录按下时位置
                //判断是否可以上拉下拉
                canPullDown = isCanPullDown();
                canPullUp = isCanPullUp();

                break;
            case  MotionEvent.ACTION_MOVE:
                //在移动是即没有到上拉位置也没有到下拉位置时
                if(!isCanPullDown() && !isCanPullUp()) {
                    canPullDown = isCanPullDown();
                    canPullUp = isCanPullUp();
                    startY = ev.getY();
                    break;
                }

                float nowY = ev.getY();
                float distanceY = nowY - startY;

                boolean shouldMove = (canPullDown && distanceY > 0)//可以下拉，并且手向下滑动
                        || (canPullUp && distanceY < 0)//可以上拉，并且手上滑
                        || (canPullUp && canPullDown);//当唯一的一个子view大小小于scrollview时

                int offSet = (int) (distanceY * MOVE_ACTION);
                if(shouldMove) {
                    contentview.layout(rect.left,rect.top + offSet,rect.right,rect.bottom + offSet);
                }
                isMove = true;
                break;
            case  MotionEvent.ACTION_UP:
                if(!isMove) {//拦截没有移动时开启动画
                    break;
                }
                //开启动画
                TranslateAnimation animation = new TranslateAnimation(0,0,contentview.getTop(),rect.top);
                animation.setDuration(TIME);
                contentview.startAnimation(animation);

                // 设置回到正常的布局位置
                contentview.layout(rect.left, rect.top,
                        rect.right, rect.bottom);
                //还原
                canPullDown = false;
                canPullUp = false;
                isMove = false;
                break;
        }

        return super.dispatchTouchEvent(ev);
    }
}
