package com.leetu.eman.models.personalcenter.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.leetu.eman.R;


/**
 * Created by 郭宁 on 2015/5/28.
 */
public class MyPrefixesDialog extends Dialog implements View.OnClickListener {

    private final Context context;
    private ClickListenerInterface clickListenerInterface;
    private TextView btMale, btFemale, btSecrecy;

    public MyPrefixesDialog(Context context) {

        super(context, R.style.selectorDialog);
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_prefixes, null);
        setContentView(view);

        btMale = (TextView) view
                .findViewById(R.id.item_popupwindows_male);
        btFemale = (TextView) view
                .findViewById(R.id.item_popupwindows_female);
        btSecrecy = (TextView) view
                .findViewById(R.id.item_popupwindows_secrecy);
        //设置按钮监听
        btMale.setOnClickListener(this);
        btFemale.setOnClickListener(this);
        btSecrecy.setOnClickListener(this);
        Window dialogWindow = getWindow();

        dialogWindow.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        dialogWindow.setWindowAnimations(R.style.mystyle);  //添加动画
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();
        lp.width = (int) (d.widthPixels * 1);
        dialogWindow.setAttributes(lp);
    }

    public void setClicklistener(ClickListenerInterface clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.item_popupwindows_male:
                clickListenerInterface.male();
                break;
            case R.id.item_popupwindows_female:
                clickListenerInterface.female();
                break;
            case R.id.item_popupwindows_secrecy:
                clickListenerInterface.secrecy();
                break;
        }
        MyPrefixesDialog.this.cancel();
    }

    public interface ClickListenerInterface {
        void female();

        void secrecy();

        void male();
    }

}
