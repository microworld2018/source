package com.leetu.eman.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

/**
 * Created by Neo on 2016/8/10.
 */
public class TestGildeRoundTransform extends BitmapTransformation {
    private static float radius = 0f;

    public TestGildeRoundTransform(Context context) {
        this(context, 4);
    }

    public TestGildeRoundTransform(Context context, int dp) {
        super(context);
        this.radius = Resources.getSystem().getDisplayMetrics().density * dp;
    }

    @Override
    protected Bitmap transform(BitmapPool pool, Bitmap toTransform, int outWidth, int outHeight) {
        return roundCrop(pool, toTransform);
    }

    private static Bitmap roundCrop(BitmapPool pool, Bitmap source) {
//        if (source == null) return null;
//
//        Bitmap result = pool.get(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
//        if (result == null) {
//            result = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
//        }
//
//        Canvas canvas = new Canvas(result);
//        Paint paint = new Paint();
//        paint.setShader(new BitmapShader(source, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP));
//        paint.setAntiAlias(true);
//        RectF rectF = new RectF(0f, 0f, source.getWidth(), source.getHeight());
//        canvas.drawRoundRect(rectF, radius, 0f, paint);
        //根据源文件新建一个darwable对象
//        Drawable imageDrawable = new BitmapDrawable(source);
//
//        // 新建一个新的输出图片
//        Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
//        Canvas canvas = new Canvas(output);
//
//        // 新建一个矩形
//        RectF outerRect = new RectF(0, 0, source.getWidth(), source.getWidth());
//
//        // 产生一个红色的圆角矩形
//        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        paint.setColor(Color.RED);
//        canvas.drawRoundRect(outerRect, 10f, 10f, paint);
//
//        // 将源图片绘制到这个圆角矩形上
//        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
//        imageDrawable.setBounds(0, 0, source.getWidth(), source.getWidth());
//        canvas.saveLayer(outerRect, paint, Canvas.ALL_SAVE_FLAG);
//        imageDrawable.draw(canvas);
//        canvas.restore();
//
//        return output;
//        public static Bitmap getRoundedCornerBitmap (Bitmap bitmap,float roundPx){
//            Bitmap output = Bitmap.createBitmap(source.getWidth(), source.getHeight(), Bitmap.Config.ARGB_8888);
//            Canvas canvas = new Canvas(output);
//            final int color = 0xff424242;
//            final Paint paint = new Paint();
//            final Rect rect = new Rect(0, 0, source.getWidth(), source.getHeight());
//            final RectF rectF = new RectF(rect);
//            paint.setAntiAlias(true);
//            canvas.drawARGB(0, 0, 0, 0);
//            paint.setColor(color);
//            canvas.drawRoundRect(rectF, radius, radius, paint);
//            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
//            canvas.drawBitmap(source, rect, rect, paint);
//            return output;
        int width = source.getWidth();
        int height = source.getHeight();

        //创建一个和原始图片一样大小位图
        Bitmap roundConcerImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        //创建带有位图roundConcerImage的画布
        Canvas canvas = new Canvas(roundConcerImage);
        //创建画笔
        Paint paint = new Paint();
        //创建一个和原始图片一样大小的矩形
        Rect rect = new Rect(0, 0, source.getWidth(), source.getHeight());
        RectF rectF = new RectF(rect);
        // 去锯齿
        paint.setAntiAlias(true);


        //画一个和原始图片一样大小的圆角矩形
        canvas.drawRoundRect(rectF, radius, radius, paint);
        //设置相交模式
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        //把图片画到矩形去
        canvas.drawBitmap(source, null, rect, paint);
//        switch(half){
//            case HalfType.LEFT:
        return Bitmap.createBitmap(roundConcerImage, 0, 0, width / 2, height);
//            case HalfType.RIGHT:
//                return Bitmap.createBitmap(roundConcerImage, width/2, 0, width/2, height);
//            case HalfType.TOP:
//                return Bitmap.createBitmap(roundConcerImage, 0, 0, width, height/2);
//            case HalfType.BOTTOM:
//                return Bitmap.createBitmap(roundConcerImage, 0, height/2, width, height/2);
//            case HalfType.NONE:
//                return roundConcerImage;
//            default:
//                return roundConcerImage;
//        }
//        }
//        return result;
    }

    @Override
    public String getId() {
        return getClass().getName() + Math.round(radius);
    }
}
