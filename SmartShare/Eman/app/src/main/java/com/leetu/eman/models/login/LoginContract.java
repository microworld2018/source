package com.leetu.eman.models.login;


import com.leetu.eman.base.BaseContract;

/**
 * Created by jyt on 2016/9/18.
 */

public interface LoginContract {
    interface View extends BaseContract {
        void showLoad();

        void showCode();
    }

    interface UserAction {
        void load(String phoneNumber, String code);

        void sendCode(String phoneNumber);
    }
}
