package com.leetu.eman.models.confirmorder;/**
 * Created by jyt on 2016/9/22.
 */

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.confirmorder.beans.ExpanableBean;
import com.leetu.eman.models.confirmorder.beans.PointAllcarDetailBean;
import com.leetu.eman.models.takecar.beans.TakeCarBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

import java.util.List;


/**
 * created by neo on 2016/9/22 17:30
 */
public class ConfirmOrderPresenter extends BasePresenter implements ConfirmOrderContract.UserAction {
    private Context context;
    private ConfirmOrderContract.View confirmOrderListener;

    public ConfirmOrderPresenter(Context context, ConfirmOrderContract.View confirmOrderListener) {
        this.context = context;
        this.confirmOrderListener = confirmOrderListener;
    }

    @Override
    public void getPointDetail(String dotId, final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.POINT_DETAIL)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("dotId", dotId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), confirmOrderListener)) {
                                PointAllcarDetailBean pointAllcarDetailBean = JsonParser.getParsedData(response.getData(), PointAllcarDetailBean.class);
                                confirmOrderListener.showPointDetail(pointAllcarDetailBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    confirmOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            if (isFrist) {
                                confirmOrderListener.contentFail();
                            } else {
                                confirmOrderListener.timeOutFail();
                            }
                        }
                    });
        } else {
            if (isFrist) {
                confirmOrderListener.contentFail();
            } else {
                confirmOrderListener.showFail(context.getResources().getString(R.string.net_error));
            }

        }
    }

    @Override
    public void startOrder(String dotId, String carId, String backDotId, String insuranceId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.RESERVATION_ORDER)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("dotId", dotId)
                    .addParam("carId", carId)
                    .addParam("backDotId", backDotId)
                    .addParam("insuranceId", insuranceId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), confirmOrderListener)) {
                                LogUtils.e("gn", "开始预约" + response.getData());
                                TakeCarBean takeCarBean = JsonParser.getParsedData(response.getData(), TakeCarBean.class);
                                confirmOrderListener.showOrder(takeCarBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    if (response.getResultCode() == 223) {
                                        confirmOrderListener.showMsg(response.getResultMsg());
                                    } else if (response.getResultCode() == 259) {//还有未支付的订单
                                        confirmOrderListener.showFail(response.getResultMsg());
                                    } else if (response.getResultCode() == 275) {//押金不足
                                        confirmOrderListener.showGoRechargDeposit();
                                    } else {
                                        confirmOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }

                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            confirmOrderListener.timeOutFail();
                        }
                    });
        } else {
            confirmOrderListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }

    @Override
    public void confirmCar(String carId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (LeTravelApplication.getSharePreferenceInstance(context).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(context).getToken().equals("")) {
                HttpEngine.post().url(URLS.CONFIRM_CAR)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .addParam("carId", carId)
                        .tag(ConfirmOrderActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), confirmOrderListener)) {
                                    LogUtils.e("gn", "首页获取个人信息,并展示车型号保险:" + response.getData());
                                    List<ExpanableBean> parsedDataToList = JsonParser.getParsedDataToList(response.getData(), ExpanableBean.class);
                                    if (parsedDataToList != null) {
//                                        LeTravelApplication.getSharePreferenceInstance(context).saveUserStatus(userInfoBean.getState());
                                        confirmOrderListener.showBaoXianDetail(parsedDataToList);
                                    }
                                } else {

                                    if (response.getResultCode() != 206) {
                                        if (response.getResultCode() == 222 || response.getResultCode() == 278 || response.getResultCode() == 279 || response.getResultCode() == 280) {//审核未通过和未认证，认证中，老用户补齐资料
                                            confirmOrderListener.showCheckUserRNA(response.getResultCode(), response.getResultMsg());
                                        } else if (response.getResultCode() == 275) {//押金不足
                                            confirmOrderListener.showGoRechargDeposit();
                                        } else if (response.getResultCode() == 259) {//未支付订单
                                            confirmOrderListener.showFail(response.getResultMsg());
                                        } else if (response.getResultCode() == 223) {//已有预约订单
                                            confirmOrderListener.showMsg(response.getResultMsg());
                                        } else {
                                            confirmOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                LogUtils.e("lv", "onFailure:" + error.getMessage());
                                confirmOrderListener.timeOutFail();
                            }
                        });
            }
        } else {
            confirmOrderListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }

}
