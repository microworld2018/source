package com.leetu.eman.models.peccancyrecord.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.models.peccancyrecord.beans.PeccancyRecordBean;

import java.util.List;


/**
 * Created by jyt on 2016/9/18.
 */

public class PeccancyRecordAdapter extends BaseAdapter {
    private Context context;
    private List<PeccancyRecordBean> list;

    public PeccancyRecordAdapter(Context context, List<PeccancyRecordBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final PeccancyRecordBean peccancyRecordBean = list.get(position);
        ViewHolder viewHolder = null;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_peccancyrecord, null);
            viewHolder.tvTime = (TextView) convertView.findViewById(R.id.tv_peccancy_time);
            viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tv_peccancy_status);
            viewHolder.tvPeccancyReason = (TextView) convertView.findViewById(R.id.tv_peccancy_reason);
            viewHolder.tvAmount = (TextView) convertView.findViewById(R.id.amount_peccancy);
            viewHolder.btnOrReOrder = (TextView) convertView.findViewById(R.id.btn_paypeccancy);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.btnOrReOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tvPayListener != null) {
                    tvPayListener.onPayClickListener(peccancyRecordBean.getAccidentId());
                }
            }
        });

        bindData(peccancyRecordBean, viewHolder);

        return convertView;
    }


    private void bindData(PeccancyRecordBean peccancyRecordBean, ViewHolder viewHolder) {
        // state 0 代表已经取消订单  1 预约成功  2预约超时 3 租车中 4 订单完成  paystatus 0未支付  1已支付
        viewHolder.tvTime.setText(peccancyRecordBean.getHappenTime());
        viewHolder.tvAmount.setText("￥"+ peccancyRecordBean.getMoney());
        viewHolder.tvPeccancyReason.setText(peccancyRecordBean.getDesc());
        if( 0 == peccancyRecordBean.getStatus()) {//未处理
            viewHolder.tvStatus.setText("待处理");
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.main_red));
            viewHolder.btnOrReOrder.setVisibility(View.VISIBLE);
        }else if( 1 == peccancyRecordBean.getStatus()){//已处理
            viewHolder.tvStatus.setText("已处理");
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.text_curorder));
            viewHolder.btnOrReOrder.setVisibility(View.GONE);
        }

    }


    class ViewHolder {
        TextView tvTime, tvStatus, btnOrReOrder, tvPeccancyReason,tvAmount;
    }

    public void setOnTvPayListener(TvPeccancyPayListener tvPayListener) {
        this.tvPayListener = tvPayListener;
    }

    private TvPeccancyPayListener tvPayListener;

    public interface TvPeccancyPayListener {
        void onPayClickListener(String orderId);
    }


}
