package com.leetu.eman.models.settings.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leetu.eman.R;

import java.util.List;

/**
 * Created by Administrator on 2016/11/9.
 */

public class CityAdapter extends BaseAdapter {
    private Context context;
    private List<String> citys;

    public CityAdapter(Context context, List<String> citys) {
        this.context = context;
        this.citys = citys;
    }

    @Override
    public int getCount() {
        return citys.size();
    }

    @Override
    public Object getItem(int position) {
        return citys.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_city, null);
            viewHolder.tvCity = (TextView) convertView.findViewById(R.id.tv_city);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvCity.setText(citys.get(position));
        return convertView;
    }

    class ViewHolder {
        TextView tvCity;
    }
}
