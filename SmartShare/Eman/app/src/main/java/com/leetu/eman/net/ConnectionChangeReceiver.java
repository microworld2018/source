package com.leetu.eman.net;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * Created by neo on 2016/11/24.
 */

public class ConnectionChangeReceiver extends BroadcastReceiver {
    private OnNetListener onNetListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo mobNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo mobNetInfo = connectivityManager.getActiveNetworkInfo();
        NetworkInfo wifiNetInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (mobNetInfo == null) {
            if (onNetListener != null) {
                onNetListener.netNo();
            }
        } else {
            if (!mobNetInfo.isConnected() && !wifiNetInfo.isConnected()) {
                if (onNetListener != null) {
                    onNetListener.netNo();
                }
                //改变背景或者 处理网络的全局变量
            } else {
                //改变背景或者 处理网络的全局变量
                if (onNetListener != null) {
                    onNetListener.netOk();
                }
            }
        }
    }

    public void setOnNetListener(OnNetListener onNetListener) {
        this.onNetListener = onNetListener;
    }

    public interface OnNetListener {
        void netOk();

        void netNo();
    }
}