package com.leetu.eman.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by lvjunfeng on 2017/5/11.
 */

public class CircleBar extends View {
    //外部圆的颜色
    private int outCicleColor = Color.parseColor("#808080");
    //外部圆的宽度
    private int outCicleWidth = 15;
    //外部圆的半径
    private int outCicleRadius;
    //圆弧的颜色
    private int arcColor = Color.parseColor("#87cefa");
    //圆弧的宽度
    private int arcWidth = 7;

    //当前进度
    private int progress = 0;
    //最大进度
    private int maxProgress = 100;

    //进度更新间隔
    private int millions = 5000;
    //文字大小
    private float textSize = 40.0f;
    private int textColor = getResources().getColor(android.R.color.holo_blue_bright);

    private String text = "跳过";

    //当前布局的宽
    private int viewWidth;
    private Paint paint;

    private ProgressListener progressListener;

    public CircleBar(Context context) {
        this(context,null);
    }

    public CircleBar(Context context, AttributeSet attrs) {
        this(context, attrs,0);
    }

    public CircleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        paint = new Paint();
        paint.setAntiAlias(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        viewWidth = getMeasuredWidth();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void start() {
        stop();
        post(progressTask);
    }

    public void stop() {
        removeCallbacks(progressTask);
    }

    Runnable progressTask = new Runnable() {

        @Override
        public void run() {
            removeCallbacks(progressTask);
            progress++;
            if (progress >= 0 && progress <= 100) {
                invalidate();
                postDelayed(progressTask,millions/100);
            }else {
                if(progress > 100) {
                    if(progressListener != null) {
                        progressListener.endProgress();
                    }
                }
                progress = maxProgress;
            }
        }
    };

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        //画圆环
        outCicleRadius = viewWidth / 2 - outCicleWidth / 2;
        paint.setColor(outCicleColor);
        paint.setStrokeWidth(outCicleWidth);
        paint.setStyle(Paint.Style.STROKE);//设置成圆环的样式
        canvas.drawCircle(viewWidth / 2, viewWidth / 2, outCicleRadius, paint);



        //画文字
        paint.setColor(textColor);
        paint.setTextSize(textSize);
        paint.setStrokeWidth(0);
        Rect bounds = new Rect();
        paint.getTextBounds(text, 0, text.length(), bounds);//得到一个文字大小的矩形边框
        //文本左下顶点坐标
        float textX = viewWidth / 2 - bounds.width() / 2;
        float textY = viewWidth / 2 + bounds.height() / 2;
        canvas.drawText(text, textX, textY, paint);

        //画圆弧
        paint.setColor(arcColor);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(arcWidth);
        RectF rectF = new RectF(outCicleWidth / 2, outCicleWidth / 2, viewWidth - outCicleWidth / 2, viewWidth - outCicleWidth / 2);
        canvas.drawArc(rectF, 0, progress * 360 / 100, false, paint);
    }

    public int getOutCicleColor() {
        return outCicleColor;
    }

    public void setOutCicleColor(int outCicleColor) {
        this.outCicleColor = outCicleColor;
    }

    public int getOutCicleWidth() {
        return outCicleWidth;
    }

    public void setOutCicleWidth(int outCicleWidth) {
        this.outCicleWidth = outCicleWidth;
    }

    public int getOutCicleRadius() {
        return outCicleRadius;
    }

    public void setOutCicleRadius(int outCicleRadius) {
        this.outCicleRadius = outCicleRadius;
    }

    public int getArcColor() {
        return arcColor;
    }

    public void setArcColor(int arcColor) {
        this.arcColor = arcColor;
    }

    public int getArcWidth() {
        return arcWidth;
    }

    public void setArcWidth(int arcWidth) {
        this.arcWidth = arcWidth;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getMaxProgress() {
        return maxProgress;
    }

    public void setMaxProgress(int maxProgress) {
        this.maxProgress = maxProgress;
    }

    public int getMillions() {
        return millions;
    }

    public void setMillions(int millions) {
        this.millions = millions;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getViewWidth() {
        return viewWidth;
    }

    public void setViewWidth(int viewWidth) {
        this.viewWidth = viewWidth;
    }

    public ProgressListener getProgressListener() {
        return progressListener;
    }

    public void setProgressListener(ProgressListener progressListener) {
        this.progressListener = progressListener;
    }

    public interface ProgressListener {
        void endProgress();
    }
}
