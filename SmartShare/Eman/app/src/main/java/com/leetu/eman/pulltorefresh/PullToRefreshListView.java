package com.leetu.eman.pulltorefresh;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.leetu.eman.R;


/**
 * Created by Administrator on 2016/11/21.
 */

public class PullToRefreshListView extends LinearLayout {
    private SuperSwipeRefreshLayout superSwipeRefreshLayout;
    private ListView mListView;
    private AnimationDrawable anim;
    private ImageView ivHeadAnim, ivFooterAnim;
    private OnLeRefreshListener leRefreshListener;
    private Animation mRotateAnimation;
    protected View footerView, headView;
    protected TextView footerTv;

    static final int ROTATION_ANIMATION_DURATION = 1200;


    public PullToRefreshListView(Context context) {
        super(context);
    }

    public PullToRefreshListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public PullToRefreshListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    public PullToRefreshListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);
    }

    private void init(AttributeSet attributeSet) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setOrientation(VERTICAL);
        inflater.inflate(R.layout.pulltorefresh_view, this, true);
        superSwipeRefreshLayout = (SuperSwipeRefreshLayout) findViewById(R.id.pulltorefresh);
        mListView = (ListView) findViewById(R.id.listview);

        headView = LayoutInflater.from(getContext()).inflate(R.layout.pulltorefresh_custom_header, null);
        ivHeadAnim = (ImageView) headView.findViewById(R.id.iv_header_anim);

        footerView = LayoutInflater.from(getContext()).inflate(R.layout.pulltorefresh_custom_footer, null);
        footerTv = (TextView) footerView.findViewById(R.id.loadmore_default_footer_tv);
        ivFooterAnim = (ImageView) footerView.findViewById(R.id.iv_footer_anim);

        buildHeadAnimation();
        buildFooterAnimation();

        superSwipeRefreshLayout.setHeaderView(headView);
        superSwipeRefreshLayout.setFooterView(footerView);
        superSwipeRefreshLayout.setOnPushLoadMoreListener(new SuperSwipeRefreshLayout.OnPushLoadMoreListener() {
            @Override
            public void onLoadMore() {
                ivFooterAnim.clearAnimation();
                ivFooterAnim.startAnimation(mRotateAnimation);
                if (leRefreshListener != null) {
                    leRefreshListener.loadMore();
                }
            }

            @Override
            public void onPushDistance(int distance) {
            }

            @Override
            public void onPushEnable(boolean enable) {

            }
        });
        superSwipeRefreshLayout.setOnPullRefreshListener(new SuperSwipeRefreshLayout.OnPullRefreshListener() {
            @Override
            public void onRefresh() {
                anim.start();
                if (leRefreshListener != null) {
                    leRefreshListener.upData();
                }
            }

            @Override
            public void onPullDistance(int distance) {
            }

            @Override
            public void onPullEnable(boolean enable) {

            }
        });
    }

    private void buildHeadAnimation() {
        anim = new AnimationDrawable();
        for (int i = 1; i <= 6; i++) {
            int id = getResources().getIdentifier("car" + i, "mipmap", getContext().getPackageName());
            Drawable drawable = getResources().getDrawable(id);
            anim.addFrame(drawable, 200);
        }
        anim.setOneShot(false);
        ivHeadAnim.setImageDrawable(anim);
    }

    //创建动画
    private void buildFooterAnimation() {
        mRotateAnimation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        LinearInterpolator lin = new LinearInterpolator();
        mRotateAnimation.setInterpolator(lin);
        mRotateAnimation.setDuration(ROTATION_ANIMATION_DURATION);
        mRotateAnimation.setRepeatCount(Animation.INFINITE);
        mRotateAnimation.setRepeatMode(Animation.RESTART);

    }

    void setMyAdapter(BaseAdapter adapter) {
        mListView.setAdapter(adapter);
    }

    public void setOnLeRefreshListener(OnLeRefreshListener leRefreshListener) {
        this.leRefreshListener = leRefreshListener;
    }

    public interface OnLeRefreshListener {
        void upData();

        void loadMore();
    }

    //刷新完成
    public void onRefreshComplete() {
        superSwipeRefreshLayout.setRefreshing(false);
        anim.stop();
    }

    //加载完成
    public void onLoadMoreComplete() {
        superSwipeRefreshLayout.setLoadMore(false);
    }

    //得到ListView
    public ListView getContentView() {
        return mListView;
    }

    public void setRefreshing(boolean isFreshing) {
        superSwipeRefreshLayout.setRefreshing(true);
    }
}
