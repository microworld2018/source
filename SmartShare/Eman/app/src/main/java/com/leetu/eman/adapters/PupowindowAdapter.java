package com.leetu.eman.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.beans.CityBean;

import java.util.List;


/**
 * Created by lvjunfeng on 2017/2/7.
 */

public class PupowindowAdapter extends BaseAdapter {

    private final List<CityBean> lists;
    private final Context mContext;

    public PupowindowAdapter(Context context, List<CityBean> list) {
        this.mContext = context;
        this.lists = list;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        CityBean cityBean = lists.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_popuwindow, null);
            viewHolder.cityName = (TextView) convertView.findViewById(R.id.item_city);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.cityName.setText(cityBean.getName());
        if (position == lists.size() - 1) {
            viewHolder.cityName.setEnabled(false);
        } else {
            viewHolder.cityName.setEnabled(true);
        }
        return convertView;
    }

    class ViewHolder {
        TextView cityName;
    }

}

