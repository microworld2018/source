package com.leetu.eman.models.currentorder.beans;/**
 * Created by gn on 2016/10/9.
 */

import java.io.Serializable;

/**
 * created by neo on 2016/10/9 13:43
 */
public class CurrentDotBean implements Serializable {
    String address;
    String dotId;
    String name;
    double lat;
    double lng;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDotId() {
        return dotId;
    }

    public void setDotId(String dotId) {
        this.dotId = dotId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
