package com.leetu.eman.models.settings;


import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.settings.beans.InvoiceShowBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.views.TitleBar;


/**
 * 索要发票
 */
public class InvoiceActivity extends BaseActivity implements View.OnClickListener, InvoiceContract.View {


    private TitleBar titleBar;
    private InvoicePresenter invoicePresenter;
    private TextView amountMoney, tvTypeVal;
    private Button btnInvice;
    private EditText invoiceAmount, invoiceHeader, invoiceHecipients, invoiceTelphone, invoiceAddress, invoicePostcode;
    private boolean isFrist = true;
    String typeV = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice);
        initView();
        loadData();
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_invoice);
        amountMoney = (TextView) findViewById(R.id.amount_money);
        btnInvice = (Button) findViewById(R.id.btn_invice);
        invoiceAmount = (EditText) findViewById(R.id.invoice_amount);
        invoiceHeader = (EditText) findViewById(R.id.invoice_header);
        tvTypeVal = (TextView) findViewById(R.id.tv_type_val);
        invoiceHecipients = (EditText) findViewById(R.id.invoice_recipients);
        invoiceTelphone = (EditText) findViewById(R.id.invoice_telphone);
        invoiceAddress = (EditText) findViewById(R.id.invoice_address);
        invoicePostcode = (EditText) findViewById(R.id.invoice_postcode);

        invoicePresenter = new InvoicePresenter(this, this);

        titleBar.setTitle("发票");
        titleBar.setLeftClickListener(this);
        btnInvice.setOnClickListener(this);
    }


    protected void loadData() {
        showLoading();
        invoicePresenter.getInvoice(isFrist);
    }

    @Override
    public void showInvoice(InvoiceShowBean invoiceshowBean) {
        showProgressBar(false);
        showContent();
        isFrist = false;
        if (invoiceshowBean != null) {
            amountMoney.setText(invoiceshowBean.getAmount());
            if (Integer.parseInt(invoiceshowBean.getAmount()) < 100) {
                btnInvice.setEnabled(false);
            } else {
                btnInvice.setEnabled(true);
            }
        }
    }

    @Override
    public void showUpOk() {
        showOkDialog("开票成功", "我们会在10个工作日内安排邮寄", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                InvoiceActivity.this.finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                InvoiceActivity.this.finish();
                break;
            case R.id.btn_invice:

                if (checkAll()) {
                } else {
//                    invoicePresenter.invoiceBill(invoiceAmount.getText().toString().trim(), invoiceHeader.getText().toString().trim(), tvTypeVal.getText().toString(), invoiceHecipients.getText().toString().trim(), invoiceTelphone.getText().toString().trim(), invoiceAddress.getText().toString().trim(), invoicePostcode.getText().toString().trim());
                    showButtomToast("功能开发中  ");
                }
                break;
        }
    }

    private boolean checkAll() {
        int invoiceNum;
        if (invoiceAmount.getText().toString().trim().length() > 0) {
            invoiceNum = Integer.parseInt(invoiceAmount.getText().toString().trim());
        } else {
            Toast.makeText(InvoiceActivity.this, "请输入开票金额！", Toast.LENGTH_SHORT).show();
            return true;
        }

        int headerLength = invoiceHeader.getText().toString().trim().length();
        int reciverNameLength = invoiceHecipients.getText().toString().trim().length();
        int telphoneLength = invoiceTelphone.getText().toString().trim().length();
        int addresslength = invoiceAddress.getText().toString().trim().length();
        int postCodeLength = invoicePostcode.getText().toString().trim().length();

        if (invoiceNum < 100) {
            Toast.makeText(InvoiceActivity.this, "开票最低额度为100元，请重新输入！", Toast.LENGTH_SHORT).show();
            return true;
        } else if (headerLength < 1) {
            Toast.makeText(InvoiceActivity.this, "发票抬头不能为空，请重新输入！", Toast.LENGTH_SHORT).show();
            return true;
        } else if (reciverNameLength < 1) {
            Toast.makeText(InvoiceActivity.this, "收件人姓名不能为空，请重新输入！", Toast.LENGTH_SHORT).show();
            return true;
        } else if (telphoneLength < 7) {
            Toast.makeText(InvoiceActivity.this, "请输入有效的电话号位数！", Toast.LENGTH_SHORT).show();
            return true;
        } else if (addresslength < 1) {
            Toast.makeText(InvoiceActivity.this, "请输入收件地址！", Toast.LENGTH_SHORT).show();
            return true;
        } else if (postCodeLength != 6) {
            Toast.makeText(InvoiceActivity.this, "请输入有效的邮编！", Toast.LENGTH_SHORT).show();
            return true;
        }

        return false;
    }


    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        invoicePresenter.getInvoice(true);
    }


    @Override
    protected void onRetryLoadData() {
        showProgressBar(true);
        invoicePresenter.getInvoice(isFrist);
        super.onRetryLoadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(InvoiceActivity.class);
    }

}
