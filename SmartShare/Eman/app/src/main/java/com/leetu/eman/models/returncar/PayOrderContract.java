package com.leetu.eman.models.returncar;/**
 * Created by gn on 2016/10/11.
 */


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.returncar.beans.OrederCreateDetailBean;
import com.leetu.eman.models.returncar.beans.PayBean;
import com.leetu.eman.models.returncar.beans.WXPayBean;

/**
 * created by neo on 2016/10/11 15:13
 */
public interface PayOrderContract {
    interface View extends BaseContract {
        void showLoadOrder(OrederCreateDetailBean orederCreateDetailBean);

        void showPay(PayBean payBean);

        void showWxPay(WXPayBean payBean);

        void showPaySuccess();

        void showUseCoupn();

        void showPayOverOrder();

        void showPayFail();
    }

    interface UserAction {
        void loadOrder(String payFlags,String selectCoupon);

        void pay(String orderId, String payType);

        void paySuccess(String resultStatus, String result);

        void useCoupon(String subCouponId, String orderId);

        void payOverOrder(String orderNo);

        void errorPay(String code, String orderId);

        void confirmWxPay(String orderId);
    }
}
