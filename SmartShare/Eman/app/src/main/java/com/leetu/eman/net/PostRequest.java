package com.leetu.eman.net;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;
import com.zhy.http.okhttp.request.RequestCall;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.HashMap;

import okhttp3.Call;

/**
 * Created by kevin on 16/3/10.
 */
final public class PostRequest {


    private HashMap<String, String> params = new HashMap<String, String>();
    private HashMap<String, String> actions = new HashMap<String, String>();
    private String url;
    private long readTimeout;
    private long writeTimeout;
    private long connectTimeout;
    private Object tag;

    public PostRequest() {
//        params.put("DeviceID", "2");
//        params.put("Version", BuildConfig.VERSION_NAME);
//        if (FactoryWarehouseApplication.getInstance() != null
//                && FactoryWarehouseApplication.getInstance().getmSharePreferenceUtils() != null) {
////            params.put("DeviceFlag", FactoryWarehouseApplication.getInstance().getmSharePreferenceUtils().getUUID());
//        }
    }

    public PostRequest readTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    public PostRequest writeTimeout(long writeTimeout) {
        this.writeTimeout = writeTimeout;
        return this;
    }

    public PostRequest connectTimeout(long connectTimeout) {
        this.connectTimeout = connectTimeout;
        return this;
    }

    public PostRequest tag(Object tag) {
        this.tag = tag;
        return this;
    }

    public PostRequest url(String url) {
        this.url = url;
        return this;
    }

    public PostRequest addParam(String key, String value) {
        params.put(key, value);
        return this;
    }

    public PostRequest addAction(String key, String value) {
        actions.put(key, value);
        return this;
    }

    public void execute(final StringCallback callback) {
        //  LogUtils.i("gn", "参数==" + NetworkHelper.formatRequestParams2(params));
        LogUtils.i("lv", "参数==" + params);
        RequestCall call = OkHttpUtils
                .post()
                .tag(tag)
                .url(NetworkHelper.formatUrl(url, actions))
                .params(NetworkHelper.formatRequestParams2(JSON.toJSONString(params)))
                .build();

        if (readTimeout > 0) {
            call.readTimeOut(readTimeout);
        }

        if (writeTimeout > 0) {
            call.writeTimeOut(writeTimeout);
        }
        if (connectTimeout > 0) {
            call.connTimeOut(connectTimeout);
        }

        call.execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e) {
                if (callback != null) {
                    if (e instanceof SocketTimeoutException) {
                        callback.onError(call, e);
                    } else if (e instanceof ConnectException) {
                    } else {
                    }
                }
            }

            @Override
            public void onResponse(String response) {

                if (callback != null) {
                    callback.onResponse(NetworkHelper.formatResponse(response));
                }
            }
        });
    }


    public void execute(final HttpEngine.ResponseCallback callback) {
        LogUtils.e("lv", "参数==" + JSON.toJSONString(params));
        LogUtils.e("lv", "加密参数==" + NetworkHelper.formatRequestParams2(JSON.toJSONString(params)));
        RequestCall call = OkHttpUtils
                .post()
                .tag(tag)
                .url(NetworkHelper.formatUrl(url, actions))
                .params(NetworkHelper.formatRequestParams2(JSON.toJSONString(params)))
                .build();

        if (readTimeout > 0) {
            call.readTimeOut(readTimeout);
        }

        if (writeTimeout > 0) {
            call.writeTimeOut(writeTimeout);
        }

        call.execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e) {
                if (callback != null) {
                    if (e instanceof SocketTimeoutException) {
                        callback.onFailure(e);
                        LogUtils.e("lv","onError--onFailure");

                    } else if (e instanceof ConnectException) {
                    } else {
//                        Toast.makeText(LeTravelApplication.getInstance(),"StringCallbackOnError异常:"+ e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onResponse(String response) {
                LogUtils.e("lv", "返回原始数据:" + response);
                if (callback != null && !TextUtils.isEmpty(response)) {
                    try {
                        ResponseStatus status = JsonParser.getParsedData(response, ResponseStatus.class);
                        callback.onResponse(status);
                    } catch (Exception e) {
                        callback.onFailure(e);
                        LogUtils.e("lv","onResponse--onFailure");
                    }

                }
            }
        });
    }

    //针对不加密，不加最外层数据包裹
    public void execute3(final HttpEngine.ResponseCallback callback) {
        RequestCall call = OkHttpUtils
                .post()
                .tag(tag)
                .url(NetworkHelper.formatUrl(url, actions))
                .addParams("resultStatus", params.get("resultStatus"))
                .addParams("result", params.get("result"))
                .build();

        if (readTimeout > 0) {
            call.readTimeOut(readTimeout);
        }

        if (writeTimeout > 0) {
            call.writeTimeOut(writeTimeout);
        }

        call.execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e) {
                if (callback != null) {
                    if (e instanceof SocketTimeoutException) {
                        callback.onFailure(e);
                    } else if (e instanceof ConnectException) {
                    } else {
                    }
                }
            }

            @Override
            public void onResponse(String response) {
                LogUtils.e("lv", "返回原始数据" + response);

                if (callback != null && response != null) {
                    try {
                        callback.onResponse(JsonParser.getParsedData(response, ResponseStatus.class));
                    } catch (Exception e) {
                        callback.onFailure(e);
                    }

                }
            }
        });
    }

    //针对不加密，不加最外层数据包裹
    public void execute4(final HttpEngine.ResponseCallback callback) {
        RequestCall call = OkHttpUtils
                .post()
                .tag(tag)
                .url(NetworkHelper.formatUrl(url, actions))
                .addParams("code", params.get("code"))
                .addParams("orderId", params.get("orderId"))
                .build();

        if (readTimeout > 0) {
            call.readTimeOut(readTimeout);
        }

        if (writeTimeout > 0) {
            call.writeTimeOut(writeTimeout);
        }

        call.execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e) {
                if (callback != null) {
                    if (e instanceof SocketTimeoutException) {
                        callback.onFailure(e);
                    } else if (e instanceof ConnectException) {
                    } else {
                    }
                }
            }

            @Override
            public void onResponse(String response) {
                LogUtils.e("lv", "返回原始数据" + response);

                if (callback != null && response != null) {
                    try {
                        callback.onResponse(JsonParser.getParsedData(response, ResponseStatus.class));
                    } catch (Exception e) {
                        callback.onFailure(e);
                    }

                }
            }
        });
    }
}
