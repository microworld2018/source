package com.leetu.eman.models.returncar;/**
 * Created by jyt on 2016/9/21.
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.returncar.views.MyShareDialog;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.TitleBar;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;

import java.util.Map;


/**
 * created by neo on 2016/9/21 10:43
 * 还车成功
 */
public class ReturnCarSuccessActivity extends BaseActivity implements View.OnClickListener, MyShareDialog.ClickListenerInterface, ReturnCarSuccessContract.View {
    private TitleBar titleBar;
    private MyShareDialog myShareDialog;
    private Button btGoHome, btShare;
    private static boolean isExit = false;

    Handler exitHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            isExit = false;
        }
    };
    private String shareContent;
    private String url;
    private String title;
    private ReturnCarSuccessPresenter returnCarSuccessPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_car_success);
        initView();
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_rcsuccess);
        btGoHome = (Button) findViewById(R.id.tv_paysuccess_gohome);
        btShare = (Button) findViewById(R.id.tv_paysuccess_share);

        titleBar.setTitle("完成订单");
        titleBar.hideLeftLayout();
        btGoHome.setOnClickListener(this);
        btShare.setOnClickListener(this);
        returnCarSuccessPresenter = new ReturnCarSuccessPresenter(this, this);

        title = "太TM方便啦";
        shareContent = "电动侠出行：随时随地租车用车，0押金，不限行，还有专属停车位！低至6元/小时，免去买车养车诸多烦恼！";
        url = URLS.SHARE;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                ReturnCarSuccessActivity.this.finish();
                break;
            case R.id.tv_paysuccess_share:
                if (myShareDialog == null) {
                    myShareDialog = new MyShareDialog(this);
                    myShareDialog.setClicklistener(this);
                    myShareDialog.setCanceledOnTouchOutside(true);
                } else {
                }
                myShareDialog.show();
                break;
            case R.id.tv_paysuccess_gohome:
                ReturnCarSuccessActivity.this.finish();
                LeTravelApplication.getInstance().finishAllExcept(MainActivity.class);
                break;
        }
    }

    @Override
    public void wechat() {
//        returnCarSuccessPresenter.goShare(true);//点击分享给个人无论成功失败都给优惠券,true代表是个人
        UMImage image = new UMImage(ReturnCarSuccessActivity.this, R.mipmap.ic_share_friend);
        ShareAction shareAction = new ShareAction(ReturnCarSuccessActivity.this).setPlatform(SHARE_MEDIA.WEIXIN);
        commonShare(title, image, shareAction, shareContent, url);
    }


    @Override
    public void wechatCircle() {
        UMImage image = new UMImage(ReturnCarSuccessActivity.this, R.mipmap.ic_share_cicle);
        ShareAction shareAction = new ShareAction(ReturnCarSuccessActivity.this).setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE);
        commonShare("电动侠出行：让用车更方便，出行更简单！", image, shareAction, shareContent, url);
    }

    @Override
    public void weibo() {
        //微博先进行授权,当设备上有客户端的时候不需要，可直接进行分享
        UMShareAPI mShareAPI = UMShareAPI.get(ReturnCarSuccessActivity.this);
        mShareAPI.doOauthVerify(ReturnCarSuccessActivity.this, SHARE_MEDIA.SINA, umAuthListener);
    }

    @Override
    public void QQ() {
        UMImage image = new UMImage(ReturnCarSuccessActivity.this, R.mipmap.ic_share_cicle);
        ShareAction shareAction = new ShareAction(ReturnCarSuccessActivity.this).setPlatform(SHARE_MEDIA.QQ);
        commonShare(title, image, shareAction, shareContent, url);
    }

    @Override
    public void qqZone() {
        UMImage image = new UMImage(ReturnCarSuccessActivity.this, R.mipmap.ic_share_cicle);
        ShareAction shareAction = new ShareAction(ReturnCarSuccessActivity.this).setPlatform(SHARE_MEDIA.QZONE);
        commonShare(title, image, shareAction, shareContent, url);
    }

    /**
     * 点击就给优惠券
     */
    @Override
    public void startShare() {
        returnCarSuccessPresenter.goShare(true);
    }

    private void commonShare(String title, UMImage image1, ShareAction shareAction, String content, String url) {
        shareAction.withTitle(title)
                .withText(content)
                .withMedia(image1)
                .withTargetUrl(url)
                .setCallback(umShareListener)
                .share();
    }

    private UMAuthListener umAuthListener = new UMAuthListener() {
        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
            UMImage image = new UMImage(ReturnCarSuccessActivity.this, R.mipmap.ic_share_friend);
            ShareAction shareAction = new ShareAction(ReturnCarSuccessActivity.this).setPlatform(platform);
            commonShare(title, image, shareAction, shareContent, url);
        }

        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            showMiddleToast(t.getMessage());
        }

        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            showMiddleToast("取消登录");
        }
    };

    private UMShareListener umShareListener = new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA platform) {

            showMiddleToast("分享成功");

//            if (platform != SHARE_MEDIA.WEIXIN) {//分开的处理
//                showProgressBar(true);
//                returnCarSuccessPresenter.goShare(false);//分享成功告知后台
//            } else {
//                showMiddleToast("分享成功");
//            }
        }

        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            showMiddleToast("分享失败，" + t.getMessage());
        }

        @Override
        public void onCancel(SHARE_MEDIA platform) {
            showMiddleToast("分享取消");
        }
    };

    @Override
    public void shareSuccess(boolean isWxFriend) {
//        if (!isWxFriend) {//不是微信分享就提示
//            showProgressBar(false);
//            showMiddleToast("分享成功");
//        }
        LogUtils.e("lv", "获得优惠券");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exit();
        }
        return true;
    }

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            // 利用handler延迟发送更改状态信息
            exitHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            finish();
            LeTravelApplication.getInstance().AppExit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);

    }


}
