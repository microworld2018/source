package com.leetu.eman.models.login.bean;/**
 * Created by jyt on 2016/9/22.
 */

/**
 * created by neo on 2016/9/22 15:44
 */
public class UserBean {
    String token;
    String phoneNoStr;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPhoneNoStr() {
        return phoneNoStr;
    }

    public void setPhoneNoStr(String phoneNoStr) {
        this.phoneNoStr = phoneNoStr;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "token='" + token + '\'' +
                ", phoneNoStr='" + phoneNoStr + '\'' +
                '}';
    }
}
