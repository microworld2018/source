package com.leetu.eman.models.balance;


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.balance.beans.BalanceBean;

/**
 * Created by jyt on 2016/9/18.
 */

public interface BalanceContract {
    interface View extends BaseContract {
        void showLoad(BalanceBean balanceBean);
    }

    interface UserAction {
        void load(boolean isFrist);
    }
}
