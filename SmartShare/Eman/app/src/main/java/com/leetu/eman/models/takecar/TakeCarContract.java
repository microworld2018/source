package com.leetu.eman.models.takecar;/**
 * Created by jyt on 2016/9/26.
 */


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.takecar.beans.TakeCarBean;

/**
 * created by neo on 2016/9/26 10:23
 */
public interface TakeCarContract {
    interface View extends BaseContract {
        void showLoadData(TakeCarBean checkOrderBean);

        void showopenOrCloseDoorOk();

        void showStartUseCar();

        void showCancelOrder();
    }

    interface UserAction {
        void loadData();

        void openOrCloseDoor(String orderId, String state, String carId, String lat, String lng);

        void startUseCar(String orderId, String carId);

        void cancelOrder(String orderId);
    }
}
