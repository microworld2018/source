package com.leetu.eman.net;

import android.os.Environment;

import com.zhy.http.okhttp.BuildConfig;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.GetBuilder;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zhy.http.okhttp.request.RequestCall;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.Call;

/**
 * Created by Neo on 2016/7/28.
 */
public class DownLoadFileRequest {

    private HashMap<String, String> params = new HashMap<String, String>();
    private HashMap<String, String> actions = new HashMap<String, String>();
    private ArrayList<FileParam> files = new ArrayList<>();
    private String url;
    private long readTimeout;
    private long writeTimeout;
    private Object tag;

    public DownLoadFileRequest() {
        params.put("DeviceID", "2");
        params.put("Version", BuildConfig.VERSION_NAME);
//        if (FactoryWarehouseApplication.getInstance() != null
//                && FactoryWarehouseApplication.getInstance().getmSharePreferenceUtils() != null) {
////            params.put("DeviceFlag", MaskManagerApplication.getInstance().getmSharePreferenceUtils().getUUID());
//        }
    }

    public DownLoadFileRequest readTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    public DownLoadFileRequest writeTimeout(long writeTimeout) {
        this.writeTimeout = writeTimeout;
        return this;
    }

    public void setFiles(ArrayList<FileParam> files) {
        this.files = files;
    }

    public DownLoadFileRequest addFile(FileParam param) {
        files.add(param);
        return this;
    }

    public DownLoadFileRequest addFiles(List<FileParam> fileParams) {
        if (fileParams == null) {
        } else {
            for (int i = 0; i < fileParams.size(); i++) {
                files.add(fileParams.get(i));
            }
        }
        return this;
    }

    public DownLoadFileRequest addFile(String name, String path) {
        files.add(new FileParam(name, path));
        return this;
    }

    public DownLoadFileRequest tag(Object tag) {
        this.tag = tag;
        return this;
    }

    public DownLoadFileRequest url(String url) {
        this.url = url;
        return this;
    }

    public DownLoadFileRequest addParam(String key, String value) {
        params.put(key, value);
        return this;
    }

    public DownLoadFileRequest addAction(String key, String value) {
        actions.put(key, value);
        return this;
    }

    public void execute(final HttpEngine.FileCallBack callback) {
//
        GetBuilder getBuilder = OkHttpUtils
                .get();


        RequestCall call = getBuilder.tag(tag)
                .url(NetworkHelper.formatUrl(url, actions))
                .build();


        if (readTimeout > 0) {
            call.readTimeOut(readTimeout);
        }

        if (writeTimeout > 0) {
            call.writeTimeOut(writeTimeout);
        }

        call.execute(new FileCallBack(Environment.getExternalStorageDirectory().getAbsolutePath(), "xmengya.apk") {
            @Override
            public void onError(Call call, Exception e) {
                if (callback != null) {
                    callback.onError(call, e);
                }
            }

            @Override
            public void onResponse(File response) {
                if (callback != null) {
                    callback.onResponse(response);
                }
            }

            @Override
            public void inProgress(float progress, long total) {
                callback.inProgress(progress, total);
            }
        });
    }

}
