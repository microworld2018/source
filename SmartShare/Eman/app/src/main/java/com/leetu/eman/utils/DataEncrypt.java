package com.leetu.eman.utils;

import java.io.IOException;

/**
 * 作者：guolei on 2016/5/23 15:19
 * 邮箱：527411767@qq.com
 */

public class DataEncrypt {
    // 加密
    public static String getEncryptStr(String data) {
        String result = null;
        try {
            result = CryptoTools.encode(data);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    // 解密
    public static String getDecrypt(String data) {
        String result = null;
        try {
            result = CryptoTools.decode(data);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }
}
