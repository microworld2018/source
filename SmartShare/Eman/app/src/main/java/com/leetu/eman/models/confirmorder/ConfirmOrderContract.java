package com.leetu.eman.models.confirmorder;/**
 * Created by jyt on 2016/9/22.
 */


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.confirmorder.beans.ExpanableBean;
import com.leetu.eman.models.confirmorder.beans.PointAllcarDetailBean;
import com.leetu.eman.models.takecar.beans.TakeCarBean;

import java.util.List;

/**
 * created by neo on 2016/9/22 17:30
 */
public interface ConfirmOrderContract {
    interface View extends BaseContract {
        void showPointDetail(PointAllcarDetailBean pointAllcarDetailBean);

        void showOrder(TakeCarBean takeCarBean);

        void showMsg(String msg);

        void showGoRechargDeposit();

        void showBaoXianDetail(List<ExpanableBean> parsedDataToList);

        void showCheckUserRNA(int code,String mes);
    }

    interface UserAction {
        void getPointDetail(String dotId, boolean isFrist);

        void startOrder(String dotId, String carId, String backDotId,String insuranceId);

        void confirmCar(String carId);

    }
}
