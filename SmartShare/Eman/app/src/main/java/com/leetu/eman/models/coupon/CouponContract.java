package com.leetu.eman.models.coupon;

import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.coupon.beans.CouponBean;

import java.util.List;

/**
 * Created by neo on 2016/12/17.
 */

public interface CouponContract {
    interface View extends BaseContract {
        void showLoadData(boolean isUp, List<CouponBean> coupon, String mode);

        void showEmptyView();


    }

    interface UserAction {
        void loadData(boolean isUp, boolean isFrist, String mode, String orderPrice);


    }
}
