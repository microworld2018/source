package com.leetu.eman.models.confirmorder.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.models.confirmorder.beans.ExpanableBean;

import java.util.List;


/**
 * Created by lvjunfeng on 2017/5/4.
 */

public class ExpanableAdapter extends BaseExpandableListAdapter {

    private final Context context;
    private final List<ExpanableBean> data;

    public ExpanableAdapter(List<ExpanableBean> datas, Context context) {
        this.data = datas;
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return data.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return data.get(groupPosition).getTipsList().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return data.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return data.get(groupPosition).getTipsList().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view = null;
        GroupHolder groupHolder = null;
        if (convertView != null) {
            view = convertView;
            groupHolder = (GroupHolder) view.getTag();
        } else {
            view = View.inflate(context, R.layout.group_item_view, null);
            groupHolder = new GroupHolder();
            groupHolder.title = (TextView) view.findViewById(R.id.tv_buchong_chexian);
            groupHolder.subTitle = (TextView) view.findViewById(R.id.buchong_price);
            groupHolder.isUsedButton = (CheckBox) view.findViewById(R.id.cb_buchong);
            groupHolder.webicon = (ImageView) view.findViewById(R.id.iv_exclamation_mark_buchong);
            view.setTag(groupHolder);
        }
        groupHolder.title.setText(data.get(groupPosition).getTitle());
        groupHolder.subTitle.setText(data.get(groupPosition).getSubtitle());

        if (data.get(groupPosition).getIsUsed().equals("1")) {//1必选 0 可选
            groupHolder.isUsedButton.setVisibility(View.GONE);
        } else {
            groupHolder.isUsedButton.setVisibility(View.VISIBLE);
        }
        groupHolder.webicon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClikListener != null) {
                    onClikListener.detailIconClick(data.get(groupPosition).getInsuranceDesc());
                }
            }
        });

        groupHolder.isUsedButton.setOnClickListener(new CustemOnClickListener(groupHolder.isUsedButton, data.get(groupPosition).getInsuranceId()));
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view = null;
        ClideHolder child = null;
        if (convertView != null) {
            view = convertView;
            child = (ClideHolder) view.getTag();
        } else {
            view = View.inflate(context, R.layout.layout_baoxian_detail, null);
            child = new ClideHolder();
            child.childTitle = (TextView) view.findViewById(R.id.tv_baoxian_detail_every);
            view.setTag(child);
        }
        child.childTitle.setText(data.get(groupPosition).getTipsList().get(childPosition).getTip());
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class GroupHolder {
        TextView title;
        TextView subTitle;
        ImageView webicon;
        CheckBox isUsedButton;

    }

    class ClideHolder {
        TextView childTitle;
    }

    private OnClikListener onClikListener;

    public void setOnClikListener(OnClikListener onClikListener) {
        this.onClikListener = onClikListener;
    }

    public interface OnClikListener {
        void selectCheckBoxClick(CheckBox checkBox, String id);

        void detailIconClick(String subscrib);
    }

    class CustemOnClickListener implements View.OnClickListener {

        private final CheckBox checkBox;
        private final String id;

        public CustemOnClickListener(CheckBox checkBox, String id) {
            this.checkBox = checkBox;
            this.id = id;
        }

        @Override
        public void onClick(View v) {
            if (onClikListener != null) {
                onClikListener.selectCheckBoxClick(checkBox, id);
            }
        }
    }
}
