package com.leetu.eman.models.returncar.beans;/**
 * Created by gn on 2016/10/10.
 */

/**
 * created by neo on 2016/10/10 14:47
 */
public class OrederDetailBean {
    OrederDetailOrder order;
    OrderDetailDotBean dot;

    public OrederDetailOrder getOrder() {
        return order;
    }

    public void setOrder(OrederDetailOrder order) {
        this.order = order;
    }

    public OrderDetailDotBean getDot() {
        return dot;
    }

    public void setDot(OrderDetailDotBean dot) {
        this.dot = dot;
    }
}
