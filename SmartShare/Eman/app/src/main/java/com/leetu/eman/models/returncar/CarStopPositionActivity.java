package com.leetu.eman.models.returncar;/**
 * Created by jyt on 2016/9/21.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.baidu.location.BDLocation;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.views.TitleBar;

/**
 * created by neo on 2016/9/21 10:05
 * 还车车位号
 */
public class CarStopPositionActivity extends BaseActivity implements View.OnClickListener, CarStopPositionContract.View, BaseActivity.MyLocationLisenter {
    private TitleBar titleBar;
    private Button btConfirmReturn;
    private EditText etFloor, etNumber;
    private CarStopPositionPresenter carStopPositionPresenter;
    private CurrentOrderBean currentOrderBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_stop_position);
        initView();
    }

    protected void initView() {
        if (getIntent().getExtras().getSerializable(ReturnCarActivity.ORDER_DETAIL) != null) {
            currentOrderBean = (CurrentOrderBean) getIntent().getExtras().getSerializable(ReturnCarActivity.ORDER_DETAIL);
        }
        titleBar = (TitleBar) findViewById(R.id.title_carstop);
        btConfirmReturn = (Button) findViewById(R.id.bt_confirm_returncar);
        etFloor = (EditText) findViewById(R.id.et_stopcar_number);
        etNumber = (EditText) findViewById(R.id.et_stopcar_floor);

        carStopPositionPresenter = new CarStopPositionPresenter(this, this);


        titleBar.setTitle("请输入车位号");
        titleBar.setLeftClickListener(this);
        btConfirmReturn.setOnClickListener(this);
        setMyLocationLisenter(this);

    }


    protected void loadData() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                CarStopPositionActivity.this.finish();
                break;
            case R.id.bt_confirm_returncar:
                showProgressBar(true);
                reGetLocation();
                break;
        }
    }

    @Override
    public void showReturnCar() {
        showProgressBar(false);
        Intent intent = new Intent(CarStopPositionActivity.this, PayOrderActivity.class);
        startActivity(intent);
    }

    @Override
    public void getLocation(BDLocation location) {
        carStopPositionPresenter.returnCar("" + location.getLatitude(), "" + location.getLongitude(), currentOrderBean.getCar().getCarId(), currentOrderBean.getDot().getDotId(), etFloor.getText().toString().trim(), etNumber.getText().toString().trim());
    }
}
