package com.leetu.eman.models.confirmorder;/**
 * Created by jyt on 2016/9/19.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.gn.myanimpulltorefreshlistview.pulltoimpl.PullToRefreshBase;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.confirmorder.adapter.ReturnCarAddressAdapter;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBean;
import com.gn.myanimpulltorefreshlistview.views.PullToRefreshListView;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * created by neo on 2016/9/19 13:25
 * 可还车的网点
 */
public class ReturnCarAddressActivity extends BaseActivity implements View.OnClickListener, ReturnCarAddressContract.View {
    private TitleBar titleBar;
    private ReturnCarAddressPresenter returnCarAddressPresenter;
    private String dotId;
    private PullToRefreshListView pullToRefreshListView;
    private ListView listView;
    private ReturnCarAddressAdapter returnCarAddressAdapter;
    private List<ReturnCarPointBean> listAll;
    public static final String RE_ADDRESS = "reAddress";
    private boolean isFrist = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_car_address);
        initView();
        loadData();
    }

    protected void initView() {
        if (getIntent().getStringExtra(ConfirmOrderActivity.DOT_ID) != null) {
            dotId = getIntent().getStringExtra(ConfirmOrderActivity.DOT_ID);
        }
        titleBar = (TitleBar) findViewById(R.id.title_returncar_address);
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.lv_returncar);
        listView = pullToRefreshListView.getRefreshableView();

        returnCarAddressPresenter = new ReturnCarAddressPresenter(this, this);
        listAll = new ArrayList<>();
        returnCarAddressAdapter = new ReturnCarAddressAdapter(listAll, this);

        titleBar.setTitle("可还车网点");
        titleBar.setLeftClickListener(this);
        listView.setAdapter(returnCarAddressAdapter);
        pullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                returnCarAddressPresenter.load(dotId, false, isFrist);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                position--;
                bundle.putSerializable(RE_ADDRESS, listAll.get(position));
                intent.putExtras(bundle);
                setResult(ConfirmOrderActivity.RESULT, intent);
                ReturnCarAddressActivity.this.finish();
            }
        });
    }


    private void loadData() {
        showLoading();
        returnCarAddressPresenter.load(dotId, false, true);
    }

    @Override
    protected void onRetryLoadData() {
        loadData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                ReturnCarAddressActivity.this.finish();
                break;
        }
    }

    @Override
    public void showLoad(List<ReturnCarPointBean> returnCarPoints) {
        isFrist = false;
        pullToRefreshListView.onRefreshComplete();
        showContent();
        if (returnCarPoints != null && returnCarPoints.size() > 0) {
            listAll.clear();
            listAll.addAll(returnCarPoints);
            returnCarAddressAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void loginOk() {
        super.loginOk();
        loadData();
    }
}
