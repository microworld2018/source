package com.leetu.eman.models.activity;

import android.content.Context;
import android.text.TextUtils;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.models.deposit.DepositRechargeActivity;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;

/**
 * Created by lvjunfeng on 2017/6/13.
 */

public class WebViewDetailPresent implements WebViewDetailContract.UserAction {
    private final Context context;
    private final WebViewDetailContract.View webListener;

    public WebViewDetailPresent(WebViewDetailContract.View webViewListener, Context context) {
        this.webListener = webViewListener;
        this.context = context;
    }

    @Override
    public void goShare() {
        if (NetworkHelper.isNetworkConnect(context)) {
            if(TextUtils.isEmpty(LeTravelApplication.getSharePreferenceInstance(context).getToken())) {
                return;
            }
            HttpEngine.post().url(URLS.ACTIVITY_SHARE_COUPNE)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .tag(WebViewDetailActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (response.getResultCode() == 200) {
                                LogUtils.e("lv", "分享领券成功:" + response.getData());
                            } else if(response.getResultCode() == 281  ){//非活动时间
                                LogUtils.e("lv", "非活动时间");
                            }else if(response.getResultCode() == 282  ){//您已获取过该优惠券
                                LogUtils.e("lv", "您已获取过该优惠券");

                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                        }
                    });
        }
    }

}
