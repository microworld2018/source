package com.leetu.eman.models.takecar;/**
 * Created by jyt on 2016/9/26.
 */

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.takecar.beans.TakeCarBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

import org.json.JSONObject;

/**
 * created by neo on 2016/9/26 10:24
 */
public class TakeCarPresenter extends BasePresenter implements TakeCarContract.UserAction {
    private Context context;
    private TakeCarContract.View takeCarListener;

    public TakeCarPresenter(Context context, TakeCarContract.View takeCarListener) {
        this.context = context;
        this.takeCarListener = takeCarListener;
    }

    @Override
    public void loadData() {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.CHECK_ORDER)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            LogUtils.e("gn", "取车检查用户订单" + response.getResultCode());
                            if (response.getResultCode() == 200) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response.getData());
                                    int orderType = jsonObject.getInt("orderType");
                                    if (orderType == 0) {
                                        TakeCarBean checkOrderBean = JsonParser.getParsedData(response.getData(), TakeCarBean.class);
                                        takeCarListener.showLoadData(checkOrderBean);
                                    } else if (orderType == 1) {
//                                    CurrentOrderBean currentOrderBean = JsonParser.getParsedData(response.getData(), CurrentOrderBean.class);
//                                    LogUtils.e("gn", currentOrderBean.toString());
//                                    currentOrderListener.showLoadData(currentOrderBean);
                                    }
                                } catch (Exception e) {
                                }
                            } else if (response.getResultCode() == 206) {

                            } else if (response.getResultCode() == 201) {
                                takeCarListener.dataFail();
                            } else {
                                takeCarListener.dataFail();
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            takeCarListener.timeOutFail();
                        }
                    });
        } else {
            takeCarListener.contentFail();
        }
    }

    @Override
    public void openOrCloseDoor(String orderId, final String state, String carId, String lat, String lng) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.OPEN_OR_CLOSE_DOOR)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("state", state)
                    .addParam("carId", carId)
                    .addParam("lat", lat)
                    .addParam("lng", lng)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), takeCarListener)) {
                                takeCarListener.showopenOrCloseDoorOk();
                            } else {
                                if (response.getResultCode() != 206) {
                                    takeCarListener.showFail(response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            takeCarListener.timeOutFail();
                        }
                    });
        } else {
            takeCarListener.showNetError("");
        }
    }

//    @Override
//    public void getCurrentOrder() {
//        if (NetworkHelper.isNetworkConnect(context)) {
//            HttpEngine.post().url(URLS.CHECK_ORDER)
//                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
//                    .execute(new HttpEngine.ResponseCallback() {
//                        @Override
//                        public void onResponse(ResponseStatus response) {
//                            if (checkCode(response.getResultCode(), takeCarListener)) {
//                                LogUtils.e("gn", "检查用户订单" + response.getData());
//                                try {
//                                    JSONObject jsonObject = new JSONObject(response.getData());
//                                    int orderType = jsonObject.getInt("orderType");
//                                    if (orderType == 0) {
//                                        TakeCarBean checkOrderBean = JsonParser.getParsedData(response.getData(), TakeCarBean.class);
//                                    } else if (orderType == 1) {
//                                        CurrentOrderBean currentOrderBean = JsonParser.getParsedData(response.getData(), CurrentOrderBean.class);
//                                        takeCarListener.showStartUseCar(currentOrderBean);
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            } else {
//                                if (response.getResultCode() != 206) {
//                                    takeCarListener.showFail(response.getResultMsg());
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Exception error) {
//                            takeCarListener.timeOutFail();
//                        }
//                    });
//        } else {
//            takeCarListener.showNetError("");
//        }
//    }

    @Override
    public void startUseCar(String orderId, String carId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.ORDER_START_CAR)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("carId", carId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), takeCarListener)) {
                                takeCarListener.showStartUseCar();
                            } else {
                                if (response.getResultCode() != 206) {
                                    takeCarListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            takeCarListener.timeOutFail();
                        }
                    });
        } else {
            takeCarListener.showNetError("");
        }
    }

    @Override
    public void cancelOrder(String orderId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.CANCEL_ORDER)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), takeCarListener)) {
                                takeCarListener.showCancelOrder();
                            } else {
                                if (response.getResultCode() != 206) {
                                    takeCarListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            takeCarListener.timeOutFail();
                        }
                    });
        } else {
            takeCarListener.showNetError("");
        }
    }
}
