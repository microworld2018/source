package com.leetu.eman.views;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leetu.eman.R;

/**
 * Created by Administrator on 2016/11/30.
 */

public class CustomEditText extends LinearLayout {
    private Context context;
    private int maxNum = 200;
    private int length = 0;
    private EditText cusEdit;
    private TextView textNum;
    private TextNumChangeListener textNumChangeListener;

    public CustomEditText(Context context) {
        super(context);
        this.context = context;
        init();
    }


    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }


    private void init() {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setOrientation(VERTICAL);
        inflater.inflate(R.layout.view_custom_edittext, this, true);
        cusEdit = (EditText) findViewById(R.id.cus_edit);
        textNum = (TextView) findViewById(R.id.text_num);
        textNum.setText(length + "/" + maxNum);
        cusEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxNum)});

        cusEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                length = s.length();
                if (length == maxNum) {
                    Toast.makeText(context, "最多输入字数为" + maxNum, Toast.LENGTH_SHORT).show();
                    textNum.setText(maxNum + "/0");
                } else {
                    int more = maxNum - length;
                    textNum.setText(length + "/" + more);
                }
                if (textNumChangeListener != null) {
                    if (length > 0) {
                        textNumChangeListener.onNumChangeListener(true);
                    } else {
                        textNumChangeListener.onNumChangeListener(false);
                    }
                }
            }
        });

    }

    public void setMaxLength(int max) {
        this.maxNum = max;
        cusEdit.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxNum)});
    }


    public void setHintText(String hintText) {
        if (!TextUtils.isEmpty(hintText)) {
            cusEdit.setHint(hintText);
        }
    }


    public String getText() {
        return cusEdit.getText().toString().trim();
    }

    public void setText(String text) {
        cusEdit.setText(text);
    }


    public interface TextNumChangeListener {
        void onNumChangeListener(boolean isChange);
    }

    public void setTextNumChangeListener(TextNumChangeListener textNumChangeListener) {
        this.textNumChangeListener = textNumChangeListener;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        getParent().requestDisallowInterceptTouchEvent(true);
        return super.dispatchTouchEvent(ev);
    }
}
