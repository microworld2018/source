//package com.leetu.eman.models.lenavigation;/**
// * Created by jyt on 2016/9/26.
// */
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Environment;
//import android.os.Handler;
//import android.os.Message;
//import android.view.View;
//import android.widget.Button;
//import android.widget.Toast;
//
//import com.baidu.location.BDLocation;
//import com.baidu.navisdk.adapter.BNOuterTTSPlayerCallback;
//import com.baidu.navisdk.adapter.BNRoutePlanNode;
//import com.baidu.navisdk.adapter.BNaviSettingManager;
//import com.baidu.navisdk.adapter.BaiduNaviManager;
//import com.huatai.gn.letravel.R;
//import com.huatai.gn.letravel.base.BaseActivity;
//import com.huatai.gn.letravel.models.takecar.TakeCarActivity;
//import com.huatai.gn.letravel.models.takecar.beans.ReCarDotBean;
//import com.huatai.gn.letravel.utils.LogUtils;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.LinkedList;
//import java.util.List;
//
///**
// * created by neo on 2016/9/26 15:54
// */
//public class LeNavigationActivity extends BaseActivity implements BaseActivity.MyLocationLisenter {
//    private String mSDCardPath = null;
//    private static final String APP_FOLDER_NAME = "BNSDKSimpleDemo";
//    String authinfo = null;
//    public static List<Activity> activityList = new LinkedList<Activity>();
//    public static final String ROUTE_PLAN_NODE = "routePlanNode";
//
//    BNRoutePlanNode sNode = null;
//    BNRoutePlanNode eNode = null;
//    private ReCarDotBean reCarDotBean;
//    public static int NA_REQUEST = 1000;
//    public static int NA_RESULT = 1001;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_le_navigtion);
//        initView();
//        loadData();
//    }
//
//    protected void initView() {
//        if (getIntent().getExtras().getSerializable(TakeCarActivity.TAKE_CAR_DOT) != null) {
//            reCarDotBean = (ReCarDotBean) getIntent().getExtras().getSerializable(TakeCarActivity.TAKE_CAR_DOT);
//        }
//
//        if (initDirs()) {
//            initNavi();
//        }
//    }
//
//
//    protected void loadData() {
//        this.setMyLocationLisenter(this);
//
//    }
//
//    private boolean initDirs() {
//        mSDCardPath = getSdcardDir();
//        if (mSDCardPath == null) {
//            return false;
//        }
//        File f = new File(mSDCardPath, APP_FOLDER_NAME);
//        if (!f.exists()) {
//            try {
//                f.mkdir();
//            } catch (Exception e) {
//                e.printStackTrace();
//                return false;
//            }
//        }
//        return true;
//    }
//
//    private String getSdcardDir() {
//        if (Environment.getExternalStorageState().equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
//            return Environment.getExternalStorageDirectory().toString();
//        }
//        return null;
//    }
//
//    private void initNavi() {
//
//        BNOuterTTSPlayerCallback ttsCallback = null;
//
//        BaiduNaviManager.getInstance().init(this, mSDCardPath, APP_FOLDER_NAME, new BaiduNaviManager.NaviInitListener() {
//            @Override
//            public void onAuthResult(int status, String msg) {
//                if (0 == status) {
////                    authinfo = "key校验成功!";
//                    finish();
//                } else {
//                    authinfo = "key校验失败, " + msg;
//                    LeNavigationActivity.this.runOnUiThread(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            Toast.makeText(LeNavigationActivity.this, authinfo, Toast.LENGTH_LONG).show();
//                        }
//                    });
//                }
//            }
//
//            public void initSuccess() {
//                showProgressBar(false);
////                showButtomToast("百度导航引擎初始化成功");
//                startNavigation();
//
//            }
//
//            public void initStart() {
//                showProgressBar(true);
////                Toast.makeText(LeNavigationActivity.this, "百度导航引擎初始化开始", Toast.LENGTH_SHORT).show();
//            }
//
//            public void initFailed() {
//                showProgressBar(false);
//                showButtomToast("百度导航引擎初始化失败");
//            }
//
//
//        }, null, ttsHandler, ttsPlayStateListener);
//
//    }
//
//    //开始定位
//    void startNavigation() {
//        reGetLocation();
//    }
//
//    private void initSetting() {
//        // 设置是否双屏显示
//        BNaviSettingManager.setShowTotalRoadConditionBar(BNaviSettingManager.PreViewRoadCondition.ROAD_CONDITION_BAR_SHOW_ON);
//        // 设置导航播报模式
//        BNaviSettingManager.setVoiceMode(BNaviSettingManager.VoiceMode.Veteran);
//        // 是否开启路况
//        BNaviSettingManager.setRealRoadCondition(BNaviSettingManager.RealRoadCondition.NAVI_ITS_ON);
//
//        if (BaiduNaviManager.isNaviInited()) {
//            routeplanToNavi();
//        }
//    }
//
//    /**
//     * 内部TTS播报状态回传handler
//     */
//    private Handler ttsHandler = new Handler() {
//        public void handleMessage(Message msg) {
//            int type = msg.what;
//            switch (type) {
//                case BaiduNaviManager.TTSPlayMsgType.PLAY_START_MSG: {
////                    showToastMsg("Handler : TTS play start");
//                    break;
//                }
//                case BaiduNaviManager.TTSPlayMsgType.PLAY_END_MSG: {
////                    showToastMsg("Handler : TTS play end");
//                    break;
//                }
//                default:
//                    break;
//            }
//        }
//    };
//    /**
//     * 内部TTS播报状态回调接口
//     */
//    private BaiduNaviManager.TTSPlayStateListener ttsPlayStateListener = new BaiduNaviManager.TTSPlayStateListener() {
//
//        @Override
//        public void playEnd() {
////            showToastMsg("TTSPlayStateListener : TTS play end");
//        }
//
//        @Override
//        public void playStart() {
////            showToastMsg("TTSPlayStateListener : TTS play start");
//        }
//    };
//
//    public void showToastMsg(final String msg) {
//        LeNavigationActivity.this.runOnUiThread(new Runnable() {
//
//            @Override
//            public void run() {
//                Toast.makeText(LeNavigationActivity.this, msg, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
//
//    private void routeplanToNavi() {
//        try {
////            eNode = new BNRoutePlanNode(Double.parseDouble(reCarDotBean.getLng()), Double.parseDouble(reCarDotBean.getLat()), reCarDotBean.getName(), null, BNRoutePlanNode.CoordinateType.BD09LL);
//        } catch (Exception e) {
//            showButtomToast("出问题了");
//            LogUtils.e("gn", reCarDotBean.toString());
//        }
//        if (sNode != null && eNode != null) {
//            List<BNRoutePlanNode> list = new ArrayList<BNRoutePlanNode>();
//            list.add(sNode);
//            list.add(eNode);
//            BaiduNaviManager.getInstance().launchNavigator(this, list, 1, true, new DemoRoutePlanListener(sNode));
//        }
//    }
//
//    @Override
//    public void getLocation(BDLocation location) {
//        sNode = new BNRoutePlanNode(location.getLongitude(), location.getLatitude(), location.getBuildingName(), null, BNRoutePlanNode.CoordinateType.BD09LL);
//        initSetting();
//    }
//
//    public class DemoRoutePlanListener implements BaiduNaviManager.RoutePlanListener {
//
//        private BNRoutePlanNode mBNRoutePlanNode = null;
//
//        public DemoRoutePlanListener(BNRoutePlanNode node) {
//            mBNRoutePlanNode = node;
//        }
//
//        @Override
//        public void onJumpToNavigator() {
//            /*
//             * 设置途径点以及resetEndNode会回调该接口
//			 */
//
//            for (Activity ac : activityList) {
//
//                if (ac.getClass().getName().endsWith("BNDemoGuideActivity")) {
//
//                    return;
//                }
//            }
//            Intent intent = new Intent(LeNavigationActivity.this, BNDemoGuideActivity.class);
//            Bundle bundle = new Bundle();
//            bundle.putSerializable(ROUTE_PLAN_NODE, (BNRoutePlanNode) mBNRoutePlanNode);
//            intent.putExtras(bundle);
//            startActivityForResult(intent, NA_REQUEST);
//
//        }
//
//        @Override
//        public void onRoutePlanFailed() {
//            // TODO Auto-generated method stub
//            Toast.makeText(LeNavigationActivity.this, "算路失败", Toast.LENGTH_SHORT).show();
//            finish();
//        }
//    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == NA_REQUEST) {
//            if (resultCode == NA_RESULT) {
//                finish();
//            }
//        }
//        super.onActivityResult(requestCode, resultCode, data);
//    }
//}
