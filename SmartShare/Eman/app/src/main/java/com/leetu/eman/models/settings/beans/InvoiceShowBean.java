package com.leetu.eman.models.settings.beans;

/**
 * Created by Larry.Shi on 2016/10/20.
 */

public class InvoiceShowBean {
    String amount;
    String subscriberId;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }
}
