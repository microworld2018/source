package com.leetu.eman.models.returncar.beans;/**
 * Created by gn on 2016/10/11.
 */

import java.io.Serializable;
import java.util.List;

/**
 * created by neo on 2016/10/11 13:48
 * 生成订单详情
 */
public class OrederCreateDetailBean implements Serializable {

    String alltime;
    double amount;
    double couponPrice;
    double allMilePrice;
    String orderId;
    double allPrice;
    double allMile;
    double payPrice;
    double allTimePrice;
    List<BaoXianBean> insurance;

    public String getAlltime() {
        return alltime;
    }

    public void setAlltime(String alltime) {
        this.alltime = alltime;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(double couponPrice) {
        this.couponPrice = couponPrice;
    }

    public double getAllMilePrice() {
        return allMilePrice;
    }

    public void setAllMilePrice(double allMilePrice) {
        this.allMilePrice = allMilePrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public double getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(double allPrice) {
        this.allPrice = allPrice;
    }

    public double getAllMile() {
        return allMile;
    }

    public void setAllMile(double allMile) {
        this.allMile = allMile;
    }

    public double getPayPrice() {
        return payPrice;
    }

    public void setPayPrice(double payPrice) {
        this.payPrice = payPrice;
    }

    public double getAllTimePrice() {
        return allTimePrice;
    }

    public void setAllTimePrice(double allTimePrice) {
        this.allTimePrice = allTimePrice;
    }

    public List<BaoXianBean> getInsurance() {
        return insurance;
    }

    public void setInsurance(List<BaoXianBean> insurance) {
        this.insurance = insurance;
    }
}
