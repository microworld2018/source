package com.leetu.eman.models.peccancyrecord;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gn.myanimpulltorefreshlistview.pulltoimpl.PullToRefreshBase;
import com.gn.myanimpulltorefreshlistview.views.PullToRefreshListView;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.deposit.bean.DepositWxPayBean;
import com.leetu.eman.models.peccancyrecord.adapter.PeccancyRecordAdapter;
import com.leetu.eman.models.peccancyrecord.beans.PeccancyRecordBean;
import com.leetu.eman.models.peccancyrecord.beans.PeccancyRecordList;
import com.leetu.eman.models.returncar.beans.PayBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.payutils.PayUtils;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.SelectorPayDialog;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

import static com.leetu.eman.wxapi.WXPayEntryActivity.ACTION_NAME;

public class PeccancyRecordActivity extends BaseActivity implements PeccancyRecordContract.View, View.OnClickListener {
    private TitleBar titleBar;
    private PullToRefreshListView pullToRefreshListView;
    private ListView listView;
    private LinearLayout listEmptyView;
    private TextView noneRecord;

    private PeccancyRecordPresenter peccancyRecordPresenter;
    private List<PeccancyRecordBean> list = new ArrayList<>();
    private SelectorPayDialog selectorPayDialog;
    private boolean isFrist = true;
    private PeccancyRecordAdapter peccancyRecordAdapter;
    private MyBroadcastReceiver myBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peccancy_record);
        initView();
        loadData();
        registWXPayReciver();
    }

    private void registWXPayReciver() {
        myBroadcastReceiver = new MyBroadcastReceiver();
        IntentFilter myIntentFilter = new IntentFilter();
        myIntentFilter.addAction(ACTION_NAME);
        registerReceiver(myBroadcastReceiver, myIntentFilter);
    }

    private void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_peccancy_order);
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.peccancy_listview);
        listEmptyView = (LinearLayout) findViewById(R.id.list_emptyview);
        noneRecord = (TextView) findViewById(R.id.none_record);
        listView = pullToRefreshListView.getRefreshableView();
        listView.setDivider(getResources().getDrawable(R.color.line));
        titleBar.setTitle("违章事故处理");
        titleBar.setLeftClickListener(this);

        peccancyRecordAdapter = new PeccancyRecordAdapter(this, list);
        noneRecord.setText("暂无违章记录");
        listView.setEmptyView(listEmptyView);
        listView.setAdapter(peccancyRecordAdapter);
        peccancyRecordPresenter = new PeccancyRecordPresenter(this, this);


        pullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                peccancyRecordPresenter.load(false, false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                peccancyRecordPresenter.load(true, false);
            }
        });

        peccancyRecordAdapter.setOnTvPayListener(new PeccancyRecordAdapter.TvPeccancyPayListener() {
            @Override
            public void onPayClickListener(String accidentId) {//此处现在传的不是ID  是描述信息，需更改
                if (!TextUtils.isEmpty(accidentId)) {
                    selectorPayWay(accidentId);
                }
            }
        });
    }

    private void loadData() {
        showLoading();
        peccancyRecordPresenter.load(false, isFrist);

    }


    @Override
    public void showLoad(PeccancyRecordList recordList, boolean isUp) {
        if (recordList == null) {
            return;
        }
        showContent();
        showProgressBar(false);
        isFrist = false;
        if (!isUp) {
            list.clear();
        }
        List<PeccancyRecordBean> accidentList = recordList.getAccidentList();
        if (accidentList != null && accidentList.size() > 0) {
            list.addAll(accidentList);
        }
        peccancyRecordAdapter.notifyDataSetChanged();
        pullToRefreshListView.onRefreshComplete();
    }

    @Override
    public void showWxPay(DepositWxPayBean wxPayBean) {
        showProgressBar(false);
        if (wxPayBean != null) {
            PayUtils.getInstance().wxPay(wxPayBean, PeccancyRecordActivity.this);
        }

    }

    @Override
    public void showAliPay(PayBean payBean) {
        showProgressBar(false);
        if (payBean != null) {
            PayUtils.getInstance().aliPay(payBean.getAlipay().getAliparameter(), PeccancyRecordActivity.this, new PayUtils.AliPayListener() {
                @Override
                public void onPayListener(String resultStatus, String resultInfo) {
                    if (TextUtils.equals(resultStatus, "9000")) {
                        showButtomToast("支付成功");
                        pullToRefreshListView.setRefreshing(true);
                    } else {
                        // 判断resultStatus 为非"9000"则代表可能支付失败
                        // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                        if (TextUtils.equals(resultStatus, "8000")) {
                            showButtomToast("支付结果确认中");
                        } else if (TextUtils.equals(resultStatus, "6001")) {//用户中途取消  5000重复请求
                            showButtomToast("取消支付");
                        } else {
                            // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                            showButtomToast("支付失败");
                            LogUtils.e("lv", "支付宝支付失败resultStatus：" + resultStatus);
                        }
                    }
                }
            });
        }
    }

    @Override
    public void showFail(String message) {
        super.showFail(message);
        pullToRefreshListView.onRefreshComplete();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                finish();
                break;
        }
    }

    @Override
    protected void onRetryLoadData() {
        super.onRetryLoadData();
        showLoading();
        peccancyRecordPresenter.load(false, isFrist);
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        peccancyRecordPresenter.load(false, true);
    }

    private void selectorPayWay(final String accidentId) {
        if (selectorPayDialog == null) {
            selectorPayDialog = new SelectorPayDialog(PeccancyRecordActivity.this);

            selectorPayDialog.setClicklistener(new SelectorPayDialog.ClickListenerInterface() {
                @Override
                public void aliPay() {
                    showProgressBar(true);
                    peccancyRecordPresenter.pay(accidentId, "3");
                }

                @Override
                public void wxPay() {
                    showProgressBar(true);
                    peccancyRecordPresenter.pay(accidentId, "5");
                }

                @Override
                public void cancelPay() {

                }
            });

            selectorPayDialog.show();

        } else {
            selectorPayDialog.show();
        }
        selectorPayDialog.goneCancel();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(PeccancyRecordActivity.class);
        unregisterReceiver(myBroadcastReceiver);
    }

    class MyBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ACTION_NAME)) {
                int err_code = intent.getIntExtra("err_code", 1);
                if (err_code == 0) {//成功
                    showButtomToast("支付成功！");
                    pullToRefreshListView.setRefreshing(true);
                } else if (err_code == -1) {
                    showButtomToast("支付失败！");
                } else if (err_code == -2) {
                    showButtomToast("支付取消！");
                }
            }
        }
    }
}
