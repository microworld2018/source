package com.leetu.eman.models.currentorder.carreport;/**
 * Created by gn on 2016/10/10.
 */

import android.content.Context;


import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.utils.PicUpUtils;

import java.util.List;

/**
 * created by neo on 2016/10/10 15:47
 */
public class ReportAccidentPresenter extends BasePresenter implements ReportAccidentContract.UserAction, PicUpUtils.PicListener {
    private Context context;
    private ReportAccidentContract.View reportAccidentListener;
    private String carId, orderId, carLose;
    private List<String> imgUrls;
    private PicUpUtils picUpUtils;

    public ReportAccidentPresenter(Context context, ReportAccidentContract.View reportAccidentListener) {
        this.context = context;
        this.reportAccidentListener = reportAccidentListener;
        picUpUtils = new PicUpUtils();
    }

    @Override
    public void upData(String carId, String orderId, String carLose, List<String> imgUrls) {
        this.carId = carId;
        this.orderId = orderId;
        this.carLose = carLose;
        this.imgUrls = imgUrls;
        picUpUtils.setPicListener(this);
        picUpUtils.upPic(context, imgUrls);
    }

    @Override
    public void picOk(String picUrls) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.CAR_ACCIDENT)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("carId", carId)
                    .addParam("carLose", carLose)
                    .addParam("imgUrls", picUrls)
                    .execute(new HttpEngine.ResponseCallback() {
                                 @Override
                                 public void onResponse(ResponseStatus response) {
                                     LogUtils.e("gn", "事故上报" + response.getData());
                                     if (checkCode(response.getResultCode(), reportAccidentListener)) {
                                         reportAccidentListener.showUpData();
                                     } else {
                                         if (response.getResultCode() != 206) {
                                             reportAccidentListener.showFail(response.getResultMsg());
                                         }
                                     }
                                 }

                                 @Override
                                 public void onFailure(Exception error) {
                                     reportAccidentListener.timeOutFail();
                                 }
                             }
                    );
        } else {
            reportAccidentListener.contentFail();
        }
    }

    @Override
    public void picFail(String msg) {
        reportAccidentListener.showFail(msg);
    }

    @Override
    public void picNetError() {
        reportAccidentListener.contentFail();
    }
}
