package com.leetu.eman.models.balance.beans;

import java.util.List;

/**
 * Created by Administrator on 2016/12/2.
 */

public class RechargeRecordList {
    private List<RechargeRecordBean> charges;
    private String amount;
    private String subscriberId;

    public String getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public List<RechargeRecordBean> getCharges() {
        return charges;
    }

    public void setCharges(List<RechargeRecordBean> charges) {
        this.charges = charges;
    }
}

