//package com.leetu.eman.mybaidumaputils;/**
// * Created by jyt on 2016/9/22.
// */
//
//import com.baidu.mapapi.map.BaiduMap;
//import com.baidu.mapapi.map.BitmapDescriptor;
//import com.baidu.mapapi.map.BitmapDescriptorFactory;
//import com.huatai.gn.letravel.R;
//
///**
// * created by neo on 2016/9/22 10:16
// */
//public class MyWalkRouteOverlay extends WalkingRouteOverlay {
//
//    public MyWalkRouteOverlay(BaiduMap baiduMap) {
//        super(baiduMap);
//    }
//
//    @Override
//    public BitmapDescriptor getStartMarker() {
//
//        return BitmapDescriptorFactory.fromResource(R.mipmap.ic_point);
//
//    }
//
//    @Override
//    public BitmapDescriptor getTerminalMarker() {
//        return BitmapDescriptorFactory.fromResource(R.mipmap.ic_point);
//
//    }
//}
