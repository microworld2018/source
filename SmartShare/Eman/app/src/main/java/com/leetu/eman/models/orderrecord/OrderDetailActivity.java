package com.leetu.eman.models.orderrecord;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.orderrecord.beans.OrderDetailBean;
import com.leetu.eman.models.returncar.PayOrderActivity;
import com.leetu.eman.models.returncar.beans.BaoXianBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.views.TitleBar;

import java.util.List;


/**
 * 订单详情 neo
 */
public class OrderDetailActivity extends BaseActivity implements View.OnClickListener, OrderDetailContract.View {
    private OrderDetailPresenter orderDetailPresenter;
    private TitleBar titleBar;
    private String orderId;
    private Button btnPay;
    private TextView tvOrderTime, tvCartype, tvCarId, tvTakePosition, tvReturePosition, tvRetureTime, tvAllmileage, tvAllTime, tvMileageCost, tvTimeCost, tvFactCost, tvCoupon;
    private TextView tvBaoXianName, tvBaoXianCost;
    private RelativeLayout rlOrderinfoCoupon;
    private LinearLayout ltOrderBaoXian;
    private boolean isFrist = true;
    private String orderPayStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        initView();
        loadData();
    }

    protected void initView() {
        if (getIntent().getStringExtra(OrdersRecordActivity.ORDER_ID) != null) {
            orderId = getIntent().getStringExtra(OrdersRecordActivity.ORDER_ID);
        }
        if (getIntent().getStringExtra(OrdersRecordActivity.ORDER_PAYSTATUS) != null) {
            orderPayStatus = getIntent().getStringExtra(OrdersRecordActivity.ORDER_PAYSTATUS);
        }

        titleBar = (TitleBar) findViewById(R.id.title_orderdetail);
        tvOrderTime = (TextView) findViewById(R.id.tv_orderdetail_time);
        tvCartype = (TextView) findViewById(R.id.tv_orderdetail_cartype);
        tvCarId = (TextView) findViewById(R.id.tv_orderdetail_cartid);
        tvTakePosition = (TextView) findViewById(R.id.tv_orderdetail_takecartposition);
        tvRetureTime = (TextView) findViewById(R.id.tv_orderdetail_returecartime);
        tvReturePosition = (TextView) findViewById(R.id.tv_orderdetail_returecartposition);

        tvAllmileage = (TextView) findViewById(R.id.tv_orderdetail_mileage);
        tvAllTime = (TextView) findViewById(R.id.tv_orderdetail_alltime);
        tvMileageCost = (TextView) findViewById(R.id.tv_orderdetail_mileagecost);
        tvTimeCost = (TextView) findViewById(R.id.tv_orderdetail_timecost);
        tvFactCost = (TextView) findViewById(R.id.tv_orderdetail_factcost);
        tvCoupon = (TextView) findViewById(R.id.tv_orderdetail_coupon);
        rlOrderinfoCoupon = (RelativeLayout) findViewById(R.id.rl_orderinfo_coupon);

        ltOrderBaoXian = (LinearLayout) findViewById(R.id.ll_orderdetail_baoxian);
        btnPay = (Button) findViewById(R.id.btn_order_pay);

        orderDetailPresenter = new OrderDetailPresenter(this, this);
        if (orderPayStatus.equals("0")) {//未支付
            btnPay.setVisibility(View.VISIBLE);
        } else if (orderPayStatus.equals("1")) {//已完成
            btnPay.setVisibility(View.GONE);
        }
        titleBar.setTitle("订单详情");
        titleBar.setLeftClickListener(this);
        btnPay.setOnClickListener(this);
    }

    protected void loadData() {
        if (orderId != null) {
            showLoading();
            orderDetailPresenter.load(orderId, isFrist);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                OrderDetailActivity.this.finish();
                break;
            case R.id.btn_order_pay:
                Intent intent = new Intent(OrderDetailActivity.this, PayOrderActivity.class);
                intent.putExtra(PAY_FLAGS, "1");
                startActivity(intent);
                OrderDetailActivity.this.finish();
                break;
        }
    }

    @Override
    public void showLoad(OrderDetailBean orderDetailBean) {
        showProgressBar(false);
        showContent();
        isFrist = false;
        if (orderDetailBean != null) {

            tvOrderTime.setText(orderDetailBean.getCreateOrderTime());
            tvCartype.setText(orderDetailBean.getCarName());
            tvCarId.setText(orderDetailBean.getCarNumber());
            tvTakePosition.setText(orderDetailBean.getTakedot());
            tvRetureTime.setText(orderDetailBean.getReturnCarTime());
            tvReturePosition.setText(orderDetailBean.getReturndot());

            tvAllmileage.setText(orderDetailBean.getAllMile() + "");
            tvAllTime.setText(orderDetailBean.getAlltime() + "");
            tvMileageCost.setText("¥" + orderDetailBean.getAllMilePrice());
            tvTimeCost.setText("¥" + orderDetailBean.getAllTimePrice());
            tvFactCost.setText("¥" + orderDetailBean.getAllPrice());
            //保险
            if (orderDetailBean.getInsurance() != null && orderDetailBean.getInsurance().size() > 0) {
                List<BaoXianBean> baoXianDatas = orderDetailBean.getInsurance();
                ltOrderBaoXian.removeAllViews();
                for (int i = 0; i < baoXianDatas.size(); i++) {
                    View viewItem = LayoutInflater.from(this).inflate(R.layout.layout_paybaoxianitem, null);
                    tvBaoXianName = (TextView) viewItem.findViewById(R.id.tv_order_baoxianname);
                    tvBaoXianCost = (TextView) viewItem.findViewById(R.id.tv_pay_chexiancost);
                    tvBaoXianName.setText(baoXianDatas.get(i).getInsuranceName());
                    tvBaoXianCost.setText("￥"+baoXianDatas.get(i).getInsuranceFee());
                    ltOrderBaoXian.addView(viewItem);
                }
            }
            if (orderDetailBean.getCoupon() != null && !"0".equals(orderDetailBean.getCoupon())) {//未使用优惠券时隐藏
                rlOrderinfoCoupon.setVisibility(View.VISIBLE);
                tvCoupon.setText("- ¥" + orderDetailBean.getCoupon());
            } else {
                rlOrderinfoCoupon.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void showFail(String message) {
        btnPay.setVisibility(View.GONE);
        super.showFail(message);
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        orderDetailPresenter.load(orderId, true);
    }

    @Override
    protected void onRetryLoadData() {
        super.onRetryLoadData();
        loadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(OrderDetailActivity.class);
    }
}
