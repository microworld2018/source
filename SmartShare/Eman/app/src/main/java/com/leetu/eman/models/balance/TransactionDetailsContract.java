package com.leetu.eman.models.balance;


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.balance.beans.RechargeRecordBean;

import java.util.List;

/**
 * Created by Administrator on 2016/11/5.
 */

public interface TransactionDetailsContract {
    interface View extends BaseContract {
        void showLoadData(boolean isUp, List<RechargeRecordBean> getRechargeCards);
    }

    interface UserAction {
        void loadData(boolean isUp, boolean isFrist);
    }
}
