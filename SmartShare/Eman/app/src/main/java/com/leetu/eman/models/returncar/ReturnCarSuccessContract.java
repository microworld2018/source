package com.leetu.eman.models.returncar;

import com.leetu.eman.base.BaseContract;

/**
 * Created by lvjunfeng on 2017/1/10.
 */

public interface ReturnCarSuccessContract {
    interface View extends BaseContract {
        void shareSuccess(boolean isWxFriend);
    }

    interface UserAction {
        void goShare(boolean isWxFriend);
    }
}
