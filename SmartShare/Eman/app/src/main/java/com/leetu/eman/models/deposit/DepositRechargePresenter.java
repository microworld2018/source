package com.leetu.eman.models.deposit;

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.deposit.bean.DepositRechargeBean;
import com.leetu.eman.models.deposit.bean.DepositWxPayBean;
import com.leetu.eman.models.returncar.beans.PayBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

/**
 * Created by lvjunfeng on 2017/4/5.
 */

public class DepositRechargePresenter extends BasePresenter implements DepositRechargeContract.UserAction {

    private final DepositRechargeContract.View depositListener;
    private final Context mContext;
    public DepositRechargePresenter(Context context, DepositRechargeContract.View depositListener) {
        this.mContext = context;
        this.depositListener = depositListener;
    }

    @Override
    public void loadData(final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(mContext)) {
            HttpEngine.post().url(URLS.GET_RECHARG_AMOUNT)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(mContext).getToken())
                    .tag(DepositRechargeActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), depositListener)) {
                                LogUtils.e("lv", "押金支付Data:" + response.getData());
                                DepositRechargeBean amountBean = JsonParser.getParsedData(response.getData(), DepositRechargeBean.class);
                                if(amountBean != null) {
                                    depositListener.showLoad(amountBean);
                                }
                            } else {
                                if (response.getResultCode() != 206) {
                                        depositListener.showFail(response.getResultCode(), response.getResultMsg());

                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            depositListener.timeOutFail();
                        }
                    });
        } else {
            if(isFrist) {
                depositListener.contentFail();
            }else {
                depositListener.showNetError("");
            }
        }
    }

    @Override
    public void loadPay(String payType) {
        if ("3".equals(payType)) {
            goToAliPay(payType);
        } else if ("5".equals(payType)) {
            goToWxPay(payType);
        }
    }



    private void goToWxPay(String payType) {
        if (NetworkHelper.isNetworkConnect(mContext)) {
            HttpEngine.post().url(URLS.RECHARG_DEPOSIT)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(mContext).getToken())
                    .addParam("payType", payType)
                    .tag(DepositRechargeActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), depositListener)) {
                                LogUtils.e("gn", "微信支付押金Data:" + response.getData());
                                DepositWxPayBean wxPayBean = JsonParser.getParsedData(response.getData(), DepositWxPayBean.class);
                                if(wxPayBean != null) {
                                depositListener.showWxPay(wxPayBean);
                                }
                            } else {
                                if (response.getResultCode() != 206) {
                                        depositListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            depositListener.timeOutFail();
                        }
                    });
        } else {
            depositListener.showNetError("");
        }
    }

    private void goToAliPay(String payType) {
        if (NetworkHelper.isNetworkConnect(mContext)) {
            HttpEngine.post().url(URLS.RECHARG_DEPOSIT)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(mContext).getToken())
                    .addParam("payType", payType)
                    .tag(DepositRechargeActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), depositListener)) {
                                LogUtils.e("gn", "支付宝支付押金" + response.getData());
                                PayBean payBean = JsonParser.getParsedData(response.getData(), PayBean.class);
                                if(payBean != null) {
                                    depositListener.showAliPay(payBean);
                                }
                            } else {
                                if (response.getResultCode() != 206) {
                                        depositListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            depositListener.timeOutFail();
                        }
                    });
        } else {
            depositListener.showNetError("");
        }

    }
//    @Override
//    public void paySuccess(String resultStatus, String result) {
//        if (NetworkHelper.isNetworkConnect(mContext)) {
//            HttpEngine.post().url(URLS.PAY_SUCCESS)
//                    .addParam("resultStatus", resultStatus)
//                    .addParam("result", result)
//                    .execute3(new HttpEngine.ResponseCallback() {
//                        @Override
//                        public void onResponse(ResponseStatus response) {
//                            if (checkCode(response.getResultCode(), depositListener)) {
//                                LogUtils.e("gn", "支付成功" + response.getData());
//                                depositListener.showPaySuccess();
//                            } else {
//                                if (response.getResultCode() != 206) {
//                                    depositListener.showFail(response.getResultCode(), response.getResultMsg());
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Exception error) {
//                            depositListener.timeOutFail();
//                        }
//                    });
//        } else {
//            depositListener.showNetError("");
//        }
//    }


}
