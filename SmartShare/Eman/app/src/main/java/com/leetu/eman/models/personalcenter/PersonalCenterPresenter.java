package com.leetu.eman.models.personalcenter;/**
 * Created by gn on 2016/9/29.
 */

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

/**
 * created by neo on 2016/9/29 13:52
 */
public class PersonalCenterPresenter extends BasePresenter implements PersonalCenterContract.UserAction {
    private Context context;
    private PersonalCenterContract.View personalCenterListener;

    public PersonalCenterPresenter(Context context, PersonalCenterContract.View personalCenterListener) {
        this.context = context;
        this.personalCenterListener = personalCenterListener;
    }

    @Override
    public void load() {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, personalCenterListener)) {
                HttpEngine.post().url(URLS.USER_INFO)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .tag(PersonalCenterActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), personalCenterListener)) {
                                    LogUtils.e("gn", "个人信息" + response.getData());
                                    UserInfoBean userInfoBean = JsonParser.getParsedData(response.getData(), UserInfoBean.class);
                                    if (userInfoBean != null) {
                                        LeTravelApplication.getSharePreferenceInstance(context).saveUserStatus(userInfoBean.getState(),userInfoBean.getHandHeldIdCard());
                                        personalCenterListener.showLoad(userInfoBean);
                                    }
                                } else {
                                    if (response.getResultCode() != 206) {
                                        personalCenterListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                personalCenterListener.timeOutFail();
                            }
                        });
            }
        } else {
            personalCenterListener.contentFail();
        }
    }

    @Override
    public void submit(String nickname, String nickstatus) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post()
                    .url(URLS.USER_INFO_UPDATE)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("nickname", nickname)
                    .addParam("nickstatus", nickstatus)
                    .tag(PersonalCenterActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), personalCenterListener)) {
                                LogUtils.e("gn", "修改个人信息" + response.getData());
                                personalCenterListener.showSubmit();
                            } else {
                                if (response.getResultCode() != 206) {
                                    personalCenterListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            personalCenterListener.timeOutFail();
                        }
                    });
        } else {
            personalCenterListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }
}
