package com.leetu.eman.models.usecar;/**
 * Created by jyt on 2016/9/22.
 */


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.models.usecar.bean.AdBean;
import com.leetu.eman.models.usecar.bean.OverLayBean;

import java.util.List;

/**
 * created by neo on 2016/9/22 16:05
 */
public interface MainContract {
    interface View extends BaseContract {
        void showPoint(List<OverLayBean> overLayBeanList);

        void showUserInfo(UserInfoBean userInfoBean);

        void showAd(AdBean adBean);

    }

    interface UserAction {
        void getPoint(String lat, String lng);

        void getUserInfo();


        void getAdInfo();

    }
}
