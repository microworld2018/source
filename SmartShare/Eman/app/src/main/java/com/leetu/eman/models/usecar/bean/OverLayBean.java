package com.leetu.eman.models.usecar.bean;/**
 * Created by jyt on 2016/9/21.
 */

import java.io.Serializable;

/**
 * created by neo on 2016/9/21 13:42
 * 覆盖物
 */
public class OverLayBean implements Serializable {
    private double latitude;
    private double longitude;
    String name;
    String carcount;
    String dotId;
    String distance;
    String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarcount() {
        return carcount;
    }

    public void setCarcount(String carcount) {
        this.carcount = carcount;
    }

    public String getDotId() {
        return dotId;
    }

    public void setDotId(String dotId) {
        this.dotId = dotId;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "OverLayBean{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", name='" + name + '\'' +
                ", carcount='" + carcount + '\'' +
                ", dotId='" + dotId + '\'' +
                ", distance='" + distance + '\'' +
                '}';
    }
}
