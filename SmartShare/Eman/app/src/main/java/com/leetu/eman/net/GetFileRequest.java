package com.leetu.eman.net;

import android.os.Environment;

import com.leetu.eman.utils.LogUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.GetBuilder;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zhy.http.okhttp.request.RequestCall;

import java.io.File;
import java.util.HashMap;

import okhttp3.Call;

/**
 * Created by lvjunfeng on 2017/6/2.
 */

public class GetFileRequest {

    private String url;
    //默认路径及文件名
    private String fileName = "emanAd.jpg";
    private String parentPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private long readTimeout;
    private long writeTimeout;
    private long connectionTimeout;
    private Object tag;

    private HashMap<String, String> params = new HashMap<String, String>();
    private HashMap<String, String> actions = new HashMap<String, String>();

    public GetFileRequest() {
    }

    public GetFileRequest imgUrl(String url) {
        this.url = url;
        return this;
    }

    public GetFileRequest fileName(String name) {
        this.fileName = name;
        return this;
    }

    public GetFileRequest fileDir(String filePath) {
        this.parentPath = filePath;
        return this;
    }

    public GetFileRequest readTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    public GetFileRequest writeTimeout(long writeTimeout) {
        this.writeTimeout = writeTimeout;
        return this;
    }

    public GetFileRequest connectionTimeout(long connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
        return this;
    }

    public GetFileRequest tag(Object tag) {
        this.tag = tag;
        return this;
    }

    public GetFileRequest addParams(String key, String value) {
        params.put(key, value);
        return this;
    }

    public GetFileRequest addAction(String key, String value) {
        actions.put(key, value);
        return this;
    }

    public void execute(final HttpEngine.FileCallBack callback) {
//
        GetBuilder getBuilder = OkHttpUtils
                .get();

        LogUtils.e("lv", "准备执行下载文件");
        RequestCall call = getBuilder.tag(tag)
                .url(url)
                .build();


        if (readTimeout > 0) {
            call.readTimeOut(readTimeout);
        }

        if (writeTimeout > 0) {
            call.writeTimeOut(writeTimeout);
        }

        if (connectionTimeout > 0) {
            call.connTimeOut(connectionTimeout);
        }
        call.execute(new FileCallBack(parentPath, fileName) {
            @Override
            public void onError(Call call, Exception e) {
                if (callback != null) {
                    callback.onError(call, e);
                }
            }

            @Override
            public void onResponse(File response) {
                LogUtils.e("lv","广告图片写出成功");
                if (callback != null) {
                    callback.onResponse(response);
                }
            }

            @Override
            public void inProgress(float progress, long total) {
                callback.inProgress(progress, total);
            }
        });
    }
}
