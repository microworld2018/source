package com.leetu.eman.models.currentorder.carreport;/**
 * Created by gn on 2016/10/10.
 */


import com.leetu.eman.base.BaseContract;

import java.util.List;

/**
 * created by neo on 2016/10/10 15:43
 */
public interface ReportDestoryContract {
    interface View extends BaseContract {
        void showUpData();
    }

    interface UserAction {
        void upData(String subscriberId, String subscriberName, String orderId, String orderNumber, String carId, String carNumber, String breakTypeFacade, String breakTypeTyre, String breakTypeDecoration, String breakTypeGlass, String breakTypeOther, String breakTypeOdor, String breakDesc, List<String> breakImg);
    }
}
