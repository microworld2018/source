package com.leetu.eman.models.peccancyrecord;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.deposit.bean.DepositWxPayBean;
import com.leetu.eman.models.peccancyrecord.beans.PeccancyRecordList;
import com.leetu.eman.models.returncar.PayOrderActivity;
import com.leetu.eman.models.returncar.beans.PayBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;


/**
 * Created by jyt on 2016/9/18.
 */

public class PeccancyRecordPresenter extends BasePresenter
        implements PeccancyRecordContract.UserAction {
    private Context context;
    private PeccancyRecordContract.View peccancyRecordListener;
    private int page = 1;

    public PeccancyRecordPresenter(Context context, PeccancyRecordContract.View peccancyRecordListener) {
        this.context = context;
        this.peccancyRecordListener = peccancyRecordListener;
    }

    @Override
    public void load(final boolean isUp, final boolean isFrist) {

        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, peccancyRecordListener)) {
                if (!isUp) {
                    page = 1;
                }
                HttpEngine.post().url(URLS.MyPeccancy)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .addParam("currentPage", page + "")
                        .tag(PeccancyRecordActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), peccancyRecordListener)) {
                                    LogUtils.e("gn", "订单记录" + response.getData());
                                    PeccancyRecordList recordList = JsonParser.getParsedData(response.getData(), PeccancyRecordList.class);

                                    if (recordList != null) {
                                        if (recordList.getAccidentList() != null && recordList.getAccidentList().size() != 0) {
                                            page++;
                                        }
                                        peccancyRecordListener.showLoad(recordList, isUp);
                                    }
                                } else {
                                    if (response.getResultCode() != 206) {
                                        peccancyRecordListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                peccancyRecordListener.timeOutFail();

                            }
                        });


            }
        } else {
            if (isFrist) {
                peccancyRecordListener.contentFail();
            } else {
                peccancyRecordListener.showFail(context.getResources().getString(R.string.net_error));
            }
        }
    }


    @Override
    public void pay(String accidentId, String payType) {
        if ("3".equals(payType)) {
            goToAliPay(accidentId, payType);
        } else if ("5".equals(payType)) {
            goToWxPay(accidentId, payType);
        }
    }


    private void goToWxPay(String accidentId, String payType) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.ACCIDENT_PAY)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("accidentId", accidentId)
                    .addParam("payType", payType)
                    .tag(PayOrderActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), peccancyRecordListener)) {
                                LogUtils.e("gn", "支付订单Data:" + response.getData());
                                DepositWxPayBean wxPayBean = JsonParser.getParsedData(response.getData(), DepositWxPayBean.class);
                                peccancyRecordListener.showWxPay(wxPayBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    peccancyRecordListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            peccancyRecordListener.timeOutFail();
                        }
                    });
        } else {
            peccancyRecordListener.showNetError("");
        }
    }

    private void goToAliPay(String accidentId, String payType) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.ACCIDENT_PAY)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("accidentId", accidentId)
                    .addParam("payType", payType)
                    .tag(PayOrderActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), peccancyRecordListener)) {
                                LogUtils.e("gn", "支付订单" + response.getData());
                                PayBean payBean = JsonParser.getParsedData(response.getData(), PayBean.class);
                                peccancyRecordListener.showAliPay(payBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    peccancyRecordListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            peccancyRecordListener.timeOutFail();
                        }
                    });
        } else {
            peccancyRecordListener.showNetError("");
        }

    }

//    @Override
//    public void paySuccess(String resultStatus, String result) {
//        if (NetworkHelper.isNetworkConnect(context)) {
//            HttpEngine.post().url(URLS.PAY_SUCCESS)
//                    .addParam("resultStatus", resultStatus)
//                    .addParam("result", result)
//                    .execute3(new HttpEngine.ResponseCallback() {
//                        @Override
//                        public void onResponse(ResponseStatus response) {
//                            if (checkCode(response.getResultCode(), peccancyRecordListener)) {
//                                LogUtils.e("gn", "支付成功" + response.getData());
//                                peccancyRecordListener.showPaySuccess();
//                            } else {
//                                if (response.getResultCode() != 206) {
//                                    peccancyRecordListener.showFail(response.getResultCode(), response.getResultMsg());
//                                }
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Exception error) {
//                            peccancyRecordListener.timeOutFail();
//                        }
//                    });
//        } else {
//            peccancyRecordListener.showNetError("");
//        }
//    }
//
//    @Override
//    public void errorPay(String code, String orderId) {
//        if (NetworkHelper.isNetworkConnect(context)) {
//            HttpEngine.post().url(URLS.PAY_Err)
//                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
//                    .addParam("code", code)
//                    .addParam("orderId", orderId)
//                    .execute4(new HttpEngine.ResponseCallback() {
//                        @Override
//                        public void onResponse(ResponseStatus response) {//重复支付
//                            if (checkCode(response.getResultCode(), peccancyRecordListener)) {
//                                peccancyRecordListener.showPaySuccess();
//                            } else {
//                                peccancyRecordListener.showFail(response.getResultCode(), response.getResultMsg());
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Exception error) {
//                            peccancyRecordListener.timeOutFail();
//                        }
//                    });
//        } else {
//            peccancyRecordListener.showFail(context.getResources().getString(R.string.net_error));
//        }
//    }


}
