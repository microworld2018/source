package com.leetu.eman.models.takecar.beans;/**
 * Created by jyt on 2016/9/27.
 */

/**
 * created by neo on 2016/9/27 15:54
 */
public class CarDetailbean {
    String androidModelPhoto;
    String lifeMileage;
    String version;
    String name;
    String carId;
    String iosModelPhoto;
    String vehiclePlateId;
    String percent;
    String cnName;

    public String getAndroidModelPhoto() {
        return androidModelPhoto;
    }

    public void setAndroidModelPhoto(String androidModelPhoto) {
        this.androidModelPhoto = androidModelPhoto;
    }

    public String getLifeMileage() {
        return lifeMileage;
    }

    public void setLifeMileage(String lifeMileage) {
        this.lifeMileage = lifeMileage;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getIosModelPhoto() {
        return iosModelPhoto;
    }

    public void setIosModelPhoto(String iosModelPhoto) {
        this.iosModelPhoto = iosModelPhoto;
    }

    public String getVehiclePlateId() {
        return vehiclePlateId;
    }

    public void setVehiclePlateId(String vehiclePlateId) {
        this.vehiclePlateId = vehiclePlateId;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }
}
