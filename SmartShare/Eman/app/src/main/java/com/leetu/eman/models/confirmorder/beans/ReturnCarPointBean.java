package com.leetu.eman.models.confirmorder.beans;/**
 * Created by jyt on 2016/9/27.
 */

import java.io.Serializable;

/**
 * created by neo on 2016/9/27 13:34
 */
public class ReturnCarPointBean implements Serializable {
    String address;
    String name;
    String latlng;
    String dotid;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getDotid() {
        return dotid;
    }

    public void setDotid(String dotid) {
        this.dotid = dotid;
    }
}
