package com.leetu.eman.mybaidumaputils;/**
 * Created by jyt on 2016/9/22.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;

import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.leetu.eman.R;
import com.leetu.eman.models.usecar.bean.OverLayBean;

/**
 * created by neo on 2016/9/22 09:07
 */
public class MyBaiduMapUtils {
    //自定义覆盖物
    public Bitmap getViewBitmap(View addViewContent) {
        addViewContent.setDrawingCacheEnabled(true);
        addViewContent.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        addViewContent.layout(0, 0,
                addViewContent.getMeasuredWidth(),
                addViewContent.getMeasuredHeight());

        addViewContent.buildDrawingCache();
        Bitmap cacheBitmap = addViewContent.getDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        return bitmap;
    }


    public BitmapDescriptor getBitmapDescriptor2(View view) {
        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory
                .fromBitmap(getViewBitmap(view));
        return bitmapDescriptor;
    }

    public BitmapDescriptor getBitmapDescriptor(Context context, View overLayout, OverLayBean overLayBean, boolean isUse) {
        View view = LayoutInflater.from(context).inflate(R.layout.my_overlay_view, null);
        MyOverLayView myOverLayView = (MyOverLayView) view.findViewById(R.id.mv_overlay);
        myOverLayView.setCount(overLayBean.getCarcount());
        myOverLayView.useOrNoUse(isUse);
        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory
                .fromBitmap(getViewBitmap(myOverLayView));
        return bitmapDescriptor;
    }


}
