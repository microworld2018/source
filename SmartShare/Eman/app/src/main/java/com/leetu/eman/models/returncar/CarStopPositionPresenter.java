package com.leetu.eman.models.returncar;/**
 * Created by gn on 2016/10/11.
 */

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;


/**
 * created by neo on 2016/10/11 13:32
 */
public class CarStopPositionPresenter extends BasePresenter implements CarStopPositionContract.UserAction {
    private Context context;
    private CarStopPositionContract.View carStopPositionListener;

    public CarStopPositionPresenter(Context context, CarStopPositionContract.View carStopPositionListener) {
        this.context = context;
        this.carStopPositionListener = carStopPositionListener;
    }

    @Override
    public void returnCar(String lat, String lng, String carId, String dotId, String floorNo, String carNo) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.RETURN_CAR)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("lat", lat)
                    .addParam("lng", lng)
                    .addParam("carId", carId)
                    .addParam("dotId", dotId)
                    .addParam("floorNo", floorNo)
                    .addParam("carNo", carNo)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), carStopPositionListener)) {
                                LogUtils.e("gn", "确认还车" + response.getData());
                                carStopPositionListener.showReturnCar();
                            } else {
                                if (response.getResultCode() != 206) {
                                    carStopPositionListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            carStopPositionListener.timeOutFail();
                        }
                    });
        } else {
            carStopPositionListener.showNetError("");
        }
    }

}
