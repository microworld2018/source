package com.leetu.eman.models.orderrecord;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.orderrecord.beans.OrderRecordList;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;


/**
 * Created by jyt on 2016/9/18.
 */

public class OrdersRecordPresenter extends BasePresenter
        implements OrderRecordContract.UserAction {
    private Context context;
    private OrderRecordContract.View orderRecordListener;
    private int page = 1;

    public OrdersRecordPresenter(Context context, OrderRecordContract.View orderRecordListener) {
        this.context = context;
        this.orderRecordListener = orderRecordListener;
    }

    @Override
    public void load(final boolean isUp, final boolean isFrist) {

        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, orderRecordListener)) {
                if (!isUp) {
                    page = 1;
                }
                HttpEngine.post().url(URLS.ORDER_RECORD)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .addParam("currentPage", page + "")
                        .tag(OrdersRecordActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), orderRecordListener)) {
                                    LogUtils.e("gn", "订单记录" + response.getData());
                                    OrderRecordList recordList = JsonParser.getParsedData(response.getData(), OrderRecordList.class);

                                    if (recordList != null) {
                                        if (recordList.getOrders() != null) {
                                            page++;
                                            orderRecordListener.showLoad(recordList.getOrders(), isUp);
                                        }
                                    }
                                } else {
                                    if (response.getResultCode() != 206) {
                                        if (isFrist) {
                                            orderRecordListener.contentFail();
                                        } else {
                                            orderRecordListener.showFail(response.getResultCode(), response.getResultMsg());
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                orderRecordListener.timeOutFail();

                            }
                        });
            }
        } else {
            if (isFrist) {
                orderRecordListener.contentFail();
            } else {
                orderRecordListener.showFail(context.getResources().getString(R.string.net_error));
            }
        }
    }


}
