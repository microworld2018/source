package com.leetu.eman.logindialog;/**
 * Created by gn on 2016/9/28.
 */


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.login.bean.UserBean;

/**
 * created by neo on 2016/9/28 13:46
 */
public interface LoginDialogContract {
    interface View extends BaseContract {
        void showDialogLoad(UserBean userBean);

        void showDialogSendCode();
    }

    interface UserAction {
        void dialogLoad(String phoneNumber, String code);

        void dialogSendCode(String phoneNumber);
    }
}
