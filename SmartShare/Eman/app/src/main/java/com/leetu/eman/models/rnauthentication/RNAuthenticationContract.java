package com.leetu.eman.models.rnauthentication;


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;

import java.util.List;

/**
 * Created by jyt on 2016/9/19.
 */

public interface RNAuthenticationContract {
    interface View extends BaseContract {
        void showLoad(UserInfoBean userInfoBean);

        void showRNA();
    }

    interface UserAction {
        void load(boolean isFrist);

        void upData(String name, String cardId, List<String> files);

        void upDataAgain(String name, String cardId, String cardImg, String driverImg);

        void upDataAgain(String name, String cardId, String idcardimg,String img, List<String> files, int type);
    }
}
