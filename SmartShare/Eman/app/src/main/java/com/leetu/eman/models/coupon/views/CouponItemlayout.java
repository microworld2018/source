package com.leetu.eman.models.coupon.views;

import android.content.Context;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leetu.eman.R;

/**
 * Created by neo on 2016/12/17.
 */

public class CouponItemlayout extends LinearLayout {
    private CouponBgView couponBgView;
    private TextView tvTtile, tvDate, tvPrice;

    public CouponItemlayout(Context context) {
        super(context);
        initView(context);
    }

    public CouponItemlayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CouponItemlayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public CouponItemlayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setOrientation(VERTICAL);
        inflater.inflate(R.layout.item_activity_coupon, this, true);
//        couponBgView = (CouponBgView) findViewById(R.id.view_couponview);
        tvTtile = (TextView) findViewById(R.id.tv_coupon_title);
        tvDate = (TextView) findViewById(R.id.tv_coupon_date);
        tvPrice = (TextView) findViewById(R.id.tv_coupon_price);

    }

    public void setData(String textTitle, String textTime, String couponCount) {
        tvTtile.setText(textTitle);
        tvDate.setText(textTime);
        postInvalidate();
    }
}
