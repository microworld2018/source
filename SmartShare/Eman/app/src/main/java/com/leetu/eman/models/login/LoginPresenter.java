//package com.leetu.eman.models.login;
//
//import android.content.Context;
//
//import com.leetu.eman.application.LeTravelApplication;
//import com.leetu.eman.models.login.bean.UserBean;
//import com.leetu.eman.net.HttpEngine;
//import com.leetu.eman.net.NetworkHelper;
//import com.leetu.eman.net.ResponseStatus;
//import com.leetu.eman.urls.URLS;
//import com.leetu.eman.utils.JsonParser;
//
///**
// * Created by jyt on 2016/9/18.
// */
//
//public class LoginPresenter implements LoginContract.UserAction {
//    private Context context;
//    private LoginContract.View loadingListener;
//
//    public LoginPresenter(Context context, LoginContract.View loadingListener) {
//        this.context = context;
//        this.loadingListener = loadingListener;
//    }
//
//    @Override
//    public void load(String phoneNumber, String code) {
//        if (NetworkHelper.isNetworkConnect(context)) {
//            HttpEngine.post().url(URLS.LOAD)
//                    .addParam("phoneNo", phoneNumber)
//                    .addParam("smsCode", code)
//                    .execute(new HttpEngine.ResponseCallback() {
//                        @Override
//                        public void onResponse(ResponseStatus response) {
//                            if (response.getResultCode()==200) {
//                                UserBean userBean = JsonParser.getParsedData(response.getData(), UserBean.class);
//                                LeTravelApplication.getSharePreferenceInstance(context).saveToken(userBean.getToken());
//                                LeTravelApplication.getSharePreferenceInstance(context).saveUserNumber(userBean.getPhoneNoStr());
//                                loadingListener.showLoad();
//                            } else {
//                                loadingListener.showFail(response.getResultMsg());
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Exception error) {
//                            loadingListener.timeOutFail();
//                        }
//                    });
//        } else {
//            loadingListener.contentFail();
//        }
//    }
//
//    @Override
//    public void sendCode(String phoneNumber) {
//        if (NetworkHelper.isNetworkConnect(context)) {
//            HttpEngine.post().url(URLS.CODE)
//                    .addParam("phoneNo", phoneNumber)
//                    .addParam("type", "login")
//                    .execute(new HttpEngine.ResponseCallback() {
//                        @Override
//                        public void onResponse(ResponseStatus response) {
//                            if (response.getResultCode()==200) {
//                                loadingListener.showCode();
//                            } else {
//                                loadingListener.showFail(response.getResultMsg());
//                            }
//                        }
//
//                        @Override
//                        public void onFailure(Exception error) {
//                            loadingListener.timeOutFail();
//                        }
//                    });
//        } else {
//            loadingListener.contentFail();
//        }
//    }
//}
