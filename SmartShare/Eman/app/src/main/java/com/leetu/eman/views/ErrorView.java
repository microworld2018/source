package com.leetu.eman.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.leetu.eman.R;


/**
 * Created by neo on 16/3/11.
 */
public class ErrorView extends LinearLayout {

    private View mEmptyView;
    private TextView mTitleTv;
    private TextView mSubTitleTv;
    private ImageView mErrorImageView;
    //    private ProgressBar mProgressBar;
    private ImageView ivGif;
    private LinearLayout ltLoading;
    private Context context;

    private RetryListener mListener;


    public ErrorView(Context context) {
        super(context);
        this.context = context;
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ErrorView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        init(attrs);

    }

    private void init(AttributeSet attributeSet) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.error_view_layout, this, true);

        mEmptyView = findViewById(R.id.error_and_empty_layout);
        mTitleTv = (TextView) findViewById(R.id.error_title);
        mSubTitleTv = (TextView) findViewById(R.id.error_subtitle);
        mErrorImageView = (ImageView) findViewById(R.id.error_image);
//        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        ivGif = (ImageView) findViewById(R.id.tv_laoding_gif);
        ltLoading = (LinearLayout) findViewById(R.id.lt_laoding);
        Glide.with(context)
                .load(R.mipmap.ic_loading_mid)
                .asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(ivGif);

        mEmptyView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onRetry();
                }
            }
        });
    }


    /**
     * Attaches a listener that to the view that reports retry events.
     *
     * @param listener {@link } to be notified when a retry
     *                 event occurs.
     */
    public void setOnRetryListener(RetryListener listener) {
        this.mListener = listener;
    }

    /**
     * Sets error subtitle to the description of the given HTTP status code
     *
     * @param errorCode HTTP status code
     */
    public void setError(int errorCode) {
        setSubtitle(errorCode + "");
    }

    /**
     * Sets error image to a given drawable resource
     *
     * @param res drawable resource.
     */
    public void setImageResource(int res) {
        mErrorImageView.setImageResource(res);
    }

    /**
     * Sets the error image to a given {@link Drawable}.
     *
     * @param drawable {@link Drawable} to use as error image.
     */
    public void setImageDrawable(Drawable drawable) {
        mErrorImageView.setImageDrawable(drawable);
    }

    /**
     * Sets the error image to a given {@link Bitmap}.
     *
     * @param bitmap {@link Bitmap} to use as error image.
     */
    public void setImageBitmap(Bitmap bitmap) {
        mErrorImageView.setImageBitmap(bitmap);
    }

    /**
     * Sets the error title to a given {@link String}.
     *
     * @param text {@link String} to use as error title.
     */
    public void setTitle(String text) {
        mTitleTv.setText(text);
    }

    /**
     * Sets the error title to a given string resource.
     *
     * @param res string resource to use as error title.
     */
    public void setTitle(int res) {
        mTitleTv.setText(res);
    }

    /**
     * Returns the current title string.
     */
    public String getTitle() {
        return mTitleTv.getText().toString();
    }

    /**
     * Sets the error title text to a given color.
     *
     * @param res color resource to use for error title text.
     */
    public void setTitleColor(int res) {
        mTitleTv.setTextColor(res);
    }

    /**
     * Returns the current title text color.
     */
    public int getTitleColor() {
        return mTitleTv.getCurrentTextColor();
    }

    /**
     * Sets the error subtitle to a given {@link String}.
     *
     * @param exception {@link String} to use as error subtitle.
     */
    public void setSubtitle(String exception) {
        mSubTitleTv.setText(exception);
    }

    /**
     * Sets the error subtitle to a given string resource.
     *
     * @param res string resource to use as error subtitle.
     */
    public void setSubtitle(int res) {
        mSubTitleTv.setText(res);
    }

    /**
     * Returns the current subtitle.
     */
    public String getSubtitle() {
        return mSubTitleTv.getText().toString();
    }

    /**
     * Sets the error subtitle text to a given color
     *
     * @param res color resource to use for error subtitle text.
     */
    public void setSubtitleColor(int res) {
        mSubTitleTv.setTextColor(res);
    }

    /**
     * Returns the current subtitle text color.
     */
    public int getSubtitleColor() {
        return mSubTitleTv.getCurrentTextColor();
    }


    /**
     * Shows or hides the error title
     */
    public void showTitle(boolean show) {
        mTitleTv.setVisibility(show ? VISIBLE : GONE);
    }

    /**
     * Indicates whether the title is currently visible.
     */
    public boolean isTitleVisible() {
        return mTitleTv.getVisibility() == VISIBLE;
    }

    /**
     * Shows or hides error subtitle.
     */
    public void showSubtitle(boolean show) {
        mSubTitleTv.setVisibility(show ? VISIBLE : GONE);
    }

    /**
     * Indicates whether the subtitle is currently visible.
     */
    public boolean isSubtitleVisible() {
        return mSubTitleTv.getVisibility() == VISIBLE;
    }

    public void showLoading() {
//        mProgressBar.setVisibility(View.VISIBLE);
        ltLoading.setVisibility(VISIBLE);
        mEmptyView.setVisibility(View.INVISIBLE);
    }


    public void showEmptyView() {
//        mProgressBar.setVisibility(View.INVISIBLE);
        ltLoading.setVisibility(INVISIBLE);
        mEmptyView.setVisibility(View.VISIBLE);
    }

    public interface RetryListener {
        public void onRetry();
    }
}
