package com.leetu.eman.base;/**
 * Created by jyt on 2016/9/26.
 */


import android.content.Context;
import android.util.Log;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.models.login.bean.UserBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;


/**
 * created by neo on 2016/9/26 10:34
 */
public class BasePresenter {

    public BasePresenter() {
    }

    protected <T extends BaseContract> boolean checkCode(int code, T baseContract) {
        boolean b = true;
        if (code == 200) {
            b = true;
        } else if (code == 206) {
            baseContract.tokenOld();
            b = false;
        } else if (code == 214) {//全锁
            baseContract.lockAll(0);
            b = false;
        } else if (code == 213) {//半锁
            baseContract.lockHalf(0);
            b = false;
        } else {
            b = false;
        }
        return b;
    }

    //判断是否登录
    protected <T extends BaseContract> boolean isLoad(Context context, T baseContract) {
        if (LeTravelApplication.getSharePreferenceInstance(context).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(context).getToken().equals("")) {
            return true;
        } else {
            baseContract.tokenOld();
            return false;
        }
    }

    //登录
    protected <T extends BaseContract> void login(final T baseContract, final Context context, String phoneNumber, String code) {

        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.LOAD)
                    .addParam("phoneNo", phoneNumber)
                    .addParam("smsCode", code)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            Log.e("gn", response.toString());
                            if (response.getResultCode() == 200) {
                                UserBean userBean = JsonParser.getParsedData(response.getData(), UserBean.class);
                                Log.e("gn", "登录回调" + userBean.toString());
                                LeTravelApplication.getSharePreferenceInstance(context).saveToken(userBean.getToken());
                                LeTravelApplication.getSharePreferenceInstance(context).saveUserNumber(userBean.getPhoneNoStr());
                                Log.e("gn", "登录回调" + LeTravelApplication.getSharePreferenceInstance(context).getToken());
                                baseContract.loginOk();
                            } else if (response.getResultCode() == 214) {
                                baseContract.lockAll(1);
                            } else {
                                baseContract.loginFail(response.getResultMsg());

                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            baseContract.timeOutFail();
                        }
                    });
        } else {
            baseContract.showFail(context.getResources().getString(R.string.net_error));
        }
    }

    public <T extends BaseContract> void sendCode(final T baseContract, final Context context, String phoneNumber) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.CODE)
                    .addParam("phoneNo", phoneNumber)
                    .addParam("type", "login")
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (response.getResultCode() == 200) {
                                baseContract.showCode();
                            } else {
                                baseContract.showFail(response.getResultMsg());
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
//                            baseContract.timeOutFail();
                        }
                    });
        } else {
            baseContract.showFail(context.getResources().getString(R.string.net_error));
        }
    }
}
