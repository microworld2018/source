package com.leetu.eman.models.settings;/**
 * Created by gn on 2016/10/11.
 */


import com.leetu.eman.base.BaseContract;

import java.util.List;

/**
 * created by neo on 2016/10/11 09:18
 */
public interface ComplaintContract {
    interface View extends BaseContract {
        void showupFileData();
    }

    interface UserAction {
        void upFileData(List<String> files, String feedbackDesc, String contactType);
    }
}
