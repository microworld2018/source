package com.leetu.eman.models.welcome;/**
 * Created by jyt on 2016/9/20.
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.activity.WebViewDetailActivity;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.AdLoadingTime;
import com.leetu.eman.views.CircleBar;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * created by neo on 2016/9/20 11:30
 * 启动页
 */
public class StartUpActivity extends BaseActivity implements CircleBar.ProgressListener, View.OnClickListener, AdLoadingTime.ProgressListener {
    private ImageView ivStartUp;
    private AdLoadingTime adTime;
    private Handler handler = new Handler();
    private File file;
    private boolean adIseffective = false;
    private long adSpTime;
    private String adDetailUrl, adImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置无标题
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //设置全屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_start_up);
        initView();
        loadData();
    }

    protected void initView() {
        ivStartUp = (ImageView) findViewById(R.id.iv_startup);
        adTime = (AdLoadingTime) findViewById(R.id.tv_adtime);

        adTime.setProgressListener(this);
        adTime.setOnClickListener(this);

    }


    protected void loadData() {
        Glide.with(this)
                .load(R.mipmap.ic_start_up)
                .into(ivStartUp);

        //获取当前时间
        long currentTime = Long.parseLong(getNowTime());
        //获取广告有效期
        String adTime = LeTravelApplication.getSharePreferenceInstance(this).getAdTime();
        adDetailUrl = LeTravelApplication.getSharePreferenceInstance(this).getAdDetailUrl();
        adImageUrl = LeTravelApplication.getSharePreferenceInstance(this).getAdImgUrl();

        //获取本地缓存图片
        final String filePath = getExternalCacheDir().getPath() + "/eman/emanAd.jpg";
        file = new File(filePath);
        if (!TextUtils.isEmpty(adTime)) {
            adSpTime = Long.parseLong(formatTime(adTime));
            //判断广告有效期
            if (currentTime > adSpTime) {
                //无效
                adIseffective = false;
            } else {
                //有效
                adIseffective = true;
            }
        } else {
            if (file.exists() && !TextUtils.isEmpty(adImageUrl)) {
                adIseffective = true;
            }
        }


        handler.postDelayed(task, 2000);
    }

    Runnable task = new Runnable() {
        public void run() {
            //如果有效同时文件照片存在
            if (file != null) {
                if (adIseffective && file.exists()) {
                    Glide.with(StartUpActivity.this)
                            .load(Uri.fromFile(file))
                            .placeholder(R.mipmap.ic_start_up)
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .listener(new RequestListener<Uri, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, Uri uri, Target<GlideDrawable> target, boolean b) {
                                    StartUpActivity.this.adTime.stop();
                                    LeTravelApplication.getSharePreferenceInstance(StartUpActivity.this).saveAdTime("");
                                    LeTravelApplication.getSharePreferenceInstance(StartUpActivity.this).saveAdDetailUrl("");
                                    LeTravelApplication.getSharePreferenceInstance(StartUpActivity.this).saveAdImgUrl("");
                                    startIntent(MainActivity.class);
                                    LogUtils.e("lv", "Glide加载失败:" + e.getMessage());
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable glideDrawable, Uri uri, Target<GlideDrawable> target, boolean b, boolean b1) {
                                    if (!TextUtils.isEmpty(adDetailUrl)) {
                                        ivStartUp.setOnClickListener(StartUpActivity.this);
                                    }

                                    StartUpActivity.this.adTime.setVisibility(View.VISIBLE);
                                    StartUpActivity.this.adTime.start();
                                    return false;
                                }
                            })
                            .into(ivStartUp);
                } else {
                    startIntent(MainActivity.class);
                }
            } else {
                startIntent(MainActivity.class);
            }

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_adtime:
                adTime.stop();
                startIntent(MainActivity.class);
                break;

            case R.id.iv_startup:
                //跳转至广告详情页
                adTime.stop();
                startIntent(WebViewDetailActivity.class);
                break;
        }
    }

    //倒计时结束监听
    @Override
    public void endProgress() {
        startIntent(MainActivity.class);
    }

    private void startIntent(final Class clas) {
        Intent intent = new Intent(StartUpActivity.this, clas);
        intent.putExtra(WebViewDetailActivity.DETAIL_URL, adDetailUrl);
        intent.putExtra(WebViewDetailActivity.RETURN_MAINACTIVITY, true);
        startActivity(intent);
        StartUpActivity.this.finish();
    }

    /**
     * 时间戳
     *
     * @param
     * @return
     */
    public static String getNowTime() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        return formatter.format(date);
    }

    @Override
    public void setstatusBar() {
    }

    /**
     * 时间转换yyyy-MM-dd hh:mm:ss------yyyyMMddHHmmss
     *
     * @param
     * @return
     */
    public static String formatTime(String time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat oldTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date data = null;
        try {
            data = oldTime.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String format = formatter.format(data);
        return format;
    }

    @Override
    protected void onDestroy() {
        adTime.stop();
        handler.removeCallbacks(task);
        super.onDestroy();
    }

}
