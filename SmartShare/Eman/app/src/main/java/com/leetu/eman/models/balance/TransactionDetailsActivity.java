package com.leetu.eman.models.balance;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.gn.myanimpulltorefreshlistview.pulltoimpl.PullToRefreshBase;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.balance.adapters.TransactionDtailsAdapter;
import com.leetu.eman.models.balance.beans.BalanceBean;
import com.leetu.eman.models.balance.beans.RechargeRecordBean;
import com.leetu.eman.net.HttpEngine;
import com.gn.myanimpulltorefreshlistview.views.PullToRefreshListView;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/25.
 * 余额明细
 */

public class TransactionDetailsActivity extends BaseActivity implements TransactionDetailsContract.View, View.OnClickListener {
    public static final String BANLANCE_DETAILS = "banlance_details";
    private TitleBar titleBar;
    private PullToRefreshListView pullToRefreshListView;
    private TransactionDetailsPresenter transactionDetailsPresenter;
    private ListView mListView;
    private BalanceBean balanceBean;
    private List<RechargeRecordBean> charges;
    private TransactionDtailsAdapter transactionDtailsAdapter;
    private boolean isFrist = true;
    private LinearLayout listEmptyView;
    private TextView tvNoneRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banlance_details);
        initView();
        loadData();
    }

    protected void initView() {

        titleBar = (TitleBar) findViewById(R.id.title_banlance_details);
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.listview_rechange_history);
        listEmptyView = (LinearLayout) findViewById(R.id.list_emptyview);
        tvNoneRecord = (TextView) findViewById(R.id.none_record);

        mListView = pullToRefreshListView.getRefreshableView();
        mListView.setDivider(getResources().getDrawable(R.color.gray));
        mListView.setDividerHeight(1);
        mListView.setEmptyView(listEmptyView);

        transactionDetailsPresenter = new TransactionDetailsPresenter(this, this);

        titleBar.setTitle("余额明细");
        tvNoneRecord.setText("暂无充值记录");
        titleBar.setLeftClickListener(this);

        charges = new ArrayList<>();

        transactionDtailsAdapter = new TransactionDtailsAdapter(this, charges);
        mListView.setAdapter(transactionDtailsAdapter);

        pullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                transactionDetailsPresenter.loadData(false, false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                transactionDetailsPresenter.loadData(true, false);
            }
        });
    }


    protected void loadData() {
        showLoading();
        transactionDetailsPresenter.loadData(false, isFrist);
    }

    @Override
    public void showLoadData(boolean isUp, List<RechargeRecordBean> getRechargeCards) {
        showProgressBar(false);
        showContent();
        isFrist = false;
        if (getRechargeCards != null) {
            if (isUp) {
                charges.addAll(getRechargeCards);
            } else {
                charges.clear();
                charges.addAll(getRechargeCards);
            }
            pullToRefreshListView.onRefreshComplete();
            transactionDtailsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                TransactionDetailsActivity.this.finish();
                break;
        }
    }


    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        transactionDetailsPresenter.loadData(false, true);
    }

    @Override
    public void showFail(String message) {
        super.showFail(message);
        pullToRefreshListView.onRefreshComplete();
    }

    @Override
    protected void onRetryLoadData() {
        showProgressBar(true);
        transactionDetailsPresenter.loadData(false, isFrist);
        super.onRetryLoadData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(TransactionDetailsActivity.class);
    }
}
