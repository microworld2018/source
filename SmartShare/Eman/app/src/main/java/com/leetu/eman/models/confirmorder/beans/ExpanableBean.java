package com.leetu.eman.models.confirmorder.beans;

import java.util.List;

/**
 * Created by lvjunfeng on 2017/5/4.
 */

public class ExpanableBean {
    /**
     * tipsList : [{"tip":"本保险合同为不定值保险合同"},{"tip":"地震及其次生灾害，不予赔偿"},{"tip":"战争、军事冲突、恐怖活动、暴乱、扣押、罚没、查封、政府征用，不予赔偿"},{"tip":"核反应、核污染、核辐射，不予赔偿"}]
     * title : 基础服务
     * insuranceId : 1
     * insuranceDesc : 11111
     * isUsed : 1
     * subtitle : 50.00元/天（1天=24小时）
     */

    private String title;
    private String insuranceId;
    private String insuranceDesc;
    private String isUsed;
    private String subtitle;
    private List<TipsListBean> tipsList;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(String insuranceId) {
        this.insuranceId = insuranceId;
    }

    public String getInsuranceDesc() {
        return insuranceDesc;
    }

    public void setInsuranceDesc(String insuranceDesc) {
        this.insuranceDesc = insuranceDesc;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public List<TipsListBean> getTipsList() {
        return tipsList;
    }

    public void setTipsList(List<TipsListBean> tipsList) {
        this.tipsList = tipsList;
    }

    public static class TipsListBean {
        /**
         * tip : 本保险合同为不定值保险合同
         */

        private String tip;

        public String getTip() {
            return tip;
        }

        public void setTip(String tip) {
            this.tip = tip;
        }
    }
}
