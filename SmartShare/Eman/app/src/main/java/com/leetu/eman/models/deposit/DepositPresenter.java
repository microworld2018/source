package com.leetu.eman.models.deposit;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.deposit.bean.MyDepositBean;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

import static com.umeng.socialize.utils.DeviceConfig.context;

/**
 * Created by lvjunfeng on 2017/4/5.
 */

public class DepositPresenter extends BasePresenter implements DepositContract.UserAction {

    private final DepositContract.View depositListener;
    private final Context mContext;
    private int page = 1;
    public DepositPresenter(Context context, DepositContract.View depositListener) {
        this.mContext = context;
        this.depositListener = depositListener;
    }

    @Override
    public void load(final boolean isUp, final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(mContext)) {
            if (isLoad(mContext, depositListener)) {
                if (!isUp) {
                    page = 1;
                }
                HttpEngine.post().url(URLS.MyDeposit)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(mContext).getToken())
                        .addParam("currentPage", "" + page)
                        .tag(DepositActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), depositListener)) {
                                    LogUtils.e("lv", "我的押金：" + response.getData());
                                    MyDepositBean depositBean = JsonParser.getParsedData(response.getData(), MyDepositBean.class);
                                    if (depositBean != null) {
                                        if (depositBean.getRecordList() != null) {
                                            if (depositBean.getRecordList().size() != 0) {
                                                page++;
                                            }
                                            depositListener.showLoad(isUp, depositBean);
                                        }
                                    }

                                } else {
                                    if (response.getResultCode() != 206) {
                                        depositListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }

                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                depositListener.timeOutFail();
                            }
                        });
            }
        } else {
            if (isFrist) {
                depositListener.contentFail();
            } else {
                depositListener.showFail(mContext.getResources().getString(R.string.net_error));
            }
        }
    }

    @Override
    public void checkUserRNA() {
        if (NetworkHelper.isNetworkConnect(mContext)) {
            if (LeTravelApplication.getSharePreferenceInstance(mContext).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(context).getToken().equals("")) {
                HttpEngine.post().url(URLS.USER_INFO)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(mContext).getToken())
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                LogUtils.e("lv", "押金充值获取个人信息" + response.getData());
                                if (checkCode(response.getResultCode(), depositListener)) {
                                    UserInfoBean userInfoBean = JsonParser.getParsedData(response.getData(), UserInfoBean.class);
                                    if (userInfoBean != null) {
                                        LeTravelApplication.getSharePreferenceInstance(mContext).saveUserStatus(userInfoBean.getState(),userInfoBean.getHandHeldIdCard());
                                        depositListener.showCheckUserRNA(userInfoBean);
                                    }
                                } else {
                                    if (response.getResultCode() != 206) {
                                        depositListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                depositListener.timeOutFail();
                            }
                        });
            }
        } else {
            depositListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }

    @Override
    public void retrunDeposit() {
        if (NetworkHelper.isNetworkConnect(mContext)) {
            if (LeTravelApplication.getSharePreferenceInstance(mContext).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(context).getToken().equals("")) {
                HttpEngine.post().url(URLS.RETURN_DEPOSIT)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(mContext).getToken())
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), depositListener)) {
                                    LogUtils.e("lv", "押金退款提交成功" + response.getData());
                                    depositListener.showReturnDeposit();
                                } else {
                                    if (response.getResultCode() != 206) {
                                        if(response.getResultCode() == 274) {//未完成订单
                                            depositListener.showUnCompleteOrder();
                                        }else {
                                            depositListener.showFail(response.getResultCode(), response.getResultMsg());
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                depositListener.timeOutFail();
                            }
                        });
            }
        } else {
            depositListener.showFail(mContext.getResources().getString(R.string.net_error));
        }
    }
}
