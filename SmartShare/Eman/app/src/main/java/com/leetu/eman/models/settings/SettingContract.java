package com.leetu.eman.models.settings;

import com.leetu.eman.base.BaseContract;

/**
 * Created by lvjunfeng on 2017/6/20.
 */

public interface SettingContract {

    interface View extends BaseContract{
        void showExit();
    }
    interface UserAction{

        void userExit();
    }
}
