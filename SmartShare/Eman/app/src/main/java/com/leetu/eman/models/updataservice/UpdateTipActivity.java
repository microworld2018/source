package com.leetu.eman.models.updataservice;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;


public class UpdateTipActivity extends BaseActivity {

    UpdateService.UpdateInfo mUpdateInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_tip);

        mUpdateInfo = (UpdateService.UpdateInfo) getIntent().getSerializableExtra("data");
        showNoticeDialog();

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK
                && event.getAction() == KeyEvent.ACTION_DOWN) {
            startUpdateService(UpdateService.PARAM_STOP_SELF);
            finish();
            if (mUpdateInfo != null && mUpdateInfo.required == 1) {
                LeTravelApplication.getInstance().finishAllActivity();
                return true;
            }
        }


        return super.onKeyDown(keyCode, event);
    }


    private void showNoticeDialog() {
        if (Build.VERSION.SDK_INT >= 11) {
            setFinishOnTouchOutside(false);
        }
        setTitle("软件版本更新");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        TextView message = (TextView) findViewById(R.id.message);
        message.setText(mUpdateInfo.newMessage);

        TextView textView = (TextView) findViewById(R.id.download);
        if (isDownload()) {
            textView.setText("安装");
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    startUpdateService(UpdateService.PARAM_INSTALL_APK);
                    finish();
                }
            });
        } else {
            builder.setPositiveButton("", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            textView.setText("下载");
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startUpdateService(UpdateService.PARAM_START_DOWNLOAD);
                    finish();
                }
            });
        }

        if (mUpdateInfo.required == 0) {
            TextView buttonCannel = (TextView) findViewById(R.id.cancel);
            buttonCannel.setVisibility(View.VISIBLE);
            buttonCannel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startUpdateService(UpdateService.PARAM_STOP_SELF);
                    finish();
                }
            });
        }
    }

    private void startUpdateService(int request) {
        Intent intent = new Intent(this, UpdateService.class);
        intent.putExtra("data", request);
        startService(intent);
    }

    private boolean isDownload() {
        return mUpdateInfo.apkFile().exists();
    }

}