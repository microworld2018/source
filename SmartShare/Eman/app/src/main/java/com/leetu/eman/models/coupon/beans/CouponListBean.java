package com.leetu.eman.models.coupon.beans;

import java.util.List;

/**
 * Created by Administrator on 2016/12/23.
 */

public class CouponListBean {
    List<CouponBean> coupon;

    public List<CouponBean> getCoupon() {
        return coupon;
    }

    public void setCoupon(List<CouponBean> coupon) {
        this.coupon = coupon;
    }
}
