package com.leetu.eman.models.login;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.usecar.MainActivity;


/**
 * 登录界面
 */
public class LoginActivity extends BaseActivity implements LoginContract.View, View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    protected void initView() {
        showLogin();
        titleBar.setTitle("登录");
        titleBar.hideLeftLayout();
        titleBar.setRightText("跳过");
        titleBar.setmRightTextColor("#ffffff");
        titleBar.setRightClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_right:
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void showLoad() {

    }

    @Override
    public void loginOk() {
        showProgressBar(false);
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        LoginActivity.this.finish();
    }
}
