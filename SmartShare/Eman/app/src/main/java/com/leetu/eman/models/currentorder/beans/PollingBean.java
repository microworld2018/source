package com.leetu.eman.models.currentorder.beans;

/**
 * Created by lvjunfeng on 2017/3/17.
 */

public class PollingBean {
    String mileage;
    String alltime;
    double allPrice;

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getAlltime() {
        return alltime;
    }

    public void setAlltime(String alltime) {
        this.alltime = alltime;
    }

    public double getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(double allPrice) {
        this.allPrice = allPrice;
    }
}
