//package com.leetu.eman.logindialog;/**
// * Created by jyt on 2016/9/23.
// */
//
//import android.app.Dialog;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Handler;
//import android.util.DisplayMetrics;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.Window;
//import android.view.WindowManager;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.leetu.eman.R;
//import com.leetu.eman.models.login.bean.UserBean;
//import com.leetu.eman.models.updataservice.CheckUserOrderService;
//import com.leetu.eman.utils.LogUtils;
//import com.leetu.eman.views.TitleBar;
//
//
///**
// * created by neo on 2016/9/23 16:17
// * 登录失效
// */
//public class LoginDialog extends Dialog implements View.OnClickListener, LoginDialogContract.View {
//
//    private final Context context;
//    private EditText etNumber, etCode;
//    private TitleBar titleBar;
//    private Button btStartUse;
//    private TextView tvGetCode;
//    private LoginDialogPresenter loadingPresenter;
//    private Handler handler;
//    private int time = 60;
//    private ProgressDialog progressDialog;
//
//    public LoginDialog(Context context) {
//        super(context, R.style.selectorDialog);
//        this.context = context;
//
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        initView();
//    }
//
//    private void initView() {
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View view = inflater.inflate(R.layout.activity_login, null);
//        setContentView(view);
//
//
//        titleBar = (TitleBar) findViewById(R.id.title_loading);
//        etNumber = (EditText) findViewById(R.id.et_load_phonenumber);
//        etCode = (EditText) findViewById(R.id.et_load_code);
//        btStartUse = (Button) findViewById(R.id.bt_start_use);
//        tvGetCode = (TextView) findViewById(R.id.tv_load_getcode);
//
//        loadingPresenter = new LoginDialogPresenter(context, this);
//        handler = new Handler();
//        progressDialog = new ProgressDialog(context);
//
//        //设置按钮监听
//        btStartUse.setOnClickListener(this);
//        tvGetCode.setOnClickListener(this);
//
//        Window dialogWindow = getWindow();
//
//        dialogWindow.setWindowAnimations(R.style.mystyle);  //添加动画
//        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
//        DisplayMetrics d = context.getResources().getDisplayMetrics();
//        lp.width = (int) (d.widthPixels * 1);
//        lp.height = (int) (d.heightPixels * 1);
//        dialogWindow.setAttributes(lp);
//    }
//
//    /**
//     * 检测手机号码
//     *
//     * @return
//     */
//    boolean checkPhoneNumber(int type) {
//        boolean b = true;
//        switch (type) {
//            case 1:
//                if (etNumber.getText().toString().trim().equals("")) {
//                    Toast.makeText(context, "请输入电话号码", Toast.LENGTH_SHORT).show();
//                    b = false;
//                }
//                break;
//            case 2:
//                if (etNumber.getText().toString().trim().equals("")) {
//                    Toast.makeText(context, "请输入电话号码", Toast.LENGTH_SHORT).show();
//                    b = false;
//                } else {
//                    if (etCode.getText().toString().trim().equals("")) {
//                        Toast.makeText(context, "请输入验证码", Toast.LENGTH_SHORT).show();
//                        b = false;
//                    }
//                }
//                break;
//        }
//
//        return b;
//    }
//
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.bt_start_use:
//                if (checkPhoneNumber(2)) {
//                    progressDialog.show();
//                    loadingPresenter.dialogLoad(etNumber.getText().toString().trim(), etCode.getText().toString().trim());
//                }
//                break;
//            case R.id.tv_load_getcode:
//                if (checkPhoneNumber(1)) {
//                    progressDialog.show();
//                    loadingPresenter.dialogSendCode(etNumber.getText().toString().trim());
//                }
//                break;
//        }
//
//    }
//
//
//    Runnable runnable = new Runnable() {
//        @Override
//        public void run() {
//            time--;
//            tvGetCode.setText("" + time);
//            if (time == 0) {
//                tvGetCode.setClickable(true);
//                tvGetCode.setText("重新获取");
//                handler.removeCallbacks(this);
//            } else {
//                handler.postDelayed(this, 1000);
//            }
//        }
//    };
//
//    @Override
//    public void showDialogLoad(UserBean userBean) {
//        progressDialog.dismiss();
//
//        LogUtils.e("gn", "再次启动检查");
//        Intent intent = new Intent(context, CheckUserOrderService.class);//登录成功再次去查看用户订单
//        context.startService(intent);
//        LoginDialog.this.cancel();
//    }
//
//    @Override
//    public void showDialogSendCode() {
//        progressDialog.dismiss();
//        time = 60;
//        handler.postDelayed(runnable, 1000);
//        tvGetCode.setClickable(false);
//    }
//
//    @Override
//    public void contentFail() {
//        progressDialog.dismiss();
//        Toast.makeText(context, R.string.net_error, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void dataFail() {
//
//    }
//
//    @Override
//    public void showFail(String message) {
//        progressDialog.dismiss();
//        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void timeOutFail() {
//        progressDialog.dismiss();
//        Toast.makeText(context, R.string.time_out, Toast.LENGTH_SHORT).show();
//    }
//
//    @Override
//    public void showNetError(String msg) {
//
//    }
//
//    @Override
//    public void tokenOld() {
//
//    }
//
//    @Override
//    public void showCode() {
//
//    }
//
//    @Override
//    public void loginOk() {
//
//    }
//
//    @Override
//    public void loginFail(String msg) {
//
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//        progressDialog.dismiss();
//    }
//}
//
