package com.leetu.eman.models.deposit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.views.TitleBar;


/**
 * Created by Administrator on 2016/11/25.
 */

public class DepositReChargeResaultActivity extends BaseActivity implements View.OnClickListener {
    private ImageView ivResault;
    private TitleBar rechargeTitle;
    private LinearLayout rechargeSuccess;
    private TextView resultMes;
    private Button btnEquiry, btnContinueRecharges, btnContinueRechargef;
    public static final String DEPOSIT_RESAULT = "result";
    private boolean resault;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit_recharge_resault);
        initView();
    }

    protected void initView() {
        intent = getIntent();
        if (intent != null) {
            resault = intent.getBooleanExtra(DEPOSIT_RESAULT, false);
        }

        rechargeTitle = (TitleBar) findViewById(R.id.deposit_result_title);
        rechargeSuccess = (LinearLayout) findViewById(R.id.deposit_recharge_success);
        ivResault = (ImageView) findViewById(R.id.iv_recharge_resault);
        resultMes = (TextView) findViewById(R.id.result_mes);
        btnEquiry = (Button) findViewById(R.id.btn_deposit_equiry);
        btnContinueRecharges = (Button) findViewById(R.id.btn_usecar);
        btnContinueRechargef = (Button) findViewById(R.id.btn_continue_rechargedeposit);

        if (resault) {
            rechargeTitle.setTitle("押金充值成功");
            resultMes.setText("恭喜您，押金充值成功");
            rechargeSuccess.setVisibility(View.VISIBLE);
            btnContinueRechargef.setVisibility(View.GONE);

            Glide.with(DepositReChargeResaultActivity.this)
                    .load(R.mipmap.iv_recharge_successful)
                    .into(ivResault);
        } else {
            rechargeTitle.setTitle("押金充值失败");
            resultMes.setText("对不起，押金充值失败");
            rechargeSuccess.setVisibility(View.GONE);
            btnContinueRechargef.setVisibility(View.VISIBLE);

            Glide.with(DepositReChargeResaultActivity.this)
                    .load(R.mipmap.iv_recharge_fail)
                    .into(ivResault);
        }

        btnEquiry.setOnClickListener(this);
        rechargeTitle.setLeftClickListener(this);
        btnContinueRecharges.setOnClickListener(this);
        btnContinueRechargef.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                if (resault) {
                    setResult(2, intent);
                } else {
                    setResult(1, intent);
                }
                DepositReChargeResaultActivity.this.finish();
                break;
            case R.id.btn_deposit_equiry:
                setResult(2, intent);
                DepositReChargeResaultActivity.this.finish();
                break;
            case R.id.btn_continue_rechargedeposit:
                setResult(1, intent);
                DepositReChargeResaultActivity.this.finish();
                break;
            case R.id.btn_usecar:
                LeTravelApplication.getInstance().finishAllExcept(MainActivity.class);
                break;
        }
    }
}
