package com.leetu.eman.models.currentorder;/**
 * Created by gn on 2016/10/9.
 */

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBean;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.models.takecar.beans.TakeCarBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * created by neo on 2016/10/9 14:23
 */
public class CurrentOrderPresenter extends BasePresenter implements CurrentOrderContract.UserAction {
    private Context context;
    private CurrentOrderContract.View currentOrderListener;

    public CurrentOrderPresenter(Context context, CurrentOrderContract.View currentOrderListener) {
        this.context = context;
        this.currentOrderListener = currentOrderListener;
    }

    @Override
    public void loadData() {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.CHECK_ORDER)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), currentOrderListener)) {
                                LogUtils.e("gn", "检查用户订单" + response.getData());
                                try {
                                    JSONObject jsonObject = new JSONObject(response.getData());
                                    int orderType = jsonObject.getInt("orderType");
                                    if (orderType == 0) {
                                        TakeCarBean checkOrderBean = JsonParser.getParsedData(response.getData(), TakeCarBean.class);
                                    } else if (orderType == 1) {
                                        CurrentOrderBean currentOrderBean = JsonParser.getParsedData(response.getData(), CurrentOrderBean.class);
                                        LogUtils.e("gn", "lalala" + currentOrderBean.getAlltime());
                                        currentOrderListener.showLoadData(currentOrderBean);
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                if (response.getResultCode() != 206) {
                                    currentOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            currentOrderListener.timeOutFail();
                        }
                    });
        } else {
            currentOrderListener.contentFail();
        }
    }

    @Override
    public void openOrCloseDoor(String orderId, final String state, String carId, String lat, String lng) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.OPEN_OR_CLOSE_DOOR)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("state", state)
                    .addParam("carId", carId)
                    .addParam("lat", lat)
                    .addParam("lng", lng)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), currentOrderListener)) {
                                LogUtils.e("gn", "当前订单开关车门" + state + response.getData());
                                currentOrderListener.showopenOrCloseDoor();
                            } else {
                                if (response.getResultCode() != 206) {
                                    currentOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            currentOrderListener.timeOutFail();
                        }
                    });
        } else {
            currentOrderListener.showNetError("");
        }
    }

    @Override
    public void upDataReturnDot(String dotId, String orderId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.UPDATA_RETURN_CAR_ADDRESS)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("dotId", dotId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), currentOrderListener)) {
                                LogUtils.e("gn", "更新还车网店" + response.getData());
                                currentOrderListener.showUpDataReturnDot();
                            } else {
                                if (response.getResultCode() != 206) {
                                    currentOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            currentOrderListener.timeOutFail();
                        }
                    });
        } else {
            currentOrderListener.showNetError("");
        }
    }

    @Override
    public void defaultReturnDot(String carId, String orderId) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.RESET_DEFAULT_RETURN_CAR_ADDRESS)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("carId", carId)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), currentOrderListener)) {
                                LogUtils.e("gn", "恢复默认还车网店" + response.getData());
                                ReturnCarPointBean returnCarPointBean = JsonParser.getParsedData(response.getData(), ReturnCarPointBean.class);
                                currentOrderListener.showDefaultReturnDot(returnCarPointBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    currentOrderListener.showFail(response.getResultCode(), response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            currentOrderListener.timeOutFail();
                        }
                    });
        } else {
            currentOrderListener.showNetError("");
        }
    }

}
