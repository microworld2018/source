package com.leetu.eman.utils;

import android.app.ActivityManager;
import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;

import java.util.List;

/**
 * 获取当前进程的名字
 * Created by lvjunfeng on 2017/6/19.
 */

public class AppUtils {
    public static String getProcessName(int pid){
        ActivityManager systemService = (ActivityManager) LeTravelApplication.getInstance().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = systemService.getRunningAppProcesses();
        if(runningAppProcesses == null) {
            return null;
        }

        for (ActivityManager.RunningAppProcessInfo processInfo : runningAppProcesses){
            if(processInfo.pid == pid) {
                return processInfo.processName;
            }
        }

        return null;

    }
}
