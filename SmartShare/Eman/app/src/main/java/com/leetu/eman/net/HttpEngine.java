package com.leetu.eman.net;

import com.zhy.http.okhttp.OkHttpUtils;

import java.io.File;

import okhttp3.Call;

/**
 * Created by kevin on 16/3/10.
 */
public class HttpEngine {


    /**
     * 创建上传文件的请求
     *
     * @return
     */
    public static PostFileRequest postFile() {
        return new PostFileRequest();
    }

    /**
     * 创建post请求
     *
     * @return
     */
    public static PostRequest post() {
        return new PostRequest();
    }

    /**
     * 创建get请求
     *
     * @return
     */
    public static GetFileRequest getFile() {
        return new GetFileRequest();
    }

    /**
     * 取消网络请求
     *
     * @param tag
     */
    public static void cancelRequest(Object tag) {
        OkHttpUtils.getInstance().cancelTag(tag);
    }


    public static interface ResponseCallback {
        public void onResponse(ResponseStatus response);

        public void onFailure(Exception error);
    }

    public static interface FileCallBack {
        public void onError(Call call, Exception e);

        public void onResponse(File response);

        public void inProgress(float progress, long total);
    }
}
