package com.leetu.eman.utils;


import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class CryptoTools {

    public static String key = "HeGeA8G3";
    public static String iv = "6LA2EyQm";


    /**
     * 加密
     *
     * @param data
     * @return
     * @throws Exception
     */
    public static String encode(String data) throws Exception {
        Cipher enCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");// 得到加密对象Cipher
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");// 获得密钥工厂
        DESKeySpec keySpec = new DESKeySpec(key.getBytes());// 设置密钥参数
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
        enCipher.init(Cipher.ENCRYPT_MODE, keyFactory.generateSecret(keySpec), ivParameterSpec);// 设置工作模式为加密模式，给出密钥和向量
        byte[] pasByte = enCipher.doFinal(data.getBytes("utf-8"));
        Base64Tool base64Encoder = new Base64Tool();
        return base64Encoder.encode(pasByte);
    }

    /**
     * 解密
     *
     * @param data
     * @return
     * @throws Exception
     */
    public static String decode(String data) throws Exception {
        Cipher deCipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");// 获得密钥工厂
        DESKeySpec keySpec = new DESKeySpec(key.getBytes());// 设置密钥参数
        IvParameterSpec ivParameterSpec = new IvParameterSpec(iv.getBytes());
        deCipher.init(Cipher.DECRYPT_MODE, keyFactory.generateSecret(keySpec), ivParameterSpec);
        Base64Tool base64Decoder = new Base64Tool();
        byte[] pasByte = deCipher.doFinal(base64Decoder.decode(data));

        return new String(pasByte, "UTF-8");
    }


}
