package com.leetu.eman.models.settings;


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.settings.beans.InvoiceShowBean;

/**
 * Created by Larry.Shi on 2016/10/20.
 */

public interface InvoiceContract {
    interface View extends BaseContract {
        void showInvoice(InvoiceShowBean invoiceshowBean);

        void showUpOk();

    }

    interface UserAction {
        void getInvoice(boolean isFrist);

        void invoiceBill(String totalFee, String title, String type, String recipients, String telphone, String address, String postcode);
    }
}
