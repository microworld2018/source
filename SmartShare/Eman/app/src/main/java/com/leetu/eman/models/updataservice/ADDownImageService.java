package com.leetu.eman.models.updataservice;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.utils.LogUtils;

import java.io.File;

import okhttp3.Call;

/**
 * Created by lvjunfeng on 2017/6/21.
 */

public class ADDownImageService extends IntentService {
    private static final String ACTION_UPLOAD_IMG = "DOWNLOAD_IMAGE";
    public static final String EXTRA_IMG_PATH = "IMG_PATH";
    public static final String IMG_URL = "IMG_URL";
    public static final String CON_URL = "CON_URL";
    public static final String END_TIME = "END_TIME";

    public static void startDownLoad(Context context, String filePath, String imgUrl, String endTime, String contentUrl) {
        endTime = endTime;
        Intent intent = new Intent(context, ADDownImageService.class);
        intent.setAction(ACTION_UPLOAD_IMG);
        intent.putExtra(EXTRA_IMG_PATH, filePath);
        intent.putExtra(IMG_URL, imgUrl);
        intent.putExtra(CON_URL, contentUrl);
        intent.putExtra(END_TIME, endTime);
        context.startService(intent);
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     * <p>
     * "DownLoadImageService" is custom, Used to name the worker thread, important only for debugging.
     */
    public ADDownImageService() {
        super("DownLoadImageService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if (ACTION_UPLOAD_IMG.equals(action)) {
                String filePath = intent.getStringExtra(EXTRA_IMG_PATH);
                String imgUrl = intent.getStringExtra(IMG_URL);
                String contentUrl = intent.getStringExtra(CON_URL);
                String endTime = intent.getStringExtra(END_TIME);
                startLoadFile(filePath, imgUrl, contentUrl, endTime);
            }
        }
    }

    private void startLoadFile(String filePath, final String imgUrl, final String contentUrl, final String endTime) {
        if (NetworkHelper.isNetworkConnect(LeTravelApplication.getInstance())) {
            HttpEngine.getFile()
                    .imgUrl(imgUrl)
                    .readTimeout(60000)
                    .writeTimeout(60000)
                    .connectionTimeout(60000)
                    .fileDir(filePath)
                    .execute(new HttpEngine.FileCallBack() {
                        @Override
                        public void onError(Call call, Exception e) {
                            LogUtils.e("lv", "图片下载失败：" + e.getMessage());
                            LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).saveAdTime("");
                            LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).saveAdDetailUrl("");
                            LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).saveAdImgUrl("");
                        }

                        @Override
                        public void onResponse(File file) {
                            if (file.exists()) {
                                LogUtils.e("lv", "图片下载成功----");
                                LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).saveAdTime(endTime);
                                LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).saveAdDetailUrl(contentUrl);
                                LeTravelApplication.getSharePreferenceInstance(LeTravelApplication.getInstance()).saveAdImgUrl(imgUrl);
                                Intent intent = new Intent();
                                intent.putExtra("filePath", file.getAbsolutePath());
                                sendBroadcast(intent);
                            }
                        }

                        @Override
                        public void inProgress(float progress, long total) {

                        }
                    });
        }
    }

    @Override
    public void onCreate() {
        LogUtils.e("lv","图片下载服务启动onCreat");
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        LogUtils.e("lv","onDestroy");
        super.onDestroy();
    }
}
