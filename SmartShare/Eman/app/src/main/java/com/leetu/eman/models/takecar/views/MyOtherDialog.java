package com.leetu.eman.models.takecar.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leetu.eman.R;


/**
 * Created by 郭宁 on 2015/5/28.
 */
public class MyOtherDialog extends Dialog implements View.OnClickListener {

    private final Context context;
    private LinearLayout ltCarOpinion, ltDestoryReport, ltAccidentReport;
    private ClickListenerInterface clickListenerInterface;
    private TextView tvCancel;

    public MyOtherDialog(Context context) {

        super(context, R.style.selectorDialog);
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.dialog_other, null);
        setContentView(view);

        ltCarOpinion = (LinearLayout) findViewById(R.id.lt_car_opinion);
        ltDestoryReport = (LinearLayout) findViewById(R.id.lt_destory_report);
        ltAccidentReport = (LinearLayout) findViewById(R.id.lt_accident_report);
        tvCancel = (TextView) findViewById(R.id.tv_cancel);
        //设置按钮监听
        ltCarOpinion.setOnClickListener(this);
        ltDestoryReport.setOnClickListener(this);
        ltAccidentReport.setOnClickListener(this);
        tvCancel.setOnClickListener(this);
        Window dialogWindow = getWindow();

        dialogWindow.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        dialogWindow.setWindowAnimations(R.style.mystyle);  //添加动画
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();
        lp.width = (int) (d.widthPixels * 1);
        dialogWindow.setAttributes(lp);
    }

    public void setClicklistener(ClickListenerInterface clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lt_car_opinion:
                clickListenerInterface.carPoinion();
                break;
            case R.id.lt_destory_report:
                clickListenerInterface.destoryReport();
                break;
            case R.id.lt_accident_report:
                clickListenerInterface.accdientReport();
                break;
            case R.id.tv_cancel:
                MyOtherDialog.this.cancel();
                break;
        }
        MyOtherDialog.this.cancel();
    }

    public interface ClickListenerInterface {
        void carPoinion();

        void destoryReport();

        void accdientReport();
    }

}
