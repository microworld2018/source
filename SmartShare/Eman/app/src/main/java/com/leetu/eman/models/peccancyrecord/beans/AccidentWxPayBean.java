package com.leetu.eman.models.peccancyrecord.beans;


/**
 * Created by lvjunfeng on 2017/4/12.
 */

public class AccidentWxPayBean {


    /**
     * wx : {"order":{"sign":"614B35F35C2B042E6EB329FB262DE1AC","return_code":"SUCCESS","partnerid":"1426900102","package":"Sign=WXPay","result_code":"SUCCESS","appid":"wxfdde3d5877da4cf8","timestamp":"1491976353","noncestr":"e20d091d084f47b0bdabd78e5f69","prepayid":"wx20170412135232ea20d1b42f0953744275"}}
     */

    private WxBean wx;

    public WxBean getWx() {
        return wx;
    }

    public void setWx(WxBean wx) {
        this.wx = wx;
    }

    public static class WxBean {
        /**
         * order : {"sign":"614B35F35C2B042E6EB329FB262DE1AC","return_code":"SUCCESS","partnerid":"1426900102","package":"Sign=WXPay","result_code":"SUCCESS","appid":"wxfdde3d5877da4cf8","timestamp":"1491976353","noncestr":"e20d091d084f47b0bdabd78e5f69","prepayid":"wx20170412135232ea20d1b42f0953744275"}
         */

        private OrderBean order;

        public OrderBean getOrder() {
            return order;
        }

        public void setOrder(OrderBean order) {
            this.order = order;
        }

        public static class OrderBean {
            /**
             * sign : 614B35F35C2B042E6EB329FB262DE1AC
             * return_code : SUCCESS
             * partnerid : 1426900102
             * package : Sign=WXPay
             * result_code : SUCCESS
             * appid : wxfdde3d5877da4cf8
             * timestamp : 1491976353
             * noncestr : e20d091d084f47b0bdabd78e5f69
             * prepayid : wx20170412135232ea20d1b42f0953744275
             */

            private String sign;
            private String return_code;
            private String partnerid;
            private String packageX;
            private String result_code;
            private String appid;
            private String timestamp;
            private String noncestr;
            private String prepayid;

            public String getSign() {
                return sign;
            }

            public void setSign(String sign) {
                this.sign = sign;
            }

            public String getReturn_code() {
                return return_code;
            }

            public void setReturn_code(String return_code) {
                this.return_code = return_code;
            }

            public String getPartnerid() {
                return partnerid;
            }

            public void setPartnerid(String partnerid) {
                this.partnerid = partnerid;
            }

            public String getPackageX() {
                return packageX;
            }

            public void setPackageX(String packageX) {
                this.packageX = packageX;
            }

            public String getResult_code() {
                return result_code;
            }

            public void setResult_code(String result_code) {
                this.result_code = result_code;
            }

            public String getAppid() {
                return appid;
            }

            public void setAppid(String appid) {
                this.appid = appid;
            }

            public String getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(String timestamp) {
                this.timestamp = timestamp;
            }

            public String getNoncestr() {
                return noncestr;
            }

            public void setNoncestr(String noncestr) {
                this.noncestr = noncestr;
            }

            public String getPrepayid() {
                return prepayid;
            }

            public void setPrepayid(String prepayid) {
                this.prepayid = prepayid;
            }
        }
    }
}
