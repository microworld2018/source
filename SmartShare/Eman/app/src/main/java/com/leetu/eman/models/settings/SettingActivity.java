package com.leetu.eman.models.settings;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.activity.WebViewActivity;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.VersionUtils;
import com.leetu.eman.views.TitleBar;

import static com.leetu.eman.models.activity.WebViewActivity.TITLE_FLAG;
import static com.leetu.eman.models.activity.WebViewActivity.WEBVIEW_URL;

public class SettingActivity extends BaseActivity implements SettingContract.View, View.OnClickListener {

    public static final int RESULT_EXIT = 101;
    private TitleBar titleBar;
    private LinearLayout suggestion;
    private LinearLayout agreement;
    private TextView versonCode;
    private Button exit;
    private SettingPresent settingPresent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initView();
    }

    private void initView() {
        titleBar = (TitleBar) findViewById(R.id.setting_title);
        suggestion = (LinearLayout) findViewById(R.id.ll_suggestion);
        agreement = (LinearLayout) findViewById(R.id.ll_agreement);
        versonCode = (TextView) findViewById(R.id.version_code);
        exit = (Button) findViewById(R.id.bt_exit);
        titleBar.setTitle("设置");
        titleBar.setLeftClickListener(this);
        suggestion.setOnClickListener(this);
        agreement.setOnClickListener(this);
        exit.setOnClickListener(this);

        settingPresent = new SettingPresent(this, this);
        loadData();
    }

    private void loadData() {
        String versionName = VersionUtils.getVersionName(this);
        if (versionName != null) {
            versonCode.setText("V " + versionName);
        }

        String token = LeTravelApplication.getSharePreferenceInstance(this).getToken();
        if(TextUtils.isEmpty(token)) {
            exit.setVisibility(View.GONE);
        }else {
            exit.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showExit() {
        showProgressBar(false);
        showButtomToast("已退出");
        LeTravelApplication.getSharePreferenceInstance(this).saveToken("");
        LeTravelApplication.getSharePreferenceInstance(this).saveUserNumber("");
        LeTravelApplication.getSharePreferenceInstance(this).saveUserStatus(0, "");
        //关闭当前页，告知主页已退出，改变leftmenu状态
        Intent intent = getIntent();
        setResult(RESULT_EXIT,intent);
       LeTravelApplication.getInstance().finishAllExcept(MainActivity.class);
    }

    @Override
    public void showFail(String message) {
        super.showFail(message);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                finish();
                break;
            case R.id.ll_suggestion:
                Intent intent = new Intent();
                intent.setClass(SettingActivity.this, ComplaintActivity.class);//意见反馈
                startActivity(intent);
                break;
            case R.id.ll_agreement:
                Intent intent1 = new Intent(SettingActivity.this, WebViewActivity.class);
                intent1.putExtra(WEBVIEW_URL, URLS.ARGUMENT);
                intent1.putExtra(TITLE_FLAG, "用户协议");
                startActivity(intent1);
                break;
            case R.id.bt_exit:
                showDialog("退出登录", "您确定退出登录？", "退出", "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showProgressBar(true);
                        settingPresent.userExit();
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                break;
        }
    }

    @Override
    public void loginOk() {
        exit.setVisibility(View.VISIBLE);
        super.loginOk();
    }
}
