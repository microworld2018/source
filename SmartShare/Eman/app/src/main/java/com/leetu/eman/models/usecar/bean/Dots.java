package com.leetu.eman.models.usecar.bean;/**
 * Created by jyt on 2016/9/22.
 */

/**
 * created by neo on 2016/9/22 16:21
 * 网点
 */
public class Dots {
    String name;
    String latlng;
    String carcount;
    String dotId;
    String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    public String getCarcount() {
        return carcount;
    }

    public void setCarcount(String carcount) {
        this.carcount = carcount;
    }

    public String getDotId() {
        return dotId;
    }

    public void setDotId(String dotId) {
        this.dotId = dotId;
    }

    @Override
    public String toString() {
        return "Dots{" +
                "name='" + name + '\'' +
                ", latlng='" + latlng + '\'' +
                ", carcount='" + carcount + '\'' +
                ", dotId='" + dotId + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
