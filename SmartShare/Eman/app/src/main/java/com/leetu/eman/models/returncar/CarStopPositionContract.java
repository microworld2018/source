package com.leetu.eman.models.returncar;/**
 * Created by gn on 2016/10/11.
 */


import com.leetu.eman.base.BaseContract;

/**
 * created by neo on 2016/10/11 13:32
 */
public interface CarStopPositionContract {
    interface View extends BaseContract {
        void showReturnCar();
    }

    interface UserAction {
        void returnCar(String lat, String lng, String carId, String dotId, String floorNo, String carNo);
    }
}
