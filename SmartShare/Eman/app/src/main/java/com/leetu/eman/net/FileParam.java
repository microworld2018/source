package com.leetu.eman.net;

/**
 * Created by kevin on 16/3/16.
 */
public class FileParam {

    private String name;
    private String path;

    public FileParam() {

    }

    public FileParam(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
