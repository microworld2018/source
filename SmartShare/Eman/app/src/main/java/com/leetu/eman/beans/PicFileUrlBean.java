package com.leetu.eman.beans;/**
 * Created by gn on 2016/10/9.
 */

/**
 * created by neo on 2016/10/9 17:29
 */
public class PicFileUrlBean {
    boolean state;
    String url;

    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
