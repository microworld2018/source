package com.leetu.eman.models.usecar.bean;/**
 * Created by jyt on 2016/9/22.
 */

import java.util.List;

/**
 * created by neo on 2016/9/22 16:22
 */
public class Points {
    List<Dots> dots;
    NearUserPoint user;

    public List<Dots> getDots() {
        return dots;
    }

    public void setDots(List<Dots> dots) {
        this.dots = dots;
    }

    public NearUserPoint getUser() {
        return user;
    }

    public void setUser(NearUserPoint user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Points{" +
                "dots=" + dots +
                ", user=" + user +
                '}';
    }
}
