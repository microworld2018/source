package com.leetu.eman.models.settings;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.settings.beans.InvoiceShowBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;


/**
 * Created by Larry.Shi on 2016/10/20.
 */

public class InvoicePresenter extends BasePresenter implements InvoiceContract.UserAction {
    private Context context;
    private InvoiceContract.View invoiceCenterListener;

    public InvoicePresenter(Context context, InvoiceContract.View invoiceCenterListener) {
        this.context = context;
        this.invoiceCenterListener = invoiceCenterListener;
    }

    @Override
    public void getInvoice(final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, invoiceCenterListener)) {
                HttpEngine.post().url(URLS.USER_AVAILABLE_BILL)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .tag(InvoiceActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), invoiceCenterListener)) {
                                    LogUtils.e("gn", "发票总金额" + response.getData());

                                    InvoiceShowBean invoiceshowBean = JsonParser.getParsedData(response.getData(), InvoiceShowBean.class);
                                    if (invoiceshowBean != null) {
                                        invoiceCenterListener.showInvoice(invoiceshowBean);
                                    }
                                } else {
                                    if (response.getResultCode() != 206) {
                                        if (isFrist) {
                                            invoiceCenterListener.contentFail();
                                        } else {
                                            invoiceCenterListener.showFail(response.getResultCode(), response.getResultMsg());
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                invoiceCenterListener.timeOutFail();
                            }
                        });
            }
        } else {
            if (isFrist) {
                invoiceCenterListener.contentFail();
            } else {
                invoiceCenterListener.showFail(context.getResources().getString(R.string.net_error));
            }
        }
    }

    @Override
    public void invoiceBill(String totalFee, String title, String type, String recipients, String telphone, String address, String postcode) {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, invoiceCenterListener)) {
                HttpEngine.post().url(URLS.DRAW_BILL)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .addParam("totalFee", totalFee)
                        .addParam("title", title)
                        .addParam("type", type)
                        .addParam("recipients", recipients)
                        .addParam("telphone", telphone)
                        .addParam("address", address)
                        .addParam("postcode", postcode)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), invoiceCenterListener)) {
                                    LogUtils.e("gn", "打印发票" + response.getData());
                                    invoiceCenterListener.showUpOk();

                                } else {
                                    if (response.getResultCode() != 206) {
                                        invoiceCenterListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                invoiceCenterListener.timeOutFail();
                            }
                        });
            }
        } else {
            invoiceCenterListener.showFail(context.getResources().getString(R.string.net_error));
        }
    }
}
