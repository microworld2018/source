package com.leetu.eman.models.rnauthentication;/**
 * Created by jyt on 2016/9/19.
 */

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.utils.PicUpUtils;

import java.util.List;

/**
 * created by neo on 2016/9/19 12:06
 */
public class RNAuthenticationPresenter extends BasePresenter implements RNAuthenticationContract.UserAction, PicUpUtils.PicListener {
    private Context context;
    private RNAuthenticationContract.View rnAuthenticationListener;
    private PicUpUtils picUpUtils;
    private String name;
    private String cardId;
    private int type = 0;
    private String img;
    private String idCardImg;
    private List<String> fils;

    public RNAuthenticationPresenter(Context context, RNAuthenticationContract.View rnAuthenticationListener) {
        this.context = context;
        this.rnAuthenticationListener = rnAuthenticationListener;
        picUpUtils = new PicUpUtils();
    }

    @Override
    public void load(final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(context)) {
            if (isLoad(context, rnAuthenticationListener)) {
                HttpEngine.post().url(URLS.USER_INFO)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .tag(RNAuthenticationActivity.class)
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (checkCode(response.getResultCode(), rnAuthenticationListener)) {
                                    LogUtils.e("gn", "个人信息" + response.getData());
                                    UserInfoBean userInfoBean = JsonParser.getParsedData(response.getData(), UserInfoBean.class);
                                    if (userInfoBean != null) {
                                        LeTravelApplication.getSharePreferenceInstance(context).saveUserStatus(userInfoBean.getState(), userInfoBean.getHandHeldIdCard());
                                        rnAuthenticationListener.showLoad(userInfoBean);
                                    }
                                } else {
                                    if (response.getResultCode() != 206) {
                                        if (isFrist) {
                                            if (response.getResultCode() == 214) {

                                            } else {
                                                rnAuthenticationListener.contentFail();
                                            }
                                        } else {
                                            rnAuthenticationListener.showFail(response.getResultCode(), response.getResultMsg());
                                            LogUtils.e("gn", response.getResultMsg());
                                        }

                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                rnAuthenticationListener.timeOutFail();
                            }
                        });
            }
        } else {
            if (isFrist) {
                rnAuthenticationListener.contentFail();
            } else {
                rnAuthenticationListener.showFail(context.getResources().getString(R.string.net_error));
            }
        }
    }


    @Override
    public void upData(String name, String cardId, List<String> files) {
        this.name = name;
        this.cardId = cardId;
        picUpUtils.setPicListener(this);
        LogUtils.e("lv", "上传照片服务器----" + files.toString());
        picUpUtils.upLinePic(context, files);

    }

    @Override
    public void upDataAgain(String name, String cardId, String cardImg, String driverImg) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.APPREG_LH)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("idCardImg", cardImg)
                    .addParam("drivingImg", driverImg)
                    .addParam("name", name)
                    .addParam("IDCard", cardId)
                    .execute(new HttpEngine.ResponseCallback() {
                                 @Override
                                 public void onResponse(ResponseStatus response) {
                                     LogUtils.e("gn", "未审核再次提交" + response.getData());
                                     rnAuthenticationListener.showRNA();
                                 }

                                 @Override
                                 public void onFailure(Exception error) {
                                     rnAuthenticationListener.timeOutFail();
                                 }
                             }
                    );

        } else {
            rnAuthenticationListener.contentFail();
        }
    }

    @Override
    public void upDataAgain(String name, String cardId, String idCardImg, String img, List<String> files, int type) {
        this.name = name;
        this.cardId = cardId;
        this.type = type;
        this.fils = files;
        fils.remove(0);//里面包含了之前已经上传的url
        fils.remove(0);
        this.idCardImg = idCardImg;
        this.img = img;
        picUpUtils.setPicListener(this);
        LogUtils.e("lv", "老用户上传手持照片到服务器----" + fils.toString());
        picUpUtils.upPic(context, fils);
    }


    @Override
    public void picOk(String picUrls) {

        if (NetworkHelper.isNetworkConnect(context)) {
            if (picUrls != null || !"".equals(picUrls)) {
                String[] strs = picUrls.split("\\|");
                String[] img = new String[3];
                if (type == 0) {
                    img = strs;
                } else if (type == 1) {
                    img[0] = this.idCardImg;
                    img[1] = this.img;
                    img[2] = strs[0];
                }
                HttpEngine.post().url(URLS.APPREG_LH)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                        .addParam("idCardImg", img[0])
                        .addParam("drivingImg", img[1])
                        .addParam("handHeldIdCard", img[2])
                        .addParam("name", name)
                        .addParam("IDCard", cardId)
                        .execute(new HttpEngine.ResponseCallback() {
                                     @Override
                                     public void onResponse(ResponseStatus response) {
                                         LogUtils.e("lv", "实名认证提交成功");
                                         if (response.getResultCode() == 200) {
                                             rnAuthenticationListener.showRNA();
                                         } else if (response.getResultCode() == 281) {
                                             rnAuthenticationListener.showFail(response.getResultCode(), response.getResultMsg());
                                         }

                                     }

                                     @Override
                                     public void onFailure(Exception error) {
                                         rnAuthenticationListener.timeOutFail();
                                     }
                                 }
                        );
            }

        } else {
            rnAuthenticationListener.contentFail();
        }
    }

    @Override
    public void picFail(String msg) {
        rnAuthenticationListener.showFail(msg);
    }

    @Override
    public void picNetError() {
        rnAuthenticationListener.timeOutFail();
    }
}
