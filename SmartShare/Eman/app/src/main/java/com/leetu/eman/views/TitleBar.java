package com.leetu.eman.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leetu.eman.R;


/**
 * Created by kevin on 16/3/13.
 */
public class TitleBar extends LinearLayout {

    private TextView mTitle;
    private TextView mLeftText;
    private TextView mRightText;
    private TextView mCity;
    private ImageView mLeftIcon;
    public ImageView mRightIcon;

    public View mLeftLayout, mSelectCity;
    private View mRightLayout;

    public TitleBar(Context context) {
        super(context);
    }

    public TitleBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);

    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TitleBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(attrs);

    }


    private void init(AttributeSet attributeSet) {
        LayoutInflater inflater = (LayoutInflater) getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        setOrientation(VERTICAL);
        inflater.inflate(R.layout.title_bar_layour, this, true);

        mTitle = (TextView) findViewById(R.id.custom_action_bar_title);
        mLeftText = (TextView) findViewById(R.id.custom_action_bar_left_tv);
        mRightText = (TextView) findViewById(R.id.custom_action_bar_right_tv);
        mLeftIcon = (ImageView) findViewById(R.id.custom_action_bar_left_icon);
        mRightIcon = (ImageView) findViewById(R.id.custom_action_bar_right_icon);
        mCity = (TextView) findViewById(R.id.custom_action_bar_city);
        mSelectCity = findViewById(R.id.custom_action_bar_select_city);
        mLeftLayout = findViewById(R.id.layout_left);
        mRightLayout = findViewById(R.id.layout_right);
    }

    public void setTitle(String title) {
        mTitle.setVisibility(View.VISIBLE);
        mTitle.setText(title);
    }

    public void setTitle(int resId) {
        mTitle.setText(resId);
    }

    public void setmTitleSize(float size) {
        mTitle.setTextSize(size);
    }

    public void showTitle(boolean isShow) {
        mTitle.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    public void setTitleColor(int color) {
        mTitle.setTextColor(color);
    }


    public void setLeftText(String title) {
        mLeftLayout.setVisibility(View.VISIBLE);
        mLeftText.setVisibility(View.VISIBLE);
        mLeftIcon.setVisibility(View.GONE);
        mLeftText.setText(title);
    }

    public void setLeftText(int resId) {
        mLeftLayout.setVisibility(View.VISIBLE);
        mLeftText.setVisibility(View.VISIBLE);
        mLeftIcon.setVisibility(View.GONE);
        mLeftText.setText(resId);
    }

    public void showLeftText() {
        mLeftLayout.setVisibility(View.VISIBLE);
        mLeftText.setVisibility(View.VISIBLE);
        mLeftIcon.setVisibility(View.GONE);
    }


    public void setRightText(String title) {
        mRightLayout.setVisibility(View.VISIBLE);
        mRightText.setVisibility(View.VISIBLE);
        mRightIcon.setVisibility(View.GONE);
        mRightText.setText(title);
    }

    public void setmRightTextColor(String color) {
        mRightText.setTextColor(Color.parseColor(color));
    }

    public void setRightText(int resId) {
        mRightLayout.setVisibility(View.VISIBLE);
        mRightText.setVisibility(View.VISIBLE);
        mRightIcon.setVisibility(View.GONE);
        mRightText.setText(resId);
    }

    public void showRightText() {
        mRightLayout.setVisibility(View.VISIBLE);
        mRightText.setVisibility(View.VISIBLE);
        mRightIcon.setVisibility(View.GONE);
    }


    public void setLeftIcon(int resId) {
        mLeftIcon.setImageResource(resId);
    }


    public void setLeftIcon(Bitmap bitmap) {
        mLeftIcon.setImageBitmap(bitmap);
    }

    public void showLeftIcon() {
        mLeftLayout.setVisibility(View.VISIBLE);
        mLeftText.setVisibility(View.GONE);
        mLeftIcon.setVisibility(View.VISIBLE);
    }

    //显示城市
    public void showCity() {
        mSelectCity.setVisibility(VISIBLE);
    }

    public void setCity(String city) {
        mCity.setText(city);
    }

    public void setRightIcon(int resId) {
        mRightIcon.setImageResource(resId);
    }

    public void setRightIcon(Bitmap bitmap) {
        mRightIcon.setImageBitmap(bitmap);
    }

    public void showRightIcon() {
        mRightLayout.setVisibility(View.VISIBLE);
        mRightText.setVisibility(View.GONE);
        mRightIcon.setVisibility(View.VISIBLE);
    }

    public void showLeftLayout() {
        mLeftLayout.setVisibility(View.VISIBLE);
    }

    public void hideLeftLayout() {
        mLeftLayout.setVisibility(View.GONE);
    }


    public void showRightLayout() {
        mRightLayout.setVisibility(View.VISIBLE);
    }

    public void hideRightLayout() {
        mRightLayout.setVisibility(View.GONE);
    }

    public void setLeftClickListener(OnClickListener onClickListener) {
        mLeftLayout.setOnClickListener(onClickListener);
    }

    public void setRightClickListener(OnClickListener onClickListener) {
        mRightLayout.setOnClickListener(onClickListener);
    }

    public void setCityClickListener(OnClickListener onClickListener) {
        mSelectCity.setOnClickListener(onClickListener);
    }
}
