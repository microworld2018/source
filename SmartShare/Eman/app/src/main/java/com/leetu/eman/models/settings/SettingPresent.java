package com.leetu.eman.models.settings;

import android.content.Context;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;

import static com.umeng.socialize.utils.DeviceConfig.context;


/**
 * Created by lvjunfeng on 2017/6/20.
 */

public class SettingPresent extends BasePresenter implements SettingContract.UserAction{

    private final SettingContract.View listener;
    private final Context mContent;

    public SettingPresent(Context context, SettingContract.View listener) {
        this.mContent = context;
        this.listener = listener;
    }

    @Override
    public void userExit() {

        if (NetworkHelper.isNetworkConnect(mContent)) {
            if (LeTravelApplication.getSharePreferenceInstance(mContent).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(mContent).getToken().equals("")) {
                HttpEngine.post().url(URLS.USER_EXIT)
                        .addParam("token", LeTravelApplication.getSharePreferenceInstance(mContent).getToken())
                        .execute(new HttpEngine.ResponseCallback() {
                            @Override
                            public void onResponse(ResponseStatus response) {
                                if (response.getResultCode() == 256) {
                                    LogUtils.e("gn", "退出登录" + response.getData());
                                    listener.showExit();
                                } else {
                                    if (response.getResultCode() == 206) {
                                        listener.showExit();
                                    } else {
                                        listener.showFail(response.getResultMsg());
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Exception error) {
                                listener.showFail(context.getResources().getString(R.string.time_out));
                            }
                        });
            }
        } else {
            listener.showFail(context.getResources().getString(R.string.net_error));
        }
    }
}
