package com.leetu.eman.models.coupon.adapters;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.TextAppearanceSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.models.coupon.beans.CouponBean;
import com.leetu.eman.models.coupon.views.CouponBgView;

import java.util.List;

/**
 * Created by neo on 2016/12/17.
 */

public class CouPonAdapter extends BaseAdapter {
    private Context context;
    private List<CouponBean> couponBeanList;
    private LayoutInflater inflater;

    public CouPonAdapter(Context context, List<CouponBean> couponBeanList) {
        this.context = context;
        this.couponBeanList = couponBeanList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return couponBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return couponBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        CouponBean couponBean = couponBeanList.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.item_activity_coupon, null);
//            viewHolder.couponBgView = (CouponBgView) convertView.findViewById(R.id.view_couponview);
            viewHolder.ltBg = (LinearLayout) convertView.findViewById(R.id.lt_coupon_bg);
            viewHolder.tvTtile = (TextView) convertView.findViewById(R.id.tv_coupon_title);
            viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tv_coupon_date);
            viewHolder.tvPrice = (TextView) convertView.findViewById(R.id.tv_coupon_price);
            viewHolder.tvDes = (TextView) convertView.findViewById(R.id.tv_coupon_des);
            viewHolder.tvIsHoliday = (TextView) convertView.findViewById(R.id.tv_coupon_isholiday);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (couponBean.getIsUsed() != null) {
            if (couponBeanList.get(position).getIsUsed().equals("0")) {
                viewHolder.ltBg.setBackgroundResource(R.mipmap.ic_coupon_bg);
            } else if (couponBeanList.get(position).getIsUsed().equals("1")) {
                viewHolder.ltBg.setBackgroundResource(R.mipmap.ic_coupon_used);
            } else if (couponBeanList.get(position).getIsUsed().equals("2")) {
                viewHolder.ltBg.setBackgroundResource(R.mipmap.ic_coupon_timeout);
            } else {
            }
        }
        viewHolder.tvTtile.setText(couponBean.getCouponName());
        viewHolder.tvDate.setText("有效期至:" + couponBean.getEndValidityTime().replace("-", "."));
        viewHolder.tvDes.setText(couponBean.getRemark());

        if(couponBean.getIsHoliday() == 0 ) {//通用券
            viewHolder.tvIsHoliday.setText("");
        }else if(couponBean.getIsHoliday() == 1) {//工作日券
            viewHolder.tvIsHoliday.setText("(工作日可用)");
        }else if(couponBean.getIsHoliday() == 2) {//节假日券
            viewHolder.tvIsHoliday.setText("(节假日可用)");
        }

        SpannableString styledText = new SpannableString("¥" + doubleTrans(couponBean.getPrice()));
        styledText.setSpan(new TextAppearanceSpan(context, R.style.coupon_symbol_style), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        styledText.setSpan(new TextAppearanceSpan(context, R.style.coupon_price_style), 1, styledText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        viewHolder.tvPrice.setText(styledText, TextView.BufferType.SPANNABLE);
        return convertView;
    }
    //小数点后为0时去除，只显示整数
    public static String doubleTrans(double num){
        if(num % 1.0 == 0){
            return String.valueOf((long)num);
        }
        return String.valueOf(num);
    }
    class ViewHolder {
        CouponBgView couponBgView;
        private LinearLayout ltBg;
        private TextView tvTtile, tvIsHoliday,tvDate, tvPrice, tvDes;
    }
}
