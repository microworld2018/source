package com.leetu.eman.models.balance;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.balance.beans.BalanceBean;
import com.leetu.eman.models.coupon.CouponActivity;
import com.leetu.eman.models.deposit.DepositActivity;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.views.SelectorPayDialog;
import com.leetu.eman.views.TitleBar;

/**
 * 余额页面
 */
public class BalanceActivity extends BaseActivity implements BalanceContract.View, View.OnClickListener {

    private TitleBar titleBar;
    private Button btRechargeDetails, btRecharge;
    private BalancePresenter balancePresenter;
    private TextView tvBalance, tvCouponeNumber, tvDepositValue;
    private BalanceBean balanceBean;
    private SelectorPayDialog selectorPayDialog;
    private LinearLayout llMyCoupon, llMyDeposit;
    private boolean isFrist = true;
    public static  int  REQUEST_BANLACE = 001;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);
        initView();
        loadData();
    }


    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_balabce);
        btRechargeDetails = (Button) findViewById(R.id.charge_detail);
        btRecharge = (Button) findViewById(R.id.bt_recharge);
        tvBalance = (TextView) findViewById(R.id.tv_balances);
        llMyCoupon = (LinearLayout) findViewById(R.id.ll_my_coupon);
        llMyDeposit = (LinearLayout) findViewById(R.id.ll_my_deposit);
        tvCouponeNumber = (TextView) findViewById(R.id.tv_coupon_number);
        tvDepositValue = (TextView) findViewById(R.id.tv_deposit_value);
        balancePresenter = new BalancePresenter(this, this);

        titleBar.setTitle("我的钱包");
        titleBar.setLeftClickListener(this);
        btRecharge.setOnClickListener(this);
        btRechargeDetails.setOnClickListener(this);
        llMyCoupon.setOnClickListener(this);
        llMyDeposit.setOnClickListener(this);
    }

    protected void loadData() {
        showLoading();
        balancePresenter.load(isFrist);
    }

    @Override
    public void showLoad(BalanceBean balanceBean) {
        showProgressBar(false);
        showContent();
        isFrist = false;
        this.balanceBean = balanceBean;
        tvBalance.setText("" + balanceBean.getAmount());
        int amount = Integer.parseInt(balanceBean.getCouponCount());
        if (amount == 0) {
            tvCouponeNumber.setText("0张");
        } else {
            tvCouponeNumber.setText(balanceBean.getCouponCount() + "张");
        }

        if (balanceBean.getDeposit() == 0) {
            tvDepositValue.setText("请充值押金");
        } else {
            tvDepositValue.setText(balanceBean.getDeposit() + "元");
        }
    }

    @Override
    protected void onRetryLoadData() {
        showProgressBar(true);
        balancePresenter.load(isFrist);
        super.onRetryLoadData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                BalanceActivity.this.finish();
                break;
            case R.id.charge_detail:
                Intent intent = new Intent(BalanceActivity.this, TransactionDetailsActivity.class);
                intent.putExtra(TransactionDetailsActivity.BANLANCE_DETAILS, balanceBean);
                startActivity(intent);
                break;
            case R.id.bt_recharge://弹出对话框
//                selectorPayWay();
                showButtomToast("功能暂未开通");
                break;

            case R.id.ll_my_coupon:
                Intent intent2 = new Intent(BalanceActivity.this, CouponActivity.class);
                startActivity(intent2);
                break;

            case R.id.ll_my_deposit:
                Intent intent3 = new Intent(BalanceActivity.this, DepositActivity.class);
                startActivityForResult(intent3,REQUEST_BANLACE);
                break;

        }
    }

    private void selectorPayWay() {
        if (selectorPayDialog == null) {
            selectorPayDialog = new SelectorPayDialog(BalanceActivity.this);

            selectorPayDialog.setClicklistener(new SelectorPayDialog.ClickListenerInterface() {
                @Override
                public void aliPay() {
//                    Intent intent = new Intent(BalanceActivity.this, ReChargeResaultActivity.class);
//                    intent.putExtra(ReChargeResaultActivity.RESAULT, false);
//                    startActivity(intent);
//                    BalanceActivity.this.finish();

                }

                @Override
                public void wxPay() {
//                    BalanceActivity.this.finish();
                }

                @Override
                public void cancelPay() {

                }
            });

            selectorPayDialog.show();

        } else {
            selectorPayDialog.show();
        }

    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        balancePresenter.load(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(BalanceActivity.class);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_BANLACE) {
            if(resultCode == 200 ) {
                showProgressBar(true);
                balancePresenter.load(false);
            }
        }
    }
}
