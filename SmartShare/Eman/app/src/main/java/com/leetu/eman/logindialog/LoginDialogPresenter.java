package com.leetu.eman.logindialog;/**
 * Created by gn on 2016/9/28.
 */

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.models.login.bean.UserBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;


/**
 * created by neo on 2016/9/28 13:46
 */
public class LoginDialogPresenter implements LoginDialogContract.UserAction {
    private Context context;
    private LoginDialogContract.View myLoginLisenter;

    public LoginDialogPresenter(Context context, LoginDialogContract.View myLoginLisenter) {
        this.context = context;
        this.myLoginLisenter = myLoginLisenter;
    }

    @Override
    public void dialogLoad(String phoneNumber, String code) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.LOAD)
                    .addParam("phoneNo", phoneNumber)
                    .addParam("smsCode", code)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (response.getResultCode() == 200) {
                                UserBean userBean = JsonParser.getParsedData(response.getData(), UserBean.class);
                                LeTravelApplication.getSharePreferenceInstance(context).saveToken(userBean.getToken());
                                LeTravelApplication.getSharePreferenceInstance(context).saveUserNumber(userBean.getPhoneNoStr());

                                myLoginLisenter.showDialogLoad(userBean);
                            } else {
                                myLoginLisenter.showFail(response.getResultMsg());
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            myLoginLisenter.timeOutFail();
                        }
                    });
        } else {
            myLoginLisenter.contentFail();
        }
    }

    @Override
    public void dialogSendCode(String phoneNumber) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.CODE)
                    .addParam("phoneNo", phoneNumber)
                    .addParam("type", "login")
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (response.getResultCode() == 200) {
                                myLoginLisenter.showDialogSendCode();
                            } else {
                                myLoginLisenter.showFail(response.getResultMsg());
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            myLoginLisenter.timeOutFail();
                        }
                    });
        } else {
            myLoginLisenter.contentFail();
        }
    }
}
