package com.leetu.eman.models.returncar.beans;

/**
 * Created by lvjunfeng on 2017/2/17.
 */

public class WXPayBean {
    /**
     * amount : 0.0
     * userPrice : 0.0
     * state : true
     * payp : 0.01
     * wx : {"order":{"sign":"D21420CC73F175EC5200C84DCC07D880","err_code":"ORDERPAID","return_code":"SUCCESS","err_code_des":"该订单已支付","result_code":"FAIL","appid":"wxfdde3d5877da4cf8","mch_id":"1426900102","nonce_str":"2jk2McvRIdgLhthZ","return_msg":"OK"}}
     */

    private String payp;
    private WxBean wx;
    private String orderNo;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getPayp() {
        return payp;
    }

    public void setPayp(String payp) {
        this.payp = payp;
    }

    public WxBean getWx() {
        return wx;
    }

    public void setWx(WxBean wx) {
        this.wx = wx;
    }

    public static class WxBean {
        /**
         * order : {"sign":"D21420CC73F175EC5200C84DCC07D880","err_code":"ORDERPAID","return_code":"SUCCESS","err_code_des":"该订单已支付","result_code":"FAIL","appid":"wxfdde3d5877da4cf8","mch_id":"1426900102","nonce_str":"2jk2McvRIdgLhthZ","return_msg":"OK"}
         */

        private OrderBean order;

        public OrderBean getOrder() {
            return order;
        }

        public void setOrder(OrderBean order) {
            this.order = order;
        }

        public static class OrderBean {
            /**
             * sign : D21420CC73F175EC5200C84DCC07D880
             * err_code : ORDERPAID
             * return_code : SUCCESS
             * err_code_des : 该订单已支付
             * result_code : FAIL
             * appid : wxfdde3d5877da4cf8
             * mch_id : 1426900102
             * nonce_str : 2jk2McvRIdgLhthZ
             * return_msg : OK
             */

            private String sign;
            private String err_code;
            private String return_code;
            private String err_code_des;
            private String result_code;
            private String appid;
            private String partnerid;
            private String noncestr;
            private String return_msg;
            private String prepayid;
            private String timestamp;

            public String getSign() {
                return sign;
            }

            public void setSign(String sign) {
                this.sign = sign;
            }

            public String getErr_code() {
                return err_code;
            }

            public void setErr_code(String err_code) {
                this.err_code = err_code;
            }

            public String getReturn_code() {
                return return_code;
            }

            public void setReturn_code(String return_code) {
                this.return_code = return_code;
            }

            public String getErr_code_des() {
                return err_code_des;
            }

            public void setErr_code_des(String err_code_des) {
                this.err_code_des = err_code_des;
            }

            public String getResult_code() {
                return result_code;
            }

            public void setResult_code(String result_code) {
                this.result_code = result_code;
            }

            public String getAppid() {
                return appid;
            }

            public void setAppid(String appid) {
                this.appid = appid;
            }

            public String getReturn_msg() {
                return return_msg;
            }

            public void setReturn_msg(String return_msg) {
                this.return_msg = return_msg;
            }

            public String getPrepayid() {
                return prepayid;
            }

            public void setPrepayid(String prepayid) {
                this.prepayid = prepayid;
            }

            public String getTimestamp() {
                return timestamp;
            }

            public void setTimestamp(String timestamp) {
                this.timestamp = timestamp;
            }

            public String getPartnerid() {
                return partnerid;
            }

            public void setPartnerid(String partnerid) {
                this.partnerid = partnerid;
            }

            public String getNoncestr() {
                return noncestr;
            }

            public void setNoncestr(String noncestr) {
                this.noncestr = noncestr;
            }
        }
    }
}
