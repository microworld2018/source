package com.leetu.eman.models.coupon.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.leetu.eman.R;

/**
 * Created by Administrator on 2016/12/17.
 */

public class CouponBgView extends View {
    /**
     * 绘制时控制文本绘制的范围
     */
    private Rect mBound;
    private Paint mPaint;
    private int r = 10;
    private int clearance = 5;
    private int y = 0;
    private String textTitle;
    private String textTime;
    private String couponCount;
    private String Symbol = "¥";

    public CouponBgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // 初始化画笔、Rect
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBound = new Rect();
    }

    public CouponBgView(Context context) {
        super(context);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBound = new Rect();
    }

    public CouponBgView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mBound = new Rect();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setColor(getResources().getColor(R.color.coupon_price_bg));
        // 绘制一个填充色为蓝色的矩形
        canvas.drawRect(0, 0, getWidth() / 3, getHeight(), mPaint);
        mPaint.setColor(getResources().getColor(R.color.coupon_title_bg));
        canvas.drawRect(getWidth() / 3, 0, getWidth(), getHeight(), mPaint);
//        //绘制Title
//        mPaint.setColor(Color.BLACK);
//        mPaint.setTextSize(50);
//        // 绘制标题
//        mPaint.getTextBounds(textTitle, 0, textTitle.length(), mBound);
//        float textTtileWidth = mBound.width();
//        float textTtileHeight = mBound.height();
//        canvas.drawText(textTitle, getWidth() / 3 + 30, getHeight() / 2, mPaint);
//        mPaint.setTextSize(35);
//        //绘制时间
//        mPaint.getTextBounds(textTime, 0, textTime.length(), mBound);
//        float textTimeWidth = mBound.width();
//        float textTimeHeight = mBound.height();
//        canvas.drawText(textTime, getWidth() / 3 + 30, getHeight() / 2
//                + textTtileHeight / 2 - 15 + textTtileHeight, mPaint);
//        //测量
//        int pH = getHeight();
//        int pW = getWidth() / 3;
//        mPaint.setColor(Color.WHITE);
//        if (couponCount != null) {
//            if (couponCount.toString().trim().length() == 1) {
//
//            } else if (couponCount.toString().trim().length() == 2) {
//            } else if (couponCount.toString().trim().length() == 3) {
//
//                mPaint.getTextBounds(Symbol, 0, Symbol.length(), mBound);
//                float textSymbolWidth = mBound.width();
//                float textSymbolHeight = mBound.height();
//                mPaint.getTextBounds(couponCount, 0, couponCount.length(), mBound);
//                float textCouponWidth = mBound.width();
//                float textCouponHeight = mBound.height();
//                mPaint.setTextSize(60);
//                mPaint.setTextAlign(Paint.Align.CENTER);
//                canvas.drawText(Symbol, (pW - textCouponWidth/2 - textSymbolWidth/2) / 2, getHeight() / 2 + textCouponHeight / 2
//                        , mPaint);
//                mPaint.setTextSize(120);
//                canvas.drawText(couponCount, (pW - textCouponWidth/2 - textSymbolWidth/2) / 2, getHeight() / 2 + textCouponHeight / 2
//                        , mPaint);
//
//            }
//        }

        //===========
        mPaint.setColor(getResources().getColor(R.color.coupon_title_bg));
        int size = getHeight() / (r + clearance);
        y = 10;
        canvas.drawCircle(getWidth() / 3, y, r, mPaint);
        for (int i = 1; i < size; i++) {
            y += 25;
            canvas.drawCircle(getWidth() / 3, y, r, mPaint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(widthMeasureSpec, heightMeasureSpec);
    }

    public void setMyBgColor(String textTitle, String textTime, String couponCount) {
        this.textTitle = textTitle;
        this.textTime = textTime;
        this.couponCount = couponCount;
        postInvalidate();
    }
}
