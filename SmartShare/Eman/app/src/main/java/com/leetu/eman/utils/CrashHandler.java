package com.leetu.eman.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Looper;
import android.widget.Toast;

import com.leetu.eman.application.LeTravelApplication;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lvjunfeng on 2017/1/17.
 */

public class CrashHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler mDefaultUncaughtExceptionHandler;
    private String versionName;
    private String versionCode;
    private String userNumber;
    private String exTime;

    //懒汉式的单例模式
    private CrashHandler() {

    }

    private static CrashHandler crashHandler = null;

    public static CrashHandler getInatance() {
        if (crashHandler == null) {
            crashHandler = new CrashHandler();
        }
        return crashHandler;
    }

    private Context mContext;

    public void init(Context context) {
        this.mContext = context;
        //获取系统默认的捕获未被处理的异常的处理器
        mDefaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        //将当前类设置为捕获未被处理的异常的处理器
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    /**
     * 如果出现了为捕获的异常,则自动调用如下的回调方法
     *
     * @param thread
     * @param ex
     */
    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        if (ex == null) {
            mDefaultUncaughtExceptionHandler.uncaughtException(thread, ex);
        } else {
            LogUtils.e("lv", "异常发生了");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日  HH:mm:ss");
            Date curDate = new Date(System.currentTimeMillis());//获取当前时间
            exTime = formatter.format(curDate);
            new Thread() {
                @Override
                public void run() {
                    //只有在主线程中才可以调用如下的Looper的方法。将要在主线程中执行的代码放在两个方法之间即可。
                    Looper.prepare();
                    Toast.makeText(mContext, "程序异常，即将退出应用!", Toast.LENGTH_SHORT).show();
                    Looper.loop();
                }
            }.start();

            //收集出现的异常的信息
            collectionException(ex);

            //接下来:2s内退出当前的应用
            try {
                Thread.sleep(2000);
                //移除栈中所有的activity
                LeTravelApplication.getInstance().finishAllActivity();
                android.os.Process.killProcess(android.os.Process.myPid());//杀掉当前的进程
                System.exit(0);//关闭当前的虚拟机
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * 收集异常信息的方法，并发送给服务器
     *
     * @param ex
     */
    private void collectionException(Throwable ex) {
        //用户
        userNumber = LeTravelApplication.getSharePreferenceInstance(mContext).getUserNumber();
        //异常信息
        final String exMessage = ex.getMessage();//异常信息
        //设备信息
        final String deviceMES = Build.PRODUCT + "--" + Build.DEVICE + "--" + Build.MODEL + "--" + Build.VERSION.SDK_INT + "--" + Build.FINGERPRINT + "--" + Build.CPU_ABI;
        //当前应用版本
        PackageManager pm = mContext.getPackageManager();
        PackageInfo pi = null;
        try {
            pi = pm.getPackageInfo(mContext.getPackageName(), PackageManager.GET_ACTIVITIES);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (pi != null) {
            versionName = pi.versionName == null ? "null" : pi.versionName;
            versionCode = pi.versionCode + "";
        }

        // 将获取的异常相关的数据发送给后台。
        new Thread() {
            @Override
            public void run() {
                LogUtils.e("lv", "异常用户：" + userNumber);
                LogUtils.e("lv", "异常发生时间：" + exTime);
                LogUtils.e("lv", "异常信息：" + exMessage);
                LogUtils.e("lv", "异常设备：" + deviceMES);
                LogUtils.e("lv", "当前版本信息versionName:" + versionName + "  versionCode:" + versionCode);
//                if (NetworkHelper.isNetworkConnect(mContext)) {
//                    HttpEngine.post()
//                            .url("")
//                            .addParam("userNumber", userNumber)
//                            .addParam("time", exTime)
//                            .addParam("exMessage", exMessage)
//                            .addParam("deviceMES", deviceMES)
//                            .addParam("versionName", versionName)
//                            .addParam("versionCode", versionCode)
//                            .execute(new HttpEngine.ResponseCallback() {
//                                @Override
//                                public void onResponse(ResponseStatus response) {
//
//                                }
//
//                                @Override
//                                public void onFailure(Exception error) {
//
//                                }
//                            });
//                }
            }
        }.start();
    }

    /**
     * 获取手机的硬件信息
     *
     * @return
     */
    private String getMobileInfo() {
        StringBuffer sb = new StringBuffer();
        //通过反射获取系统的硬件信息
        try {

            Field[] fields = Build.class.getDeclaredFields();
            for (Field field : fields) {
                //暴力反射 ,获取私有的信息
                field.setAccessible(true);
                String name = field.getName();
                String value = field.get(null).toString();
                sb.append(name + "=" + value);
                sb.append("\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb.toString();
    }
}
