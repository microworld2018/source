package com.leetu.eman.models.coupon.beans;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/12/17.
 */

public class CouponBean implements Serializable {
    double orderUsePrice;
    double price;
    String couponName;
    String endValidityTime;
    String id;
    String remark;
    String dateTime;
    String isUsed;
    String startValidityTime;
    String couponId;
    int isHoliday;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public double getOrderUsePrice() {
        return orderUsePrice;
    }

    public void setOrderUsePrice(double orderUsePrice) {
        this.orderUsePrice = orderUsePrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getEndValidityTime() {
        return endValidityTime;
    }

    public void setEndValidityTime(String endValidityTime) {
        this.endValidityTime = endValidityTime;
    }

    public String getStartValidityTime() {
        return startValidityTime;
    }

    public void setStartValidityTime(String startValidityTime) {
        this.startValidityTime = startValidityTime;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public int getIsHoliday() {
        return isHoliday;
    }

    public void setIsHoliday(int isHoliday) {
        this.isHoliday = isHoliday;
    }
}
