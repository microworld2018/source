package com.leetu.eman.models.notify.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.models.notify.beans.NotifyBean;

import java.util.List;


/**
 * 作者：尚硅谷-lvjunfeng on 2016/11/27 22:35
 * 微信：825801863
 * QQ号：825801863
 * 作用：xxxx
 */
public class NotifyAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<NotifyBean> lists;

    public NotifyAdapter(Context context, List<NotifyBean> messages) {
        this.mContext = context;
        this.lists = messages;
    }

    @Override
    public int getCount() {
        return lists == null ? 0 : lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NotifyBean notifyBean = lists.get(position);
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_notify, null);
            viewHolder.redPoint = convertView.findViewById(R.id.redpoint);
            viewHolder.notifyTime = (TextView) convertView.findViewById(R.id.tv_notify_time);
            viewHolder.notifyCotent = (TextView) convertView.findViewById(R.id.tv_notify_content);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.notifyCotent.setText(notifyBean.getContent());
//        viewHolder.notifyTime.setText(notifyBean.getTs());
//        int state = notifyBean.getIsread();
//        if (state == 0) {
//            viewHolder.redPoint.setVisibility(View.VISIBLE);
//        } else {
//            viewHolder.redPoint.setVisibility(View.GONE);
//        }

        return convertView;
    }

    class ViewHolder {
        View redPoint;
        TextView notifyTime, notifyCotent;
    }
}
