package com.leetu.eman.models.currentorder.carreport;/**
 * Created by jyt on 2016/9/20.
 */

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.currentorder.CurrentOrderActivity;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.views.CustomEditText;
import com.leetu.eman.views.PhotoFragment;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * created by neo on 2016/9/20 11:06
 * 事故上报
 */
public class ReportAccidentActivity extends BaseActivity implements View.OnClickListener, ReportAccidentContract.View, PhotoFragment.OnSelectPicListener {
    private TitleBar titleBar;
    private ReportAccidentPresenter reportAccidentPresenter;
    private Button btAccident;
    private PhotoFragment fragment;
    private CurrentOrderBean currentOrderBean;
    private String carLose;
    private CustomEditText etDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_accident);
        initView();
    }

    protected void initView() {
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getSerializable(CurrentOrderActivity.CURRENT_ORDER) != null) {
                currentOrderBean = (CurrentOrderBean) getIntent().getExtras().getSerializable(CurrentOrderActivity.CURRENT_ORDER);
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_accident);
        btAccident = (Button) findViewById(R.id.bt_report_accident_commit);
        etDesc = (CustomEditText) findViewById(R.id.et_accident_desc);

        savePhotoFragment();
        reportAccidentPresenter = new ReportAccidentPresenter(this, this);

        etDesc.setHintText("损坏与问题描述");
        etDesc.setMaxLength(200);
        titleBar.setTitle("事故上报");
        titleBar.setLeftClickListener(this);
        btAccident.setOnClickListener(this);
        etDesc.setTextNumChangeListener(new CustomEditText.TextNumChangeListener() {
            @Override
            public void onNumChangeListener(boolean isChange) {
                if (isChange && fragment.factSelected.size() > 0) {
                    btAccident.setEnabled(true);
                } else {
                    btAccident.setEnabled(false);
                }
            }
        });
    }

    void savePhotoFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.
                beginTransaction();
        fragment = new PhotoFragment();
        fragment.setOnSelectPicListener(this);
        //加到Activity中
        fragmentTransaction.add(R.id.lt_frgament_content, fragment);
        //加到后台堆栈中，有下一句代码的话，点击返回按钮是退到Activity界面，没有的话，直接退出Activity
        //后面的参数是此Fragment的Tag。相当于id
//        fragmentTransaction.addToBackStack("fragment1");
        //记住提交
        fragmentTransaction.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                ReportAccidentActivity.this.finish();
                break;
            case R.id.bt_report_accident_commit:
                if (fragment.factSelected.size() != 0) {
                    List<String> list = new ArrayList<>();
                    for (int i = 0; i < fragment.factSelected.size(); i++) {
                        list.add(fragment.factSelected.get(i).getOriginalPath());
                    }
                    if (currentOrderBean != null) {
                        showProgressBar(true);
                        carLose = etDesc.getText().toString().trim();
                        reportAccidentPresenter.upData(currentOrderBean.getCar().getCarId(), currentOrderBean.getOrderId(), carLose, list);
                    }
                }
                break;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

    }

    @Override
    public void showUpData() {
        showButtomToast("提交成功");
        showProgressBar(false);
        ReportAccidentActivity.this.finish();
    }

    @Override
    public void selectPic(int size) {
        if (!etDesc.getText().toString().trim().equals("")) {
            if (size > 0) {
                btAccident.setEnabled(true);
            } else {
                btAccident.setEnabled(false);
            }
        }
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showContent();
    }
}
