package com.leetu.eman.models.returncar;/**
 * Created by gn on 2016/10/10.
 */


import com.leetu.eman.base.BaseContract;

/**
 * created by neo on 2016/10/10 14:36
 */
public interface ReturnCarCortract {
    interface View extends BaseContract {
        void showLockCarDoor();

        void showReturnCar();

    }

    interface UserAction {
        void lockCarDoor(String lat, String lng, String carId, String orderId, String dotId);

        void returnCar(String lat, String lng, String carId, String dotId);
    }
}
