package com.leetu.eman.models.rnauthentication;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.models.rnauthentication.views.GridImageActivity;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.utils.PictureUtil;
import com.leetu.eman.views.MyPhotoDialog;
import com.leetu.eman.views.TitleBar;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by neo on 2016/9/19.
 * 实名认证页面
 */

public class RNAuthenticationActivity extends BaseActivity implements View.OnClickListener, RNAuthenticationContract.View {
    private TitleBar titleBar;
    private RelativeLayout ivID, ivDriver;
    private MyPhotoDialog myPhotoDialog;
    private Button btUpPic;
    PictureUtil pictureUtil;
    private String imagename;
    private String path;
    private File dir;
    private ImageView ivIdCardImg, ivDriverImg, ivHandCardImg;
    private EditText etRelname, etCardid;
    private LinearLayout llTitle;
    private TextView tvTitle;
    private RNAuthenticationPresenter rnAuthenticationPresenter;
    //
    private final int SELECT_IMAGE_CODE = 1001;
    public static final int CAMERA_PHOTO = 10002;
    public static final int PHOTO_PHOTO = 1003;
    public static final int DATA_CHANGE_REQUST = 2000;
    public static final int DATA_CHANGE_RESULT = 2001;
    public static final String POSITION = "position";
    public int type;//1代表身份证，2代表驾驶证,3代表两个都改
    public int imgType;//已经修改了
    private int status;//1未提交 4未通过
    private UserInfoBean userInfoBean;
    private int upData = 1;
    // 选择图片
    private String str_choosed_img = "";
    public static List<String> selected;
    private String[] pic = new String[3];

    private boolean isFrist = true;
    private boolean isHaveName = false;
    private boolean isHaveId = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rnauthentication);
        initView();
        loadData();
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_rn_authentiaction);
        llTitle = (LinearLayout) findViewById(R.id.ll_title);
        tvTitle = (TextView) findViewById(R.id.tv_info_title);

        ivID = (RelativeLayout) findViewById(R.id.iv_id_pic);
        ivDriver = (RelativeLayout) findViewById(R.id.iv_driver_pic);
        ivIdCardImg = (ImageView) findViewById(R.id.iv_idcard_img);
        ivDriverImg = (ImageView) findViewById(R.id.iv_driver_img);
        ivHandCardImg = (ImageView) findViewById(R.id.iv_handcard_img);

        btUpPic = (Button) findViewById(R.id.bt_up_pic);
        etRelname = (EditText) findViewById(R.id.et_relname);
        etCardid = (EditText) findViewById(R.id.et_cardid);


        selected = new ArrayList<String>();
        rnAuthenticationPresenter = new RNAuthenticationPresenter(this, this);

        titleBar.setTitle("实名认证");
        titleBar.setLeftClickListener(this);
        ivID.setOnClickListener(this);
        ivDriver.setOnClickListener(this);
        ivHandCardImg.setOnClickListener(this);
        btUpPic.setOnClickListener(this);


        etRelname.addTextChangedListener(new TextNameWatCher());
        etCardid.addTextChangedListener(new TextCardIdWatCher());

    }

    protected void loadData() {
        showLoading();
        rnAuthenticationPresenter.load(isFrist);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                RNAuthenticationActivity.this.finish();
                break;
            case R.id.iv_id_pic:
                type = 1;
                showChooseIMG_WAYDialog();
                break;

            case R.id.iv_driver_pic:
                type = 2;
                showChooseIMG_WAYDialog();
                break;
            case R.id.iv_handcard_img:
                type = 3;
                showChooseIMG_WAYDialog();
                break;
            case R.id.bt_up_pic:
                if (check()) {
                    showEnableProgressBar(true,"");
                    if (status == 1 || status == 4) {//未提交 审核未通过
                        rnAuthenticationPresenter.upData(etRelname.getText().toString().trim(), etCardid.getText().toString().trim(), selected);//没有做非空判断
                    } else if (status == 3 && TextUtils.isEmpty(userInfoBean.getHandHeldIdCard())) {//补齐资料
                        rnAuthenticationPresenter.upDataAgain(etRelname.getText().toString().trim(), etCardid.getText().toString().trim(), userInfoBean.getIdCardImg(), userInfoBean.getDrivingLicenseImg(), selected, 1);
                    }
                } else {
                    Toast.makeText(RNAuthenticationActivity.this, "请输入正确的姓名或身份证号", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean check() {
        if (etRelname.getText().toString().trim().length() > 1 && etCardid.getText().toString().trim().length() == 18) {
            return true;
        }
        return false;
    }


    /*
    * 选择图片上传的方式
    */
    private void showChooseIMG_WAYDialog() {
        //显示窗口
        if (myPhotoDialog == null) {
            myPhotoDialog = new MyPhotoDialog(this);
            myPhotoDialog.setClicklistener(new MyPhotoDialog.ClickListenerInterface() {
                @Override
                public void camera() {
                    takeCamrea();
                }

                @Override
                public void cancel() {

                }

                @Override
                public void pic() {
                    Intent intent = new Intent(RNAuthenticationActivity.this, GridImageActivity.class);
                    startActivityForResult(intent, SELECT_IMAGE_CODE);
                }
            });
            myPhotoDialog.show();
        } else {
            myPhotoDialog.show();
        }
    }

    void takeCamrea() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            path = getExternalCacheDir().getPath() + "/" + "leTravel/photo/";
        } else {
            path = getCacheDir().getPath() + "/" + "leTravel/photo/";
        }
        dir = new File(path);
        if (!dir.exists()) dir.mkdirs();
        imagename = getTimeName(System.currentTimeMillis()) + ".png";

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(dir, imagename);
        Uri u;
        if (Build.VERSION.SDK_INT < 24) {//当系统小于6.0的时候存储的路径
            u = Uri.fromFile(f);
        } else {//系统大于6.0存储的路径
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            u = FileProvider.getUriForFile(this, getPackageName() + ".fileProvider", f);

        }
        // 启动相机程序
        intent.putExtra(MediaStore.EXTRA_OUTPUT, u);
        startActivityForResult(intent, CAMERA_PHOTO);
    }

    /**
     * 时间戳
     *
     * @param time
     * @return
     */
    public static String getTimeName(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(time);
        return formatter.format(date);
    }


    @SuppressWarnings("unchecked")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == PHOTO_PHOTO && requestCode == SELECT_IMAGE_CODE) {
            List<String> photos = (List<String>) data.getExtras()
                    .getSerializable("photos");
            if (type == 1) {
                pic[0] = photos.get(0);
                ivIdCardImg.setImageResource(R.mipmap.ic_complet_photo);
            } else if (type == 2) {
                pic[1] = photos.get(0);
                ivDriverImg.setImageResource(R.mipmap.ic_complet_photo);
            } else if (type == 3) {
                if (userInfoBean != null) {
                    if (userInfoBean.getState() == 3 && TextUtils.isEmpty(userInfoBean.getHandHeldIdCard())) {
                        pic[0] = userInfoBean.getIdCardImg();
                        pic[1] = userInfoBean.getDrivingLicenseImg();
                    }
                }
                pic[2] = photos.get(0);
                ivHandCardImg.setImageResource(R.mipmap.ic_complet_photo);
            }

        }
        if (requestCode == CAMERA_PHOTO) {
            if (resultCode != RESULT_OK) {
                return;
            }
            if (type == 1) {
                pic[0] = path + imagename;
                ivIdCardImg.setImageResource(R.mipmap.ic_complet_photo);
            } else if (type == 2) {
                pic[1] = path + imagename;
                ivDriverImg.setImageResource(R.mipmap.ic_complet_photo);
            } else if (type == 3) {
                if (userInfoBean != null) {
                    if (userInfoBean.getState() == 3 && TextUtils.isEmpty(userInfoBean.getHandHeldIdCard())) {//老用户未补齐资料
                        pic[0] = userInfoBean.getIdCardImg();
                        pic[1] = userInfoBean.getDrivingLicenseImg();
                    }
                }
                pic[2] = path + imagename;
                ivHandCardImg.setImageResource(R.mipmap.ic_complet_photo);
            }
        }
        checkPic();
    }

    /**
     * 判断数据添加及是否可上传
     */
    void checkPic() {
        selected.clear();

        if (pic[0] == null || "".equals(pic[0])) {

        } else {
            selected.add(pic[0]);
            if (pic[1] == null || "".equals(pic[1])) {

            } else {
                selected.add(pic[1]);
                if (pic[2] == null || "".equals(pic[2])) {

                } else {
                    selected.add(pic[2]);
                }
            }
        }
        if (selected.size() == 3 && etRelname.getText().toString().trim().length() > 1 && etCardid.getText().toString().trim().length() == 18) {
            btUpPic.setEnabled(true);
        } else {
            btUpPic.setEnabled(false);
        }

    }

    @Override
    public void showLoad(UserInfoBean userInfoBean) {
        showProgressBar(false);
        showContent();
        isFrist = false;
        if (userInfoBean != null) {
            this.userInfoBean = userInfoBean;
            int state = userInfoBean.getState();
            switch (state) {
                case 1://未审核
                    status = 1;
                    llTitle.setVisibility(View.GONE);
                    etRelname.setEnabled(true);
                    etCardid.setEnabled(true);
                    btUpPic.setVisibility(View.VISIBLE);
                    changePhoneImageIcon(0);
                    break;

                case 2:
                    llTitle.setVisibility(View.VISIBLE);
                    tvTitle.setText("审核中");
                    tvTitle.setTextColor(getResources().getColor(R.color.banlance));
                    etRelname.setEnabled(false);
                    etCardid.setEnabled(false);
                    etRelname.setText(userInfoBean.getName());
                    etCardid.setText(userInfoBean.getIDCard());
                    btUpPic.setVisibility(View.GONE);
                    changePhoneImageIcon(1);
                    break;

                case 3:
                    status = 3;
                    if (userInfoBean != null && !TextUtils.isEmpty(userInfoBean.getHandHeldIdCard())) {
                        //新注册用户身份审核通过
                        llTitle.setVisibility(View.VISIBLE);
                        tvTitle.setText("身份已认证");
                        etRelname.setEnabled(false);
                        etCardid.setEnabled(false);
                        etRelname.setText(userInfoBean.getName());
                        etCardid.setText(userInfoBean.getIDCard());
                        btUpPic.setVisibility(View.GONE);
                        changePhoneImageIcon(1);
                    } else {
                        //老用户补齐资料
                        llTitle.setVisibility(View.VISIBLE);
                        tvTitle.setText("请补齐资料");
                        tvTitle.setTextColor(getResources().getColor(R.color.text_rnauthentication));
                        etRelname.setEnabled(false);
                        etCardid.setEnabled(false);
                        etRelname.setText(userInfoBean.getName());
                        etCardid.setText(userInfoBean.getIDCard());
                        btUpPic.setVisibility(View.VISIBLE);
                        btUpPic.setEnabled(false);
                        changePhoneImageIcon(2);
                    }

                    break;
                case 4:
                    //未通过
                    status = 4;
                    llTitle.setVisibility(View.VISIBLE);
                    tvTitle.setText("审核未通过，请修改个人信息");
                    tvTitle.setTextColor(getResources().getColor(R.color.text_rnauthentication));
                    etRelname.setEnabled(true);
                    etCardid.setEnabled(true);
                    etRelname.setText(userInfoBean.getName());
                    etCardid.setText(userInfoBean.getIDCard());
                    btUpPic.setVisibility(View.VISIBLE);
                    btUpPic.setEnabled(false);
                    changePhoneImageIcon(0);
                    break;
            }
        }
    }

    private void changePhoneImageIcon(int statue) {
        if (statue == 0) {//需上传，可编辑（未认证，认证失败，修改认证）
            ivIdCardImg.setImageResource(R.mipmap.ic_takephoto);
            ivDriverImg.setImageResource(R.mipmap.ic_takephoto);
            ivHandCardImg.setImageResource(R.mipmap.ic_takephoto);
            ivID.setEnabled(true);
            ivDriver.setEnabled(true);
            ivHandCardImg.setEnabled(true);
        } else if (statue == 1) {//已上传。不可编辑
            ivIdCardImg.setImageResource(R.mipmap.ic_complet_photo);
            ivDriverImg.setImageResource(R.mipmap.ic_complet_photo);
            ivHandCardImg.setImageResource(R.mipmap.ic_complet_photo);
            ivID.setEnabled(false);
            ivDriver.setEnabled(false);
            ivHandCardImg.setEnabled(false);
        } else if (statue == 2) {//补齐资料
            ivIdCardImg.setImageResource(R.mipmap.ic_complet_photo);
            ivDriverImg.setImageResource(R.mipmap.ic_complet_photo);
            ivHandCardImg.setImageResource(R.mipmap.ic_takephoto);
            ivID.setEnabled(false);
            ivDriver.setEnabled(false);
            ivHandCardImg.setEnabled(true);
        }
    }

    @Override
    public void showFail(int code, String message) {
        showEnableProgressBar(false,"");
        if (code == 281) {
            showRnAuthenDialog("您的身份证已认证其他手机\n如有问题请联系客服", "400-888-1212", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        } else {
            super.showFail(code, message);
        }
    }

    @Override
    protected void onRetryLoadData() {
        showProgressBar(true);
        rnAuthenticationPresenter.load(isFrist);
        super.onRetryLoadData();
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        rnAuthenticationPresenter.load(true);
    }

    @Override
    public void showRNA() {
        showEnableProgressBar(false,"");
        showButtomToast("提交成功，等待审核！");
        RNAuthenticationActivity.this.finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(RNAuthenticationActivity.class);
    }

    class TextNameWatCher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().trim().length() > 1) {
                isHaveName = true;
            } else {
                isHaveName = false;
            }
            if (status == 4) {
                if (isHaveName && isHaveId) {
                    btUpPic.setEnabled(true);
                } else {
                    btUpPic.setEnabled(false);
                }
            } else if (status == 1) {
                if (isHaveName && isHaveId && selected.size() == 3) {
                    btUpPic.setEnabled(true);
                } else {
                    btUpPic.setEnabled(false);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

    class TextCardIdWatCher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().trim().length() == 18) {
                isHaveId = true;
            } else {
                isHaveId = false;
            }
            if (status == 4) {
                if (isHaveName && isHaveId) {
                    btUpPic.setEnabled(true);
                } else {
                    btUpPic.setEnabled(false);
                }
            } else if (status == 1) {
                if (isHaveName && isHaveId && selected.size() == 3) {
                    btUpPic.setEnabled(true);
                } else {
                    btUpPic.setEnabled(false);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

}
