package com.leetu.eman.models.confirmorder.beans;/**
 * Created by jyt on 2016/9/27.
 */

import java.util.List;

/**
 * created by neo on 2016/9/27 14:11
 */
public class ReturnCarPointBeans {
    List<ReturnCarPointBean> dots;

    public List<ReturnCarPointBean> getDots() {
        return dots;
    }

    public void setDots(List<ReturnCarPointBean> dots) {
        this.dots = dots;
    }
}
