package com.leetu.eman.base;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.models.activity.WebViewActivity;
import com.leetu.eman.models.confirmorder.ConfirmOrderActivity;
import com.leetu.eman.models.currentorder.CurrentOrderActivity;
import com.leetu.eman.models.currentorder.beans.CurrentOrderBean;
import com.leetu.eman.models.returncar.PayOrderActivity;
import com.leetu.eman.models.returncar.beans.OrederCreateDetailBean;
import com.leetu.eman.models.takecar.TakeCarActivity;
import com.leetu.eman.models.takecar.beans.TakeCarBean;
import com.leetu.eman.models.updataservice.CheckUserOrderService;
import com.leetu.eman.models.updataservice.beans.TokenBean;
import com.leetu.eman.mygreedao.NavigationRecord;
import com.leetu.eman.mygreedao.utils.DaoOperationUtils;
import com.leetu.eman.mygreedao.utils.DebugMessage;
import com.leetu.eman.mygreedao.utils.MyDateTimeUtil;
import com.leetu.eman.net.ConnectionChangeReceiver;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.utils.StatusBarUtil;
import com.leetu.eman.views.DepositMessageDialog;
import com.leetu.eman.views.LoadingDialog;
import com.leetu.eman.views.MessageDialog;
import com.leetu.eman.views.TitleBar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URISyntaxException;
import java.util.List;

import static com.leetu.eman.models.activity.WebViewActivity.TITLE_FLAG;
import static com.leetu.eman.models.activity.WebViewActivity.WEBVIEW_URL;


/**
 * Created by neo on 16/11/29.
 */
public abstract class BaseActivity extends LifecycleActivity implements BaseContract {//    private ErrorView mErrorView;

    //带进度条和文字的对话框
//    private ProgressDialog mProgressDialog;
    private LoadingDialog mProgressDialog;
    private Toast mToast;
    private PullToRefreshCompleteListener pullToRefreshCompleteListener;
    //地图
    public LocationClient mLocClient;
    private MyLocationLisenter myLocationLisenter;
    //网络接收
    protected ConnectionChangeReceiver myReceiver;
    public IntentFilter filter;
    //基类布局
    private LinearLayout parentLinearLayout;//把父类activity和子类activity的view都add到这里
    protected View baseView;
    private ViewGroup viewGroup;
    private ImageView baseErrorIV, baseLoadIV;
    private TextView baseErrorTitle, tvLoadGetcode, tvArgument;
    private TextView baseErrorSubTitle;
    private LinearLayout ltLoading, ltError, ltLogin;
    private EditText etloadPhonenumber, etLoadCode;
    protected Button btStartUse;
    private Handler handler;
    private BasePresenter basePresenter;
    private int time = 60;
    private MessageDialog checkDialog;
    protected TitleBar titleBar;
    public static final String PAY_FLAGS = "payFlags";//支付标记
    //权限
    private static final int MY_PERMISSIONS_REQUEST_CALL_PHONE = 1;//gps定位权限

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LeTravelApplication.getInstance().addActivity(this);
        initContentAndBaseView(R.layout.activity_base);
        registerReceiver();//监听网络变化

        getLocation();
        initProgressAndToast();

    }


    /**
     * 初始化contentview和基类
     */
    private void initContentAndBaseView(int layoutResID) {
        viewGroup = (ViewGroup) findViewById(android.R.id.content);
        viewGroup.removeAllViews();
        parentLinearLayout = new LinearLayout(this);
        parentLinearLayout.setOrientation(LinearLayout.VERTICAL);
        viewGroup.addView(parentLinearLayout);
        LayoutInflater.from(this).inflate(layoutResID, parentLinearLayout, true);

        baseView = viewGroup.findViewById(R.id.base_view);
        ltLoading = (LinearLayout) baseView.findViewById(R.id.lt_laoding);
        baseLoadIV = (ImageView) baseView.findViewById(R.id.tv_laoding_gif);
        ltError = (LinearLayout) baseView.findViewById(R.id.error_and_empty_layout);
        baseErrorIV = (ImageView) baseView.findViewById(R.id.error_image);
        baseErrorTitle = (TextView) baseView.findViewById(R.id.error_title);
        baseErrorSubTitle = (TextView) baseView.findViewById(R.id.error_subtitle);
        ltLogin = (LinearLayout) baseView.findViewById(R.id.base_login);
        titleBar = (TitleBar) findViewById(R.id.title_loading);

        tvLoadGetcode = (TextView) baseView.findViewById(R.id.tv_load_getcode);
        etloadPhonenumber = (EditText) baseView.findViewById(R.id.et_load_phonenumber);
        etLoadCode = (EditText) baseView.findViewById(R.id.et_load_code);
        btStartUse = (Button) baseView.findViewById(R.id.bt_start_use);
        tvArgument = (TextView) findViewById(R.id.tv_argument);


        String strText = "点击“立即登录”，即表示已阅读并同意《电动侠出行用户协议》";


        Glide.with(this)
                .load(R.mipmap.ic_loading_mid)
                .asGif().diskCacheStrategy(DiskCacheStrategy.SOURCE).into(baseLoadIV);

        basePresenter = new BasePresenter();
        handler = new Handler();

        SpannableString builder = new SpannableString(strText);
        etloadPhonenumber.addTextChangedListener(new PhoneTextwatcher());
        etLoadCode.addTextChangedListener(new CodeTextwatcher());

        titleBar.setLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BaseActivity.this.finish();
            }
        });
        btStartUse.setOnClickListener(myOnClickListenr);
        tvLoadGetcode.setOnClickListener(myOnClickListenr);
        ltError.setOnClickListener(myOnClickListenr);

        builder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(BaseActivity.this, WebViewActivity.class);
                intent.putExtra(WEBVIEW_URL, URLS.ARGUMENT);
                intent.putExtra(TITLE_FLAG, "用户协议");
                startActivity(intent);
            }
        }, 15, builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        builder.setSpan(new UnderlineSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(getResources().getColor(R.color.main));
                ds.setUnderlineText(false);
            }

        }, 15, builder.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvArgument.setText(builder);
        tvArgument.setHighlightColor(getResources().getColor(android.R.color.transparent));
        tvArgument.setMovementMethod(LinkMovementMethod.getInstance());
    }


    @Override
    public void setContentView(int layoutResID) {

        LayoutInflater.from(this).inflate(layoutResID, parentLinearLayout, true);
        setstatusBar();
    }

    public void setstatusBar(){
        StatusBarUtil.setColor(this,getResources().getColor(R.color.main));
    }

    @Override
    public void setContentView(View view) {

        parentLinearLayout.addView(view);
    }

    @Override
    public void setContentView(View view, ViewGroup.LayoutParams params) {

        parentLinearLayout.addView(view, params);

    }


    private void initProgressAndToast() {
        mProgressDialog = new LoadingDialog(this);
        mProgressDialog.setCancelable(true);
        mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
    }


    class PhoneTextwatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().trim().length() == 11) {
                tvLoadGetcode.setClickable(true);
                tvLoadGetcode.setTextColor(getResources().getColor(R.color.main));
            } else {
                tvLoadGetcode.setClickable(false);
                tvLoadGetcode.setTextColor(getResources().getColor(R.color.gray));
            }
            if (etLoadCode.getText().toString().length() == 4 && etloadPhonenumber.getText().toString().trim().length() == 11) {
                btStartUse.setEnabled(true);
            } else {
                btStartUse.setEnabled(false);
            }


        }
    }

    class CodeTextwatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (etLoadCode.getText().toString().length() == 4 && etloadPhonenumber.getText().toString().trim().length() == 11) {
                btStartUse.setEnabled(true);
            } else {
                btStartUse.setEnabled(false);
            }
        }
    }

    /**
     * 得到指定id的view
     *
     * @param id  所要获取view的id
     * @param <T> 所要获取view的类型
     * @return
     */
    protected <T> T getViewById(int id) {
        return (T) findViewById(id);
    }

    private View.OnClickListener myOnClickListenr = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.bt_start_use:
                    if (checkPhoneNumber(2)) {
                        showProgressBar(true);
                        basePresenter.login(BaseActivity.this, BaseActivity.this, etloadPhonenumber.getText().toString().trim(), etLoadCode.getText().toString().trim());
                    }
                    break;

                case R.id.tv_load_getcode:
                    if (checkPhoneNumber(1)) {
                        time = 120;
                        showProgressBar(true);
                        basePresenter.sendCode(BaseActivity.this, BaseActivity.this, etloadPhonenumber.getText().toString().trim());
                    }
                    break;

                case R.id.error_and_empty_layout:
                    onRetryLoadData();
                    break;
            }
        }
    };

    boolean checkPhoneNumber(int type) {
        boolean b = true;
        switch (type) {
            case 1:
                if (etloadPhonenumber.getText().toString().trim().equals("")) {
                    showButtomToast("请输入电话号码");
                    b = false;
                }
                break;
            case 2:
                if (etloadPhonenumber.getText().toString().trim().equals("")) {
                    showButtomToast("请输入电话号码");
                    b = false;
                } else {
                    if (etLoadCode.getText().toString().trim().equals("")) {
                        showButtomToast("请输入验证码");
                        b = false;
                    }
                }
                break;
        }

        return b;
    }

    @Override
    public void showCode() {
        showProgressBar(false);
        showButtomToast("获取验证码请求已发送");
        handler.postDelayed(runnable, 1000);
        tvLoadGetcode.setClickable(false);
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            time--;
            tvLoadGetcode.setText("" + time + "s");
            if (time == 0) {
                handler.removeCallbacks(this);
                tvLoadGetcode.setClickable(true);
                tvLoadGetcode.setText("重新获取");
            } else {
                handler.postDelayed(this, 1000);
            }
        }
    };

    //显示BaseView
    protected void showBaseView() {
        if (baseView != null) {
            baseView.setVisibility(View.VISIBLE);
        }
    }

    //隐藏
    protected void hideBaseView() {
        if (baseView != null) {
            baseView.setVisibility(View.GONE);
        }
    }

    /**
     * 显示内容视图
     */
    protected void showContent() {
        if (baseView != null) {
            baseView.setVisibility(View.GONE);
        }
    }

    /**
     *
     */
    protected void showLoading() {

        if (baseView == null) {
            return;
        }
        if (baseView.getVisibility() != View.VISIBLE) {
            baseView.setVisibility(View.VISIBLE);
        }
        if (ltError != null) {
            if (ltError.getVisibility() != View.INVISIBLE) {
                ltError.setVisibility(View.INVISIBLE);
            }
        }
        if (ltLogin != null) {
            if (ltLogin.getVisibility() != View.INVISIBLE) {
                ltLogin.setVisibility(View.INVISIBLE);
            }
        }
        if (ltLoading != null) {
            if (ltLoading.getVisibility() != View.VISIBLE) {
                ltLoading.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * 显示加载用户数据出错时的提示信息
     *
     * @param title    主标题
     * @param subTitle 子标题
     */
    protected void showErrorMessage(String title, String subTitle) {
        if (baseView == null) {
            return;
        }
        if (baseView.getVisibility() != View.VISIBLE) {
            baseView.setVisibility(View.VISIBLE);
        }
        if (ltError != null) {
            if (ltError.getVisibility() != View.VISIBLE) {
                ltError.setVisibility(View.VISIBLE);
                baseErrorIV.setImageResource(R.mipmap.error_view_cloud);
            }
        }
        if (ltLoading != null) {
            if (ltLoading.getVisibility() != View.INVISIBLE) {
                ltLoading.setVisibility(View.INVISIBLE);
            }
        }
        if (ltLogin != null) {
            if (ltLogin.getVisibility() != View.INVISIBLE) {
                ltLogin.setVisibility(View.INVISIBLE);
            }
        }
        if (title != null && !"".equals(title.trim())) {
            baseErrorTitle.setVisibility(View.VISIBLE);
            baseErrorTitle.setText(title);
        } else {
            baseErrorTitle.setVisibility(View.GONE);
        }

        if (subTitle != null && !"".equals(subTitle.trim())) {
            baseErrorSubTitle.setVisibility(View.VISIBLE);
            baseErrorSubTitle.setText(subTitle);
        } else {
            baseErrorSubTitle.setVisibility(View.GONE);
        }
    }

    //显示登录框
    protected void showLogin() {
        if (baseView == null) {
            return;
        }
        if (baseView.getVisibility() != View.VISIBLE) {
            baseView.setVisibility(View.VISIBLE);
        }
        if (ltError != null) {
            if (ltError.getVisibility() != View.INVISIBLE) {
                ltError.setVisibility(View.INVISIBLE);
            }
        }
        if (ltLoading != null) {
            if (ltLoading.getVisibility() != View.INVISIBLE) {
                ltLoading.setVisibility(View.INVISIBLE);
            }
        }
        if (ltLogin != null) {
            if (ltLogin.getVisibility() != View.VISIBLE) {
                ltLogin.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * 显示用户数据为空时的提示信息
     *
     * @param title    提示标题
     * @param subTitle 提示子标题
     */

    protected void showBlankMessage(String title, String subTitle) {

//        if (mErrorView == null) {
//            return;
//        }
//
//        mErrorView.setImageResource(R.mipmap.ic_exception_blank_task);
//        mErrorView.showEmptyView();
//
//        if (mErrorView.getVisibility() != View.VISIBLE) {
//            mErrorView.setVisibility(View.VISIBLE);
//        }
//
//        if (title != null && !"".equals(title.trim())) {
//            mErrorView.showTitle(true);
//            mErrorView.setTitle(title);
//        } else {
//            mErrorView.showTitle(false);
//        }
//
//        if (subTitle != null && !"".equals(subTitle.trim())) {
//            mErrorView.showSubtitle(true);
//            mErrorView.setSubtitle(subTitle);
//        } else {
//            mErrorView.showTitle(false);
//        }
//
//        if (getContentView() != null) {
//            getContentView().setVisibility(View.GONE);
//        }


    }


    /**
     * 显示用户数据为空时的提示信息
     *
     * @param title    提示标题
     * @param subTitle 提示子标题
     */
    protected void showBlankMessage(String title, String subTitle, int resId) {

//        if (mErrorView == null) {
//            return;
//        }
//
//        mErrorView.setImageResource(resId);
//        mErrorView.showEmptyView();
//
//        if (mErrorView.getVisibility() != View.VISIBLE) {
//            mErrorView.setVisibility(View.VISIBLE);
//        }
//
//        if (title != null && !"".equals(title.trim())) {
//            mErrorView.showTitle(true);
//            mErrorView.setTitle(title);
//        } else {
//            mErrorView.showTitle(false);
//        }
//
//        if (subTitle != null && !"".equals(subTitle.trim())) {
//            mErrorView.showSubtitle(true);
//            mErrorView.setSubtitle(subTitle);
//        } else {
//            mErrorView.showTitle(false);
//        }
//
//        if (getContentView() != null) {
//            getContentView().setVisibility(View.GONE);
//        }


    }


    /**
     * 重新加载数据，当点击重试按钮时会调用此方法
     */
    protected void onRetryLoadData() {

    }

    /**
     * 设置弹出框是否可以被取消
     *
     * @param Cancelable
     */
    protected void setProgressType(boolean Cancelable) {
        mProgressDialog.setCancelable(Cancelable);
    }

    /**
     * 如果show为true则显示进度条对话框，否则关闭对话框
     *
     * @param show 为true则显示对话框，否则关闭对话框
     */
    protected void showProgressBar(boolean show) {
        showProgressBar(show, "");
    }


    /**
     * 如果show为true则显示进度条对话框，否则关闭对话框.
     *
     * @param show    为true则显示对话框，否则关闭对话框
     * @param message 对话所要显示的提示消息
     */
    protected void showProgressBar(boolean show, String message) {
        if (show) {
            mProgressDialog.setMessage(message);
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }
    /**
     * 加载Progressbar 外部不可点击取消.
     *
     * @param show    为true则显示对话框，否则关闭对话框
     * @param message 对话所要显示的提示消息
     */
    protected void showEnableProgressBar(boolean show, String message) {
        if (show) {
            mProgressDialog.setMessage(message);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();
        } else {
            mProgressDialog.dismiss();
        }
    }


    /**
     * 显示带消息的进度条对话框
     *
     * @param messageId 所要显示的文字的资源id
     */
    protected void showProgressBar(int messageId) {
        String message = getString(messageId);
        showProgressBar(true, message);
    }

    /**
     * 显示吐司
     *
     * @param msg 吐司中所要显示的提示信息
     */
    protected void showButtomToast(String msg) {
        mToast.setText(msg);
        mToast.setGravity(Gravity.NO_GRAVITY, 0, 0);
        mToast.show();
    }

    /**
     * 显示吐司
     *
     * @param messageId 吐司中所要显示的提示信息的id
     */
    protected void showButtomToast(int messageId) {
        mToast.setText(messageId);
        mToast.setGravity(Gravity.NO_GRAVITY, 0, 0);
        mToast.show();
    }

    /**
     * 居中显示吐司
     *
     * @param msg 吐司中所要显示的提示信息
     */

    protected void showMiddleToast(String msg) {
        mToast.setText(msg);
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }


    /**
     * 显示提示对话框
     *
     * @param title   对话框的title
     * @param msg     对话框要显示的提示信息
     * @param clickOk 确定按钮的点击监听器
     */
    protected void showDialog(String title, String msg, DialogInterface.OnClickListener clickOk) {

        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setBtnNo("取消");
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.setCancelListener(null);
        dialog.show();
    }

    /**
     * 显示预约取车提示对话框
     *
     * @param title
     * @param msg
     * @param ok
     * @param cancel
     * @param clickOk
     * @param clickCancel
     * @param onListener
     */
    protected void showTakeDialog(String title, String msg, String ok, String cancel, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel, DialogInterface.OnCancelListener onListener) {
        MessageDialog dialog = new MessageDialog(this);
        if (title.equals("")) {
            dialog.hideTitle();
        } else {
            dialog.showTitle();
        }
        dialog.setTitle(title);
        dialog.setBtnYes(ok);
        dialog.setBtnNo(cancel);
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.setCancelListener(clickCancel);
        dialog.setOnCancelListener(onListener);
        dialog.showAddressLimit();
        dialog.show();
    }

    /**
     * 显示退押金提示对话框
     *
     * @param title
     * @param msg
     * @param ok
     * @param cancel
     * @param clickOk
     * @param clickCancel
     * @param
     */
    protected void showDpositDialog(String title, String msg,String promptMes, String ok, String cancel, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel) {
        DepositMessageDialog dialog = new DepositMessageDialog(this);
        if (title.equals("")) {
            dialog.hideTitle();
        } else {
            dialog.showTitle();
        }
        dialog.setTitle(title);
        dialog.setBtnYes(ok);
        dialog.setBtnNo(cancel);
        dialog.setMessage(msg);
        dialog.setPromptMes(promptMes);
        dialog.setOkListener(clickOk);
        dialog.setCancelListener(clickCancel);
        dialog.setMessageTvColor(getResources().getColor(R.color.text_curorder));
        dialog.showAddressLimit();
        dialog.show();
    }

    /**
     * 显示提示对话框
     *
     * @param title       对话框的title
     * @param msg         对话框要显示的提示信息
     * @param clickOk
     * @param clickCancel
     * @param onListener
     */
    protected void showDialog(String title, String msg, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel, DialogInterface.OnCancelListener onListener) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setBtnNo("取消");
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.setCancelListener(clickCancel);
        dialog.setOnCancelListener(onListener);
        dialog.show();

    }

    /**
     * 显示重复认证提示对话框
     *
     * @param title       对话框的title
     * @param msg         对话框要显示的提示信息
     * @param clickOk
     * @param
     */
    protected void showRnAuthenDialog(String title, String msg, DialogInterface.OnClickListener clickOk) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setBtnNo("");
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.setTitleColor(getResources().getColor(R.color.text_curorder));
        dialog.setOkButtonColor(getResources().getColor(R.color.text_curorder));
        dialog.setMesColor(getResources().getColor(R.color.main));
        dialog.show();

    }

    /**
     * 显示提示对话框
     *
     * @param title
     * @param msg
     * @param ok
     * @param cancel
     * @param clickOk
     * @param clickCancel
     * @param onListener
     */
    protected void showDialog(String title, String msg, String ok, String cancel, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel, DialogInterface.OnCancelListener onListener) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setBtnYes(ok);
        dialog.setBtnNo(cancel);
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.setCancelListener(clickCancel);
        dialog.setOnCancelListener(onListener);
        dialog.show();
    }

    /**
     * 显示提示对话框
     *
     * @param title
     * @param msg
     * @param ok
     * @param cancel
     * @param canCancel
     * @param clickOk
     * @param clickCancel
     * @param onListener
     */
    protected void showDialog(String title, String msg, String ok, String cancel, boolean canCancel, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel, DialogInterface.OnCancelListener onListener) {
        MessageDialog messageDialog = new MessageDialog(this);
        messageDialog.setTitle(title);
        if (title.equals("")) {
            messageDialog.hideTitle();
        } else {
            messageDialog.showTitle();
        }
        messageDialog.setBtnYes(ok);
        messageDialog.setBtnNo(cancel);
        messageDialog.setMessage(msg);
        messageDialog.setCancelable(canCancel);
        messageDialog.setOkListener(clickOk);
        messageDialog.setCancelListener(clickCancel);
        messageDialog.setOnCancelListener(onListener);
        messageDialog.show();

    }

    /**
     * 显示提示对话框
     *
     * @param title
     * @param msg
     * @param ok
     * @param cancel
     * @param canCancel
     * @param clickOk
     * @param clickCancel
     * @param onListener
     */
    private void showOrderDialog(String title, String msg, String ok, String cancel, boolean canCancel, DialogInterface.OnClickListener clickOk, DialogInterface.OnClickListener clickCancel, DialogInterface.OnCancelListener onListener) {
        if (checkDialog == null) {
            checkDialog = new MessageDialog(this);
        }
        checkDialog.setTitle(title);
        if (title.equals("")) {
            checkDialog.hideTitle();
        } else {
            checkDialog.showTitle();
        }
        checkDialog.setBtnYes(ok);
        checkDialog.setBtnNo(cancel);
        checkDialog.setMessage(msg);
        checkDialog.setCancelable(canCancel);
        checkDialog.setOkListener(clickOk);
        checkDialog.setCancelListener(clickCancel);
        checkDialog.setOnCancelListener(onListener);
        checkDialog.show();

    }

    /**
     * 显示提示对话框
     *
     * @param title
     * @param msg
     */
    protected void showOkDialog(String title, String msg) {
        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setMessage(msg);
        dialog.setOkListener(null);
        dialog.show();
    }


    /***
     * 显示提示对话框
     *
     * @param title
     * @param msg
     * @param clickOk
     */
    protected void showOkDialog(String title, String msg, DialogInterface.OnClickListener clickOk) {

        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setMessage(msg);
        dialog.setOkListener(clickOk);
        dialog.show();
    }

    /***
     * 显示提示对话框
     *
     * @param title
     * @param msg
     * @param clickOk
     */
    protected void showOkDialog(String title, String msg, boolean cancel, DialogInterface.OnClickListener clickOk) {

        MessageDialog dialog = new MessageDialog(this);
        dialog.setTitle(title);
        dialog.setBtnYes("确定");
        dialog.setMessage(msg);
        dialog.setCancelable(cancel);
        dialog.setOkListener(clickOk);
        dialog.show();
    }

    public void setPullToRefreshCompleteListener(PullToRefreshCompleteListener pullToRefreshCompleteListener) {
        this.pullToRefreshCompleteListener = pullToRefreshCompleteListener;
    }


    public interface PullToRefreshCompleteListener {
        void pullToRefreshComplete();
    }

    /**
     * 停止刷新
     */
    public class GetDataTask extends AsyncTask<Void, Void, String> {

        //后台处理部分
        @Override
        protected String doInBackground(Void... params) {
            // Simulates a background job.
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            String str = "Added after refresh...I add";
            return str;
        }

        //这里是对刷新的响应，可以利用addFirst（）和addLast()函数将新加的内容加到LISTView中
        //根据AsyncTask的原理，onPostExecute里的result的值就是doInBackground()的返回值
        @Override
        protected void onPostExecute(String result) {
            //在头部增加新添内容
//            mListItems.addFirst(result);

            //通知程序数据集已经改变，如果不做通知，那么将不会刷新mListItems的集合
//            adapter.notifyDataSetChanged();
            // Call onRefreshComplete when the list has been refreshed.
            if (pullToRefreshCompleteListener != null) {
                pullToRefreshCompleteListener.pullToRefreshComplete();
            }
            super.onPostExecute(result);
        }
    }

    //开启定位
    void getLocation() {
        LocationClientOption locOption = new LocationClientOption();
        locOption.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);// 设置定位模式
        locOption.setCoorType("bd09ll");// 设置定位结果类型
//        locOption.setScanSpan(5000);// 设置发起定位请求的间隔时间,ms
        locOption.setIsNeedAddress(true);// 返回的定位结果包含地址信息
        locOption.setNeedDeviceDirect(true);// 设置返回结果包含手机的方向

        mLocClient = new LocationClient(getApplicationContext());
        mLocClient.setLocOption(locOption);
        mLocClient.registerLocationListener(new MyLocationListener());
//        mLocClient.start();
    }

    //==========================
    class MyLocationListener implements BDLocationListener {
        // 异步返回的定位结果
        @Override
        public void onReceiveLocation(BDLocation location) {
            if (myLocationLisenter != null) {
                if (location != null) {
                    if (location.getLatitude() != 4.9E-324 && location.getLongitude() != 4.9E-324) {
                        myLocationLisenter.getLocation(location);
                    }
                }
            }
            mLocClient.stop();//如果定位到的话就停止定位
        }
    }

    public void setMyLocationLisenter(MyLocationLisenter myLocationLisenter) {
        this.myLocationLisenter = myLocationLisenter;
    }

    public interface MyLocationLisenter {
        void getLocation(BDLocation location);
    }

    //重新获取定位
    protected void reGetLocation() {
        if (mLocClient.isStarted()) {
            mLocClient.stop();
        } else {
            mLocClient.start();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtils.e("gn", "执行注册");
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {

        }

    }

    @Override
    protected void onStop() {
        super.onStop();
        LogUtils.e("gn", "执行解注册");
        try {
            EventBus.getDefault().unregister(this);
        } catch (Exception e) {

        }
        if(mToast != null) {
            mToast.cancel();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocClient.stop();
        LeTravelApplication.getInstance().finishActivity(this);

    }


    @Override
    public void contentFail() {//第一次展示页面，没有网络，直接显示error页面
        showErrorMessage(getResources().getString(R.string.net_error), getResources().getString(R.string.relaod));
        showProgressBar(false);
    }

    @Override
    public void dataFail() {
        showProgressBar(false);
    }

    @Override
    public void showFail(String message) {
        showProgressBar(false);
        showButtomToast(message);
    }

    @Override
    public void showFail(int code, String message) {
        LogUtils.e("gn", code + "状态");
        if (code == 213 || code == 214) {
            LogUtils.e("gn", code + "状态11");
        } else {
            showButtomToast(message);
        }
        showProgressBar(false);
    }

    @Override
    public void timeOutFail() {
        showButtomToast(getString(R.string.time_out));
        showProgressBar(false);
    }

    @Override
    public void showNetError(String msg) {//非第一次展示页面，网络未连接只是一个toast
        showProgressBar(false);
        showButtomToast(getString(R.string.net_error));
    }

    @Override
    public void tokenOld() {
        showProgressBar(false);
        showLogin();
    }

    @Override
    public void loginOk() {
        showProgressBar(true);
        Intent intent = new Intent(this, CheckUserOrderService.class);//登录成功再次去查看用户订单
        startService(intent);
    }

    @Override
    public void loginFail(String msg) {
        showProgressBar(false);
        showButtomToast(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void timeOut(String msg) {
        if (msg.equals("")) {
            showProgressBar(false);
        } else {
            showFail(msg);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onError(ResponseStatus response) {
        if (response.getResultCode() == 206) {
            showFail(response.getResultMsg());
        } else if (response.getResultCode() == 214) {
            lockAll(1);
        } else {
            showFail(response.getResultMsg());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onErrorView(TokenBean tokenBean) {
        showProgressBar(false);
        showLogin();
    }

    //如果有订单，回调
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onOrderEvent(final TakeCarBean checkOrderBean) {//在ui线程执行
        showProgressBar(false);
        if (checkOrderBean != null) {
            if (checkOrderBean.getIsexist() == 1) {
//                showOrderDialog("您还有未完成的订单", "是否前去完成？", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(BaseActivity.this, TakeCarActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(ConfirmOrderActivity.ORDER_BREN, checkOrderBean);
                intent.putExtras(bundle);
                startActivity(intent);
//                    }
//                });
            }
        }
    }

    //如果有订单，回调
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onCurrentOrderEvent(final CurrentOrderBean currentOrderBean) {//在ui线程执行
        LogUtils.e("gn", "测试走几次");

        showProgressBar(false);
        if (currentOrderBean != null) {
            if (currentOrderBean.getIsexist() == 1) {
                Intent intent = new Intent(BaseActivity.this, CurrentOrderActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(CurrentOrderActivity.CURRENT_ORDER, currentOrderBean);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }
    }

    //如果未支付的订单
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onNoPayOrder(final OrederCreateDetailBean orederCreateDetailBean) {//在ui线程执行
        showProgressBar(false);
        showOrderDialog("", "您有未支付的订单", "前往", "忽略", false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(BaseActivity.this, PayOrderActivity.class);
                intent.putExtra(PAY_FLAGS, "1");
                startActivity(intent);
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });

    }

    private void registerReceiver() {
        filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        myReceiver = new ConnectionChangeReceiver();
    }

    //检查GPS权限
    protected void checkGPSPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_CALL_PHONE);
        } else {
            LogUtils.e("gn", "权限已经开了");
            reGetLocation();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CALL_PHONE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                reGetLocation();
            } else {
                showButtomToast("权限被拒绝");
                openSetingActivity();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //打开设置页面
    public void openSetingActivity() {
        showDialog("", "定位服务已关闭\n请到设置中开启【电动侠出行】定位服务，以便更方便用车", "设置", "取消", false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Uri packageURI = Uri.parse("package:" + BaseActivity.this.getPackageName());
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
                startActivity(intent);
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
        showButtomToast("打开设置");
    }

    /**
     * 使用外部应用导航
     *
     * @param startLat
     * @param startLng
     * @param startName
     * @param endLat
     * @param endLng
     * @param endName
     * @param navMode
     */
    protected void navigationLocationApp(double startLat, double startLng, String startName, double endLat, double endLng, String endName, int navMode) {
        if (isInstalled(this, "com.baidu.BaiduMap")) {
//            dbNavigationRecord(startLat, startLng, startName, endLat, endLng, endName);
            try {
                String mode = "driving";
                switch (navMode) {
                    case 1:
                        mode = "transit";
                        break;
                    case 2:
                        mode = "driving";
                        break;
                    case 3:
                        mode = "walking";
                        break;
                }
                Intent intent7 = Intent.getIntent("intent://map/direction?origin=latlng:" + startLat + "," + startLng + "| name:" + startName + "&destination=latlng:" + endLat + "," + endLng + "|name:" + endName + "&mode=" + mode + "&src=yourCompanyName|letravel#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end");
                startActivity(intent7);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            showButtomToast("未安装百度地图");
        }
    }

    /**
     * 检查手机上是否安装了指定的软件
     *
     * @param context
     * @param packageName：应用包名
     * @return
     */
    public static boolean isInstalled(Context context, String packageName) {
        final PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> packageInfos = packageManager.getInstalledPackages(0);
        if (packageInfos != null) {
            for (int i = 0; i < packageInfos.size(); i++) {
                String pkName = packageInfos.get(i).packageName;
                if (pkName.equals(packageName))
                    return true;
            }
        }
        return false;
    }

    /**
     * 数据库操作（导航记录）
     */
    public void dbNavigationRecord(double startLat, double startLng, String startName, double endLat, double endLng, String endName) {
        try {//导航数据库
            NavigationRecord navigationRecord = new NavigationRecord();
            navigationRecord.setStartAddress("开始地点:" + startName + startLat + "," + startLng);
            navigationRecord.setEndAddress("目的地:" + endName + endLat + "," + endLng);
            navigationRecord.setResgisterTime(MyDateTimeUtil.getDate());
            navigationRecord.setUserName("token:" + (LeTravelApplication.getSharePreferenceInstance(this).getToken() == null ? "" : LeTravelApplication.getSharePreferenceInstance(this).getToken()) + "手机号：" + (LeTravelApplication.getSharePreferenceInstance(this).getUserNumber() == null ? "" : LeTravelApplication.getSharePreferenceInstance(this).getUserNumber()));
            boolean b = DaoOperationUtils.insert(navigationRecord);
            if (b) {
                List<NavigationRecord> list = DaoOperationUtils.queryAll();
                if (list != null) {
                    if (list.size() > 0) {
                        String navRecord = list.get(list.size() - 1).toString();
                        DebugMessage.put(this, navRecord, "eManNavigationRecord");
                    }
                }
            } else {
            }
        } catch (Exception e) {
            LogUtils.e("gn", "数据库" + e.toString());
        }
    }

    @Override
    public void lockAll(int mode) {
        if (mode == 1) {
            showProgressBar(false);
        }
        showOkDialog("账号已被冻结，请联系客服", "400-888-1212");
    }

    @Override
    public void lockHalf(int mode) {
        if (mode == 1) {
            showProgressBar(false);
        }
        showOkDialog("暂不支持下单，请联系客服", "400-888-1212");
    }
}
