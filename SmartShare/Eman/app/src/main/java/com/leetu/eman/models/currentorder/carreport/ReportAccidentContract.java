package com.leetu.eman.models.currentorder.carreport;/**
 * Created by gn on 2016/10/10.
 */


import com.leetu.eman.base.BaseContract;

import java.util.List;

/**
 * created by neo on 2016/10/10 15:47
 */
public interface ReportAccidentContract {
    interface View extends BaseContract {
        void showUpData();
    }

    interface UserAction {
        void upData(String carId, String orderId, String carLose, List<String> imgUrls);
    }
}
