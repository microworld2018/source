package com.leetu.eman.models.confirmorder.beans;/**
 * Created by jyt on 2016/9/23.
 */

/**
 * created by neo on 2016/9/23 16:43
 */
public class DefaultReturnCarPoint {
    String id;
    String address;
    String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
