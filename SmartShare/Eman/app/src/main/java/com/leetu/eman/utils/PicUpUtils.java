package com.leetu.eman.utils;/**
 * Created by gn on 2016/10/9.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.leetu.eman.beans.PicFileUrlBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.urls.URLS;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/**
 * created by neo on 2016/10/9 16:01
 * 图片上传工具
 */
public class PicUpUtils {
    private PicListener picListener;
    private Context context;
    private int position;
    private StringBuffer stringBuffer = new StringBuffer();

    public void upPic(Context context, List<String> filePaths) {
        this.context = context;
        save(filePaths);
    }

    //顺序
    public void upLinePic(Context context, List<String> filePaths) {
        this.context = context;
        saveLine(filePaths);
    }

    //压缩图片
    private void save(List<String> picPths) {
        List<String> listSmall = new ArrayList<>();
        if (picPths != null && picPths.size() > 0) {
            for (int i = 0; i < picPths.size(); i++) {
                try {
                    File f = new File(picPths.get(i));
                    Bitmap bm = PictureUtil.getSmallBitmap(picPths.get(i));
                    FileOutputStream fos = new FileOutputStream(new File(
                            PictureUtil.getAlbumDir(), "small_" + f.getName()));
                    bm.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                    listSmall.add(PictureUtil.getAlbumDir().toString().trim() + "/" + "small_" + f.getName());
                } catch (Exception e) {
                    Log.e("gn", "error", e);
                }
            }
            upData(listSmall);  //完成
        } else {
            // 没图片
        }
    }

    //顺序压缩图片
    private void saveLine(List<String> picPths) {
        List<String> listSmall = new ArrayList<>();
        if (picPths != null && picPths.size() > 0) {
            for (int i = 0; i < picPths.size(); i++) {
                try {
                    File f = new File(picPths.get(i));
                    Bitmap bm = PictureUtil.getSmallBitmap(picPths.get(i));
                    FileOutputStream fos = new FileOutputStream(new File(
                            PictureUtil.getAlbumDir(), "small_" + f.getName()));
                    bm.compress(Bitmap.CompressFormat.JPEG, 40, fos);
                    listSmall.add(PictureUtil.getAlbumDir().toString().trim() + "/" + "small_" + f.getName());
                } catch (Exception e) {
                    Log.e("gn", "error", e);
                }
            }
            lineUpData(listSmall, 0);  //完成
        } else {
            // 没图片
        }
    }

    //顺序操作
    public void lineUpData(final List<String> smallFiles, final int index) {
        if (NetworkHelper.isNetworkConnect(context)) {
            LogUtils.e("gn", "图片上传");
            HttpEngine
                    .postFile()
                    .url(URLS.IMG_UP)
                    .addParam("type", "single")
                    .addParam("userId", "systemTest")
                    .addParam("quality", "")
                    .addParam("useType", "1")
                    .addFile("filedata", smallFiles.get(index))

                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e) {
                            if (picListener != null) {
                                picListener.picFail(e.toString());
                            }
                            LogUtils.e("gn", "失败" + e.toString());
                        }

                        @Override
                        public void onResponse(String response) {
                            Log.e("gn", response + "测试图片接口");
                            try {
                                PicFileUrlBean picFileUrlBean = JsonParser.getParsedData(response, PicFileUrlBean.class);
                                if (picFileUrlBean.isState()) {
                                    stringBuffer.append(picFileUrlBean.getUrl());
                                    position++;
                                    if (position == smallFiles.size()) {
                                        LogUtils.e("gn", "position" + position);
                                        if (picListener != null) {
                                            picListener.picOk(stringBuffer.toString().trim());
                                        }
                                    } else {
                                        stringBuffer.append("|");
                                        lineUpData(smallFiles, position);
                                    }

                                }
                            } catch (Exception e) {
                            }

                        }
                    });
        } else {
            if (picListener != null) {
                picListener.picNetError();
            }
        }
    }

    void upData(final List<String> smallFiles) {
        final StringBuffer stringBuffer = new StringBuffer();

        for (int i = 0; i < smallFiles.size(); i++) {
            if (NetworkHelper.isNetworkConnect(context)) {
                LogUtils.e("gn", "图片上传");
                HttpEngine
                        .postFile()
                        .url(URLS.IMG_UP)
                        .addParam("type", "single")
                        .addParam("userId", "systemTest")
                        .addParam("quality", "")
                        .addParam("useType", "1")
                        .addFile("filedata", smallFiles.get(i))

                        .execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Exception e) {
                                if (picListener != null) {
                                    picListener.picFail(e.toString());
                                }
                                LogUtils.e("gn", "失败" + e.toString());
                            }

                            @Override
                            public void onResponse(String response) {
                                Log.e("gn", response + "测试图片接口");
                                try {
                                    PicFileUrlBean picFileUrlBean = JsonParser.getParsedData(response, PicFileUrlBean.class);
                                    if (picFileUrlBean.isState()) {
                                        stringBuffer.append(picFileUrlBean.getUrl());
                                        position++;
                                        if (position == smallFiles.size()) {
                                            LogUtils.e("gn", "position" + position);
                                            if (picListener != null) {
                                                picListener.picOk(stringBuffer.toString().trim());
                                            }
                                        } else {
                                            stringBuffer.append("|");
                                        }

                                    }
                                } catch (Exception e) {
                                }

                            }
                        });
            } else {
                if (picListener != null) {
                    picListener.picNetError();
                }
            }
        }
    }

    public void setPicListener(PicListener picListener) {
        this.picListener = picListener;
    }

    public interface PicListener {
        void picOk(String picUrls);

        void picFail(String msg);

        void picNetError();
    }
}
