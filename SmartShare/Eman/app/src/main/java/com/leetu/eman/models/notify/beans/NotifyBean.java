package com.leetu.eman.models.notify.beans;

/**
 * 作者：尚硅谷-lvjunfeng on 2016/11/27 22:15
 * 微信：825801863
 * QQ号：825801863
 * 作用：xxxx
 */
public class NotifyBean {
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
