package com.leetu.eman.models.currentorder.carreport;/**
 * Created by gn on 2016/10/9.
 */

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.utils.PicUpUtils;

import java.util.List;


/**
 * created by neo on 2016/10/9 15:45
 */
public class ReportCarPresenter extends BasePresenter implements ReportCarContract.UserAction, PicUpUtils.PicListener {
    private Context context;
    private ReportCarContract.View rePortCarListener;
    private PicUpUtils picUpUtils;
    private String orderId, carId, isOk, carFace, carClean;

    public ReportCarPresenter(Context context, ReportCarContract.View rePortCarListener) {
        this.context = context;
        this.rePortCarListener = rePortCarListener;
        picUpUtils = new PicUpUtils();

    }

    @Override
    public void upData(String orderId, String carId, String isOk, String carFace, String carClean, List<String> picPaths) {
        this.orderId = orderId;
        this.carId = carId;
        this.isOk = isOk;
        this.carFace = carFace;
        this.carClean = carClean;
        picUpUtils.setPicListener(this);
        picUpUtils.upPic(context, picPaths);
    }

    @Override
    public void picOk(String pics) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.CAR_COMMENT)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .addParam("carId", carId)
                    .addParam("isOk", isOk)
                    .addParam("carFace", carFace)
                    .addParam("carClean", carClean)
                    .addParam("carImg", pics)
                    .execute(new HttpEngine.ResponseCallback() {
                                 @Override
                                 public void onResponse(ResponseStatus response) {
                                     LogUtils.e("gn", "提交评价" + response.getData());
                                 }

                                 @Override
                                 public void onFailure(Exception error) {
                                     rePortCarListener.timeOutFail();
                                 }
                             }
                    );
        } else {
            rePortCarListener.contentFail();
        }
    }

    @Override
    public void picFail(String msg) {
        rePortCarListener.showFail(msg);
    }

    @Override
    public void picNetError() {
        rePortCarListener.contentFail();
    }
}
