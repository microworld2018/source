package com.leetu.eman.models.activity.bean;

/**
 * Created by lvjunfeng on 2017/6/14.
 */

public class WebDetailsBean {
    private String title;
    private String shareIco;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShareIco() {
        return shareIco;
    }

    public void setShareIco(String shareIco) {
        this.shareIco = shareIco;
    }
}
