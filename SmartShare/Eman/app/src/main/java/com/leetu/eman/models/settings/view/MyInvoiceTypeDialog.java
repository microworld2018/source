package com.leetu.eman.models.settings.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.leetu.eman.R;

/**
 * Created by Administrator on 2016/11/9.
 */

public class MyInvoiceTypeDialog extends Dialog implements View.OnClickListener {

    private View baseView;
    private Context context;
    private TextView tvPersonal, tvCompany;
    private TypeListener typeListener;

    public MyInvoiceTypeDialog(Context context) {
        super(context, R.style.selectorDialog);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        baseView = LayoutInflater.from(context).inflate(R.layout.dialog_type, null);
        setContentView(baseView);
        tvPersonal = (TextView) baseView.findViewById(R.id.tv_personal);
        tvCompany = (TextView) baseView.findViewById(R.id.tv_company);
        tvPersonal.setOnClickListener(this);
        tvCompany.setOnClickListener(this);

        Window dialogWindow = getWindow();

        dialogWindow.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        dialogWindow.setWindowAnimations(R.style.mystyle);  //添加动画
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();
        lp.width = (int) (d.widthPixels * 1);
        dialogWindow.setAttributes(lp);
    }

    public void setTypeListener(TypeListener typeListener) {
        this.typeListener = typeListener;
    }

    public interface TypeListener {
        void personal();

        void company();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_personal:
                if (typeListener != null) {
                    typeListener.personal();
                    MyInvoiceTypeDialog.this.cancel();
                }
                break;
            case R.id.tv_company:
                if (typeListener != null) {
                    typeListener.company();
                    MyInvoiceTypeDialog.this.cancel();
                }
                break;
        }
    }
}
