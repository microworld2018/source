package com.leetu.eman.mygreedao.utils;

import java.text.SimpleDateFormat;

/**
 * Created by Administrator on 2017/2/7.
 */

public class MyDateTimeUtil {
    public static String getDate() {
        SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd    hh:mm:ss");
        String date = sDateFormat.format(new java.util.Date());
        return date;
    }
}

