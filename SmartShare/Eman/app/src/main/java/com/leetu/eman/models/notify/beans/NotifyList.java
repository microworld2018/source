package com.leetu.eman.models.notify.beans;

import java.util.List;

/**
 * Created by Administrator on 2016/12/2.
 */

public class NotifyList {
    List<NotifyBean> orders;

    public List<NotifyBean> getData() {
        return orders;
    }

    public void setData(List<NotifyBean> data) {
        this.orders = data;
    }
}
