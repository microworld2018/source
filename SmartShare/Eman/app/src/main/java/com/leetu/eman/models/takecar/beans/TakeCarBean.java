package com.leetu.eman.models.takecar.beans;/**
 * Created by jyt on 2016/9/27.
 */


import com.leetu.eman.models.confirmorder.beans.CarDetailBean;

import java.io.Serializable;

/**
 * created by neo on 2016/9/27 15:50
 */
public class TakeCarBean implements Serializable {
    String orderId;
    String startTimingTime;
    int timing;
    CarDetailBean car;
    ReCarDotBean dot;
    int orderType;
    int isexist;
    int residualTime;

    public int getResidualTime() {
        return residualTime;
    }

    public void setResidualTime(int residualTime) {
        this.residualTime = residualTime;
    }

    public int getTiming() {
        return timing;
    }

    public void setTiming(int timing) {
        this.timing = timing;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public int getIsexist() {
        return isexist;
    }

    public void setIsexist(int isexist) {
        this.isexist = isexist;
    }

    public CarDetailBean getCar() {
        return car;
    }

    public void setCar(CarDetailBean car) {
        this.car = car;
    }

    public ReCarDotBean getDot() {
        return dot;
    }

    public void setDot(ReCarDotBean dot) {
        this.dot = dot;
    }


    public String getStartTimingTime() {
        return startTimingTime;
    }

    public void setStartTimingTime(String startTimingTime) {
        this.startTimingTime = startTimingTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "TakeCarBean{" +
                "orderId='" + orderId + '\'' +
                ", startTimingTime='" + startTimingTime + '\'' +
                ", timing=" + timing +
                ", car=" + car +
                ", dot=" + dot +
                ", orderType=" + orderType +
                ", isexist=" + isexist +
                ", residualTime=" + residualTime +
                '}';
    }
}
