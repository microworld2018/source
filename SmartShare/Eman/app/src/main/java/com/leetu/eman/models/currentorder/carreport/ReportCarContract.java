package com.leetu.eman.models.currentorder.carreport;/**
 * Created by gn on 2016/10/9.
 */


import com.leetu.eman.base.BaseContract;

import java.util.List;

/**
 * created by neo on 2016/10/9 15:45
 */
public interface ReportCarContract {
    interface View extends BaseContract {
        void showUpData();
    }

    interface UserAction {
        void upData(String orderId, String carId, String isOk, String carFace, String carClean, List<String> picPaths);
    }
}
