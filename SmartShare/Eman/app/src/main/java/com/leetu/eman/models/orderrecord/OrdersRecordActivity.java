package com.leetu.eman.models.orderrecord;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gn.myanimpulltorefreshlistview.pulltoimpl.PullToRefreshBase;
import com.gn.myanimpulltorefreshlistview.views.PullToRefreshListView;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.confirmorder.ConfirmOrderActivity;
import com.leetu.eman.models.currentorder.CurrentOrderActivity;
import com.leetu.eman.models.orderrecord.adapters.OrderRecordAdapter;
import com.leetu.eman.models.orderrecord.beans.OrderRecordBean;
import com.leetu.eman.models.takecar.CancelOrderActivity;
import com.leetu.eman.models.takecar.TakeCarActivity;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

import static com.leetu.eman.models.takecar.CancelOrderActivity.AGAIN;
import static com.leetu.eman.models.takecar.CancelOrderActivity.GO_HOME;


/**
 * 订单记录
 */
public class OrdersRecordActivity extends BaseActivity implements OrderRecordContract.View, View.OnClickListener {

    private TitleBar titleBar;
    private OrdersRecordPresenter ordersRecordPresenter;
    private OrderRecordAdapter orderRecordAdapter;
    private List<OrderRecordBean> listAll;
    private PullToRefreshListView pullToRefreshListView;
    private ListView mListView;
    private LinearLayout listEmptyView;
    private TextView noneRecord;

    public static final int CANCEL_ORDER_CODE = 2000;
    public static final String ORDER_ID = "orderId";
    public static final String ORDER_PAYSTATUS = "order_paystatus";
    private boolean isFrist = true;
    private OrderRecordBean bean = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders_record);
        initView();
        loadData();
    }

    protected void initView() {
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.lv_orderrecord);
        mListView = pullToRefreshListView.getRefreshableView();
        titleBar = (TitleBar) findViewById(R.id.title_order);
        listEmptyView = (LinearLayout) findViewById(R.id.list_emptyview);
        noneRecord = (TextView) findViewById(R.id.none_record);


        listAll = new ArrayList<>();
        ordersRecordPresenter = new OrdersRecordPresenter(this, this);
        orderRecordAdapter = new OrderRecordAdapter(this, listAll);

        titleBar.setTitle("订单记录");
        titleBar.setLeftClickListener(this);
        mListView.setDivider(getResources().getDrawable(R.color.line));
        mListView.setDividerHeight(1);

        noneRecord.setText("暂无订单");
        mListView.setEmptyView(listEmptyView);
        mListView.setAdapter(orderRecordAdapter);


        pullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                ordersRecordPresenter.load(false, false);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                ordersRecordPresenter.load(true, false);
            }
        });

        // state 0 代表已经取消订单  1 预约成功  2预约超时 3 租车中 4 订单完成  paystatus 0未支付  1已支付
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            private OrderRecordBean orderRecordBean;

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                position--;  //由于下拉刷新添加头时造成的下角标越界
                orderRecordBean = listAll.get(position);
                if (orderRecordBean != null) {
                    if (orderRecordBean.getState().equals("0") || orderRecordBean.getState().equals("2")) {
                        Intent intent = new Intent(OrdersRecordActivity.this, CancelOrderActivity.class);
                        startActivityForResult(intent, CANCEL_ORDER_CODE);
                    } else if (orderRecordBean.getState().equals("4")) {
                        Intent intent = new Intent(OrdersRecordActivity.this, OrderDetailActivity.class);
                        intent.putExtra(ORDER_ID, orderRecordBean.getOrderId());
                        intent.putExtra(ORDER_PAYSTATUS, orderRecordBean.getPayStatus());
                        startActivity(intent);
                    } else if (orderRecordBean.getState().equals("1")) {
                        Intent intent = new Intent(OrdersRecordActivity.this, TakeCarActivity.class);
                        startActivityForResult(intent, ConfirmOrderActivity.REQUEST);

                    } else if (orderRecordBean.getState().equals("3")) {
                        Intent intent = new Intent(OrdersRecordActivity.this, CurrentOrderActivity.class);
                        startActivity(intent);

                    }
                }
            }
        });

        orderRecordAdapter.setOnTvPayListener(new onTvPayClickListener());
    }


    protected void loadData() {
        showLoading();
        ordersRecordPresenter.load(false, isFrist);
    }

    @Override
    protected void onRetryLoadData() {
        super.onRetryLoadData();
        showProgressBar(true);
        ordersRecordPresenter.load(true, isFrist);
    }

    @Override
    public void showLoad(List<OrderRecordBean> list, boolean isUp) {
        showContent();
        showProgressBar(false);
        isFrist = false;
        if (!isUp) {
            listAll.clear();
            bean = null;
        }

        if (list != null) {
            confirmOrderTpye(list, isUp);
        }
        orderRecordAdapter.notifyDataSetChanged();
    }

    // state 0 代表已经取消订单  1 预约成功  2预约超时 3 租车中 4 订单完成  paystatus 0未支付  1已支付
    private void confirmOrderTpye(List<OrderRecordBean> list, boolean isUp) {

        for (int i = 0; i < list.size(); i++) {
            OrderRecordBean orderRecordBean = list.get(i);
            if (orderRecordBean.getState().equals("1") || orderRecordBean.getState().equals("3")) {
                bean = orderRecordBean;
                listAll.add(0, bean);//放在position==0
                list.remove(i);
                i--;
            }
        }

        if (list.size() > 0) {
            listAll.addAll(list);
        }

        pullToRefreshListView.onRefreshComplete();
    }


    class onTvPayClickListener implements OrderRecordAdapter.TvPayListener {
        @Override
        public void onPayClickListener() {
            OrdersRecordActivity.this.finish();
        }

        @Override
        public void onAgainClickListener() {
//                Intent intent = new Intent(OrdersRecordActivity.this, ConfirmOrderActivity.class);
//                intent.putExtra(MainActivity.DOT_ID, "--------------");//添加网店ID
//                startActivity(intent);

            if (bean != null) {
                showButtomToast("您当前有进行中的订单，无法再次预定");
            } else {
//                OverLayBean overLayBean = new OverLayBean();
//                    overLayBean.setDotId(orderRecordBean.get);
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable(MainActivity.DOT_ID, overLayBean);
//                    Intent intent = new Intent(OrdersRecordActivity.this, ConfirmOrderActivity.class);
//                    intent.putExtras(bundle);

//                    Intent intent = new Intent();
//                    setResult(AGAIN, intent);
                OrdersRecordActivity.this.finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CANCEL_ORDER_CODE) {
            if (resultCode == AGAIN) {

//                Intent intent = new Intent(OrdersRecordActivity.this, ConfirmOrderActivity.class);
//                intent.putExtra(MainActivity.DOT_ID, "--------------");//添加网店ID
//                startActivity(intent);
//
                if (bean != null) {
                    showButtomToast("您当前有进行中的订单，无法再次预定");
                } else {
                    OrdersRecordActivity.this.finish();
                }

            } else if (resultCode == GO_HOME) {
                OrdersRecordActivity.this.finish();
            }
        }
        if (requestCode == ConfirmOrderActivity.REQUEST) {
            if (resultCode == ConfirmOrderActivity.RESULT_FINISH) {
                LogUtils.e("gn", "取消订单返回");
                pullToRefreshListView.setRefreshing(true);
            } else if (resultCode == CancelOrderActivity.AGAIN) {
                OrdersRecordActivity.this.finish();
            } else if (resultCode == CancelOrderActivity.GO_HOME) {
                OrdersRecordActivity.this.finish();
            }else if(resultCode == TakeCarActivity.AUTO_CANCLE){
                LogUtils.e("gn", "预约超时取消订单返回");
                pullToRefreshListView.setRefreshing(true);
            }
        }


    }


    @Override
    public void showFail(String message) {
        super.showFail(message);
        pullToRefreshListView.onRefreshComplete();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                OrdersRecordActivity.this.finish();
                break;
        }
    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        ordersRecordPresenter.load(false, true);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(OrdersRecordActivity.class);
    }
}
