package com.leetu.eman.views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leetu.eman.R;


/**
 * Created by 郭宁 on 2015/5/28.
 */
public class SelectorPayDialog extends Dialog implements View.OnClickListener {

    private final Context context;
    private TextView btnAliPay, btnWxPay;
    private LinearLayout btnCancelPay;
    private ClickListenerInterface clickListenerInterface;

    public SelectorPayDialog(Context context) {

        super(context, R.style.selectorDialog);
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
    }

    private void initView() {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.select_pay_pop, null);
        setContentView(view);

        btnAliPay = (TextView) findViewById(R.id.btn_alipay);
        btnWxPay = (TextView) findViewById(R.id.btn_wxpay);
        btnCancelPay = (LinearLayout) findViewById(R.id.btn_cancel_pay);
        //设置按钮监听
        btnAliPay.setOnClickListener(this);
        btnWxPay.setOnClickListener(this);
        btnCancelPay.setOnClickListener(this);
        Window dialogWindow = getWindow();

        dialogWindow.setGravity(Gravity.BOTTOM);  //此处可以设置dialog显示的位置
        dialogWindow.setWindowAnimations(R.style.mystyle);  //添加动画
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics();
        lp.width = (int) (d.widthPixels * 1);
        dialogWindow.setAttributes(lp);
    }

    public void setClicklistener(ClickListenerInterface clickListenerInterface) {
        this.clickListenerInterface = clickListenerInterface;
    }

    public void goneCancel() {
        if (btnCancelPay != null) {
            btnCancelPay.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_alipay:
                clickListenerInterface.aliPay();
                break;
            case R.id.btn_wxpay:
                clickListenerInterface.wxPay();
                break;
            case R.id.btn_cancel_pay:
                clickListenerInterface.cancelPay();
                break;
        }
        SelectorPayDialog.this.cancel();
    }

    public interface ClickListenerInterface {
        void aliPay();

        void wxPay();

        void cancelPay();
    }

}
