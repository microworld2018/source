package com.leetu.eman.models.orderrecord.beans;

import java.io.Serializable;

/**
 * Created by jyt on 2016/9/18.
 */

public class OrderRecordBean implements Serializable {
    String address;
    String end_time;
    String begin_time;
    String orderId;
    String vehicle_plate_id;
    String name;
    String state;
    String payStatus;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getBegin_time() {
        return begin_time;
    }

    public void setBegin_time(String begin_time) {
        this.begin_time = begin_time;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getVehicle_plate_id() {
        return vehicle_plate_id;
    }

    public void setVehicle_plate_id(String vehicle_plate_id) {
        this.vehicle_plate_id = vehicle_plate_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }
}
