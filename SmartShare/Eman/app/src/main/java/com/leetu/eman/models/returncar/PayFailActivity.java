package com.leetu.eman.models.returncar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.views.TitleBar;


/**
 * Created by Administrator on 2016/12/8.
 */

public class PayFailActivity extends BaseActivity implements View.OnClickListener {
    private Button btRePay;
    private TitleBar titleBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_fail);
        initView();
    }

    private void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_payfail);
        btRePay = (Button) findViewById(R.id.bt_repay);

        btRePay.setOnClickListener(this);
        titleBar.setTitle("支付失败");
        titleBar.hideLeftLayout();
    }

    @Override
    public void onClick(View v) {
        PayFailActivity.this.finish();
    }

}
