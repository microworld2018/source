package com.leetu.eman.models.returncar;

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;

/**
 * Created by lvjunfeng on 2017/1/10.
 */

public class ReturnCarSuccessPresenter extends BasePresenter implements ReturnCarSuccessContract.UserAction {
    private final Context context;
    private final ReturnCarSuccessContract.View returnSuccessListener;

    public ReturnCarSuccessPresenter(Context context, ReturnCarSuccessContract.View returnSuccessListener) {
        this.context = context;
        this.returnSuccessListener = returnSuccessListener;
    }

    @Override
    public void goShare(final boolean isWxFriend) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.SHARE_COUPON)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            LogUtils.e("lv", "获得优惠券" + response.getResultCode());
                            if (response.getResultCode() == 200) {
                                returnSuccessListener.shareSuccess(isWxFriend);
                            } else {
                                if (response.getResultCode() == 206) {
                                    returnSuccessListener.showFail("登录失效，请重新登录！");
                                } else {
                                    returnSuccessListener.showFail(response.getResultMsg());
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            returnSuccessListener.timeOutFail();
                        }
                    });
        } else {
            returnSuccessListener.showFail("网络异常");
        }
    }
}
