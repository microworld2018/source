package com.leetu.eman.models.deposit;

import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.deposit.bean.DepositRechargeBean;
import com.leetu.eman.models.deposit.bean.DepositWxPayBean;
import com.leetu.eman.models.returncar.beans.PayBean;

/**
 * Created by lvjunfeng on 2017/4/5.
 */

public interface DepositRechargeContract {
    interface View extends BaseContract {
        void showLoad(DepositRechargeBean amountBean);

        void showAliPay(PayBean payBean);

        void showWxPay(DepositWxPayBean wxPayBean);

//        void showPaySuccess();
    }

    interface UserAction {
        void loadData(boolean isFrist);

        void loadPay(String type);

//        void paySuccess(String resultStatus, String result);
    }
}
