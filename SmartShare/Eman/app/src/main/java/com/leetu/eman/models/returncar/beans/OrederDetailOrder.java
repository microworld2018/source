package com.leetu.eman.models.returncar.beans;/**
 * Created by gn on 2016/10/10.
 */

/**
 * created by neo on 2016/10/10 14:48
 */
public class OrederDetailOrder {
    String alltime;
    int allMilePrice;
    String orderId;
    int allPrice;
    int allMile;
    String carId;
    int allTimePrice;
    int allminutes;

    public String getAlltime() {
        return alltime;
    }

    public void setAlltime(String alltime) {
        this.alltime = alltime;
    }

    public int getAllMilePrice() {
        return allMilePrice;
    }

    public void setAllMilePrice(int allMilePrice) {
        this.allMilePrice = allMilePrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(int allPrice) {
        this.allPrice = allPrice;
    }

    public int getAllMile() {
        return allMile;
    }

    public void setAllMile(int allMile) {
        this.allMile = allMile;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public int getAllTimePrice() {
        return allTimePrice;
    }

    public void setAllTimePrice(int allTimePrice) {
        this.allTimePrice = allTimePrice;
    }

    public int getAllminutes() {
        return allminutes;
    }

    public void setAllminutes(int allminutes) {
        this.allminutes = allminutes;
    }
}
