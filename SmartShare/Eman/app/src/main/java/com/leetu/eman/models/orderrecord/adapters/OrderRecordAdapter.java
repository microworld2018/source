package com.leetu.eman.models.orderrecord.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.models.orderrecord.beans.OrderRecordBean;
import com.leetu.eman.models.returncar.PayOrderActivity;

import java.util.List;

import static com.leetu.eman.base.BaseActivity.PAY_FLAGS;


/**
 * Created by jyt on 2016/9/18.
 */

public class OrderRecordAdapter extends BaseAdapter {
    private Context context;
    private List<OrderRecordBean> list;
// state 0 代表已经取消订单  1 预约成功  2预约超时 3 租车中 4 订单完成  paystatus 0未支付  1已支付

    private static final int CURRENT_ORDER = 0;
    private static final int ORDER_RECODER = 1;

    public OrderRecordAdapter(Context context, List<OrderRecordBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getItemViewType(int position) {
        int state = 0;
        OrderRecordBean orderRecordBean = list.get(position);
        if ("1".equals(orderRecordBean.getState()) || "3".equals(orderRecordBean.getState())) {
            state = CURRENT_ORDER;
            return state;
        } else {
            state = ORDER_RECODER;
            return state;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewCurrentHolder viewCurrHolder = null;
        ViewHolder viewHolder = null;
        int itemViewType = getItemViewType(position);
        final OrderRecordBean orderRecordBean = list.get(position);

        if (convertView == null) {
            if (itemViewType == CURRENT_ORDER) {
                viewCurrHolder = new ViewCurrentHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.current_order, null);
                viewCurrHolder.tvCarId = (TextView) convertView.findViewById(R.id.car_number);
                viewCurrHolder.tvCarType = (TextView) convertView.findViewById(R.id.car_name);
                convertView.setTag(viewCurrHolder);
            } else if (itemViewType == ORDER_RECODER) {
                viewHolder = new ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.item_orderrecord, null);
                viewHolder.tvTime = (TextView) convertView.findViewById(R.id.tv_order_time);
                viewHolder.tvCarPosition = (TextView) convertView.findViewById(R.id.tv_order_cartposition);
                viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tv_order_status);
                viewHolder.btnOrReOrder = (TextView) convertView.findViewById(R.id.btn_payorreorder);
                viewHolder.tvCarId = (TextView) convertView.findViewById(R.id.tv_order_cartid);
                viewHolder.tvCarType = (TextView) convertView.findViewById(R.id.tv_order_cartype);

                convertView.setTag(viewHolder);

            }

        } else {
            if (itemViewType == CURRENT_ORDER) {
                viewCurrHolder = (ViewCurrentHolder) convertView.getTag();
            } else if (itemViewType == ORDER_RECODER) {
                viewHolder = (ViewHolder) convertView.getTag();
            }
        }

        if (itemViewType == ORDER_RECODER) {
            viewHolder.btnOrReOrder.setTag(orderRecordBean);//也可通过设置Tag的方式

            viewHolder.btnOrReOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrderRecordBean bean = (OrderRecordBean) v.getTag();
                    if (bean.getState().equals("4")) {//前往主流层支付
                        Intent intent = new Intent(context, PayOrderActivity.class);
                        intent.putExtra(PAY_FLAGS, "1");
                        context.startActivity(intent);
                        if (tvPayListener != null) {
                            tvPayListener.onPayClickListener();
                        }
                    } else if (bean.getState().equals("0") || bean.getState().equals("2")) {//再次预约

                        if (tvPayListener != null) {
                            tvPayListener.onAgainClickListener();
                        }
                    }
                }
            });
        }

        if (itemViewType == CURRENT_ORDER) {
            bindCurrentData(orderRecordBean, itemViewType, viewCurrHolder);
        } else {
            bindData(orderRecordBean, itemViewType, viewHolder);
        }

        return convertView;
    }

    private void bindCurrentData(OrderRecordBean orderRecordBean, int itemViewType, ViewCurrentHolder viewCurrHolder) {
        viewCurrHolder.tvCarId.setText(orderRecordBean.getVehicle_plate_id());
        viewCurrHolder.tvCarType.setText(orderRecordBean.getName());

    }

    private void bindData(OrderRecordBean orderRecordBean, int itemViewType, ViewHolder viewHolder) {
        // state 0 代表已经取消订单  1 预约成功  2预约超时 3 租车中 4 订单完成  paystatus 0未支付  1已支付
        viewHolder.tvTime.setText(orderRecordBean.getBegin_time());
        viewHolder.tvCarId.setText(orderRecordBean.getVehicle_plate_id());
        viewHolder.tvCarType.setText(orderRecordBean.getName());
        viewHolder.tvCarPosition.setText(orderRecordBean.getAddress());
        if (orderRecordBean.getState().equals("0") || orderRecordBean.getState().equals("2")) {
            viewHolder.tvStatus.setText("已取消");
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.text_cancle));
            viewHolder.btnOrReOrder.setText("再次预约");
            viewHolder.btnOrReOrder.setBackgroundResource(R.drawable.btn_reorder_bg);
            viewHolder.btnOrReOrder.setVisibility(View.VISIBLE);

        } else if (orderRecordBean.getPayStatus().equals("0")) {
            viewHolder.tvStatus.setText("待付款");
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.main_red));
            viewHolder.btnOrReOrder.setText("立即支付");
            viewHolder.btnOrReOrder.setBackgroundResource(R.drawable.btn_pay_bg);
            viewHolder.btnOrReOrder.setVisibility(View.VISIBLE);

        } else if (orderRecordBean.getState().equals("4")) {
            viewHolder.tvStatus.setText("已完成");
            viewHolder.tvStatus.setTextColor(context.getResources().getColor(R.color.main));
            if (orderRecordBean.getPayStatus().equals("0")) {
                viewHolder.btnOrReOrder.setVisibility(View.VISIBLE);
            } else {
                viewHolder.btnOrReOrder.setVisibility(View.GONE);
            }
        }
    }


    class ViewHolder {
        TextView tvTime, tvCarType, tvCarId, tvCarPosition, tvStatus, btnOrReOrder;
    }

    class ViewCurrentHolder {
        TextView tvCarId, tvCarType;
    }

    public void setOnTvPayListener(TvPayListener tvPayListener) {
        this.tvPayListener = tvPayListener;
    }

    private TvPayListener tvPayListener;

    public interface TvPayListener {
        void onPayClickListener();

        void onAgainClickListener();

    }


}
