package com.leetu.eman.models.deposit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.models.deposit.bean.DepositRecordBean;

import java.util.List;

/**
 * Created by lvjunfeng on 2017/4/5.
 */

public class DepositRecordAdapter extends BaseAdapter {

    private final List<DepositRecordBean> datas;
    private final Context mContext;

    public DepositRecordAdapter(Context context, List<DepositRecordBean> mList) {
        this.mContext = context;
        this.datas = mList;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int position) {
        return datas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        DepositRecordBean depositRecordBean = datas.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_deposit_layout, null);
            viewHolder.tvRechangeDescrible = (TextView) convertView.findViewById(R.id.rechange_decrible);
            viewHolder.tvRechangeTime = (TextView) convertView.findViewById(R.id.rechang_time);
            viewHolder.tvRechangeMoney = (TextView) convertView.findViewById(R.id.rechange_money);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (0 == depositRecordBean.getType()) { //0充值 1扣款
            viewHolder.tvRechangeMoney.setText("+" + depositRecordBean.getMoney());
        } else if (1 == depositRecordBean.getType() || 2 == depositRecordBean.getType()) {
            viewHolder.tvRechangeMoney.setText("-" + depositRecordBean.getMoney());
        }
        viewHolder.tvRechangeDescrible.setText(depositRecordBean.getDesc());
        viewHolder.tvRechangeTime.setText(depositRecordBean.getCreateTime());

        return convertView;
    }

    class ViewHolder {
        TextView tvRechangeDescrible, tvRechangeTime, tvRechangeMoney;
    }
}
