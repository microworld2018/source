package com.leetu.eman.models.usecar;


import android.Manifest;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.baidu.location.BDLocation;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.map.UiSettings;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.utils.DistanceUtil;
import com.bumptech.glide.Glide;
import com.leetu.eman.R;
import com.leetu.eman.adapters.PupowindowAdapter;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.beans.CityBean;
import com.leetu.eman.models.activity.WebViewActivity;
import com.leetu.eman.models.balance.BalanceActivity;
import com.leetu.eman.models.confirmorder.ConfirmOrderActivity;
import com.leetu.eman.models.notify.NotifyActivity;
import com.leetu.eman.models.orderrecord.OrdersRecordActivity;
import com.leetu.eman.models.peccancyrecord.PeccancyRecordActivity;
import com.leetu.eman.models.personalcenter.PersonalCenterActivity;
import com.leetu.eman.models.personalcenter.beans.UserInfoBean;
import com.leetu.eman.models.rnauthentication.RNAuthenticationActivity;
import com.leetu.eman.models.settings.ComplaintActivity;
import com.leetu.eman.models.settings.SettingActivity;
import com.leetu.eman.models.updataservice.ADDownImageService;
import com.leetu.eman.models.updataservice.CheckUserOrderService;
import com.leetu.eman.models.updataservice.UpdateService;
import com.leetu.eman.models.usecar.bean.AdBean;
import com.leetu.eman.models.usecar.bean.OverLayBean;
import com.leetu.eman.mybaidumaputils.MyBaiduMapUtils;
import com.leetu.eman.net.ConnectionChangeReceiver;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.utils.StatusBarUtil;
import com.leetu.eman.views.TitleBar;
import com.umeng.analytics.MobclickAgent;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.leetu.eman.models.activity.WebViewActivity.TITLE_FLAG;
import static com.leetu.eman.models.activity.WebViewActivity.WEBVIEW_URL;
import static com.leetu.eman.models.takecar.CancelOrderActivity.AGAIN;


/**
 * 主要的地图页面，加侧滑菜单（现在所用的侧滑菜单不是很好，希望可以找到更好的)
 */
public class MainActivity extends BaseActivity implements View.OnClickListener, BaiduMap.OnMarkerClickListener, MainContract.View, BaseActivity.MyLocationLisenter, ConnectionChangeReceiver.OnNetListener {
    private TitleBar titleBar;
    private View menuView;
    private RelativeLayout rtMenuBalance, rtMenuAccident, rtMenuNotify, rtAuthentication, rtMenuOrderRecord, rtMenuInvoice, rtMenuSetting, rtMenuService, baiduAllview;
    private LinearLayout ltMenu, ltAddressMsg;
    private DrawerLayout drawerLayout;
    private Button btStartUseCar;
    private ImageView ivPersonalPic, ivNav, ivNoOpenCity;
    private List<OverLayBean> overLayBeanList;
    private List<MarkerOptions> markerOptionsList;
    private OverlayOptions overlayOptionLocation;
    private MainPresenter mainPresenter;
    private OverLayBean overLayBean;
    public static String DOT_ID = "dotId";
    private TextView tvUserNo, tvAddressName, tvAddress, tvDistance;
    private TextView tvMenuStatus;
    public static final int REQUEST = 1000;
    public static final int RESULT = 1001;
    private static final int ORDERREQUEST = 1002;
    private static final int REQUEST_SETTING = 1003;
    // 定义一个变量，来标识是否退出
    private static boolean isExit = false;


    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    isExit = false;
                    break;
            }
        }
    };
    // 定位相关
    private BDLocation myDBlocation;
    private MapView mMapView;
    private BaiduMap mBaiduMap;
    private ImageView ivLocation;
    private UiSettings mUiSettings;
    private LatLng meLatLng;//定位的经纬度
    //覆盖物相关
    private List<Marker> allMarkers;
    int index;//记录下
    boolean isFirst = true;//是不是第一次点击
    boolean isTouch = false;//是不是移动了地图
    private View overLayoutView;
    private MyBaiduMapUtils myBaiduMapUtils;
    private LinearLayout cityNoOpen;
    private PopupWindow mPopWindow;
    List<CityBean> list = new ArrayList();
    private View rootview;
    //线程的处理
    boolean isGetPointData = false;
    private MyCountDownTimer myTimer;
    private String filePath;
    private File file;
    private AdBean adBean;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myReceiver.setOnNetListener(this);
        initView();
    }

    protected void initView() {
        //菜单界面
        menuView = LayoutInflater.from(this).inflate(R.layout.left_menu, null);
        titleBar = (TitleBar) findViewById(R.id.title_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.dl_main);
        btStartUseCar = (Button) findViewById(R.id.bt_start_usecar);
        ltAddressMsg = (LinearLayout) findViewById(R.id.lt_main_addressmsg);
        tvAddress = (TextView) findViewById(R.id.tv_main_address);
        tvAddressName = (TextView) findViewById(R.id.tv_main_name);
        tvDistance = (TextView) findViewById(R.id.tv_main_distance);
        ivLocation = (ImageView) findViewById(R.id.iv_main_location);
        ivNav = (ImageView) findViewById(R.id.iv_main_nav);
        baiduAllview = (RelativeLayout) findViewById(R.id.baidu_allview);
        rtMenuBalance = (RelativeLayout) menuView.findViewById(R.id.rt_menu_balance);
        rtMenuAccident = (RelativeLayout) menuView.findViewById(R.id.rt_menu_accident);
        rtAuthentication = (RelativeLayout) menuView.findViewById(R.id.rt_rn_authentication);
        tvMenuStatus = (TextView) menuView.findViewById(R.id.tv_left_status);
        rtMenuOrderRecord = (RelativeLayout) menuView.findViewById(R.id.rt_menu_oredersrecord);
        rtMenuNotify = (RelativeLayout) menuView.findViewById(R.id.rt_menu_notify);


        rtMenuInvoice = (RelativeLayout) menuView.findViewById(R.id.rt_menu_invoice);
        rtMenuSetting = (RelativeLayout) menuView.findViewById(R.id.rt_menu_setting);
        rtMenuService = (RelativeLayout) menuView.findViewById(R.id.rt_menu_service);
        ivPersonalPic = (ImageView) menuView.findViewById(R.id.iv_personal_pic);
        tvUserNo = (TextView) menuView.findViewById(R.id.tv_user);
        ltMenu = (LinearLayout) findViewById(R.id.left_menu);
        mMapView = (MapView) findViewById(R.id.bmapView);

        //初始化城市切换弹窗
        rootview = LayoutInflater.from(MainActivity.this).inflate(R.layout.activity_main, null);
        View popupView = LayoutInflater.from(MainActivity.this).inflate(R.layout.citychange_popuplayout, null);
        GridView gridView = (GridView) popupView.findViewById(R.id.gridview);
        TextView btnCancle = (TextView) popupView.findViewById(R.id.btn_cancle);

        cityNoOpen = (LinearLayout) findViewById(R.id.city_noopen);
        ivNoOpenCity = (ImageView) findViewById(R.id.iv_noopen_city);
        Button gocomplain = (Button) findViewById(R.id.btn_gocomplain);
        mPopWindow = new PopupWindow(popupView,
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
        mPopWindow.setContentView(popupView);
        mPopWindow.setFocusable(false);
        mPopWindow.setOutsideTouchable(false);
        addCity();//模拟数据
        PupowindowAdapter pupowindowAdapter = new PupowindowAdapter(MainActivity.this, list);
        gridView.setAdapter(pupowindowAdapter);
        pupItemClickListener(gridView);
        mPopWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                setStatusBarColor(0);
            }
        });
        overLayBeanList = new ArrayList<>();
        allMarkers = new ArrayList<>();
        mainPresenter = new MainPresenter(this, this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        menuView.setLayoutParams(layoutParams);
        ltMenu.addView(menuView);
        mMapView.showZoomControls(false); //去掉放大缩小
        mBaiduMap = mMapView.getMap();
        View child = mMapView.getChildAt(1);//隐藏BaiduMap的Logo
        if (child != null && (child instanceof ImageView || child instanceof ZoomControls)) {
            child.setVisibility(View.INVISIBLE);
        }
        mUiSettings = mBaiduMap.getUiSettings();
        mUiSettings.setRotateGesturesEnabled(false);//关闭旋转
        mUiSettings.setOverlookingGesturesEnabled(false);//关闭俯视
        mBaiduMap.setBuildingsEnabled(false);
        titleBar.setTitle("电动侠出行");
        titleBar.setLeftIcon(R.mipmap.ic_personal_center);
        titleBar.setRightIcon(R.mipmap.ic_main_activity);
        titleBar.showRightLayout();
        titleBar.showRightIcon();
        titleBar.showCity();

        titleBar.setCityClickListener(this);
        ivNav.setOnClickListener(this);

        titleBar.setLeftClickListener(this);
        titleBar.setRightClickListener(this);
        ivLocation.setOnClickListener(this);
        btStartUseCar.setOnClickListener(StartUseCarListener);
        rtMenuBalance.setOnClickListener(this);
        rtMenuAccident.setOnClickListener(this);
        rtAuthentication.setOnClickListener(this);
        rtMenuOrderRecord.setOnClickListener(this);
        rtMenuNotify.setOnClickListener(this);
        rtMenuInvoice.setOnClickListener(this);
        ivPersonalPic.setOnClickListener(this);
        rtMenuSetting.setOnClickListener(this);
        rtMenuService.setOnClickListener(this);
        ltMenu.setOnClickListener(this);
        btnCancle.setOnClickListener(this);
        gocomplain.setOnClickListener(this);
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        drawerLayout.addDrawerListener(drawerListener);

        mBaiduMap.setOnMarkerClickListener(this);//设置覆盖物点击事件
        filePath = getExternalCacheDir().getPath() + "/eman/emanAd.jpg";
        loadData();
        initListener();
    }

    private void pupItemClickListener(GridView gridView) {
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == list.size() - 1) {
                } else {
                    mPopWindow.dismiss();
                    CityBean cityBean = list.get(position);
                    titleBar.setCity(cityBean.getName());
                    showCityNoOpen(cityBean.getName());//不是天津市 显示暂未开通
                    if (myDBlocation != null) {
                        LatLng selectCityLatLng = new LatLng(cityBean.getLat(), cityBean.getLon());
                        mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLngZoom(selectCityLatLng, 11f));
                        mainPresenter.getPoint("" + selectCityLatLng.latitude, "" + selectCityLatLng.longitude);
                        isTouch = true;
                    }
                }
            }
        });
    }

    @Override
    public void setstatusBar() {
        setStatusBarColor(0);
    }

    private void setStatusBarColor(int statusBarAlpha) {
        StatusBarUtil.setColorForDrawerLayout(this, (DrawerLayout) findViewById(R.id.dl_main), getResources()
                .getColor(R.color.main), statusBarAlpha);
    }

    //移动监听
    private void initListener() {
        mBaiduMap.setOnMapStatusChangeListener(new BaiduMap.OnMapStatusChangeListener() {
            @Override
            public void onMapStatusChangeStart(MapStatus mapStatus) {
                if (isGetPointData) {
                } else {
                    HttpEngine.cancelRequest(MainActivity.class);//停止线程
                    if (myTimer != null) {
                        myTimer.cancel();

                    }
                }
            }

            @Override
            public void onMapStatusChange(MapStatus mapStatus) {
            }

            @Override
            public void onMapStatusChangeFinish(MapStatus mapStatus) {
                isGetPointData = false;
                if (myTimer == null) {
                    myTimer = new MyCountDownTimer(mapStatus, 1 * 1000, 1000);
                } else {
                    myTimer.mapStatus = mapStatus;
                }
                myTimer.start();
            }
        });
    }

    //==计时器=====
    class MyCountDownTimer extends CountDownTimer {
        private MapStatus mapStatus;

        /**
         * @param millisInFuture    表示以毫秒为单位 倒计时的总数
         *                          <p>
         *                          例如 millisInFuture=1000 表示1秒
         * @param countDownInterval 表示 间隔 多少微秒 调用一次 onTick 方法
         *                          <p>
         *                          例如: countDownInterval =1000 ; 表示每1000毫秒调用一次onTick()
         */
        public MyCountDownTimer(MapStatus mapStatus, long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
            this.mapStatus = mapStatus;
        }

        @Override
        public void onFinish() {
            this.cancel();
            sendMoveAction(mapStatus);
        }

        @Override
        public void onTick(long millisUntilFinished) {
        }
    }

    //递归思想,解决延时问题
    private void sendMoveAction(final MapStatus mapStatus) {
        updateMapState(mapStatus);
    }

    private void updateMapState(MapStatus status) {
        LatLng mCenterLatLng = status.target;
        /**获取经纬度*/
        double lat = mCenterLatLng.latitude;
        double lng = mCenterLatLng.longitude;
        if (myDBlocation != null) {
            myDBlocation.setLatitude(lat);
            myDBlocation.setLongitude(lng);
        } else {
            showButtomToast("定位失败!");
            return;
        }

        isTouch = true;
        Geocoder geo = new Geocoder(this); //参数是context
        try {
            List<Address> list = geo.getFromLocation(lat, lng, 1);
            if (list != null && list.size() > 0) {
                Address address = null;
                address = list.get(0);
                if (address != null) {
                    if (address.getLocality() != null) {
                        titleBar.setCity(address.getLocality());
                        if ("天津市".equals(address.getLocality())) {
                            mainPresenter.getPoint("" + lat, "" + lng);
                        }
                        showCityNoOpen(address.getLocality());//x写死判断是不是天津显示暂未开通
                    }
                }
            } else {
                showNetError("");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //设置侧滑打开可以侧滑，关闭不可以从侧面滑出
    private DrawerLayout.DrawerListener drawerListener = new DrawerLayout.DrawerListener() {
        @Override
        public void onDrawerSlide(View drawerView, float slideOffset) {

        }

        @Override
        public void onDrawerOpened(View drawerView) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }

        @Override
        public void onDrawerStateChanged(int newState) {

        }
    };

    private void loadData() {
        this.setMyLocationLisenter(this);//注册定位
        showProgressBar(true);
        reGetLocation();
        mainPresenter.getUserInfo();
        mainPresenter.getAdInfo();
        checkUpdate();
    }

    /**
     * 检查更新
     */
    void checkUpdate() {
        Intent intent = new Intent(this, UpdateService.class);
        intent.putExtra(UpdateService.EXTRA_BACKGROUND, false);
        startService(intent);
    }

    /**
     * 检查用户是否有订单
     */
    public void checkService() {
        Intent intent = new Intent(this, CheckUserOrderService.class);
        startService(intent);
    }


    //回调定位
    @Override
    public void getLocation(BDLocation location) {
        ivLocation.setVisibility(View.VISIBLE);
        if (location.getLatitude() != 4.9E-324 && location.getLongitude() != 4.9E-324) {
            myDBlocation = location;
            if (location.getCity() != null) {
                titleBar.setCity(location.getCity());
                //天津市 目前写死
                checkCity(location);
            }
        }
    }

    private void checkCity(BDLocation location) {
        if ("天津市".equals(location.getCity().trim())) {
            setPic(myDBlocation);
            mainPresenter.getPoint("" + myDBlocation.getLatitude(), "" + myDBlocation.getLongitude());
        } else {
            showProgressBar(false);
            showCityNoOpen(location.getCity());
            checkService();
        }
    }

    @Override
    public void showPoint(List<OverLayBean> dots) {
        isGetPointData = true;
        overLayBeanList.clear();
        showProgressBar(false);
        showContent();
        isFirst = true;//每次重新加载数据都要重置回到第一次加载数据
        if (dots != null) {
            if (dots.size() != 0) {
                overLayBeanList.addAll(dots);
                initOverlay();//加覆盖物
            }
        }
        if (isTouch) {
        } else {
            checkService();
        }
    }

    @Override
    public void showUserInfo(UserInfoBean userInfoBean) {
        if (userInfoBean != null) {
            switch (userInfoBean.getState()) {
                case 1:
                    tvMenuStatus.setText("未认证");
                    tvMenuStatus.setTextColor(getResources().getColor(R.color.small_text));
                    break;
                case 2:
                    tvMenuStatus.setText("审核中");
                    tvMenuStatus.setTextColor(getResources().getColor(R.color.rnaing));
                    break;
                case 4:
                    tvMenuStatus.setText("未通过");
                    tvMenuStatus.setTextColor(getResources().getColor(R.color.red));
                    break;
                case 3:
                    tvMenuStatus.setText("已审核");
                    tvMenuStatus.setTextColor(getResources().getColor(R.color.main));
                    break;
                default:
                    tvMenuStatus.setText("");
                    break;
            }
        }
    }


    @Override
    public void showAd(AdBean bean) {
        this.adBean = bean;
        file = new File(filePath);
        LogUtils.e("lv", "showAd");
        if (adBean.getEndTime() == null) {//没有结束日期，已url判断是否下载
            if (adBean.getImgUrl().equals(LeTravelApplication.getSharePreferenceInstance(this).getAdImgUrl())) {//且上一次的图片与这次相同，无需再下载
                if (!file.exists()) {
                    saveDataDownLoad(false, adBean);
                } else {
                    return;
                }
            } else {
                saveDataDownLoad(true, adBean);
            }
        } else {//用结束日期来判断是否下载
            String endTime = formatTime(adBean.getEndTime());
            //获取当前活动有效时间
            long effectiveTime = Long.parseLong(endTime);
            //获取上次广告有效期
            String adLastSaveTime = LeTravelApplication.getSharePreferenceInstance(this).getAdTime();
            if (!TextUtils.isEmpty(adLastSaveTime)) {
                long adLastTime = Long.parseLong(formatTime(adLastSaveTime));
                if (adLastTime == effectiveTime) {
                    if (!file.exists()) {
                        saveDataDownLoad(false, adBean);
                    }
                } else {
                    //无效
                    saveDataDownLoad(true, adBean);
                }
            } else {
                if (file.exists()) {
                    saveDataDownLoad(true, adBean);
                } else {
                    saveDataDownLoad(false, adBean);
                }

            }
        }

    }


    private void saveDataDownLoad(boolean haveCacth, AdBean adBean) {
        if (haveCacth) {
            deleteADImg();
        }

        //下载新图片
        if (adBean.getImgUrl() != null) {
            ADDownImageService.startDownLoad(MainActivity.this, getExternalCacheDir().getPath() + "/eman/", adBean.getImgUrl(), adBean.getEndTime(), adBean.getContentUrl());
        }
    }

    private void deleteADImg() {

        //删除图片
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        ContentResolver mContentResolver = MainActivity.this.getContentResolver();
        String where = MediaStore.Images.Media.DATA + "='" + filePath + "'";
        mContentResolver.delete(uri, where, null);

        //发送广播
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(getExternalCacheDir().getPath() + "/eman/");
        Uri uri2 = Uri.fromFile(f);
        intent.setData(uri2);
        sendBroadcast(intent);
        LogUtils.e("lv", "删除原图");
    }

    @Override
    public void showFail(int code, String message) {
        if (code == 283) {//现在没有开屏活动
            LeTravelApplication.getSharePreferenceInstance(this).saveAdTime("");
            LeTravelApplication.getSharePreferenceInstance(this).saveAdDetailUrl("");
            LeTravelApplication.getSharePreferenceInstance(this).saveAdImgUrl("");
            deleteADImg();
        } else {
            super.showFail(code, message);
        }
    }

    //定位设置覆盖物
    void setPic(BDLocation location) {
        meLatLng = new LatLng(location.getLatitude(), location.getLongitude());
        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.mipmap.ic_me_location);
        overlayOptionLocation = new MarkerOptions()
                .position(meLatLng)
                .icon(bitmapDescriptor).zIndex(101);
        mBaiduMap.addOverlay(overlayOptionLocation);
        mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLngZoom(meLatLng, 16f));
    }

    //初始化百度地图覆盖物以及工具类
    void initBaiDuOverlayView() {
        if (overLayoutView == null) {
            overLayoutView = LayoutInflater.from(this).inflate(R.layout.my_overlay_view, null);
        }
        if (myBaiduMapUtils == null) {
            myBaiduMapUtils = new MyBaiduMapUtils();
        }
    }

    //网点设置覆盖物
    public void initOverlay() {
        // add marker overlay
        initBaiDuOverlayView();
        ltAddressMsg.setVisibility(View.INVISIBLE);
        btStartUseCar.setVisibility(View.INVISIBLE);
        if (overLayBeanList != null) {
            if (overLayBeanList.size() >= 0) {
                mBaiduMap.clear();
                allMarkers.clear();
                mBaiduMap.addOverlay(overlayOptionLocation);
                if (markerOptionsList == null || markerOptionsList.size() == 0) {
                    markerOptionsList = new ArrayList<>();
                }
                if (markerOptionsList.size() == 0) {
                    for (int i = 0; i < overLayBeanList.size(); i++) {
                        LatLng latLng = new LatLng(overLayBeanList.get(i).getLatitude(), overLayBeanList.get(i).getLongitude());
                        MarkerOptions markerOptions = new MarkerOptions().icon(myBaiduMapUtils.getBitmapDescriptor(this, overLayoutView, overLayBeanList.get(i), false))
                                .zIndex(100).draggable(false);
                        markerOptions.position(latLng);
                        Marker marker = (Marker) mBaiduMap.addOverlay(markerOptions);
                        markerOptionsList.add(markerOptions);
                        overLayBeanList.get(i).setDistance(getDistance(latLng));
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("info", overLayBeanList.get(i));
                        bundle.putInt("index", i);
                        marker.setExtraInfo(bundle);
                        allMarkers.add(marker);//加入一个集合
                    }
                } else {
                    if (overLayBeanList.size() > markerOptionsList.size()) {//如果网点大于缓存集合
                        for (int i = 0; i < markerOptionsList.size(); i++) {
                            LatLng latLng = new LatLng(overLayBeanList.get(i).getLatitude(), overLayBeanList.get(i).getLongitude());
                            markerOptionsList.get(i).position(latLng);
                            Marker marker = (Marker) mBaiduMap.addOverlay(markerOptionsList.get(i));
                            marker.setIcon(myBaiduMapUtils.getBitmapDescriptor(this, overLayoutView, overLayBeanList.get(i), false));
                            Bundle bundle = new Bundle();
                            overLayBeanList.get(i).setDistance(getDistance(latLng));
                            bundle.putSerializable("info", overLayBeanList.get(i));
                            bundle.putInt("index", i);
                            marker.setExtraInfo(bundle);
                            allMarkers.add(marker);//加入一个集合
                        }
                        for (int i = markerOptionsList.size(); i < overLayBeanList.size(); i++) {
                            LatLng latLng = new LatLng(overLayBeanList.get(i).getLatitude(), overLayBeanList.get(i).getLongitude());
                            MarkerOptions markerOptions = new MarkerOptions().icon(myBaiduMapUtils.getBitmapDescriptor(this, overLayoutView, overLayBeanList.get(i), false))
                                    .zIndex(100).draggable(false);
                            markerOptions.position(latLng);
                            Marker marker = (Marker) mBaiduMap.addOverlay(markerOptions);
                            markerOptionsList.add(markerOptions);
                            Bundle bundle = new Bundle();
                            overLayBeanList.get(i).setDistance(getDistance(latLng));
                            bundle.putSerializable("info", overLayBeanList.get(i));
                            bundle.putInt("index", i);
                            marker.setExtraInfo(bundle);
                            allMarkers.add(marker);//加入一个集合
                        }

                    } else {//网点小于等于缓存集合
                        for (int i = 0; i < overLayBeanList.size(); i++) {
                            LatLng latLng = new LatLng(overLayBeanList.get(i).getLatitude(), overLayBeanList.get(i).getLongitude());
                            markerOptionsList.get(i).position(latLng);
                            Marker marker = (Marker) mBaiduMap.addOverlay(markerOptionsList.get(i));
                            marker.setIcon(myBaiduMapUtils.getBitmapDescriptor(this, overLayoutView, overLayBeanList.get(i), false));
                            Bundle bundle = new Bundle();
                            overLayBeanList.get(i).setDistance(getDistance(latLng));
                            bundle.putSerializable("info", overLayBeanList.get(i));
                            bundle.putInt("index", i);
                            marker.setExtraInfo(bundle);
                            allMarkers.add(marker);//加入一个集合
                        }
                    }
                }
            } else {
            }
        }
    }

    /**
     * 计算距离
     *
     * @param latLng2
     * @return
     */
    private String getDistance(LatLng latLng2) {
        if (latLng2 != null) {
            LatLng latLng1 = new LatLng(myDBlocation.getLatitude(), myDBlocation.getLongitude());
            double distance = DistanceUtil.getDistance(latLng1, latLng2);
            DecimalFormat df = new DecimalFormat("0");
            String k = "";
            String m = "";
            if (distance > 1000) {
                k = df.format(distance / 1000);
                m = df.format(distance % 1000);
                return k + "." + m + "公里";
            } else {
                m = df.format(distance);
                return m + "米";
            }

        } else {
            return "";
        }
    }

    //覆盖物点击事件
    @Override
    public boolean onMarkerClick(Marker marker) {
        overLayBean = (OverLayBean) marker.getExtraInfo().getSerializable("info");
        if (isFirst) {//如果是首次进入点击
            index = marker.getExtraInfo().getInt("index");//点击用车
            marker.setIcon(myBaiduMapUtils.getBitmapDescriptor(this, overLayoutView, overLayBean, true));
            marker.setZIndex(101);
            isFirst = false;
        } else {//不是第一次点击
            OverLayBean overLayBeanBefore = (OverLayBean) allMarkers.get(index).getExtraInfo().getSerializable("info");
            allMarkers.get(index).setIcon(myBaiduMapUtils.getBitmapDescriptor(this, overLayoutView, overLayBeanBefore, false));
            allMarkers.get(index).setZIndex(100);
            marker.setIcon(myBaiduMapUtils.getBitmapDescriptor(this, overLayoutView, overLayBean, true));
            marker.setZIndex(101);
            index = marker.getExtraInfo().getInt("index");//点击用车
        }
        if (overLayBeanList.get(index).getCarcount().equals("0") || overLayBeanList.get(index).getCarcount() == null || overLayBeanList.get(index).getCarcount().equals("")) {//没有车啦
            ltAddressMsg.setVisibility(View.INVISIBLE);
            btStartUseCar.setVisibility(View.VISIBLE);
            btStartUseCar.setBackgroundResource(R.drawable.shape_main_null);
            btStartUseCar.setText("当前网点没有可用车辆");
            btStartUseCar.setEnabled(false);
            btStartUseCar.setTextColor(getResources().getColor(R.color.main));
        } else {
            ltAddressMsg.setVisibility(View.VISIBLE);
            btStartUseCar.setVisibility(View.VISIBLE);
            tvAddressName.setText(overLayBean.getName());
            tvAddress.setText(overLayBean.getAddress());
            tvDistance.setText(overLayBean.getDistance());
            btStartUseCar.setBackgroundResource(R.drawable.shape_main_have);
            btStartUseCar.setText("立即用车");
            btStartUseCar.setEnabled(true);
            btStartUseCar.setTextColor(getResources().getColor(R.color.white));
        }
        return true;
    }


    //开始用车
    private View.OnClickListener StartUseCarListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainActivity.this, ConfirmOrderActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(DOT_ID, overLayBeanList.get(index));
            intent.putExtras(bundle);
            startActivity(intent);
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.layout_left:
                drawerLayout.openDrawer(ltMenu);
                checkUserStatus();
                break;
            case R.id.layout_right:
                Intent intent8 = new Intent();
                intent8.setClass(MainActivity.this, WebViewActivity.class);
                intent8.putExtra(WEBVIEW_URL, URLS.ACTIVITY);
                intent8.putExtra(TITLE_FLAG, "活动");
                startActivity(intent8);
                break;
            case R.id.custom_action_bar_select_city:
//                showButtomToast("暂无其他城市");

                if (mPopWindow.isShowing()) {
                    mPopWindow.dismiss();
                } else {
                    setStatusBarColor(80);
                    mPopWindow.showAtLocation(rootview, Gravity.CENTER, 0, 0);
                }
                break;
            case R.id.iv_main_location:
                reGetLocation();
                mBaiduMap.setMapStatus(MapStatusUpdateFactory.newLatLngZoom(meLatLng, 16f));
                break;
            case R.id.rt_menu_balance:
                Intent intent = new Intent();
                intent.setClass(MainActivity.this, BalanceActivity.class);//余额
                startActivity(intent);
                break;
            case R.id.rt_menu_accident:
                Intent intent7 = new Intent();
                intent7.setClass(MainActivity.this, PeccancyRecordActivity.class);//违章事故处理
                startActivity(intent7);
                break;
            case R.id.rt_rn_authentication://认证
                Intent intent1 = new Intent(MainActivity.this, RNAuthenticationActivity.class);
                startActivity(intent1);
                break;

            case R.id.rt_menu_oredersrecord:
                Intent intent2 = new Intent();
                intent2.setClass(MainActivity.this, OrdersRecordActivity.class);//订单管理
                startActivityForResult(intent2, ORDERREQUEST);
                break;
            case R.id.rt_menu_notify:
                Intent intent3 = new Intent();
                intent3.setClass(MainActivity.this, NotifyActivity.class);//通知
                startActivity(intent3);
                break;
            case R.id.rt_menu_invoice:
                showButtomToast("功能开发中");
//                Intent intent4 = new Intent();
//                intent4.setClass(MainActivity.this, InvoiceActivity.class);//索取发票
//                startActivity(intent4);
                break;
            case R.id.rt_menu_setting:
                Intent intent5 = new Intent();
                intent5.setClass(MainActivity.this, SettingActivity.class);//设置
                startActivityForResult(intent5, REQUEST_SETTING);
                break;

            case R.id.rt_menu_service:
                drawerLayout.closeDrawer(ltMenu);
                showDialog("联系客服", "400-888-1212", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        callPhone("400-888-1212");
                    }
                });
                break;
            case R.id.iv_personal_pic:
                Intent intent6 = new Intent();
                intent6.setClass(MainActivity.this, PersonalCenterActivity.class);//个人中心
                startActivityForResult(intent6, REQUEST);
                break;
            case R.id.iv_main_nav:
                navigationLocationApp(myDBlocation.getLatitude(), myDBlocation.getLongitude(), "", overLayBean.getLatitude(), overLayBean.getLongitude(), overLayBean.getName(), 2);
                break;

            case R.id.btn_cancle:
                mPopWindow.dismiss();
                break;

            case R.id.btn_gocomplain:
                Intent intent9 = new Intent();
                intent9.setClass(MainActivity.this, ComplaintActivity.class);//个人中心
                startActivity(intent9);
                break;
        }

    }

    /**
     * 拨打电话
     */
    private void callPhone(String number) {
        Intent intent = new Intent(Intent.ACTION_CALL);
        Uri data = Uri.parse("tel:" + number);
        intent.setData(data);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
        MobclickAgent.onPause(this);
    }

    /**
     * 检查用户状态
     */
    private void checkUserStatus() {
        if (LeTravelApplication.getSharePreferenceInstance(this).getToken() != null && !LeTravelApplication.getSharePreferenceInstance(this).getToken().equals("")) {
            if (LeTravelApplication.getSharePreferenceInstance(this).getUserNumber() != null) {
                if (LeTravelApplication.getSharePreferenceInstance(this).getUserNumber().equals("")) {
                    tvUserNo.setText("请登录");
                } else {
                    tvUserNo.setText(LeTravelApplication.getSharePreferenceInstance(this).getUserNumber());
                }
            }
            if (LeTravelApplication.getSharePreferenceInstance(this).getUserStatus() != null) {
                try {
                    switch (Integer.parseInt(LeTravelApplication.getSharePreferenceInstance(this).getUserStatus())) {
                        case 1:
                            tvMenuStatus.setText("未认证");
                            tvMenuStatus.setTextColor(getResources().getColor(R.color.small_text));
                            break;
                        case 2:
                            tvMenuStatus.setText("审核中");
                            tvMenuStatus.setTextColor(getResources().getColor(R.color.rnaing));
                            break;
                        case 4:
                            tvMenuStatus.setText("未通过");
                            tvMenuStatus.setTextColor(getResources().getColor(R.color.red));
                            break;
                        case 3:
                            if (TextUtils.isEmpty(LeTravelApplication.getSharePreferenceInstance(this).getHandIdCarImg())) {
                                tvMenuStatus.setText("待补齐");
                                tvMenuStatus.setTextColor(getResources().getColor(R.color.red));
                            } else {
                                tvMenuStatus.setText("已审核");
                                tvMenuStatus.setTextColor(getResources().getColor(R.color.main));
                            }

                            break;
                        default:
                            tvMenuStatus.setText("");
                            break;
                    }
                } catch (Exception e) {

                }
            }
        } else {
            tvUserNo.setText("请登录");
            tvMenuStatus.setText("");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
        this.registerReceiver(myReceiver, filter);
        mMapView.onResume();
        checkUserStatus();
    }

    @Override
    protected void onStop() {
        this.unregisterReceiver(myReceiver);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        mBaiduMap.setMyLocationEnabled(false);
        super.onDestroy();
        // 退出时销毁定位
        if (mMapView != null) {
            mMapView.onDestroy();
            mMapView = null;
        }
        if (myBaiduMapUtils != null) {
            myBaiduMapUtils = null;
        }
        if (myTimer != null) {
            myTimer.cancel();
            myTimer = null;
        }
        HttpEngine.cancelRequest(MainActivity.class);//停止线程
    }

    @Override
    public void loginOk() {
        super.loginOk();
        Intent intent = new Intent(this, CheckUserOrderService.class);
        startService(intent);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (drawerLayout.isDrawerOpen(ltMenu)) {
                drawerLayout.closeDrawers();
            } else {
                exit();
            }
        }
        return true;
    }

    @Override
    public void timeOutFail() {
        super.timeOutFail();
        showDialog("", "网络连接失败，请重试", "重试", "", false, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                reGetLocation();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
    }


    @Override
    public void netOk() {
//        if (isFirstOpen) {
//            showProgressBar(true);
//            isFirstOpen = false;
//            reGetLocation();
//        }
    }

    @Override
    public void netNo() {
//        isFirstOpen = true;
    }

    private void exit() {
        if (!isExit) {
            isExit = true;
            Toast.makeText(getApplicationContext(), "再按一次退出程序",
                    Toast.LENGTH_SHORT).show();
            // 利用handler延迟发送更改状态信息
            mHandler.sendEmptyMessageDelayed(0, 2000);
        } else {
            finish();
            LeTravelApplication.getInstance().AppExit();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ORDERREQUEST) {
            if (resultCode == AGAIN) {
//                drawerLayout.closeDrawer(ltMenu);
            }
        } else if (requestCode == REQUEST_SETTING && resultCode == SettingActivity.RESULT_EXIT) {//成功退出
            drawerLayout.closeDrawer(ltMenu);
            tvUserNo.setText("请登录");
            tvMenuStatus.setText("");
        }
    }

    //如果超出范围我们要告诉他，并且要清除网点
    @Override
    public void dataFail() {
        super.dataFail();
        mBaiduMap.clear();
        mBaiduMap.addOverlay(overlayOptionLocation);
    }

    private void addCity() {
        //模拟数据
        CityBean tianjin = new CityBean();
        tianjin.setLat(39.1046910000);
        tianjin.setLon(117.1958530000);
        tianjin.setName("天津市");


        CityBean cityBean3 = new CityBean();
        cityBean3.setLat(0.0);
        cityBean3.setLon(0.0);
        cityBean3.setName("暂缓开通");
        list.add(tianjin);
        list.add(cityBean3);

    }


    public void showCityNoOpen(String cityName) {
        if ("天津市".equals(cityName.trim())) {
            cityNoOpen.setVisibility(View.GONE);
            baiduAllview.setVisibility(View.VISIBLE);
        } else {
            Glide.with(this).load(R.mipmap.ic_city_noopen).into(ivNoOpenCity);
            cityNoOpen.setVisibility(View.VISIBLE);
            baiduAllview.setVisibility(View.GONE);
        }
    }

    /**
     * 时间戳
     *
     * @param
     * @return
     */
    public static String formatTime(String time) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        SimpleDateFormat oldTime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date data = null;
        try {
            data = oldTime.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String format = formatter.format(data);
        return format;
    }
}
