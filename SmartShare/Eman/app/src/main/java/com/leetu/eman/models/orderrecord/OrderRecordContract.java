package com.leetu.eman.models.orderrecord;


import com.leetu.eman.base.BaseContract;
import com.leetu.eman.models.orderrecord.beans.OrderRecordBean;

import java.util.List;

/**
 * Created by jyt on 2016/9/18.
 */

public interface OrderRecordContract {
    interface View extends BaseContract {
        void showLoad(List<OrderRecordBean> orders, boolean isUp);
    }

    interface UserAction {
        void load(boolean isUp, boolean isFrist);
    }
}
