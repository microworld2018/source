package com.leetu.eman.models.confirmorder.beans;/**
 * Created by jyt on 2016/9/23.
 */

import java.io.Serializable;

/**
 * created by neo on 2016/9/23 16:43
 */
public class CarDetailBean implements Serializable {
    String id;
    String carId;
    String androidModelPhoto;
    String seatingNum;
    String lifeMileage;
    String name;
    String kmPrice;
    String brand;
    String iosModelPhoto;
    String casesNum;
    String basePrice;
    String vehiclePlateId;
    String timelyFeeUnit;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getAndroidModelPhoto() {
        return androidModelPhoto;
    }

    public void setAndroidModelPhoto(String androidModelPhoto) {
        this.androidModelPhoto = androidModelPhoto;
    }

    public String getSeatingNum() {
        return seatingNum;
    }

    public void setSeatingNum(String seatingNum) {
        this.seatingNum = seatingNum;
    }

    public String getLifeMileage() {
        return lifeMileage;
    }

    public void setLifeMileage(String lifeMileage) {
        this.lifeMileage = lifeMileage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKmPrice() {
        return kmPrice;
    }

    public void setKmPrice(String kmPrice) {
        this.kmPrice = kmPrice;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getIosModelPhoto() {
        return iosModelPhoto;
    }

    public void setIosModelPhoto(String iosModelPhoto) {
        this.iosModelPhoto = iosModelPhoto;
    }

    public String getCasesNum() {
        return casesNum;
    }

    public void setCasesNum(String casesNum) {
        this.casesNum = casesNum;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getVehiclePlateId() {
        return vehiclePlateId;
    }

    public void setVehiclePlateId(String vehiclePlateId) {
        this.vehiclePlateId = vehiclePlateId;
    }

    public String getTimelyFeeUnit() {
        return timelyFeeUnit;
    }

    public void setTimelyFeeUnit(String timelyFeeUnit) {
        this.timelyFeeUnit = timelyFeeUnit;
    }

    @Override
    public String toString() {
        return "CarDetailBean{" +
                "carId='" + carId + '\'' +
                ", androidModelPhoto='" + androidModelPhoto + '\'' +
                ", seatingNum='" + seatingNum + '\'' +
                ", lifeMileage='" + lifeMileage + '\'' +
                ", name='" + name + '\'' +
                ", kmPrice='" + kmPrice + '\'' +
                ", brand='" + brand + '\'' +
                ", iosModelPhoto='" + iosModelPhoto + '\'' +
                ", casesNum='" + casesNum + '\'' +
                ", basePrice='" + basePrice + '\'' +
                ", vehiclePlateId='" + vehiclePlateId + '\'' +
                ", timelyFeeUnit='" + timelyFeeUnit + '\'' +
                '}';
    }
}
