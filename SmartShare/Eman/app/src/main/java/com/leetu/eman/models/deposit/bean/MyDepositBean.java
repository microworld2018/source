package com.leetu.eman.models.deposit.bean;

import java.util.List;

/**
 * Created by lvjunfeng on 2017/4/6.
 */

public class MyDepositBean {
    private double deposit;
    private double strategyMoney;
    private List<DepositRecordBean> recordList;

    public double getDeposit() {
        return deposit;
    }

    public void setDeposit(double deposit) {
        this.deposit = deposit;
    }

    public double getStrategyMoney() {
        return strategyMoney;
    }

    public void setStrategyMoney(double strategyMoney) {
        this.strategyMoney = strategyMoney;
    }

    public List<DepositRecordBean> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<DepositRecordBean> recordList) {
        this.recordList = recordList;
    }
}
