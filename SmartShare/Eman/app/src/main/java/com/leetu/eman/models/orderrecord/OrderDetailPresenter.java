package com.leetu.eman.models.orderrecord;

import android.content.Context;

import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BasePresenter;
import com.leetu.eman.models.orderrecord.beans.OrderDetailBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.net.NetworkHelper;
import com.leetu.eman.net.ResponseStatus;
import com.leetu.eman.urls.URLS;
import com.leetu.eman.utils.JsonParser;
import com.leetu.eman.utils.LogUtils;

/**
 * Created by Administrator on 2016/11/5.
 */

public class OrderDetailPresenter extends BasePresenter implements OrderDetailContract.UserAction {
    private Context context;
    private OrderDetailContract.View orderDetailListener;

    public OrderDetailPresenter(Context context, OrderDetailContract.View orderDetailListener) {
        this.context = context;
        this.orderDetailListener = orderDetailListener;
    }

    @Override
    public void load(String orderId, final boolean isFrist) {
        if (NetworkHelper.isNetworkConnect(context)) {
            HttpEngine.post().url(URLS.ORDER_DETAIL)
                    .addParam("token", LeTravelApplication.getSharePreferenceInstance(context).getToken())
                    .addParam("orderId", orderId)
                    .tag(OrderDetailActivity.class)
                    .execute(new HttpEngine.ResponseCallback() {
                        @Override
                        public void onResponse(ResponseStatus response) {
                            if (checkCode(response.getResultCode(), orderDetailListener)) {
                                LogUtils.e("gn", "订单详情" + response.getData());
                                OrderDetailBean orderDetailBean = JsonParser.getParsedData(response.getData(), OrderDetailBean.class);
                                orderDetailListener.showLoad(orderDetailBean);
                            } else {
                                if (response.getResultCode() != 206) {
                                    if (isFrist) {
                                        orderDetailListener.contentFail();
                                    } else {
                                        orderDetailListener.showFail(response.getResultCode(), response.getResultMsg());
                                    }
                                }
                            }
                        }

                        @Override
                        public void onFailure(Exception error) {
                            orderDetailListener.timeOutFail();
                        }
                    });
        } else {
            orderDetailListener.contentFail();
        }
    }
}
