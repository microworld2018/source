package com.leetu.eman.models.peccancyrecord.beans;

import java.io.Serializable;

/**
 * Created by jyt on 2016/9/18.
 */

public class PeccancyRecordBean implements Serializable {
    private String happenTime;
    private int type;
    private String desc;
    private int status;
    private double money;
    private String accidentId;

    public String getHappenTime() {
        return happenTime;
    }

    public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getAccidentId() {
        return accidentId;
    }

    public void setAccidentId(String accidentId) {
        this.accidentId = accidentId;
    }
}
