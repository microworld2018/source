package com.leetu.eman.models.welcome;/**
 * Created by jyt on 2016/9/20.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.leetu.eman.R;
import com.leetu.eman.application.LeTravelApplication;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.login.LoginActivity;
import com.leetu.eman.models.welcome.adapters.MyViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * created by neo on 2016/9/20 11:29
 * 欢迎页
 */
public class WelcomeActivity extends BaseActivity implements View.OnClickListener {
    private ViewPager vpWel;
    private List<View> listViews;
    private int[] pic = {R.mipmap.ic_welcome_one, R.mipmap.ic_welcome_two, R.mipmap.ic_welcome_three, R.mipmap.ic_welcome_four};
    private MyViewPagerAdapter viewPagerAdapter;
    private Button btStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initView();
    }

    protected void initView() {
        //首先判断是否是第一次运行
        if (LeTravelApplication.getSharePreferenceInstance(this).getFirst() != null) {
            Intent intent = new Intent(WelcomeActivity.this, StartUpActivity.class);
            startActivity(intent);
            WelcomeActivity.this.finish();
        } else {
            listViews = new ArrayList<>();
            View one = LayoutInflater.from(this).inflate(R.layout.welcome_viewpage_one, null);
            View two = LayoutInflater.from(this).inflate(R.layout.welcome_viewpage_two, null);
            View three = LayoutInflater.from(this).inflate(R.layout.welcome_viewpage_three, null);
            View four = LayoutInflater.from(this).inflate(R.layout.welcome_viewpage_four, null);
            btStart = (Button) four.findViewById(R.id.bt_welcome_start);
            btStart.setOnClickListener(this);
            listViews.add(one);
            listViews.add(two);
            listViews.add(three);
            listViews.add(four);

            vpWel = (ViewPager) findViewById(R.id.vp_welcome);

            viewPagerAdapter = new MyViewPagerAdapter(listViews);
            vpWel.setAdapter(viewPagerAdapter);
        }


    }

    @Override
    public void onClick(View v) {
        LeTravelApplication.getSharePreferenceInstance(this).saveFirst("false");
        Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
        startActivity(intent);
        WelcomeActivity.this.finish();
    }
}
