package com.leetu.eman.models.usecar.bean;

/**
 * Created by Administrator on 2016/12/1.
 */

public class CheckUserOrderCar {
    String carId;
    String bizState;
    String androidModelPhoto;
    String cnName;
    String name;
    String iosModelPhoto;
    String vehiclePlateId;
    int version;

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }

    public String getBizState() {
        return bizState;
    }

    public void setBizState(String bizState) {
        this.bizState = bizState;
    }

    public String getAndroidModelPhoto() {
        return androidModelPhoto;
    }

    public void setAndroidModelPhoto(String androidModelPhoto) {
        this.androidModelPhoto = androidModelPhoto;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIosModelPhoto() {
        return iosModelPhoto;
    }

    public void setIosModelPhoto(String iosModelPhoto) {
        this.iosModelPhoto = iosModelPhoto;
    }

    public String getVehiclePlateId() {
        return vehiclePlateId;
    }

    public void setVehiclePlateId(String vehiclePlateId) {
        this.vehiclePlateId = vehiclePlateId;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
