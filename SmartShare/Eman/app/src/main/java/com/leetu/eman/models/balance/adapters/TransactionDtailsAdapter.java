package com.leetu.eman.models.balance.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.leetu.eman.R;
import com.leetu.eman.models.balance.beans.RechargeRecordBean;

import java.util.List;


/**
 * Created by Administrator on 2016/11/25.
 */

public class TransactionDtailsAdapter extends BaseAdapter {
    private final Context mContext;
    private final List<RechargeRecordBean> list;

    public TransactionDtailsAdapter(Context context, List<RechargeRecordBean> charges) {
        this.mContext = context;
        this.list = charges;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        RechargeRecordBean rechargeRecordBean = list.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_balance_details, null);
            viewHolder.tvRechangeType = (TextView) convertView.findViewById(R.id.rechange_type);
            viewHolder.tvRechangeTime = (TextView) convertView.findViewById(R.id.rechang_time);
            viewHolder.tvRechangeMoney = (TextView) convertView.findViewById(R.id.rechange_money);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvRechangeType.setText(rechargeRecordBean.getPayChannel());
        viewHolder.tvRechangeTime.setText(rechargeRecordBean.getTradeTime());
        viewHolder.tvRechangeMoney.setText("+" + rechargeRecordBean.getAmount());

        return convertView;
    }

    class ViewHolder {
        TextView tvRechangeType, tvRechangeTime, tvRechangeMoney;
    }
}
