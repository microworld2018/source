package com.leetu.eman.models.confirmorder;/**
 * Created by jyt on 2016/9/19.
 */

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baidu.location.BDLocation;
import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.base.BaseAppCompatActivity;
import com.leetu.eman.models.activity.WebViewActivity;
import com.leetu.eman.models.confirmorder.adapter.ExpanableAdapter;
import com.leetu.eman.models.confirmorder.beans.CarDetailBean;
import com.leetu.eman.models.confirmorder.beans.ExpanableBean;
import com.leetu.eman.models.confirmorder.beans.PointAllcarDetailBean;
import com.leetu.eman.models.confirmorder.beans.ReturnCarPointBean;
import com.leetu.eman.models.deposit.DepositActivity;
import com.leetu.eman.models.rnauthentication.RNAuthenticationActivity;
import com.leetu.eman.models.takecar.CancelOrderActivity;
import com.leetu.eman.models.takecar.TakeCarActivity;
import com.leetu.eman.models.takecar.beans.TakeCarBean;
import com.leetu.eman.models.usecar.MainActivity;
import com.leetu.eman.models.usecar.bean.OverLayBean;
import com.leetu.eman.net.HttpEngine;
import com.leetu.eman.utils.LogUtils;
import com.leetu.eman.utils.ScreenHelper;
import com.leetu.eman.views.CustomExpandableListView;
import com.leetu.eman.views.MyDotsViewPager;
import com.leetu.eman.views.MyRelativeLayout;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * created by neo on 2016/9/19 13:12
 * 确认订单
 */

public class ConfirmOrderActivity extends BaseAppCompatActivity implements View.OnClickListener, ConfirmOrderContract.View, MyDotsViewPager.PagerListener, BaseActivity.MyLocationLisenter, ExpanableAdapter.OnClikListener {
    private TitleBar titleBar;
    private LinearLayout ltStartTravel, ltNocar, ltContent;
    private Button btConfirmInfo, btStartTravel, btOther;
    private ConfirmOrderPresenter confirmOrderPresenter;
    private String dotId, carId, backDotId;
    private MyDotsViewPager dotsViewPager;
    private List<Fragment> fragmentList;
    private TextView tvReturnName, tvReturnAddress, tvTakeName, tvTakeAddress, baoXianTakeName, baoXianReturnName;

    private LinearLayout baoXianContent;
    private RelativeLayout contentView;
    private MyRelativeLayout pullDowm;
    public static String DOT_ID = "dotId";
    private List<CarDetailBean> carDetailBeanList;
    public static final int REQUEST = 1000;
    public static final int RESULT = 1001;
    public static final int RESULT_FINISH = 1002;
    public static final String ORDER_BREN = "order";
    private String takeCarDotId;
    private OverLayBean overLayBean;
    private ImageView ivNa;
    private boolean isFrist = true;

    private int height;
    private ObjectAnimator transAnimator;
    private ObjectAnimator alphaAnimator;

    private CustomExpandableListView listView;
    private boolean backFlag = false;//按下back键时动画执行的标志
    private String buffer = "";//记录下单时所选的车保险的id
    private List<ExpanableBean> datas;
    private ExpanableAdapter expanableAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmorder);
        this.setMyLocationLisenter(this);
        initView();
        loadData();
    }

    protected void initView() {
        overLayBean = (OverLayBean) getIntent().getSerializableExtra(MainActivity.DOT_ID);
        if (overLayBean != null) {
            if (overLayBean.getDotId() != null) {
                dotId = overLayBean.getDotId();
                takeCarDotId = overLayBean.getDotId();
            }
        }
        titleBar = (TitleBar) findViewById(R.id.title_confirmorder);
        ltStartTravel = (LinearLayout) findViewById(R.id.lt_start_travel);
        btConfirmInfo = (Button) findViewById(R.id.bt_confirm_info);
        dotsViewPager = (MyDotsViewPager) findViewById(R.id.vp_car_detail);
        tvReturnName = (TextView) findViewById(R.id.tv_return_name);
        tvReturnAddress = (TextView) findViewById(R.id.tv_return_adddress);
        tvTakeName = (TextView) findViewById(R.id.tv_confirmorder_takename);
        tvTakeAddress = (TextView) findViewById(R.id.tv_confirmorder_takeaddress);
        ivNa = (ImageView) findViewById(R.id.iv_confirmorder_na);
        ltNocar = (LinearLayout) findViewById(R.id.lt_confirmorder_nocar);
        ltContent = (LinearLayout) findViewById(R.id.lt_confirmorder_content);
        btOther = (Button) findViewById(R.id.bt_confirmorder_other);

        baoXianContent = (LinearLayout) findViewById(R.id.ll_baoxian_content);
        contentView = (RelativeLayout) findViewById(R.id.content_view);
        pullDowm = (MyRelativeLayout) findViewById(R.id.rl_pulldown);
        baoXianTakeName = (TextView) findViewById(R.id.tv_confirmorder_insurance_take);
        baoXianReturnName = (TextView) findViewById(R.id.tv_confirmorder_insurance_return);
        btStartTravel = (Button) findViewById(R.id.bt_start_travel);
        listView = (CustomExpandableListView) findViewById(R.id.expanablelistview);

        datas = new ArrayList<>();
        expanableAdapter = new ExpanableAdapter(datas, this);
        listView.setAdapter(expanableAdapter);
        listView.setGroupIndicator(null);//去掉图标
        expanableAdapter.setOnClikListener(this);
        listView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return true;//组的子item不可点击
            }
        });

        height = ScreenHelper.getScreenHeightExStatus(this);
        confirmOrderPresenter = new ConfirmOrderPresenter(this, this);
        fragmentList = new ArrayList<>();

        titleBar.setTitle("确认订单");
        titleBar.setLeftClickListener(this);
        contentView.setOnClickListener(this);
        ltStartTravel.setOnClickListener(this);
        btConfirmInfo.setOnClickListener(this);
        btStartTravel.setOnClickListener(this);
        dotsViewPager.setPagerListener(this);
        ivNa.setOnClickListener(this);
        btOther.setOnClickListener(this);
        tvTakeName.setText(overLayBean.getName());
        baoXianTakeName.setText(overLayBean.getName());
        tvTakeAddress.setText(overLayBean.getAddress());
        baoXianReturnName.setText(overLayBean.getAddress());
        pullDowm.setOnTouchListener(new MyRelativeLayout.OnTouchListener() {
            @Override
            public void scrollDown() {
                backFlag = false;
                initAnimator(contentView, "translationY", -height, 0);
                initAlphaAnimator(baoXianContent, "alpha", 1.0f, 0.0f);
            }

            @Override
            public void down() {
            }
        });
    }

    private void loadData() {
        showLoading();
        confirmOrderPresenter.getPointDetail(dotId, isFrist);
    }

    @Override
    protected void onRetryLoadData() {
        loadData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                ConfirmOrderActivity.this.finish();
                break;
            case R.id.lt_start_travel:
                Intent intent = new Intent(ConfirmOrderActivity
                        .this, ReturnCarAddressActivity.class);
                intent.putExtra(DOT_ID, takeCarDotId);
                startActivityForResult(intent, REQUEST);
                break;
            case R.id.bt_confirm_info://这个地方是先去实名认证，然后是展示取车
                showProgressBar(true);
                buffer = "";
                confirmOrderPresenter.confirmCar(carId);
                break;
            case R.id.iv_confirmorder_na:
                showProgressBar(true);
                reGetLocation();
                break;
            case R.id.bt_confirmorder_other:
                ConfirmOrderActivity.this.finish();
                break;

            case R.id.ll_baoxian_content:
                break;
            case R.id.content_view://防止被点穿
                break;

            case R.id.bt_start_travel:
                if (TextUtils.isEmpty(buffer)) {
                    LogUtils.e("lv", "没有选保险");
                } else {
                    LogUtils.e("lv", buffer);
                }
                //获取保险
                showTakeDialog("", "需在15分钟内使用车辆\n否则订单会被系统自动取消哦！", "确定", "取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        showProgressBar(true);
                        confirmOrderPresenter.startOrder(dotId, carId, backDotId, buffer);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                break;

        }
    }

    //viewpager回调页码
    @Override
    public void pagerPosition(int page) {
        carId = carDetailBeanList.get(page).getId();
    }

    @Override
    public void showPointDetail(PointAllcarDetailBean pointAllcarDetailBean) {
        showProgressBar(false);
        showContent();
        if (pointAllcarDetailBean != null) {
            if (pointAllcarDetailBean.getCars() != null) {
                if (pointAllcarDetailBean.getCars().size() > 0) {//开始设置数据
                    ltContent.setVisibility(View.VISIBLE);
                    carDetailBeanList = pointAllcarDetailBean.getCars();
                    for (int i = 0; i < carDetailBeanList.size(); i++) {
                        CarFragment carFragment = new CarFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("info", carDetailBeanList.get(i));
                        carFragment.setArguments(bundle);
                        fragmentList.add(carFragment);
                    }
                    dotsViewPager.saveData(carDetailBeanList, getSupportFragmentManager(), fragmentList);
                    carId = carDetailBeanList.get(0).getId();
                } else {
                    ltNocar.setVisibility(View.VISIBLE);
                    ltContent.setVisibility(View.INVISIBLE);
                }
            }
            if (pointAllcarDetailBean.getReturnDot() != null) {
                tvReturnName.setText(pointAllcarDetailBean.getReturnDot().getName());
                tvReturnAddress.setText(pointAllcarDetailBean.getReturnDot().getAddress());
                baoXianReturnName.setText(pointAllcarDetailBean.getReturnDot().getName());
                backDotId = pointAllcarDetailBean.getReturnDot().getId();
            }
        }
    }

    @Override
    public void showOrder(TakeCarBean takeCarBean) {
        showProgressBar(false);
        Intent intent = new Intent(ConfirmOrderActivity
                .this, TakeCarActivity.class);
        startActivityForResult(intent, REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST) {
            if (resultCode == RESULT) {
                ReturnCarPointBean returnCarPointBean = (ReturnCarPointBean) data.getExtras().getSerializable(ReturnCarAddressActivity.RE_ADDRESS);
                if (returnCarPointBean != null) {
                    backDotId = returnCarPointBean.getDotid();
                    tvReturnName.setText(returnCarPointBean.getName());
                    tvReturnAddress.setText(returnCarPointBean.getAddress());
                    baoXianReturnName.setText(returnCarPointBean.getName());
                }
            } else if (resultCode == RESULT_FINISH || resultCode == CancelOrderActivity.GO_HOME) {
                ConfirmOrderActivity.this.finish();
            } else if (resultCode == CancelOrderActivity.AGAIN) {
                loadData();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showProgressBar(false);
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void showMsg(String msg) {
        showProgressBar(false);
        showDialog("提示", msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ConfirmOrderActivity
                        .this, TakeCarActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void showCheckUserRNA(int code, String mes) {
        showProgressBar(false);

        switch (code) {
            case 278:
                showDialog("身份未认证", "您要现在去认证吗？", "去认证", "取消", false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ConfirmOrderActivity.this, RNAuthenticationActivity.class);
                        startActivity(intent);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                break;
            case 279:
                showMiddleToast("身份审核中");
                break;
            case 222:
                showDialog("审核未通过", "是否修改个人信息", "是", "否", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ConfirmOrderActivity.this, RNAuthenticationActivity.class);
                        startActivity(intent);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                break;
            case 280:
                showDialog("资料待补齐", "请上传手持身份证照片", "去补齐", "取消", false, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ConfirmOrderActivity.this, RNAuthenticationActivity.class);
                        startActivity(intent);
                    }
                }, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }, new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                });
                break;
            default:
                break;
        }


    }

    @Override
    public void showGoRechargDeposit() {
        showProgressBar(false);
        showTakecarDepositDialog("", "押金不足，请先充值！", "去充值", "取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ConfirmOrderActivity.this, DepositActivity.class);
                startActivity(intent);
                ConfirmOrderActivity.this.finish();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {

            }
        });
    }

    @Override
    public void showBaoXianDetail(List<ExpanableBean> parsedDataToList) {
        showProgressBar(false);
        if (parsedDataToList != null && parsedDataToList.size() > 0) {
            datas.clear();
            datas.addAll(parsedDataToList);
            for (int i = 0; i < parsedDataToList.size(); i++) {
                String isUsed = parsedDataToList.get(i).getIsUsed();
                if ("1".equals(isUsed)) { //"0"可选
                    buffer = buffer + parsedDataToList.get(i).getInsuranceId() + "/";
                }
            }

            expanableAdapter.notifyDataSetChanged();
            //默认展开
            int count = parsedDataToList.size();
            for (int i = 0; i < count; i++) {
                listView.expandGroup(i);
            }
        }
        baoXianContent.setVisibility(View.VISIBLE);
        backFlag = true;
        initAnimator(contentView, "translationY", 0, -height);
        initAlphaAnimator(baoXianContent, "alpha", 0.0f, 1.0f);

    }

    @Override
    public void loginOk() {
        super.loginOk();
        showLoading();
        confirmOrderPresenter.getPointDetail(dotId, isFrist);
    }

    @Override
    public void getLocation(BDLocation location) {
        showProgressBar(false);
        navigationLocationApp(location.getLatitude(), location.getLongitude(), "我的位置", overLayBean.getLatitude(), overLayBean.getLongitude(), overLayBean.getName(), 2);
    }

    private void initAlphaAnimator(final View view, String propertyName, float start, float end) {
        alphaAnimator = ObjectAnimator.ofFloat(view, propertyName, start, end);
        alphaAnimator.setDuration(800);
        alphaAnimator.start();
    }

    private void initAnimator(View contentView, String propertyName, int start, int end) {
        transAnimator = ObjectAnimator.ofFloat(contentView, propertyName, start, end);
        transAnimator.setDuration(800);
        transAnimator.start();
    }

    @Override
    public void onBackPressed() {
        if (backFlag) {
            backFlag = false;
            initAnimator(contentView, "translationY", -height, 0);
            initAlphaAnimator(baoXianContent, "alpha", 1.0f, 0.0f);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void selectCheckBoxClick(CheckBox checkBox, String id) {
        if (checkBox.isChecked()) {
            buffer = buffer + (id + "/");
        } else {
            buffer = buffer.replace(id + "/", "");
        }
        LogUtils.e("lv", buffer);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        HttpEngine.cancelRequest(ConfirmOrderActivity.class);
    }

    @Override
    public void detailIconClick(String subscrib) {
        if (subscrib != null) {
            Intent intent1 = new Intent(ConfirmOrderActivity.this, WebViewActivity.class);
            intent1.putExtra(WebViewActivity.CONTENT_FLAG, subscrib);
            startActivity(intent1);
        }
    }
}
