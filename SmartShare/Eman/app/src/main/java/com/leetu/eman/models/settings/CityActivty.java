package com.leetu.eman.models.settings;


import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.leetu.eman.R;
import com.leetu.eman.base.BaseActivity;
import com.leetu.eman.models.settings.adapter.CityAdapter;
import com.leetu.eman.pulltorefresh.PullToRefreshListView;
import com.leetu.eman.views.TitleBar;

import java.util.ArrayList;
import java.util.List;

/**
 * 城市切换
 */
public class CityActivty extends BaseActivity implements View.OnClickListener {
    private TitleBar titleBar;
    private PullToRefreshListView pullToRefreshListView;
    private ListView listView;
    private CityAdapter cityAdapter;
    private List<String> citys;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_activty);
        initView();
        loadData();
    }

    protected void initView() {
        titleBar = (TitleBar) findViewById(R.id.title_city);
        pullToRefreshListView = (PullToRefreshListView) findViewById(R.id.listview_city);

        listView = pullToRefreshListView.getContentView();
        citys = new ArrayList<>();
        cityAdapter = new CityAdapter(this, citys);
        TextView footerView = new TextView(this);


        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        footerView.setLayoutParams(layoutParams);
        footerView.setGravity(Gravity.END);
        footerView.setTextSize(12f);
        footerView.setPadding(0, 20, 20, 0);
        footerView.setText("更多城市，敬请期待");
        titleBar.setTitle("城市选择");
        titleBar.setLeftClickListener(this);
        listView.setAdapter(cityAdapter);
        listView.addFooterView(footerView);
    }


    protected void loadData() {
        citys.add("天津");
        cityAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_left:
                CityActivty.this.finish();
                break;
        }
    }
}
