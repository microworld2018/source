package com.leetu.eman.models.currentorder.beans;/**
 * Created by gn on 2016/10/9.
 */

import java.io.Serializable;

/**
 * created by neo on 2016/10/9 13:42
 */
public class CurrentOrderBean implements Serializable {
    CurrentDotBean dot;
    CurrentCarBean car;

    double allMilePrice;
    String orderId;
    int allMile;
    double allTimePrice;
    int isexist;
    int allminutes;
    String timeType;
    String alltime;
    double allPrice;
    int orderType;
    double timePrice;
    double milePrice;

    public CurrentDotBean getDot() {
        return dot;
    }

    public void setDot(CurrentDotBean dot) {
        this.dot = dot;
    }

    public CurrentCarBean getCar() {
        return car;
    }

    public void setCar(CurrentCarBean car) {
        this.car = car;
    }

    public double getAllMilePrice() {
        return allMilePrice;
    }

    public void setAllMilePrice(double allMilePrice) {
        this.allMilePrice = allMilePrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getAllMile() {
        return allMile;
    }

    public void setAllMile(int allMile) {
        this.allMile = allMile;
    }

    public double getAllTimePrice() {
        return allTimePrice;
    }

    public void setAllTimePrice(double allTimePrice) {
        this.allTimePrice = allTimePrice;
    }

    public int getIsexist() {
        return isexist;
    }

    public void setIsexist(int isexist) {
        this.isexist = isexist;
    }

    public int getAllminutes() {
        return allminutes;
    }

    public void setAllminutes(int allminutes) {
        this.allminutes = allminutes;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }

    public String getAlltime() {
        return alltime;
    }

    public void setAlltime(String alltime) {
        this.alltime = alltime;
    }

    public double getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(double allPrice) {
        this.allPrice = allPrice;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public double getTimePrice() {
        return timePrice;
    }

    public void setTimePrice(double timePrice) {
        this.timePrice = timePrice;
    }

    public double getMilePrice() {
        return milePrice;
    }

    public void setMilePrice(double milePrice) {
        this.milePrice = milePrice;
    }

    @Override
    public String toString() {
        return "CurrentOrderBean{" +
                "dot=" + dot +
                ", car=" + car +
                ", allMilePrice=" + allMilePrice +
                ", orderId='" + orderId + '\'' +
                ", allMile=" + allMile +
                ", allTimePrice=" + allTimePrice +
                ", isexist=" + isexist +
                ", allminutes=" + allminutes +
                ", timeType='" + timeType + '\'' +
                ", alltime='" + alltime + '\'' +
                ", allPrice=" + allPrice +
                ", orderType=" + orderType +
                ", timePrice=" + timePrice +
                ", milePrice=" + milePrice +
                '}';
    }
}
