package com.dearho.cs.subscriber.dao;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.subscriber.pojo.SubReviewLog;

/**会员审核记录Dao层接口
 * @author Gaopl
 * @createDate 2017年4月27日
 */
public interface SubReviewLogDao {
	
	/**分页获取审核记录
	 * @param page
	 * @param subReviewLog
	 * @return
	 * @throws Exception
	 */
	public Page<SubReviewLog> getPageSubReviewLog(Page<SubReviewLog> page,SubReviewLog subReviewLog) throws Exception;
	
	/**根据条件获取会员审核记录
	 * @param subReviewLog
	 * @return
	 * @throws Exception
	 */
	public List<SubReviewLog> getListSubReviewLog(SubReviewLog subReviewLog)throws Exception;
	
	
	/**添加会员审核记录
	 * @param subReviewLog
	 * @throws Exception
	 */
	public void saveSubReviewLog(SubReviewLog subReviewLog)throws Exception;

}
