package com.dearho.cs.subscriber.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.subscriber.dao.SubReviewLogDao;
import com.dearho.cs.subscriber.pojo.SubReviewLog;
import com.dearho.cs.sys.pojo.AdministrativeArea;

public class SubReviewLogDaoImpl extends AbstractDaoSupport implements SubReviewLogDao {

	@Override
	public Page<SubReviewLog> getPageSubReviewLog(Page<SubReviewLog> page,
			SubReviewLog subReviewLog) throws Exception {
		StringBuffer hql = new StringBuffer();
		Page<SubReviewLog> resultPage=page(page, hql.toString());
		resultPage.setResults(idToObj(SubReviewLog.class, resultPage.getmResults()));
		return resultPage;
	}

	@Override
	public List<SubReviewLog> getListSubReviewLog(SubReviewLog subReviewLog)
			throws Exception {
		String hql = "select a.id from SubReviewLog a where a.subId = '"+subReviewLog.getSubId()+"'";
		return getList(SubReviewLog.class, queryFList(hql));
	}

	@Override
	public void saveSubReviewLog(SubReviewLog subReviewLog) throws Exception {
		addEntity(subReviewLog);
	}

}
