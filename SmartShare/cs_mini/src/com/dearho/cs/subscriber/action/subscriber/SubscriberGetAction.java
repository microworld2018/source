package com.dearho.cs.subscriber.action.subscriber;


/**
 * Copyrigh  (c) dearho Team
 * All rights reserved.
 *
 *This file MemberCheckGetAction.java creation date:[2015-5-18 上午10:04:40] by liusong
 *http://www.dearho.com
 */



import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.common.util.StringUtils;

import com.dearho.cs.Deposit.pojo.AccDeposit;
import com.dearho.cs.Deposit.service.AccDepositService;
import com.dearho.cs.orders.service.OrdersService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.subscriber.service.SubscriberService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.Constants;
import com.dearho.cs.wechat.pojo.WechatUserInfo;
import com.dearho.cs.wechat.service.WechatUserInfoService;

/**
 * @Author liusong
 * @Description 
 * @Version 1.0,2015-5-18
 *
 */
public class SubscriberGetAction extends AbstractAction {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6988611144656386336L;
	private SubscriberService SubscriberService;
	private Subscriber subscriber ;
	
	private OrdersService ordersService;
	private WechatUserInfoService wechatUserInfoService;
	private AccDepositService accDepositService;
	
	
	private List<AdministrativeArea> citys=new ArrayList<AdministrativeArea>();
	private List<AdministrativeArea> areas=new ArrayList<AdministrativeArea>();
	private List<BranchDot> dots=new ArrayList<BranchDot>();
	
	private WechatUserInfo wechatUserInfo=null;

	protected String result;
	
	private AccDeposit accDeposit;
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public SubscriberGetAction() {
		super();
		subscriber=new Subscriber();
		
	}

	/**
	 * 根据会员姓名查询 会员是否存在！
	 * @return
	 */
	public String isExistSubscriberName(){
		try {
			Subscriber sub  =SubscriberService.querySubscriberByName(subscriber.getName());
			if( null != sub){
				result = Ajax.JSONResult(Constants.RESULT_CODE_SUCCESS, "");
			}else{
				result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "");
			}
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "");
			return SUCCESS;
		}
	}
	
	
	public String isExistIdCard(){
		Subscriber sub  =SubscriberService.querySubscriberByIdCard(subscriber.getIDCard());
		if(sub == null){
			result = Ajax.JSONResult(0, "该用户未注册！");
			return SUCCESS;
		}else{
			result = Ajax.JSONResult(1, "该用户已注册！");
			return SUCCESS;
		}
	}
	
	@Override
	public String process() {
		subscriber=SubscriberService.querySubscriberAllInfoById(subscriber.getId());
		if(!StringUtils.isEmpty(subscriber.getWechatUnionId())){
			wechatUserInfo=wechatUserInfoService.getUserInfoByUnionId(subscriber.getWechatUnionId());
		}
		if(subscriber != null){
			AccDeposit deposit = new AccDeposit();
			deposit.setSubscriberId(subscriber.getId());
			accDeposit = accDepositService.getDeposit(deposit);
			subscriber.setAccDeposit(accDeposit);
		}
		return SUCCESS;
	}



	public SubscriberService getSubscriberService() {
		return SubscriberService;
	}



	public void setSubscriberService(SubscriberService subscriberService) {
		SubscriberService = subscriberService;
	}



	public Subscriber getSubscriber() {
		return subscriber;
	}



	public void setSubscriber(Subscriber subscriber) {
		this.subscriber = subscriber;
	}



	public OrdersService getOrdersService() {
		return ordersService;
	}



	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}



	public List<AdministrativeArea> getCitys() {
		return citys;
	}



	public void setCitys(List<AdministrativeArea> citys) {
		this.citys = citys;
	}



	public List<AdministrativeArea> getAreas() {
		return areas;
	}



	public void setAreas(List<AdministrativeArea> areas) {
		this.areas = areas;
	}



	public List<BranchDot> getDots() {
		return dots;
	}



	public void setDots(List<BranchDot> dots) {
		this.dots = dots;
	}



	public WechatUserInfoService getWechatUserInfoService() {
		return wechatUserInfoService;
	}



	public void setWechatUserInfoService(WechatUserInfoService wechatUserInfoService) {
		this.wechatUserInfoService = wechatUserInfoService;
	}



	public WechatUserInfo getWechatUserInfo() {
		return wechatUserInfo;
	}



	public void setWechatUserInfo(WechatUserInfo wechatUserInfo) {
		this.wechatUserInfo = wechatUserInfo;
	}

	public AccDepositService getAccDepositService() {
		return accDepositService;
	}

	public void setAccDepositService(AccDepositService accDepositService) {
		this.accDepositService = accDepositService;
	}

	public AccDeposit getAccDeposit() {
		return accDeposit;
	}

	public void setAccDeposit(AccDeposit accDeposit) {
		this.accDeposit = accDeposit;
	}
	
}
