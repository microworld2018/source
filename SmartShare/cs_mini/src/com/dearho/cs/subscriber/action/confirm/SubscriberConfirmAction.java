package com.dearho.cs.subscriber.action.confirm;

import java.rmi.RemoteException;
import java.util.Date;

import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;




import org.apache.cxf.common.util.StringUtils;

import cn.id5.gboss.businesses.validator.service.app.QueryValidatorServicesServiceLocator;
import cn.id5.gboss.businesses.validator.service.app.QueryValidatorServicesSoapBindingStub;

import com.dearho.cs.subscriber.pojo.SubReviewLog;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.subscriber.pojo.SubscriberConfirm;
import com.dearho.cs.subscriber.service.SubReviewLogSerive;
import com.dearho.cs.subscriber.service.SubscriberConfirmService;
import com.dearho.cs.subscriber.service.SubscriberService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.SysOperateLogRecord;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.SystemOperateLogService;
import com.dearho.cs.sys.util.SystemOperateLogUtil;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.Constants;
import com.dearho.cs.util.Des2;
import com.opensymphony.webwork.ServletActionContext;

/**
 * @Author liusong
 * @Description 用户信息审核
 * @Version 1.0,2015-5-18
 *
 */
public class SubscriberConfirmAction extends AbstractAction {
	
	private static final long serialVersionUID = -8871763447510238571L;
	private SubscriberConfirmService subscriberConfirmService;
	private SubscriberConfirm subscriberConfirm ;
	private SubscriberService subscriberService;
	private SubReviewLogSerive subReviewLogSerive;
	
	private SystemOperateLogService systemOperateLogService;//记录操作日志Service
	
	
	
	
	
	private QueryValidatorServicesSoapBindingStub binding;

		
	public SubscriberConfirmAction() {
		super();
		subscriberConfirm=new SubscriberConfirm();
		try {
			QueryValidatorServicesServiceLocator locator = new QueryValidatorServicesServiceLocator();
			locator.setQueryValidatorServicesEndpointAddress(Des2.IDCard_ADDRESS);
			binding = (QueryValidatorServicesSoapBindingStub) locator.getQueryValidatorServices();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Override
	public String process()  {
		Integer checkResult=subscriberConfirm.getIsComplete();
		Subscriber subscriber = subscriberService.querySubscriberByIdCard(subscriberConfirm.getSubscriber().getIDCard());
		if(subscriber != null && subscriber.getState() == 3 && checkResult == 1){
			result=Ajax.JSONResult(Constants.RESULT_CODE_FAILED,"审核失败原因证件号码已注册!");
			return ERROR;
		}
		if(SubscriberConfirm.RESULT_SUCCESS.equals(checkResult)){
			
			if(subscriberConfirm.getSubscriber()==null || StringUtils.isEmpty(subscriberConfirm.getSubscriber().getDrivingLicenseNo())){
				result=Ajax.JSONResult(Constants.RESULT_CODE_FAILED,"驾驶证编号不能为空!");
				return ERROR;
			}
		}else{
			if( StringUtils.isEmpty(subscriberConfirm.getResultDesc())){
				result=Ajax.JSONResult(Constants.RESULT_CODE_FAILED,"审核失败原因不能为空!");
				return ERROR;
			}
			subscriberConfirm.setResultDesc(subscriberConfirm.getResultDesc().replaceAll(" ", ""));
		}
	
		User operator=(User)getSession().getAttribute(Constants.SESSION_USER);
		try {
			subscriberConfirm.setResult(checkResult);
			subscriberConfirmService.confirm(subscriberConfirm, operator);
			
			//添加审核记录
			HttpSession session = ServletActionContext.getRequest().getSession();
			User user=(User) session.getAttribute("user");
			SubReviewLog subReviewLog = new SubReviewLog();
			subReviewLog.setIdCard(subscriberConfirm.getSubscriber().getIDCard());
			subReviewLog.setLockedState(subscriberConfirm.getSubscriber().getEventState());
			subReviewLog.setReciewIsPassing(subscriberConfirm.getIsComplete());
			subReviewLog.setReviewReason(subscriberConfirm.getResultDesc());
			subReviewLog.setReviewTime(new Date());
			subReviewLog.setSubId(subscriberConfirm.getSubscriberId());
			subReviewLog.setReviewUserName(user.getName());
			subReviewLog.setReviewUserId(user.getId());
			subReviewLogSerive.saveSubReviewLog(subReviewLog);
			result=Ajax.JSONResult(Constants.RESULT_CODE_SUCCESS,"操作成功");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return SUCCESS;
	}
	
	
	/**会员身份信息验证
	 * @return
	 * @throws RemoteException
	 */
	public String searchIdCard() throws RemoteException{
		try {
			String name = getRequest().getParameter("name");
			String idCard = getRequest().getParameter("idCard");
			String type = getRequest().getParameter("type");
			SysOperateLogRecord log = new SysOperateLogRecord();
			String resultXML = "";
			/*Map map = new HashMap();*/
			String queryParam1 = name+","+idCard;
			String datasource = "";
			String userName = Des2.encode(Des2.KEY, Des2.USERNAME);
			String password = Des2.encode(Des2.KEY, Des2.PASSWORD);
			if(type.equals("0")){
				datasource = Des2.encode(Des2.KEY, Des2.DATASOURCE);
				log.setModelName("会员信息审核身份证验证");
				log.setOperateContent("查询该身份证和姓名是否一致:用户名："+name+",身份证号："+idCard);
			}else{
				datasource = Des2.encode(Des2.KEY, Des2.DATASOURCE1);
				log.setModelName("会员不良信息查询");
				log.setOperateContent("查询会员是否存在不良信息:用户名："+name+",身份证号："+idCard);
			}
			resultXML = binding.querySingle(userName, password, datasource, Des2.encode(Des2.KEY, queryParam1));
			resultXML = Des2.decodeValue(Des2.KEY, resultXML);
	        /***************记录查询操作日志**********************/
			HttpSession session = ServletActionContext.getRequest().getSession();
			User user=(User) session.getAttribute("user");
			Date date = new Date();
			log.setOperatorId(user.getId());
			log.setOperatorName(user.getName());
			log.setOperateDate(date);
			log.setOperateRemark(SystemOperateLogUtil.DOT_DATA_VIEW);
			log.setRecordId(idCard);
			log.setKeyword(name+":"+idCard);
			systemOperateLogService.addSysOperateLogRecord(log);
//			JSONObject jsonObject = JSONObject.fromObject(map);
			result=Ajax.JSONResult(0,"操作成功",resultXML);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}


	public SubscriberConfirmService getSubscriberConfirmService() {
		return subscriberConfirmService;
	}


	public void setSubscriberConfirmService(
			SubscriberConfirmService subscriberConfirmService) {
		this.subscriberConfirmService = subscriberConfirmService;
	}


	public SubscriberConfirm getSubscriberConfirm() {
		return subscriberConfirm;
	}


	public void setSubscriberConfirm(SubscriberConfirm subscriberConfirm) {
		this.subscriberConfirm = subscriberConfirm;
	}


	public SubscriberService getSubscriberService() {
		return subscriberService;
	}


	public void setSubscriberService(SubscriberService subscriberService) {
		this.subscriberService = subscriberService;
	}


	public SystemOperateLogService getSystemOperateLogService() {
		return systemOperateLogService;
	}


	public void setSystemOperateLogService(
			SystemOperateLogService systemOperateLogService) {
		this.systemOperateLogService = systemOperateLogService;
	}


	public SubReviewLogSerive getSubReviewLogSerive() {
		return subReviewLogSerive;
	}


	public void setSubReviewLogSerive(SubReviewLogSerive subReviewLogSerive) {
		this.subReviewLogSerive = subReviewLogSerive;
	}
	
	/*public static void main(String[] args) throws Exception{
		String xml = "<?xml version='1.0' encoding='UTF-8'?>"+
                "<data>"+
                   "<message>"+
                      "<status>0</status>"+
                      "<value>处理成功</value>"+
                      "</message>"+
                      "<policeCheckInfos>"+
                   "<policeCheckInfo name='化希琨' id='62042319900328281X'>"+
                   "<message>"+
                      "<status>0</status>"+
                      "<value>查询成功</value>"+
                      "</message>"+
                      "<name desc='姓名'>化希琨</name>"+
                   "<identitycard desc='身份证号'>62042319900328281X</identitycard>"+
                "<compStatus desc='比对状态'>3</compStatus>"+
                "<compResult desc='比对结果'>一致</compResult>"+
                "<no desc='唯一标识' />"+
                "</policeCheckInfo>"+
                "</policeCheckInfos>"+
                "</data>";
		
		File file =new File("D:\\dataTest");    
		//如果文件夹不存在则创建    
		if  (!file .exists()  && !file .isDirectory())      
		{       
		    System.out.println("//不存在");  
		    file .mkdir();    
		} else   
		{  
		    System.out.println("//目录存在");  
		}  
		
//		memberreview
		File file1=new File("D:\\dataTest\\netDataToLocalFile.xml");    
		if(!file1.exists())    
		{    
		    try {    
		        file1.createNewFile(); 
		        System.out.println(file1.getPath());
		    } catch (IOException e) {    
		        // TODO Auto-generated catch block    
		        e.printStackTrace();    
		    }    
		}    
		
//		org.dom4j.Document doc = XmlTool.getDocument("D:\\dataTest\\netDataToLocalFile.xml");
//		System.out.println(doc);
//		String text = doc.asXML();
//		System.out.println(text);
		
		
		
		
//		String LOCAL_PC_SAVEFILE_URL = "D:\\dataTest\\netDataToLocalFile.xml";
//    	Document document =  DuXMLDoc.getProvinceCode(xml);
//    	DuXMLDoc.helloOK(document, LOCAL_PC_SAVEFILE_URL);
    }*/
	
}
