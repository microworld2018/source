package com.dearho.cs.subscriber.pojo;

import java.io.Serializable;
import java.util.Date;


/**会员审核记录实体类
 * @author Gaopl
 * @createDate 2017年4月27日
 */
public class SubReviewLog  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 会员审核记录ID
	 */
	private String id;
	
	/**
	 * 会员ID
	 */
	private String subId;
	
	/**
	 * 会员身份证号
	 */
	private String idCard;
	
	/**
	 * 审核人ID
	 */
	private String reviewUserId;
	
	/**
	 * 审核人姓名
	 */
	private String reviewUserName;
	
	/**
	 * 锁定状态0：正常 4：半锁 5：全锁
	 */
	private Integer lockedState;
	
	/**
	 * 审核不通过原因
	 */
	private String reviewReason;
	
	/**
	 * 审核时间
	 */
	private Date reviewTime;
	
	/**
	 * 审核是否通过0：审核不通过 1：审核通过
	 */
	private Integer reciewIsPassing;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public String getReviewUserId() {
		return reviewUserId;
	}

	public void setReviewUserId(String reviewUserId) {
		this.reviewUserId = reviewUserId;
	}

	public String getReviewUserName() {
		return reviewUserName;
	}

	public void setReviewUserName(String reviewUserName) {
		this.reviewUserName = reviewUserName;
	}

	public Integer getLockedState() {
		return lockedState;
	}

	public void setLockedState(Integer lockedState) {
		this.lockedState = lockedState;
	}

	public String getReviewReason() {
		return reviewReason;
	}

	public void setReviewReason(String reviewReason) {
		this.reviewReason = reviewReason;
	}

	public Date getReviewTime() {
		return reviewTime;
	}

	public void setReviewTime(Date reviewTime) {
		this.reviewTime = reviewTime;
	}

	public Integer getReciewIsPassing() {
		return reciewIsPassing;
	}

	public void setReciewIsPassing(Integer reciewIsPassing) {
		this.reciewIsPassing = reciewIsPassing;
	}

}
