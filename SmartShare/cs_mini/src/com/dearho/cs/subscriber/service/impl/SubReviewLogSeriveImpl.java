package com.dearho.cs.subscriber.service.impl;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.subscriber.dao.SubReviewLogDao;
import com.dearho.cs.subscriber.pojo.SubReviewLog;
import com.dearho.cs.subscriber.service.SubReviewLogSerive;

public class SubReviewLogSeriveImpl implements SubReviewLogSerive {
	
	private SubReviewLogDao subReviewLogDao;

	public SubReviewLogDao getSubReviewLogDao() {
		return subReviewLogDao;
	}

	public void setSubReviewLogDao(SubReviewLogDao subReviewLogDao) {
		this.subReviewLogDao = subReviewLogDao;
	}

	@Override
	public Page<SubReviewLog> getPageSubReviewLog(Page<SubReviewLog> page,
			SubReviewLog subReviewLog) throws Exception {
		return subReviewLogDao.getPageSubReviewLog(page, subReviewLog);
	}

	@Override
	public List<SubReviewLog> getListSubReviewLog(SubReviewLog subReviewLog)
			throws Exception {
		return subReviewLogDao.getListSubReviewLog(subReviewLog);
	}

	@Override
	public void saveSubReviewLog(SubReviewLog subReviewLog) throws Exception {
		subReviewLogDao.saveSubReviewLog(subReviewLog);
	}

}
