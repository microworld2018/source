package com.dearho.cs.place.action;

import java.util.Date;
import java.util.List;

import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.service.OrdersService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.pojo.CarDotBinding;
import com.dearho.cs.place.service.CarDotBindingService;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.Constants;
import com.opensymphony.xwork.Action;

/**
 * @author GaoYunpeng
 * @Description 
 * @version 1.0 2015年4月22日 上午10:30:49
 */
public class BranchDotUpdateAction extends BranchDotAddAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String code;
	
	private String id;
	
	private OrdersService ordersService;
	
	
	public String getCode() {
		return code;
	}



	public void setCode(String code) {
		this.code = code;
	}

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public OrdersService getOrdersService() {
		return ordersService;
	}



	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}



	public String process(){
		try {
			BranchDot hasDot = getBranchDotService().getBranchDotByCode(code);
			if(hasDot != null && !hasDot.getId().equals(getBranchDot().getId())){
				result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "更新失败！编码重复！");
				return Action.ERROR;
			}
			BranchDot updateDot = getBranchDot();
			if(updateDot.getIsActive() == 0){
				//查询该网点下是否有车辆
				List<CarDotBinding> list = getCarDotBindingService().searchBindingByDotId(hasDot.getId(), 1);
				//查询正在进行的订单是否使用该网点还车
				Orders orders = new Orders();
				orders.setOrdersBackSiteId(updateDot.getId());
				orders.setState("3");
				List<Orders> ordersList = ordersService.queryOrdersByCon(orders);
				if(list != null && list.size() > 0){
					result = Ajax.JSONResult(1, "网点下有车辆不能停用！");
				}else if(ordersList != null && ordersList.size() > 0){
					result = Ajax.JSONResult(1, "有未结束的订单使用此网点还车不能停用！");
				}else{
					updateDot.setUpdateTime(new Date());
					updateDot.setCreateTime(hasDot.getCreateTime());
					updateDot.setCreatorId(hasDot.getCreatorId());
					updateDot.setTs(new Date());
					updateDot.setDeleteState(1);
					getBranchDotService().updateBranchDot(updateDot);
					result = Ajax.JSONResult(Constants.RESULT_CODE_SUCCESS, "更新成功");
				}
			}else{
				updateDot.setUpdateTime(new Date());
				updateDot.setCreateTime(hasDot.getCreateTime());
				updateDot.setCreatorId(hasDot.getCreatorId());
				updateDot.setTs(new Date());
				updateDot.setDeleteState(1);
				getBranchDotService().updateBranchDot(updateDot);
				result = Ajax.JSONResult(Constants.RESULT_CODE_SUCCESS, "更新成功");
			}
			
			return Action.SUCCESS;
		} catch (Exception e) {
			result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "更新失败！");
			return Action.ERROR;
		}
	}
	
	public String deleteBranchDot(){
		//查询该网点下是否有车辆
		List<CarDotBinding> list = getCarDotBindingService().searchBindingByDotId(id, 0);
		//查询正在进行的订单是否使用该网点还车
		Orders orders = new Orders();
		orders.setOrdersBackSiteId(id);
		orders.setState("3");
		List<Orders> ordersList = ordersService.queryOrdersByCon(orders);
		if(list != null && list.size() > 0){
			result = Ajax.JSONResult(1, "网点下有车辆不能进行删除！");
		}else if(ordersList != null && ordersList.size() > 0){
			result = Ajax.JSONResult(1, "有未结束的订单使用此网点还车不能删除！");
		}else{
			BranchDot hasDot = getBranchDotService().getBranchDotById(id);
			hasDot.setDeleteState(0);
			hasDot.setIsActive(0);
			getBranchDotService().updateBranchDot(hasDot);
			result = Ajax.JSONResult(0, "网点删除成功！");
		}
		return SUCCESS;
	}
}
