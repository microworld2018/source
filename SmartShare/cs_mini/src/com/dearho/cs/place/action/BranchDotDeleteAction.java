package com.dearho.cs.place.action;

import java.util.Date;
import java.util.List;

import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.service.OrdersService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.pojo.CarDotBinding;
import com.dearho.cs.place.service.BranchDotService;
import com.dearho.cs.place.service.CarDotBindingService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.Constants;
import com.dearho.cs.util.StringHelper;
import com.opensymphony.xwork.Action;


/**
 * @author GaoYunpeng
 * @Description 
 * @version 1.0 2015年4月22日 上午10:22:43
 */
public class BranchDotDeleteAction extends AbstractAction{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String[] checkdel;
	
	private BranchDotService branchDotService;
	
	private OrdersService ordersService;
	
	private CarDotBindingService carDotBindingService;
	
	private String id;
	

	@Override
	public String process() {
		try {
			if(StringHelper.isNotEmpty(id)){
				//查询该网点下是否有车辆
				List<CarDotBinding> list = carDotBindingService.searchBindingByDotId(id, 1);
				//查询正在进行的订单是否使用该网点还车
				Orders orders = new Orders();
				orders.setOrdersBackSiteId(id);
				orders.setState("3");
				List<Orders> ordersList = ordersService.queryOrdersByCon(orders);
				if(list != null && list.size() > 0){
					result = Ajax.JSONResult(1, "","网点下有车辆不能进行删除！");
				}else if(ordersList != null && ordersList.size() > 0){
					result = Ajax.JSONResult(1, "","有未结束的订单使用此网点还车不能删除！");
				}else{
					BranchDot hasDot = getBranchDotService().getBranchDotById(id);
					hasDot.setDeleteState(0);
					hasDot.setIsActive(0);
					getBranchDotService().updateBranchDot(hasDot);
					result = Ajax.JSONResult(0, "","网点删除成功！");
				}
			}
			return Action.SUCCESS;
		} catch (Exception e) {
			result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED,"", "删除失败！");
			return Action.ERROR;
		}
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String[] getCheckdel() {
		return checkdel;
	}

	public void setCheckdel(String[] checkdel) {
		this.checkdel = checkdel;
	}

	public BranchDotService getBranchDotService() {
		return branchDotService;
	}

	public void setBranchDotService(BranchDotService branchDotService) {
		this.branchDotService = branchDotService;
	}

	public OrdersService getOrdersService() {
		return ordersService;
	}

	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}

	public CarDotBindingService getCarDotBindingService() {
		return carDotBindingService;
	}

	public void setCarDotBindingService(CarDotBindingService carDotBindingService) {
		this.carDotBindingService = carDotBindingService;
	}

	
}
