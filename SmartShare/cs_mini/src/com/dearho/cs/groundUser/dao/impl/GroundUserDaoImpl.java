package com.dearho.cs.groundUser.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.groundUser.dao.GroundUserDao;
import com.dearho.cs.groundUser.pojo.GroundUser;
import com.dearho.cs.subDot.pojo.SubDot;
import com.dearho.cs.subscriber.pojo.Subscriber;

public class GroundUserDaoImpl extends AbstractDaoSupport implements GroundUserDao {

	@Override
	public GroundUser queryGroundUserById(String id) {
		return get(GroundUser.class, id);
	}

	@Override
	public Page<GroundUser> queryGroundUserByPage(Page<GroundUser> page, GroundUser groundUser) {
		StringBuffer hql = new StringBuffer();
		hql.append("select a.id from GroundUser a,SubDot s where 1 = 1 and a.isUse = 0");
		if(groundUser != null){
			if(StringUtils.isNotEmpty(groundUser.getPhone())){
				hql.append(" and a.phone like '%"+groundUser.getPhone()+"%'");
			}
			if(StringUtils.isNotEmpty(groundUser.getName())){
				hql.append(" and a.name like '%"+groundUser.getName()+"%'");
			}
			if(StringUtils.isNotEmpty(groundUser.getSubDotName())){
				hql.append(" and s.dotName like '%" + groundUser.getSubDotName() + "%'");
				hql.append(" and a.id = s.userId");
			}
		}
		hql.append(" group by a.id");
		Page<GroundUser> resultPage=pageCache(GroundUser.class, page, hql.toString());
		resultPage.setResults(idToObj(GroundUser.class, resultPage.getmResults()));
		return resultPage;
	}

	@Override
	public void saveGroundUser(GroundUser gUser) {
		addEntity(gUser);
	}

	@Override
	public void updateGroundUser(GroundUser gUser) {
		updateEntity(gUser);
	}

	@Override
	public void deleteGroundUser(GroundUser gUser) {
		updateEntity(gUser);
	}

	@Override
	public List<GroundUser> queryGroundUserList(GroundUser groundUser) {
		StringBuffer sql = new StringBuffer("SELECT * FROM (SELECT id, `name` , `phone`,"
	                                        + " (SELECT GROUP_CONCAT(`dot_name`  SEPARATOR ',') "
				                            + " FROM `q_sub_dot` d WHERE `user_id` = qu.`id`) AS dot_name,is_use" 
	                                        + " FROM `q_user` qu ) e where 1 = 1 and e.is_use = 0");
		if(groundUser != null){
			if(StringUtils.isNotEmpty(groundUser.getPhone())){
				sql.append(" and e.phone like '%"+groundUser.getPhone()+"%'");
			}
			if(StringUtils.isNotEmpty(groundUser.getName())){
				sql.append(" and e.name like '%"+groundUser.getName()+"%'");
			}
			if(StringUtils.isNotEmpty(groundUser.getSubDotName())){
				sql.append(" and e.dot_name like '%" + groundUser.getSubDotName() + "%'");
			}
		}
		List<GroundUser> gUserList = new ArrayList<>();
		List<Object> groundUserList = getSession().createSQLQuery(sql.toString()).list();
		if(groundUserList != null && groundUserList.size() > 0){
			for(int i = 0;i < groundUserList.size();i++){
				GroundUser gUser = new GroundUser();
				Object[] o = (Object[]) groundUserList.get(i);
				gUser.setId(o[0].toString());
				gUser.setName(o[1].toString());
				gUser.setPhone(o[2].toString());
				gUserList.add(gUser);
			}
		}
		return gUserList;
	}

	@Override
	public List<GroundUser> queryGroundUserByPhone(String phone) {
		List<GroundUser> list = search(GroundUser.class, "select a.id from GroundUser a where isUse = 0 and phone = '" + phone + "'");
		return list;
	}

}





