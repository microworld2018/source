package com.dearho.cs.groundUser.dao;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.groundUser.pojo.GroundUser;

public interface GroundUserDao {

	GroundUser queryGroundUserById(String id);

	Page<GroundUser> queryGroundUserByPage(Page<GroundUser> page, GroundUser groundUser);

	void saveGroundUser(GroundUser gUser);

	void updateGroundUser(GroundUser gUser);

	void deleteGroundUser(GroundUser gUser);

	List<GroundUser> queryGroundUserList(GroundUser groundUser);

	List<GroundUser> queryGroundUserByPhone(String phone);

}
