package com.dearho.cs.groundUser.pojo;

import java.io.Serializable;
import java.util.List;

import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.subDot.pojo.SubDot;

/**
 * 地勤人员
 * 
 * @author Jin Guangyu
 *
 */
public class GroundUser implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;

	private String password;

	private String phone;

	private String name;

	private Integer isUse = 0;

	private List<SubDot> subDotList;// 网点集合

	private String subDotName;// 网点名称

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getIsUse() {
		return isUse;
	}

	public void setIsUse(Integer isUse) {
		this.isUse = isUse;
	}

	public List<SubDot> getSubDotList() {
		return subDotList;
	}

	public void setSubDotList(List<SubDot> subDotList) {
		this.subDotList = subDotList;
	}

	public String getSubDotName() {
		return subDotName;
	}

	public void setSubDotName(String subDotName) {
		this.subDotName = subDotName;
	}

}
