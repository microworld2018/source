package com.dearho.cs.groundUser.action;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.coupon.pojo.SubCoupon;
import com.dearho.cs.groundUser.pojo.GroundUser;
import com.dearho.cs.groundUser.service.GroundUserService;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.service.BranchDotService;
import com.dearho.cs.subDot.pojo.SubDot;
import com.dearho.cs.subDot.service.SubDotService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.Group;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.AreaService;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.Constants;
import com.dearho.cs.util.Sha1Util;
import com.dearho.cs.util.StringHelper;
import com.opensymphony.xwork.Action;

import net.sf.json.util.JSONUtils;

public class GroundUserAction extends AbstractAction {

	private GroundUserService groundUserService;

	private SubDotService subDotService;

	private BranchDotService branchDotService;

	private AreaService areaService;

	private List<AdministrativeArea> areas;

	private Page<GroundUser> page = new Page<>();

	private GroundUser groundUser;

	private SubDot subDot;

	private String id;// 地勤人员表id

	private String state;

	private String name;// 地勤人员名称

	private String phone;// 手机号

	private String subDotName;// 网点名称

	private List<GroundUser> groundUserList = new ArrayList<>();

	@Override
	public String process() {
		if ("order".equals(state)) {
			groundUser.setIsUse(0);
			page = groundUserService.queryGroundUserByPage(page, groundUser);
			return "order";
		}
		return SUCCESS;
	}

	/**
	 * 地勤人员列表展示
	 * 
	 * @return
	 */
	public String groundUserList() {
		if (StringHelper.isNotEmpty(name)) {
			groundUser.setName(name);
		}
		if (StringHelper.isNotEmpty(phone)) {
			groundUser.setPhone(phone);
		}
		if (StringHelper.isNotEmpty(subDotName)) {
			groundUser.setSubDotName(subDotName);
		}
		// page = groundUserService.queryGroundUserByPage(page, groundUser);
		groundUserList = groundUserService.queryGroundUserList(groundUser);

		if (groundUserList != null && groundUserList.size() > 0) {
			for (GroundUser gUser : groundUserList) {
				if (gUser.getId() != null) {
					List<SubDot> subDotList = subDotService.querySubDotByGroundUserId(gUser.getId());
					gUser.setSubDotList(subDotList);
				}
			}
		}

		/*
		 * if (page != null && page.getResults() != null) { for (Object obj :
		 * page.getResults()) { GroundUser gUser = (GroundUser) obj; if
		 * (gUser.getId() != null) { List<SubDot> subDotList =
		 * subDotService.querySubDotByGroundUserId(gUser.getId());
		 * gUser.setSubDotList(subDotList); } } }
		 */

		return SUCCESS;
	}

	/**
	 * 打开 添加或编辑地勤人员页
	 * 
	 * @return
	 */
	public String groundUserGet() {
		if (id == null || id.trim().equals("")) {
			groundUser = new GroundUser();
			getRequest().setAttribute("subDotList", new net.sf.json.JSONArray());
		} else {
			groundUser = groundUserService.queryGroundUserById(id);
			List<SubDot> subDotList = subDotService.querySubDotByGroundUserId(id);
			net.sf.json.JSONArray jsonArray = net.sf.json.JSONArray.fromObject(subDotList);
			if (jsonArray != null) {
				getRequest().setAttribute("subDotList", jsonArray);
			}
			if (groundUser == null)
				groundUser = new GroundUser();
		}
		areas = areaService.searchAreaByCode(null);
		List<BranchDot> branchDotList = branchDotService.queryAllBranchDot();
		getRequest().setAttribute("branchDotList", branchDotList);

		return SUCCESS;
	}

	/**
	 * 添加地勤人员
	 */
	public String groundUserAdd() {
		try {
			String[] branchDotDoc = getRequest().getParameterValues("branchDotDoc");
			List<GroundUser> list = groundUserService.queryGroundUserByPhone(groundUser.getPhone());
			if (list != null && list.size() > 0) {
				result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "手机号已存在！");
				return Action.ERROR;
			}
			groundUserService.saveGroundUser(groundUser, branchDotDoc);
			result = Ajax.JSONResult(Constants.RESULT_CODE_SUCCESS, groundUser.getId());
			return Action.SUCCESS;
		} catch (Exception e) {
			result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "地勤用户保存失败！");
			return Action.ERROR;
		}
	}

	/**
	 * 编辑地勤人员
	 */
	public String groundUserUpdate() {
		try {
			String[] branchDotDoc = getRequest().getParameterValues("branchDotDoc");
			GroundUser gUser = groundUserService.queryGroundUserById(groundUser.getId());
			gUser.setName(groundUser.getName());
			gUser.setPassword(groundUser.getPassword());
			gUser.setPhone(groundUser.getPhone());
			groundUserService.updateGroundUser(gUser, branchDotDoc);
			result = Ajax.JSONResult(Constants.RESULT_CODE_SUCCESS, groundUser.getId());
			return Action.SUCCESS;
		} catch (Exception e) {
			result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "用户更新失败！");
			return Action.ERROR;
		}
	}

	/**
	 * 删除地勤人员
	 * 
	 * @return
	 */
	public String deleteGroundUser() {
		try {
			GroundUser gUser = groundUserService.queryGroundUserById(id);
			if (gUser != null) {
				gUser.setIsUse(1);
			}
			groundUserService.deleteGroundUser(gUser);
			result = Ajax.JSONResult(Constants.RESULT_CODE_SUCCESS, "删除成功！");
			return SUCCESS;
		} catch (Exception e) {
			result = Ajax.JSONResult(Constants.RESULT_CODE_FAILED, "删除失败！");
			return ERROR;
		}
	}

	/**
	 * 根据区域id查名字
	 * 
	 * @param id
	 * @return
	 */
	public String getAreaName(String id) {
		if (areas != null && areas.size() > 0) {
			for (AdministrativeArea a : areas) {
				if (a.getId().equals(id)) {
					return a.getName();
				}
			}
		}
		return "";
	}

	public GroundUserAction() {
		super();
		groundUser = new GroundUser();
		subDot = new SubDot();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}

	public GroundUserService getGroundUserService() {
		return groundUserService;
	}

	public void setGroundUserService(GroundUserService groundUserService) {
		this.groundUserService = groundUserService;
	}

	public Page<GroundUser> getPage() {
		return page;
	}

	public void setPage(Page<GroundUser> page) {
		this.page = page;
	}

	public GroundUser getGroundUser() {
		return groundUser;
	}

	public void setGroundUser(GroundUser groundUser) {
		this.groundUser = groundUser;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public SubDotService getSubDotService() {
		return subDotService;
	}

	public void setSubDotService(SubDotService subDotService) {
		this.subDotService = subDotService;
	}

	public String getSubDotName() {
		return subDotName;
	}

	public void setSubDotName(String subDotName) {
		this.subDotName = subDotName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BranchDotService getBranchDotService() {
		return branchDotService;
	}

	public void setBranchDotService(BranchDotService branchDotService) {
		this.branchDotService = branchDotService;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<AdministrativeArea> getAreas() {
		return areas;
	}

	public void setAreas(List<AdministrativeArea> areas) {
		this.areas = areas;
	}

	public AreaService getAreaService() {
		return areaService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public SubDot getSubDot() {
		return subDot;
	}

	public void setSubDot(SubDot subDot) {
		this.subDot = subDot;
	}

	public List<GroundUser> getGroundUserList() {
		return groundUserList;
	}

	public void setGroundUserList(List<GroundUser> groundUserList) {
		this.groundUserList = groundUserList;
	}

}
