package com.dearho.cs.groundUser.service;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.groundUser.pojo.GroundUser;

public interface GroundUserService {

	GroundUser queryGroundUserById(String id);

	Page<GroundUser> queryGroundUserByPage(Page<GroundUser> page, GroundUser groundUser);

	void saveGroundUser(GroundUser groundUser, String[] branchDotDoc);

	void updateGroundUser(GroundUser gUser, String[] branchDotDoc);

	void deleteGroundUser(GroundUser gUser);

	List<GroundUser> queryGroundUserList(GroundUser groundUser);

	List<GroundUser> queryGroundUserByPhone(String phone);


}
