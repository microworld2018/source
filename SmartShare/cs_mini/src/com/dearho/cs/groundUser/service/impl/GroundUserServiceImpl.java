package com.dearho.cs.groundUser.service.impl;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.groundUser.dao.GroundUserDao;
import com.dearho.cs.groundUser.pojo.GroundUser;
import com.dearho.cs.groundUser.service.GroundUserService;
import com.dearho.cs.place.dao.BranchDotDAO;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.subDot.dao.SubDotDao;
import com.dearho.cs.subDot.pojo.SubDot;

public class GroundUserServiceImpl implements GroundUserService {

	private GroundUserDao groundUserDao;

	private SubDotDao subDotDao;

	private BranchDotDAO branchDotDAO;

	@Override
	public GroundUser queryGroundUserById(String id) {
		return groundUserDao.queryGroundUserById(id);
	}

	@Override
	public Page<GroundUser> queryGroundUserByPage(Page<GroundUser> page, GroundUser groundUser) {
		return groundUserDao.queryGroundUserByPage(page, groundUser);
	}

	@Override
	public void saveGroundUser(GroundUser gUser, String[] branchDotDoc) {
		groundUserDao.saveGroundUser(gUser);
		if (branchDotDoc != null) {
			for (int i = 0; i < branchDotDoc.length; i++) {
				BranchDot dot = branchDotDAO.getBranchDotById(branchDotDoc[i]);
				if (dot != null) {
					SubDot subDot = new SubDot();
					subDot.setUserId(gUser.getId());
					subDot.setDotId(branchDotDoc[i]);
					subDot.setDotName(dot.getName());
					subDot.setDotAddr(dot.getAddress());
					subDot.setDotLat(dot.getLat());
					subDot.setDotLon(dot.getLng());
					subDot.setUserName(gUser.getName());
					subDot.setUserPhone(gUser.getPhone());
					subDotDao.saveSubDot(subDot);
				}
			}
		}
	}

	@Override
	public void updateGroundUser(GroundUser gUser, String[] branchDotDoc) {
		groundUserDao.updateGroundUser(gUser);
		//删除原来关联网点信息
		subDotDao.deleteSubDot(gUser);
		//保存当前关联网点信息
		if (branchDotDoc != null) {
			for (int i = 0; i < branchDotDoc.length; i++) {
				BranchDot dot = branchDotDAO.getBranchDotById(branchDotDoc[i]);
				if (dot != null) {
					SubDot subDot = new SubDot();
					subDot.setUserId(gUser.getId());
					subDot.setDotId(branchDotDoc[i]);
					subDot.setDotName(dot.getName());
					subDot.setDotAddr(dot.getAddress());
					subDot.setDotLat(dot.getLat());
					subDot.setDotLon(dot.getLng());
					subDot.setUserName(gUser.getName());
					subDot.setUserPhone(gUser.getPhone());
					subDotDao.saveSubDot(subDot);
				}
			}
		}
	}
	
	@Override
	public void deleteGroundUser(GroundUser gUser) {
		groundUserDao.deleteGroundUser(gUser);
	}

	public GroundUserDao getGroundUserDao() {
		return groundUserDao;
	}

	public void setGroundUserDao(GroundUserDao groundUserDao) {
		this.groundUserDao = groundUserDao;
	}

	public SubDotDao getSubDotDao() {
		return subDotDao;
	}

	public void setSubDotDao(SubDotDao subDotDao) {
		this.subDotDao = subDotDao;
	}

	public BranchDotDAO getBranchDotDAO() {
		return branchDotDAO;
	}

	public void setBranchDotDAO(BranchDotDAO branchDotDAO) {
		this.branchDotDAO = branchDotDAO;
	}

	@Override
	public List<GroundUser> queryGroundUserList(GroundUser groundUser) {
		return groundUserDao.queryGroundUserList(groundUser);
	}

	@Override
	public List<GroundUser> queryGroundUserByPhone(String phone) {
		return groundUserDao.queryGroundUserByPhone(phone);
	}


}
