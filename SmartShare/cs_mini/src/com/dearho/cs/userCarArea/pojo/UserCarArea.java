package com.dearho.cs.userCarArea.pojo;

import java.io.Serializable;

import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.User;

/**用户区域关联
 * @author Gaopl
 * @date 2016-09-28
 */
public class UserCarArea implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;		//用户车辆区域管理ID
	
	private String userId;	//用户ID
	
	private String areaId;	//区域ID
	
	private User user; 		//用户实体类
	
	private AdministrativeArea administrativeArea; //区域实体类
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public AdministrativeArea getAdministrativeArea() {
		return administrativeArea;
	}

	public void setAdministrativeArea(AdministrativeArea administrativeArea) {
		this.administrativeArea = administrativeArea;
	}

}
