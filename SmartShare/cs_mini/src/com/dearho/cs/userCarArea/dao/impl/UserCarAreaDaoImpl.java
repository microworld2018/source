package com.dearho.cs.userCarArea.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.userCarArea.dao.UserCarAreaDao;
import com.dearho.cs.userCarArea.pojo.UserCarArea;
import com.dearho.cs.util.StringHelper;

/**用户区域关联Dao接口实现类
 * @author Gaopl
 * @date 2016-09-28
 */
public class UserCarAreaDaoImpl extends AbstractDaoSupport implements UserCarAreaDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<UserCarArea> getByUserId(String userId) {
		String hql="select a.id from UserCarArea a where a.userId='"+userId+"'";
//		return getHibernateTemplate().find(hql);
		return getList(UserCarArea.class, queryFList(hql));
	}

	@Override
	public void addUserCarArea(UserCarArea userCarArea) {
		addEntity(userCarArea);
	}

	@Override
	public void updateUserCarArea(UserCarArea userCarArea) {
		updateEntity(userCarArea);
	}

	@Override
	public void deleteUserCarArea(String[] ids) {
		final String hql="delete UserCarArea where id in (:ids)";
		deleteEntity(UserCarArea.class, hql, ids);
	}

	@Override
	public Page<UserCarArea> getPageUserArea(Page<UserCarArea> page, String hql) {
		Page<UserCarArea> resultPage=pageCache(UserCarArea.class, page, hql);
		resultPage.setResults(idToObj(UserCarArea.class, resultPage.getmResults()));
		if(resultPage.getResults()!=null&&resultPage.getResults().size()>0){
			for (int i = 0; i < resultPage.getResults().size(); i++) {
				UserCarArea userArea=(UserCarArea) resultPage.getResults().get(i);
				if(StringHelper.isNotEmpty(userArea.getUserId()) && StringHelper.isNotEmpty(userArea.getAreaId())){
					userArea.setUser(get(User.class,userArea.getUserId().trim()));
					userArea.setAdministrativeArea(get(AdministrativeArea.class,userArea.getAreaId().trim()));
				}
			}
		}
		return resultPage;
	}

	@Override
	public UserCarArea getUserAreaById(String id) {
		return get(UserCarArea.class,id);
	}

}
