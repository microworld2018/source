package com.dearho.cs.userCarArea.dao;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.userCarArea.pojo.UserCarArea;

/**用户区域关联Dao接口层
 * @author Gaopl
 * @date 2016-09-28
 */
public interface UserCarAreaDao {
	
	/**根据用户ID查询当前用户所管理的区域
	 * @param userId 用户ID
	 * @return
	 */
	public List<UserCarArea> getByUserId(String userId);
	
	
	/**增加用户区域关联管理
	 * @param userCarArea
	 */
	public void addUserCarArea(UserCarArea userCarArea);
	
	
	/**更新用户区域关联关系
	 * @param userCarArea
	 */
	public void updateUserCarArea(UserCarArea userCarArea);
	
	
	/**根据ID删除用户区域关联关系
	 * @param id
	 */
	public void deleteUserCarArea(String[] ids);
	
	
	/**分页获取用户和区域的关系
	 * @param userArea
	 * @return
	 */
	public Page<UserCarArea> getPageUserArea(Page<UserCarArea> page, String hql);
	
	
	/**根据ID获取用户区域关联关系
	 * @param id 用户区域关联ID
	 * @return
	 */
	public UserCarArea getUserAreaById(String id);
}
