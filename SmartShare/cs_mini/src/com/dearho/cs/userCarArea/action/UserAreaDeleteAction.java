package com.dearho.cs.userCarArea.action;

import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.userCarArea.service.UserCarAreaService;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.Constants;

/**删除用户区域关联关系
 * @author Gaopl
 * @date 2016-09-30
 */
public class UserAreaDeleteAction extends AbstractAction{
	
	private static final long serialVersionUID = 1L;
	
	private UserCarAreaService userCarAreaService;//用户区域关联Service
	
	private String[] checkdel;//用户区域关联ID
	
	public void setUserCarAreaService(UserCarAreaService userCarAreaService) {
		this.userCarAreaService = userCarAreaService;
	}

	public String[] getCheckdel() {
		return checkdel;
	}

	public void setCheckdel(String[] checkdel) {
		this.checkdel = checkdel;
	}





	@Override
	public String process() {
		try {
			userCarAreaService.deleteUserCarArea(checkdel);
			result=Ajax.JSONResult(0, "删除成功！");
		} catch (Exception e) {
			result=Ajax.JSONResult(1, "删除失败！");
		}
		
		return SUCCESS;
	}

}
