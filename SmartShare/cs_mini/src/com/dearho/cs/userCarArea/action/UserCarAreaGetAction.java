package com.dearho.cs.userCarArea.action;

import java.util.List;

import org.apache.cxf.common.util.StringUtils;

import com.ctc.wstx.util.StringUtil;
import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.pojo.CarVehicleModel;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.car.service.CarVehicleModelService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.pojo.CarDotBinding;
import com.dearho.cs.place.service.BranchDotService;
import com.dearho.cs.place.service.CarDotBindingService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.util.DictUtil;
import com.dearho.cs.userCarArea.pojo.UserCarArea;
import com.dearho.cs.userCarArea.service.UserCarAreaService;
import com.dearho.cs.util.Constants;
import com.dearho.cs.util.StringHelper;
import com.opensymphony.xwork.Action;

public class UserCarAreaGetAction extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	private UserCarAreaService userCarAreaService;//用户区域车辆管理Service
	
	private BranchDotService branchDotService;//网点Service
	
	private CarDotBindingService carDotBindingService;//车辆网点绑定关系Service
	
	private CarService carService;//车辆Service
	
	private CarVehicleModelService carVehicleModelService;//车辆型号Service
	
	private Page<Car> page=new Page<Car>();//分页
	
	private Car car;//车辆实体类
	
	

	public void setUserCarAreaService(UserCarAreaService userCarAreaService) {
		this.userCarAreaService = userCarAreaService;
	}
	
	public void setBranchDotService(BranchDotService branchDotService) {
		this.branchDotService = branchDotService;
	}

	public void setCarDotBindingService(CarDotBindingService carDotBindingService) {
		this.carDotBindingService = carDotBindingService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public void setPage(Page<Car> page) {
		this.page = page;
	}

	public Page<Car> getPage() {
		return page;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}
	
	public UserCarAreaGetAction() {
		super();
		car=new Car();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}
	
	public CarVehicleModelService getCarVehicleModelService() {
		return carVehicleModelService;
	}

	public void setCarVehicleModelService(
			CarVehicleModelService carVehicleModelService) {
		this.carVehicleModelService = carVehicleModelService;
	}

	public List<CarVehicleModel> getAllModel(String brand){
		CarVehicleModel model = null;
		if(!StringHelper.isRealNull(brand)){
			
			if(brand.equals("brand") && car != null && car.getCarVehicleModel() != null){
				model = car.getCarVehicleModel();
			}
			else{
				model = new CarVehicleModel();
				model.setBrand(brand);
			}
			
		}
		List<CarVehicleModel> carVehicleModels = carVehicleModelService.queryModelByCon(model);
		for (CarVehicleModel cm : carVehicleModels) {
			cm.setEngineTypeName(DictUtil.getCnNameByGroupCodeAndDictId("2", cm.getEngineType()));
		}
		return carVehicleModels;
	}
	
	
	
	

	@SuppressWarnings("null")
	@Override
	public String process() {
		User user=(User)getSession().getAttribute(Constants.SESSION_USER);
		//根据用户ID获取区域
		List<UserCarArea> userCarArea = userCarAreaService.getByUserId(user.getId());
		if(userCarArea != null){
			for(int u =0;u<userCarArea.size();u++){
				//根据区域ID获取网点
				List<BranchDot> branchDotList = branchDotService.getByAreaId(userCarArea.get(u).getId());
				if(branchDotList != null && branchDotList.size() > 0){
					StringBuffer ids = new StringBuffer();
					for(int i =0;i<branchDotList.size();i++){
						ids.append("'"+branchDotList.get(i).getId()+"'" + ",");
					}
					ids.deleteCharAt(ids.length() - 1);
					if(ids.toString() != ""){
						//根据网点ID查询网点下的车辆
						List<CarDotBinding> list = carDotBindingService.getByDotIds(ids.toString());
						if(list != null && list.size() > 0){
							StringBuffer idsArrStr = new StringBuffer();
							for(int i=0;i<list.size();i++){
								idsArrStr.append("'"+list.get(i).getCarId()+"'" + ",");
							}
							idsArrStr.deleteCharAt(idsArrStr.length() - 1);
							car.setIds(idsArrStr.toString());
							page = carService.queryCarByPage(page, car);
						}
					}
				}
			}
		}
		return SUCCESS;
	}

}
