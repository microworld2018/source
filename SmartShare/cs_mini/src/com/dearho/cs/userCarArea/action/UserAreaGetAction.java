package com.dearho.cs.userCarArea.action;


import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.AreaService;
import com.dearho.cs.sys.service.UserService;
import com.dearho.cs.userCarArea.pojo.UserCarArea;
import com.dearho.cs.userCarArea.service.UserCarAreaService;
import com.dearho.cs.util.StringHelper;

/**获取用户和对应的区域
 * @author Gaopl
 * @date 2016-09-29
 */
public class UserAreaGetAction extends AbstractAction{
	
	private static final long serialVersionUID = 1L;

	private Page<UserCarArea> page = new Page<UserCarArea>(); //分页设置
	
	private UserCarAreaService userCarAreaService;//用户区域车辆管理Service
	
	private UserService userService; //用户Service
	
	private AreaService areaService; //区域Service

	private UserCarArea userCarArea; //用户区域关系实体类
	
	private String userName;	//用户名
	
	private String areaName;    //区域名称
	
	public Page<UserCarArea> getPage() {
		return page;
	}

	public void setPage(Page<UserCarArea> page) {
		this.page = page;
	}

	public void setUserCarAreaService(UserCarAreaService userCarAreaService) {
		this.userCarAreaService = userCarAreaService;
	}
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public AreaService getAreaService() {
		return areaService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public UserCarArea getUserCarArea() {
		return userCarArea;
	}

	public void setUserCarArea(UserCarArea userCarArea) {
		this.userCarArea = userCarArea;
	}

	
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public UserAreaGetAction() {
		super();
		userCarArea = new UserCarArea();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}
	
	

	@Override
	public String process() {
		//根据用户名查询用户区域连关系
		if(StringHelper.isNotEmpty(userName)){
			User user = userService.searchUserByName(userName);
			if(user != null && StringHelper.isNotEmpty(user.getId())){
				userCarArea.setUserId(user.getId());
			}
		}
		//根据区域名称查询用户区域关联关系
		if(StringHelper.isNotEmpty(areaName)){
			List<AdministrativeArea> list = areaService.searchAreaByName(areaName);
			//AdministrativeArea area =  areaService.getAreaByName(areaName);
			if(list != null){
				userCarArea.setAreaId(list.get(0).getId());
			}
		}
		page = userCarAreaService.getPageUserArea(page, userCarArea);
		return SUCCESS;
	}

}
