package com.dearho.cs.userCarArea.action;

import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.userCarArea.pojo.UserCarArea;
import com.dearho.cs.userCarArea.service.UserCarAreaService;
import com.dearho.cs.util.Ajax;

/**更新用户区域关联关系
 * @author Gaopl
 * @date 2016-09-30
 */
public class UserAreaUpdateAction extends AbstractAction{
	
	private static final long serialVersionUID = 1L;
	
	
	private UserCarAreaService userCarAreaService;//用户区域关联Service
	
	private UserCarArea carArea;//用户区域关联实体类

	public UserCarAreaService getUserCarAreaService() {
		return userCarAreaService;
	}

	public void setUserCarAreaService(UserCarAreaService userCarAreaService) {
		this.userCarAreaService = userCarAreaService;
	}

	public UserCarArea getCarArea() {
		return carArea;
	}

	public void setCarArea(UserCarArea carArea) {
		this.carArea = carArea;
	}

	public UserAreaUpdateAction() {
		super();
		carArea = new UserCarArea();
	}

	@Override
	public String process() {
		try {
			userCarAreaService.updateUserCarArea(carArea);
			result=Ajax.JSONResult(0, "更新成功！");
		} catch (Exception e) {
			result=Ajax.JSONResult(1, "更新失败！");
		}
		return SUCCESS;
	}

}
