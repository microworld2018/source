package com.dearho.cs.userCarArea.action;

import java.util.List;

import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.Group;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.AreaService;
import com.dearho.cs.sys.service.GroupService;
import com.dearho.cs.sys.service.UserService;
import com.dearho.cs.userCarArea.pojo.UserCarArea;
import com.dearho.cs.userCarArea.service.UserCarAreaService;
import com.dearho.cs.util.Ajax;

/**新增用户区域关联关系
 * @author Gaopl
 * @date 2016-09-30
 */
public class UserAreaAddAction extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	private UserCarAreaService userCarAreaService;//用户区域关联Service
	
	private GroupService groupService;//用户组Service
	
	private AreaService areaService; //区域Service
	
	private UserService userService; //用户Service
	
	private UserCarArea carArea;//用户区域关联实体类
	
	public UserAreaAddAction() {
		super();
		carArea = new UserCarArea();
	}

	public void setGroupService(GroupService groupService) {
		this.groupService = groupService;
	}

	public void setUserCarAreaService(UserCarAreaService userCarAreaService) {
		this.userCarAreaService = userCarAreaService;
	}

	public UserCarArea getCarArea() {
		return carArea;
	}

	public void setCarArea(UserCarArea carArea) {
		this.carArea = carArea;
	}
	
	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	@Override
	public String process() {
		try {
			userCarAreaService.addUserCarArea(carArea);
			result=Ajax.JSONResult(0, "添加成功！");
		} catch (Exception e) {
			result=Ajax.JSONResult(1, "添加失败！");
		}
		return SUCCESS;
	}
	
	public String getUserArea(){
		/*List<Group> groupList=groupService.searchGroup(null);*/
		List<AdministrativeArea> areaList = areaService.searchAreaByCode(null);
		List<User> userList = userService.searchUser();
		/*getRequest().setAttribute("groupList", groupList);*/
		getRequest().setAttribute("areaList", areaList);
		getRequest().setAttribute("userList", userList);
		UserCarArea userArea = userCarAreaService.getUserAreaById(carArea.getId());
		getRequest().setAttribute("userArea", userArea);
		return SUCCESS;
	}

}
