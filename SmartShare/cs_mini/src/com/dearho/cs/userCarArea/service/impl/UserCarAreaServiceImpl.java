package com.dearho.cs.userCarArea.service.impl;


import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.userCarArea.dao.UserCarAreaDao;
import com.dearho.cs.userCarArea.pojo.UserCarArea;
import com.dearho.cs.userCarArea.service.UserCarAreaService;
import com.dearho.cs.util.StringHelper;

/**用户区域关联Service实现层
 * @author Gaopl
 * @date 2016-09-28
 */
public class UserCarAreaServiceImpl implements UserCarAreaService{
	
	private UserCarAreaDao userCarAreaDao;

	public void setUserCarAreaDao(UserCarAreaDao userCarAreaDao) {
		this.userCarAreaDao = userCarAreaDao;
	}

	@Override
	public List<UserCarArea> getByUserId(String userId) {
		// TODO Auto-generated method stub
		return userCarAreaDao.getByUserId(userId);
	}

	@Override
	public void addUserCarArea(UserCarArea userCarArea) {
		userCarAreaDao.addUserCarArea(userCarArea);
	}

	@Override
	public void updateUserCarArea(UserCarArea userCarArea) {
		userCarAreaDao.updateUserCarArea(userCarArea);
	}

	@Override
	public void deleteUserCarArea(String[] ids) {
		userCarAreaDao.deleteUserCarArea(ids);
	}

	@Override
	public Page<UserCarArea> getPageUserArea(Page<UserCarArea> page,UserCarArea userArea) {
		StringBuffer hql = new StringBuffer();
		hql.append("select a.id from UserCarArea a where 1=1");
		if(StringHelper.isNotEmpty(userArea.getAreaId())){
			hql.append(" and a.areaId = '"+userArea.getAreaId()+"'");
		}
		if(StringHelper.isNotEmpty(userArea.getUserId())){
			hql.append(" and a.userId = '"+userArea.getUserId()+"'");
		}
		page = userCarAreaDao.getPageUserArea(page, hql.toString());
		return page;
	}

	@Override
	public UserCarArea getUserAreaById(String id) {
		return userCarAreaDao.getUserAreaById(id);
	}
	
	
	
	
}
