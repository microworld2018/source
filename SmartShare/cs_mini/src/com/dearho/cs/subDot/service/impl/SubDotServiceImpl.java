package com.dearho.cs.subDot.service.impl;

import java.util.List;

import com.dearho.cs.subDot.dao.SubDotDao;
import com.dearho.cs.subDot.pojo.SubDot;
import com.dearho.cs.subDot.service.SubDotService;

public class SubDotServiceImpl implements SubDotService {

	private SubDotDao subDotDao;

	@Override
	public List<SubDot> querySubDotByGroundUserId(String id) {
		return subDotDao.querySubDotByGroundUserId(id);
	}

	public SubDotDao getSubDotDao() {
		return subDotDao;
	}

	public void setSubDotDao(SubDotDao subDotDao) {
		this.subDotDao = subDotDao;
	}

	@Override
	public void saveSubDot(SubDot subDot) {
		subDotDao.saveSubDot(subDot);
	}

}
