package com.dearho.cs.subDot.service;

import java.util.List;

import com.dearho.cs.subDot.pojo.SubDot;

public interface SubDotService {

	List<SubDot> querySubDotByGroundUserId(String id);

	void saveSubDot(SubDot subDot);

}
