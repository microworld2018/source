package com.dearho.cs.subDot.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.groundUser.pojo.GroundUser;
import com.dearho.cs.subDot.dao.SubDotDao;
import com.dearho.cs.subDot.pojo.SubDot;

public class SubDotDaoImpl extends AbstractDaoSupport implements SubDotDao {

	@Override
	public List<SubDot> querySubDotByGroundUserId(String id) {
		StringBuffer sb = new StringBuffer();
		sb.append("select a.id from SubDot a where a.userId = ");
		if(id != null){
			sb.append("'" + id + "'");
		}
		return getList(SubDot.class, queryFList(sb.toString()));
	}

	@Override
	public void saveSubDot(SubDot subDot) {
		addEntity(subDot);
	}

	@Override
	public void deleteSubDot(GroundUser gUser) {
		final String queryString="delete SubDot where userId in (:ids)";
		Object[] array = new Object[1];
		array[0] = gUser.getId();
		deleteEntity(SubDot.class, queryString, array);
	}

}
