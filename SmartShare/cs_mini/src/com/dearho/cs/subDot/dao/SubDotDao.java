package com.dearho.cs.subDot.dao;

import java.util.List;

import com.dearho.cs.groundUser.pojo.GroundUser;
import com.dearho.cs.subDot.pojo.SubDot;

public interface SubDotDao {

	List<SubDot> querySubDotByGroundUserId(String id);

	void saveSubDot(SubDot subDot);

	void deleteSubDot(GroundUser gUser);

}
