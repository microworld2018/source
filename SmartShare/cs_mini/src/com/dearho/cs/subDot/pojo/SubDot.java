package com.dearho.cs.subDot.pojo;

import java.io.Serializable;

/**
 * 地勤人员网点关联
 * 
 * @author Jin Guangyu
 *
 */
public class SubDot implements Serializable {

	private static final long serialVersionUID = -8302789580745216122L;
	private Integer id;
	private String userId;// 地勤人员id
	private String dotId;// 网点id
	private String dotName;// 网点名称
	private String dotAddr;// 网点地址
	private Double dotLat;// 纬度
	private Double dotLon;// 经度
	private String userName;// 用户名称
	private String userPhone;// 用户手机号

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDotId() {
		return dotId;
	}

	public void setDotId(String dotId) {
		this.dotId = dotId;
	}

	public String getDotName() {
		return dotName;
	}

	public void setDotName(String dotName) {
		this.dotName = dotName;
	}

	public String getDotAddr() {
		return dotAddr;
	}

	public void setDotAddr(String dotAddr) {
		this.dotAddr = dotAddr;
	}

	public Double getDotLat() {
		return dotLat;
	}

	public void setDotLat(Double dotLat) {
		this.dotLat = dotLat;
	}

	public Double getDotLon() {
		return dotLon;
	}

	public void setDotLon(Double dotLon) {
		this.dotLon = dotLon;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

}
