package com.dearho.cs.orders.service.impl;

import java.util.List;

import com.dearho.cs.orders.dao.OrderDataReportDao;
import com.dearho.cs.orders.pojo.OrderDatareport;
import com.dearho.cs.orders.pojo.OrderDayData;
import com.dearho.cs.orders.service.OrderDataReportService;
import com.dearho.cs.orders.service.OrdersDetailService;

public class OrderDataReportServiceImpl implements OrderDataReportService {
	
	private OrderDataReportDao orderDataReportDao;

	@Override
	public List<Object> getOrderDatareportPage(String startTime,String endTime,String phone,String payType,String carNumber,String memberName,Integer payStatus, OrdersDetailService ordersDetailService) {
		return orderDataReportDao.getOrderDatareportPage(startTime, endTime,phone,payType,carNumber,memberName,payStatus,ordersDetailService);
	}

	@Override
	public List<OrderDayData> getOrderDatareportList(
			OrderDatareport orderDatareport) {
		return orderDataReportDao.getOrderDatareportList(orderDatareport);
	}
	
	
	
	
	

	public OrderDataReportDao getOrderDataReportDao() {
		return orderDataReportDao;
	}

	public void setOrderDataReportDao(OrderDataReportDao orderDataReportDao) {
		this.orderDataReportDao = orderDataReportDao;
	}

	@Override
	public String getPayTypeByTradeOrderNo(String tradeOrderNo) {
		// TODO Auto-generated method stub
		return orderDataReportDao.getPayTypeByTradeOrderNo(tradeOrderNo);
	}
}
