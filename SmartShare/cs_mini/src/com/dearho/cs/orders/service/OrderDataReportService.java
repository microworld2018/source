package com.dearho.cs.orders.service;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.pojo.OrderDatareport;
import com.dearho.cs.orders.pojo.OrderDayData;

public interface OrderDataReportService {
	/**导出订单数据
	 * @return
	 */
	List<Object> getOrderDatareportPage(String startTime,String endTime,String phone,String payType,String carNumber,String memberName,Integer payStatus, OrdersDetailService ordersDetailService);
	
	/**根据条件获取订单数据列表
	 * @param orderDatareport
	 * @return
	 */
	List<OrderDayData> getOrderDatareportList(OrderDatareport orderDatareport);
	
	String getPayTypeByTradeOrderNo(String tradeOrderNo);	
	
}
