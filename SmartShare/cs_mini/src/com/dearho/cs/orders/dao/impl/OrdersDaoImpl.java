/**
 * copyright (c) dearho Team
 * All rights reserved
 *
 * This file OrdersDaoImpl.java creation date: [2015年5月28日 上午9:49:02] by Carsharing03
 * http://www.dearho.com
 */
package com.dearho.cs.orders.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.cxf.common.util.StringUtils;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.pojo.CarVehicleModel;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.dao.OrdersDao;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.pojo.OrdersDetail;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.resmonitor.pojo.CarRealtimeState;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.Dict;
import com.dearho.cs.util.DateUtil;
import com.dearho.cs.util.StringHelper;

/**
 * @author lvlq
 * @Description:(此类型的描述)
 * @Version 1.0, 2015年5月28日
 */
public class OrdersDaoImpl extends AbstractDaoSupport implements OrdersDao {

	@Override
	public List<AdministrativeArea> queryAreaByCon(String hql) {
		return getList(AdministrativeArea.class, queryFList(hql));
	}

	@Override
	public List<BranchDot> queryDotByHql(String hql) {
		return getList(BranchDot.class, queryFList(hql));
	}

	@Override
	public List<Car> queryCarByHql(String hql) {
		return getList(Car.class, queryFList(hql));
	}

	@Override
	public Page<Car> queryCarPages(Page<Car> page, String hql) {
		Page<Car> resultPage=pageCache(Car.class, page, hql);
		resultPage.setResults(idToObj(Car.class, resultPage.getmResults()));
		if(resultPage.getResults()!=null){
			for (int i = 0; i < resultPage.getResults().size(); i++) {
				Car car=(Car) resultPage.getResults().get(i);
				if(StringHelper.isNotEmpty(car.getModelId())){
					car.setCarVehicleModel(get(CarVehicleModel.class, car.getModelId().trim()));
				}
			}
		}
		
		return resultPage;
	}

	@Override
	public Page<CarRealtimeState> queryCarRealPage(Page<CarRealtimeState> page,
			String hql) {
		Page<CarRealtimeState> resultPage=pageCache(CarRealtimeState.class, page, hql);
		resultPage.setResults(idToObj(CarRealtimeState.class, resultPage.getmResults()));
		if(resultPage.getResults()!=null){
			for (int i = 0; i < resultPage.getResults().size(); i++) {
				CarRealtimeState real=(CarRealtimeState) resultPage.getResults().get(i);
				Car car=get(Car.class, real.getId().trim());
				real.setCar(car);
				if(StringHelper.isNotEmpty(car.getModelId())){
					car.setCarVehicleModel(get(CarVehicleModel.class, car.getModelId().trim()));
				}
				
			}
		}
		
		return resultPage;
	}

	@Override
	public void addOrders(Orders orders) {
		addEntity(orders);
	}

	@Override
	public void updateOrders(Orders orders) {
		updateEntity(orders);
	}

	@Override
	public Page<Orders> queryOrdersPage(Page<Orders> page, String hql) {
		Page<Orders> resultPage=pageCache(Orders.class, page, hql);
		resultPage.setResults(idToObj(Orders.class, resultPage.getmResults()));
		return resultPage;
	}

	@Override
	public BranchDot queryDotById(String dotId) {
		return get(BranchDot.class, dotId);
	}

	@Override
	public CarRealtimeState queryCarRealtimeState(String id) {
		return get(CarRealtimeState.class, id);
	}

	@Override
	public void updateCarRealTimeState(CarRealtimeState carRealtimeState) {
		updateEntity(carRealtimeState);
	}

	@Override
	public CarRealtimeState queryCarRealStateById(String id) {
		CarRealtimeState carRealtimeState=get(CarRealtimeState.class, id);
		if(carRealtimeState!=null&&StringHelper.isNotEmpty(carRealtimeState.getId())){
			Car car=get(Car.class, carRealtimeState.getId().trim());
			if(null!=car&&StringHelper.isNotEmpty(car.getModelId())){
				car.setCarVehicleModel(get(CarVehicleModel.class, car.getModelId().trim()));
			}
			carRealtimeState.setCar(car);
		}
		return carRealtimeState;
	}

	@Override
	public List<Orders> queryOrders(String hql) {
		return getList(Orders.class, queryFList(hql));
	}
	

	@Override
	public List<CarRealtimeState> queryCarRealtimeList(String hql) {
		List<CarRealtimeState> list=getList(CarRealtimeState.class, queryFList(hql));
		if(list!=null&&list.size()>0){
			for (int i = 0; i < list.size(); i++) {
				CarRealtimeState carRealtimeState=list.get(i);
				Car car=get(Car.class, carRealtimeState.getId().trim());
				if(null!=car&&StringHelper.isNotEmpty(car.getModelId())){
					car.setCarVehicleModel(get(CarVehicleModel.class, car.getModelId().trim()));
				}
				carRealtimeState.setCar(car);
			}
		}
		return list;
	}

	@Override
	public Orders queryOrdersById(String id) {
		Orders order=get(Orders.class, id);
		if(order != null && !StringUtils.isEmpty(order.getCarId())){
			order.setCar(get(Car.class, order.getCarId()));
			String hql = "select a.id from OrdersDetail a where a.ordersId = '"+order.getId()+"'";
				List<OrdersDetail> list = getList(OrdersDetail.class, queryFList(hql));
				if(list != null){
					order.setOrdersDetai(list.get(0));
				}
		}
		return order;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> queryOrderByHql(String hql) {
		return getHibernateTemplate().find(hql);
	}

	@Override
	public int queryOrdersReport(String startDate, String endDate,
			JSONArray json,String type,String sortingType) {
		int code = 0;
		try {
			StringBuffer hql = new StringBuffer();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:MM:ss");  
			if(type.equals("0")){
				hql = new StringBuffer("select b.name,count(o.beginSiteId)"
						+ " FROM Orders o,BranchDot b "
						+ "WHERE o.beginSiteId = b.id AND o.payStatus = 1 ");
				Date sdate = sdf.parse(startDate);
				Date edate = sdf.parse(endDate);
				if(sdate.getTime() > edate.getTime()){
					code = 1;
				}
				hql.append(" and o.createDate >= '").append(startDate).append("'");
				hql.append(" and o.createDate <= '").append(endDate).append("'");
				hql.append(" GROUP BY o.beginSiteId ORDER BY COUNT(o.beginSiteId)");
				if(sortingType.equals("0")){
					hql.append(" DESC");
				}else if(sortingType.equals("1")){
					hql.append(" ASC");
				}
			}
			if(type.equals("1")){
				hql = new StringBuffer("select b.name,SUM(o.totalFee)"
						+ " FROM Orders o,BranchDot b "
						+ "WHERE o.beginSiteId = b.id AND o.payStatus = 1 ");
				Date sdate = sdf.parse(startDate);
				Date edate = sdf.parse(endDate);
				if(sdate.getTime() > edate.getTime()){
					code = 1;
				}
				hql.append(" and o.createDate >= '").append(startDate).append("'");
				hql.append(" and o.createDate <= '").append(endDate).append("'");
				hql.append(" GROUP BY o.beginSiteId ORDER BY SUM(o.totalFee)");
				if(sortingType.equals("0")){
					hql.append(" DESC");
				}else if(sortingType.equals("1")){
					hql.append(" ASC");
				}else if(sortingType.equals("2")){
					
				}
			}
			@SuppressWarnings("unchecked")
			List<Object> objs = getHibernateTemplate().find(hql.toString());
			if(objs != null && objs.size() > 0){
				if(objs.size()>10){
					if(sortingType.equals("2")){
						for(int i=0;i<objs.size();i++){
							Object[] o = (Object[]) objs.get(i);
							JSONObject jso = new JSONObject();
							jso.put(o[0], o[1]);
							json.add(jso);
						}
					}else{
						for(int i=0;i<10;i++){
							Object[] o = (Object[]) objs.get(i);
							JSONObject jso = new JSONObject();
							jso.put(o[0], o[1]);
							json.add(jso);
						}
					}
				}else{
					for(int i=0;i<objs.size();i++){
						Object[] o = (Object[]) objs.get(i);
						JSONObject jso = new JSONObject();
						jso.put(o[0], o[1]);
						json.add(jso);
					}
				}
				code = 0;
			}else{
				code = 2;
			}
		} catch (ParseException e) {
			e.printStackTrace();
			code = 3;
		}
		return code;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Orders> getOrderTime(String hql) {
		return getHibernateTemplate().find(hql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getOrderPeriodAnalysis(String startDate, String endDate,
			JSONArray json) {
		int code = 0;
		StringBuffer sql = new StringBuffer("select count(*),date_format(begin_time, '%k') from ord_orders WHERE state = 4 ");
		if(StringHelper.isNotEmpty(startDate) && StringHelper.isNotEmpty(endDate)){
			sql.append(" and begin_time >= '").append(startDate).append("'");
			sql.append(" and begin_time <= '").append(endDate).append("'");
		}else{
			code = 1;
		}
		sql.append(" group by date_format(begin_time, '%H')");
		List<Object> objs = getSession().createSQLQuery(sql.toString()).list();
		if(objs != null && objs.size() > 0){
			int timeNumber = 0;
			int timeNumber1 = 0;
			int timeNumber2 = 0;
			int timeNumber3 = 0;
			int timeNumber4 = 0;
			int timeNumber5 = 0;
			int timeNumber6 = 0;
			JSONObject jso = new JSONObject();
			for(int i=0;i<objs.size();i++){
				Object[] o = (Object[]) objs.get(i);
				int houre = Integer.parseInt(o[1].toString());
				int number = Integer.parseInt(o[0].toString());
				//0~6点
				if(houre >= 0 && houre < 6){
					timeNumber = timeNumber+number;
				}
				//6~9点
				if(houre >= 6 && houre < 9){
					timeNumber1 = timeNumber1+number;
				}
				//9~12点
				if(houre >= 9 && houre < 12){
					timeNumber2 = timeNumber2+number;
				}
				//12~14点
				if(houre >= 12 && houre < 14){
					timeNumber3 = timeNumber3+number;
				}
				//14~17点
				if(houre >= 14 && houre < 17){
					timeNumber4 = timeNumber4+number;
				}
				//17~20点
				if(houre >= 17 && houre < 20){
					timeNumber5 = timeNumber5+number;
				}
				//20~24点
				if(houre >= 20 && houre < 24){
					timeNumber6 = timeNumber6+number;
				}
			}
			jso.put("0点~6点", timeNumber);
			jso.put("6点~9点", timeNumber1);
			jso.put("9点~12点", timeNumber2);
			jso.put("12点~14点", timeNumber3);
			jso.put("14点~17点", timeNumber4);
			jso.put("17点~20点", timeNumber5);
			jso.put("20点~24点", timeNumber6);
			json.add(jso);
			code = 0;
		}else{
			code = 2;
		}
		return code;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public int getOrderPeriodAnalysis2(String startDate, String endDate,
			JSONArray json) {
		int code = 0;
		StringBuffer sql = new StringBuffer("select count(*),date_format(end_time, '%k') from ord_orders WHERE state = 4 ");
		if(StringHelper.isNotEmpty(startDate) && StringHelper.isNotEmpty(endDate)){
			sql.append(" and end_time >= '").append(startDate).append("'");
			sql.append(" and end_time <= '").append(endDate).append("'");
		}else{
			code = 1;
		}
		sql.append(" group by date_format(end_time, '%H')");
		List<Object> objs = getSession().createSQLQuery(sql.toString()).list();
		if(objs != null && objs.size() > 0){
			int timeNumber = 0;
			int timeNumber1 = 0;
			int timeNumber2 = 0;
			int timeNumber3 = 0;
			int timeNumber4 = 0;
			int timeNumber5 = 0;
			int timeNumber6 = 0;
			JSONObject jso = new JSONObject();
			for(int i=0;i<objs.size();i++){
				Object[] o = (Object[]) objs.get(i);
				int houre = Integer.parseInt(o[1].toString());
				int number = Integer.parseInt(o[0].toString());
				//0~6点
				if(houre >= 0 && houre < 6){
					timeNumber = timeNumber+number;
				}
				//6~9点
				if(houre >= 6 && houre < 9){
					timeNumber1 = timeNumber1+number;
				}
				//9~12点
				if(houre >= 9 && houre < 12){
					timeNumber2 = timeNumber2+number;
				}
				//12~14点
				if(houre >= 12 && houre < 14){
					timeNumber3 = timeNumber3+number;
				}
				//14~17点
				if(houre >= 14 && houre < 17){
					timeNumber4 = timeNumber4+number;
				}
				//17~20点
				if(houre >= 17 && houre < 20){
					timeNumber5 = timeNumber5+number;
				}
				//20~24点
				if(houre >= 20 && houre < 24){
					timeNumber6 = timeNumber6+number;
				}
			}
			jso.put("0点~6点", timeNumber);
			jso.put("6点~9点", timeNumber1);
			jso.put("9点~12点", timeNumber2);
			jso.put("12点~14点", timeNumber3);
			jso.put("14点~17点", timeNumber4);
			jso.put("17点~20点", timeNumber5);
			jso.put("20点~24点", timeNumber6);
			json.add(jso);
			code = 0;
		}else{
			code = 2;
		}
		return code;
	}


	@Override
	public List<Orders> getAbnomalOrder(String startDate, String endDate,
			String phone, String abnomalType, String processState,
			String orderNo, String carNumber) {
		List<Orders> abnomalOrderList = new ArrayList<Orders>();//定义异常订单集合
		List<Object> violationList = new ArrayList<Object>();//违章查询结果集合
		List<Object> accidentList = new ArrayList<Object>();//事故查询结果集合
		//获取违章订单
		StringBuffer sb = new StringBuffer("SELECT o.id,o.orders_no,o.orders_time,"
				+ "o.begin_time,o.end_time,cc.vehicle_plate_id,s.name,s.phone_no,"
				+ "c.biz_status,d.insurance_name,d.insurance_fee,c.code,c.biz_status,c.id as violationId "
				+ "FROM ord_orders o,c_vehicle_violation c,sub_subscriber s,c_car cc,ord_orders_detail d "
				+ "WHERE o.id = c.order_id AND o.member_id = s.id AND o.car_id = cc.id AND o.id = d.orders_id");
		if(StringHelper.isNotEmpty(startDate)){
			Date starttime = null;
			try {
				starttime = DateUtil.parseDate(startDate+" 00:00:00", "yyyy-MM-dd HH:mm:ss");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sb.append(" and o.end_time >= '").append(DateUtil.getChar19DateString(starttime)).append("'");
		}
		if(StringHelper.isNotEmpty(endDate)){
			Date endTime = null;
			try {
				endTime = DateUtil.parseDate(endDate+" 23:59:59", "yyyy-MM-dd HH:mm:ss");
			} catch (Exception e) {
			}
			sb.append(" and o.end_time <= '").append(DateUtil.getChar19DateString(endTime)).append("'");
		}
		if(StringHelper.isNotEmpty(phone)){
			sb.append(" and s.phone_no like '%").append(phone).append("%'");
		}
		if(StringHelper.isNotEmpty(carNumber)){
			sb.append(" and cc.vehicle_plate_id like '%").append(carNumber).append("%'");
		}
		if(StringHelper.isNotEmpty(processState)){
			sb.append(" and c.biz_status = '").append(processState).append("'");
		}
		if(StringHelper.isNotEmpty(orderNo)){
			sb.append(" and o.orders_no = '").append(orderNo).append("'");
		}
		//获取事故订单
		StringBuffer sb1 = new StringBuffer("SELECT o.id,o.orders_no,o.orders_time,o.begin_time,"
				+ "o.end_time,cc.vehicle_plate_id,s.name,s.phone_no,c.accept_flag,d.insurance_name,d.insurance_fee,c.code,sd.cn_name,c.id as accidentId,c.handle_status "
				+ "FROM ord_orders o,c_vehicle_accident c,sub_subscriber s,c_car cc,ord_orders_detail d,s_dict sd "
				+ "WHERE o.id = c.order_id AND o.member_id = s.id AND o.car_id = cc.id AND o.id = d.orders_id AND c.handle_status = sd.id");
		if(StringHelper.isNotEmpty(startDate)){
			Date starttime = null;
			try {
				starttime = DateUtil.parseDate(startDate+" 00:00:00", "yyyy-MM-dd HH:mm:ss");
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sb1.append(" and o.end_time >= '").append(DateUtil.getChar19DateString(starttime)).append("'");
		}
		if(StringHelper.isNotEmpty(endDate)){
			Date endTime = null;
			try {
				endTime = DateUtil.parseDate(endDate+" 23:59:59", "yyyy-MM-dd HH:mm:ss");
			} catch (Exception e) {
			}
			sb1.append(" and o.end_time <= '").append(DateUtil.getChar19DateString(endTime)).append("'");
		}
		if(StringHelper.isNotEmpty(phone)){
			sb1.append(" and s.phone_no like '%").append(phone).append("%'");
		}
		if(StringHelper.isNotEmpty(carNumber)){
			sb1.append(" and cc.vehicle_plate_id like '%").append(carNumber).append("%'");
		}
		if(StringHelper.isNotEmpty(processState)){
			sb1.append(" and c.accept_flag = '").append(processState).append("'");
		}
		if(StringHelper.isNotEmpty(orderNo)){
			sb1.append(" and o.orders_no = '").append(orderNo).append("'");
		}
		if(StringHelper.isNotEmpty(abnomalType)){
			//根据条件查询违章或者事故的异常订单
			if(abnomalType.equals("0")){
				violationList = getSession().createSQLQuery(sb.toString()).list();
			}else{
				accidentList = getSession().createSQLQuery(sb1.toString()).list();
			}
			
		}else{
			//查询所有异常订单
			violationList = getSession().createSQLQuery(sb.toString()).list();
			accidentList = getSession().createSQLQuery(sb1.toString()).list();
		}
		//遍历违章记录
		if(violationList != null && violationList.size()>0){
			for(int i=0;i<violationList.size();i++){
				Orders order = new Orders();
				Object[] o = (Object[]) violationList.get(i);
				order.setId(o[0].toString());
				order.setOrdersNo(o[1].toString());
				order.setOrdersTimeStr(o[2] == null?"数据异常":o[2].toString());
				order.setBeginTimeStr(o[3] == null?"数据异常":o[3].toString());
				order.setEndTimeStr(o[4] == null?"数据异常":o[4].toString());
				order.setPlateNumber(o[5] == null?"数据异常":o[5].toString());
				order.setMemberName(o[6] == null?"数据异常":o[6].toString());
				order.setMemberPhoneNo(o[7] == null?"数据异常":o[7].toString());
				order.setIsAbnormity(Integer.parseInt(o[8] == null?"0":o[8].toString()));
				order.setInsuranceName(o[9] == null?"":o[9].toString());
				order.setInsuranceFee(o[10] == null?"":o[10].toString());
				order.setRemindCode(o[11] == null?"":o[11].toString());
				order.setBizStatus(o[12] == null?null:Integer.parseInt(o[12].toString()));
				order.setAbnormalId(o[13] == null?"数据异常":o[13].toString());//违章ID
				order.setAbnormalType(0);
				abnomalOrderList.add(order);
			}
		}
		//遍历事故记录
		if(accidentList != null  && accidentList.size()>0){
			for(int i=0;i<accidentList.size();i++){
				Orders order = new Orders();
				Object[] o = (Object[]) accidentList.get(i);
				Dict dict = get(Dict.class, o[14].toString());
				Integer state=0;
				if(dict != null){
					switch (dict.getId()) {
					case "402881e74e2473c2014e247cca95000b":
						state = 0;
						break;
					case "402881e74e2473c2014e247d48db000c":
						state = 0;
						break;
					case "402881e74e2473c2014e247d791e000d":
						state = 0;
						break;
					case "402881e74e2473c2014e247da59f000e":
						state = 1;
						break;
					case "402881e74e2473c2014e247dd238000f":
						state = 1;
						break;

					default:
						break;
					}
				}
				
				order.setId(o[0].toString());
				order.setOrdersNo(o[1].toString());
				order.setOrdersTimeStr(o[2].toString());
				order.setBeginTimeStr(o[3] == null?"数据异常":o[3].toString());
				order.setEndTimeStr(o[4] == null?"数据异常":o[4].toString());
				order.setPlateNumber(o[5] == null?"数据异常":o[5].toString());
				order.setMemberName(o[6] == null?"数据异常":o[6].toString());
				order.setMemberPhoneNo(o[7] == null?"数据异常":o[7].toString());
				order.setIsAbnormity(state);
				order.setInsuranceName(o[9]==null?"":o[9].toString());
				order.setInsuranceFee(o[10] == null ? "" : o[10].toString());
				order.setRemindCode(o[11] == null?"":o[11].toString());
				order.setHandleStatusName(o[12] == null?"":o[12].toString());
				order.setAbnormalId(o[13].toString());//事故ID
				order.setAbnormalType(1);
				abnomalOrderList.add(order);
			}
		}
		return abnomalOrderList;
	}

}
