package com.dearho.cs.orders.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.orders.dao.OrdersDetailDao;
import com.dearho.cs.orders.pojo.OrdersDetail;
import com.dearho.cs.util.StringHelper;

/**
 * 订单详情DAO 实现类
 * @author wangjing
 *
 */
public class OrdersDetailDaoImpl  extends AbstractDaoSupport implements OrdersDetailDao{

	@Override
	public List<OrdersDetail> getOrdersDetailsByHql(String hql) {
		return getList(OrdersDetail.class, queryFList(hql));
	}

	@Override
	public void updateOrdDetail(OrdersDetail ordersDetail) {
		updateEntity(ordersDetail);
	}

	@Override
	public void addOrdDetail(OrdersDetail ordersDetail) {
		addEntity(ordersDetail);
	}

	@Override
	public List<OrdersDetail> queryOrdersDetail(String hql) {
		return getList(OrdersDetail.class, queryFList(hql));
	}

	@Override
	public void deleteOrdDetail(OrdersDetail ordersDetail) {
		remove(ordersDetail);
	}

	@Override
	public OrdersDetail getByOrderDetail(String hql) {
		List<OrdersDetail> list = getList(OrdersDetail.class, queryFList(hql));
		OrdersDetail ordersDetail1 = list.get(0);
		return ordersDetail1;
	}

}
