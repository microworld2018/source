package com.dearho.cs.orders.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.jms.Session;

import net.sf.json.JSONObject;

import com.alibaba.fastjson.JSONArray;
import com.dearho.cs.account.pojo.AccountTradeRecord;
import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.pojo.CarVehicleModel;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.coupon.pojo.SubCoupon;
import com.dearho.cs.orders.dao.OrderDataReportDao;
import com.dearho.cs.orders.pojo.OrderDatareport;
import com.dearho.cs.orders.pojo.OrderDayData;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.pojo.OrdersDetail;
import com.dearho.cs.orders.service.OrdersDetailService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.resmonitor.pojo.CarRealtimeState;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.util.DateUtil;
import com.dearho.cs.util.StringHelper;
import com.dearho.cs.util.orders.Arith;

public class OrderDataReportDaoImpl extends AbstractDaoSupport implements OrderDataReportDao {

	@SuppressWarnings("rawtypes")
	@Override
	public List<Object> getOrderDatareportPage(String startTime,String endTime,String phone,String payType,String carNumber,String memberName,Integer payStatus, OrdersDetailService ordersDetailService) {
		/*StringBuffer sql = new StringBuffer("SELECT a.orders_no,"
				+ "s.name,"
				+ "s.phone_no,"
				+ "c.vehicle_plate_id,"
				+ "DATE_FORMAT(a.begin_time, '%Y-%m-%d %H:%i:%S'),"
				+ "DATE_FORMAT(a.end_time, '%Y-%m-%d %H:%i:%S'),"
				+ "a.total_fee,"
				+ "sc.price,"
				+ "a.total_fee,"
				+ "a.actual_fee,"
				+ "a.pay_style,"
				+ "a.pay_status,"
				+ "DATE_FORMAT(a.orders_time, '%Y-%m-%d %H:%i:%S'),"
				+ "b.name");
		sql.append(" FROM ord_orders a ，sub_subscriber,c_car c,spo_sub_coupon ssc,spo_coupon sc,c_branch_dot b "
				+ " WHERE 	a.member_id = s.id AND a.car_id = c.id AND a.id = ssc.order_id AND ssc.coupon_id = sc.ID AND a.begin_site_id = b.ID AND s.`name` IS NOT NULL ");
		if(StringHelper.isNotEmpty(startTime)&&StringHelper.isNotEmpty(endTime)){
			sql.append("and a.end_time >= '").append(startTime).append(" 00:00:00'");
			sql.append("and a.end_time <= '").append(endTime).append(" 23:59:59'");
		}
		if(StringHelper.isNotEmpty(phone)){
			sql.append(" and s.phone_no like '%").append(phone).append("%'");
		}
		if(StringHelper.isNotEmpty(payType)){
			if(payType.equals("1")){
				sql.append(" and a.pay_style in (0,1,2,4,6)");
			}else{
				sql.append(" and a.pay_style = '").append(payType).append("'");
			}
		}
		if(payStatus != null){
			sql.append(" and a.pay_status = ").append(payStatus);
		}
		if(StringHelper.isNotEmpty(carNumber)){
			sql.append(" and c.vehicle_plate_id like '%").append(carNumber).append("%'");
		}
		
		if(StringHelper.isNotEmpty(memberName)){
			sql.append(" and s.name like '%").append(memberName).append("%'");
		}
		sql.append(" ORDER BY a.end_time DESC");
		List list=getSession().createSQLQuery(sql.toString()).list();
				if(list.size()>0){
			for(int i=0;i<list.size();i++){
				String payState = "";
				OrderDatareport o = new OrderDatareport();
				Object[] m=(Object[])list.get(i);
				endTime = m[5] ==null?"":m[5].toString();
				startTime = m[4] ==null?"":m[4].toString();
				payType = m[10] ==null?"":m[10].toString();
				payStatus = m[11] == null?null:Integer.parseInt(m[11].toString());
				double price = m[7] == null?0.0:Double.parseDouble(m[7].toString());
				double totalFee = m[6] == null?0.0:Double.parseDouble(m[6].toString());
				double amountsPayable = m[8] == null?0.0:Double.parseDouble(m[8].toString());
				double totalFee1 = m[9] == null?0.0:Double.parseDouble(m[9].toString());
				String vehiclePlateId = m[3] == null?"":m[3].toString();
				//1账户支付   2信用卡支付   3支付宝   4银联 5微信 
				switch (payType) {
				case "0":
					payType = "账户支付";
					break;
				case "1":
					payType = "账户支付";
					break;
				case "2":
					payType = "信用卡支付";
					break;
				case "3":
					payType = "支付宝支付";
					break;
				case "4":
					payType = "银联支付";
					break;
				case "5":
					payType = "微信支付";
					break;
				case "6":
					payType = "账户支付";
					break;
				default:
					payType = "账户支付";
					break;
				}
				switch (payStatus) {
				case 0:
					payState = "未支付";
					break;
				case 1:
					payState = "已支付";
					break;
				default:
					payState = "数据错误";
					break;
				}
				o.setOrdersNo(m[0].toString());
				o.setName(m[1].toString());
				o.setPhoneNo(m[2].toString());
				o.setVehiclePlateId(vehiclePlateId);
				o.setBeginTime(startTime);
				o.setEndTime(endTime);
				o.setTotalFee(totalFee);
				o.setPrice(price);
				o.setAmountsPayable(amountsPayable);
				o.setTotalFee1(totalFee1);
				o.setPayType(payType);
				o.setPayState(payState);
				o.setOrdersTime(m[12].toString());
				orderDataReportList.add(o);
			}
		}*/
		StringBuffer sb=new StringBuffer("select a.id from Orders a,Subscriber s,Car c,OrdersDetail d where a.memberId = s.id and a.carId = c.id and a.id = d.ordersId");
		if(StringHelper.isNotEmpty(startTime)){
			Date startDate = null;
			try {
				startDate = DateUtil.parseDate(startTime+" 00:00:00", "yyyy-MM-dd HH:mm:ss");
				sb.append(" and a.createDate >= '").append(DateUtil.getChar19DateString(startDate)).append("'");
			} catch (Exception e) {
			}
		}
		if(StringHelper.isNotEmpty(endTime)){
			Date endDate = null;
			try {
				endDate = DateUtil.parseDate(endTime+" 23:59:59", "yyyy-MM-dd HH:mm:ss");
				sb.append(" and a.createDate <= '").append(DateUtil.getChar19DateString(endDate)).append("'");
			} catch (Exception e) {
			}
		}
		
		if(StringHelper.isNotEmpty(phone)){
			sb.append(" and s.phoneNo like '%").append(phone).append("%'");
		}
		if(StringHelper.isNotEmpty(payType)){
			if(payType.equals("1")){
				sb.append(" and a.payStyle in (0,1,2,4,6)");
			}else{
				sb.append(" and a.payStyle = '").append(payType).append("'");
			}
		}
		if(payStatus != null){
			sb.append(" and a.payStatus = ").append(payStatus);
		}
		if(StringHelper.isNotEmpty(carNumber)){
			sb.append(" and c.vehiclePlateId like '%").append(carNumber).append("%'");
		}
		
		if(StringHelper.isNotEmpty(memberName)){
			sb.append(" and s.name like '%").append(memberName).append("%'");
		}
		sb.append("  ORDER BY a.createDate DESC");
		
		List<Orders> list=getList(Orders.class, queryFList(sb.toString()));
		List<Object> orderDataReportList = new ArrayList<Object>();
		
		if(list != null){
			for(int i=0;i<list.size();i++){
				OrderDatareport o = new OrderDatareport();
				String payState = "";
				Subscriber sub = get(Subscriber.class,list.get(i).getMemberId());//获取会员信息
				Car car = get(Car.class,list.get(i).getCarId());
				
				//获取订单详情，来获取保险信息
				OrdersDetail od = new OrdersDetail();
				od.setOrdersId(list.get(i).getId());
				OrdersDetail ordersDetail = ordersDetailService.getByOrderDetail(od);
				
				String str = list.get(i).getPayStyle() == null?"":list.get(i).getPayStyle();
				switch (str) {
				case "0":
					payType = "账户支付";
					break;
				case "1":
					payType = "账户支付";
					break;
				case "2":
					payType = "信用卡支付";
					break;
				case "3":
					payType = "支付宝支付";
					break;
				case "4":
					payType = "银联支付";
					break;
				case "5":
					payType = "微信支付";
					break;
				case "6":
					payType = "账户支付";
					break;
				case "":
					payType = "数据错误";
					break;
				default:
					payType = "账户支付";
					break;
				}
				switch (list.get(i).getPayStatus()) {
				case 0:
					payState = "未支付";
					break;
				case 1:
					payState = "已支付";
					break;
				default:
					payState = "数据错误";
					break;
				}
				o.setOrdersNo(list.get(i).getOrdersNo());
				o.setName(sub.getName());
				o.setPhoneNo(sub.getPhoneNo());
				o.setVehiclePlateId(car.getVehiclePlateId());
				o.setBeginTime(list.get(i).getBeginTime() == null?"":list.get(i).getBeginTime().toString());
				o.setEndTime(list.get(i).getEndTime() == null?"":list.get(i).getEndTime().toString());
				o.setTotalFee(list.get(i).getTotalFee() == null?0.0:list.get(i).getTotalFee().doubleValue());
				o.setPrice(list.get(i).getCouponFee() == null?0.0:list.get(i).getCouponFee().doubleValue());
				o.setInsuranceName(ordersDetail.getInsuranceName() == null?"":ordersDetail.getInsuranceName());
				o.setInsuranceFee(ordersDetail.getInsuranceFee() == null?"":ordersDetail.getInsuranceFee());
				
				//以下为保险总额，实体类中类型为double
				/*String insuranceFee = ordersDetail.getInsuranceFee();
				if(insuranceFee != null){
					Double insuranceFeeTotal = 0.0;
					String[] iFee = insuranceFee.split("/");
					for (String iFeeStr : iFee) {
						double iFeeDouble = Double.parseDouble(iFeeStr.trim());
						insuranceFeeTotal += iFeeDouble;
					}
					o.setInsuranceFee(insuranceFeeTotal);
				}*/
				
				o.setAmountsPayable(list.get(i).getTotalFee() == null?0.0:list.get(i).getTotalFee().doubleValue());
				o.setTotalFee1(list.get(i).getActualFee() == null?0.0:list.get(i).getActualFee().doubleValue());
				o.setPayType(payType);
				o.setPayState(payState);
				o.setOrdersTime(list.get(i).getOrdersTime() == null?"":list.get(i).getOrdersTime().toString());
				orderDataReportList.add(o);
			}
		}
		return orderDataReportList;
	}

	@Override
	public List<OrderDayData> getOrderDatareportList(
			OrderDatareport orderDatareport) {
		StringBuffer sql = new StringBuffer("SELECT "
				+"DATE_FORMAT(MAX(t.trade_time),'%Y-%m-%d'),"
				+ "o.actual_fee,"
				+ "o.total_fee,"
				+ "o.coupon_fee"
				+" FROM acc_trade_record_list t,ord_orders o"
				+" WHERE o.orders_no = t.biz_id  AND o.pay_status = 1 AND o.state = 4");
				if(StringHelper.isNotEmpty(orderDatareport.getBeginTime())){
					sql.append(" and t.trade_time >= '").append(orderDatareport.getBeginTime()).append("'");
					sql.append(" and t.trade_time <= '").append(orderDatareport.getEndTime()).append("'");
				}
				sql.append(" GROUP BY t.biz_id ORDER BY t.trade_time DESC");
		@SuppressWarnings("rawtypes")
		List list=getSession().createSQLQuery(sql.toString()).list();
		Map<String, OrderDayData> map = new TreeMap<String, OrderDayData>(
                new Comparator<String>() {
                    public int compare(String obj1, String obj2) {
                        // 降序排序
                        return obj2.compareTo(obj1);
                    }
                });
		for(int i=0;i<list.size();i++){
			Object[] m=(Object[])list.get(i);
			OrderDayData order = new OrderDayData();
			OrderDayData orderold = new OrderDayData();
			order.setDateTime(m[0].toString());
			order.setTposPayFee(Arith.round(Double.parseDouble(m[1].toString())));
			order.setTotalFee3(Arith.round(Double.parseDouble(m[2].toString())));
			order.setCouponFee(Arith.round(Double.parseDouble(m[3].toString())));
			if(map.containsKey(m[0])){
				orderold = map.get(m[0]);
				orderold.setTposPayFee(Arith.round(Arith.add(orderold.getTposPayFee().doubleValue(),order.getTposPayFee().doubleValue())));
				orderold.setTotalFee3(Arith.round(Arith.add(orderold.getTotalFee3().doubleValue(), order.getTotalFee3().doubleValue())));
				orderold.setCouponFee(Arith.round(Arith.add(orderold.getCouponFee().doubleValue(), order.getCouponFee().doubleValue())));
				orderold.setOrderCount(orderold.getOrderCount()+1);
				map.put(m[0].toString(), orderold);
			}else{
				OrderDayData ordernew = new OrderDayData();
				ordernew.setTposPayFee(order.getTposPayFee());
				ordernew.setTotalFee3(order.getTotalFee3());
				ordernew.setCouponFee(order.getCouponFee());
				ordernew.setOrderCount(1);
				map.put(m[0].toString(), ordernew);
			}
		}
		Set set = map.entrySet();         
		Iterator i = set.iterator();  
		OrderDayData o = new OrderDayData();
		List<OrderDayData> orderDatareportList = new ArrayList<OrderDayData>();
		while(i.hasNext()){      
			 Map.Entry<String, OrderDayData> entry1=(Map.Entry<String, OrderDayData>)i.next();    
		      o = entry1.getValue();
		      o.setDateTime(entry1.getKey());
		      orderDatareportList.add(o);
		}   
		return orderDatareportList;
	}
	
	@Override
	public String getPayTypeByTradeOrderNo(String tradeOrderNo){
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT a2.pay_type FROM acc_refund_record a ");
		sql.append("LEFT JOIN acc_trade_record_list a2 ON a.trade_order_no = a2.trade_order_no");
		List list=queryFList(sql.toString(),true);
		String payType="";
		if(list.size()>0){
				payType=(String)list.get(0);
		}		
		return payType;
	}	
	
public static void main(String[] args) {
		Map<Object, Object> map = new HashMap<Object, Object>();
	}
}
