/**
 * copyright (c) dearho Team
 * All rights reserved
 *
 * This file OrdersDao.java creation date: [2015年5月28日 上午9:47:20] by Carsharing03
 * http://www.dearho.com
 */
package com.dearho.cs.orders.dao;

import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.resmonitor.pojo.CarRealtimeState;
import com.dearho.cs.sys.pojo.AdministrativeArea;

/**
 * @author lvlq
 * @Description:(此类型的描述)
 * @Version 1.0, 2015年5月28日
 */
public interface OrdersDao {
	
	List<AdministrativeArea> queryAreaByCon(String hql);
	
	List<BranchDot> queryDotByHql(String hql);
	
	List<Car> queryCarByHql(String hql);
	
	Page<Car> queryCarPages(Page<Car> page,String hql);
	
	Page<CarRealtimeState> queryCarRealPage(Page<CarRealtimeState> page,String hql);
	
	CarRealtimeState queryCarRealStateById(String id);
	
	void addOrders(Orders orders);
	
	void updateOrders(Orders orders);
	
	Page<Orders> queryOrdersPage(Page<Orders> page,String hql);
	
	List<Orders> queryOrders(String hql);
	
	Orders queryOrdersById(String id);
	
	BranchDot queryDotById(String dotId);
	
	CarRealtimeState queryCarRealtimeState(String id);
	
	void updateCarRealTimeState(CarRealtimeState carRealtimeState);
	
	List<CarRealtimeState> queryCarRealtimeList(String hql);

	List<Orders> queryOrderByHql(String string);
	
	/**返回网点数据
	 * @param hql
	 * @return
	 */
	public int queryOrdersReport(String startDate,String endDate,JSONArray json,String type,String sortingType);
	
	
	/**订单分布时段
	 * @param orders
	 * @return
	 */
	public List<Orders> getOrderTime(String hql);
	
	
	/**订单时间段分布报表
	 * @param startDate
	 * @param endDate
	 * @param json
	 * @return
	 */
	public int getOrderPeriodAnalysis(String startDate,String endDate,JSONArray json);
	/**订单还车时间段分布报表
	 * @param startDate
	 * @param endDate
	 * @param json
	 * @return
	 * 2017-5-25zhanghtadd
	 */
	public int getOrderPeriodAnalysis2(String startDate,String endDate,JSONArray json);
	
	/**获取异常订单列表
	 * @param startDate 开始时间
	 * @param endDate 结束时间
	 * @param phone 会员手机号
	 * @param abnomalType 异常类型
	 * @param processState 处理状态
	 * @param orderNo 订单编号
	 * @param carNumber 车牌号
	 * @return
	 */
	public List<Orders> getAbnomalOrder(String startDate,String endDate,String phone,String abnomalType,String processState,String orderNo,String carNumber);
}
