package com.dearho.cs.orders.action;

import java.util.List;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.service.OrdersService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.pojo.CarDotBinding;
import com.dearho.cs.place.service.BranchDotService;
import com.dearho.cs.place.service.CarDotBindingService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.Dict;
import com.dearho.cs.sys.service.DictService;
import com.dearho.cs.util.Ajax;

/**客服根据客户要求更换车辆Action
 * @author Gaopl
 * @date 2016-10-09
 */ 
public class OrderUpdateCarAction extends AbstractAction{
	
	private static final long serialVersionUID = 1L;

	private CarService carService;		//车辆Service 
	
	private OrdersService ordersService; //订单Service
	
	private DictService dictService;	//字典Service(获取未租借状态的ID)
	
	private Page<Car> page=new Page<Car>(); //分页
	
	private String orderId;	//订单ID
	
	private String carId;	//车辆ID
	
	private CarDotBindingService carDotBindingService;
	private BranchDotService branchDotService;
	
	private Car car; //车辆实体类
	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public CarService getCarService() {
		return carService;
	}

	public Car getCar() {
		return car;
	}
	
	public OrdersService getOrdersService() {
		return ordersService;
	}

	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}

	public void setCar(Car car) {
		this.car = car;
	}
	
	public DictService getDictService() {
		return dictService;
	}

	public void setDictService(DictService dictService) {
		this.dictService = dictService;
	}

	public Page<Car> getPage() {
		return page;
	}

	public void setPage(Page<Car> page) {
		this.page = page;
	}
	
	public CarDotBindingService getCarDotBindingService() {
		return carDotBindingService;
	}

	public void setCarDotBindingService(CarDotBindingService carDotBindingService) {
		this.carDotBindingService = carDotBindingService;
	}

	public BranchDotService getBranchDotService() {
		return branchDotService;
	}

	public void setBranchDotService(BranchDotService branchDotService) {
		this.branchDotService = branchDotService;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public OrderUpdateCarAction() {
		super();
		car=new Car();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}

	public String getBelongDotName(String carId){
		CarDotBinding binding = carDotBindingService.getBindingByCarId(carId, 1);
		if(binding != null){
			BranchDot dot = branchDotService.getBranchDotById(binding.getDotId());
			if(dot != null){
				return dot.getName();
			}
		}
		return "";
	}
	
	
	
	@Override
	public String process() {
		Dict dict = dictService.getCarStart("未租借");
		car.setBizState(dict.getId());
		page = carService.queryCarByPage(page, car);
		return SUCCESS;
	}
	
	
	/**更新订单绑定车辆
	 * @return
	 */
	public String updateOrderCarId(){
		try {
			Orders orders = ordersService.queryOrdersById(orderId);
			orders.setCarId(carId);
			ordersService.updateOrders(orders);
			result=Ajax.JSONResult(0, "更新成功！");
		} catch (Exception e) {
			result=Ajax.JSONResult(1, "更新失败！");
		}
		
		return SUCCESS;
	}
	
	
}
