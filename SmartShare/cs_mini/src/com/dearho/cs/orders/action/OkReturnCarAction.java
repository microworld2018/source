package com.dearho.cs.orders.action;

import java.util.Date;
import java.util.List;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.pojo.OrdersDetail;
import com.dearho.cs.orders.service.OrdersDetailService;
import com.dearho.cs.orders.service.OrdersService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.service.BranchDotService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.Dict;
import com.dearho.cs.sys.service.AreaService;
import com.dearho.cs.sys.util.DictUtil;
import com.dearho.cs.util.Ajax;
import com.foxinmy.weixin4j.payment.mch.Order;

/**后台确认还车
 * @author Gaopl
 *
 */
public class OkReturnCarAction  extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 设置分页数据
	 */
	private Page<BranchDot> page = new Page<BranchDot>();
	
	/**
	 * 订单ID
	 */
	private String orderId;
	
	/**
	 * 网点实体类
	 */
	private BranchDot branchDot;
	
	/**
	 * 订单Service
	 */
	private OrdersService ordersService;
	
	/**
	 * 车辆Service
	 */
	private CarService carService;
	
	/**
	 * 订单详情Service
	 */
	private OrdersDetailService ordersDetailService;
	
	/**
	 * 网点Service
	 */
	private BranchDotService branchDotService;
	
	/**
	 * 行政区划Service
	 */
	private AreaService areaService;
	
	/**
	 * 行政区划实体类
	 */
	private List<AdministrativeArea> areas;
	
/********方法开始****************************************************************************************************/	

	@Override
	public String process() {
		/*获取网点列表*/
		page = branchDotService.searchBranchDot(page, branchDot);
		getRequest().setAttribute("orderId", orderId);
		return SUCCESS;
	}
	
	
	/**确认还车
	 * @return
	 */
	public String okAlsoCar(){
		try {
			Orders order = ordersService.queryOrdersById(orderId);
			order.setState("4");
			ordersService.updateOrders(order);
			OrdersDetail orderDetail = new OrdersDetail();
			orderDetail.setOrdersId(orderId);
			OrdersDetail orderDetail1 = ordersDetailService.getByOrderDetail(orderDetail);
			orderDetail1.setEndTime(new Date());
			ordersDetailService.updateOrdersDetail(orderDetail1);
			Car car = carService.queryCarById(order.getCarId());
			Dict ydDict = DictUtil.getDictByCodes("carBizState", "0");
			car.setBizState(ydDict.getId());
			carService.updateCar(car);
			result=Ajax.JSONResult(0, "还车成功！");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result=Ajax.JSONResult(1, "还车失败！");
		}
		return SUCCESS;
	}
	
	
	
/********封装Get和Set开始******************************************************************************************/
	
	public OkReturnCarAction() {
		super();
		branchDot = new BranchDot();
		page.setCountField("a.id");
		page.setCurrentPage(1);
	}
	
	
	
	public Page<BranchDot> getPage() {
		return page;
	}

	public void setPage(Page<BranchDot> page) {
		this.page = page;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public BranchDot getBranchDot() {
		return branchDot;
	}

	public void setBranchDot(BranchDot branchDot) {
		this.branchDot = branchDot;
	}

	public OrdersService getOrdersService() {
		return ordersService;
	}

	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public OrdersDetailService getOrdersDetailService() {
		return ordersDetailService;
	}

	public void setOrdersDetailService(OrdersDetailService ordersDetailService) {
		this.ordersDetailService = ordersDetailService;
	}



	public BranchDotService getBranchDotService() {
		return branchDotService;
	}



	public void setBranchDotService(BranchDotService branchDotService) {
		this.branchDotService = branchDotService;
	}

	public AreaService getAreaService() {
		return areaService;
	}



	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}



	public List<AdministrativeArea> getAreas() {
		List<AdministrativeArea> allAreas = areaService.searchAreaByCode(null);
		areas = allAreas;
		return areas;
	}



	public void setAreas(List<AdministrativeArea> areas) {
		this.areas = areas;
	}



	/**根据ID获取区域
	 * @param id
	 * @return
	 */
	public String getAreaName(String id){
		if(areas != null && areas.size() > 0){
			for (AdministrativeArea a : areas) {
				if(a.getId().equals(id)){
					return a.getName();
				}
			}
		}
		return "";
	}
	
}
