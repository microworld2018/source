/**
 * copyright (c) dearho Team
 * All rights reserved
 *
 * This file OrdersSearchAction.java creation date: [2015年6月4日 下午1:32:12] by Carsharing03
 * http://www.dearho.com
 */
package com.dearho.cs.orders.action;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.dearho.cs.Deposit.pojo.AccDeposit;
import com.dearho.cs.Deposit.service.AccDepositService;
import com.dearho.cs.RefundRecord.pojo.DubDepositRecord;
import com.dearho.cs.RefundRecord.pojo.RefundRecord;
import com.dearho.cs.RefundRecord.service.DubDepositRecordService;
import com.dearho.cs.RefundRecord.service.RefundRecordService;
import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.pojo.CarVehicleModel;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.carservice.pojo.CarAccident;
import com.dearho.cs.carservice.pojo.CarViolation;
import com.dearho.cs.carservice.pojo.ViolationAccidentRecord;
import com.dearho.cs.carservice.service.CarAccidentService;
import com.dearho.cs.carservice.service.CarViolationService;
import com.dearho.cs.carservice.service.ViolationAccidentRecordService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.coupon.pojo.SubCoupon;
import com.dearho.cs.coupon.service.CouponService;
import com.dearho.cs.coupon.service.SubCouponService;
import com.dearho.cs.device.service.DeviceBindingService;
import com.dearho.cs.feestrategy.pojo.StrategyBase;
import com.dearho.cs.feestrategy.service.StrategyBaseService;
import com.dearho.cs.insuranceRules.service.CarInsuranceNewService;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.pojo.OrdersDetail;
import com.dearho.cs.orders.service.OrdersDetailService;
import com.dearho.cs.orders.service.OrdersService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.service.BranchDotService;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.subscriber.service.SubscriberService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.Dict;
import com.dearho.cs.sys.pojo.SysOperateLogRecord;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.SystemOperateLogService;
import com.dearho.cs.sys.util.DictUtil;
import com.dearho.cs.sys.util.SystemOperateLogUtil;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.CryptoTools;
import com.dearho.cs.util.DateUtil;
import com.dearho.cs.util.HttpRequestUtil;
import com.dearho.cs.util.PropertiesHelper;
import com.dearho.cs.util.StringHelper;
import com.dearho.cs.util.TradeRecordNo;
import com.dearho.cs.util.orders.Arith;
import com.opensymphony.webwork.ServletActionContext;

/**
 * @author lvlq
 * @Description:(此类型的描述)
 * @Version 1.0, 2015年6月4日
 */
public class OrdersSearchAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private OrdersService ordersService;

	private SubscriberService subscriberService;

	private BranchDotService branchDotService;

	private CarService carService;

	private SubCouponService subCouponService;

	private OrdersDetailService ordersDetailService;

	private StrategyBaseService strategyBaseService;

	private DeviceBindingService deviceBindingService;

	private CouponService couponService;
	
	private AccDepositService accDepositService;

	private SystemOperateLogService systemOperateLogService;// 记录操作日志Service
	
	private CarAccidentService carAccidentService;//车辆事故Service
	
	private CarViolationService carViolationService;//车辆违章Service
	
	private ViolationAccidentRecordService violationAccidentRecordService;//车辆违章事故记录Service
	
	private DubDepositRecordService dubDepositRecordService;//会员押金记录
	
	private CarInsuranceNewService carInsuranceNewService;//车险关联
	
	private RefundRecordService refundRecordService;//会员押金退款申请记录Service

	private Page<Orders> page = new Page<Orders>();

	private Orders orders;

	private String state;
	private String query;

	private String orderState;

	private String startTime;
	private String endTime;

	private String carNumber;

	private String phone;

	private String payType;
	private String memberName;
	
	private Integer payStatus;
	
	private String timeout;

	private List<BranchDot> dots = new ArrayList<BranchDot>();

	private List<OrdersDetail> ordersDetailList = new ArrayList<OrdersDetail>();
	
	private OrdersDetail orderDetail;

	private String param;

	private String sortingType;
	
	private String branchDotName;   //订单管理增加网点 zhanght 2017-5-23add  
	
	private String isAbnormity;
	
	private String abnormalType;
	
	private String ordersNo;
	
	private List<Orders> orderList = new ArrayList<Orders>();

	@Override
	public String process() {
		String returnPage = "";
		if ("page".equals(state)) {
			if ("accident".equals(query)) {
				page = ordersService.queryOrdersPageNotAccident(page, orders);
			}
			if (page != null && page.getResults() != null) {
				for (Object obj : page.getResults()) {
					Orders orders = (Orders) obj;
					orders.setInsuranceStr(carInsuranceNewService.getInsuranceStrByCarId(orders.getId()));
				}
			}

			returnPage = "search";
		} else {
			if (StringHelper.isNotEmpty(orderState)) {
				Dict d = DictUtil.getDictByCodes("14", orderState);
				if (d != null) {
					orders.setState(d.getId());
				}
			}
			if (StringHelper.isNotEmpty(carNumber)) {
				orders.setPlateNumber(carNumber);
			}
			if (StringHelper.isNotEmpty(phone)) {
				orders.setMemberPhoneNo(phone);
			}
			
			page = ordersService.queryOrdersPage(page, orders, startTime,
					endTime,timeout);
			returnPage = SUCCESS;
		}
		if (page != null && page.getResults() != null) {
			for (Object obj : page.getResults()) {
				Orders o = (Orders) obj;
				if (!StringHelper.isRealNull(o.getMemberId())) {
					o.setMemberName(getMemberName(o.getMemberId()).getName());
				}
				if (!StringHelper.isRealNull(o.getCarId())) {
					Car c = carService.queryCarById(o.getCarId());
					if (c != null) {
						o.setPlateNumber(c.getVehiclePlateId());
					}
					SubCoupon subCoupon = subCouponService
							.getSubcouponByOrderId(o.getId());
					if (subCoupon != null) {
						Coupon coupon = couponService.queryCouponById(subCoupon
								.getCouponId());
						o.setCouponName(coupon.getCouponName());
					}
				}
				//获取订单详情开始
				orderDetail.setOrdersId(o.getId());
				orderDetail = ordersDetailService.getByOrderDetail(orderDetail);
				o.setOrdersDetai(orderDetail);
			}
		}
		return returnPage;
	}

	/**
	 * 查看订单详情
	 * 
	 * @return
	 */
	public String ordersDetail() {
		//获取订单基本信息
		String id = getRequest().getParameter("id");
		orders = ordersService.queryOrdersById(id);
		orders.setMemberName(getMemberName(orders.getMemberId()).getName());
		if (!StringHelper.isRealNull(orders.getCarId())) {
			Car c = carService.queryCarById(orders.getCarId());
			if (c != null) {
				orders.setPlateNumber(c.getVehiclePlateId());
				CarVehicleModel cvm = c.getCarVehicleModel();
				if (cvm != null) {
					Dict d = DictUtil.getDictById(cvm.getBrand());
					orders.setVehicleModelName(d.getCnName() + " "
							+ cvm.getName());
				}
			}
		}
		orders.setInsuranceStr(carInsuranceNewService.getInsuranceStrByCarId(orders.getId()));

		dots = ordersService.queryDotByCon(null);
		Map<String, String> dotMap = new HashMap<String, String>();
		for (BranchDot dot : dots) {
			dotMap.put(dot.getId(), dot.getName());
		}
		if (!StringHelper.isRealNull(orders.getBeginSiteId())) {
			orders.setBeginSiteId(dotMap.get(orders.getBeginSiteId()));
		}
		if (!StringHelper.isRealNull(orders.getEndSiteId())) {
			orders.setEndSiteId(dotMap.get(orders.getEndSiteId()));
		}
		if (!StringHelper.isRealNull(orders.getOrdersBackSiteId())) {
			orders.setOrdersBackSiteId(dotMap.get(orders.getOrdersBackSiteId()));
		}
		//获取订单详情开始
		orderDetail.setOrdersId(orders.getId());
		orderDetail = ordersDetailService.getByOrderDetail(orderDetail);
		if(orderDetail.getEndTime() == null){
			orders.setOrdersDuration(null);
			orderDetail.setTimeFee(null);
			orderDetail.setMileage(null);
			orderDetail.setMileFee(null);
			orders.setCouponFee(null);
			orders.setTotalFee(null);
			orders.setPayStatus(null);
			orders.setTposPayFee(null);
		}
		String type = getRequest().getParameter("type");
		if(type == null || type.equals("0")){
			return SUCCESS;
		}else{
			return "editOrders";
		}
	}


	/**
	 * 保存编辑订单
	 * 
	 * @return
	 */
	public String saveOrderEdit() {
		try {
			// 获取订单基本信息
			Orders order = ordersService.queryOrdersById(orders.getId());
			SimpleDateFormat sdf=new SimpleDateFormat("yy-MM-dd HH:mm:ss");
			if (StringHelper.isNotEmpty(orders.getEndTimeStr().toString())
					|| StringHelper.isNotEmpty(orders.getBeginTimeStr()
							.toString())) {
				result = Ajax.JSONResult(1, "请选择租车开始时间和结束时间！");
			}
			OrdersDetail orderDetail = new OrdersDetail();
			orderDetail.setOrdersId(order.getId());
			OrdersDetail orderDetail1 = ordersDetailService
					.getByOrderDetail(orderDetail);
			Car car = carService.queryCarById(order.getCarId());
			// 获取里程信息
			String mileage = "0";// 默认0
			String msgUrl = PropertiesHelper.getValue("msg_url")
					+ "/car/getCarMileageForGPS";
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("sn", car.getVehiclePlateId());
			paramMap.put("beginTime", sdf.format(order.getBeginTime()));
			paramMap.put("endTime", orders.getEndTimeStr());
			mileage = getCarMileageForGPS(paramMap, msgUrl);
			if (mileage != null && !"".equals(mileage) && !"0".equals(mileage)) {
				// 计算里程、精确到小数点后两位
				mileage = String.valueOf(div(Double.valueOf(mileage), 1000, 2));
			} else {
				mileage = "0";
			}
			// 获取计费规则
			StrategyBase StrategyBase = strategyBaseService
					.searchStrategyBaseById(orderDetail1.getStrategyId());
			// 计算里程费用
			BigDecimal kmPrice = StrategyBase.getKmPrice();// 里程价(单位：公里)
			BigDecimal allMail = new BigDecimal(mileage);
			double allMilePrice = mul(kmPrice.doubleValue(),
					allMail.doubleValue());
			// 计算时长费用
			/*int allTime = (int) ((DateUtil.parseDate(orders.getEndTimeStr(),
					"yyyy-MM-dd HH:mm").getTime() - DateUtil.parseDate(
					orders.getBeginTimeStr(), "yyyy-MM-dd HH:mm").getTime()) / 1000 / 60);*/
			int allTime = (int) ((DateUtil.parseDate(order.getBeginTime().toString(),
					"yyyy-MM-dd HH:mm").getTime() - DateUtil.parseDate(
							orders.getBeginTimeStr(), "yyyy-MM-dd HH:mm").getTime()) / 1000 / 60);
			double allTimePrice = mul(allTime, StrategyBase.getBasePrice()
					.doubleValue());
			// 计算总费用
			double allPrice = add(allMilePrice, allTimePrice);

			// 更新订单主表
			/*order.setBeginTime(DateUtil.parseDate(orders.getBeginTimeStr(),
					"yyyy-MM-dd HH:mm"));*/
			order.setBeginTime(DateUtil.parseDate(order.getBeginTime().toString(),
					"yyyy-MM-dd HH:mm"));
			order.setEndTime(DateUtil.parseDate(orders.getEndTimeStr(),
					"yyyy-MM-dd HH:mm"));
			order.setState(orders.getState());
			BigDecimal allPrice1 = new BigDecimal(allPrice);
			order.setTotalFee(allPrice1);
			ordersService.updateOrders(order);
			// 更新订单详情表
			/*orderDetail1.setBeginTime(DateUtil.parseDate(
					orders.getBeginTimeStr(), "yyyy-MM-dd HH:mm"));*/
			orderDetail1.setEndTime(DateUtil.parseDate(orders.getEndTimeStr(),
					"yyyy-MM-dd HH:mm"));

			orderDetail1.setTimeFee(BigDecimal.valueOf(allTimePrice));
			orderDetail1.setTotalFee(allPrice1);
			orderDetail1.setMileFee(BigDecimal.valueOf(allMilePrice));
			orderDetail1.setMileage(allMail);
			ordersDetailService.updateOrdersDetail(orderDetail1);
			// 更新车辆状态
			Dict ydDict = DictUtil.getDictByCodes("carBizState", "0");
			car.setBizState(ydDict.getId());
			carService.updateCar(car);
			result = Ajax.JSONResult(0, "修改完成！");
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.JSONResult(1, "修改失败！");
		}
		return SUCCESS;
	}

	/**
	 * 订单销售数据分析报表查询
	 * 
	 * @return
	 */
	public String queryOrdersReport() {
		String startDate = param + "-01 00:00:00";
		String endDate = param + "-31 23:59:59";
		JSONArray json = new JSONArray();
		int i = ordersService.queryOrdersReport(startDate, endDate, json,
				state, sortingType);
		result = Ajax.JSONResult(i, "成功！", json);
		/*************** 添加站点数据查看日志 **********************/
		SysOperateLogRecord log = new SysOperateLogRecord();
		HttpSession session = ServletActionContext.getRequest().getSession();
		User user = (User) session.getAttribute("user");
		Date date = new Date();
		log.setOperatorId(user.getId());
		log.setOperatorName(user.getName());
		log.setOperateDate(date);
		log.setOperateRemark(SystemOperateLogUtil.DOT_DATA_VIEW);
		log.setModelName("站点月度分析查看");
		log.setKeyword("查看：" + param + " 月份数据站点数据");
		log.setOperateContent("查看：" + param + " 月份站点数据");
		// 记录日志
		systemOperateLogService.addSysOperateLogRecord(log);
		return SUCCESS;
	}

	/**
	 * 订单数据报表
	 * 
	 * @return
	 */
	public String lokOrdersDataReprot() {
		orders.setMemberName(memberName);
		orders.setMemberPhoneNo(phone);
		orders.setPlateNumber(carNumber);
		orders.setPayType(payType);
		orders.setPayStatus(payStatus);
		page = ordersService.queryOrdersPage(page, orders, startTime, endTime,timeout);
		if (page != null && page.getResults() != null) {
			for (Object obj : page.getResults()) {
				Orders o = (Orders) obj;
				if (!StringHelper.isRealNull(o.getMemberId())) {
					o.setMemberName(getMemberName(o.getMemberId()).getName());
					o.setMemberPhoneNo(getMemberName(o.getMemberId())
							.getPhoneNo());
				}
				if (!StringHelper.isRealNull(o.getCarId())) {
					Car c = carService.queryCarById(o.getCarId());
					if (c != null) {
						o.setPlateNumber(c.getVehiclePlateId());
					}
					SubCoupon subCoupon = subCouponService
							.getSubcouponByOrderId(o.getId());
					if (subCoupon != null) {
						Coupon coupon = couponService.queryCouponById(subCoupon
								.getCouponId());
						o.setCouponName(coupon.getCouponName());
					}
				}
				//获取订单详情开始
				orderDetail.setOrdersId(o.getId());
				orderDetail = ordersDetailService.getByOrderDetail(orderDetail);
				o.setOrdersDetai(orderDetail);
			}
		}
		return SUCCESS;
	}
	
	
	/**订单时长统计
	 * @return
	 */
	public String orderTimeStatisics(){
		orders.setState("4");
		String startDate ="";
		String endDate = "";
		orders.setState("4");
		if(state.equals("1")){
			startDate = param + "-01 00:00:00";
			endDate = param + "-31 23:59:59";
		}
		if(state.equals("0")){
			startDate = param + "-01-01 00:00:00";
			endDate = param + "-12-31 23:59:59";
		}
		orders.setBeginTimeStr(startDate);
		orders.setEndTimeStr(endDate);
		List<Orders> orderList = ordersService.queryOrdersByCon(orders);
		if(orderList == null){
			result = Ajax.JSONResult(1,"暂无数据！");
			return SUCCESS;
		}
		JSONObject json = new JSONObject();
		int str = 0;
		int str1 = 0;
		int str2 = 0;
		int str3 = 0;
		int str4 = 0;
		for(Orders order : orderList){
			//计算用车时长
			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			long nd = 1000*24*60*60;//一天的毫秒数
			long nh = 1000*60*60;//一小时的毫秒数
			long nm = 1000*60;//一分钟的毫秒数
			long diff = 0;
			try {
				diff = sd.parse(order.getEndTime().toString()).getTime() - sd.parse(order.getBeginTime().toString()).getTime();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			long day = diff/nd;//计算差多少天
			long hour = diff%nd/nh;//计算差多少小时
			long min = diff%nd%nh/nm;//计算差多少分钟
			//判断时长是否超过1天超过则转为分钟
			if(day > 0){
				day = day*24;
			}
			if(min > 0){
				min = min/60;
			}
			double house = day+min+hour;
    		if(house >= 0 && house < 2){
    			str = str+1;
    		}
    		if(house >= 2 && house < 4){
    			str1 = str1+1;
    		}
    		if(house >= 4 && house < 8){
    			str2 = str2+1;
    		}
    		if(house >= 8 && house < 24){
    			str3 = str3+1;
    		}
    		if(house >= 24 ){
    			str4 = str4+1;
    		}
		}
		json.put("0~2", str);
		json.put("2~4", str1);
		json.put("4~8", str2);
		json.put("8~24", str3);
		json.put("24", str4);
		result = Ajax.JSONResult(0,"成功！", json);
		return SUCCESS;
	}
	
	
	
	/**订单分布时段分析 取车时间
	 * @return
	 */
	public String orderPeriodAnalysis(){
		String startDate ="";
		String endDate = "";
		orders.setState("4");
		if(state.equals("1")){
			startDate = param + "-01 00:00:00";
			endDate = param + "-31 23:59:59";
		}
		if(state.equals("0")){
			startDate = param + "-01-01 00:00:00";
			endDate = param + "-12-31 23:59:59";
		}
		JSONArray json = new JSONArray();
		int code = ordersService.getOrderPeriodAnalysis(startDate, endDate, json);
		result = Ajax.JSONResult(code,"成功！", json);
		return SUCCESS;
	}
	
	
	/**订单分布时段分析2 还车时间
	 * @return
	 */
	public String orderPeriodAnalysis2(){
		String startDate ="";
		String endDate = "";
		orders.setState("4");
		if(state.equals("1")){
			startDate = param + "-01 00:00:00";
			endDate = param + "-31 23:59:59";
		}
		if(state.equals("0")){
			startDate = param + "-01-01 00:00:00";
			endDate = param + "-12-31 23:59:59";
		}
		JSONArray json = new JSONArray();
		int code = ordersService.getOrderPeriodAnalysis2(startDate, endDate, json);
		result = Ajax.JSONResult(code,"成功！", json);
		return SUCCESS;
	}
	
	/**异常订单列表
	 * @return
	 */
	public String orderAbnormalType(){
		orderList = ordersService.getAbnormalOrders(startTime, endTime, phone, abnormalType, isAbnormity, ordersNo, carNumber);
		if(orderList != null && orderList.size()>0){
			for(int i=0;i<orderList.size();i++){
				//查询提醒次数
				List<SysOperateLogRecord> logList = systemOperateLogService.getByRecordId(orderList.get(i).getRemindCode(), "用户手机号：");
				orderList.get(i).setRemindNumber(logList == null?0:logList.size());
			}
		}
		/*************** 添加记录 **********************/
		HttpSession session = ServletActionContext.getRequest().getSession();
		User user = (User) session.getAttribute("user");
		SysOperateLogRecord log = new SysOperateLogRecord();
		Date date = new Date();
		log.setOperatorId(user.getId());
		log.setOperatorName(user.getName());
		log.setOperateDate(date);
		log.setOperateRemark(SystemOperateLogUtil.REMIND_SEND_SMS);
		log.setModelName("异常订单列表查看");
		log.setKeyword("异常订单列表查看");
		log.setOperateContent("异常订单列表查看");
		// 记录日志
		systemOperateLogService.addSysOperateLogRecord(log);
		return SUCCESS;
	}
	
	
	/**获取单条异常订单信息
	 * @return
	 */
	public String abnormalTypeOrderDetail(){
		String id = getRequest().getParameter("id");
		String code = getRequest().getParameter("remindCode");
		String abnormalType = getRequest().getParameter("abnormalType");
		String abnormalId = getRequest().getParameter("abnormalId");
		orders = ordersService.queryOrdersById(id);
		orders.setAbnormalType(Integer.parseInt(abnormalType));
		orders.setRemindCode(code);
		orders.setAbnormalId(abnormalId);//违章或者事故ID
		if (orders != null && !StringHelper.isRealNull(orders.getMemberId())) {
			orders.setMemberName(getMemberName(orders.getMemberId()).getName());
			orders.setMemberPhoneNo(getMemberName(orders.getMemberId()).getPhoneNo());
			AccDeposit deposit = new AccDeposit();
			deposit.setSubscriberId(orders.getMemberId());
			deposit = accDepositService.getDeposit(deposit);
			if(deposit != null){
				orders.setDeposit(deposit.getAmount().doubleValue());
			}
		}
		if (orders != null && !StringHelper.isRealNull(orders.getCarId())) {
			Car c = carService.queryCarById(orders.getCarId());
			if (c != null) {
				orders.setPlateNumber(c.getVehiclePlateId());
				CarVehicleModel cvm = c.getCarVehicleModel();
				if (cvm != null) {
					Dict d = DictUtil.getDictById(cvm.getBrand());
					orders.setVehicleModelName(d.getCnName() + " "
							+ cvm.getName());
				}
			}
			List<SysOperateLogRecord> logList = systemOperateLogService.getByRecordId(orders.getId(), "异常订单扣款押金不足提醒用户缴费充值,用户手机号");
			orders.setRemindNumber(logList == null?0:logList.size());
			orders.setInsuranceStr(carInsuranceNewService.getInsuranceStrByCarId(orders.getId()));			
		}
		dots = ordersService.queryDotByCon(null);
		Map<String, String> dotMap = new HashMap<String, String>();
		for (BranchDot dot : dots) {
			dotMap.put(dot.getId(), dot.getName());
		}
		if (!StringHelper.isRealNull(orders.getBeginSiteId())) {
			orders.setBeginSiteId(dotMap.get(orders.getBeginSiteId()));
		}
		if (!StringHelper.isRealNull(orders.getEndSiteId())) {
			orders.setEndSiteId(dotMap.get(orders.getEndSiteId()));
		}
		if (!StringHelper.isRealNull(orders.getOrdersBackSiteId())) {
			orders.setOrdersBackSiteId(dotMap.get(orders.getOrdersBackSiteId()));
		}
		return SUCCESS;
	}
	
	
	
	/**发送短信提醒用户缴费
	 * @return
	 */
	public String remindPayment(){
		try {
			HttpSession session = ServletActionContext.getRequest().getSession();
			User user = (User) session.getAttribute("user");
			String subphone = getRequest().getParameter("subphone");
			String coutent = getRequest().getParameter("coutent");
			String remindCode = getRequest().getParameter("remindCode");
			JSONObject obj = new JSONObject();
			obj.put("moblie", subphone);
			obj.put("content", coutent);
			obj.put("subId", user.getId());
			obj.put("name", user.getName());
			//加密
			CryptoTools des = new CryptoTools("HeGeA8G3".getBytes(),"6LA2EyQm".getBytes());
			String encode = des.encode(obj.toString());
			String ipcUrl = PropertiesHelper.getValue("ipc_url")+"/system/smsSend";
			String content = HttpRequestUtil.sendPostMethod(ipcUrl, encode);
			if(StringHelper.isNotEmpty(content)){
				JSONObject resultCode = JSONObject.fromObject(content);
				Integer code = Integer.parseInt(resultCode.get("resultCode").toString());
				result = Ajax.JSONResult(code,"成功！");
			}else{
				result = Ajax.JSONResult(1,"发送失败请重新发送！");
			}
			/*************** 添加短信发送记录 **********************/
			SysOperateLogRecord log = new SysOperateLogRecord();
			Date date = new Date();
			log.setOperatorId(user.getId());
			log.setOperatorName(user.getName());
			log.setOperateDate(date);
			log.setOperateRemark(SystemOperateLogUtil.REMIND_SEND_SMS);
			log.setRecordId(remindCode);
			log.setModelName("异常订单扣款");
			log.setKeyword("用户手机号："+subphone);
			log.setOperateContent("短信内容："+obj.get("content"));
			// 记录日志
			systemOperateLogService.addSysOperateLogRecord(log);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	
	/**异常订单扣款
	 * @return
	 */
	public String orderCharge(){
		HttpSession session = ServletActionContext.getRequest().getSession();
		User user = (User) session.getAttribute("user");
		Double paidfees = Double.parseDouble(getRequest().getParameter("paidfees"));//实收金额
		Double deposit = Double.parseDouble(getRequest().getParameter("deposit"));//会员押金金额
		Double chargespayable = Double.parseDouble(getRequest().getParameter("chargespayable"));//应收金额
		String orderId = getRequest().getParameter("orderId");
		String abnormalId = getRequest().getParameter("abnormalId");
		Integer abnormalType = Integer.parseInt(getRequest().getParameter("abnormalType"));
		Orders order = ordersService.queryOrdersById(orderId);
		//获取会员押金信息
		AccDeposit deposits = new AccDeposit();
		deposits.setSubscriberId(order.getMemberId());
		AccDeposit deposit1 = accDepositService.getDeposit(deposits);
		double sum = deposit - paidfees;//计算扣除相关费用之后的押金余额
		//获取会员押金可用金额
		double amount = deposit1.getUsableAmount().doubleValue();
		//获取会员冻结金额
		double frozenAmount = deposit1.getFrozenAmount().doubleValue();
		if(amount > 0){
			//如果没有申请退款则扣除可用金额
			amount = amount - paidfees;
			deposit1.setUsableAmount(Arith.round(amount));
		}else if(frozenAmount > 0){
			//获取最近一次的退款申请记录
			RefundRecord refundRecord = refundRecordService.getBySubMaxTime(order.getMemberId());
			if(refundRecord == null){
				result = Ajax.JSONResult(1,"退款失败数据错误请联系开发人员！");
				return SUCCESS;
			}
			//重新计算可退款金额更新退款申请记录的应退金额
			double actualMoney = refundRecord.getActualMoney() - paidfees;
			refundRecord.setActualMoney(actualMoney);
			refundRecordService.updateRefundRecord(refundRecord);
			//更新会员押金记录表金额
			DubDepositRecord dubDepositRecord = dubDepositRecordService.getByTradeOrderNo(refundRecord.getTradeOrderNo());
			dubDepositRecord.setMoney(actualMoney);
			dubDepositRecordService.updateDubDepositRecord(dubDepositRecord);
			//如果已申请退款则扣除冻结金额
			frozenAmount = frozenAmount - paidfees;//重新计算冻结金额
			deposit1.setFrozenAmount(Arith.round(frozenAmount));
		}
		//更新会员押金数据
		deposit1.setAmount(Arith.round(sum));
		accDepositService.updateDeposit(deposit1);
		//更新事故或者违章处理状态
		String errorType = "";
		if(order != null && abnormalType == 0){
			errorType = "违章扣款";
			CarViolation carViolation = carViolationService.searchCarViolationById(abnormalId);
			carViolation.setBizStatus("1");
			carViolation.setMoney(paidfees);
			carViolation.setHandleTime(new Date());
			carViolationService.updateCarViolation(carViolation);
		}else if(abnormalType == 1){
			errorType = "事故扣款";
			CarAccident carAccident = carAccidentService.searchCarAccidentById(abnormalId);
			Dict ydDict = DictUtil.getDictByCodes("handleStatus", "05");
			carAccident.setHandleStatus(ydDict.getId());
			carAccident.setMemberMoney(paidfees);
			carAccidentService.updateCarAccident(carAccident);
		}else{
			errorType = "未知";
			result = Ajax.JSONResult(1,"数据错误！");
			return SUCCESS;
		}
		//更新车辆违章事故记录表
		ViolationAccidentRecord violationAccidentRecord = new ViolationAccidentRecord();
		violationAccidentRecord = violationAccidentRecordService.getByOrdeeNo(abnormalId);
		violationAccidentRecord.setStatus(1);
		violationAccidentRecord.setPayStatus(1);
		violationAccidentRecord.setMoney(0.0);
		violationAccidentRecordService.updateViolationAccident(violationAccidentRecord);
		//添加用户押金扣款记录
		DubDepositRecord dubDepositRecord = new DubDepositRecord();
		dubDepositRecord.setCreateTime(new Date());
		dubDepositRecord.setSubId(order.getMemberId());
		dubDepositRecord.setType(1);
		dubDepositRecord.setMoney(paidfees);
		dubDepositRecord.setDesc(errorType+",订单编号："+order.getOrdersNo());
		dubDepositRecord.setPayStatus(1);
		dubDepositRecord.setCreatorId(user.getId());
		dubDepositRecord.setTradeOrderNo(TradeRecordNo.getCutPaymentNo());
		dubDepositRecordService.saveDubDepositRecord(dubDepositRecord);
		/*************** 添加异常订单扣款记录 **********************/
		SysOperateLogRecord log = new SysOperateLogRecord();
		Date date = new Date();
		log.setOperatorId(user.getId());
		log.setOperatorName(user.getName());
		log.setOperateDate(date);
		log.setOperateRemark(SystemOperateLogUtil.UPDATE_OPERATION);
		log.setModelName("异常订单扣款");
		log.setKeyword("异常订单类型："+errorType);
		log.setOperateContent("订单编号："+order.getOrdersNo()+",应扣款金额："+chargespayable+",实扣金额："+paidfees);
		// 记录日志
		systemOperateLogService.addSysOperateLogRecord(log);
		result = Ajax.JSONResult(0,"成功！");
		return SUCCESS;
	}

	public String orderReturnCar() {
		String id = getRequest().getParameter("id");
		orders = ordersService.queryOrdersById(id);
		Date endDate = orders.getEndTime();
		endDate = endDate == null ? new Date() : endDate;
		if (!StringHelper.isRealNull(orders.getMemberId())) {
			orders.setMemberName(getMemberName(orders.getMemberId()).getName());
		}
		if (!StringHelper.isRealNull(orders.getCarId())) {
			Car c = carService.queryCarById(orders.getCarId());
			if (c != null) {
				orders.setPlateNumber(c.getVehiclePlateId());
				CarVehicleModel cvm = c.getCarVehicleModel();
				if (cvm != null) {
					Dict d = DictUtil.getDictById(cvm.getBrand());
					orders.setVehicleModelName(d.getCnName() + " "
							+ cvm.getName());
				}
			}
		}
		dots = ordersService.queryDotByCon(null);
		Map<String, String> dotMap = new HashMap<String, String>();

		for (BranchDot dot : dots) {
			dotMap.put(dot.getId(), dot.getName());
		}
		if (!StringHelper.isRealNull(orders.getBeginSiteId())) {
			orders.setBeginSiteId(dotMap.get(orders.getBeginSiteId()));
		}
		if (!StringHelper.isRealNull(orders.getEndSiteId())) {
			orders.setEndSiteId(dotMap.get(orders.getEndSiteId()));
		}
		if (!StringHelper.isRealNull(orders.getOrdersBackSiteId())) {
			orders.setOrdersBackSiteId(dotMap.get(orders.getOrdersBackSiteId()));
		}
		if (orders.getEndTime() != null && orders.getBeginTime() != null) {
			Long time = orders.getEndTime().getTime()
					- orders.getBeginTime().getTime();
			Long minuteTime = (time / (1000 * 60)) % 60;
			orders.setOrdersDuration(Integer.parseInt("" + minuteTime));
		}
		ordersDetailList = ordersDetailService.getPaidOrdersDetailsByNo(orders
				.getOrdersNo());
		if (ordersDetailList != null && ordersDetailList.size() != 0) {
			for (int i = 0; i < ordersDetailList.size(); i++) {
				OrdersDetail ordersDetail = ordersDetailList.get(i);
				BigDecimal timeFee = strategyBaseService
						.conTimeFee(ordersDetail.getStrategyId(),
								ordersDetail.getBeginTime(), endDate,
								orders.getCarId());
				StrategyBase strategyBase = strategyBaseService
						.searchStrategyBaseById(ordersDetailList.get(i)
								.getStrategyId());
				ordersDetail.setMileFee(strategyBase.getKmPrice().multiply(
						ordersDetailList.get(i).getMileage()));
				ordersDetail.setMileage(ordersDetailList.get(i).getMileage());
				ordersDetail.setTimeFee(timeFee);
			}
		}
		orders.setOrdersDetail(ordersDetailList);
		return SUCCESS;
	}

	/**
	 * 根据订单编号获取订单信息
	 * 
	 * @return
	 */
	public String ordersDetailByNo() {
		String no = getRequest().getParameter("no");
		orders = ordersService.queryOrdersByOrderNo(no);
		if(orders == null){
			return ERROR;
		}
		if (!StringHelper.isRealNull(orders.getMemberId())) {
			orders.setMemberName(getMemberName(orders.getMemberId()).getName());
		}
		if (!StringHelper.isRealNull(orders.getCarId())) {
			Car c = carService.queryCarById(orders.getCarId());
			if (c != null) {
				orders.setPlateNumber(c.getVehiclePlateId());
				CarVehicleModel cvm = c.getCarVehicleModel();
				if (cvm != null) {
					Dict d = DictUtil.getDictById(cvm.getBrand());
					orders.setVehicleModelName(d.getCnName() + " "
							+ cvm.getName());
				}
			}
		}
		dots = ordersService.queryDotByCon(null);
		Map<String, String> dotMap = new HashMap<String, String>();

		for (BranchDot dot : dots) {
			dotMap.put(dot.getId(), dot.getName());
		}
		if (!StringHelper.isRealNull(orders.getBeginSiteId())) {
			orders.setBeginSiteId(dotMap.get(orders.getBeginSiteId()));
		}
		if (!StringHelper.isRealNull(orders.getEndSiteId())) {
			orders.setEndSiteId(dotMap.get(orders.getEndSiteId()));
		}
		if (!StringHelper.isRealNull(orders.getOrdersBackSiteId())) {
			orders.setOrdersBackSiteId(dotMap.get(orders.getOrdersBackSiteId()));
		}
		if (orders.getEndTime() != null && orders.getBeginTime() != null) {
			Long time = orders.getEndTime().getTime()
					- orders.getBeginTime().getTime();
			Long minuteTime = (time / (1000 * 60)) % 60;
			orders.setOrdersDuration(Integer.parseInt("" + minuteTime));
		}
		ordersDetailList = ordersDetailService.getPaidOrdersDetailsByNo(orders
				.getOrdersNo());
		orders.setOrdersDetail(ordersDetailList);
		return SUCCESS;
	}

	public static String getCarMileageForGPS(Map<String, String> map,
			String msgUrl) {

		String mileage = "";

		String content = HttpRequestUtil.getPostMethod(msgUrl, map);

		if (content != null && !"".equals(content)) {

			JSONObject obj = JSONObject.fromObject(content);

			if (obj.containsKey("result")) {

				if (obj.getString("result") != null
						&& !"".equals(obj.getString("result"))) {

					JSONObject objs = JSONObject.fromObject(obj
							.getString("result"));

					if (objs.getString("result").equals("1")) {// 接口返回数据成功

						mileage = objs.getString("value");
					}
				}
			}
		}
		System.out.println("里程接口调用返回数据:" + mileage);

		return mileage;

	}

	public static double div(double v1, double v2, int scale) {
		if (scale < 0) {
			throw new IllegalArgumentException(
					"The   scale   must   be   a   positive   integer   or   zero");
		}
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.divide(b2, scale, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	public static double mul(double v1, double v2) {
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.multiply(b2).doubleValue();
	}

	/**
	 * 加法计算
	 * 
	 * @param v1
	 * @param v2
	 * @return
	 */
	public static double add(double v1, double v2) {
		BigDecimal b1 = new BigDecimal(Double.toString(v1));
		BigDecimal b2 = new BigDecimal(Double.toString(v2));
		return b1.add(b2).doubleValue();
	}

	public SubCouponService getSubCouponService() {
		return subCouponService;
	}

	public void setSubCouponService(SubCouponService subCouponService) {
		this.subCouponService = subCouponService;
	}

	public CouponService getCouponService() {
		return couponService;
	}

	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getSortingType() {
		return sortingType;
	}

	public void setSortingType(String sortingType) {
		this.sortingType = sortingType;
	}

	public SystemOperateLogService getSystemOperateLogService() {
		return systemOperateLogService;
	}

	public void setSystemOperateLogService(
			SystemOperateLogService systemOperateLogService) {
		this.systemOperateLogService = systemOperateLogService;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public String getOrderState() {
		return orderState;
	}

	public void setOrderState(String orderState) {
		this.orderState = orderState;
	}
	
	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public BranchDotService getBranchDotService() {
		return branchDotService;
	}

	public void setBranchDotService(BranchDotService branchDotService) {
		this.branchDotService = branchDotService;
	}

	public List<BranchDot> getDots() {
		return dots;
	}

	public void setDots(List<BranchDot> dots) {
		this.dots = dots;
	}

	public List<OrdersDetail> getOrdersDetailList() {
		return ordersDetailList;
	}

	public void setOrdersDetailList(List<OrdersDetail> ordersDetailList) {
		this.ordersDetailList = ordersDetailList;
	}

	public OrdersDetailService getOrdersDetailService() {
		return ordersDetailService;
	}

	public void setOrdersDetailService(OrdersDetailService ordersDetailService) {
		this.ordersDetailService = ordersDetailService;
	}

	public StrategyBaseService getStrategyBaseService() {
		return strategyBaseService;
	}

	public void setStrategyBaseService(StrategyBaseService strategyBaseService) {
		this.strategyBaseService = strategyBaseService;
	}

	public OrdersDetail getOrderDetail() {
		return orderDetail;
	}

	public void setOrderDetail(OrdersDetail orderDetail) {
		this.orderDetail = orderDetail;
	}
	
	public OrdersSearchAction() {
		super();
		orders = new Orders();
		orderDetail = new OrdersDetail();
		page.setCountField("a.id");
		page.setCurrentPage(1);
	}

	public SubscriberService getSubscriberService() {
		return subscriberService;
	}

	public void setSubscriberService(SubscriberService subscriberService) {
		this.subscriberService = subscriberService;
	}

	public OrdersService getOrdersService() {
		return ordersService;
	}

	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}

	public Page<Orders> getPage() {
		return page;
	}

	public void setPage(Page<Orders> page) {
		this.page = page;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public DeviceBindingService getDeviceBindingService() {
		return deviceBindingService;
	}

	public void setDeviceBindingService(
			DeviceBindingService deviceBindingService) {
		this.deviceBindingService = deviceBindingService;
	}
	

	/**
	 * 根据ID获取会员信息
	 * 
	 * @param id
	 * @return
	 */
	public Subscriber getMemberName(String id) {
		Subscriber subscriber = subscriberService.querySubscriberById(id);
		if (subscriber != null) {
			return subscriber;
		}
		return null;
	}

	/**
	 * 更具ID获取网点信息
	 * 
	 * @param dotId
	 * @return
	 */
	public String getDotNameById(String dotId) {
		BranchDot bd = branchDotService.getBranchDotById(dotId);
		if (bd != null) {
			return bd.getName();
		}
		return "";
	}
	
	/**纠正错误数据方法勿动
	 * @return
	 * @throws ParseException
	 */
	public String updateOrdersFee() throws ParseException{
		//获取所有订单
		orders.setState("4");
		orders.setPayStatus(1);
//		orders.setOrdersNo("2017032112140001");
		List<Orders> list = ordersService.queryOrdersByCon(orders);
		for(int i=0;i<list.size();i++){
			Orders order = list.get(i);
			System.out.println("订单编号："+order.getOrdersNo());
			//根据订单编号获取订单详情
			List<OrdersDetail> orderDetailList = ordersDetailService.getAllOrdersDetailsByNo(order.getOrdersNo());
			//获取当前订单计费规则
			StrategyBase StrategyBase = strategyBaseService.searchStrategyBaseById(orderDetailList.get(0).getStrategyId());
			//获取订单是否使用优惠券
			SubCoupon scoupon = subCouponService.getSubcouponByOrderId(order.getId());
			//优惠券费用
			double couponFee = 0.0;
			//如果使用优惠卷获取优惠卷金额
			if(scoupon != null){
				Coupon coupon = couponService.queryCouponById(scoupon.getCouponId());
				couponFee = coupon.getPrice().doubleValue();
			}
//			couponFee = order.getCouponFee().doubleValue();
			//里程单价
			double kmPrice = StrategyBase.getKmPrice().doubleValue();
			//基础价格
			double basePrice = StrategyBase.getBasePrice().doubleValue();
			//计算用车时长
			SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			long nd = 1000*24*60*60;//一天的毫秒数
			long nh = 1000*60*60;//一小时的毫秒数
			long nm = 1000*60;//一分钟的毫秒数
			long diff = sd.parse(orderDetailList.get(0).getEndTime().toString()).getTime() - sd.parse(orderDetailList.get(0).getBeginTime().toString()).getTime();
			long day = diff/nd;//计算差多少天
			long hour = diff%nd/nh;//计算差多少小时
			long min = diff%nd%nh/nm;//计算差多少分钟
			//判断时长是否超过1天超过则转为分钟
			if(day > 0){
				day = day*24*60;
			}
			//判断时长是否超过1小时超过则转为分钟
			if(hour > 0){
				hour = hour*60;
			}
			//获取总用车时长
			double allTime = min+day+hour;
			if(allTime <= 0){
				allTime = 1.0;
			}
			//计算用车时长费用
			double allTimeFee = Arith.mul(allTime, basePrice);
			System.out.println("时间费用:"+allTimeFee);
			//获取用车里程(如果为空则赋值为0.0)
			double milage = orderDetailList.get(0).getMileage() == null?0.0:orderDetailList.get(0).getMileage().doubleValue();
			//计算里程费用
			double milageFee = Arith.mul(milage, kmPrice);
			System.out.println("里程费用:"+milageFee);
			//计算总费用
			double allFee = Arith.add(allTimeFee, milageFee);
			System.out.println("订单总费用:"+allFee);
			//获取实际支付金额
			double actualPayment = allFee;
			if(couponFee > 0){
				actualPayment = Arith.sub(allFee, couponFee);
				if(actualPayment < 0){
					actualPayment = 0.0;
				}
			}
			System.out.println("实际支付费用:"+actualPayment);
			//更新订单主表
			order.setTotalFee(Arith.round(allFee));//订单总费用
			order.setActualFee(Arith.round(actualPayment));//订单实际支付费用
			order.setCouponFee(Arith.round(couponFee));//订单优惠券费用
			ordersService.updateOrders(order);
			//更新订单详情表
			OrdersDetail orderDetail = orderDetailList.get(0);
			orderDetail.setTimeFee(Arith.round(allTimeFee));
			orderDetail.setMileFee(Arith.round(milageFee));
			ordersDetailService.updateOrdersDetail(orderDetail);
			System.out.println("结果数量："+list.size());
			System.out.println("更新第："+i+"条数据");
		}
		return null;
	}
	
	/**纠正错误数据方法勿动
	 * @return
	 * @throws ParseException
	 */
	public String updateOrdersFeeTo() throws ParseException{
		//获取所有订单
		orders.setState("4");
		orders.setPayStatus(1);
		List<Orders> orderList = new ArrayList<Orders>();
		List<Orders> list = ordersService.queryOrdersByCon(orders);
		int i=0;
		for(Orders order:list){
			double a = order.getCouponFee().doubleValue();
			System.out.println(i);
			System.out.println(order.getOrdersNo());
			i++;
			BigDecimal ss = new BigDecimal(0);
			BigDecimal topsPay =  (order.getTposPayFee() == null?ss:order.getTposPayFee());
			int b = order.getTotalFee().compareTo(topsPay);
			if(a == 0 && b == 1){
			 //更新优惠券
				orderList.add(order);
				double coupon = Arith.sub(order.getTotalFee().doubleValue(), topsPay.doubleValue());
				order.setCouponFee(Arith.round(coupon));
				ordersService.updateOrders(order);
			}
		}
		System.out.println(orderList.size());
		return null;
	}
	
	/**纠正错误数据方法勿动
	 * @return
	 * @throws ParseException
	 */
	public String updateOrdersFeeThree() throws ParseException{
		//获取所有订单
		orders.setState("4");
		orders.setPayStatus(1);
//		orders.setOrdersNo("2017031921280002");
		List<Orders> orderList = new ArrayList<Orders>();
		List<Orders> list = ordersService.queryOrdersByCon(orders);
		int i=0;
		for(Orders order:list){
			double a = order.getCouponFee().doubleValue();
			System.out.println(i);
			System.out.println(order.getOrdersNo());
			i++;
			BigDecimal ss = new BigDecimal(0);
			BigDecimal topsPay =  (order.getTposPayFee() == null?ss:order.getTposPayFee());
			int b = order.getTotalFee().compareTo(topsPay);
			if(a > 0 && b == 1){
				//更新订单实际支付金额
				orderList.add(order);
				double topsFee = Arith.sub(order.getTotalFee().doubleValue(), order.getCouponFee().doubleValue());
				if(topsFee < 0){
					topsFee = 0;
				}
				order.setActualFee(Arith.round(topsFee));
				ordersService.updateOrders(order);
			}
		}
		System.out.println(orderList.size());
		return null;
	}

	public AccDepositService getAccDepositService() {
		return accDepositService;
	}

	public void setAccDepositService(AccDepositService accDepositService) {
		this.accDepositService = accDepositService;
	}
	
	public CarAccidentService getCarAccidentService() {
		return carAccidentService;
	}

	public void setCarAccidentService(CarAccidentService carAccidentService) {
		this.carAccidentService = carAccidentService;
	}

	public CarViolationService getCarViolationService() {
		return carViolationService;
	}

	public void setCarViolationService(CarViolationService carViolationService) {
		this.carViolationService = carViolationService;
	}
	
	public ViolationAccidentRecordService getViolationAccidentRecordService() {
		return violationAccidentRecordService;
	}

	public void setViolationAccidentRecordService(
			ViolationAccidentRecordService violationAccidentRecordService) {
		this.violationAccidentRecordService = violationAccidentRecordService;
	}
	public DubDepositRecordService getDubDepositRecordService() {
		return dubDepositRecordService;
	}

	public void setDubDepositRecordService(
			DubDepositRecordService dubDepositRecordService) {
		this.dubDepositRecordService = dubDepositRecordService;
	}

    	
	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public static void main(String[] args) {
		JSONObject obj1 = JSONObject.fromObject("{\"resultCode\": 200,\"resultMsg\": \"成功\"}");
		
		System.out.println(obj1.get("resultCode"));
	}

	public CarInsuranceNewService getCarInsuranceNewService() {
		return carInsuranceNewService;
	}

	public void setCarInsuranceNewService(
			CarInsuranceNewService carInsuranceNewService) {
		this.carInsuranceNewService = carInsuranceNewService;
	}

	public List<Orders> getOrderList() {
		return orderList;
	}

	public void setOrderList(List<Orders> orderList) {
		this.orderList = orderList;
	}

	public String getIsAbnormity() {
		return isAbnormity;
	}

	public void setIsAbnormity(String isAbnormity) {
		this.isAbnormity = isAbnormity;
	}

	public String getAbnormalType() {
		return abnormalType;
	}

	public void setAbnormalType(String abnormalType) {
		this.abnormalType = abnormalType;
	}

	public String getOrdersNo() {
		return ordersNo;
	}

	public void setOrdersNo(String ordersNo) {
		this.ordersNo = ordersNo;
	}

	public RefundRecordService getRefundRecordService() {
		return refundRecordService;
	}

	public void setRefundRecordService(RefundRecordService refundRecordService) {
		this.refundRecordService = refundRecordService;
	}

	public String getBranchDotName() {
		return branchDotName;
	}

	public void setBranchDotName(String branchDotName) {
		this.branchDotName = branchDotName;
	}
	
}
