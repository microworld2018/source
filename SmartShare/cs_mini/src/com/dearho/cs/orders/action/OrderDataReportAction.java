package com.dearho.cs.orders.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.dearho.cs.orders.pojo.OrderDatareport;
import com.dearho.cs.orders.pojo.OrderDayData;
import com.dearho.cs.orders.service.OrderDataReportService;
import com.dearho.cs.orders.service.OrdersDetailService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.util.ExcelUtil;

/**
 * 订单数据报表
 * 
 * @author Gaopl
 *
 */
public class OrderDataReportAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private OrderDataReportService orderDataReportService;

	private OrdersDetailService ordersDetailService;

	private String startTime;
	private String endTime;
	private String phone;
	private String payType;
	private String carNumber;
	private String memberName;
	private Integer payStatus;

	/**
	 * 订单数据报表实体类
	 */
	private OrderDatareport orderDataReport;

	private List<OrderDayData> list;

	@Override
	public String process() {
		// 导出订单数据报表
		List<Object> list = orderDataReportService.getOrderDatareportPage(startTime, endTime, phone, payType, carNumber,
				memberName, payStatus,ordersDetailService);
		ExcelUtil.exportExcel(list, "订单数据报表" + new Date().getTime());
		return SUCCESS;
	}

	/**
	 * 订单日销售数据报表
	 * 
	 * @return
	 */
	public String orderDayData() {
		/*
		 * System.out.println(startTime);
		 * if(StringHelper.isNotEmpty(startTime)){
		 * orderDataReport.setBeginTime(startTime + "-01 00:00:00");
		 * orderDataReport.setEndTime(startTime + "-31 23:59:59"); }
		 */
		list = orderDataReportService.getOrderDatareportList(orderDataReport);
		return SUCCESS;
	}

	public String exportOrder() {
		// 导出订单数据报表
		List<OrderDayData> listOrder = orderDataReportService.getOrderDatareportList(orderDataReport);
		List<Object> list = new ArrayList<Object>();
		for (int i = 0; i < listOrder.size(); i++) {
			list.add(listOrder.get(i));
		}
		ExcelUtil.exportExcel(list, "订单每日数据报表" + new Date().getTime());
		return SUCCESS;
	}

	public OrderDataReportAction() {
		super();
		orderDataReport = new OrderDatareport();
	}

	public OrderDataReportService getOrderDataReportService() {
		return orderDataReportService;
	}

	public void setOrderDataReportService(OrderDataReportService orderDataReportService) {
		this.orderDataReportService = orderDataReportService;
	}

	public OrderDatareport getOrderDataReport() {
		return orderDataReport;
	}

	public void setOrderDataReport(OrderDatareport orderDataReport) {
		this.orderDataReport = orderDataReport;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public List<OrderDayData> getList() {
		return list;
	}

	public void setList(List<OrderDayData> list) {
		this.list = list;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public OrdersDetailService getOrdersDetailService() {
		return ordersDetailService;
	}

	public void setOrdersDetailService(OrdersDetailService ordersDetailService) {
		this.ordersDetailService = ordersDetailService;
	}

}
