package com.dearho.cs.orders.pojo;

import java.io.Serializable;
import java.math.BigDecimal;

import com.dearho.cs.sys.pojo.FieldComment;

public class OrderDayData  implements  Serializable {
	private static final long serialVersionUID = 1L;
	
	/**
	 * 交易日期
	 */
	@FieldComment("交易日期")
	private String dateTime;
	
	
	/**
	 * 实际支付金额
	 */
	@FieldComment("实际支付金额")
	private BigDecimal tposPayFee;
	/**
	 * 订单总金额
	 */
	@FieldComment("订单总金额")
	private BigDecimal totalFee3;
	/**
	 * 优惠券支付金额
	 */
	@FieldComment("优惠券支付金额")
	private BigDecimal couponFee;
	/**
	 * 订单数量
	 */
	@FieldComment("订单数量")
	private Integer orderCount;
	
	
	
	public BigDecimal getTposPayFee() {
		return tposPayFee;
	}
	public void setTposPayFee(BigDecimal tposPayFee) {
		this.tposPayFee = tposPayFee;
	}
	public BigDecimal getTotalFee3() {
		return totalFee3;
	}
	public void setTotalFee3(BigDecimal totalFee3) {
		this.totalFee3 = totalFee3;
	}
	public BigDecimal getCouponFee() {
		return couponFee;
	}
	public void setCouponFee(BigDecimal couponFee) {
		this.couponFee = couponFee;
	}
	public Integer getOrderCount() {
		return orderCount;
	}
	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}
	public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	
	
}
