package com.dearho.cs.orders.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.dearho.cs.sys.pojo.FieldComment;

/**
 * 数据报表实体类
 * 
 * @author Gaopl
 *
 */
public class OrderDatareport implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 订单编号
	 */
	@FieldComment("订单编号")
	private String ordersNo;

	/**
	 * 承租人
	 */
	@FieldComment("承租人")
	private String name;
	/**
	 * 联系方式
	 */
	@FieldComment("联系方式")
	private String phoneNo;
	/**
	 * 承租车辆
	 */
	@FieldComment("承租车辆")
	private String vehiclePlateId;
	/**
	 * 开始时间
	 */
	@FieldComment("开始时间")
	private String beginTime;
	/**
	 * 结束时间
	 */
	@FieldComment("结束时间")
	private String endTime;
	/**
	 * 订单总金额
	 */
	@FieldComment("订单总金额")
	private double totalFee;
	/**
	 * 优惠卷金额
	 */
	@FieldComment("优惠卷金额")
	private double price;
	/**
	 * 保险名称
	 */
	@FieldComment("保险名称")
	private String insuranceName;
	/**
	 * 保险金额
	 */
	@FieldComment("保险金额")
	private String insuranceFee;
	/**
	 * 保险总额
	 */
	/*@FieldComment("保险总额")
	private double insuranceFee;*/
	/**
	 * 应付金额
	 */
	@FieldComment("应付金额")
	private double amountsPayable;
	/**
	 * 实际支付金额
	 */
	@FieldComment("实际支付金额")
	private double totalFee1;
	/**
	 * 支付类型
	 */
	@FieldComment("支付类型")
	private String payType;

	/**
	 * 是否支付
	 */
	@FieldComment("是否支付")
	private String payState;

	/**
	 * 下单时间
	 */
	@FieldComment("下单时间")
	private String ordersTime;

	/**
	 * 交易日期
	 */
	@FieldComment("交易日期")
	private String dateTime;

	/**
	 * 实际支付金额
	 */
	@FieldComment("实际支付金额")
	private BigDecimal tposPayFee;
	/**
	 * 订单总金额
	 */
	@FieldComment("订单总金额")
	private BigDecimal totalFee3;
	/**
	 * 优惠券支付金额
	 */
	@FieldComment("优惠券支付金额")
	private BigDecimal couponFee;
	/**
	 * 订单数量
	 */
	@FieldComment("订单数量")
	private Integer orderCount;

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public BigDecimal getTposPayFee() {
		return tposPayFee;
	}

	public void setTposPayFee(BigDecimal tposPayFee) {
		this.tposPayFee = tposPayFee;
	}

	public BigDecimal getTotalFee3() {
		return totalFee3;
	}

	public void setTotalFee3(BigDecimal totalFee3) {
		this.totalFee3 = totalFee3;
	}

	public BigDecimal getCouponFee() {
		return couponFee;
	}

	public void setCouponFee(BigDecimal couponFee) {
		this.couponFee = couponFee;
	}

	public String getOrdersNo() {
		return ordersNo;
	}

	public void setOrdersNo(String ordersNo) {
		this.ordersNo = ordersNo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getVehiclePlateId() {
		return vehiclePlateId;
	}

	public void setVehiclePlateId(String vehiclePlateId) {
		this.vehiclePlateId = vehiclePlateId;
	}

	public String getBeginTime() {
		return beginTime;
	}

	public void setBeginTime(String beginTime) {
		this.beginTime = beginTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public double getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(double totalFee) {
		this.totalFee = totalFee;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getAmountsPayable() {
		return amountsPayable;
	}

	public void setAmountsPayable(double amountsPayable) {
		this.amountsPayable = amountsPayable;
	}

	public double getTotalFee1() {
		return totalFee1;
	}

	public void setTotalFee1(double totalFee1) {
		this.totalFee1 = totalFee1;
	}

	public String getPayType() {
		return payType;
	}

	public void setPayType(String payType) {
		this.payType = payType;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public String getPayState() {
		return payState;
	}

	public void setPayState(String payState) {
		this.payState = payState;
	}

	public String getOrdersTime() {
		return ordersTime;
	}

	public void setOrdersTime(String ordersTime) {
		this.ordersTime = ordersTime;
	}

	public String getInsuranceName() {
		return insuranceName;
	}

	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}

	public String getInsuranceFee() {
		return insuranceFee;
	}

	public void setInsuranceFee(String insuranceFee) {
		this.insuranceFee = insuranceFee;
	}

	/*public double getInsuranceFee() {
		return insuranceFee;
	}

	public void setInsuranceFee(double insuranceFee) {
		this.insuranceFee = insuranceFee;
	}*/

}
