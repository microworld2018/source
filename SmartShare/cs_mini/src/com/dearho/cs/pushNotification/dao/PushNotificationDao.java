package com.dearho.cs.pushNotification.dao;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.pushNotification.pojo.PushNotification;

public interface PushNotificationDao {

	Page<PushNotification> getPushNotificationPage(Page<PushNotification> page, PushNotification pushNotification) throws Exception;

	PushNotification getById(String id);

	void updatePushNotification(PushNotification pushNotification);

	void savePushNotification(PushNotification pushNotification);

}
