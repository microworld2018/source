package com.dearho.cs.pushNotification.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.pojo.InsuranceDesc;
import com.dearho.cs.insuranceRules.pojo.InsuranceRules;
import com.dearho.cs.pushNotification.dao.PushNotificationDao;
import com.dearho.cs.pushNotification.pojo.PushNotification;
import com.dearho.cs.util.StringHelper;

public class PushNotificationDaoImpl extends AbstractDaoSupport implements PushNotificationDao {

	@Override
	public Page<PushNotification> getPushNotificationPage(Page<PushNotification> page,
			PushNotification pushNotification) throws Exception {
		StringBuffer hql = new StringBuffer("select a.id from PushNotification a ");
		/*if(StringHelper.isNotEmpty(pushNotification.getId())){
			hql.append(" and a.id= '").append(pushNotification.getId()).append("'");
		}*/
		if(StringHelper.isNotEmpty(pushNotification.getTitle())){
			hql.append(" where a.title like '%").append(pushNotification.getTitle().trim()).append("%'");
		}
		hql.append(" order by a.createTime");
		Page<PushNotification> resultPage=pageCache(PushNotification.class, page, hql.toString());
		resultPage.setResults(idToObj(PushNotification.class, resultPage.getmResults()));
		return resultPage;
	}

	@Override
	public PushNotification getById(String id) {
		PushNotification pushNotification = get(PushNotification.class, id);
		return pushNotification;
	}

	@Override
	public void updatePushNotification(PushNotification pushNotification) {
		updateEntity(pushNotification);
	}

	@Override
	public void savePushNotification(PushNotification pushNotification) {
		addEntity(pushNotification);
	}

}
