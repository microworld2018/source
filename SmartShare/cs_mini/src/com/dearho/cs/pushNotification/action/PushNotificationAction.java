package com.dearho.cs.pushNotification.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.text.AbstractDocument.Content;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.pushNotification.pojo.PushNotification;
import com.dearho.cs.pushNotification.service.PushNotificationService;
import com.dearho.cs.pushNotification.util.HtmlUtil;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.HttpRequestUtil;
import com.dearho.cs.util.PropertiesHelper;
import com.dearho.cs.util.StringHelper;

import net.sf.json.JSONObject;

public class PushNotificationAction extends AbstractAction {

	private static final long serialVersionUID = -5970063595464294561L;

	private PushNotificationService pushNotificationService;

	private Page<PushNotification> page = new Page<>();// 推送通知分页

	private PushNotification pushNotification;// 推送通知实体类

	private String id;// 推送通知ID
	
	private String title;//前台查询条件

	private String state;// 类型（新增或者更新）

	@Override
	public String process() {
		try {
			page = pushNotificationService.getPushNotificationPage(page, pushNotification);
			// pushNotificationDaoList =
			// insuranceRulesService.getInusrancaeRulesLisst(new
			// InsuranceRules());
			if (state != null && state.equals("carInsurance")) {
				return "carInsurance";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}
	
	public String getByIdPushNotification(){
		try {
			if(state.equals("update")){
				pushNotification = pushNotificationService.getById(id);
			}
//			//查看保险内容
//			if(state.equals("toView")){
//				insuranceRules = insuranceRulesService.getById(id);
//				return "toView";
//			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	/**
	 * 立即发布推送消息
	 * @return
	 */
	public String toViewInsuranceOperating(){
		String pushId = getRequest().getParameter("id");
		try {
			Map<String,String> paramMap = new HashMap<String, String>();
			paramMap.put("pushId", pushId);
			String ipcUrl = PropertiesHelper.getValue("ipc_url")+"/message/pushAll";
			String content = HttpRequestUtil.getPostMethod(ipcUrl, paramMap);
			JSONObject jsonObject = new JSONObject();
//			if(StringHelper.isNotEmpty(content)){
//				jsonObject = JSONObject.fromObject(content);
//				Integer code = Integer.parseInt(jsonObject.getString("msg_id"));
////				result=Ajax.JSONResult(code, jsonObject.getString("sendno"));
//				result=Ajax.JSONResult(code, jsonObject.getString("sendno"));
//			}else{
//				jsonObject = JSONObject.fromObject(content);
//				result=Ajax.JSONResult(Integer.parseInt(jsonObject.getString("resultCode")), jsonObject.getString("resultMsg"));
//			}
			
			if(content!=null){
				result=Ajax.JSONResult(0, "推送成功");
				PushNotification pushNotificationSet = pushNotificationService.getById(pushId);
				int pushCount=pushNotificationSet.getPushCount();
				pushCount=pushCount+1;
				pushNotificationSet.setPushCount(pushCount);
				pushNotificationService.updatePushNotification(pushNotificationSet);
			}else{
				result=Ajax.JSONResult(1, "推送失败！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result=Ajax.JSONResult(1, "服务器异常请联系管理员！"+e.getMessage());
		}
		return SUCCESS;
	}
	
	/**新增或者更新推送消息
	 * @return
	 */
	public String saveOrUpdateNotification() {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try {
			if (StringHelper.isNotEmpty(pushNotification.getId())) {
				// 更新推送通知信息
				PushNotification pushNotificationSet = pushNotificationService.getById(pushNotification.getId());
				String title = pushNotification.getTitle();
				String content=pushNotification.getContent();
				String[] content1 = content.split("<p>");
				String[] content2 = content1[1].split("</p>");
				content=content2[0];
				pushNotificationSet.setUpdateTime(sdf.parse(sdf.format(new Date())));
				pushNotificationSet.setContent(content);
				pushNotificationSet.setTitle(title);
				pushNotificationService.updatePushNotification(pushNotificationSet);
				result = Ajax.JSONResult(0, "更新成功！");
			} else {
				//保存text
				//取得根目录路径  
//				String realPath1 = getRequest().getServletContext().getRealPath("/");
//				String[] realPath2 = realPath1.split("\\.", 0);
//				String realPath=realPath2[0];
//				String htmlContent=HtmlUtil.getHtml(pushNotification.getContent(), pushNotification.getTitle(),realPath);
				
				// 添加推送通知信息
				pushNotification.setCreateTime(sdf.parse(sdf.format(new Date())));
				pushNotification.setType(1);
				pushNotification.setContent(pushNotification.getContent());
				pushNotification.setPushCount(0);
				pushNotificationService.savePushNotification(pushNotification);
				result = Ajax.JSONResult(0, "添加成功！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.JSONResult(1, "更新或者添加失败！");
		}
		return SUCCESS;
	} 
	
	public PushNotificationAction() {
		super();
		pushNotification = new PushNotification();
		page.setCountField("a.id");
		page.setCurrentPage(1);
	}

	public PushNotificationService getPushNotificationService() {
		return pushNotificationService;
	}

	public void setPushNotificationService(PushNotificationService pushNotificationService) {
		this.pushNotificationService = pushNotificationService;
	}

	public Page<PushNotification> getPage() {
		return page;
	}

	public void setPage(Page<PushNotification> page) {
		this.page = page;
	}

	public PushNotification getPushNotification() {
		return pushNotification;
	}

	public void setPushNotification(PushNotification pushNotification) {
		this.pushNotification = pushNotification;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
