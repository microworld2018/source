package com.dearho.cs.pushNotification.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 推送消息实体类
 * 
 * @author Jin Guangyu
 *
 */
public class PushNotification implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 推送通知ID
	 */
	private String id;

	/**
	 * 通知标题
	 */
	private String title;

	/**
	 * 通知内容
	 */
	private String content;

	/**
	 * 通知类型 1:活动通知
	 */
	private Integer type;

	/**
	 * 创建时间
	 */
	private Date createTime;

	/**
	 * 最后一次更新时间
	 */
	private int pushCount;

	/**
	 * 最后一次更新时间
	 */
	private Date updateTime;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public int getPushCount() {
		return pushCount;
	}
	
	public void setPushCount(int pushCount) {
		this.pushCount = pushCount;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

}
