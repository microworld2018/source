package com.dearho.cs.pushNotification.service.impl;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.pushNotification.dao.PushNotificationDao;
import com.dearho.cs.pushNotification.pojo.PushNotification;
import com.dearho.cs.pushNotification.service.PushNotificationService;

public class PushNotificationServiceImpl implements PushNotificationService {

	private PushNotificationDao pushNotificationDao;

	public PushNotificationDao getPushNotificationDao() {
		return pushNotificationDao;
	}

	public void setPushNotificationDao(PushNotificationDao pushNotificationDao) {
		this.pushNotificationDao = pushNotificationDao;
	}

	@Override
	public Page<PushNotification> getPushNotificationPage(Page<PushNotification> page,
			PushNotification pushNotification) throws Exception {

		return pushNotificationDao.getPushNotificationPage(page, pushNotification);
	}

	@Override
	public PushNotification getById(String id) {
		return pushNotificationDao.getById(id);
	}

	@Override
	public void updatePushNotification(PushNotification pushNotification) {
		pushNotificationDao.updatePushNotification(pushNotification);
	}

	@Override
	public void savePushNotification(PushNotification pushNotification) {
		pushNotificationDao.savePushNotification(pushNotification);
	}

}
