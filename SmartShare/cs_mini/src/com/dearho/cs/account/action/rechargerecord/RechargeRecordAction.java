package com.dearho.cs.account.action.rechargerecord;

import java.util.ArrayList;
import java.util.List;

import org.omg.CORBA.PRIVATE_MEMBER;

import com.dearho.cs.Deposit.service.AccDepositService;
import com.dearho.cs.Deposit.service.DepositStrategyService;
import com.dearho.cs.RefundRecord.pojo.DubDepositRecord;
import com.dearho.cs.RefundRecord.pojo.RefundRecord;
import com.dearho.cs.RefundRecord.service.DubDepositRecordService;
import com.dearho.cs.RefundRecord.service.RefundRecordService;
import com.dearho.cs.account.service.AccountTradeRecordService;
import com.dearho.cs.account.service.RechargeRecordService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.service.OrderDataReportService;
import com.dearho.cs.subscriber.service.SubscriberService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.service.SystemOperateLogService;

/**
 * 押金充退记录
 * @author Jin Guangyu
 *
 */
public class RechargeRecordAction extends AbstractAction {
	
	private static final long serialVersionUID = 1L;
	private RefundRecordService refundRecordService;
	private SubscriberService subscriberService;
	private AccDepositService accDepositService;
	private DepositStrategyService depositStrategyService;
	private DubDepositRecordService dubDepositRecordService;
	private OrderDataReportService orderDataReportService;
	private AccountTradeRecordService accountTradeRecordService;
	private RechargeRecordService rechargeRecordService;
	private SystemOperateLogService systemOperateLogService;// 记录操作日志Service
	
	private List<DubDepositRecord> rechargeRefundRecordList = new ArrayList<>();
	
	private RefundRecord refundRecord;
	
	private String subPhoneNo;
	private String subName;
	private String tradeOrderNo;
	private String refundTradeNo;
	

	@Override
	public String process() {
		rechargeRefundRecordList = rechargeRecordService.getRechargeRefundRecords(subPhoneNo, subName, tradeOrderNo, refundTradeNo);
		return SUCCESS;
	}
	
	public RefundRecordService getRefundRecordService() {
		return refundRecordService;
	}

	public void setRefundRecordService(RefundRecordService refundRecordService) {
		this.refundRecordService = refundRecordService;
	}

	public SubscriberService getSubscriberService() {
		return subscriberService;
	}

	public void setSubscriberService(SubscriberService subscriberService) {
		this.subscriberService = subscriberService;
	}

	public AccDepositService getAccDepositService() {
		return accDepositService;
	}

	public void setAccDepositService(AccDepositService accDepositService) {
		this.accDepositService = accDepositService;
	}

	public DepositStrategyService getDepositStrategyService() {
		return depositStrategyService;
	}

	public void setDepositStrategyService(DepositStrategyService depositStrategyService) {
		this.depositStrategyService = depositStrategyService;
	}

	public DubDepositRecordService getDubDepositRecordService() {
		return dubDepositRecordService;
	}

	public void setDubDepositRecordService(DubDepositRecordService dubDepositRecordService) {
		this.dubDepositRecordService = dubDepositRecordService;
	}

	public OrderDataReportService getOrderDataReportService() {
		return orderDataReportService;
	}

	public void setOrderDataReportService(OrderDataReportService orderDataReportService) {
		this.orderDataReportService = orderDataReportService;
	}

	public AccountTradeRecordService getAccountTradeRecordService() {
		return accountTradeRecordService;
	}

	public void setAccountTradeRecordService(AccountTradeRecordService accountTradeRecordService) {
		this.accountTradeRecordService = accountTradeRecordService;
	}

	public SystemOperateLogService getSystemOperateLogService() {
		return systemOperateLogService;
	}

	public void setSystemOperateLogService(SystemOperateLogService systemOperateLogService) {
		this.systemOperateLogService = systemOperateLogService;
	}

	public RefundRecord getRefundRecord() {
		return refundRecord;
	}

	public void setRefundRecord(RefundRecord refundRecord) {
		this.refundRecord = refundRecord;
	}

	public RechargeRecordService getRechargeRecordService() {
		return rechargeRecordService;
	}

	public void setRechargeRecordService(RechargeRecordService rechargeRecordService) {
		this.rechargeRecordService = rechargeRecordService;
	}

	public List<DubDepositRecord> getRechargeRefundRecordList() {
		return rechargeRefundRecordList;
	}

	public void setRechargeRefundRecordList(List<DubDepositRecord> rechargeRefundRecordList) {
		this.rechargeRefundRecordList = rechargeRefundRecordList;
	}

	public String getSubPhoneNo() {
		return subPhoneNo;
	}

	public void setSubPhoneNo(String subPhoneNo) {
		this.subPhoneNo = subPhoneNo;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public String getTradeOrderNo() {
		return tradeOrderNo;
	}

	public void setTradeOrderNo(String tradeOrderNo) {
		this.tradeOrderNo = tradeOrderNo;
	}

	public String getRefundTradeNo() {
		return refundTradeNo;
	}

	public void setRefundTradeNo(String refundTradeNo) {
		this.refundTradeNo = refundTradeNo;
	}
	
}
