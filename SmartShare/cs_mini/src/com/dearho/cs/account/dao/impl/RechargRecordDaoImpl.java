/**
 * Copyrigh  (c) dearho Team
 * All rights reserved.
 *
 *This file AccountTradeRecordDaoImpl.java creation date:[2015-6-1 下午02:38:41] by liusong
 *http://www.dearho.com
 */
package com.dearho.cs.account.dao.impl;

import java.util.ArrayList;
import java.util.List;

import com.dearho.cs.RefundRecord.pojo.DubDepositRecord;
import com.dearho.cs.account.dao.RechargeRecordDao;
import com.dearho.cs.account.pojo.RechargeRecord;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.pojo.OrdersBill;
import com.dearho.cs.util.StringHelper;

/**
 * @Author liusong
 * @Description 
 * @Version 1.0,2015-6-1
 *
 */
public class RechargRecordDaoImpl extends AbstractDaoSupport implements RechargeRecordDao {




	@Override
	public void addRechargeRecord(RechargeRecord rechargeRecord) {
		addEntity(rechargeRecord);
	}


	@Override
	public void updateRechargeRecord(RechargeRecord rechargeRecord) {
		updateEntity(rechargeRecord);
	}


	@Override
	public Page<RechargeRecord> queryRechargeRecordByPage(Page<RechargeRecord> page, String hql) {
		Page<RechargeRecord> resultPage=pageCache(RechargeRecord.class, page, hql);
		resultPage.setResults(idToObj(RechargeRecord.class, resultPage.getmResults()));
		return resultPage;
	}


	@Override
	public RechargeRecord getRechargeRecordById(String id) {
		return get(RechargeRecord.class, id);
	}


	@Override
	public List<RechargeRecord> queryRechargeRecordList(String hql) {
		return getList(RechargeRecord.class, queryFList(hql));
	}


	/**
	 * 获取押金充退记录
	 */
	@Override
	public List<DubDepositRecord> getRechargeRefundRecords(String subPhoneNo, String subName, String tradeOrderNo, String refundTradeNo) {
		List<Object> rechargeRefundRecordList = new ArrayList<>();
		List<DubDepositRecord> records = new ArrayList<>();
		StringBuffer sb = new StringBuffer("SELECT * FROM "
				              + "(SELECT(SELECT ss.`name` FROM  sub_subscriber ss WHERE ss.id =dd.`sub_id`) AS `name`,"
				              + "(SELECT ss.`phone_no` FROM  sub_subscriber ss WHERE ss.id =dd.`sub_id` ) AS `phone_no`,"
				              + "money,trade_order_no,create_time,"
				              + "(SELECT `money` FROM `acc_refund_record` rr WHERE rr.`recharge_trade_no` = dd.`trade_order_no`) tk_money,"
				              + "(SELECT `trade_order_no` FROM `acc_refund_record` rr WHERE rr.`recharge_trade_no` = dd.`trade_order_no`) tk_trade_order_no,"
				              + "(SELECT `create_time` FROM `acc_refund_record` rr WHERE rr.`recharge_trade_no` = dd.`trade_order_no`) tk_create_time,"
				              + "(SELECT `status` FROM `acc_refund_record` rr WHERE rr.`recharge_trade_no` = dd.`trade_order_no`) tk_status,"
				              + "(SELECT `id` FROM `acc_refund_record` rr WHERE rr.`recharge_trade_no` = dd.`trade_order_no`) tk_id,"
				              + "(SELECT `immediately` FROM `acc_refund_record` rr WHERE rr.`recharge_trade_no` = dd.`trade_order_no`) tk_immediately"
				              + " FROM d_sub_deposit_record dd WHERE dd.`type`=0 AND dd.`pay_status`=1) e WHERE 1 = 1");
		if(StringHelper.isNotEmpty(subPhoneNo)){
			sb.append(" and e.phone_no like '%").append(subPhoneNo).append("%'");
		}
		if(StringHelper.isNotEmpty(subName)){
			sb.append(" and e.name like '%").append(subName).append("%'");
		}
		if(StringHelper.isNotEmpty(tradeOrderNo)){
			sb.append(" and trade_order_no like '%").append(tradeOrderNo).append("%'");
		}
		if(StringHelper.isNotEmpty(refundTradeNo)){
			sb.append(" and tk_trade_order_no like '%").append(refundTradeNo).append("%'");
		}
		sb.append(" ORDER BY e.tk_create_time DESC");
		rechargeRefundRecordList = getSession().createSQLQuery(sb.toString()).list();
		//遍历充退记录集合
		if(rechargeRefundRecordList != null && rechargeRefundRecordList.size() > 0){
			for(int i = 0;i < rechargeRefundRecordList.size();i++){
				DubDepositRecord record = new DubDepositRecord();
				Object[] o = (Object[])rechargeRefundRecordList.get(i);
				record.setSubName(o[0] == null ? null : o[0].toString());
				record.setSubPhoneNo(o[1] == null ? null : o[1].toString());
				record.setMoney(Double.parseDouble(o[2].toString()));
				record.setTradeOrderNo(o[3].toString());
				record.setRechargeTimeStr(o[4].toString());
				if(o[5] != null){
					record.setRefundMoney(Double.parseDouble(o[5].toString()));
				}
				record.setRefundTradeNo(o[6] == null ? null : o[6].toString());
				record.setRefundTime(o[7] == null ? null : o[7].toString().substring(0, 19));
				if(o[8] != null){
					record.setRefundStatus(Integer.parseInt(o[8].toString()));
				}
				record.setRefundRecordId(o[9] == null ? null : o[9].toString());
				if(o[10] != null){
					record.setImmediately(Integer.parseInt(o[10].toString()));
				}
//				record.setId(o[0].toString());
//				record.setSubName(o[1].toString());
//				record.setSubPhoneNo(o[2].toString());
//				record.setMoney(Double.parseDouble(o[3].toString()));
//				record.setTradeOrderNo(o[4].toString());
//				record.setRechargeTimeStr(o[5].toString().substring(0, 19));
//				if(o[6] != null){
//					record.setRefundMoney(Double.parseDouble(o[6].toString()));
//				}
//				record.setRefundTradeNo(o[7] == null ? null : o[7].toString());
//				record.setRefundTime(o[8] == null ? null : o[8].toString().substring(0, 19));
//				if(o[11] != null){
//					record.setRefundStatus(Integer.parseInt(o[11].toString()));
//				}
//				record.setRefundRecordId(o[12] == null ? null : o[12].toString());
//				if(o[13] != null){
//					record.setPayType(Integer.parseInt(o[13].toString()));
//				}
//				record.setSubId(o[14].toString());
//				if(o[15] != null){
//					record.setImmediately(Integer.parseInt(o[15].toString()));
//				}
				records.add(record);
			}
		}
		return records;
	}

}
