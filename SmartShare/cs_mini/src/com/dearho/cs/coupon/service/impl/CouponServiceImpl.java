package com.dearho.cs.coupon.service.impl;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.dao.CouponDao;
import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.coupon.service.CouponService;
import com.dearho.cs.util.StringHelper;

public class CouponServiceImpl implements CouponService {
	
	private CouponDao couponDao;
	

	public CouponDao getCouponDao() {
		return couponDao;
	}

	public void setCouponDao(CouponDao couponDao) {
		this.couponDao = couponDao;
	}

	@Override
	public void addCoupon(Coupon coupon) {
		couponDao.addCoupon(coupon);
	}

	@Override
	public void updateCoupon(Coupon coupon) {
		couponDao.updateCoupon(coupon);
	}

	@Override
	public void deleteCoupon(String[] ids) {
		// TODO Auto-generated method stub

	}

	@Override
	public Coupon queryCouponById(String id) {
		return couponDao.queryCouponById(id);
	}

	@Override
	public Page<Coupon> queryCouponByPage(Page<Coupon> page, Coupon coupon) {
		StringBuffer hql = new StringBuffer();
		hql.append("select a.id from Coupon a where 1=1");
		if(StringHelper.isNotEmpty(coupon.getCouponName())){
			hql.append(" and a.couponName = '"+coupon.getCouponName()+"'");
		}
		if(coupon.getPrice() != null){
			hql.append(" and a.price = '"+coupon.getPrice()+"'");
		}
		page = couponDao.queryCouponByPage(page, hql.toString());
		return page;
	}

	@Override
	public List<Coupon> getSubCouponList(String subId) throws Exception {
		return couponDao.getSubCouponList(subId);
	}

}
