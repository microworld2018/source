package com.dearho.cs.coupon.service;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.pojo.Coupon;

public interface CouponService {
	/**
	 * 添加优惠卷
	 * @param coupon
	 */
	void addCoupon(Coupon coupon);
	
	/**
	 * 修改优惠卷
	 * @param coupon
	 */
	void updateCoupon(Coupon coupon);
	
	/**
	 * 删除优惠卷
	 * @param id
	 */
	void deleteCoupon(String [] ids);
	
	/**
	 * 根据ID查询优惠卷
	 * @param id
	 * @return
	 */
	Coupon queryCouponById(String id);
	
	/**
	 * 分页
	 * @param page
	 * @param hql
	 * @return
	 */
	Page<Coupon> queryCouponByPage(Page<Coupon> page,Coupon coupon);
	
	
	/**查看用户优惠券使用情况
	 * @param subId
	 * @return
	 */
	List<Coupon> getSubCouponList(String subId)throws Exception;
}
