package com.dearho.cs.coupon.service.impl;

import java.util.List;

import com.dearho.cs.coupon.dao.SubCouponDao;
import com.dearho.cs.coupon.pojo.SubCoupon;
import com.dearho.cs.coupon.service.SubCouponService;

public class SubCouponServiceImpl implements SubCouponService {
	
	private SubCouponDao subCouponDao;

	public SubCouponDao getSubCouponDao() {
		return subCouponDao;
	}

	public void setSubCouponDao(SubCouponDao subCouponDao) {
		this.subCouponDao = subCouponDao;
	}

	@Override
	public void saveSubCoupon(SubCoupon subCoupon) {
		// TODO Auto-generated method stub
		subCouponDao.saveSubCoupon(subCoupon);
	}

	@Override
	public SubCoupon getSubcouponByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return subCouponDao.getSubcouponByOrderId(orderId);
	}

}
