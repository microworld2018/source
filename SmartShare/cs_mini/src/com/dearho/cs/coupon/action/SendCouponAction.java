package com.dearho.cs.coupon.action;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.coupon.pojo.SubCoupon;
import com.dearho.cs.coupon.service.CouponService;
import com.dearho.cs.coupon.service.SubCouponService;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.subscriber.service.SubscriberService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.SysOperateLogRecord;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.SystemOperateLogService;
import com.dearho.cs.sys.util.SystemOperateLogUtil;
import com.dearho.cs.util.Ajax;
import com.opensymphony.webwork.ServletActionContext;

public class SendCouponAction extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	
	private CouponService couponService;
	
	private Page<Subscriber> page = new Page<Subscriber>();//会员分页设置
	
	private SubscriberService subscriberService; //会员Service
	
	private SubCouponService subCouponService; //发放优惠卷Service
	
	private SystemOperateLogService systemOperateLogService;//记录操作日志Service

	private Coupon coupon;
	
	private Subscriber subscriber;
	
	private String [] checkdel;
	
	
	private String all;

	public SendCouponAction() {
		super();
		subscriber = new Subscriber();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}

	public CouponService getCouponService() {
		return couponService;
	}

	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

	public Page<Subscriber> getPage() {
		return page;
	}

	public void setPage(Page<Subscriber> page) {
		this.page = page;
	}

	public SubscriberService getSubscriberService() {
		return subscriberService;
	}

	public void setSubscriberService(SubscriberService subscriberService) {
		this.subscriberService = subscriberService;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}

	public Subscriber getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(Subscriber subscriber) {
		this.subscriber = subscriber;
	}

	public String[] getCheckdel() {
		return checkdel;
	}

	public void setCheckdel(String[] checkdel) {
		this.checkdel = checkdel;
	}

	public SubCouponService getSubCouponService() {
		return subCouponService;
	}

	public void setSubCouponService(SubCouponService subCouponService) {
		this.subCouponService = subCouponService;
	}

	public String getAll() {
		return all;
	}

	public void setAll(String all) {
		this.all = all;
	}

	public SystemOperateLogService getSystemOperateLogService() {
		return systemOperateLogService;
	}

	public void setSystemOperateLogService(
			SystemOperateLogService systemOperateLogService) {
		this.systemOperateLogService = systemOperateLogService;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String process() {
		page=subscriberService.querySubscriberByPage(page, subscriber);
		coupon = couponService.queryCouponById(coupon.getId());
		getRequest().setAttribute("coupon", coupon);
		return SUCCESS;
	}

	
	/**发放优惠卷
	 * @return
	 */
	public String sendCouponSub(){
		try {
			//获取优惠卷基本信息
			coupon = couponService.queryCouponById(coupon.getId());
			
			//判断优惠卷是否超过发放时间
			if (coupon.getSendEndTime().getTime() < new Date().getTime()) {
				result = Ajax.JSONResult(1, "优惠卷已过期！");
				return SUCCESS;
			}
			//判断优惠卷是否在发放时间之内
			if (new Date().getTime() < coupon.getSendStartTime().getTime()) {
				result = Ajax.JSONResult(1, "未到发放日期！");
				return SUCCESS;
			}
			//计算优惠卷到期时间
			Calendar now = Calendar.getInstance();  
		    now.setTime(new Date());  
		    now.set(Calendar.DATE, now.get(Calendar.DATE) + coupon.getValidPeriod());  
			//向所有用户发放
			List<Subscriber> list = subscriberService.querySubscriber(subscriber);
			if(all != null && all.equals("1")){
				int num1 = list.size();
				int sendNum1 = (coupon.getSendNumber() == null?0:coupon.getSendNumber()) + num1;
				if(sendNum1 > coupon.getNumber()){
					result=Ajax.JSONResult(1, "剩余数量不足！");
					return SUCCESS;
				}
				for(int i=0;i < list.size();i++){
					SubCoupon subCoupon = new SubCoupon();
					subCoupon.setCouponId(coupon.getId());
					subCoupon.setSubId(list.get(i).getId());
					subCoupon.setTs(new Date());
					subCoupon.setIsUsed(0);
					subCoupon.setExpirationTime(now.getTime());
					subCouponService.saveSubCoupon(subCoupon);
					coupon.setSendNumber(sendNum1);
					couponService.updateCoupon(coupon);
				}
				/***************发放优惠卷日志**********************/
				SysOperateLogRecord log = new SysOperateLogRecord();
				HttpSession session = ServletActionContext.getRequest().getSession();
				User user=(User) session.getAttribute("user");
				Date date = new Date();
				log.setOperatorId(user.getId());
				log.setOperatorName(user.getName());
				log.setOperateDate(date);
				log.setOperateRemark(SystemOperateLogUtil.UPDATE_OPERATION);
				log.setModelName("向所有用户发放优惠卷");
				log.setRecordId(coupon.getId());
				log.setKeyword(coupon.getCouponName());
				log.setOperateContent("发放优惠卷名称:"+coupon.getCouponName());
				// 记录日志
				systemOperateLogService.addSysOperateLogRecord(log);
				result=Ajax.JSONResult(0, "发放成功！");
				return SUCCESS;
			}
			//给指定用户发放
			int num = checkdel.length;
			int sendNum = (coupon.getSendNumber() == null?0:coupon.getSendNumber()) + num;
			if(sendNum > coupon.getNumber()){
				result=Ajax.JSONResult(1, "剩余数量不足！");
				return SUCCESS;
			}
			StringBuffer ids = new StringBuffer();
			for(int i=0;i < num;i++){
				SubCoupon subCoupon = new SubCoupon();
				subCoupon.setCouponId(coupon.getId());
				subCoupon.setSubId(checkdel[i]);
				subCoupon.setTs(new Date());
				subCoupon.setIsUsed(0);
				subCoupon.setExpirationTime(now.getTime());
				subCouponService.saveSubCoupon(subCoupon);
				coupon.setSendNumber(sendNum);
				couponService.updateCoupon(coupon);
				result=Ajax.JSONResult(0, "发放成功！");
				if(i<num-1){
					ids.append("'").append(checkdel[i]).append("',");
				}else{
					ids.append("'").append(checkdel[i]).append("'");
				}
			}
			/***************发放优惠卷日志**********************/
			List<Subscriber> list1 = subscriberService.searchSubscribersIn(ids.toString());
			StringBuffer userName = new StringBuffer();
			for(int i=0;i<list1.size();i++){
				if(i<list1.size()-1){
					userName.append(list1.get(i).getName() == null?"暂无会员名称":list1.get(i).getName()).append(":").append(list1.get(i).getPhoneNo()).append(",");
				}else{
					userName.append(list1.get(i).getName() == null?"暂无会员名称":list1.get(i).getName()).append(":").append(list1.get(i).getPhoneNo()).append(",");
				}
			}
			SysOperateLogRecord log = new SysOperateLogRecord();
			HttpSession session = ServletActionContext.getRequest().getSession();
			User user=(User) session.getAttribute("user");
			Date date = new Date();
			log.setOperatorId(user.getId());
			log.setOperatorName(user.getName());
			log.setOperateDate(date);
			log.setOperateRemark(SystemOperateLogUtil.UPDATE_OPERATION);
			log.setModelName("指定用户发放优惠卷");
			log.setRecordId(coupon.getId());
			log.setKeyword(coupon.getCouponName());
			log.setOperateContent("发放人员:"+userName);
			// 记录日志
			systemOperateLogService.addSysOperateLogRecord(log);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result=Ajax.JSONResult(1, "系统异常请联系管理员！");
		}
		return SUCCESS;
	}
	
}
