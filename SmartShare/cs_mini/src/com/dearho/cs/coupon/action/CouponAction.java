package com.dearho.cs.coupon.action;


import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.cxf.common.util.StringUtils;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.coupon.service.CouponService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.SysOperateLogRecord;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.SystemOperateLogService;
import com.dearho.cs.sys.util.SystemOperateLogUtil;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.StringHelper;
import com.opensymphony.webwork.ServletActionContext;

public class CouponAction extends AbstractAction {

	private static final long serialVersionUID = 1L;

	private CouponService couponService;

	private Page<Coupon> page = new Page<Coupon>(); // 分页设置
	
	private Coupon coupon;
	
	private List<Coupon> list;
	
	private SystemOperateLogService systemOperateLogService;//记录操作日志Service
	
	public SystemOperateLogService getSystemOperateLogService() {
		return systemOperateLogService;
	}

	public void setSystemOperateLogService(
			SystemOperateLogService systemOperateLogService) {
		this.systemOperateLogService = systemOperateLogService;
	}

	public CouponService getCouponService() {
		return couponService;
	}

	public void setCouponService(CouponService couponService) {
		this.couponService = couponService;
	}

	public Page<Coupon> getPage() {
		return page;
	}

	public void setPage(Page<Coupon> page) {
		this.page = page;
	}

	public Coupon getCoupon() {
		return coupon;
	}

	public void setCoupon(Coupon coupon) {
		this.coupon = coupon;
	}
	
	public List<Coupon> getList() {
		return list;
	}

	public void setList(List<Coupon> list) {
		this.list = list;
	}

	public CouponAction() {
		super();
		coupon = new Coupon();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}

	@Override
	public String process() {
		//新增或者修改优惠卷信息
		try {
			if(StringUtils.isEmpty(coupon.getId())){
				coupon.setTs(new Date());
				coupon.setIsUsed(1);
				coupon.setSendType(0);
				coupon.setSendNumber(0);
				couponService.addCoupon(coupon);
				result=Ajax.JSONResult(0, "添加成功！");
				/***************记录新增优惠卷日志**********************/
				SysOperateLogRecord log = new SysOperateLogRecord();
				HttpSession session = ServletActionContext.getRequest().getSession();
				User user=(User) session.getAttribute("user");
				Date date = new Date();
				log.setOperatorId(user.getId());
				log.setOperatorName(user.getName());
				log.setOperateDate(date);
				log.setOperateRemark(SystemOperateLogUtil.ADD_OPERATION);
				log.setModelName("新增优惠券");
				log.setRecordId(coupon.getId());
				log.setKeyword(coupon.getCouponName());
				log.setOperateContent("新增优惠券:"+coupon.getCouponName());
				// 记录日志
				systemOperateLogService.addSysOperateLogRecord(log);
			}else{
				Coupon coupon1 = couponService.queryCouponById(coupon.getId());
				Integer number = coupon1.getSendNumber() == null?0:coupon1.getSendNumber();
				if(number > coupon.getNumber()){
					result=Ajax.JSONResult(1, "发行数量不能小于发放数量，当前发放数量为:"+coupon1.getSendNumber());
					return SUCCESS;
				}
				coupon1.setCouponName(coupon.getCouponName());
				coupon1.setPrice(coupon.getPrice());
				coupon1.setOrderUserPrice(coupon.getOrderUserPrice());
				coupon1.setNumber(coupon.getNumber());
//				coupon1.setCouponContent(coupon.getCouponContent());
				coupon1.setRemark(coupon.getRemark());
				if(coupon.getCouponType() != null){
					coupon1.setCouponType(coupon.getCouponType());
				}
				coupon1.setSendStartTime(coupon.getSendStartTime());
				coupon1.setSendEndTime(coupon.getSendEndTime());
				coupon1.setStartValidityTime(coupon.getStartValidityTime());
				coupon1.setEndValidityTime(coupon.getEndValidityTime());
				coupon1.setValidPeriod(coupon.getValidPeriod());
				coupon1.setIsHoliday(coupon.getIsHoliday());
				couponService.updateCoupon(coupon1);
				result=Ajax.JSONResult(0, "修改成功！");
				/***************记录修改优惠卷日志**********************/
				SysOperateLogRecord log = new SysOperateLogRecord();
				HttpSession session = ServletActionContext.getRequest().getSession();
				User user=(User) session.getAttribute("user");
				Date date = new Date();
				log.setOperatorId(user.getId());
				log.setOperatorName(user.getName());
				log.setOperateDate(date);
				log.setOperateRemark(SystemOperateLogUtil.UPDATE_OPERATION);
				log.setModelName("修改优惠券");
				log.setRecordId(coupon.getId());
				log.setKeyword(coupon.getCouponName());
				log.setOperateContent("修改优惠券:"+coupon.getCouponName());
				// 记录日志
				systemOperateLogService.addSysOperateLogRecord(log);
			}
			
		} catch (Exception e) {
			result=Ajax.JSONResult(1, "操作失败！");
		}
		return SUCCESS;
	}

	/**根据条件分页获取优惠卷列表
	 * @return
	 */
	public String getPageCoupon() {
		page = couponService.queryCouponByPage(page, coupon);
		return SUCCESS;
	}
	
	
	/**根据ID获取优惠卷信息
	 * @return
	 */
	public String getCouponById(){
		coupon = couponService.queryCouponById(coupon.getId());
		getRequest().setAttribute("coupon", coupon);
		return SUCCESS;
	}
	
	public String getBySubIdCoupon(){
		String subId = getRequest().getParameter("subId");
		try {
			list = couponService.getSubCouponList(subId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
}
