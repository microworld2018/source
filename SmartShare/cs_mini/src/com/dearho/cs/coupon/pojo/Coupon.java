package com.dearho.cs.coupon.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


import com.dearho.cs.util.DateUtil;
	
	public class Coupon  implements Serializable{

		private static final long serialVersionUID = -1022843122831880207L;
		
		private String id;
		
		private String couponName;//优惠券名字
		
		private Integer couponType;//类型
		
		private String couponContent;//描述
		
		private BigDecimal price;//金额
		
		private Integer isUsed;//是否启用 0否 1是
		
		private Date startValidityTime;//有效期开始时间
		
		private Date endValidityTime;//有效期结束时间
		
		private Date ts;//时间戳
		
		private Integer number;//发行数量
		
		private Integer sendNumber;//发放数量
		
		private Integer sendType;//发放类型 0:安用户发放
		
		private String picture;//图片地址
		
		private BigDecimal orderUserPrice;//可使用订单金额
		
		private Date sendStartTime;//发放开始日期
		
		private Date sendEndTime;//发放结束日期
		
		private String remark;//备注
		
		
		private String testStartTime;
		
		private Integer validPeriod;//优惠卷有效期（单位/天）
		
		
		private Integer allCount;
		
		private Integer useCount;
		
		private Integer isHoliday;//可用时间
		
		public String getId() {
			return id;
		}
		
		public void setId(String id) {
			this.id = id;
		}
		
		public String getCouponName() {
			return couponName;
		}
		
		public void setCouponName(String couponName) {
			this.couponName = couponName;
		}
		
		public Integer getCouponType() {
			return couponType;
		}
		
		public void setCouponType(Integer couponType) {
			this.couponType = couponType;
		}
		
		public String getCouponContent() {
			return couponContent;
		}
		
		public void setCouponContent(String couponContent) {
			this.couponContent = couponContent;
		}
		
		public BigDecimal getPrice() {
			return price;
		}
		
		public void setPrice(BigDecimal price) {
			this.price = price;
		}
		
		public Integer getIsUsed() {
			return isUsed;
		}
		
		public void setIsUsed(Integer isUsed) {
			this.isUsed = isUsed;
		}
		
		public Date getStartValidityTime() {
			return startValidityTime;
		}
		
		public void setStartValidityTime(Date startValidityTime) {
			this.startValidityTime = startValidityTime;
		}
		
		public Date getEndValidityTime() {
			return endValidityTime;
		}
		
		public void setEndValidityTime(Date endValidityTime) {
			this.endValidityTime = endValidityTime;
		}
		
		public Date getTs() {
			
			return ts;
		}
		
		public void setTs(Date ts) {
			this.ts = ts;
		}

		public Integer getNumber() {
			return number;
		}

		public void setNumber(Integer number) {
			this.number = number;
		}

		public Integer getSendNumber() {
			return sendNumber;
		}

		public void setSendNumber(Integer sendNumber) {
			this.sendNumber = sendNumber;
		}

		public Integer getSendType() {
			return sendType;
		}

		public void setSendType(Integer sendType) {
			this.sendType = sendType;
		}

		public String getPicture() {
			return picture;
		}

		public void setPicture(String picture) {
			this.picture = picture;
		}

		public BigDecimal getOrderUserPrice() {
			return orderUserPrice;
		}

		public void setOrderUserPrice(BigDecimal orderUserPrice) {
			this.orderUserPrice = orderUserPrice;
		}

		public Date getSendStartTime() {
			return sendStartTime;
		}

		public void setSendStartTime(Date sendStartTime) {
			this.sendStartTime = sendStartTime;
		}

		public Date getSendEndTime() {
			return sendEndTime;
		}

		public void setSendEndTime(Date sendEndTime) {
			this.sendEndTime = sendEndTime;
		}

		public String getRemark() {
			return remark;
		}

		public void setRemark(String remark) {
			this.remark = remark;
		}

		public String getTestStartTime() {
			return testStartTime =  DateUtil.formatDate(getSendStartTime(),"yyyy-MM-dd");
		}

		public void setTestStartTime(String testStartTime) {
			this.testStartTime = testStartTime;
		}

		public Integer getValidPeriod() {
			return validPeriod;
		}

		public void setValidPeriod(Integer validPeriod) {
			this.validPeriod = validPeriod;
		}

		public Integer getAllCount() {
			return allCount;
		}

		public void setAllCount(Integer allCount) {
			this.allCount = allCount;
		}

		public Integer getUseCount() {
			return useCount;
		}

		public void setUseCount(Integer useCount) {
			this.useCount = useCount;
		}

		public Integer getIsHoliday() {
			return isHoliday;
		}

		public void setIsHoliday(Integer isHoliday) {
			this.isHoliday = isHoliday;
		}
		
		
		
}
