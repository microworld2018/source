package com.dearho.cs.coupon.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class SubCoupon implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String id;
	
	/**
	 * 优惠卷ID
	 */
	private String couponId;
	
	/**
	 * 用户ID
	 */
	private String subId;
	
	/**
	 * 是否使用
	 */
	private Integer isUsed;
	
	/**
	 * 时间戳
	 */
	private Date ts;
	
	/**
	 * 订单ID
	 */
	private String orderId;
	
	/**
	 * 优惠卷到期时间
	 */
	private Date expirationTime;
	
	
	private BigDecimal price;
	
	private String couponName;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCouponId() {
		return couponId;
	}

	public void setCouponId(String couponId) {
		this.couponId = couponId;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public Integer getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
	}

	public Date getTs() {
		return ts;
	}

	public void setTs(Date ts) {
		this.ts = ts;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getCouponName() {
		return couponName;
	}

	public void setCouponName(String couponName) {
		this.couponName = couponName;
	}

	public Date getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Date expirationTime) {
		this.expirationTime = expirationTime;
	}

	
	
}
