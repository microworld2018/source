package com.dearho.cs.coupon.dao;

import java.util.List;

import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.coupon.pojo.SubCoupon;

public interface SubCouponDao {
	
	/**发放优惠卷
	 * @param subCoupon
	 */
	void saveSubCoupon(SubCoupon subCoupon);
	
	/**根据订单ID查询优惠卷信息
	 * @param orderId
	 * @return
	 */
	SubCoupon getSubcouponByOrderId(String orderId);

}
