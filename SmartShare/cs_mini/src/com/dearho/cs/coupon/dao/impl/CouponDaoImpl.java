package com.dearho.cs.coupon.dao.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;






import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.dao.CouponDao;
import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.util.orders.Arith;

public class CouponDaoImpl extends AbstractDaoSupport implements CouponDao {

	class mapKey{
		public String name;
		public Integer price;
		public Date endTime;
		public mapKey() {
		}
		public mapKey(String name, Integer price, Date endTime) {
			this.name = name;
			this.price = price;
			this.endTime = endTime;
		}
	}
	class mapVal{
		public Integer allCount;
		public Integer useCount;
	}
	
	@Override
	public void addCoupon(Coupon coupon) {
		addEntity(coupon);
	}

	@Override
	public void updateCoupon(Coupon coupon) {
		updateEntity(coupon);
	}

	@Override
	public void deleteCoupon(String[] ids) {
		final String hql = "delete Coupon where id in (:ids)";
		deleteEntity(Coupon.class, hql, ids);
	}

	@Override
	public Coupon queryCouponById(String id) {
		Coupon coupon = get(Coupon.class, id);
		if (null == coupon)
			return null;
		return coupon;
	}

	@Override
	public Page<Coupon> queryCouponByPage(Page<Coupon> page, String hql) {
		Page<Coupon> resultPage = pageCache(Coupon.class, page, hql);
		resultPage.setResults(idToObj(Coupon.class, resultPage.getmResults()));
		return resultPage;
	}

	/*@Override
	public List<Coupon> getSubCouponList(String subId) {
		StringBuffer sql = new StringBuffer("SELECT c.coupon_name,c.price,sc.is_used,"
				+ "c.start_validity_time,c.end_validity_time"
				+ " FROM spo_sub_coupon sc,spo_coupon c"
				+ " WHERE sc.coupon_id = c.ID AND sc.sub_id = '"+subId+"'");
		@SuppressWarnings("rawtypes")
		List list=getSession().createSQLQuery(sql.toString()).list();
		Map<mapKey, mapVal> map = new HashMap<mapKey, mapVal>();
		mapKey condition = new mapKey();
		mapVal val = new mapVal();
		for(int i = 0; i < list.size(); i++){
			Object[] m=(Object[])list.get(i);
			condition.name = m[0].toString();
			condition.price = Integer.parseInt(m[1].toString());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd "); 
			String str = m[4].toString();
			try {
				condition.endTime = sdf.parse(str);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			if(map.containsKey(condition)){
				//如果相同则进行数量相加
				val = map.get(condition);
				val.allCount = val.allCount + 1;
				val.useCount = val.useCount + Integer.parseInt(m[2].toString());
			}else{
				//如果不同做插入
				val.allCount = 1;
				val.useCount = Integer.parseInt(m[2].toString());
			}
			map.put(condition, val);
		}
		System.out.println(map);
		return null;
	}*/
	@Override
	public List<Coupon> getSubCouponList(String subId) throws ParseException {
		StringBuffer sql = new StringBuffer("SELECT c.coupon_name, c.price, COUNT(*), "
				+ "SUM(case sc.is_used when 1 then 1 else 0 END), "
				+ "DATE_FORMAT(sc.expiration_time,'%Y-%m-%d') FROM spo_sub_coupon sc, "
				+ "spo_coupon c "
				+ "WHERE sc.coupon_id = c.ID"
				+ " AND sc.sub_id = '"+subId+"' GROUP BY C.coupon_name");
		@SuppressWarnings("rawtypes")
		List list=getSession().createSQLQuery(sql.toString()).list();
		List<Coupon> couponList = new ArrayList<Coupon>();
		for(int i=0;i<list.size();i++){
			Object[] obj = (Object[]) list.get(i);
			Coupon cou = new Coupon();
			cou.setCouponName(obj[0].toString());
			cou.setPrice(Arith.round(Double.parseDouble(obj[1].toString())));
			cou.setAllCount(Integer.parseInt(obj[2].toString()));
			cou.setUseCount(Integer.parseInt(obj[3].toString()));
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
			 Date date = sdf.parse(obj[4].toString());  
			cou.setEndValidityTime(date);
			couponList.add(cou);
		}
		return couponList;
	}
}
