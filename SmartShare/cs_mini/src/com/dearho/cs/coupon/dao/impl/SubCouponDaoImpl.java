package com.dearho.cs.coupon.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.coupon.dao.SubCouponDao;
import com.dearho.cs.coupon.pojo.SubCoupon;
import com.dearho.cs.util.StringHelper;

public class SubCouponDaoImpl extends AbstractDaoSupport implements SubCouponDao {

	@Override
	public void saveSubCoupon(SubCoupon subCoupon) {
		addEntity(subCoupon);
	}

	@Override
	public SubCoupon getSubcouponByOrderId(String orderId) {
		StringBuffer hql = new StringBuffer("SELECT id FROM SubCoupon ");
		if(StringHelper.isNotEmpty(orderId)){
			hql.append(" WHERE orderId = '").append(orderId).append("'");
			List<SubCoupon> list = getList(SubCoupon.class, queryFList(hql.toString()));
			if(list != null && list.size()>0){
				return list.get(0);
			}else{
				return null;
			}
		}else{
			return null;
		}
		
	}
}
