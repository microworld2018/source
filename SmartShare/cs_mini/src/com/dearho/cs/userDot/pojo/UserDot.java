package com.dearho.cs.userDot.pojo;

import java.io.Serializable;
import java.util.Date;

/**用户网点实体类
 * @author Gaopl
 * @date 2017-01-13
 */
public class UserDot implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 用户网点关系ID
	 */
	private String id;
	
	/**
	 * 网点ID
	 */
	private String dotId;
	
	/**
	 * 用户ID
	 */
	private String userId;
	
	/**
	 * 区域ID
	 */
	private String areaId;
	
	/**
	 * 创建人
	 */
	private String createUser;
	
	/**
	 * 创建时间
	 */
	private Date createDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDotId() {
		return dotId;
	}

	public void setDotId(String dotId) {
		this.dotId = dotId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
