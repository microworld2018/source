package com.dearho.cs.userDot.action;

import java.util.Date;
import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.service.BranchDotService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.AreaService;
import com.dearho.cs.sys.service.UserService;
import com.dearho.cs.userDot.pojo.UserDot;
import com.dearho.cs.userDot.service.UserDotService;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.Constants;

public class UserDotAction extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 网点Service
	 */
	private BranchDotService branchDotService;
	
	/**
	 * 用户Service
	 */
	private UserService userService;
	
	/**
	 * 分页工具类
	 */
	private Page<BranchDot> page = new Page<BranchDot>();
	
	/**
	 * 用户网点关系实体类
	 */
	private UserDot userDot;
	
	/**
	 * 行政区划Service
	 */
	private AreaService areaService;
	
	/**
	 * 行政区划实体类
	 */
	private List<AdministrativeArea> areas;
	
	/**
	 * 参数
	 */
	private String id;
	
	/**
	 * 网点实体类
	 */
	private BranchDot branchDot;
	
	/**
	 * 选中的网点ID数组
	 */
	private String [] checkdel;
	
	/**
	 * 用户实体类
	 */
	private User user;
	
	/**
	 * 用户网点Service
	 */
	private UserDotService userDotService;
	
	
/****页面控制方法开始***********************************************************************************/
	
	@Override
	public String process() {
		/*获取网点列表*/
		UserDot userDot1 = new UserDot();
		userDot1.setUserId(id);
		List<UserDot> list = userDotService.getUserDotList(userDot1);
		if(list != null){
			StringBuffer sb = new StringBuffer();
			for(int i=0;i<list.size();i++){
				if(i<list.size()-1){
					sb.append("'").append(list.get(i).getDotId()).append("',");
				}else{
					sb.append("'").append(list.get(i).getDotId()).append("'");
				}
				
			}
		branchDot.setNotIds(sb.toString());
		page = branchDotService.searchBranchDot(page, branchDot);
		user = userService.searchUserByID(id);
		getRequest().setAttribute("user", user);
		return SUCCESS;
		}else{
			page = branchDotService.searchBranchDot(page, branchDot);
			user = userService.searchUserByID(id);
			getRequest().setAttribute("user", user);
			return SUCCESS;
		}
	}
	
	
	/**分配网点
	 * @return
	 */
	public String distributionDot(){
		try {
			int num = checkdel.length;
			User user1=(User)getSession().getAttribute(Constants.SESSION_USER);
			for(int i=0;i < num;i++){
				userDot = new UserDot();
				userDot.setUserId(id);
				userDot.setDotId(checkdel[i]);
				//userDot.setAreaId(areaId);
				userDot.setCreateDate(new Date());
				userDot.setCreateUser(user1.getId());
				userDotService.asveUserDot(userDot);
			}
			result=Ajax.JSONResult(0, "分配完成！");
		} catch (Exception e) {
			e.printStackTrace();
			result=Ajax.JSONResult(1, "分配失败！");
		}
		return SUCCESS;
	}
	
	
	/**查看用户管理的网点        
	 * @return
	 */
	public String lookUserDot(){
		List<UserDot> list = userDotService.getUserDotList(userDot);
		if(list != null){
			StringBuffer sb = new StringBuffer();
			for(int i=0;i<list.size();i++){
				if(i<list.size()-1){
					sb.append("'").append(list.get(i).getDotId()).append("',");
				}else{
					sb.append("'").append(list.get(i).getDotId()).append("'");
				}
				
			}
			branchDot.setIds(sb.toString());
			page = branchDotService.searchBranchDot(page, branchDot);
			user = userService.searchUserByID(userDot.getUserId());
			getRequest().setAttribute("user", user);
		}
		return SUCCESS;
	}
	
	
	
	/**删除用户管理的网点
	 * @return
	 */
	public String deleteUserDot(){
		try {
			userDotService.deleteUserDot(userDot,checkdel);
			result=Ajax.JSONResult(0, "删除成功！");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result=Ajax.JSONResult(1, "删除失败！");
		}
		return SUCCESS;
	}
	
	
	
/****页面控制方法结束***********************************************************************************/
	
	
	
/****封装Get,Set方法********************************************************************************************/
	public UserDotAction() {
		super();
		branchDot = new BranchDot();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}

	public BranchDotService getBranchDotService() {
		return branchDotService;
	}

	public void setBranchDotService(BranchDotService branchDotService) {
		this.branchDotService = branchDotService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public Page<BranchDot> getPage() {
		return page;
	}

	public void setPage(Page<BranchDot> page) {
		this.page = page;
	}

	public UserDot getUserDot() {
		return userDot;
	}

	public void setUserDot(UserDot userDot) {
		this.userDot = userDot;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public BranchDot getBranchDot() {
		return branchDot;
	}

	public void setBranchDot(BranchDot branchDot) {
		this.branchDot = branchDot;
	}


	public AreaService getAreaService() {
		return areaService;
	}


	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}


	public List<AdministrativeArea> getAreas() {
		List<AdministrativeArea> allAreas = areaService.searchAreaByCode(null);
		areas = allAreas;
		return areas;
	}


	public void setAreas(List<AdministrativeArea> areas) {
		this.areas = areas;
	}
	
	public String[] getCheckdel() {
		return checkdel;
	}

	public void setCheckdel(String[] checkdel) {
		this.checkdel = checkdel;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public UserDotService getUserDotService() {
		return userDotService;
	}


	public void setUserDotService(UserDotService userDotService) {
		this.userDotService = userDotService;
	}


	/**根据ID获取区域
	 * @param id
	 * @return
	 */
	public String getAreaName(String id){
		if(areas != null && areas.size() > 0){
			for (AdministrativeArea a : areas) {
				if(a.getId().equals(id)){
					return a.getName();
				}
			}
		}
		return "";
	}
	
}
