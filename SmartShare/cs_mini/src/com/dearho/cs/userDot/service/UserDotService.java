package com.dearho.cs.userDot.service;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.userDot.pojo.UserDot;

public interface UserDotService {

	/**根据条件获取用户网点关系
	 * @param userDot
	 * @param hql
	 * @return
	 */
	public List<UserDot> getUserDotList(UserDot userDot);
	
	/**添加用户网点关系
	 * @param userDot
	 */
	public void asveUserDot(UserDot userDot);
	
	/**更新用户网点关系
	 * @param userDot
	 */
	public void updateUserDot(UserDot userDot);
	
	/**删除用户网点关系
	 * @param userDot
	 */
	public void deleteUserDot(UserDot userDot,String[] checkdel);
	
	/**根据条件获取用户网点关系
	 * @param userDot
	 * @return
	 */
	public UserDot getUserDot(UserDot userDot);
	
}
