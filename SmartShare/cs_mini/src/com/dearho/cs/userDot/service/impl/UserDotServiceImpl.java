package com.dearho.cs.userDot.service.impl;

import java.util.List;

import com.dearho.cs.userDot.dao.UserDotDao;
import com.dearho.cs.userDot.pojo.UserDot;
import com.dearho.cs.userDot.service.UserDotService;

public class UserDotServiceImpl implements UserDotService {

	private UserDotDao userDotDao;
	
	public UserDotDao getUserDotDao() {
		return userDotDao;
	}

	public void setUserDotDao(UserDotDao userDotDao) {
		this.userDotDao = userDotDao;
	}

	@Override
	public List<UserDot> getUserDotList(UserDot userDot) {
		return userDotDao.getUserDotList(userDot);
	}

	@Override
	public void asveUserDot(UserDot userDot) {
		userDotDao.asveUserDot(userDot);
	}

	@Override
	public void updateUserDot(UserDot userDot) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteUserDot(UserDot userDot,String[] checkdel) {
		userDotDao.deleteUserDot(userDot,checkdel);
	}

	@Override
	public UserDot getUserDot(UserDot userDot) {
		// TODO Auto-generated method stub
		return null;
	}

}
