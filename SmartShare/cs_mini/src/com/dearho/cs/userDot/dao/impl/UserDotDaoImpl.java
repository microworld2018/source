package com.dearho.cs.userDot.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.userDot.dao.UserDotDao;
import com.dearho.cs.userDot.pojo.UserDot;

public class UserDotDaoImpl extends AbstractDaoSupport implements UserDotDao {
	
	@Override
	public List<UserDot> getUserDotList(UserDot userDot) {
		StringBuffer hql = new StringBuffer("select a.id from UserDot a where a.userId='").append(userDot.getUserId()).append("'");
		return getList(UserDot.class, queryFList(hql.toString()));
	}

	@Override
	public void asveUserDot(UserDot userDot) {
		addEntity(userDot);
	}

	@Override
	public void updateUserDot(UserDot userDot) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteUserDot(UserDot userDot,final String[] checkdel) {
		final String queryString="delete UserDot where userId = '"+userDot.getUserId()+"' and dotId in (:ids)";
//		deleteEntity(User.class, queryString, userDot.getDotIds());
		deleteEntity(UserDot.class, queryString, checkdel);
	}

	@Override
	public UserDot getUserDot(UserDot userDot) {
		// TODO Auto-generated method stub
		return null;
	}

}
