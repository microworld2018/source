package com.dearho.cs.appStart.dao.impl;

import java.util.List;

import com.dearho.cs.appStart.dao.AppStartDao;
import com.dearho.cs.appStart.pojo.AppStart;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.pojo.InsuranceOperationLog;
import com.dearho.cs.util.StringHelper;

public class AppStartDaoImpl extends AbstractDaoSupport implements AppStartDao {

	@Override
	public Page<AppStart> getAppStartPage(Page<AppStart> page, AppStart appStart) throws Exception {
		StringBuffer hql = new StringBuffer("select a.id from AppStart a ");
		hql.append(" where a.status != -1");
		
		if(StringHelper.isNotEmpty(appStart.getTitle())){
			hql.append(" and a.title like '%").append(appStart.getTitle().trim()).append("%'");
		}
		hql.append(" order by a.startTime desc");
		Page<AppStart> resultPage=pageCache(AppStart.class, page, hql.toString());
		resultPage.setResults(idToObj(AppStart.class, resultPage.getmResults()));
		return resultPage;
	}

	@Override
	public AppStart getById(String id) {
//		AppStart appStart = get(AppStart.class, id);
		return get(AppStart.class, id);
	}

	@Override
	public void updateAppStart(AppStart appStart) throws Exception {
		updateEntity(appStart);
	}

	@Override
	public void saveAppStart(AppStart appStart) throws Exception {
		addEntity(appStart);
	}

	@Override
	public List<AppStart> getByParam(int status) {
		String hql = "select a.id from AppStart a where a.status = '"+status+"'";
		return getList(AppStart.class, queryFList(hql));
	}

	@Override
	public List<AppStart> getAppStartList() {
		String hql = "select a.id from AppStart a where a.status != '-1' AND a.status != '2' ORDER BY a.startTime ASC";
		return getList(AppStart.class, queryFList(hql));
	}

}
