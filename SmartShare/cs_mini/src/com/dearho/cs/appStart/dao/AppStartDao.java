package com.dearho.cs.appStart.dao;

import java.util.List;

import com.dearho.cs.appStart.pojo.AppStart;
import com.dearho.cs.core.db.page.Page;

public interface AppStartDao {

	Page<AppStart> getAppStartPage(Page<AppStart> page, AppStart appStart) throws Exception;

	AppStart getById(String id);

	void updateAppStart(AppStart appStart) throws Exception;

	void saveAppStart(AppStart appStart) throws Exception;

	List<AppStart> getByParam(int status);

	List<AppStart> getAppStartList();

}
