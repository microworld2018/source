package com.dearho.cs.appStart.pojo;

import java.io.Serializable;
import java.util.Date;

/**
 * APP开屏实体类
 * 
 * @author Jin Guangyu
 *
 */
public class AppStart implements Serializable {

	private static final long serialVersionUID = 6787610139263004619L;

	private String id;

	private String title;

	private String startImg;

	private String content;

	private Date startTime;

	private Date endTime;

	private Integer status;// 开屏图的状态 0:待使用 1:正在使用 2:已结束 -1:已删除

	private String startTimeStr;// 开始时间字符串

	private String endTimeStr;// 结束时间字符串

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStartImg() {
		return startImg;
	}

	public void setStartImg(String startImg) {
		this.startImg = startImg;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getStartTimeStr() {
		return startTimeStr;
	}

	public void setStartTimeStr(String startTimeStr) {
		this.startTimeStr = startTimeStr;
	}

	public String getEndTimeStr() {
		return endTimeStr;
	}

	public void setEndTimeStr(String endTimeStr) {
		this.endTimeStr = endTimeStr;
	}

}
