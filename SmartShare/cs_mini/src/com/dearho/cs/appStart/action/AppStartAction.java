package com.dearho.cs.appStart.action;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.dearho.cs.appStart.pojo.AppStart;
import com.dearho.cs.appStart.service.AppStartService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.Constants;
import com.dearho.cs.util.ImageHelper;
import com.dearho.cs.util.StringHelper;
import com.opensymphony.webwork.ServletActionContext;

import net.sf.json.JSONObject;

public class AppStartAction extends AbstractAction {
	
	private static final Log logger = LogFactory.getLog(AppStartAction.class);
	
	private static final String uploadPath="upload/avatar/user/";
	
	private File[] startImg;
	
	private String startImgStr;

	private AppStartService appStartService;

	private AppStart appStart;

	private Page<AppStart> page = new Page<>();

	private String title;// 前台查询条件

	private String state;// 类型（新增或更新）

	private String id;// 开屏id

	@Override
	public String process() {
		try {
			page = appStartService.getAppStartPage(page, appStart);
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}

	/**
	 * 打开开屏新增或者编辑页面
	 * 
	 * @return
	 */
	public String getByIdAppStart() {
		try {
			if (state.equals("update")) {
				appStart = appStartService.getById(id);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**
	 * 确认保存开屏
	 * @return
	 */
	public String saveOrUpdateAppStart(){
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date now = new Date();
			if(StringHelper.isNotEmpty(appStart.getId())){
				//更新开屏
				AppStart appStartForUpdate = new AppStart();
				appStartForUpdate = appStartService.getById(appStart.getId());
				if(StringHelper.isNotEmpty(appStart.getStartTimeStr()) && StringHelper.isNotEmpty(appStart.getEndTimeStr())){
					Date startTime = sdf.parse(appStart.getStartTimeStr() + ":00");
					Date endTime = sdf.parse(appStart.getEndTimeStr() + ":00");
					//判断当前时间是否在传入时间内
					if(now.before(endTime) && now.after(startTime)){
						//首先结束其他正在使用的开屏
						List<AppStart> list = appStartService.getByParam(1);
						if(list != null && list.size() > 0){
							for (AppStart appStart : list) {
								appStart.setStatus(2);
								appStart.setEndTime(now);
								appStartService.updateAppStart(appStart);
							}
						}
						//在传入时间内，状态设置为正在使用
						appStartForUpdate.setStatus(1);
					}else{
						//不在传入时间内，状态设置为待使用
						appStartForUpdate.setStatus(0);
					}
					appStartForUpdate.setStartTime(startTime);
					appStartForUpdate.setEndTime(endTime);
				}else{
					//首先结束其他正在使用的开屏
					List<AppStart> list = appStartService.getByParam(1);
					if(list != null && list.size() > 0){
						for (AppStart appStart : list) {
							appStart.setStatus(2);
							appStart.setEndTime(now);
							appStartService.updateAppStart(appStart);
						}
					}
					//没有开始和结束时间，开始时间设置为当前时间，状态设为正在使用
					appStartForUpdate.setStartTime(now);
					appStartForUpdate.setStatus(1);
				}
				appStartForUpdate.setTitle(appStart.getTitle());
				appStartForUpdate.setStartImg(appStart.getStartImg());
				appStartForUpdate.setContent(appStart.getContent());
				appStartService.updateAppStart(appStartForUpdate);
				result=Ajax.JSONResult(0, "更新成功！");
			}else{
				//新建开屏
				if(StringHelper.isNotEmpty(appStart.getStartTimeStr()) && StringHelper.isNotEmpty(appStart.getEndTimeStr())){
					Date startTime = sdf.parse(appStart.getStartTimeStr() + ":00");
					Date endTime = sdf.parse(appStart.getEndTimeStr() + ":00");
					//判断当前时间是否在传入时间内
					if(now.before(endTime) && now.after(startTime)){
						//首先结束其他正在使用的开屏
						List<AppStart> list = appStartService.getByParam(1);
						if(list != null && list.size() > 0){
							for (AppStart appStart : list) {
								appStart.setStatus(2);
								appStart.setEndTime(now);
								appStartService.updateAppStart(appStart);
							}
						}
						//在传入时间内，状态设置为正在使用
						appStart.setStatus(1);
					}else{
						//不在传入时间内，状态设置为待使用
						appStart.setStatus(0);
					}
					appStart.setStartTime(startTime);
					appStart.setEndTime(endTime);
				}else{
					//首先结束其他正在使用的开屏
					List<AppStart> list = appStartService.getByParam(1);
					if(list != null && list.size() > 0){
						for (AppStart appStart : list) {
							appStart.setStatus(2);
							appStart.setEndTime(now);
							appStartService.updateAppStart(appStart);
						}
					}
					//没有开始和结束时间，把开始时间设置为当前时间，状态设为正在使用
					appStart.setStartTime(now);
					appStart.setStatus(1);
				}
				appStartService.saveAppStart(appStart);
				result=Ajax.JSONResult(0, "添加成功！");
			}
		} catch (Exception e) {
			e.printStackTrace();
			result=Ajax.JSONResult(1, "更新或者添加失败！");
		}
		
		return SUCCESS;
	}
	
	/**根据ID进行逻辑删除开屏
	 * @return
	 */
	public String delAppStart(){
		try {
			AppStart appStartForDel = appStartService.getById(appStart.getId());
			appStartForDel.setStatus(-1);
			appStartService.updateAppStart(appStartForDel);
			result=Ajax.JSONResult(0, "删除成功！");
		} catch (Exception e) {
			e.printStackTrace();
			result=Ajax.JSONResult(1, "删除失败！");
		}
		return SUCCESS;
	}
	
	/**
	 * 立即结束开屏
	 */
	public String overAppStart(){
		try {
			AppStart appStartForOver = appStartService.getById(appStart.getId());
			appStartForOver.setStatus(2);
			appStartForOver.setEndTime(new Date());
			appStartService.updateAppStart(appStartForOver);
			result=Ajax.JSONResult(0, "结束成功！");
		} catch (Exception e) {
			e.printStackTrace();
			result=Ajax.JSONResult(1, "结束失败！");
		}
		return SUCCESS;
	}
	
	/**
	 * 立即开始开屏
	 */
	public String startAppStart(){
		try {
			//首先结束其他正在使用的开屏
			List<AppStart> list = appStartService.getByParam(1);
			if(list != null && list.size() > 0){
				for (AppStart appStart : list) {
					appStart.setStatus(2);
					appStart.setEndTime(new Date());
					appStartService.updateAppStart(appStart);
				}
			}
			AppStart appStartForOver = appStartService.getById(appStart.getId());
			appStartForOver.setStatus(1);
			appStartForOver.setStartTime(new Date());
			appStartService.updateAppStart(appStartForOver);
			result=Ajax.JSONResult(0, "开始成功！");
		} catch (Exception e) {
			e.printStackTrace();
			result=Ajax.JSONResult(1, "开始失败！");
		}
		return SUCCESS;
	}
	
	/**
	 * 上传开屏图片
	 * @return
	 */
	public String uploadStartImg(){
		String canvasData = ServletActionContext.getRequest().getParameter("imageData");
		if(canvasData==null){
			result=new JSONObject().element("result", "").element("state", 500).element("message", "上传数据为空!").toString();
			return SUCCESS;
		}
		if(getSession().getAttribute(Constants.SESSION_USER)==null){
			result=new JSONObject().element("result", "").element("state", 500).element("message", "请重新登录!").toString();
			return SUCCESS;
		}
		try {
			String imgurl=ImageHelper.uploadForCanvasToService(canvasData, uploadPath, null,"-1");
			result=new JSONObject().element("result", imgurl).element("state", 200).element("message", "成功").toString();
		} catch (Exception e) {
			logger.error(e);
			result=new JSONObject().element("result", "").element("state", 500).element("message", "系统异常!").toString();
			return ERROR;
		}
		return SUCCESS;
	}

	public AppStartAction() {
		super();
		appStart = new AppStart();
		page.setCountField("a.id");
		page.setCurrentPage(1);
	}

	public AppStartService getAppStartService() {
		return appStartService;
	}

	public void setAppStartService(AppStartService appStartService) {
		this.appStartService = appStartService;
	}

	public AppStart getAppStart() {
		return appStart;
	}

	public void setAppStart(AppStart appStart) {
		this.appStart = appStart;
	}

	public Page<AppStart> getPage() {
		return page;
	}

	public void setPage(Page<AppStart> page) {
		this.page = page;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public File[] getStartImg() {
		return startImg;
	}

	public void setStartImg(File[] startImg) {
		this.startImg = startImg;
	}

	public String getStartImgStr() {
		return startImgStr;
	}

	public void setStartImgStr(String startImgStr) {
		this.startImgStr = startImgStr;
	}

}
