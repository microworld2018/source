package com.dearho.cs.appStart.service.impl;

import java.util.List;

import com.dearho.cs.appStart.dao.AppStartDao;
import com.dearho.cs.appStart.pojo.AppStart;
import com.dearho.cs.appStart.service.AppStartService;
import com.dearho.cs.core.db.page.Page;

public class AppStartServiceImpl implements AppStartService {

	private AppStartDao appStartDao;

	public AppStartDao getAppStartDao() {
		return appStartDao;
	}

	public void setAppStartDao(AppStartDao appStartDao) {
		this.appStartDao = appStartDao;
	}

	@Override
	public Page<AppStart> getAppStartPage(Page<AppStart> page, AppStart appStart) throws Exception {
		return appStartDao.getAppStartPage(page, appStart);
	}

	@Override
	public AppStart getById(String id) {
		return appStartDao.getById(id);
	}

	@Override
	public void updateAppStart(AppStart appStart) throws Exception {
		appStartDao.updateAppStart(appStart);
	}

	@Override
	public void saveAppStart(AppStart appStart) throws Exception {
		appStartDao.saveAppStart(appStart);
	}

	@Override
	public List<AppStart> getByParam(int status) throws Exception {
		return appStartDao.getByParam(status);
	}

	@Override
	public List<AppStart> getAppStartList() {
		return appStartDao.getAppStartList();
	}

}
