package com.dearho.cs.appStart.service;

import java.util.List;

import com.dearho.cs.appStart.pojo.AppStart;
import com.dearho.cs.core.db.page.Page;

public interface AppStartService {

	Page<AppStart> getAppStartPage(Page<AppStart> page, AppStart appStart) throws Exception;

	AppStart getById(String id);

	void updateAppStart(AppStart appStart) throws Exception;

	void saveAppStart(AppStart appStart) throws Exception;

	List<AppStart> getByParam(int status) throws Exception;

	List<AppStart> getAppStartList();

}
