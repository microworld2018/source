package com.dearho.cs.locus.service.impl;

import java.util.List;

import com.dearho.cs.locus.dao.CartLocusDao;
import com.dearho.cs.locus.pojo.CartLocus;
import com.dearho.cs.locus.service.CartLocusService;

public class CartLocusServiceuImpl implements CartLocusService{
	
	private CartLocusDao cartLocusDao;

	public void setCartLocusDao(CartLocusDao cartLocusDao) {
		this.cartLocusDao = cartLocusDao;
	}

	@Override
	public List<CartLocus> getCartLocus(String ids) {
		return cartLocusDao.getCartLocus(ids);
	}
	
	
}
