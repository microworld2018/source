package com.dearho.cs.locus.service;

import java.util.List;

import com.dearho.cs.locus.pojo.CartLocus;

/**轨迹Service层接口
 * @author Gaopl
 * @date 2016-09-22
 */
public interface CartLocusService {
	
	public List<CartLocus> getCartLocus(String ids);
}
