package com.dearho.cs.locus.service.impl;

import java.util.List;

import com.dearho.cs.locus.dao.LocusConditionDao;
import com.dearho.cs.locus.pojo.LocusCondition;
import com.dearho.cs.locus.service.LocusConditionService;

/**根据条件获取轨迹Service实现类
 * @author Gaopl
 * @date 2016-09-26
 */
public class LocusConditionServiceImpl implements LocusConditionService {
	
	private LocusConditionDao locusConditionDao;

	public void setLocusConditionDao(LocusConditionDao locusConditionDao) {
		this.locusConditionDao = locusConditionDao;
	}

	@Override
	public List<LocusCondition> getLocus(LocusCondition locus) {
		return locusConditionDao.getLocus(locus);
	}
	
	
}
