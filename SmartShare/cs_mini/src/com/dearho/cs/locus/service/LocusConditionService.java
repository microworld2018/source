package com.dearho.cs.locus.service;

import java.util.List;

import com.dearho.cs.locus.pojo.LocusCondition;

/**轨迹条件Service层接口
 * @author Gaopl
 * @date 2016-09-22
 */
public interface LocusConditionService {
	
	/**根据条件查询轨迹ID
	 * @param locus
	 * @return
	 */
	public List<LocusCondition> getLocus(LocusCondition locus);
}
