package com.dearho.cs.locus.pojo;

import java.io.Serializable;
import java.util.Date;

/**轨迹查询条件关联实体类
 * @author Gaopl
 * @data 2016-09-22
 */
public class LocusCondition implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**ID**/
	private String id;
	
	/**轨迹ID**/
	private String locusId;
	
	/**车辆ID**/
	private String cartId;
	
	/**会员ID**/
	private String memberId;
	
	/**上传时间**/
	private Date createDate;
	
	/**
	 * 订单ID
	 */
	private String orderId;
	
	
	/**条件参数暂存字段开始**/
	private String carNumber;
	
	private String carType;
	
	private String startTime;
	
	private String endTime;
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLocusId() {
		return locusId;
	}

	public void setLocusId(String locusId) {
		this.locusId = locusId;
	}

	public String getCartId() {
		return cartId;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	
}
