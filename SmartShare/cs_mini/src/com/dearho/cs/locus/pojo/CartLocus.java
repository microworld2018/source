package com.dearho.cs.locus.pojo;

import java.io.Serializable;
import java.util.Date;

/**车辆轨迹实体类
 * @author Gaopl
 * @data 2016-09-22
 */
public class CartLocus implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**轨迹主键**/
	private String id;
	
	/**纬度**/
	private Double lat;
	
	/**经度**/
	private Double lng;
	
	/**当前车速**/
	private Double currentSpeed;
	
	/**创建时间查询时用来排序**/
	private Date createrDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getLat() {
		return lat;
	}

	public void setLat(Double lat) {
		this.lat = lat;
	}

	public Double getLng() {
		return lng;
	}

	public void setLng(Double lng) {
		this.lng = lng;
	}

	public Double getCurrentSpeed() {
		return currentSpeed;
	}

	public void setCurrentSpeed(Double currentSpeed) {
		this.currentSpeed = currentSpeed;
	}

	public Date getCreaterDate() {
		return createrDate;
	}

	public void setCreaterDate(Date createrDate) {
		this.createrDate = createrDate;
	}
	
}
