package com.dearho.cs.locus.action;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.device.pojo.DeviceBinding;
import com.dearho.cs.device.service.DeviceBindingService;
import com.dearho.cs.groundOrders.pojo.GroundOrder;
import com.dearho.cs.groundOrders.service.GroundOrdersService;
import com.dearho.cs.locus.pojo.CartLocus;
import com.dearho.cs.locus.pojo.LocusCondition;
import com.dearho.cs.locus.service.CartLocusService;
import com.dearho.cs.locus.service.LocusConditionService;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.pojo.OrdersDetail;
import com.dearho.cs.orders.service.OrdersDetailService;
import com.dearho.cs.orders.service.OrdersService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.HttpRequestUtil;
import com.dearho.cs.util.PropertiesHelper;
import com.dearho.cs.util.ToolDateTime;

/**根据条件获取轨迹
 * @author Gaopl
 * @date 2016-09-22
 */
public class LocusConditionGetAction extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	private LocusConditionService locusConditionService;//轨迹条件Service
	
	private CartLocusService cartLocusService;//获取轨迹坐标Service
	
	private CarService carService;//车辆Service
	
	private String carNumber;//车牌号或者车机号
	
	private String carType;//选择类型
	
	private String startTime;//开始时间
	
	private String endTime;//结束时间
	
	private String orderId;
	
	private String groundOrderId;
	
	private DeviceBindingService deviceBindingService;
	
	private OrdersService ordersService;
	
	private OrdersDetailService ordersDetailService;
	
	private GroundOrdersService groundOrdersService;
	

	public LocusConditionService getLocusConditionService() {
		return locusConditionService;
	}

	public void setCartLocusService(CartLocusService cartLocusService) {
		this.cartLocusService = cartLocusService;
	}

	public void setLocusConditionService(LocusConditionService locusConditionService) {
		this.locusConditionService = locusConditionService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public DeviceBindingService getDeviceBindingService() {
		return deviceBindingService;
	}

	public void setDeviceBindingService(DeviceBindingService deviceBindingService) {
		this.deviceBindingService = deviceBindingService;
	}

	public CartLocusService getCartLocusService() {
		return cartLocusService;
	}

	public CarService getCarService() {
		return carService;
	}
	
	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String carType) {
		this.carType = carType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getGroundOrderId() {
		return groundOrderId;
	}

	public void setGroundOrderId(String groundOrderId) {
		this.groundOrderId = groundOrderId;
	}

	public OrdersService getOrdersService() {
		return ordersService;
	}
	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}

	public OrdersDetailService getOrdersDetailService() {
		return ordersDetailService;
	}

	public void setOrdersDetailService(OrdersDetailService ordersDetailService) {
		this.ordersDetailService = ordersDetailService;
	}

	public GroundOrdersService getGroundOrdersService() {
		return groundOrdersService;
	}

	public void setGroundOrdersService(GroundOrdersService groundOrdersService) {
		this.groundOrdersService = groundOrdersService;
	}

	@Override
	public String process() {
		Car car = new Car();
		//判断车牌号还是车机号
		if(carType.equals("0")){
			car.setVehiclePlateId(carNumber);
		}else if(carType.equals("1")){
			car.setEngineNumber(carNumber);
		}
//		根据条件获取车辆信息
		List<Car> carList = carService.queryCars(car);
		if(carList == null){
			result = Ajax.JSONResult(1, "没有改车辆信息！");
			return SUCCESS;
		}
		//获取车辆SN号
		List<DeviceBinding> list = deviceBindingService.queryBindByCarId(carList.get(0).getId(), null);
		
		String url = PropertiesHelper.getValue("msg_url")+"/car/getDrivingTrack";
		Map<String,String> paramMap = new HashMap<String, String>();
		paramMap.put("sn", list.get(0).getDeviceNo());//设备编号
		paramMap.put("startTime", startTime);
		paramMap.put("endTime",endTime);
		String content = HttpRequestUtil.getPostMethod(url, paramMap);
		JSONObject json = JSONObject.fromObject(content);
		JSONArray jsonarray = (JSONArray) json.get("data");
		JSONArray longlatArray = new JSONArray();
		if(jsonarray == null){
			result = Ajax.JSONResult(1, "没有搜索到你想要的数据！");
		}else{
			
			for(int i = 0; i < jsonarray.size();i++){
				JSONObject obj = (JSONObject)jsonarray.get(i);
				String apiContents = HttpRequestUtil.getPostMethod("http://api.map.baidu.com/geoconv/v1/?coords="+obj.getString("longitude")+","+obj.getString("latitude")+"&from=1&to=5&output=json&ak=BtxULUWSmvG50D5GKe0ka9Yk",null);
				JSONObject appObj = JSONObject.fromObject(apiContents);
				
				JSONArray ary =JSONArray.fromObject(appObj.getString("result"));
				
				if(ary != null && ary.size() > 0){
					for(int j  = 0; j<ary.size();j++){
						JSONObject o = (JSONObject)ary.get(j);
						
						obj.put("longitude", o.getString("x"));
						obj.put("latitude", o.getString("y"));
						
					}
				}
				longlatArray.add(obj);
			}
		}
		
		result=Ajax.JSONResult(0, "查询成功",longlatArray);
		
		
		
//		Car car = new Car();
//		/*判断是车牌号还是车机号*/
//		if(carType.equals("0")){
//			car.setVehiclePlateId(carNumber);
//		}else if(carType.equals("1")){
//			car.setEngineNumber(carNumber);
//		}
//		//根据条件获取车辆信息
//		List<Car> carList = carService.queryCars(car);
//		/*获取经纬度ID开始*/
//		LocusCondition locus = new LocusCondition();
//		if(carList != null){
//			locus.setCartId(carList.get(0).getId());
//			locus.setStartTime(startTime);
//			locus.setEndTime(endTime);
//			List<LocusCondition> locusList = locusConditionService.getLocus(locus);
//			/*获取经纬度ID结束*/
//			if(locusList.size() > 0){
//				StringBuffer idsArrStr = new StringBuffer();
//				for (LocusCondition locusCon : locusList) {
//					idsArrStr.append("'"+locusCon.getLocusId()+"'" + ",");
//				}
//				idsArrStr.deleteCharAt(idsArrStr.length() - 1);
//				//获取经纬度
//				List<CartLocus> cartLocus = cartLocusService.getCartLocus(idsArrStr.toString());
//				//转换cartLocus为JSON
//				JSONArray jsonarray = JSONArray.fromObject(cartLocus); 
//				result=Ajax.JSONResult(0, "查询成功",jsonarray);
//			}else{
//				result = Ajax.JSONResult(1, "没有搜索到你想要的数据！");
//			}
//		}else{
//			result = Ajax.JSONResult(1, "没有搜索到你想要的数据！");
//		}
		
		return SUCCESS;
	}

	

	public String lookCarLocus() {

		/*获取经纬度ID开始*/
		LocusCondition locus = new LocusCondition();
		if(orderId != null  || orderId != ""){
			locus.setOrderId(orderId);
			List<LocusCondition> locusList = locusConditionService.getLocus(locus);
			/*获取经纬度ID结束*/
			if(locusList != null && locusList.size() > 0){
				StringBuffer idsArrStr = new StringBuffer();
				for (LocusCondition locusCon : locusList) {
					idsArrStr.append("'"+locusCon.getLocusId()+"'" + ",");
				}
				idsArrStr.deleteCharAt(idsArrStr.length() - 1);
				//获取经纬度
				List<CartLocus> cartLocus = cartLocusService.getCartLocus(idsArrStr.toString());
				//转换cartLocus为JSON
				JSONArray jsonarray = JSONArray.fromObject(cartLocus); 
				result=Ajax.JSONResult(0, "查询成功",jsonarray);
			}else{
				result = Ajax.JSONResult(1, "当前订单没有数据！");
			}
		}else{
			result = Ajax.JSONResult(1, "没有搜索到你想要的数据！");
		}
		
		return SUCCESS;
	}
	
	
	
	public String openOrderCarLocus(){
		GroundOrder groundOrder = groundOrdersService.queryGroundOrderById(groundOrderId);
		if(groundOrder != null){
			Orders o = ordersService.queryOrdersByOrderNo(groundOrder.getOrderNo());
			orderId = o.getId();
		}
		getRequest().setAttribute("orderId", orderId);
		return SUCCESS;
	}
	
	
	/**新版查看订单轨迹
	 * @return
	 */
	public String viewOrderTrajectory(){
		GroundOrder groundOrder = groundOrdersService.queryGroundOrderById(groundOrderId);
		if(groundOrder != null){
			Orders o = ordersService.queryOrdersByOrderNo(groundOrder.getOrderNo());
			orderId = o.getId();
		}
		
		//获取订单基本信息
		Orders orders = ordersService.queryOrdersById(orderId);
		OrdersDetail orderDetail = new OrdersDetail();
		orderDetail.setOrdersId(orderId);
		OrdersDetail ordersDetail = ordersDetailService.getByOrderDetail(orderDetail);
		
		//获取车辆SN号
		List<DeviceBinding> list = deviceBindingService.queryBindByCarId(orders.getCarId(), null);
		
		String url = PropertiesHelper.getValue("msg_url")+"/car/getDrivingTrack";
		Map<String,String> paramMap = new HashMap<String, String>();
		paramMap.put("sn", list.get(0).getDeviceNo());//设备编号
		if(orders.getBeginTime()!= null && !"".equals(orders.getBeginTime())){
			paramMap.put("startTime", ToolDateTime.format(orders.getBeginTime(), ToolDateTime.pattern_ymd_hms));
		}else{
			paramMap.put("startTime", ToolDateTime.format(new Date(), ToolDateTime.pattern_ymd_hms));
		}
		paramMap.put("endTime", ToolDateTime.format(orders.getEndTime(), ToolDateTime.pattern_ymd_hms));
		String content = HttpRequestUtil.getPostMethod(url, paramMap);
//		String content = "{\"result\":true,\"message\":\"查询成功\",\"data\":[{\"sn\":\"250004392\",\"longitude\":\"116.9323\",\"latitude\":\"38.82028\",\"speed\":\"0\",\"direction\":\"0\"}]}";
		JSONObject json = JSONObject.fromObject(content);
		List li = (List) json.get("data");
		
		List returnList = new LinkedList();
		if(li == null){
			result = Ajax.JSONResult(1, "没有搜索到你想要的数据！");
		}else{
			for(int i = 0; i <li.size(); i++){
				JSONObject obj = (JSONObject)li.get(i);
				String apiContents = HttpRequestUtil.getPostMethod("http://api.map.baidu.com/geoconv/v1/?coords="+obj.getString("longitude")+","+obj.getString("latitude")+"&from=1&to=5&output=json&ak=BtxULUWSmvG50D5GKe0ka9Yk",null);
				JSONObject appObj = JSONObject.fromObject(apiContents);
				
				JSONArray ary =JSONArray.fromObject(appObj.getString("result"));
				
				if(ary != null && ary.size() > 0){
					for(int j  = 0; j<ary.size();j++){
						JSONObject o = (JSONObject)ary.get(j);
						
						obj.put("longitude", o.getString("x"));
						obj.put("latitude", o.getString("y"));
						
					}
				}
				returnList.add(obj);
			}
			
			result=Ajax.JSONResult(0, "查询成功",returnList);
		}
		return SUCCESS;
	}
}
