package com.dearho.cs.locus.action;

import com.dearho.cs.locus.service.CartLocusService;
import com.dearho.cs.sys.action.AbstractAction;

/**获取轨迹Action
 * @author Gaopl
 * @date 2016-09-22
 */
public class CartLocusGetAction extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	private CartLocusService cartLocusService;

	@Override
	public String process() {
		
		return SUCCESS;
	}

	public CartLocusService getCartLocusService() {
		return cartLocusService;
	}

	public void setCartLocusService(CartLocusService cartLocusService) {
		this.cartLocusService = cartLocusService;
	}

	
}
