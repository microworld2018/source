package com.dearho.cs.locus.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.locus.dao.CartLocusDao;
import com.dearho.cs.locus.pojo.CartLocus;
import com.dearho.cs.locus.pojo.LocusCondition;
import com.dearho.cs.resmonitor.entity.CarInfoEntiry;

/**轨迹Dao层接口实现类
 * @author Gaopl
 * @date 2016-09-26
 */
public class CartLocusDaoImpl extends AbstractDaoSupport implements CartLocusDao {

	@Override
	public void addCartLocus(CartLocus cartLocus) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<CartLocus> getCartLocus(String ids) {
		/*String hql = null;
		if(ids.size() > 0){
			hql="SELECT lat,lng,currentSpeed FROM CartLocus WHERE ";
			StringBuffer idsArrStr = new StringBuffer();
			for (String id : ids) {
				idsArrStr.append("'"+id+"'" + ",");
			}
			idsArrStr.deleteCharAt(idsArrStr.length() - 1);
			hql += "c.id in (" + idsArrStr.toString() +")";
			return search(CartLocus.class,hql);
		}
		return null;*/
		/*String hql="SELECT lat,lng,currentSpeed FROM CartLocus WHERE id IN (1,2,3)";*/
		StringBuffer hql = new StringBuffer();
		hql.append("SELECT lat,lng,currentSpeed FROM CartLocus WHERE id IN ("+ids+")");
		hql.append(" order by createrDate asc");
		return search(CartLocus.class,hql.toString());
	}

}
