package com.dearho.cs.locus.dao;

import java.util.List;

import com.dearho.cs.locus.pojo.CartLocus;

/**轨迹Dao层接口
 * @author Gaopl
 * @date 2016-09-22
 */
public interface CartLocusDao {

	/**添加轨迹
	 * @param cartLocus 轨迹实体类
	 */
	public void addCartLocus(CartLocus cartLocus);
	
	/**根据ID集合获取轨迹
	 * @param ids ID集合
	 * @return list
	 */
	public List<CartLocus> getCartLocus(String ids);
}
