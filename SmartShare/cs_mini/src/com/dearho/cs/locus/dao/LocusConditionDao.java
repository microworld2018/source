package com.dearho.cs.locus.dao;

import java.util.List;

import com.dearho.cs.locus.pojo.LocusCondition;

/**轨迹条件Dao层接口
 * @author Gaopl
 * @data 2016-09-22
 */
public interface LocusConditionDao {

	/**根据条件获取轨迹ID
	 * @param locus
	 * @return
	 */
	public List<LocusCondition> getLocus(LocusCondition locus);
}
