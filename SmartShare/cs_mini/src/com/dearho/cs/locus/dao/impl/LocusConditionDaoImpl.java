package com.dearho.cs.locus.dao.impl;

import java.util.List;



import org.apache.cxf.common.util.StringUtils;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.locus.dao.LocusConditionDao;
import com.dearho.cs.locus.pojo.LocusCondition;

public class LocusConditionDaoImpl extends AbstractDaoSupport implements LocusConditionDao {

	@Override
	public List<LocusCondition> getLocus(LocusCondition locus) {
		StringBuffer hql = new StringBuffer();
		hql.append("SELECT locusId FROM LocusCondition WHERE 1=1");
		
		if(locus.getCartId() != null && !"".equals(locus.getCartId())){
			hql.append("and cartId ="+locus.getCartId());
		}
		
		if(locus.getStartTime() != null && locus.getEndTime() != null){
			hql.append("and  createDate >="+locus.getStartTime());
			hql.append("and  createDate <="+locus.getEndTime());
		}
		
		if(locus.getMemberId() != null){
			hql.append("and  memberId ="+locus.getMemberId());
		}
		
		if(locus.getOrderId() != null){
			hql.append(" and  orderId ='"+locus.getOrderId()+"'");
		}else{
			return null;
		}
		
		return getList(LocusCondition.class,queryFList(hql.toString()));
	}

}
