package com.dearho.cs.car.action;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.HttpRequestUtil;
import com.dearho.cs.util.PropertiesHelper;

public class CarsStatusAction extends AbstractAction{
	private static final long serialVersionUID = 1L;
	
	private String sn;
	
	private String type;
	
	public CarsStatusAction() {
		super();
	}

	@Override
	public String process() {
		
		Map<String,String> paramMap = new HashMap<String, String>();
		
		paramMap.put("sn", sn);//设备编号
		
		if(type.equals("carStatus")){//获取车辆状态
			
			String msgUrl = PropertiesHelper.getValue("msg_url")+"/car/getStatus";
			
			String content = HttpRequestUtil.getPostMethod(msgUrl, paramMap);
			
			JSONObject obj  = JSONObject.fromObject(content);
			
			JSONArray jsonarray = JSONArray.fromObject(obj.get("data"));
			
			if(obj.getString("result").equals("true")){
				result = Ajax.JSONResult(1, obj.getString("result"),jsonarray);
			}else{
				result = Ajax.JSONResult(0, obj.getString("result"),jsonarray);
			}
			
		}else if(type.equals("findCar") 	|| //远程寻车
				type.equals("openLockPower")||//开中控锁并恢复供电
				type.equals("lockPowerOff") ||//关中控锁并断电
				type.equals("restorePower")	||//远程恢复供电
				type.equals("powerFailure")	||//远程断电
				type.equals("closeDoor")	||//关中控锁
				type.equals("openDoor")){//开中控锁
			String msgUrl = returnContent();
			
			String content = HttpRequestUtil.getPostMethod(msgUrl, paramMap);
			
			JSONObject obj  = JSONObject.fromObject(content);
			
			JSONObject josnObject = JSONObject.fromObject(obj.getString("result"));
			
			String re = josnObject.getString("result");
			
			if(re == null || re.isEmpty()){
				result = Ajax.JSONResult(Car.CONTROL_CAR_CALL_ERROR, "寻车失败",null);
			}else{
				if(re.equals(Car.CONTROL_CAR_NOT_ONLINE+"")){
					result = Ajax.JSONResult(Car.CONTROL_CAR_NOT_ONLINE, "设备不在线",null);
				}else if(re.equals(Car.CONTROL_CAR_SUCCESS+"")){
					result = Ajax.JSONResult(Car.CONTROL_CAR_SUCCESS, "调用成功",null);
				}else if(re.equals(Car.CONTROL_CAR_TIME_OUT+"")){
					result = Ajax.JSONResult(Car.CONTROL_CAR_TIME_OUT, "调用超时",null);
				}else if(re.equals(Car.CONTROL_CAR_BUSY+"")){
					result = Ajax.JSONResult(Car.CONTROL_CAR_BUSY, "远程控制总线忙",null);
				}else if(re.equals(Car.CONTROL_CAR_NOT_CLOSE_DOOR+"")){
					result = Ajax.JSONResult(Car.CONTROL_CAR_NOT_CLOSE_DOOR, "车门或发动机未关闭",null);
				}else{
					result = Ajax.JSONResult(Car.CONTROL_CAR_CALL_ERROR, "寻车失败",null);
				}
			}
		}else if(type.equals("gpsInfo")){//获取车辆gps数据
			String msgUrl = returnContent();
			
			String content = HttpRequestUtil.getPostMethod(msgUrl, paramMap);
			
			JSONObject obj  = JSONObject.fromObject(content);
			
			JSONObject josnObject = JSONObject.fromObject(obj.getString("result"));
			
			String re = josnObject.getString("result");
		}
		
		return SUCCESS;
	}
	
	public String returnContent(){
		
		String url = "";
		
		if(type.equals("findCar")){//远程寻车
			url = PropertiesHelper.getValue("msg_url")+"/car/findCar";
		}else if(type.equals("openLockPower")){//开中控锁并恢复供电
			url = PropertiesHelper.getValue("msg_url")+"/car/openLockPower";
		}else if(type.equals("lockPowerOff")){//关中控锁并断电
			url = PropertiesHelper.getValue("msg_url")+"/car/lockPowerOff";
		}else if(type.equals("restorePower")){//远程恢复供电
			url = PropertiesHelper.getValue("msg_url")+"/car/restorePower";
		}else if(type.equals("powerFailure")){//远程断电
			url = PropertiesHelper.getValue("msg_url")+"/car/powerFailure";
		}else if(type.equals("closeDoor")){//关中控锁
			url = PropertiesHelper.getValue("msg_url")+"/car/closeDoor";
		}else if(type.equals("openDoor")){//开中控锁
			url = PropertiesHelper.getValue("msg_url")+"/car/openDoor";
		}else if(type.equals("gpsInfo")){//gps数据
			url = PropertiesHelper.getValue("msg_url")+"/car/getGPSInfo";
		}
		
		return url;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
