package com.dearho.cs.car.action;

import java.util.List;
import com.dearho.cs.device.pojo.DeviceBinding;
import com.dearho.cs.device.service.DeviceBindingService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.StringHelper;

public class CarControlAction extends AbstractAction{
	private static final long serialVersionUID = 1L;

	private String id;

	private String state;
	
	private DeviceBindingService deviceBindingService;
	
	private DeviceBinding  deviceBinding;

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public CarControlAction() {
		super();
		deviceBinding = new DeviceBinding();
	}

	public DeviceBindingService getDeviceBindingService() {
		return deviceBindingService;
	}

	public void setDeviceBindingService(DeviceBindingService deviceBindingService) {
		this.deviceBindingService = deviceBindingService;
	}
	
	public DeviceBinding getDeviceBinding() {
		return deviceBinding;
	}

	public void setDeviceBinding(DeviceBinding deviceBinding) {
		this.deviceBinding = deviceBinding;
	}

	@Override
	public String process() {
		
		Integer type = 1;//查询状态绑定的设备SN
		
		if(StringHelper.isNotEmpty(id)){
			
			List<DeviceBinding> bindingList = deviceBindingService.queryBindByCarId(id, type);
		
			if(bindingList != null && bindingList.size() > 0){

				deviceBinding = deviceBindingService.queryBindByCarId(id, type).get(0);
				
			}else{
				
				result=Ajax.JSONResult(0, "未绑定设备",null);
				
				return SUCCESS;
			}
		}
		
		return state;
		
		
		
	}
}
