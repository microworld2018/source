package com.dearho.cs.util;


import java.security.Key;
import java.security.SecureRandom;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
/**
 * Created by IntelliJ IDEA.
 * Date: 2008-6-18 11:48:56
 * 身份证验证加密解密工具类
 */
public class Des2
{
    public static final String ALGORITHM_DES = "DES/CBC/PKCS5Padding";
    private static Log log = LogFactory.getLog(Des2.class);
    
    /**
     * 身份证验证WebService接口
     */
    public static final String IDCard_ADDRESS = "http://gboss.id5.cn/services/QueryValidatorServices?wsdl";
    /**
     * WebService账号
     */
    public static final String USERNAME = "tjltservice";
    /**
     * WebService密码
     */
    public static final String PASSWORD = "tjltservice_-xSc609C";
    /**
     * 密钥(加密解密需要)
     */
    public static final String KEY = "12345678";
    /**
     * 产品编号身份信息审核
     */
    public static final String DATASOURCE = "1A020201";
    /**
     * 产品编号不良信息查询
     */
    public static final String DATASOURCE1 = "3X010101";
    /**
     * 是否进行加密
     */
    public static final boolean IFJIAMI = true;

    /**
     * DES算法，加密
     *
     * @param data 待加密字符串
     * @param key  加密私钥，长度不能够小于8位
     * @return 加密后的字节数组，一般结合Base64编码使用
     * @throws CryptException 异常
     */
    public static String encode(String key,String data) throws Exception
    {
        return encode(key, data.getBytes("GB18030"));
        //return encode(key, new String(data.getBytes(),"GB18030").getBytes());
    }
    /**
     * DES算法，加密
     *
     * @param data 待加密字符串
     * @param key  加密私钥，长度不能够小于8位
     * @return 加密后的字节数组，一般结合Base64编码使用
     * @throws CryptException 异常
     */
    public static String encode(String key,byte[] data) throws Exception
    {
        try
        {
	    	DESKeySpec dks = new DESKeySpec(key.getBytes());
	    	
	    	SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            //key的长度不能够小于8位字节
            Key secretKey = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance(ALGORITHM_DES);
            IvParameterSpec iv = new IvParameterSpec("12345678".getBytes());
            AlgorithmParameterSpec paramSpec = iv;
            cipher.init(Cipher.ENCRYPT_MODE, secretKey,paramSpec);
            
            byte[] bytes = cipher.doFinal(data);
            return Base64.encode(bytes);
        } catch (Exception e)
        {
            throw new Exception(e);
        }
    }

    /**
     * DES算法，解密
     *
     * @param data 待解密字符串
     * @param key  解密私钥，长度不能够小于8位
     * @return 解密后的字节数组
     * @throws Exception 异常
     */
    public static byte[] decode(String key,byte[] data) throws Exception
    {
        try
        {
        	SecureRandom sr = new SecureRandom();
	    	DESKeySpec dks = new DESKeySpec(key.getBytes());
	    	SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            //key的长度不能够小于8位字节
            Key secretKey = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance(ALGORITHM_DES);
            IvParameterSpec iv = new IvParameterSpec("12345678".getBytes());
            AlgorithmParameterSpec paramSpec = iv;
            cipher.init(Cipher.DECRYPT_MODE, secretKey,paramSpec);
            return cipher.doFinal(data);
        } catch (Exception e)
        {
            throw new Exception(e);
        }
    }
    
    /**
     * 获取编码后的值
     * @param key
     * @param data
     * @return
     * @throws Exception
     */
    public static String decodeValue(String key,String data) 
    {
    	byte[] datas;
    	String value = null;
		try {
			if(System.getProperty("os.name") != null && (System.getProperty("os.name").equalsIgnoreCase("sunos") || System.getProperty("os.name").equalsIgnoreCase("linux")))
	        {
	    		log.debug("os.name(true)=" + System.getProperty("os.name"));
	    		datas = decode(key, Base64.decode(data));
	    		log.debug("ddd=" + new String(datas));
	        }
	    	else
	    	{
	    		log.debug("os.name(false)=" + System.getProperty("os.name"));
	    		datas = decode(key, Base64.decode(data));
	    		log.debug("ddd=" + new String(datas,"GB18030"));
	    	}
			
			value = new String(datas,"GB18030");
		} catch (Exception e) {
			e.printStackTrace();
			log.warn("解密失败");
			value = "";
		}
    	return value;
    }

    public static void main(String[] args) throws Exception
    {
      
      System.out.println("明：abc ；密：" + Des2.encode("abcd123423456","abc"));
  	  //System.out.println("明：ABC ；密：" + Des2.encode("12345678","ABC"));
  	  //System.out.println("明：中国人 ；密：" + Des2.encode("12345678","中国人"));
  	 // System.out.println("明：abc123木头人 ；密：" + Des2.encode("12345678","caishenbaojiekou"));
  	//  System.out.println("g3ME4dIH/5dpd1M mHqZ2eWk8pxuq8V5=" + Des2.decodeValue("88888888", "g3ME4dIH/5dpd1MmHqZ2eWk8pxuq8V5"));
//  	  System.out.println("ss=" + Des2.decodeValue("88888888", "rc+uwHsk8I4SGFElQ4NMQbZgdrPwMSNrfv0Pt5RQ15/2q+i7jd6JFOgahlPhhGDuohv5MS7K8UZiVxP7Zp796Y0FQPJu58wx5LhlHUEZCUXcHAKnjX0EsQSrpfmIF/JZxMfcGCNWCABDTRQG8oDs2CMYGI/g2cmL9nU6EpG4RGH9zhx9Gsovv/mnxKKqLI9fySUfAaMxReEZoUWpEFPKk7xvV7mxQsUNNaaCekCt0D+FP65bQBweZA=="));
  	 
  	 //System.out.println("明：在那遥远的地方 ；密：" + Des2.encode("12345678","9YR6ZPdZufM="));
  	  //System.out.println(new String(Des2.decode("wwwid5cn", "ABB0E340805D367C438E24FC4005C121F60247F6EE4430B5".toLowerCase().getBytes())));
  	  //System.out.println("dd=" + Des2.encode("12345678", new String("idtagpckhd")));
//  	 System.out.println("dd=" + Des2.encode("88888888", new String("周慧".getBytes("GB18030"),"GB18030")));
  	//System.out.println("dd=" + Des2.decodeValue("12345678", "S078JKKtSvAMWap/sIGpd4eiphXoB0MB8EfFuFogsz8RLhrqnYxuAI8jchhX2zrYlCcVJcbeIfkwP7h6A+13QRIPDjUZPyuGqJN1Hfgu6zla5sJ7OIu+XqHVkHREHfz8GACNdIpgJzVSUC2zPYI1inB/fcB74Pu0njF6Eo36IUzcNCfgLcsikj8OY3+iNKxcnprPdiKCh3eeV4Tc3NF1GGBKSMj5L63y"));
  	System.out.println("dd=" + Des2.decodeValue("abcd1234abcd1234abcd1234", "GHsC28U/830="));
    }
}

