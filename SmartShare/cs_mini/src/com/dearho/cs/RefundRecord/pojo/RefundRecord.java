package com.dearho.cs.RefundRecord.pojo;

import java.util.Date;

import javax.persistence.Column;

import com.dearho.cs.Deposit.pojo.AccDeposit;
import com.dearho.cs.subscriber.pojo.Subscriber;

/**会员退款申请实体类
 * @author Gaopl
 *
 */
public class RefundRecord {

	private String id;
	private String subId;
	private Integer type;
	private Integer status;
	private String name;
	@Column(name = "[DESC]", nullable = false)
	private String desc;
	private Double money;
	private Double actualMoney;
	private String depositTime;
	private Date createTime;
	private String tradeOrderNo;
	private String tradeOrderNo2;
	private String rechargeTradeNo;	
	private Date refundTime;
	private String payType;
	private Integer editable;
	private Integer immediately;
	private Integer hasAbnormal;
	
	public Integer getHasAbnormal() {
		return hasAbnormal;
	}
	public void setHasAbnormal(Integer hasAbnormal) {
		this.hasAbnormal = hasAbnormal;
	}
	public Integer getImmediately() {
		return immediately;
	}
	public void setImmediately(Integer immediately) {
		this.immediately = immediately;
	}
	public String getRechargeTradeNo() {
		return rechargeTradeNo;
	}
	public void setRechargeTradeNo(String rechargeTradeNo) {
		this.rechargeTradeNo = rechargeTradeNo;
	}	
	
	public String getDepositTime() {
		return depositTime;
	}
	public void setDepositTime(String depositTime) {
		this.depositTime = depositTime;
	}
	
	public String getTradeOrderNo2() {
		return tradeOrderNo2;
	}
	public void setTradeOrderNo2(String tradeOrderNo2) {
		this.tradeOrderNo2 = tradeOrderNo2;
	}
	
	public Integer getEditable() {
		return editable;
	}
	public void setEditable(Integer editable) {
		this.editable = editable;
	}
	public String getPayType() {
		return payType;
	}
	public void setPayType(String payType) {
		this.payType = payType;
	}
	
	private Subscriber subscriber;
	
	private AccDeposit accDeposit;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSubId() {
		return subId;
	}
	public void setSubId(String subId) {
		this.subId = subId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public Double getActualMoney() {
		return actualMoney;
	}
	public void setActualMoney(Double actualMoney) {
		this.actualMoney = actualMoney;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	public Subscriber getSubscriber() {
		return subscriber;
	}
	public void setSubscriber(Subscriber subscriber) {
		this.subscriber = subscriber;
	}
	public AccDeposit getAccDeposit() {
		return accDeposit;
	}
	public void setAccDeposit(AccDeposit accDeposit) {
		this.accDeposit = accDeposit;
	}
	public String getTradeOrderNo() {
		return tradeOrderNo;
	}
	public void setTradeOrderNo(String tradeOrderNo) {
		this.tradeOrderNo = tradeOrderNo;
	}
	public Date getRefundTime() {
		return refundTime;
	}
	public void setRefundTime(Date refundTime) {
		this.refundTime = refundTime;
	}
	
}
