package com.dearho.cs.RefundRecord.pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

/**会员押金记录表
 * @author Gaopl
 *
 */
public class DubDepositRecord implements Serializable{

	private static final long serialVersionUID = 1L;

	private String id;
	
	private String subId;//会员id
	
	private Integer type;//0:充值   2:退款
	
	private double money;//充值金额
	
	@Column(name = "[DESC]", nullable = false)
	private String desc;
	
	private String tradeOrderNo;//充值流水号
	
	private Integer payStatus;
	
	private String creatorId;
	
	private Date createTime;//创建记录的时间
	
	private String rechargeTimeStr;//充值时间
	
	private double refundMoney;//退款金额
	
	private String refundTimeStr;//退款时间
	
	private String refundTradeNo;//退款流水号
	
	private Integer refundStatus;//退款状态
	
	//临时字段 用户名称
	private String subName;
	
	//临时字段 用户手机号
	private String subPhoneNo;
	
	//临时字段 退款记录表id
	private String refundRecordId;
	
	//临时字段 用户充值方式
	private Integer payType;
	
	//临时字段 是否操作过立即退款
	private Integer immediately;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getTradeOrderNo() {
		return tradeOrderNo;
	}

	public void setTradeOrderNo(String tradeOrderNo) {
		this.tradeOrderNo = tradeOrderNo;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getRechargeTimeStr() {
		return rechargeTimeStr;
	}

	public void setRechargeTimeStr(String rechargeTimeStr) {
		this.rechargeTimeStr = rechargeTimeStr;
	}

	public double getRefundMoney() {
		return refundMoney;
	}

	public void setRefundMoney(double refundMoney) {
		this.refundMoney = refundMoney;
	}

	public String getRefundTimeStr() {
		return refundTimeStr;
	}

	public void setRefundTime(String refundTimeStr) {
		this.refundTimeStr = refundTimeStr;
	}

	public String getRefundTradeNo() {
		return refundTradeNo;
	}

	public void setRefundTradeNo(String refundTradeNo) {
		this.refundTradeNo = refundTradeNo;
	}

	public Integer getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(Integer refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public String getSubPhoneNo() {
		return subPhoneNo;
	}

	public void setSubPhoneNo(String subPhoneNo) {
		this.subPhoneNo = subPhoneNo;
	}

	public String getRefundRecordId() {
		return refundRecordId;
	}

	public void setRefundRecordId(String refundRecordId) {
		this.refundRecordId = refundRecordId;
	}

	public Integer getPayType() {
		return payType;
	}

	public void setPayType(Integer payType) {
		this.payType = payType;
	}

	public Integer getImmediately() {
		return immediately;
	}

	public void setImmediately(Integer immediately) {
		this.immediately = immediately;
	}
	
}
