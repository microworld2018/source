
package com.dearho.cs.RefundRecord.service;

import java.util.Map;

import com.dearho.cs.RefundRecord.pojo.RefundRecord;
import com.dearho.cs.core.db.page.Page;

public interface RefundRecordService {
	
	public Page<RefundRecord> getPageRefund(Page<RefundRecord> page,RefundRecord refundRecord);
	
	/**根据实体类不为空的属性获取退款记录
	 * @param refundRecord
	 * @return
	 */
	public RefundRecord getByParam(RefundRecord refundRecord);
	
	
	/**更新退款申请记录
	 * @param refundRecord
	 */
	public void updateRefundRecord(RefundRecord refundRecord);
	
	
	Map<String,String> getPayTypeBySubId(String subId);

	/**
	 * 是否有异常订单或未处理订单
	 * @param subId
	 * @return
	 */
	boolean hasAbnormal(String subId);
	
	/**根据会员ID获取最后一次退款申请记录
	 * @param subId
	 * @return
	 */
	RefundRecord getBySubMaxTime(String subId);

}
