package com.dearho.cs.RefundRecord.service.impl;

import java.util.Map;

import com.dearho.cs.RefundRecord.dao.RefundRecordDao;
import com.dearho.cs.RefundRecord.pojo.RefundRecord;
import com.dearho.cs.RefundRecord.service.RefundRecordService;
import com.dearho.cs.core.db.page.Page;

public class RefundRecordServiceImpl implements RefundRecordService {

	private RefundRecordDao refundRecordDao;
	
	public RefundRecordDao getRefundRecordDao() {
		return refundRecordDao;
	}



	public void setRefundRecordDao(RefundRecordDao refundRecordDao) {
		this.refundRecordDao = refundRecordDao;
	}



	@Override
	public Page<RefundRecord> getPageRefund(Page<RefundRecord> page,
			RefundRecord refundRecord) {
		return refundRecordDao.getPageRefundN(page, refundRecord);
	}



	@Override
	public RefundRecord getByParam(RefundRecord refundRecord) {
		return refundRecordDao.getByParam(refundRecord);
	}



	@Override
	public void updateRefundRecord(RefundRecord refundRecord) {
		refundRecordDao.updateRefundRecord(refundRecord);
	}



	@Override
	public Map<String,String> getPayTypeBySubId(String subId) {
		return refundRecordDao.getPayTypeBySubId(subId);
	}

	@Override
	public boolean hasAbnormal(String subId) {
		return refundRecordDao.hasAbnormal(subId);
	}



	@Override
	public RefundRecord getBySubMaxTime(String subId) {
		return refundRecordDao.getBySubMaxTime(subId);
	}
	
	
}
