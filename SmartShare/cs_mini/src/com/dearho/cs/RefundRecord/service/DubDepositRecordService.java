package com.dearho.cs.RefundRecord.service;

import com.dearho.cs.RefundRecord.pojo.DubDepositRecord;
import com.dearho.cs.account.pojo.AccountTradeRecord;

/**会员押金记录Service接口
 * @author Gaopl
 *
 */
public interface DubDepositRecordService {
	
	/**根据记录编号获取押金记录
	 * @param tradeOrderNo
	 * @return
	 */
	public DubDepositRecord getByTradeOrderNo(String tradeOrderNo);
	
	
	/**更新押金记录
	 * @param dubDepositRecord
	 */
	public void updateDubDepositRecord(DubDepositRecord dubDepositRecord);
	
	
	/**添加会员押金退款记录
	 * @param dubDepositRecord
	 */
	public void saveDubDepositRecord(DubDepositRecord dubDepositRecord);
	
}
