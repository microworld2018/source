package com.dearho.cs.RefundRecord.service.impl;

import com.dearho.cs.RefundRecord.dao.DubDepositRecordDao;
import com.dearho.cs.RefundRecord.pojo.DubDepositRecord;
import com.dearho.cs.RefundRecord.service.DubDepositRecordService;

/**会员押金记录Service接口实现层
 * @author Gaopl
 *
 */
public class DubDepositRecordServiceImpl implements DubDepositRecordService {
	
	private DubDepositRecordDao dubDepositRecordDao;

	@Override
	public DubDepositRecord getByTradeOrderNo(String tradeOrderNo) {
		// TODO Auto-generated method stub
		return dubDepositRecordDao.getByTradeOrderNo(tradeOrderNo);
	}

	@Override
	public void updateDubDepositRecord(DubDepositRecord dubDepositRecord) {
		dubDepositRecordDao.updateDubDepositRecord(dubDepositRecord);
	}
	
	
	@Override
	public void saveDubDepositRecord(DubDepositRecord dubDepositRecord) {
		dubDepositRecordDao.saveDubDepositRecord(dubDepositRecord);
	}
	
	
	
	public DubDepositRecordDao getDubDepositRecordDao() {
		return dubDepositRecordDao;
	}

	public void setDubDepositRecordDao(DubDepositRecordDao dubDepositRecordDao) {
		this.dubDepositRecordDao = dubDepositRecordDao;
	}
}
