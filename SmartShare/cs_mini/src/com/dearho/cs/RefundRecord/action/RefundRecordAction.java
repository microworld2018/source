package com.dearho.cs.RefundRecord.action;



import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.transaction.annotation.Transactional;

import com.dearho.cs.Deposit.pojo.AccDeposit;
import com.dearho.cs.Deposit.service.AccDepositService;
import com.dearho.cs.Deposit.service.DepositStrategyService;
import com.dearho.cs.RefundRecord.pojo.DubDepositRecord;
import com.dearho.cs.RefundRecord.pojo.RefundRecord;
import com.dearho.cs.RefundRecord.service.DubDepositRecordService;
import com.dearho.cs.RefundRecord.service.RefundRecordService;
import com.dearho.cs.account.pojo.Account;
import com.dearho.cs.account.pojo.AccountTradeRecord;
import com.dearho.cs.account.service.AccountTradeRecordService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.service.OrderDataReportService;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.subscriber.service.SubscriberService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.SysOperateLogRecord;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.SystemOperateLogService;
import com.dearho.cs.sys.util.SystemOperateLogUtil;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.DateUtil;
import com.dearho.cs.util.orders.Arith;
import com.opensymphony.webwork.ServletActionContext;

/**会员退款申请
 * @author Gaopl
 *
 */
public class RefundRecordAction extends AbstractAction{
	
	private static final long serialVersionUID = 1L;
	private RefundRecordService refundRecordService;
	private SubscriberService subscriberService;
	private AccDepositService accDepositService;
	private DepositStrategyService depositStrategyService;
	private DubDepositRecordService dubDepositRecordService;
	private OrderDataReportService orderDataReportService;
	private AccountTradeRecordService accountTradeRecordService;
	
	public AccountTradeRecordService getAccountTradeRecordService() {
		return accountTradeRecordService;
	}

	public void setAccountTradeRecordService(AccountTradeRecordService accountTradeRecordService) {
		this.accountTradeRecordService = accountTradeRecordService;
	}

	public OrderDataReportService getOrderDataReportService() {
		return orderDataReportService;
	}

	public void setOrderDataReportService(OrderDataReportService orderDataReportService) {
		this.orderDataReportService = orderDataReportService;
	}

	private SystemOperateLogService systemOperateLogService;// 记录操作日志Service
	
	
	private Page<RefundRecord> page = new Page<RefundRecord>();
	
	private RefundRecord refundRecord;
	
	@Override
	public String process() {
		page = refundRecordService.getPageRefund(page, refundRecord);
		if(page != null && page.getResults() != null){
			for(Object obj : page.getResults()){
				RefundRecord rr = (RefundRecord) obj;
				if (DateUtil.daysBetween(rr.getCreateTime(), new Date()) <= 10)
					rr.setEditable(0);
				else
					rr.setEditable(1);
				
				// String
				// payType=refundRecordService.getPayTypeBySubId(rr.getSubId());
				// orderDataReportService.getPayTypeByTradeOrderNo(rr.getTradeOrderNo());

				Map<String, String> map = refundRecordService.getPayTypeBySubId(rr.getSubId());
//				rr.setPayType(map.get("payType"));
//				rr.setTradeOrderNo(map.get("tradeOrderNo"));
				rr.setDepositTime(map.get("tradeTime"));
				if (refundRecordService.hasAbnormal(rr.getSubId())){
					rr.setHasAbnormal(1);					
				}else{
					rr.setHasAbnormal(0);
				}				
				Subscriber subscriber = subscriberService.querySubscriberById(rr.getSubId());
				rr.setSubscriber(subscriber);
			}
		}
		
		
		return SUCCESS;
	}
	
	/**获取退款申请详情
	 * @return
	 */
	public String getRefundRecordById(){
		refundRecord = refundRecordService.getByParam(refundRecord);
		if(refundRecord != null){
			Subscriber subscriber = subscriberService.querySubscriberById(refundRecord.getSubId());
			AccDeposit accDeposits = new AccDeposit();
			accDeposits.setSubscriberId(refundRecord.getSubId());
			accDeposits = accDepositService.getDeposit(accDeposits);
			if(accDeposits == null){
				return "timeout";
			}
			refundRecord.setSubscriber(subscriber);
			refundRecord.setAccDeposit(accDeposits);
			
			Map<String, String> map = refundRecordService.getPayTypeBySubId(refundRecord.getSubId());
			refundRecord.setTradeOrderNo(map.get("tradeOrderNo"));//
			refundRecord.setPayType(map.get("payType"));
			
		}
		
		/***********************添加退款记录********************************/
		SysOperateLogRecord log = new SysOperateLogRecord();
		HttpSession session = ServletActionContext.getRequest().getSession();
		User user = (User) session.getAttribute("user");
		Date date = new Date();
		log.setOperatorId(user.getId());
		log.setOperatorName(user.getName());
		log.setOperateDate(date);
		log.setOperateRemark(SystemOperateLogUtil.DOT_DATA_VIEW);
		log.setModelName("查看退款申请记录");
		log.setKeyword("查看退款申请记录");
		log.setOperateContent("查看退款申请记录");
		systemOperateLogService.addSysOperateLogRecord(log);
		return SUCCESS;
	}
	
	
	/**确认退款
	 * @return
	 */
	//@Transactional
	public String okRefundRecord(){
		String id = getRequest().getParameter("id");
		String subId = getRequest().getParameter("subId");
		double money = Double.parseDouble(getRequest().getParameter("money"));
		String tradeOrderNo2 = getRequest().getParameter("tradeOrderNo2");
		String payType = getRequest().getParameter("payType");
				
		try{			
		//更新退款申请记录主表
		refundRecord.setId(id);
		refundRecord = refundRecordService.getByParam(refundRecord);
		refundRecord.setType(1);
		refundRecord.setStatus(4);
		refundRecord.setActualMoney(money);
		refundRecord.setTradeOrderNo2(tradeOrderNo2);//
		refundRecord.setRefundTime(new Date());
		refundRecordService.updateRefundRecord(refundRecord);
		
		//更新会员押金
		AccDeposit accDeposit = new AccDeposit();
		accDeposit.setSubscriberId(subId);
		accDeposit = accDepositService.getDeposit(accDeposit);
		accDeposit.setAmount(Arith.round(0));
		accDeposit.setFrozenAmount(Arith.round(0));
		accDeposit.setUsableAmount(Arith.round(0));
		accDeposit.setLastOperateTime(new Date());
		accDeposit.setLastOperateAmount(Arith.round(money));
		accDeposit.setLastOperateType(4);
		accDepositService.updateDeposit(accDeposit);
		
		//更新会员押金记录
		//DubDepositRecord dubDepositRecord = new DubDepositRecord();
		DubDepositRecord dubDepositRecord = dubDepositRecordService.getByTradeOrderNo(refundRecord.getTradeOrderNo());
		dubDepositRecord.setType(2);
		dubDepositRecord.setPayStatus(3);//
		dubDepositRecord.setDesc("退款成功");		
		dubDepositRecordService.updateDubDepositRecord(dubDepositRecord);
		
		//写交易记录AccountTradeRecord	
		AccountTradeRecord accountTradeRecord= new AccountTradeRecord();
		accountTradeRecord.setSubscriberId(subId);
		accountTradeRecord.setType(Account.TYPE_REFUNDTRUE);
		accountTradeRecord.setPayChannel(Account.PAY_CHANNEL_CONSOLE);
		accountTradeRecord.setResult(AccountTradeRecord.RESULT_SUCCESS);
		accountTradeRecord.setBizId("");//instance.getId());
		accountTradeRecord.setTradeOrderNo(tradeOrderNo2);//mainRecord.getTradeOrderNo());
		accountTradeRecord.setSubOrderId("");//getRefundTradeNo());		
		accountTradeRecord.setAmount(money);//instance.getRemainingAmount());
		accountTradeRecord.setPayType(Integer.valueOf(payType));
		accountTradeRecord.setIsAutoClear(AccountTradeRecord.IS_AUTO_CLEAR_FALSE);
//		accountTradeRecord.setIsPresetCard();//走退款流程
		accountTradeRecord.setDescription("押金退款 "+money+"元");		
		accountTradeRecord.setTradeTime(new Date());
		accountTradeRecord.setTs(new Date());
		accountTradeRecord.setRemainingAmount(0.0);
		accountTradeRecord.setOrderIndex(1);
		accountTradeRecordService.addAccountTradeRecord(accountTradeRecord);
		result = Ajax.JSONResult(0, "保存成功！");
				
		/***********************添加退款记录********************************/
		SysOperateLogRecord log = new SysOperateLogRecord();
		HttpSession session = ServletActionContext.getRequest().getSession();
		User user = (User) session.getAttribute("user");
		Date date = new Date();
		log.setOperatorId(user.getId());
		log.setOperatorName(user.getName());
		log.setOperateDate(date);
		log.setOperateRemark(SystemOperateLogUtil.UPDATE_OPERATION);
		log.setModelName("押金退款");
		log.setKeyword("押金退款");
		log.setOperateContent("会员押金退款押金退款");
		systemOperateLogService.addSysOperateLogRecord(log);
		return SUCCESS;		
		}
		catch (Exception e) {
			result = Ajax.JSONResult(1, "押金退款失败！");
			return SUCCESS;
		}		
	}	
	
	/**
	 * 立即退款按钮操作
	 */
	public String immediately(){
		try {
			String id = getRequest().getParameter("id");
			refundRecord.setId(id);
			refundRecord = refundRecordService.getByParam(refundRecord);
			refundRecord.setImmediately(1);
			refundRecordService.updateRefundRecord(refundRecord);
			result = Ajax.JSONResult(0, "操作成功！");
			
			/***********************添加立即退款操作记录********************************/
			SysOperateLogRecord log = new SysOperateLogRecord();
			HttpSession session = ServletActionContext.getRequest().getSession();
			User user = (User) session.getAttribute("user");
			Date date = new Date();
			log.setOperatorId(user.getId());
			log.setOperatorName(user.getName());
			log.setOperateDate(date);
			log.setOperateRemark(SystemOperateLogUtil.UPDATE_OPERATION);
			log.setModelName("立即退款操作");
			log.setKeyword("立即退款操作");
			log.setOperateContent("立即退款操作");
			systemOperateLogService.addSysOperateLogRecord(log);
			return SUCCESS;
		} catch (Exception e) {
			result = Ajax.JSONResult(1, "操作失败！");
			return SUCCESS;
		}
	}
	
	public RefundRecordService getRefundRecordService() {
		return refundRecordService;
	}
	public void setRefundRecordService(RefundRecordService refundRecordService) {
		this.refundRecordService = refundRecordService;
	}
	public SubscriberService getSubscriberService() {
		return subscriberService;
	}
	public void setSubscriberService(SubscriberService subscriberService) {
		this.subscriberService = subscriberService;
	}
	public AccDepositService getAccDepositService() {
		return accDepositService;
	}
	public void setAccDepositService(AccDepositService accDepositService) {
		this.accDepositService = accDepositService;
	}
	public DepositStrategyService getDepositStrategyService() {
		return depositStrategyService;
	}
	public void setDepositStrategyService(
			DepositStrategyService depositStrategyService) {
		this.depositStrategyService = depositStrategyService;
	}
	public DubDepositRecordService getDubDepositRecordService() {
		return dubDepositRecordService;
	}
	public void setDubDepositRecordService(
			DubDepositRecordService dubDepositRecordService) {
		this.dubDepositRecordService = dubDepositRecordService;
	}
	public RefundRecord getRefundRecord() {
		return refundRecord;
	}
	public void setRefundRecord(RefundRecord refundRecord) {
		this.refundRecord = refundRecord;
	}
	public Page<RefundRecord> getPage() {
		return page;
	}
	public void setPage(Page<RefundRecord> page) {
		this.page = page;
	}

	public RefundRecordAction() {
		super();
		refundRecord = new RefundRecord();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}

	public SystemOperateLogService getSystemOperateLogService() {
		return systemOperateLogService;
	}

	public void setSystemOperateLogService(
			SystemOperateLogService systemOperateLogService) {
		this.systemOperateLogService = systemOperateLogService;
	}
	
}
