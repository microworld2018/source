package com.dearho.cs.RefundRecord.dao;

import com.dearho.cs.RefundRecord.pojo.DubDepositRecord;

/**会员押金记录DAO层接口
 * @author Gaopl
 *
 */
public interface DubDepositRecordDao {
	
	/**根据记录编号获取押金记录
	 * @param tradeOrderNo
	 * @return
	 */
	public DubDepositRecord getByTradeOrderNo(String tradeOrderNo);
	
	
	/**更新押金记录
	 * @param dubDepositRecord
	 */
	public void updateDubDepositRecord(DubDepositRecord dubDepositRecord);
	
	
	/**添加会员押金退款记录
	 * @param dubDepositRecord
	 */
	public void saveDubDepositRecord(DubDepositRecord dubDepositRecord);
}
