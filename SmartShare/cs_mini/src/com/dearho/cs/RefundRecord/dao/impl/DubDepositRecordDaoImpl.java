package com.dearho.cs.RefundRecord.dao.impl;

import java.util.List;

import com.dearho.cs.RefundRecord.dao.DubDepositRecordDao;
import com.dearho.cs.RefundRecord.pojo.DubDepositRecord;
import com.dearho.cs.core.db.AbstractDaoSupport;

/**会员押金记录DAO层接口实现类
 * @author Gaopl
 *
 */
public class DubDepositRecordDaoImpl extends AbstractDaoSupport implements DubDepositRecordDao {

	@Override
	public DubDepositRecord getByTradeOrderNo(String tradeOrderNo) {
		StringBuffer hql=new StringBuffer();
		hql.append("select a.id from DubDepositRecord a where a.tradeOrderNo = '").append(tradeOrderNo).append("'");
		List<DubDepositRecord> list= getList(DubDepositRecord.class, queryFList(hql.toString()));
		if(list ==null || list.size()==0){
			return null;
		}else{
			return list.get(0);
		}
	}

	@Override
	public void updateDubDepositRecord(DubDepositRecord dubDepositRecord) {
		updateEntity(dubDepositRecord);
	}

	@Override
	public void saveDubDepositRecord(DubDepositRecord dubDepositRecord) {
		addEntity(dubDepositRecord);
	}

}
