package com.dearho.cs.RefundRecord.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dearho.cs.RefundRecord.dao.RefundRecordDao;
import com.dearho.cs.RefundRecord.pojo.RefundRecord;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.util.StringHelper;

public class RefundRecordDaoImpl extends AbstractDaoSupport implements RefundRecordDao {

	@Override
	public Page<RefundRecord> getPageRefund(Page<RefundRecord> page, RefundRecord refundRecord) {
		// StringBuffer hql = new StringBuffer("select a.id from RefundRecord
		// a,Subscriber s where a.subId = s.id");
		// if(refundRecord != null && refundRecord.getSubscriber() != null &&
		// StringHelper.isNotEmpty(refundRecord.getSubscriber().getName())){
		// hql.append(" and s.name like
		// '%").append(refundRecord.getSubscriber().getName()).append("%'");
		// }
		// if(refundRecord != null && refundRecord.getSubscriber() != null &&
		// StringHelper.isNotEmpty(refundRecord.getSubscriber().getPhoneNo())){
		// hql.append(" and s.phoneNo
		// ='").append(refundRecord.getSubscriber().getPhoneNo()).append("'");
		// }
		// if(refundRecord != null && refundRecord.getSubscriber() != null &&
		// StringHelper.isNotEmpty(refundRecord.getSubscriber().getSex())){
		// hql.append(" and s.sex =
		// '").append(refundRecord.getSubscriber().getSex()).append("'");
		// }
		// if(refundRecord != null && refundRecord.getSubscriber() != null &&
		// refundRecord.getSubscriber().getState() != null){
		// hql.append(" and s.state =
		// ").append(refundRecord.getSubscriber().getState());
		// }
		// if(refundRecord.getStatus() != null){
		// hql.append(" and a.status = ").append(refundRecord.getStatus());
		// }else{
		// hql.append(" and a.status != 4");
		// }
		// hql.append(" order By a.createTime desc");
		// Page<RefundRecord> resultPage=pageCache(RefundRecord.class, page,
		// hql.toString());
		// resultPage.setResults(idToObj(RefundRecord.class,
		// resultPage.getmResults()));
		// return resultPage;

		StringBuffer hql = new StringBuffer(
				"select a.id from acc_refund_record a,sub_subscriber s where a.sub_id = s.id");
		if (refundRecord != null && refundRecord.getSubscriber() != null
				&& StringHelper.isNotEmpty(refundRecord.getSubscriber().getName())) {
			hql.append(" and s.name like '%").append(refundRecord.getSubscriber().getName()).append("%'");
		}
		if (refundRecord != null && refundRecord.getSubscriber() != null
				&& StringHelper.isNotEmpty(refundRecord.getSubscriber().getPhoneNo())) {
			hql.append(" and s.phone_no ='").append(refundRecord.getSubscriber().getPhoneNo()).append("'");
		}
		if (refundRecord != null && refundRecord.getSubscriber() != null
				&& StringHelper.isNotEmpty(refundRecord.getSubscriber().getSex())) {
			hql.append(" and s.sex = '").append(refundRecord.getSubscriber().getSex()).append("'");
		}
		if (refundRecord != null && refundRecord.getSubscriber() != null
				&& refundRecord.getSubscriber().getState() != null) {
			hql.append(" and s.state = ").append(refundRecord.getSubscriber().getState());
		}
		if (refundRecord.getStatus() != null) {
			if (refundRecord.getStatus() != 0)
				hql.append(" and a.status = ").append(refundRecord.getStatus());
		} else {
			hql.append(" and a.status != 4");
		}

		// hql.append(" AND sub_id NOT IN (SELECT member_id FROM
		// c_vehicle_violation WHERE biz_status <> 0)");
		// hql.append(" AND sub_id NOT IN (SELECT sub_id FROM c_vehicle_accident
		// WHERE accept_flag = 0)");
		// hql.append(" AND sub_id NOT IN (SELECT member_id FROM ord_orders
		// WHERE pay_status = 0)");
		//
		hql.append(" order By a.create_time asc");
		Page<RefundRecord> resultPage = page(page, hql.toString(), true);
		resultPage.setResults(idToObj(RefundRecord.class, resultPage.getResults()));
		return resultPage;
	}

	@Override
	public Page<RefundRecord> getPageRefundN(Page<RefundRecord> page, RefundRecord refundRecord) {
		StringBuffer hql = new StringBuffer(
				"select a.id from acc_refund_record a,sub_subscriber s where a.sub_id = s.id");
		if (refundRecord != null && refundRecord.getSubscriber() != null
				&& StringHelper.isNotEmpty(refundRecord.getSubscriber().getName())) {
			hql.append(" and s.name like '%").append(refundRecord.getSubscriber().getName()).append("%'");
		}
		if (refundRecord != null && refundRecord.getSubscriber() != null
				&& StringHelper.isNotEmpty(refundRecord.getSubscriber().getPhoneNo())) {
			hql.append(" and s.phone_no ='").append(refundRecord.getSubscriber().getPhoneNo()).append("'");
		}
		if (refundRecord != null && refundRecord.getSubscriber() != null
				&& StringHelper.isNotEmpty(refundRecord.getSubscriber().getSex())) {
			hql.append(" and s.sex = '").append(refundRecord.getSubscriber().getSex()).append("'");
		}
		if (refundRecord != null && refundRecord.getSubscriber() != null
				&& refundRecord.getSubscriber().getState() != null) {
			hql.append(" and s.state = ").append(refundRecord.getSubscriber().getState());
		}
		if (refundRecord.getStatus() != null) {
			if (refundRecord.getStatus() != 0)
				hql.append(" and a.status = ").append(refundRecord.getStatus());
		} else {
			hql.append(" and a.status != 4");
		}
		hql.append(" AND ((sub_id NOT IN (SELECT member_id FROM c_vehicle_violation WHERE biz_status = 0 AND is_discard = 0)");
		hql.append(" AND sub_id NOT IN (SELECT member_id FROM c_vehicle_accident WHERE (handle_status = '402881e74e2473c2014e247cca95000b'");
		hql.append(" OR handle_status = '402881e74e2473c2014e247d48db000c'");
		hql.append(" OR handle_status = '402881e74e2473c2014e247d791e000d') AND is_discard = 0)");
		hql.append(" AND sub_id NOT IN (SELECT member_id FROM ord_orders WHERE pay_status = 0 AND state = 4)");
		hql.append(" ) OR immediately=1)");
		hql.append(" order By a.create_time asc");
		Page<RefundRecord> resultPage = page(page, hql.toString(), true);
		resultPage.setResults(idToObj(RefundRecord.class, resultPage.getResults()));
		
/*		//判断是否可编辑
		if (page != null && page.getResults() != null) {
			for (Object obj : page.getResults()) {
				RefundRecord rr = (RefundRecord) obj;
				if (isEditable(rr.getSubId())) {
					rr.setEditable(1);
				} else {
					rr.setEditable(0);
				}
			}
		}*/
		return resultPage;
	}
	
	private boolean isEditable(String subId){
		StringBuffer hql = new StringBuffer(
				"select a.id from acc_refund_record a,sub_subscriber s where a.sub_id = s.id");
		hql.append(" AND s.id='").append(subId).append("'");
		hql.append(" AND sub_id NOT IN (SELECT member_id FROM c_vehicle_violation WHERE biz_status = 0)");
		hql.append(" AND sub_id NOT IN (SELECT sub_id FROM c_vehicle_accident WHERE accept_flag = 0)");
		hql.append(" AND sub_id NOT IN (SELECT member_id FROM ord_orders WHERE pay_status = 0 AND state = 4)");
		hql.append(" AND DATEDIFF(SYSDATE(),a.create_time)>11 ORDER BY a.create_time DESC");
		
//		StringBuffer hql = new StringBuffer(
//				"select a.id from d_sub_deposit_record a,sub_subscriber s where a.sub_id = s.id");
//		hql.append(" AND s.id='").append(subId).append("'");
//		hql.append(" AND sub_id NOT IN (SELECT member_id FROM c_vehicle_violation WHERE biz_status = 0)");
//		hql.append(" AND sub_id NOT IN (SELECT sub_id FROM c_vehicle_accident WHERE accept_flag = 0)");
//		hql.append(" AND sub_id NOT IN (SELECT member_id FROM ord_orders WHERE pay_status = 0 AND state = 4)");
//		hql.append(" AND DATEDIFF(SYSDATE(),a.create_time)>11");
//		hql.append(" AND a.pay_status=2 and a.type=2");

		List list = queryFList(hql.toString(), true);
		if (list.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public RefundRecord getByParam(RefundRecord refundRecord) {
		StringBuffer hql = new StringBuffer("select id from RefundRecord where id='").append(refundRecord.getId())
				.append("'");
		return get(RefundRecord.class, (String) getQuery(hql.toString()).uniqueResult());
	}

	@Override
	public void updateRefundRecord(RefundRecord refundRecord) {
		updateEntity(refundRecord);
	}

	@Override
	public Map<String, String> getPayTypeBySubId(String subId) {
		Map<String, String> resultMap = new HashMap<String, String>();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT a2.pay_type,	a2.trade_order_no, DATE_FORMAT(a2.trade_time, '%Y-%m-%d %H:%i:%S') FROM acc_refund_record a, acc_trade_record_list a2");
		sql.append(" WHERE a.sub_id=a2.subscriber_id AND a2.type=2 AND a2.result=1");
		sql.append(" AND a.sub_id='"+subId+"'");
		sql.append(" ORDER BY a2.trade_time");
		List list = queryFList(sql.toString(),true);//getSession().createSQLQuery(sql.toString()).list();

		String payType = null;
		String tradeOrderNo = null;
		String tradeTime = null;
		if (list.size() > 0) {
			Object[] m = (Object[]) list.get(0);
			payType = m[0] == null ? "" : m[0].toString();
			tradeOrderNo = m[1] == null ? "" : m[1].toString();
			tradeTime = m[2] == null ? "" : m[2].toString();			
			resultMap.put("payType", payType);
			resultMap.put("tradeOrderNo", tradeOrderNo);
			resultMap.put("tradeTime", tradeTime);
		}
		return resultMap;
	}
	
	@Override	
	public boolean hasAbnormal(String subId) {
		StringBuffer hql = new StringBuffer(
				"select s.id from sub_subscriber s where 1=1");
		hql.append(" AND s.id='").append(subId).append("'");
		hql.append(" AND s.id NOT IN (SELECT member_id FROM c_vehicle_violation WHERE biz_status = 0 AND is_discard = 0)");
		hql.append(" AND s.id NOT IN (SELECT member_id FROM c_vehicle_accident WHERE (handle_status = '402881e74e2473c2014e247cca95000b'");
		hql.append(" OR handle_status = '402881e74e2473c2014e247d48db000c'");
		hql.append(" OR handle_status = '402881e74e2473c2014e247d791e000d') AND is_discard = 0)");		
		hql.append(" AND s.id NOT IN (SELECT member_id FROM ord_orders WHERE pay_status = 0 AND state = 4)");		

		List list = queryFList(hql.toString(), true);
		if (list.size() > 0) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public RefundRecord getBySubMaxTime(String subId) {
		StringBuffer hql = new StringBuffer(" FROM RefundRecord  WHERE subId = '").append(subId).append("'");
		hql.append(" AND 'status' NOT IN (4) ORDER BY createTime DESC");
		@SuppressWarnings("unchecked")
		List<RefundRecord> list = getHibernateTemplate().find(hql.toString());
		if(list != null && list.size()>0){
			return list.get(0);
		}else{
			return null;
		}
		
	}
	
}
