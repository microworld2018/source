package com.dearho.cs.groundOrders.dao;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.groundOrders.pojo.GroundOrder;
import com.dearho.cs.place.pojo.BranchDot;

public interface GroundOrdersDao {

	Page<GroundOrder> queryGroundOrderPage(Page<GroundOrder> page, GroundOrder groundOrder, String takeTime, String backTime);

	GroundOrder queryGroundOrderById(String id);

	List<BranchDot> queryDotByCon(BranchDot dot);

	void updateGroundOrder(GroundOrder gOrder);

}
