package com.dearho.cs.groundOrders.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.cxf.common.util.StringUtils;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.groundOrders.dao.GroundOrdersDao;
import com.dearho.cs.groundOrders.pojo.GroundOrder;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.pojo.OrdersDetail;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.util.DateUtil;
import com.dearho.cs.util.StringHelper;

public class GroundOrdersDaoImpl extends AbstractDaoSupport implements GroundOrdersDao {

	@Override
	public Page<GroundOrder> queryGroundOrderPage(Page<GroundOrder> page, GroundOrder groundOrder, String takeTime,
			String backTime) {
		StringBuffer sb = new StringBuffer();
		sb.append("select a.id from GroundOrder a,GroundUser u,Car c where a.subId = u.id and a.carId = c.id");
		if (groundOrder != null) {
			if (StringHelper.isNotEmpty(groundOrder.getOrderNo())) {
				sb.append(" and a.orderNo like '%").append(groundOrder.getOrderNo().trim()).append("%'");
			}
			if (groundOrder.getOrderState() != null) {
				sb.append(" and a.orderState = '").append(groundOrder.getOrderState()).append("'");
			}
			if (StringHelper.isNotEmpty(groundOrder.getSubId())) {
				sb.append(" and a.subId = '").append(groundOrder.getSubId()).append("'");
			}
			if (StringHelper.isNotEmpty(groundOrder.getCarId())) {
				sb.append(" and a.carId = '").append(groundOrder.getCarId()).append("'");
			}
			if (StringHelper.isNotEmpty(groundOrder.getSubPhoneNum())) {
				sb.append(" and u.phone like '%").append(groundOrder.getSubPhoneNum()).append("%'");
			}
			if (StringHelper.isNotEmpty(groundOrder.getCarNum())) {
				sb.append(" and c.vehiclePlateId like '%").append(groundOrder.getCarNum()).append("%'");
			}
			String tuneCarReason = groundOrder.getTuneCarReason();
			if (StringHelper.isNotEmpty(tuneCarReason) && !tuneCarReason.equals("其他")) {
				sb.append(" and a.tuneCarReason = '").append(tuneCarReason).append("'");
			}else if("其他".equals(tuneCarReason)){
				sb.append(" and a.tuneCarReason NOT IN('内部调用车辆','维修车辆','车辆充电','车辆救援')");
			}
		}
		if (StringHelper.isNotEmpty(takeTime)) {
			Date startDate = null;
			try {
				startDate = DateUtil.parseDate(takeTime + " 00:00:00", "yyyy-MM-dd HH:mm:ss");
				sb.append(" and a.takeTime >= '").append(DateUtil.getChar19DateString(startDate)).append("'");
			} catch (Exception e) {
			}
		}
		if (StringHelper.isNotEmpty(backTime)) {
			Date endDate = null;
			try {
				endDate = DateUtil.parseDate(backTime + " 23:59:59", "yyyy-MM-dd HH:mm:ss");
				sb.append(" and a.backTime <= '").append(DateUtil.getChar19DateString(endDate)).append("'");
			} catch (Exception e) {
			}
		}
		sb.append(" order by a.takeTime desc");
		// sb.append(StringHelper.isEmpty(page.getOrderByString()) ? "" :
		// page.getOrderByString());
		Page<GroundOrder> resultPage = pageCache(GroundOrder.class, page, sb.toString());
		resultPage.setResults(idToObj(GroundOrder.class, resultPage.getmResults()));
		return resultPage;
	}

	@Override
	public GroundOrder queryGroundOrderById(String id) {
		GroundOrder groundOrder = get(GroundOrder.class, id);
		if(groundOrder != null && !StringUtils.isEmpty(groundOrder.getCarId())){
			groundOrder.setCar(get(Car.class, groundOrder.getCarId()));
		}
		return groundOrder;
	}

	@Override
	public List<BranchDot> queryDotByCon(BranchDot dot) {
		StringBuffer sb=new StringBuffer();
		sb.append("select a.id from BranchDot a where 1=1");
		if(dot!=null){
			if(StringHelper.isNotEmpty(dot.getAreaId())){
				sb.append(" and a.areaId='").append(dot.getAreaId()).append("'");
			}
			if(StringHelper.isNotEmpty(dot.getId())){
				sb.append(" and a.id in(select dotId from Parking where bizComp in (select bizComp from Parking where dotId='").append(dot.getId()).append("' group by bizComp) group by dotId)");
			}
		}
		return getList(BranchDot.class, queryFList(sb.toString()));
	}

	@Override
	public void updateGroundOrder(GroundOrder gOrder) {
		updateEntity(gOrder);
	}

}
