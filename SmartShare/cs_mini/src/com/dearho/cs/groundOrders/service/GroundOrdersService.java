package com.dearho.cs.groundOrders.service;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.groundOrders.pojo.GroundOrder;
import com.dearho.cs.place.pojo.BranchDot;

public interface GroundOrdersService {

	/**
	 * 按条件查询地勤订单分页信息
	 * @param page
	 * @param groundOrder
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	Page<GroundOrder> queryGroundOrderPage(Page<GroundOrder> page, GroundOrder groundOrder, String takeTime,String backTime);

	/**
	 * 根据id查询地勤订单信息
	 * @param id
	 * @return
	 */
	GroundOrder queryGroundOrderById(String id);

	List<BranchDot> queryDotByCon(BranchDot dot);

	void updateGroundOrder(GroundOrder gOrder);

}
