package com.dearho.cs.groundOrders.service.impl;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.groundOrders.dao.GroundOrdersDao;
import com.dearho.cs.groundOrders.pojo.GroundOrder;
import com.dearho.cs.groundOrders.service.GroundOrdersService;
import com.dearho.cs.place.pojo.BranchDot;

public class GroundOrdersServiceImpl implements GroundOrdersService {

	private GroundOrdersDao groundOrdersDao;

	public GroundOrdersDao getGroundOrdersDao() {
		return groundOrdersDao;
	}

	public void setGroundOrdersDao(GroundOrdersDao groundOrdersDao) {
		this.groundOrdersDao = groundOrdersDao;
	}

	@Override
	public Page<GroundOrder> queryGroundOrderPage(Page<GroundOrder> page, GroundOrder groundOrder, String takeTime,
			String backTime) {
		return groundOrdersDao.queryGroundOrderPage(page, groundOrder, takeTime, backTime);
	}

	@Override
	public GroundOrder queryGroundOrderById(String id) {
		return groundOrdersDao.queryGroundOrderById(id);
	}

	@Override
	public List<BranchDot> queryDotByCon(BranchDot dot) {
		return groundOrdersDao.queryDotByCon(dot);
	}

	@Override
	public void updateGroundOrder(GroundOrder gOrder) {
		groundOrdersDao.updateGroundOrder(gOrder);
	}
	
	
	
	
	
}
