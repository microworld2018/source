package com.dearho.cs.groundOrders.pojo;

import java.io.Serializable;
import java.util.Date;

import com.dearho.cs.car.pojo.Car;

/**
 * 地勤订单
 * 
 * @author Jin Guangyu
 *
 */
public class GroundOrder implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private String orderNo;// 订单号
	private String subId;// 用户id
	private String carId;// 车辆id
	private String takeDotId;// 取车网点
	private String backDotId;// 还车网点
	private Date takeTime;// 取车时间
	private Date backTime;// 还车时间
	private Integer orderState;// 订单状态
	private String tuneCarReason;// 调车原因

	private String backTimeStr;// 字符串类型的结束时间
	private String subName;// 用户名字
	private String subPhoneNum;// 用户手机号
	private String carNum;// 车牌号
	private Car car;
	private String vehicleModelName;// 车型号

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getTakeDotId() {
		return takeDotId;
	}

	public void setTakeDotId(String takeDotId) {
		this.takeDotId = takeDotId;
	}

	public String getBackDotId() {
		return backDotId;
	}

	public void setBackDotId(String backDotId) {
		this.backDotId = backDotId;
	}

	public Date getTakeTime() {
		return takeTime;
	}

	public void setTakeTime(Date takeTime) {
		this.takeTime = takeTime;
	}

	public Date getBackTime() {
		return backTime;
	}

	public void setBackTime(Date backTime) {
		this.backTime = backTime;
	}

	public Integer getOrderState() {
		return orderState;
	}

	public void setOrderState(Integer orderState) {
		this.orderState = orderState;
	}

	public String getTuneCarReason() {
		return tuneCarReason;
	}

	public void setTuneCarReason(String tuneCarReason) {
		this.tuneCarReason = tuneCarReason;
	}

	public String getSubPhoneNum() {
		return subPhoneNum;
	}

	public void setSubPhoneNum(String subPhoneNum) {
		this.subPhoneNum = subPhoneNum;
	}

	public String getCarNum() {
		return carNum;
	}

	public void setCarNum(String carNum) {
		this.carNum = carNum;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public String getVehicleModelName() {
		return vehicleModelName;
	}

	public void setVehicleModelName(String vehicleModelName) {
		this.vehicleModelName = vehicleModelName;
	}

	public String getBackTimeStr() {
		return backTimeStr;
	}

	public void setBackTimeStr(String backTimeStr) {
		this.backTimeStr = backTimeStr;
	}

}
