package com.dearho.cs.groundOrders.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.pojo.CarVehicleModel;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.coupon.pojo.Coupon;
import com.dearho.cs.coupon.pojo.SubCoupon;
import com.dearho.cs.feestrategy.pojo.StrategyBase;
import com.dearho.cs.groundOrders.pojo.GroundOrder;
import com.dearho.cs.groundOrders.service.GroundOrdersService;
import com.dearho.cs.groundUser.pojo.GroundUser;
import com.dearho.cs.groundUser.service.GroundUserService;
import com.dearho.cs.orders.pojo.Orders;
import com.dearho.cs.orders.pojo.OrdersDetail;
import com.dearho.cs.orders.service.OrdersDetailService;
import com.dearho.cs.orders.service.OrdersService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.Dict;
import com.dearho.cs.sys.util.DictUtil;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.DateUtil;
import com.dearho.cs.util.HttpRequestUtil;
import com.dearho.cs.util.PropertiesHelper;
import com.dearho.cs.util.StringHelper;

import net.sf.json.JSONObject;

public class GroundOrdersAction extends AbstractAction {

	private static final long serialVersionUID = 5713694406681534527L;

	private GroundOrdersService groundOrdersService;

	private GroundUserService groundUserService;

	private CarService carService;

	private OrdersService ordersService;

	private OrdersDetailService ordersDetailService;

	private Page<GroundOrder> page = new Page<GroundOrder>();

	private Orders orders;

	private OrdersDetail ordersDetail;

	private GroundOrder groundOrder;

	private String subId;// 用户id

	private String branchDotId;// 网点id

	private String tuneCarReason;// 用车事由

	private List<BranchDot> dots = new ArrayList<BranchDot>();

	private String carId;

	// private String orderState;//前台查询条件,订单状态

	private String carNumber;// 前台查询条件，车牌号

	private String subPhoneNum;// 前台查询条件，手机号

	private String takeTime;
	private String backTime;

	@Override
	public String process() {
		String returnPage = "";
		// if (StringHelper.isNotEmpty(orderState)) {
		// Dict d = DictUtil.getDictByCodes("14", orderState);
		// if (d != null) {
		// groundOrder.setOrderState(d.getId());
		// }
		// }
		if (StringHelper.isNotEmpty(carNumber)) {
			groundOrder.setCarNum(carNumber);
		}
		if (StringHelper.isNotEmpty(subPhoneNum)) {
			groundOrder.setSubPhoneNum(subPhoneNum);
		}

		page = groundOrdersService.queryGroundOrderPage(page, groundOrder, takeTime, backTime);
		returnPage = SUCCESS;

		if (page != null && page.getResults() != null) {
			for (Object obj : page.getResults()) {
				GroundOrder o = (GroundOrder) obj;
				if (!StringHelper.isRealNull(o.getSubId())) {
					o.setSubName(getGroundUser(o.getSubId()).getName());
					o.setSubPhoneNum(getGroundUser(o.getSubId()).getPhone());
				}
				if (!StringHelper.isRealNull(o.getCarId())) {
					Car c = carService.queryCarById(o.getCarId());
					if (c != null) {
						o.setCarNum(c.getVehiclePlateId());
					}
					// SubCoupon subCoupon =
					// subCouponService.getSubcouponByOrderId(o.getId());
					// if (subCoupon != null) {
					// Coupon coupon =
					// couponService.queryCouponById(subCoupon.getCouponId());
					// o.setCouponName(coupon.getCouponName());
					// }
				}
			}
		}

		return returnPage;
	}

	/**
	 * 通过后台下地勤订单
	 * 
	 * @return
	 */
	public String backAddGroundOrder() {
		try {
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put("dotId", branchDotId);
			paramMap.put("carId", carId);
			paramMap.put("userId", subId);
			paramMap.put("tuneCarReason", tuneCarReason);
			String ipcUrl = PropertiesHelper.getValue("ipc_url") + "/gorundOrder/backPlaceAnOrder";
			String content = HttpRequestUtil.getPostMethod(ipcUrl, paramMap);
			JSONObject jsonObject = new JSONObject();
			if (StringHelper.isNotEmpty(content)) {
				jsonObject = JSONObject.fromObject(content);
				Integer code = Integer.parseInt(jsonObject.getString("resultCode"));
				result = Ajax.JSONResult(code, jsonObject.getString("resultMsg"));
			} else {
				jsonObject = JSONObject.fromObject(content);
				result = Ajax.JSONResult(Integer.parseInt(jsonObject.getString("resultCode")),
						jsonObject.getString("resultMsg"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.JSONResult(1, "服务器异常请联系管理员！" + e.getMessage());
		}
		return SUCCESS;
	}

	/**
	 * 查看地勤订单详情
	 * 
	 * @return
	 */
	public String groundOrderDetail() {
		// 获取订单基本信息
		String id = getRequest().getParameter("id");
		groundOrder = groundOrdersService.queryGroundOrderById(id);
		groundOrder.setSubName(getGroundUser(groundOrder.getSubId()).getName());
		if (!StringHelper.isRealNull(groundOrder.getCarId())) {
			Car c = carService.queryCarById(groundOrder.getCarId());
			if (c != null) {
				groundOrder.setCarNum(c.getVehiclePlateId());
				CarVehicleModel cvm = c.getCarVehicleModel();
				if (cvm != null) {
					Dict d = DictUtil.getDictById(cvm.getBrand());
					groundOrder.setVehicleModelName(d.getCnName() + " " + cvm.getName());
				}
			}
		}
		dots = groundOrdersService.queryDotByCon(null);
		Map<String, String> dotMap = new HashMap<String, String>();
		for (BranchDot dot : dots) {
			dotMap.put(dot.getId(), dot.getName());
		}
		if (!StringHelper.isRealNull(groundOrder.getTakeDotId())) {
			groundOrder.setTakeDotId(dotMap.get(groundOrder.getTakeDotId()));
		}
		if (!StringHelper.isRealNull(groundOrder.getBackDotId())) {
			groundOrder.setBackDotId(dotMap.get(groundOrder.getBackDotId()));
		}

		// 根据订单号获取原订单表信息
		orders = getOrdersByOrderNo(groundOrder.getOrderNo());
		// 获取订单详情开始
		ordersDetail.setOrdersId(orders.getId());
		ordersDetail = ordersDetailService.getByOrderDetail(ordersDetail);

		String type = getRequest().getParameter("type");
		if (type == null || type.equals("0")) {
			return SUCCESS;
		} else {
			return "editOrders";
		}
	}

	/**
	 * 保存编辑地勤订单
	 * 
	 * @return
	 */
	public String saveGroundOrderEdit() {
		try {
			// 获取地勤订单表信息
			GroundOrder gOrder = groundOrdersService.queryGroundOrderById(groundOrder.getId());
			gOrder.setOrderState(groundOrder.getOrderState());
			Date backTime = DateUtil.parseDate(groundOrder.getBackTimeStr(), "yyyy-MM-dd HH:mm:ss");
			gOrder.setBackTime(backTime);

			// 获取订单基本信息
			Orders order = ordersService.queryOrdersByOrderNo(gOrder.getOrderNo());
			if (groundOrder.getOrderState() == 1) {
				order.setState("4");
			}
			order.setEndTime(backTime);

			// 获取订单详情信息
			OrdersDetail orderDetail = new OrdersDetail();
			orderDetail.setOrdersId(order.getId());
			OrdersDetail oDetail = ordersDetailService.getByOrderDetail(orderDetail);
			oDetail.setEndTime(backTime);

			// 获取车辆信息
			Car car = carService.queryCarById(gOrder.getCarId());
			Dict ydDict = DictUtil.getDictByCodes("carBizState", "0");
			car.setBizState(ydDict.getId());

			groundOrdersService.updateGroundOrder(gOrder);
			ordersService.updateOrders(order);
			ordersDetailService.updateOrdersDetail(oDetail);
			carService.updateCar(car);

			result = Ajax.JSONResult(0, "修改完成！");

			// Orders order = ordersService.queryOrdersById(orders.getId());
			// if (StringHelper.isNotEmpty(orders.getEndTimeStr().toString())
			// || StringHelper.isNotEmpty(orders.getBeginTimeStr()
			// .toString())) {
			// result = Ajax.JSONResult(1, "请选择租车开始时间和结束时间！");
			// }
			// OrdersDetail orderDetail = new OrdersDetail();
			// orderDetail.setOrdersId(order.getId());
			// OrdersDetail orderDetail1 = ordersDetailService
			// .getByOrderDetail(orderDetail);

			// // 获取里程信息
			// String mileage = "0";// 默认0
			// String msgUrl = PropertiesHelper.getValue("msg_url")
			// + "/car/getCarMileageForGPS";
			// Map<String, String> paramMap = new HashMap<String, String>();
			// paramMap.put("sn", car.getVehiclePlateId());
			// paramMap.put("beginTime", orders.getBeginTimeStr());
			// paramMap.put("endTime", orders.getEndTimeStr());
			// mileage = getCarMileageForGPS(paramMap, msgUrl);
			// if (mileage != null && !"".equals(mileage) &&
			// !"0".equals(mileage)) {
			// // 计算里程、精确到小数点后两位
			// mileage = String.valueOf(div(Double.valueOf(mileage), 1000, 2));
			// } else {
			// mileage = "0";
			// }
			// // 获取计费规则
			// StrategyBase StrategyBase = strategyBaseService
			// .searchStrategyBaseById(orderDetail1.getStrategyId());
			// // 计算里程费用
			// BigDecimal kmPrice = StrategyBase.getKmPrice();// 里程价(单位：公里)
			// BigDecimal allMail = new BigDecimal(mileage);
			// double allMilePrice = mul(kmPrice.doubleValue(),
			// allMail.doubleValue());
			// // 计算时长费用
			// int allTime = (int) ((DateUtil.parseDate(orders.getEndTimeStr(),
			// "yyyy-MM-dd HH:mm").getTime() - DateUtil.parseDate(
			// orders.getBeginTimeStr(), "yyyy-MM-dd HH:mm").getTime()) / 1000 /
			// 60);
			// double allTimePrice = mul(allTime, StrategyBase.getBasePrice()
			// .doubleValue());
			// // 计算总费用
			// double allPrice = add(allMilePrice, allTimePrice);
			//
			// // 更新订单主表
			// order.setBeginTime(DateUtil.parseDate(orders.getBeginTimeStr(),
			// "yyyy-MM-dd HH:mm"));
			// order.setEndTime(DateUtil.parseDate(orders.getEndTimeStr(),
			// "yyyy-MM-dd HH:mm"));
			// order.setState(orders.getState());
			// BigDecimal allPrice1 = new BigDecimal(allPrice);
			// order.setTotalFee(allPrice1);
			// ordersService.updateOrders(order);
			// // 更新订单详情表
			// orderDetail1.setBeginTime(DateUtil.parseDate(
			// orders.getBeginTimeStr(), "yyyy-MM-dd HH:mm"));
			// orderDetail1.setEndTime(DateUtil.parseDate(orders.getEndTimeStr(),
			// "yyyy-MM-dd HH:mm"));
			//
			// orderDetail1.setTimeFee(BigDecimal.valueOf(allTimePrice));
			// orderDetail1.setTotalFee(allPrice1);
			// orderDetail1.setMileFee(BigDecimal.valueOf(allMilePrice));
			// orderDetail1.setMileage(allMail);
			// ordersDetailService.updateOrdersDetail(orderDetail1);
			// // 更新车辆状态

		} catch (Exception e) {
			e.printStackTrace();
			result = Ajax.JSONResult(1, "修改失败！");
		}
		return SUCCESS;
	}

	/**
	 * 根据订单号获取原订单表信息
	 * 
	 * @param orderNo
	 * @return
	 */
	public Orders getOrdersByOrderNo(String orderNo) {
		Orders order = ordersService.queryOrdersByOrderNo(orderNo);
		if (order != null) {
			return order;
		}
		return null;
	}

	/**
	 * 根据ID获取会员信息
	 * 
	 * @param id
	 * @return
	 */
	public GroundUser getGroundUser(String id) {
		GroundUser groundUser = groundUserService.queryGroundUserById(id);
		if (groundUser != null) {
			return groundUser;
		}
		return null;
	}

	public GroundOrdersAction() {
		super();
		groundOrder = new GroundOrder();
		orders = new Orders();
		ordersDetail = new OrdersDetail();
		page.setCountField("a.id");
		page.setCurrentPage(1);
	}

	public GroundOrdersService getGroundOrdersService() {
		return groundOrdersService;
	}

	public void setGroundOrdersService(GroundOrdersService groundOrdersService) {
		this.groundOrdersService = groundOrdersService;
	}

	public GroundUserService getGroundUserService() {
		return groundUserService;
	}

	public void setGroundUserService(GroundUserService groundUserService) {
		this.groundUserService = groundUserService;
	}

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public OrdersService getOrdersService() {
		return ordersService;
	}

	public void setOrdersService(OrdersService ordersService) {
		this.ordersService = ordersService;
	}

	public OrdersDetailService getOrdersDetailService() {
		return ordersDetailService;
	}

	public void setOrdersDetailService(OrdersDetailService ordersDetailService) {
		this.ordersDetailService = ordersDetailService;
	}

	public OrdersDetail getOrdersDetail() {
		return ordersDetail;
	}

	public void setOrdersDetail(OrdersDetail ordersDetail) {
		this.ordersDetail = ordersDetail;
	}

	public Page<GroundOrder> getPage() {
		return page;
	}

	public void setPage(Page<GroundOrder> page) {
		this.page = page;
	}

	public GroundOrder getGroundOrder() {
		return groundOrder;
	}

	public void setGroundOrder(GroundOrder groundOrder) {
		this.groundOrder = groundOrder;
	}

	public String getCarNumber() {
		return carNumber;
	}

	public void setCarNumber(String carNumber) {
		this.carNumber = carNumber;
	}

	public String getSubPhoneNum() {
		return subPhoneNum;
	}

	public void setSubPhoneNum(String subPhoneNum) {
		this.subPhoneNum = subPhoneNum;
	}

	public String getTakeTime() {
		return takeTime;
	}

	public void setTakeTime(String takeTime) {
		this.takeTime = takeTime;
	}

	public String getBackTime() {
		return backTime;
	}

	public void setBackTime(String backTime) {
		this.backTime = backTime;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getBranchDotId() {
		return branchDotId;
	}

	public void setBranchDotId(String branchDotId) {
		this.branchDotId = branchDotId;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public List<BranchDot> getDots() {
		return dots;
	}

	public void setDots(List<BranchDot> dots) {
		this.dots = dots;
	}

	public Orders getOrders() {
		return orders;
	}

	public void setOrders(Orders orders) {
		this.orders = orders;
	}

	public String getTuneCarReason() {
		return tuneCarReason;
	}

	public void setTuneCarReason(String tuneCarReason) {
		this.tuneCarReason = tuneCarReason;
	}

}
