package com.dearho.cs.quartz;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.dearho.cs.appStart.pojo.AppStart;
import com.dearho.cs.appStart.service.AppStartService;

@Component
public class ChangeAppStartStatus {

	@Autowired
	private AppStartService appStartService;

//	@Scheduled(cron = "0 0/2 0/1 * * ? ")//每两分钟
	@Scheduled(cron = "0 0 0/1 * * ? ")//每小时检测一次
	public void changeAppStartStatus() throws Exception {
		System.out.println("检测开屏有效期");
		List<AppStart> appStartList = appStartService.getAppStartList();
		if (appStartList != null && appStartList.size() > 0) {
			for (AppStart appStart : appStartList) {
				if (appStart.getEndTime() != null) {
					Date startTime = appStart.getStartTime();
					Date endTime = appStart.getEndTime();
					Date now = new Date();
					if (now.after(endTime)) {// 开屏已过期
						appStart.setStatus(2);
						appStartService.updateAppStart(appStart);
					}
					if (now.after(startTime) && now.before(endTime)) {// 开屏在使用期内，设为正在使用
						// 首先结束其他正在使用的开屏
						List<AppStart> list = appStartService.getByParam(1);
						if (list != null && list.size() > 0) {
							for (AppStart appSta : list) {
								appSta.setStatus(2);
								appSta.setEndTime(now);
								appStartService.updateAppStart(appSta);
							}
						}
						appStart.setStatus(1);
						appStartService.updateAppStart(appStart);
					}

				}

			}
		}

	}

}
