package com.dearho.cs.sys.dao;

import java.util.Date;
import java.util.Map;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.pojo.OrderStatistics;
import com.dearho.cs.sys.pojo.UsageStatistics;

public interface  StatisticsDao {
	public  Map<String,OrderStatistics>  orderFeeStatistics(String type, Date fromDate, Date toDate);
	
	public  Map<String,UsageStatistics>  usageStatistics(Date fromDate, Date toDate);

	Page<UsageStatistics> queryUsageStatisticsByPage(Page<UsageStatistics> page, Date fromDate, Date toDate);

	Page<UsageStatistics> queryUsageStatisticsByPage(Page<UsageStatistics> page, String hql, boolean naTive);


}
