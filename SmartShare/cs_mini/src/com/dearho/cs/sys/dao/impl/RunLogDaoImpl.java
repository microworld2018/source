package com.dearho.cs.sys.dao.impl;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.dao.RunLogDao;
import com.dearho.cs.sys.pojo.RunLog;
import com.dearho.cs.util.StringHelper;

/**APP操作记录DAO层接口实现类
 * @author Gaopl
 *
 */
public class RunLogDaoImpl extends AbstractDaoSupport implements RunLogDao {

	@Override
	public Page<RunLog> getPageRunLog(Page<RunLog> page, RunLog runLog) {
		StringBuffer hql = new StringBuffer("select a.id from RunLog a where 1 = 1 ");
		if(StringHelper.isNotEmpty(runLog.getOrderOn())){
			hql.append(" and a.orderOn = '").append(runLog.getOrderOn()).append("'");
		}
		if(StringHelper.isNotEmpty(runLog.getLicensePlateNumber())){
			hql.append(" and a.licensePlateNumber like '%").append(runLog.getLicensePlateNumber()).append("%'");
		}
		Page<RunLog> resultPage = pageCache(RunLog.class ,page, hql.toString());
		resultPage.setResults(idToObj(RunLog.class, resultPage.getmResults()));
		return resultPage;
	}

}
