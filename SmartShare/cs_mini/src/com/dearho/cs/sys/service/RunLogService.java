package com.dearho.cs.sys.service;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.pojo.RunLog;

/**APP操作记录Service接口层
 * @author Gaopl
 *
 */
public interface RunLogService {
	
	/**根据条件获取操作记录
	 * @param page
	 * @param runLog
	 * @return
	 */
	public Page<RunLog> getPageRunLog(Page<RunLog> page,RunLog runLog);
	
}
