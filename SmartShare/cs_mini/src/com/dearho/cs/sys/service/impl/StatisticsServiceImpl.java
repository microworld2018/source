package com.dearho.cs.sys.service.impl;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.sys.dao.StatisticsDao;
import com.dearho.cs.sys.pojo.OrderStatistics;
import com.dearho.cs.sys.pojo.UsageStatistics;
import com.dearho.cs.sys.service.StatisticsService;
import com.dearho.cs.util.StringHelper;

public class StatisticsServiceImpl implements StatisticsService {

	
	private StatisticsDao statisticsDao;
	
	public   Map<String,OrderStatistics> orderFeeStatistics(String type,Date fromDate, Date toDate){
		return statisticsDao.orderFeeStatistics(type, fromDate,  toDate);
	}

	public StatisticsDao getStatisticsDao() {
		return statisticsDao;
	}

	public void setStatisticsDao(StatisticsDao statisticsDao) {
		this.statisticsDao = statisticsDao;
	}

	public Map<String, UsageStatistics> usageStatistics(Date fromDate, Date toDate) {
		return statisticsDao.usageStatistics(fromDate,  toDate);
	}

	public Page<UsageStatistics> queryUsageStatisticsByPage(Page<UsageStatistics> page, Date fromDate, Date toDate) {
		return statisticsDao.queryUsageStatisticsByPage(page, fromDate,  toDate);
	}
		
}
