package com.dearho.cs.sys.service;

import java.util.Date;
import java.util.Map;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.pojo.OrderStatistics;
import com.dearho.cs.sys.pojo.UsageStatistics;

public interface StatisticsService {

	public   Map<String,OrderStatistics> orderFeeStatistics(String type, Date fromDate, Date toDate);
	
	public   Map<String,UsageStatistics> usageStatistics(Date fromDate, Date toDate);
	
	public Page<UsageStatistics> queryUsageStatisticsByPage(Page<UsageStatistics> page, Date fromDate, Date toDate);
		
}
