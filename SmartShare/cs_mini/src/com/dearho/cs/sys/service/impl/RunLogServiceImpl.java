package com.dearho.cs.sys.service.impl;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.dao.RunLogDao;
import com.dearho.cs.sys.pojo.RunLog;
import com.dearho.cs.sys.service.RunLogService;

/**APP操作记录Service层接口实现类
 * @author Gaopl
 *
 */
public class RunLogServiceImpl implements RunLogService {
	
	private RunLogDao runLogDao;

	public RunLogDao getRunLogDao() {
		return runLogDao;
	}

	public void setRunLogDao(RunLogDao runLogDao) {
		this.runLogDao = runLogDao;
	}


	@Override
	public Page<RunLog> getPageRunLog(Page<RunLog> page, RunLog runLog) {
		return runLogDao.getPageRunLog(page, runLog);
	}

}
