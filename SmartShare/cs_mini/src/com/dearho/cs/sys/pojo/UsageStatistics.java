package com.dearho.cs.sys.pojo;

import java.io.Serializable;
import java.util.Date;

public class UsageStatistics implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	@FieldComment("姓名")
	private String name;

	@FieldComment("电话")
	private String phoneNo;

	@FieldComment("最后使用时间 ")
	private Date lastestUseTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public Date getLastestUseTime() {
		return lastestUseTime;
	}

	public void setLastestUseTime(Date lastestUseTime) {
		this.lastestUseTime = lastestUseTime;
	}

}
