package com.dearho.cs.sys.pojo;

import java.io.Serializable;
import java.util.Date;

/**APP操作记录
 * @author Gaopl
 *
 */
public class RunLog  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String orderOn;
	private String operatorName;
	private String operatorIp;
	private String licensePlateNumber;
	private String operatorContent;
	private String operatorType;
	private Date createDate;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getOrderOn() {
		return orderOn;
	}
	public void setOrderOn(String orderOn) {
		this.orderOn = orderOn;
	}
	public String getOperatorName() {
		return operatorName;
	}
	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}
	public String getOperatorIp() {
		return operatorIp;
	}
	public void setOperatorIp(String operatorIp) {
		this.operatorIp = operatorIp;
	}
	public String getLicensePlateNumber() {
		return licensePlateNumber;
	}
	public void setLicensePlateNumber(String licensePlateNumber) {
		this.licensePlateNumber = licensePlateNumber;
	}
	public String getOperatorContent() {
		return operatorContent;
	}
	public void setOperatorContent(String operatorContent) {
		this.operatorContent = operatorContent;
	}
	public String getOperatorType() {
		return operatorType;
	}
	public void setOperatorType(String operatorType) {
		this.operatorType = operatorType;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
}
