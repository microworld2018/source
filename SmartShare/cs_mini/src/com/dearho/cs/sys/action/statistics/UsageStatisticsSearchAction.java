package com.dearho.cs.sys.action.statistics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.subscriber.pojo.Subscriber;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.SysOperateLogRecord;
import com.dearho.cs.sys.pojo.UsageStatistics;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.service.StatisticsService;
import com.dearho.cs.sys.service.SystemOperateLogService;
import com.dearho.cs.sys.util.SystemOperateLogUtil;
import com.dearho.cs.util.DateUtil;
import com.dearho.cs.util.ExcelUtil;
import com.opensymphony.webwork.ServletActionContext;

public class UsageStatisticsSearchAction extends AbstractAction {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private StatisticsService statisticsService;

	private Page<UsageStatistics> page = new Page<UsageStatistics>();
	Map<String, UsageStatistics> usageStatisticsMap;

	private Date fromDate;
	private Date toDate;
	private String downLoadDateStr;
	private SystemOperateLogService systemOperateLogService;// 记录操作日志Service
	private String sCount;

	public String getsCount() {
		return sCount;
	}

	public void setsCount(String sCount) {
		this.sCount = sCount;
	}

	public SystemOperateLogService getSystemOperateLogService() {
		return systemOperateLogService;
	}

	public void setSystemOperateLogService(SystemOperateLogService systemOperateLogService) {
		this.systemOperateLogService = systemOperateLogService;
	}

	public UsageStatisticsSearchAction() {
		super();
		page.setCurrentPage(1);
		page.setCountField("unitTime");
	}

	public String process() {
		Date today = null;
		Date lateMonth = null;

		Calendar calendar = Calendar.getInstance();
		// calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		calendar.add(Calendar.DATE, -1);
		lateMonth = calendar.getTime();

		today = new Date();

		if (fromDate == null) {
			fromDate = lateMonth;
		}
		if (toDate == null) {
			toDate = today;
		}

		downLoadDateStr = transDate10String(fromDate) + "_" + transDate10String(toDate);

		usageStatisticsMap = statisticsService.usageStatistics(fromDate, toDate);
		sCount = String.valueOf(usageStatisticsMap.size());
		// page = statisticsService.queryUsageStatisticsByPage(page, fromDate,
		// toDate);
		return SUCCESS;
	}

	public void download() {
		process();

		/*************** 添加站点数据查看日志 **********************/
		SysOperateLogRecord log = new SysOperateLogRecord();
		HttpSession session = ServletActionContext.getRequest().getSession();
		User user = (User) session.getAttribute("user");
		Date date = new Date();
		log.setOperatorId(user.getId());
		log.setOperatorName(user.getName());
		log.setOperateDate(date);
		log.setOperateRemark(SystemOperateLogUtil.EXPORT_EXCEL);
		log.setModelName("新增用户用车统计");
		log.setKeyword("导出用户用车数据");
		log.setOperateContent("导出：" + downLoadDateStr + " 期间用户用车数据");
		// 记录日志
		systemOperateLogService.addSysOperateLogRecord(log);

		List<Object> objList = new ArrayList<Object>();
		for (Map.Entry<String, UsageStatistics> entry : usageStatisticsMap.entrySet()) {
			UsageStatistics UsageStatistics = entry.getValue();
			UsageStatistics.setId(entry.getKey());
			objList.add(UsageStatistics);
		}

		ExcelUtil.exportExcel(objList, "新增用户用车统计" + downLoadDateStr + "_" + new Date().getTime());

	}

	public StatisticsService getStatisticsService() {
		return statisticsService;
	}

	public void setStatisticsService(StatisticsService statisticsService) {
		this.statisticsService = statisticsService;
	}

	public Page<UsageStatistics> getPage() {
		return page;
	}

	public void setPage(Page<UsageStatistics> page) {
		this.page = page;
	}

	public Map<String, UsageStatistics> getusageStatisticsMap() {
		return usageStatisticsMap;
	}

	public void setusageStatisticsMap(Map<String, UsageStatistics> usageStatisticsMap) {
		this.usageStatisticsMap = usageStatisticsMap;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

}
