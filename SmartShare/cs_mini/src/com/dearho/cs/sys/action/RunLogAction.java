package com.dearho.cs.sys.action;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.sys.pojo.RunLog;
import com.dearho.cs.sys.service.RunLogService;

/**APP操作记录Action
 * @author Gaopl
 *
 */
public class RunLogAction extends AbstractAction{
	
	private static final long serialVersionUID = 1L;
	
	private RunLogService runLogService;
	
	private RunLog runLog;
	
	private Page<RunLog> page = new Page<RunLog>();

	public RunLogService getRunLogService() {
		return runLogService;
	}

	public void setRunLogService(RunLogService runLogService) {
		this.runLogService = runLogService;
	}

	public RunLog getRunLog() {
		return runLog;
	}

	public void setRunLog(RunLog runLog) {
		this.runLog = runLog;
	}

	public Page<RunLog> getPage() {
		return page;
	}

	public void setPage(Page<RunLog> page) {
		this.page = page;
	}

	public RunLogAction() {
		super();
		runLog = new RunLog();
		page.setCurrentPage(1);
		page.setCountField("a.id");
	}

	@Override
	public String process() {
		page = runLogService.getPageRunLog(page, runLog);
		return SUCCESS;
	}
	
	
}
