package com.dearho.cs.carservice.pojo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

/**会员车辆违章事故记录实体类
 * @author Gaopl
 *
 */
public class ViolationAccidentRecord implements Serializable{

	private static final long serialVersionUID = 1L;

	/**
	 * 记录ID
	 */
	private String id;
	
	/**
	 * 类型:0:车辆违章1:车辆事故
	 */
	private Integer type;
	
	/**
	 * 会员ID
	 */
	private String subId;
	
	/**
	 * 车辆ID
	 */
	private String carId;
	
	/**
	 * 订单编号
	 */
	private String orderNo;
	
	/**
	 * 金额
	 */
	private Double money;
	
	/**
	 * 处理状态0:待处理1:已处理
	 */
	private Integer status;
	
	/**
	 * 交易编号
	 */
	private String tradeOrderNo;
	
	/**
	 * 支付状态0:未支付1:已支付
	 */
	private Integer payStatus;
	
	/**
	 * 发生时间
	 */
	private Date happenTime;
	
	/**
	 * 创建人ID
	 */
	private String creatorId;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	private String accidentViolationId;//违章或者事故ID
	
	/**
	 * 描述
	 */
	@Column(name = "[DESC]", nullable = false)
	private String desc;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getSubId() {
		return subId;
	}

	public void setSubId(String subId) {
		this.subId = subId;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getTradeOrderNo() {
		return tradeOrderNo;
	}

	public void setTradeOrderNo(String tradeOrderNo) {
		this.tradeOrderNo = tradeOrderNo;
	}

	public Integer getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Integer payStatus) {
		this.payStatus = payStatus;
	}

	public Date getHappenTime() {
		return happenTime;
	}

	public void setHappenTime(Date happenTime) {
		this.happenTime = happenTime;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getAccidentViolationId() {
		return accidentViolationId;
	}

	public void setAccidentViolationId(String accidentViolationId) {
		this.accidentViolationId = accidentViolationId;
	}
	
}
