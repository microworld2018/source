package com.dearho.cs.carservice.dao.impl;

import java.util.List;

import com.dearho.cs.Deposit.pojo.AccDeposit;
import com.dearho.cs.carservice.dao.ViolationAccidentRecordDao;
import com.dearho.cs.carservice.pojo.ViolationAccidentRecord;
import com.dearho.cs.core.db.AbstractDaoSupport;

/**会员车辆违章事故记录Dao接口实现类
 * @author Gaopl
 *
 */
public class ViolationAccidentRecordDaoImpl extends AbstractDaoSupport implements
		ViolationAccidentRecordDao {

	@Override
	public void addViolationAccident(
			ViolationAccidentRecord violationAccidentRecord) {
		addEntity(violationAccidentRecord);
	}

	@Override
	public void updateViolationAccident(
			ViolationAccidentRecord violationAccidentRecord) {
		updateEntity(violationAccidentRecord);
	}

	@Override
	public ViolationAccidentRecord getByOrdeeNo(String orderNo) {
		StringBuffer hql=new StringBuffer();
		hql.append("select a.id from ViolationAccidentRecord a where a.accidentViolationId = '").append(orderNo).append("'");
		List<ViolationAccidentRecord> list= getList(ViolationAccidentRecord.class, queryFList(hql.toString()));
		if(list ==null || list.size()==0){
			return null;
		}else{
			return list.get(0);
		}
	}

}
