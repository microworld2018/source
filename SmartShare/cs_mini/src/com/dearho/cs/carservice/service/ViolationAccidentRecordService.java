package com.dearho.cs.carservice.service;

import com.dearho.cs.carservice.pojo.ViolationAccidentRecord;

/**会员车辆违章事故记录Service接口
 * @author Gaopl
 *
 */
public interface ViolationAccidentRecordService {
	
	/**添加会员车辆违章事故记录
	 * @param violationAccidentRecord
	 */
	public void addViolationAccident(ViolationAccidentRecord violationAccidentRecord);
	
	
	/**更新会员车辆违章事故记录
	 * @param violationAccidentRecord
	 */
	public void updateViolationAccident(ViolationAccidentRecord violationAccidentRecord);
	
	/**根据订单编号获取车辆违章事故记录
	 * @param orderNo
	 * @return
	 */
	public ViolationAccidentRecord getByOrdeeNo(String orderNo);
	

}
