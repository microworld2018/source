package com.dearho.cs.carservice.service.impl;

import com.dearho.cs.carservice.dao.ViolationAccidentRecordDao;
import com.dearho.cs.carservice.pojo.ViolationAccidentRecord;
import com.dearho.cs.carservice.service.ViolationAccidentRecordService;

/**会员车辆违章事故记录Service接口实现类
 * @author Gaopl
 *
 */
public class ViolationAccidentRecordServiceImpl implements
		ViolationAccidentRecordService {

	private ViolationAccidentRecordDao violationAccidentRecordDao;
	
	@Override
	public void addViolationAccident(
			ViolationAccidentRecord violationAccidentRecord) {
		violationAccidentRecordDao.addViolationAccident(violationAccidentRecord);
	}

	public ViolationAccidentRecordDao getViolationAccidentRecordDao() {
		return violationAccidentRecordDao;
	}

	public void setViolationAccidentRecordDao(
			ViolationAccidentRecordDao violationAccidentRecordDao) {
		this.violationAccidentRecordDao = violationAccidentRecordDao;
	}

	@Override
	public void updateViolationAccident(
			ViolationAccidentRecord violationAccidentRecord) {
		violationAccidentRecordDao.updateViolationAccident(violationAccidentRecord);
	}

	@Override
	public ViolationAccidentRecord getByOrdeeNo(String orderNo) {
		return violationAccidentRecordDao.getByOrdeeNo(orderNo);
	}

	
}
