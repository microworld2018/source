package com.dearho.cs.insuranceRules.action;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.pojo.InsuranceDesc;
import com.dearho.cs.insuranceRules.pojo.InsuranceOperationLog;
import com.dearho.cs.insuranceRules.pojo.InsuranceRules;
import com.dearho.cs.insuranceRules.service.InsuranceDescService;
import com.dearho.cs.insuranceRules.service.InsuranceOperationLogService;
import com.dearho.cs.insuranceRules.service.InsuranceRulesService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.StringHelper;
import com.opensymphony.webwork.ServletActionContext;

/**保险规则管理Action
 * @author Gaopl
 * @createDate 2017年5月9日
 */
public class InsuranceRulesAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private InsuranceRulesService insuranceRulesService;//保险规则Service
	
	private InsuranceDescService insuranceDescService;//保险基础内容Service
	
	private InsuranceOperationLogService insuranceOperationLogService;//保险操作记录Service
	
	private List<InsuranceRules> insuranceList;//保险规则列表
	
	private List<InsuranceOperationLog> insuranceOperationLogList;//保险操作记录
	
	private Page<InsuranceRules> page = new Page<InsuranceRules>();//保险分页
	
	private InsuranceRules insuranceRules;//保险实体类
	
	private String id;//保险ID
	
	private String state;//类型（添加或者更新）
	
	//保险基础内容
	private String[] insuranceDescArr;
	//保险基础内容ID
	private String[] insuranceDescIdArr;
	
	private String[] checkdel;
	

	/*
	 * 获取保险规则列表
	 */
	@Override
	public String process() {
		try {
			page = insuranceRulesService.getInsuranceRulesPage(page, insuranceRules);
			insuranceList = insuranceRulesService.getInusrancaeRulesLisst(new InsuranceRules());
			if(state != null && state.equals("carInsurance")){
				return "carInsurance";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ERROR;
		}
		return SUCCESS;
	}
	
	/**根据ID进行逻辑删除保险规则
	 * @return
	 */
	public String delInsuranceRules(){
		try {
			InsuranceRules insurance = insuranceRulesService.getById(insuranceRules.getId());
			insurance.setIsDel(1);
			insurance.setUpdateTime(new Date());
			insuranceRulesService.updateInsuranceRules(insurance);
			//添加保险规则删除记录
			HttpSession session = ServletActionContext.getRequest().getSession();
			User user=(User) session.getAttribute("user");
			InsuranceOperationLog insuranceOperationLog = new InsuranceOperationLog();
			insuranceOperationLog.setType(0);
			insuranceOperationLog.setTime(new Date());
			insuranceOperationLog.setCreatorId(user.getId());
			insuranceOperationLog.setCreatorName(user.getName());
			insuranceOperationLog.setName(insuranceRules.getName());
			insuranceOperationLog.setOperationContent("删除保险规则，保险规则名称："+insuranceRules.getName()+"保险ID："+insuranceRules.getId());
			insuranceOperationLogService.saveInsuranceOperationLogDao(insuranceOperationLog);
			result=Ajax.JSONResult(0, "删除成功！");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result=Ajax.JSONResult(1, "删除失败！");
		}
		return SUCCESS;
	}

	/**打开保险添加或者编辑页面
	 * @return
	 */
	public String getByIdInsurance(){
		try {
			if(state.equals("update")){
				insuranceRules = insuranceRulesService.getById(id);
				List<InsuranceDesc> list = insuranceDescService.getByInsuranceId(id);
				insuranceRules.setInsuranceDescList(list);
			}
			/*查看保险内容*/
			if(state.equals("toView")){
				insuranceRules = insuranceRulesService.getById(id);
				List<InsuranceDesc> list = insuranceDescService.getByInsuranceId(id);
				insuranceRules.setInsuranceDescList(list);
				return "toView";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**新增或者更新保险
	 * @return
	 */
	public String saveOrUpdateInsurance(){
		HttpSession session = ServletActionContext.getRequest().getSession();
		User user=(User) session.getAttribute("user");
		String name = "";//存储保险名称（添加操作记录使用）
		try {
			if(StringHelper.isNotEmpty(insuranceRules.getId())){
				//更新保险信息
				InsuranceRules insurance = new InsuranceRules();
				insurance = insuranceRulesService.getById(insuranceRules.getId());
				name = insurance.getName();
				insurance.setUpdateTime(new Date());
				insurance.setInsuranceDesc(insuranceRules.getInsuranceDesc());
				insurance.setName(insuranceRules.getName());
				insuranceRulesService.updateInsuranceRules(insurance);
				//更新保险基础内容
				if(insuranceDescIdArr != null && insuranceDescIdArr.length > 0){
					insuranceDescService.delInsuranceDesc(insuranceDescIdArr);//删除已有的保险基础内容
				}
				/*添加新的保险基础信息*/
				if(insuranceDescArr != null && insuranceDescArr.length > 0){
					for(int i=0;i<insuranceDescArr.length;i++){
						InsuranceDesc insuranceDesc = new InsuranceDesc();
						insuranceDesc.setCreateTime(new Date());
						insuranceDesc.setContent(insuranceDescArr[i]);
						insuranceDesc.setCreatorId(user.getId());
						insuranceDesc.setInsuranceId(insuranceRules.getId());
						insuranceDescService.saveInsuranceDesc(insuranceDesc);
					}
				}
				//添加保险更新记录
				InsuranceOperationLog insuranceOperationLog = new InsuranceOperationLog();
				insuranceOperationLog.setType(0);
				insuranceOperationLog.setTime(new Date());
				insuranceOperationLog.setCreatorName(user.getName());
				insuranceOperationLog.setCreatorId(user.getId());
				insuranceOperationLog.setName(insuranceRules.getName());
				insuranceOperationLog.setOperationContent("保险规则更新，更新前名称："+name+"，更新后的名称："+insuranceRules.getName());
				insuranceOperationLogService.saveInsuranceOperationLogDao(insuranceOperationLog);
				result=Ajax.JSONResult(0, "更新成功！");
			}else{
				//添加保险信息
				insuranceRules.setCreateTime(new Date());
				insuranceRules.setCreatorId(user.getId());
				insuranceRules.setIsDel(0);
				insuranceRulesService.saveInsuranceRules(insuranceRules);
				/*添加保险基础信息*/
				if(insuranceDescArr != null && insuranceDescArr.length > 0){
					for(int i=0;i<insuranceDescArr.length;i++){
						InsuranceDesc insuranceDesc = new InsuranceDesc();
						insuranceDesc.setCreateTime(new Date());
						insuranceDesc.setContent(insuranceDescArr[i]);
						insuranceDesc.setCreatorId(user.getId());
						insuranceDesc.setInsuranceId(insuranceRules.getId());
						insuranceDescService.saveInsuranceDesc(insuranceDesc);
					}
				}
				//添加保险新增记录
				InsuranceOperationLog insuranceOperationLog = new InsuranceOperationLog();
				insuranceOperationLog.setType(0);
				insuranceOperationLog.setTime(new Date());
				insuranceOperationLog.setCreatorName(user.getName());
				insuranceOperationLog.setCreatorId(user.getId());
				insuranceOperationLog.setName(insuranceRules.getName());
				insuranceOperationLog.setOperationContent("保险规则新增，保险规则名称："+insuranceRules.getName());
				insuranceOperationLogService.saveInsuranceOperationLogDao(insuranceOperationLog);
				result=Ajax.JSONResult(0, "添加成功！");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result=Ajax.JSONResult(1, "更新或者添加失败！");
		}
		return SUCCESS;
	}
	
	
	/**删除保险基础内容
	 * @return
	 */
	public String delInsuranceDes(){
		try {
			insuranceDescService.delInsuranceDesc(insuranceDescIdArr);
/*			//添加保险基础内容删除记录
			HttpSession session = ServletActionContext.getRequest().getSession();
			User user=(User) session.getAttribute("user");
			InsuranceOperationLog insuranceOperationLog = new InsuranceOperationLog();
			insuranceOperationLog.setType(0);
			insuranceOperationLog.setTime(new Date());
			insuranceOperationLog.setCreatorName(user.getName());
			insuranceOperationLog.setCreatorId(user.getId());
			insuranceOperationLog.setOperationContent("删除保险基础内容");
			insuranceOperationLogService.saveInsuranceOperationLogDao(insuranceOperationLog);*/
			result=Ajax.JSONResult(0, "删除成功！");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**查看保险操作记录
	 * @return
	 */
	public String toViewInsuranceOperating(){
		try {
			insuranceRules = insuranceRulesService.getById(id);
			insuranceOperationLogList = insuranceOperationLogService.getByParam(insuranceRules.getName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public InsuranceRulesAction() {
		super();
		insuranceRules = new InsuranceRules();
		page.setCountField("a.id");
		page.setCurrentPage(1);
	}

	public InsuranceRulesService getInsuranceRulesService() {
		return insuranceRulesService;
	}

	public void setInsuranceRulesService(InsuranceRulesService insuranceRulesService) {
		this.insuranceRulesService = insuranceRulesService;
	}

	public List<InsuranceRules> getInsuranceList() {
		return insuranceList;
	}

	public void setInsuranceList(List<InsuranceRules> insuranceList) {
		this.insuranceList = insuranceList;
	}

	public Page<InsuranceRules> getPage() {
		return page;
	}

	public void setPage(Page<InsuranceRules> page) {
		this.page = page;
	}

	public InsuranceRules getInsuranceRules() {
		return insuranceRules;
	}

	public void setInsuranceRules(InsuranceRules insuranceRules) {
		this.insuranceRules = insuranceRules;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String[] getInsuranceDescArr() {
		return insuranceDescArr;
	}

	public void setInsuranceDescArr(String[] insuranceDescArr) {
		this.insuranceDescArr = insuranceDescArr;
	}

	public InsuranceDescService getInsuranceDescService() {
		return insuranceDescService;
	}

	public void setInsuranceDescService(InsuranceDescService insuranceDescService) {
		this.insuranceDescService = insuranceDescService;
	}

	public String[] getInsuranceDescIdArr() {
		return insuranceDescIdArr;
	}

	public void setInsuranceDescIdArr(String[] insuranceDescIdArr) {
		this.insuranceDescIdArr = insuranceDescIdArr;
	}

	public InsuranceOperationLogService getInsuranceOperationLogService() {
		return insuranceOperationLogService;
	}

	public void setInsuranceOperationLogService(
			InsuranceOperationLogService insuranceOperationLogService) {
		this.insuranceOperationLogService = insuranceOperationLogService;
	}

	public List<InsuranceOperationLog> getInsuranceOperationLogList() {
		return insuranceOperationLogList;
	}

	public void setInsuranceOperationLogList(
			List<InsuranceOperationLog> insuranceOperationLogList) {
		this.insuranceOperationLogList = insuranceOperationLogList;
	}

	public String[] getCheckdel() {
		return checkdel;
	}

	public void setCheckdel(String[] checkdel) {
		this.checkdel = checkdel;
	}
	
}
