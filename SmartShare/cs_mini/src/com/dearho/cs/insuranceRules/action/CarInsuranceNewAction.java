package com.dearho.cs.insuranceRules.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.pojo.CarVehicleModel;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.car.service.CarVehicleModelService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.pojo.CarInsuranceNew;
import com.dearho.cs.insuranceRules.pojo.InsuranceOperationLog;
import com.dearho.cs.insuranceRules.pojo.InsuranceRules;
import com.dearho.cs.insuranceRules.service.CarInsuranceNewService;
import com.dearho.cs.insuranceRules.service.InsuranceDescService;
import com.dearho.cs.insuranceRules.service.InsuranceOperationLogService;
import com.dearho.cs.insuranceRules.service.InsuranceRulesService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.Dict;
import com.dearho.cs.sys.pojo.User;
import com.dearho.cs.sys.util.DictUtil;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.StringHelper;
import com.opensymphony.webwork.ServletActionContext;

/**车型保险管理Action
 * @author Gaopl
 * @createDate 2017年5月9日
 */
public class CarInsuranceNewAction extends AbstractAction {

	private static final long serialVersionUID = 1L;
	
	private CarInsuranceNewService carInsuranceNewService;//车型保险Service
	
	private InsuranceRulesService insuranceRulesService;//保险规则Service
	
	private InsuranceDescService insuranceDescService;//保险基础内容Service
	
	private CarVehicleModelService carVehicleModelService;//车型Service
	
	private InsuranceOperationLogService insuranceOperationLogService;//保险操作记录Service
	
	private CarService carService;//车辆Service
	
	private Page<CarVehicleModel> page = new Page<CarVehicleModel>();//车型保险分页
	
	private List<CarVehicleModel> carVehicleModelList;//车型集合
	
	private CarVehicleModel carVehicleModel;//车型
	
	private CarInsuranceNew carInsuranceNew;//车型保险关联实体类
	
	private List<InsuranceRules> insuranceList;//保险规则集合
	
	private List<CarInsuranceNew> carInsuranceNewList=new ArrayList<CarInsuranceNew>();//车型保险关联集合
	
	private List<InsuranceOperationLog> insuranceOperationLogList;//保险操作记录集合
	
	private String carModelName;
	
	private String insuranceName;
	
	private Integer number;
	
	/**
	 * 选中的保险规则ID
	 */
	private String[] checkdel;

	@Override
	public String process() {
		try {
			//获取车型集合
			carVehicleModelList = carVehicleModelService.queryModelByName(new CarVehicleModel());
			//分页获取车型
			page = carVehicleModelService.queryPageModel(page, carVehicleModel);
			if(page.getResults() != null){
				for(int i=0;i<page.getResults().size();i++){
					//获取当前车型信息
					CarVehicleModel carModel = (CarVehicleModel) page.getResults().get(i);
					//根据车型ID获取车型保险关联集合
					List<CarInsuranceNew> carInsuranceList = carInsuranceNewService.getByParamList(carModel.getId());
					StringBuffer str = new StringBuffer();
					if(carInsuranceList != null){
						//根据车型保险关联获取保险规则名称
						for(int j=0;j<carInsuranceList.size();j++){
							InsuranceRules insuranceRules = insuranceRulesService.getById(carInsuranceList.get(j).getInsuranceId());
							if(insuranceRules != null){
								str.append(insuranceRules.getName()).append(",");
							}
						}
						carModel.setInsuranceUpdateTime(carInsuranceList.get(0).getCreateTime().toString());
					}
					carModel.setInsuranceName(str.toString());
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	/**打开车型保险关联编辑或者新增页面
	 * @return
	 */
	public String getByIdCarInsurance(){
		try {
			//获取车型集合
			carVehicleModelList = carVehicleModelService.queryModelByName(new CarVehicleModel());
			int baseId=0;
			int baseId2=0;
			int count=0;
			if(carVehicleModel != null && StringHelper.isNotEmpty(carVehicleModel.getId())){
				List<CarInsuranceNew> carInsuranceNewList1 = carInsuranceNewService.getByParamList(carVehicleModel.getId());
				CarInsuranceNew carInsuranceNewBase=new CarInsuranceNew();
				if(carInsuranceNewList1 != null){
					for(int i=0;i<carInsuranceNewList1.size();i++){
						InsuranceRules insuranceRules = insuranceRulesService.getById(carInsuranceNewList1.get(i).getInsuranceId());
						if(insuranceRules.getName().equals("基础保险")){
							baseId2=baseId=i;
						}
						carInsuranceNewList1.get(i).setInsuranceRules(insuranceRules);
					}
					carInsuranceNewBase=carInsuranceNewList1.get(baseId);
					//对点击"编辑"后  基础保险与附加保险的排序
					if(baseId==0){
						carInsuranceNewList=carInsuranceNewList1;
					}else{
						for (CarInsuranceNew carInsuranceNew : carInsuranceNewList1) {
							//再加一个int
							if(baseId!=0){
								carInsuranceNewList.add(carInsuranceNewBase);
								carInsuranceNewList.add(carInsuranceNew);
								baseId=0;
							}else{
								if(baseId2==count){
									
								}else{
									carInsuranceNewList.add(carInsuranceNew);
								}
								
							}
							count++;
						}
						
					}
				}
				number = carInsuranceNewList == null?0:carInsuranceNewList.size();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**获取选中的保险规则
	 * @return
	 */
	public String getSelectInsurance(){
		try {
			InsuranceRules insuranceRules = new InsuranceRules();
			StringBuffer sb = new StringBuffer();
			if(checkdel != null){
				for(int i=0;i<checkdel.length;i++){
					if(i<checkdel.length-1){
						sb.append("'").append(checkdel[i]).append("',");
					}else{
						sb.append("'").append(checkdel[i]).append("'");
					}
				}
				insuranceRules.setIds(sb.toString());
				insuranceList = insuranceRulesService.getInusrancaeRulesLisst(insuranceRules);
				result=Ajax.JSONResult(0, "选择成功！",insuranceList);
			}else{
				result=Ajax.JSONResult(1, "未选择保险规则！");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	
	/**添加车型保险关联关系
	 * @return
	 */
	public String saveCarInsurance(){
		try {
			//添加保险规则删除记录
			HttpSession session = ServletActionContext.getRequest().getSession();
			User user=(User) session.getAttribute("user");
			carInsuranceNew.setIsDel(0);
			carInsuranceNew.setCreatorId(user.getId());
			carInsuranceNew.setCreateTime(new Date());
			carInsuranceNewService.saveCarInsuranceNew(carInsuranceNew);
			//添加车型保险关联记录
			InsuranceOperationLog insuranceOperationLog = new InsuranceOperationLog();
			insuranceOperationLog.setType(1);
			insuranceOperationLog.setTime(new Date());
			insuranceOperationLog.setCreatorId(user.getId());
			insuranceOperationLog.setCreatorName(user.getName());
			insuranceOperationLog.setName(carModelName);
			insuranceOperationLog.setOperationContent("关联保险："+insuranceName);
			insuranceOperationLogService.saveInsuranceOperationLogDao(insuranceOperationLog);
			result=Ajax.JSONResult(0, "新增成功！");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**删除车型保险关联关系
	 * @return
	 */
	public String delCarModelInusrance(){
		try {
			List<CarInsuranceNew> list = carInsuranceNewService.getByParamList(carVehicleModel.getId());
			/*根据车型ID获取车辆信息*/
			List<Car> carList = carService.queryCarListByModelId(carVehicleModel.getId());
			/*判断车型下面车辆是否有在租借中的*/
//			if(carList != null){
//				for(int i=0;i<carList.size();i++){
//					Dict ydDict = DictUtil.getDictByCodes("carBizState", "1");
//					if(!carList.get(i).getBizState().equals(ydDict.getId())){
//						result=Ajax.JSONResult(1, "当前保险下有车辆在租借中，车牌号为："+carList.get(i).getVehiclePlateId());
//						return SUCCESS;
//					}
//				}
//			}
			if(list != null){
				for(int i=0;i<list.size();i++){
					CarInsuranceNew carInsurance = list.get(i);
					carInsurance.setIsDel(1);
					carInsuranceNewService.updateCarInsuranceNew(carInsurance);
				}
				result=Ajax.JSONResult(0, "删除完成！");
			}else{
				result=Ajax.JSONResult(0, "删除完成！");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	
	/**删除车型下关联的保险规则
	 * @return
	 */
	public String insuranceById(){
		try {
			carInsuranceNew = carInsuranceNewService.getById(carInsuranceNew.getId());
			if(carInsuranceNew != null){
				carInsuranceNew.setIsDel(1);
				carInsuranceNewService.updateCarInsuranceNew(carInsuranceNew);
				result=Ajax.JSONResult(0, "删除完成！");
			}else{
				result=Ajax.JSONResult(1, "未找到关联数据！");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	/**查看车型保险操作记录
	 * @return
	 */
	public String toViewCarInsuranceOperating(){
		try {
			insuranceOperationLogList = insuranceOperationLogService.getByParam(carModelName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public CarInsuranceNewAction() {
		super();
		carVehicleModel = new CarVehicleModel();
		page.setCountField("a.id");
		page.setCurrentPage(1);
	}
	public CarInsuranceNewService getCarInsuranceNewService() {
		return carInsuranceNewService;
	}

	public void setCarInsuranceNewService(
			CarInsuranceNewService carInsuranceNewService) {
		this.carInsuranceNewService = carInsuranceNewService;
	}

	public InsuranceRulesService getInsuranceRulesService() {
		return insuranceRulesService;
	}

	public void setInsuranceRulesService(InsuranceRulesService insuranceRulesService) {
		this.insuranceRulesService = insuranceRulesService;
	}

	public InsuranceDescService getInsuranceDescService() {
		return insuranceDescService;
	}

	public void setInsuranceDescService(InsuranceDescService insuranceDescService) {
		this.insuranceDescService = insuranceDescService;
	}

	public InsuranceOperationLogService getInsuranceOperationLogService() {
		return insuranceOperationLogService;
	}

	public void setInsuranceOperationLogService(
			InsuranceOperationLogService insuranceOperationLogService) {
		this.insuranceOperationLogService = insuranceOperationLogService;
	}

	public CarVehicleModelService getCarVehicleModelService() {
		return carVehicleModelService;
	}

	public void setCarVehicleModelService(
			CarVehicleModelService carVehicleModelService) {
		this.carVehicleModelService = carVehicleModelService;
	}

	public List<CarVehicleModel> getCarVehicleModelList() {
		return carVehicleModelList;
	}

	public void setCarVehicleModelList(List<CarVehicleModel> carVehicleModelList) {
		this.carVehicleModelList = carVehicleModelList;
	}

	public CarVehicleModel getCarVehicleModel() {
		return carVehicleModel;
	}

	public void setCarVehicleModel(CarVehicleModel carVehicleModel) {
		this.carVehicleModel = carVehicleModel;
	}

	public Page<CarVehicleModel> getPage() {
		return page;
	}

	public void setPage(Page<CarVehicleModel> page) {
		this.page = page;
	}

	public CarInsuranceNew getCarInsuranceNew() {
		return carInsuranceNew;
	}

	public void setCarInsuranceNew(CarInsuranceNew carInsuranceNew) {
		this.carInsuranceNew = carInsuranceNew;
	}

	public List<InsuranceRules> getInsuranceList() {
		return insuranceList;
	}

	public void setInsuranceList(List<InsuranceRules> insuranceList) {
		this.insuranceList = insuranceList;
	}

	public String[] getCheckdel() {
		return checkdel;
	}

	public void setCheckdel(String[] checkdel) {
		this.checkdel = checkdel;
	}

	public String getCarModelName() {
		return carModelName;
	}

	public void setCarModelName(String carModelName) {
		this.carModelName = carModelName;
	}

	public String getInsuranceName() {
		return insuranceName;
	}

	public void setInsuranceName(String insuranceName) {
		this.insuranceName = insuranceName;
	}

	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public List<CarInsuranceNew> getCarInsuranceNewList() {
		return carInsuranceNewList;
	}

	public void setCarInsuranceNewList(List<CarInsuranceNew> carInsuranceNewList) {
		this.carInsuranceNewList = carInsuranceNewList;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public List<InsuranceOperationLog> getInsuranceOperationLogList() {
		return insuranceOperationLogList;
	}

	public void setInsuranceOperationLogList(
			List<InsuranceOperationLog> insuranceOperationLogList) {
		this.insuranceOperationLogList = insuranceOperationLogList;
	}
	
}
