package com.dearho.cs.insuranceRules.dao.impl;

import java.util.List;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.pojo.CarVehicleModel;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.dao.InsuranceRulesDao;
import com.dearho.cs.insuranceRules.pojo.InsuranceDesc;
import com.dearho.cs.insuranceRules.pojo.InsuranceRules;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.util.StringHelper;

/**保险规则Dao接口层接口实现类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceRulesDaoImpl extends AbstractDaoSupport implements InsuranceRulesDao {

	@Override
	public Page<InsuranceRules> getInsuranceRulesPage(
			Page<InsuranceRules> page, InsuranceRules insuranceRules)
			throws Exception {
		StringBuffer hql = new StringBuffer("select a.id from InsuranceRules a where a.isDel = 0 ");
		if(StringHelper.isNotEmpty(insuranceRules.getId())){
			hql.append(" and a.id= '").append(insuranceRules.getId()).append("'");
		}
		hql.append(" order by a.createTime");
		Page<InsuranceRules> resultPage=pageCache(InsuranceRules.class, page, hql.toString());
		resultPage.setResults(idToObj(InsuranceRules.class, resultPage.getmResults()));
		if(resultPage.getResults()!=null){
			for (int i = 0; i < resultPage.getResults().size(); i++) {
				InsuranceRules ir = (InsuranceRules) resultPage.getResults().get(i);
				//根据获取保险基础规则信息
				String sql = "select a.id from InsuranceDesc a where a.insuranceId = '"+ir.getId()+"' order by a.seq";
				List<InsuranceDesc> insuranceDesc = getList(InsuranceDesc.class, queryFList(sql));
				ir.setInsuranceDescList(insuranceDesc);
			}
		}
		return resultPage;
	}

	@Override
	public InsuranceRules getById(String id) throws Exception {
		InsuranceRules insuranceRules = get(InsuranceRules.class, id);
		if(insuranceRules != null){
			String sql = "select a.id from InsuranceDesc a where a.insuranceId = '"+insuranceRules.getId()+"' order by a.seq";
			List<InsuranceDesc> insuranceDesc = getList(InsuranceDesc.class, queryFList(sql));
			insuranceRules.setInsuranceDescList(insuranceDesc);
		}
		return insuranceRules;
	}

	@Override
	public InsuranceRules getByParam(InsuranceRules insuranceRules)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveInsuranceRules(InsuranceRules insuranceRules)
			throws Exception {
		addEntity(insuranceRules);
	}

	@Override
	public void updateInsuranceRules(InsuranceRules insuranceRules)
			throws Exception {
		updateEntity(insuranceRules);
	}

	@Override
	public List<InsuranceRules> getInusrancaeRulesLisst(
			InsuranceRules insuranceRules) throws Exception {
//		String hql = "select a.id from InsuranceRules a where a.isDel = 0 order by a.createTime";
		StringBuffer sb = new StringBuffer("select a.id from InsuranceRules a where a.isDel = 0");
		if(StringHelper.isNotEmpty(insuranceRules.getIds())){
			sb.append(" and a.id in (").append(insuranceRules.getIds()).append(")");
		}
		sb.append(" order by a.createTime");
		List<InsuranceRules> list = getList(InsuranceRules.class, queryFList(sb.toString()));
		if(list != null){
			for(int i=0;i<list.size();i++){
				String sql = "select a.id from InsuranceDesc a where a.insuranceId = '"+list.get(i).getId()+"' order by a.seq";
				List<InsuranceDesc> insuranceDesc = getList(InsuranceDesc.class, queryFList(sql));
				list.get(i).setInsuranceDescList(insuranceDesc);
			}
		}
		return list;
	}

}
