package com.dearho.cs.insuranceRules.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.insuranceRules.dao.InsuranceDescDao;
import com.dearho.cs.insuranceRules.pojo.InsuranceDesc;
import com.dearho.cs.insuranceRules.pojo.InsuranceRules;

/**保险基础内容Dao层接口实现类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceDescDaoImpl extends AbstractDaoSupport implements InsuranceDescDao {

	@Override
	public List<InsuranceDesc> getByInsuranceId(String id) throws Exception {
		String hql = "select a.id from InsuranceDesc a where a.insuranceId = '"+id+"'";
		return getList(InsuranceDesc.class, queryFList(hql));
	}

	@Override
	public void saveInsuranceDesc(InsuranceDesc insuranceDesc) throws Exception {
		addEntity(insuranceDesc);
	}

	@Override
	public void updateInsuranceDesc(InsuranceDesc insuranceDesc)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void delInsuranceDesc(String[] ids) throws Exception {
		final String queryString="delete InsuranceDesc where id in (:ids)";
		deleteEntity(InsuranceDesc.class, queryString, ids);
	}

}
