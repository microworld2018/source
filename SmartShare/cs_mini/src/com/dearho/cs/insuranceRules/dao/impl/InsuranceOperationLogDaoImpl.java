package com.dearho.cs.insuranceRules.dao.impl;

import java.util.List;

import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.insuranceRules.dao.InsuranceOperationLogDao;
import com.dearho.cs.insuranceRules.pojo.InsuranceOperationLog;

/**保险操作记录Dao接口层实现类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceOperationLogDaoImpl extends AbstractDaoSupport implements InsuranceOperationLogDao {

	@Override
	public void saveInsuranceOperationLogDao(InsuranceOperationLog insuranceOperationLog) throws Exception {
		addEntity(insuranceOperationLog);
	}

	@Override
	public List<InsuranceOperationLog> getByParam(String name) throws Exception {
		String hql = "select a.id from InsuranceOperationLog a where a.name = '"+name+"' order by a.time desc";
		return getList(InsuranceOperationLog.class, queryFList(hql));
	}

}
