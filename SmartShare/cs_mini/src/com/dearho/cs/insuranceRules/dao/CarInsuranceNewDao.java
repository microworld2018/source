package com.dearho.cs.insuranceRules.dao;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.pojo.CarInsuranceNew;

/**车型保险关联Dao层接口
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public interface CarInsuranceNewDao {
	
	/**根据条件分页获取车型保险关联列表
	 * @param page
	 * @param carInsuranceNew
	 * @return
	 * @throws Exception
	 */
	public Page<CarInsuranceNew> getByParamPage(Page<CarInsuranceNew> page,CarInsuranceNew carInsuranceNew)throws Exception;
	
	/**根据ID获取车型保险关联关系
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public CarInsuranceNew getById(String id)throws Exception;
	
	/**根据条件获取车型保险关联集合
	 * @param carInsuranceNew
	 * @return
	 * @throws Exception
	 */
	public List<CarInsuranceNew> getByParamList(String carModelId)throws Exception;
	
	/**添加车型保险关联关系
	 * @param carInsuranceNew
	 * @throws Exception
	 */
	public void saveCarInsuranceNew(CarInsuranceNew carInsuranceNew)throws Exception;
	
	/**更新车型保险关联关系
	 * @param carInsuranceNew
	 * @throws Exception
	 */
	public void updateCarInsuranceNew(CarInsuranceNew carInsuranceNew)throws Exception;
	

	public String getInsuranceStrByCarId(String carId);
	
}
