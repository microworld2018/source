package com.dearho.cs.insuranceRules.dao;

import java.util.List;

import com.dearho.cs.insuranceRules.pojo.InsuranceOperationLog;

/**保险操作记录Dao接口层
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public interface InsuranceOperationLogDao {
	
	/**添加保险操作记录
	 * @param insuranceOperationLogDao
	 * @throws Exception
	 */
	public void saveInsuranceOperationLogDao(InsuranceOperationLog insuranceOperationLog)throws Exception;
	
	/**根据条件获取操作记录
	 * @param insuranceOperationLogDao
	 * @return
	 * @throws Exception
	 */
	public List<InsuranceOperationLog> getByParam(String name) throws Exception;

}
