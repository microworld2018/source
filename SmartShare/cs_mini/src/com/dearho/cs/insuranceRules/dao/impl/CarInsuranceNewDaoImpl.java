package com.dearho.cs.insuranceRules.dao.impl;

import java.math.BigDecimal;
import java.util.List;

import com.dearho.cs.car.pojo.CarVehicleModel;
import com.dearho.cs.core.db.AbstractDaoSupport;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.dao.CarInsuranceNewDao;
import com.dearho.cs.insuranceRules.pojo.CarInsuranceNew;

/**车型保险关联Dao层接口实现类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class CarInsuranceNewDaoImpl extends AbstractDaoSupport implements CarInsuranceNewDao {

	@Override
	public Page<CarInsuranceNew> getByParamPage(Page<CarInsuranceNew> page,
			CarInsuranceNew carInsuranceNew) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarInsuranceNew getById(String id) throws Exception {
		return get(CarInsuranceNew.class, id);
	}

	@Override
	public void saveCarInsuranceNew(CarInsuranceNew carInsuranceNew)
			throws Exception {
		addEntity(carInsuranceNew);
	}

	@Override
	public void updateCarInsuranceNew(CarInsuranceNew carInsuranceNew)
			throws Exception {
		updateEntity(carInsuranceNew);
	}

	@Override
	public List<CarInsuranceNew> getByParamList(String carModelId)
			throws Exception {
		String hql = "select a.id from CarInsuranceNew a where a.isDel = 0 and a.carTypeId = '"+carModelId+"' order by a.createTime desc";
//		String hql = "select a.id from CarInsuranceNew a where a.isDel = 0 and a.carTypeId = '"+carModelId+"' ";
		return getList(CarInsuranceNew.class, queryFList(hql));
	}

	@Override
	public String getInsuranceStrByCarId(String carId) {
		StringBuffer result = new StringBuffer();
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT c2.amount, c3.`name`");
		sql.append(" FROM c_car c,c_car_insurance c2,c_insurance_rules c3");
		sql.append(" WHERE c.model_id = c2.car_type_id AND c2.insurance_id = c3.id");
		sql.append(" AND c.id='" + carId + "'");

		List list = queryFList(sql.toString(), true);
		String amount = null;
		String name = null;
		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				Object[] m = (Object[]) list.get(i);
				amount = m[0] == null ? "" : ((BigDecimal) m[0]).toPlainString();
				name = m[1] == null ? "" : m[1].toString();
				result.append(name).append("：").append(amount);//.append("\n； ");//<br />");
				if (i != (list.size() - 1)) {
					result.append("\n； ");
				}
			}
		}
		return result.toString();
	}	

}
