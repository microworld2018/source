package com.dearho.cs.insuranceRules.service;

import java.util.List;

import com.dearho.cs.insuranceRules.pojo.InsuranceDesc;

/**保险基础内容Service层接口
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public interface InsuranceDescService {
	
	/**根据保险ID获取基础内容
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public List<InsuranceDesc> getByInsuranceId(String id)throws Exception;
	
	/**添加保险基础内容
	 * @param insuranceDesc
	 * @throws Exception
	 */
	public void saveInsuranceDesc(InsuranceDesc insuranceDesc) throws Exception;
	
	/**更新保险基础内容
	 * @param insuranceDesc
	 * @throws Exception
	 */
	public void updateInsuranceDesc(InsuranceDesc insuranceDesc)throws Exception;
	
	/**删除保险基础内容
	 * @param insuranceDesc
	 * @throws Exception
	 */
	public void delInsuranceDesc(String[] ids)throws Exception ;
	
}
