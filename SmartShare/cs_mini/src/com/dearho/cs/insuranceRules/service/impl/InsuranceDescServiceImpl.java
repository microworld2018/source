package com.dearho.cs.insuranceRules.service.impl;

import java.util.List;

import com.dearho.cs.insuranceRules.dao.InsuranceDescDao;
import com.dearho.cs.insuranceRules.pojo.InsuranceDesc;
import com.dearho.cs.insuranceRules.service.InsuranceDescService;

/**保险基础内容Service层接口实现类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceDescServiceImpl implements InsuranceDescService {
	
	private InsuranceDescDao insuranceDescDao;
	
	public InsuranceDescDao getInsuranceDescDao() {
		return insuranceDescDao;
	}

	public void setInsuranceDescDao(InsuranceDescDao insuranceDescDao) {
		this.insuranceDescDao = insuranceDescDao;
	}

	@Override
	public List<InsuranceDesc> getByInsuranceId(String id) throws Exception {
		return insuranceDescDao.getByInsuranceId(id);
	}

	@Override
	public void saveInsuranceDesc(InsuranceDesc insuranceDesc) throws Exception {
		insuranceDescDao.saveInsuranceDesc(insuranceDesc);
	}

	@Override
	public void updateInsuranceDesc(InsuranceDesc insuranceDesc)
			throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void delInsuranceDesc(String[] ids) throws Exception {
		insuranceDescDao.delInsuranceDesc(ids);
	}

}
