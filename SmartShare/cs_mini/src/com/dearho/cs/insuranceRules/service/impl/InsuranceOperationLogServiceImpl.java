package com.dearho.cs.insuranceRules.service.impl;

import java.util.List;

import com.dearho.cs.insuranceRules.dao.InsuranceOperationLogDao;
import com.dearho.cs.insuranceRules.pojo.InsuranceOperationLog;
import com.dearho.cs.insuranceRules.service.InsuranceOperationLogService;

/**保险操作记录Service接口层实现类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceOperationLogServiceImpl implements InsuranceOperationLogService {
	
	private InsuranceOperationLogDao insuranceOperationLogDao;

	public InsuranceOperationLogDao getInsuranceOperationLogDao() {
		return insuranceOperationLogDao;
	}

	public void setInsuranceOperationLogDao(
			InsuranceOperationLogDao insuranceOperationLogDao) {
		this.insuranceOperationLogDao = insuranceOperationLogDao;
	}

	@Override
	public void saveInsuranceOperationLogDao(InsuranceOperationLog insuranceOperationLog) throws Exception {
		insuranceOperationLogDao.saveInsuranceOperationLogDao(insuranceOperationLog);
	}

	@Override
	public List<InsuranceOperationLog> getByParam(String name) throws Exception {
		return insuranceOperationLogDao.getByParam(name);
	}

}
