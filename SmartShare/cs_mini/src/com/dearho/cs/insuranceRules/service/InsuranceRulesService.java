package com.dearho.cs.insuranceRules.service;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.pojo.InsuranceRules;

/**保险规则Service接口层
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public interface InsuranceRulesService {
	/**根据条件分页获取保险规则
	 * @param page
	 * @param insuranceRules
	 * @return
	 * @throws Exception
	 */
	public Page<InsuranceRules> getInsuranceRulesPage(Page<InsuranceRules> page,InsuranceRules insuranceRules)throws Exception;
	
	/**根据保险规则ID获取保险规则详情
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public InsuranceRules getById(String id) throws Exception;
	
	/**根据条件获取单挑保险规则详情
	 * @param insuranceRules
	 * @return
	 * @throws Exception
	 */
	public InsuranceRules getByParam(InsuranceRules insuranceRules)throws Exception;
	
	/**添加保险规则
	 * @param insuranceRules
	 * @throws Exception
	 */
	public void saveInsuranceRules(InsuranceRules insuranceRules)throws Exception;
	
	/**更新保险规则
	 * @param insuranceRules
	 * @throws Exception
	 */
	public void updateInsuranceRules(InsuranceRules insuranceRules)throws Exception;
	
	
	/**根据条件获取保险规则列表
	 * @param insuranceRules
	 * @return
	 * @throws Exception
	 */
	public List<InsuranceRules> getInusrancaeRulesLisst(InsuranceRules insuranceRules)throws Exception;

}
