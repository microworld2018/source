package com.dearho.cs.insuranceRules.service.impl;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.dao.InsuranceRulesDao;
import com.dearho.cs.insuranceRules.pojo.InsuranceRules;
import com.dearho.cs.insuranceRules.service.InsuranceRulesService;

/**保险规则Service接口层实现类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceRulesServiceImpl implements InsuranceRulesService {
	
	private InsuranceRulesDao insuranceRulesDao;
	
	public InsuranceRulesDao getInsuranceRulesDao() {
		return insuranceRulesDao;
	}

	public void setInsuranceRulesDao(InsuranceRulesDao insuranceRulesDao) {
		this.insuranceRulesDao = insuranceRulesDao;
	}

	@Override
	public Page<InsuranceRules> getInsuranceRulesPage(
			Page<InsuranceRules> page, InsuranceRules insuranceRules)
			throws Exception {
		return insuranceRulesDao.getInsuranceRulesPage(page, insuranceRules);
	}

	@Override
	public InsuranceRules getById(String id) throws Exception {
		return insuranceRulesDao.getById(id);
	}

	@Override
	public InsuranceRules getByParam(InsuranceRules insuranceRules)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveInsuranceRules(InsuranceRules insuranceRules)
			throws Exception {
		insuranceRulesDao.saveInsuranceRules(insuranceRules);
	}

	@Override
	public void updateInsuranceRules(InsuranceRules insuranceRules)
			throws Exception {
		insuranceRulesDao.updateInsuranceRules(insuranceRules);
	}

	@Override
	public List<InsuranceRules> getInusrancaeRulesLisst(
			InsuranceRules insuranceRules) throws Exception {
		// TODO Auto-generated method stub
		return insuranceRulesDao.getInusrancaeRulesLisst(insuranceRules);
	}

}
