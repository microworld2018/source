package com.dearho.cs.insuranceRules.service.impl;

import java.util.List;

import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.insuranceRules.dao.CarInsuranceNewDao;
import com.dearho.cs.insuranceRules.pojo.CarInsuranceNew;
import com.dearho.cs.insuranceRules.service.CarInsuranceNewService;

/**车型保险关联Service层接口实现类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class CarInsuranceNewServiceImpl implements CarInsuranceNewService {
	
	private CarInsuranceNewDao carInsuranceNewDao;
	
	public CarInsuranceNewDao getCarInsuranceNewDao() {
		return carInsuranceNewDao;
	}

	public void setCarInsuranceNewDao(CarInsuranceNewDao carInsuranceNewDao) {
		this.carInsuranceNewDao = carInsuranceNewDao;
	}

	@Override
	public Page<CarInsuranceNew> getByParamPage(Page<CarInsuranceNew> page,
			CarInsuranceNew carInsuranceNew) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CarInsuranceNew getById(String id) throws Exception {
		return carInsuranceNewDao.getById(id);
	}

	@Override
	public void saveCarInsuranceNew(CarInsuranceNew carInsuranceNew)
			throws Exception {
		carInsuranceNewDao.saveCarInsuranceNew(carInsuranceNew);
	}

	@Override
	public void updateCarInsuranceNew(CarInsuranceNew carInsuranceNew)
			throws Exception {
		carInsuranceNewDao.updateCarInsuranceNew(carInsuranceNew);
	}

	@Override
	public List<CarInsuranceNew> getByParamList(String carModelId)
			throws Exception {
		return carInsuranceNewDao.getByParamList(carModelId);
	}

	@Override
	public String getInsuranceStrByCarId(String carId) {
		// TODO Auto-generated method stub
		return carInsuranceNewDao.getInsuranceStrByCarId(carId);
	}

}
