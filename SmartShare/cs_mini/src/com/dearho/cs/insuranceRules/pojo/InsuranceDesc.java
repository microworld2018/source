package com.dearho.cs.insuranceRules.pojo;

import java.io.Serializable;
import java.util.Date;

/**保险基础内容详情实体类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceDesc implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 保险基础内容ID
	 */
	private String id;
	
	/**
	 * 保险ID
	 */
	private String insuranceId;
	
	/**
	 * 基础内容
	 */
	private String content;
	
	/**
	 * 序号
	 */
	private Integer seq;
	
	/**
	 * 创建人ID
	 */
	private String creatorId;
	
	/**
	 * 创建时间
	 */
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getInsuranceId() {
		return insuranceId;
	}

	public void setInsuranceId(String insuranceId) {
		this.insuranceId = insuranceId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
