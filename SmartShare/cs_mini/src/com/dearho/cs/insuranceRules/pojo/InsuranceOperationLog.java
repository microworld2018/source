package com.dearho.cs.insuranceRules.pojo;

import java.io.Serializable;
import java.util.Date;

/**保险操作记录表
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceOperationLog implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 保险操作记录ID
	 */
	private String id;
	
	/**
	 * 操作名称
	 */
	private String name;
	
	/**
	 * 操作类型0:保险规则1:车型保险
	 */
	private Integer type;
	
	/**
	 * 操作时间
	 */
	private Date time;
	
	/**
	 * 创建人ID
	 */
	private String creatorId;
	
	/**
	 * 物理地址
	 */
	private String macAddress;
	
	/**
	 * IP地址
	 */
	private String ipAddress;
	
	/**
	 * 操作内容
	 */
	private String operationContent;
	
	/**
	 * 操作人名称
	 */
	private String creatorName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getOperationContent() {
		return operationContent;
	}

	public void setOperationContent(String operationContent) {
		this.operationContent = operationContent;
	}

	public String getCreatorName() {
		return creatorName;
	}

	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	
}
