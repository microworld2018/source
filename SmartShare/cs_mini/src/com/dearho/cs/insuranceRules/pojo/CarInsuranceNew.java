package com.dearho.cs.insuranceRules.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**车型保险关联表
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class CarInsuranceNew implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/**
	 * 车型保险关联ID
	 */
	private String id;
	
	/**
	 * 车型ID
	 */
	private String carTypeId;
	
	/**
	 * 保险ID
	 */
	private String insuranceId;
	
	/**
	 * 金额
	 */
	private BigDecimal amount;
	
	/**
	 * 是否使用
	 */
	private Integer isUsed;
	
	/**
	 * 创建人ID
	 */
	private String creatorId;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 是否删除
	 */
	private Integer isDel;
	
	/**
	 * 保险规则实体类
	 */
	private InsuranceRules insuranceRules;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCarTypeId() {
		return carTypeId;
	}

	public void setCarTypeId(String carTypeId) {
		this.carTypeId = carTypeId;
	}

	public String getInsuranceId() {
		return insuranceId;
	}

	public void setInsuranceId(String insuranceId) {
		this.insuranceId = insuranceId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Integer getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Integer isUsed) {
		this.isUsed = isUsed;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getIsDel() {
		return isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	public InsuranceRules getInsuranceRules() {
		return insuranceRules;
	}

	public void setInsuranceRules(InsuranceRules insuranceRules) {
		this.insuranceRules = insuranceRules;
	}

}
