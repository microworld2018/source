package com.dearho.cs.insuranceRules.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**保险规则实体类
 * @author Gaopl
 * @createDate 2017年5月3日
 */
public class InsuranceRules implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 保险规则ID
	 */
	private String id;
	
	/**
	 * 保险规则名称
	 */
	private String name;
	
	/**
	 * 保险细则
	 */
	private String insuranceDesc;
	
	/**
	 * 创建人ID
	 */
	private String creatorId;
	
	/**
	 * 创建时间
	 */
	private Date createTime;
	
	/**
	 * 最后一次更新时间
	 */
	private Date updateTime;
	
	/**
	 * 是否删除0:未删除 1:已删除
	 */
	private Integer isDel;
	
	/**
	 * 保险基础内容集合
	 */
	private List<InsuranceDesc> insuranceDescList;
	
	/**
	 * 临时字段保险id集合
	 */
	private String ids;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInsuranceDesc() {
		return insuranceDesc;
	}

	public void setInsuranceDesc(String insuranceDesc) {
		this.insuranceDesc = insuranceDesc;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getIsDel() {
		return isDel;
	}

	public void setIsDel(Integer isDel) {
		this.isDel = isDel;
	}

	public List<InsuranceDesc> getInsuranceDescList() {
		return insuranceDescList;
	}

	public void setInsuranceDescList(List<InsuranceDesc> insuranceDescList) {
		this.insuranceDescList = insuranceDescList;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}
	
}
