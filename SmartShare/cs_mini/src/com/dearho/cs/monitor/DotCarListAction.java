package com.dearho.cs.monitor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.device.pojo.DeviceBinding;
import com.dearho.cs.device.service.DeviceBindingService;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.util.Ajax;
import com.dearho.cs.util.HttpRequestUtil;
import com.dearho.cs.util.JsonTools;
import com.dearho.cs.util.PropertiesHelper;

/**查询网点下车辆信息
 * @author Gaopl
 * @date 2017-01-05
 */
public class DotCarListAction  extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	private CarService carService;
	
	private Page<Car> page = new Page<Car>();
	
	private BranchDot branchDot;
	
	private String dotId;
	
	private Car car;
	
	private DeviceBindingService deviceBindingService;
	
	private DeviceBinding  deviceBinding;
	
	private String carLoction;
	
	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}

	public Page<Car> getPage() {
		return page;
	}

	public void setPage(Page<Car> page) {
		this.page = page;
	}

	public BranchDot getBranchDot() {
		return branchDot;
	}

	public void setBranchDot(BranchDot branchDot) {
		this.branchDot = branchDot;
	}
	
	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}
	
	public DeviceBindingService getDeviceBindingService() {
		return deviceBindingService;
	}

	public void setDeviceBindingService(DeviceBindingService deviceBindingService) {
		this.deviceBindingService = deviceBindingService;
	}

	public DeviceBinding getDeviceBinding() {
		return deviceBinding;
	}

	public void setDeviceBinding(DeviceBinding deviceBinding) {
		this.deviceBinding = deviceBinding;
	}
	
	public String getCarLoction() {
		return carLoction;
	}

	public void setCarLoction(String carLoction) {
		this.carLoction = carLoction;
	}
	
	public String getDotId() {
		return dotId;
	}

	public void setDotId(String dotId) {
		this.dotId = dotId;
	}

	public DotCarListAction() {
		super();
		car = new Car();
		page.setCountField("a.id");
		page.setCurrentPage(1);
	}

	@Override
	public String process() {
		//获取当前网点车辆
		page = carService.queryCarByPageDot(page, car,dotId);
		return SUCCESS;
	}
	
	
	/**获取车辆当前位置
	 * @return
	 */
	public String opThisLocation(){
		System.out.println(car.getId());
		List<DeviceBinding> bindingList = deviceBindingService.queryBindByCarId(car.getId(), 1);
		if(bindingList != null && bindingList.size() > 0){

			deviceBinding = bindingList.get(0);
			String url = PropertiesHelper.getValue("msg_url")+"/car/getGPSInfo";
			Map<String,String> paramMap = new HashMap<String, String>();
			paramMap.put("sn", deviceBinding.getDeviceNo());//设备编号
			String longitude = null;
			String latitude = null;
			String content = HttpRequestUtil.getPostMethod(url, paramMap);
			JSONObject obj = JSONObject.fromObject(content);
			if(obj.getString("data") != null && !"".equals(obj.getString("data"))){
				JSONArray array = JSONArray.fromObject(obj.getString("data"));
				if(!array.isEmpty()){
					for(int i = 0; i < array.size();i++){
						JSONObject job = (JSONObject)array.get(i);
						longitude = job.getString("longitude");
						latitude = job.getString("latitude");
					}
				}
			}
			
			String apiContents = HttpRequestUtil.getPostMethod("http://api.map.baidu.com/geoconv/v1/?coords="+longitude+","+latitude+"&from=1&to=5&output=json&ak=BtxULUWSmvG50D5GKe0ka9Yk",null);
			
			JSONObject appObj = JSONObject.fromObject(apiContents);
			
			JSONArray ary =JSONArray.fromObject(appObj.getString("result"));
			
			if(ary != null && ary.size() > 0){
				for(int j  = 0; j<ary.size();j++){
					JSONObject o = (JSONObject)ary.get(j);
					
					longitude = o.getString("x");
					latitude =o.getString("y");
				}
			}
			
			getRequest().setAttribute("latitude", latitude);
			getRequest().setAttribute("longitude", longitude);
			
			
		}else{
			
			getRequest().setAttribute("carLoction", car.getId());
			
			return SUCCESS;
		}
		
		return SUCCESS;
	}

}
