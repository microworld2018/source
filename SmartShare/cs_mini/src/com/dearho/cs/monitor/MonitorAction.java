package com.dearho.cs.monitor;

import java.util.List;

import com.dearho.cs.car.pojo.Car;
import com.dearho.cs.car.service.CarService;
import com.dearho.cs.core.db.page.Page;
import com.dearho.cs.orders.pojo.BookCarInfoEntity;
import com.dearho.cs.place.pojo.BranchDot;
import com.dearho.cs.place.pojo.CarDotBinding;
import com.dearho.cs.place.service.BranchDotService;
import com.dearho.cs.place.service.CarDotBindingService;
import com.dearho.cs.report.service.IndexDataShowService;
import com.dearho.cs.sys.action.AbstractAction;
import com.dearho.cs.sys.pojo.AdministrativeArea;
import com.dearho.cs.sys.pojo.Dict;
import com.dearho.cs.sys.service.AreaService;
import com.dearho.cs.sys.util.DictUtil;
import com.dearho.cs.util.Ajax;

/**监控中心首页
 * @author Gaopl
 * @date 2017-01-03
 */
public class MonitorAction extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	private IndexDataShowService indexDataShowService;
	
	private BranchDotService branchDotService;
	
	
	private CarDotBindingService carDotBindingService;
	
	private CarService carService;
	
	private BranchDot branchDot;
	
	private Page<BranchDot> page = new Page<BranchDot>();
	
	private Page<Car> page1 = new Page<Car>();
	
	private Car car;
	
	private AreaService areaService;//行政区划service
	
	@SuppressWarnings("unused")
	private Integer allCarNumber;
	
	@SuppressWarnings("unused")
	private Integer leaseNumber;
	

	public IndexDataShowService getIndexDataShowService() {
		return indexDataShowService;
	}

	public void setIndexDataShowService(IndexDataShowService indexDataShowService) {
		this.indexDataShowService = indexDataShowService;
	}

	public BranchDot getBranchDot() {
		return branchDot;
	}

	public void setBranchDot(BranchDot branchDot) {
		this.branchDot = branchDot;
	}

	public Page<BranchDot> getPage() {
		return page;
	}

	public void setPage(Page<BranchDot> page) {
		this.page = page;
	}

	public BranchDotService getBranchDotService() {
		return branchDotService;
	}

	public void setBranchDotService(BranchDotService branchDotService) {
		this.branchDotService = branchDotService;
	}

	public Integer getAllCarNumber() {
		return allCarNumber = indexDataShowService.getCarCount(0);
	}

	public void setAllCarNumber(Integer allCarNumber) {
		this.allCarNumber = allCarNumber;
	}

	public Integer getLeaseNumber() {
		return leaseNumber = indexDataShowService.getCarCount(12);
	}

	public void setLeaseNumber(Integer leaseNumber) {
		this.leaseNumber = leaseNumber;
	}
	
	public CarDotBindingService getCarDotBindingService() {
		return carDotBindingService;
	}

	public void setCarDotBindingService(CarDotBindingService carDotBindingService) {
		this.carDotBindingService = carDotBindingService;
	}
	
	public CarService getCarService() {
		return carService;
	}

	public void setCarService(CarService carService) {
		this.carService = carService;
	}
	
	public Page<Car> getPage1() {
		return page1;
	}

	public void setPage1(Page<Car> page1) {
		this.page1 = page1;
	}

	public Car getCar() {
		return car;
	}

	public void setCar(Car car) {
		this.car = car;
	}

	public AreaService getAreaService() {
		return areaService;
	}

	public void setAreaService(AreaService areaService) {
		this.areaService = areaService;
	}

	public MonitorAction() {
		super();
		branchDot = new BranchDot();
		page.setCountField("a.id");
		page.setCurrentPage(1);
		
		car = new Car();
		page1.setCountField("a.id");
		page1.setCurrentPage(1);
		page1.setPageSize(1000);
	}

	@Override
	public String process() {
		page = branchDotService.searchBranchDot(page, branchDot);
		if(page.getResults() == null){
			return SUCCESS;
		}
		for(int i =0; i< page.getResults().size(); i++){
			BranchDot branchDot1 = (BranchDot) page.getResults().get(i);
			/*Integer n = branchDotService.getCarCount(branchDot1.getId());
			page1 = carService.queryCarByPageDot(page1, car, branchDot1.getId());
			Integer count = 0;
			if(page1.getResults() != null){
				count = page1.getResults().size();
			}
			branchDot1.setCarCount(count);
			List<BookCarInfoEntity> no = branchDotService.getUnbookCars(branchDot1.getId());
			branchDot1.setNoUse(count - n);*/
			Integer count = branchDotService.getList(branchDot1.getId(), null);
			branchDot1.setCarCount(count);
			
			//停止使用的车辆
			Dict NoUse1 = DictUtil.getDictByCodes("carBizState", "3");
			Integer NoUse = branchDotService.getList(branchDot1.getId(), NoUse1.getId());
			branchDot1.setNoUse(NoUse);
			//租借中的车辆
			Dict onLoan1 = DictUtil.getDictByCodes("carBizState", "1");//租借中车辆
			Integer onLoan = branchDotService.getList(branchDot1.getId(), onLoan1.getId());
			branchDot1.setOnLoan(onLoan);
		}
		return SUCCESS;
	}
	
	
	/**查看当前网点车辆
	 * @return
	 */
	public String opCarList(){
		List<CarDotBinding> list = carDotBindingService.searchBindingByDotId(branchDot.getId(), null);
		getRequest().setAttribute("dotCar", list);
		return SUCCESS;
	}
	
	
	/**获取所有网点
	 * @return
	 */
	public String queryDotList(){
		String hql = "from BranchDot";
		List<BranchDot> list = branchDotService.find(hql);
		System.out.println(list);
		result=Ajax.JSONResult(0, "查询完成！",list);
		return SUCCESS;
	}
	
	
	public String getArrayList(){
		List<AdministrativeArea> list = areaService.searchAreaByCode(null);
		System.out.println(list);
		result=Ajax.JSONResult(0, "查询完成！",list);
		return SUCCESS;
	}

}
