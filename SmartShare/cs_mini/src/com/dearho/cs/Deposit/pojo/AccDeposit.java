package com.dearho.cs.Deposit.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**押金实体类
 * @author Gaopl
 *
 */
public class AccDeposit  implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 会员ID
	 */
	private String subscriberId;
	
	
	/**
	 * 押金金额
	 */
	private BigDecimal amount;
	
	
	/**
	 * 冻结金额
	 */
	private BigDecimal frozenAmount;
	
	
	/**
	 * 可用金额
	 */
	private BigDecimal usableAmount;
	
	
	/**
	 * 是否退款0.使用中1.申请退款2.退款中3.退款中断4.已退款
	 */
	private Integer isRefund;
	
	
	/**
	 * 最后操作时间
	 */
	private Date lastOperateTime;
	
	
	/**
	 * 最后操作总金额
	 */
	private BigDecimal lastOperateAmount;
	
	
	/**
	 * 最后操作类型
	 */
	private Integer lastOperateType;
	
	/**
	 * 创建时间
	 */
	private Date ts;


	public String getSubscriberId() {
		return subscriberId;
	}


	public void setSubscriberId(String subscriberId) {
		this.subscriberId = subscriberId;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public BigDecimal getFrozenAmount() {
		return frozenAmount;
	}


	public void setFrozenAmount(BigDecimal frozenAmount) {
		this.frozenAmount = frozenAmount;
	}


	public BigDecimal getUsableAmount() {
		return usableAmount;
	}


	public void setUsableAmount(BigDecimal usableAmount) {
		this.usableAmount = usableAmount;
	}


	public Integer getIsRefund() {
		return isRefund;
	}


	public void setIsRefund(Integer isRefund) {
		this.isRefund = isRefund;
	}


	public Date getLastOperateTime() {
		return lastOperateTime;
	}


	public void setLastOperateTime(Date lastOperateTime) {
		this.lastOperateTime = lastOperateTime;
	}


	public BigDecimal getLastOperateAmount() {
		return lastOperateAmount;
	}


	public void setLastOperateAmount(BigDecimal lastOperateAmount) {
		this.lastOperateAmount = lastOperateAmount;
	}


	public Integer getLastOperateType() {
		return lastOperateType;
	}


	public void setLastOperateType(Integer lastOperateType) {
		this.lastOperateType = lastOperateType;
	}


	public Date getTs() {
		return ts;
	}


	public void setTs(Date ts) {
		this.ts = ts;
	}

}
