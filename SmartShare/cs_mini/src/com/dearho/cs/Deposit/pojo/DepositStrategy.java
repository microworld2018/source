package com.dearho.cs.Deposit.pojo;

import java.io.Serializable;
import java.util.Date;

/**押金规则实体类
 * @author Gaopl
 *
 */
public class DepositStrategy implements Serializable{

	private static final long serialVersionUID = 1L;
	
	/**
	 * 规则ID
	 */
	private String id;
	
	/**
	 * 规则类型
	 */
	private Integer type;
	
	/**
	 * 是否有效
	 */
	private Integer isActive;
	
	/**
	 * 规则描述
	 */
	private String desc;
	
	/**
	 * 押金金额
	 */
	private Double Money;
	
	/**
	 * 创建人ID
	 */
	private String creatorId;
	
	/**
	 * 创建时间
	 */
	private Date createTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Double getMoney() {
		return Money;
	}

	public void setMoney(Double money) {
		Money = money;
	}

	public String getCreatorId() {
		return creatorId;
	}

	public void setCreatorId(String creatorId) {
		this.creatorId = creatorId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	

}
