package com.dearho.cs.Deposit.service;

import java.util.List;

import com.dearho.cs.Deposit.pojo.AccDeposit;

/**押金Service接口
 * @author Gaopl
 *
 */
public interface AccDepositService {
	/**根据条件获取押金列表
	 * @param accDeposit
	 * @return
	 */
	public List<AccDeposit> getDepositList(AccDeposit accDeposit);
	
	/**根据条件获单条押金数据
	 * @param accDeposit
	 * @return
	 */
	public AccDeposit getDeposit(AccDeposit accDeposit);
	
	
	/**更新押金数据
	 * @param accDeposit
	 */
	public void updateDeposit(AccDeposit accDeposit);

}
