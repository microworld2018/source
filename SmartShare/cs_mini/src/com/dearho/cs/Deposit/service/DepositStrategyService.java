package com.dearho.cs.Deposit.service;

import com.dearho.cs.Deposit.pojo.DepositStrategy;

/**押金规则Service接口
 * @author Gaopl
 *
 */
public interface DepositStrategyService {
	
	/**根据条件获取押金规则
	 * @param depositStrategy
	 * @return
	 */
	public DepositStrategy getByParam(DepositStrategy depositStrategy);
}
