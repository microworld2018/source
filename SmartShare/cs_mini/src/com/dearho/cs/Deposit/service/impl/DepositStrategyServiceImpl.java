package com.dearho.cs.Deposit.service.impl;

import com.dearho.cs.Deposit.dao.DepositStrategyDao;
import com.dearho.cs.Deposit.pojo.DepositStrategy;
import com.dearho.cs.Deposit.service.DepositStrategyService;

public class DepositStrategyServiceImpl implements DepositStrategyService {
	
	private DepositStrategyDao depositStrategyDao;

	@Override
	public DepositStrategy getByParam(DepositStrategy depositStrategy) {
		return depositStrategyDao.getByParam(depositStrategy);
	}

	public DepositStrategyDao getDepositStrategyDao() {
		return depositStrategyDao;
	}

	public void setDepositStrategyDao(DepositStrategyDao depositStrategyDao) {
		this.depositStrategyDao = depositStrategyDao;
	}
	
}
