package com.dearho.cs.Deposit.service.impl;

import java.util.List;

import com.dearho.cs.Deposit.dao.AccDepositDao;
import com.dearho.cs.Deposit.pojo.AccDeposit;
import com.dearho.cs.Deposit.service.AccDepositService;

public class AccDepositServiceImpl implements AccDepositService {
	
	private AccDepositDao accDepositDao;
	public AccDepositDao getAccDepositDao() {
		return accDepositDao;
	}
	public void setAccDepositDao(AccDepositDao accDepositDao) {
		this.accDepositDao = accDepositDao;
	}
	@Override
	public List<AccDeposit> getDepositList(AccDeposit accDeposit) {
		return accDepositDao.getDepositList(accDeposit);
	}
	@Override
	public AccDeposit getDeposit(AccDeposit accDeposit) {
		return accDepositDao.getDeposit(accDeposit);
	}
	@Override
	public void updateDeposit(AccDeposit accDeposit) {
		accDepositDao.updateDeposit(accDeposit);
	}
	
	

}
