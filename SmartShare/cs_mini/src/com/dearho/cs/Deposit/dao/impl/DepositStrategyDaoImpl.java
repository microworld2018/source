package com.dearho.cs.Deposit.dao.impl;

import java.util.List;

import com.dearho.cs.Deposit.dao.DepositStrategyDao;
import com.dearho.cs.Deposit.pojo.AccDeposit;
import com.dearho.cs.Deposit.pojo.DepositStrategy;
import com.dearho.cs.core.db.AbstractDaoSupport;

public class DepositStrategyDaoImpl extends AbstractDaoSupport implements DepositStrategyDao {

	@Override
	public DepositStrategy getByParam(DepositStrategy depositStrategy) {
		StringBuffer hql=new StringBuffer();
		hql.append("select a.id from DepositStrategy a");
		List<DepositStrategy> list= getList(DepositStrategy.class, queryFList(hql.toString()));
		if(list ==null || list.size()==0){
			return null;
		}else{
			return list.get(0);
		}
	}

}
