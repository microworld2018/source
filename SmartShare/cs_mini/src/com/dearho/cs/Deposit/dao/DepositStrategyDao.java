package com.dearho.cs.Deposit.dao;

import com.dearho.cs.Deposit.pojo.DepositStrategy;

public interface DepositStrategyDao {
	
	/**根据条件获取押金规则
	 * @param depositStrategy
	 * @return
	 */
	public DepositStrategy getByParam(DepositStrategy depositStrategy);

}
