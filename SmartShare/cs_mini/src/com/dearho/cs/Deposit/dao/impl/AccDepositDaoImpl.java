package com.dearho.cs.Deposit.dao.impl;

import java.util.List;

import com.dearho.cs.Deposit.dao.AccDepositDao;
import com.dearho.cs.Deposit.pojo.AccDeposit;
import com.dearho.cs.core.db.AbstractDaoSupport;

public class AccDepositDaoImpl extends AbstractDaoSupport implements AccDepositDao {

	@Override
	public List<AccDeposit> getDepositList(AccDeposit accDeposit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AccDeposit getDeposit(AccDeposit accDeposit) {
		StringBuffer hql=new StringBuffer();
		hql.append("select a.id from AccDeposit a where a.subscriberId = '").append(accDeposit.getSubscriberId()).append("'");
		List<AccDeposit> list= getList(AccDeposit.class, queryFList(hql.toString()));
		if(list ==null || list.size()==0){
			return null;
		}else{
			return list.get(0);
		}
	}

	@Override
	public void updateDeposit(AccDeposit accDeposit) {
		updateEntity(accDeposit);
	}

}
