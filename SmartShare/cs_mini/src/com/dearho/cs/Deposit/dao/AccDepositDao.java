package com.dearho.cs.Deposit.dao;

import java.util.List;

import com.dearho.cs.Deposit.pojo.AccDeposit;


public interface AccDepositDao {
	
	/**根据条件获取押金列表
	 * @param accDeposit
	 * @return
	 */
	public List<AccDeposit> getDepositList(AccDeposit accDeposit);
	
	/**根据条件获单挑押金数据
	 * @param accDeposit
	 * @return
	 */
	public AccDeposit getDeposit(AccDeposit accDeposit);
	
	/**更新押金数据
	 * @param accDeposit
	 */
	public void updateDeposit(AccDeposit accDeposit);
}
