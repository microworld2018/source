<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>

<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Insert title here</title>

<%@ include file="/pages/common/common_head.jsp"%>


<script type="text/javascript">
	/* 添加用户区域关系 */
	function editEntity(id){
		var dialoguser = $.dialog({
		    id:'useredit', 
		    title:(id == "")?"新增用户区域关系":"编辑用户区域关系",
			content : 'url:<%=path%>/userArea/getUserArea.action?carArea.id='+id,
			fixed:true,
			width:550,
			height:300,
			resize:false,
	 		max: false,
		    min: false,
		    lock: true,
		    ok: function(){
		    	var valid = this.content.isValid();
		    	if (valid){
		    		var form = this.content.getForm();
		    		showLoading(parent);
		    		$.post(form.attr("action"),form.serialize(),r_savedata,'json').error(requestError);
		    	}
		    	return false;
		    },
		    okVal:isnull(id)?'添加':'保存',
		    cancelVal: '关闭',
		    cancel: true,
		    close: function () {
		        this.hide();
		        restoreInfo('hospitalinfo');
		        return true;
		    },
		    init: function(){
		    	if (typeof this.content.isError != 'undefined'){
		    		$(":button").slice(0,1).hide();
		    	}
		    }
		});
		
	}
	
	function r_savedata(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("保存成功！", function(){
				$('#sform').submit();	
		    });
			break;
		case 1:
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
	return false;
}
	
	
	
	//查询方法
	function searchEntity(){
		$("#sform").submit();
	}
	//删除方法
	function deleteEntity(){
		var ob = document.getElementsByName("checkdel");
		var check = false;
		for (var i = 0; i < ob.length; i++) {
			if (ob[i].checked) {
				check = true;
				break;
			}
		}
		if (!check) {
			alertinfo("请选择要删除的数据！");
			return false;
		}
		alertconfirm("确认删除选中的数据吗？",function (){
			showLoading();
			$.post('userAreaDelete.action',$('#sform').serialize(),r_delete,'json').error(requestError);
		});	
	}
	//删除会后执行的方法
	function r_delete(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("删除成功！", function(){
		    	$('#sform').submit();		
		    });
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}

</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
			<form name="sform" class="form-horizontal" id="sform" method="post" action="<%=path%>/userArea/userAreaGet.action">
				<input type="hidden" name="page.orderFlag" id="page.orderFlag"
						value="<ww:property value="page.orderFlag"/>">
				<input type="hidden" name="page.orderString" id="page.orderString"
						value="<ww:property value="page.orderString"/>">
				<input type="hidden" id="allModelInput" value="
					<ww:iterator value="getAllModel('null')" id="data" status="rl"><ww:property value="id" />:<ww:property value="name" />:<ww:property value="brand" />,</ww:iterator>"/>
				<div class="ControlBlock">
		<div class="row SelectBlock">
			<div class="col-sm-4 col-xs-12">
				<div class="form-group">
					<label for="userName" class="col-xs-4 control-label">用户名称</label>
					<div class="col-xs-8">
						<input class="form-control"name="userName" id="userName" type="text" value="" />
					</div>
				</div>
				
			</div>
			<div class="col-sm-4 col-xs-12">
				<div class="form-group">
					<label for="areaName" class="col-xs-4 control-label">区域名称</label>
					<div class="col-xs-8">
						<input class="form-control"name="areaName" id="areaName" type="text" value="" />
					</div>
				</div>
				
			</div>
		</div>
		<div class="row SubmitButtonBlock">
			<div class="col-sm-2 col-sm-offset-3 col-xs-4">
				<a class="btn btn-block Button1"  onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a>
			</div>
			<ww:if test="hasPrivilegeUrl('/user/userGet.action')"><div class="col-sm-2 col-xs-4">
				<a class="btn btn-block Button2"  onclick="editEntity('');" target="_blank"><i class="fa fa-floppy-o"></i>添加</a>
			</div></ww:if>
			<ww:if test="hasPrivilegeUrl('/user/userDelete.action')"><div class="col-sm-2 col-xs-4">
				<a class="btn btn-block Button3"  onclick="deleteEntity('');" target="_blank"><i class="fa fa-trash-o"></i>删除</a>
			</div></ww:if>
		</div>
	</div>
				<div class="row TableBlock">
					<table class="table table-striped table-bordered table-condensed">
						<tr class="ths" id="tab_bg_cl">
							<td  width="68" height="50">
								<input type="checkbox" name="checkdelcheckall" onclick="funCheck('','checkdel')" />
							</td>
							<td width="100">
								<a href="javascript:SetOrder('vehiclePlateId')">用户名</a>
							</td>
							<td width="80">
								管理区域
							</td>
							<td width="157">
								操作
							</td>
						</tr>
						<tr>
						</tr>
						<ww:iterator value="page.results" id="data" status="rl">
							<tr
								<ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
								<td align="center">
									<input type="checkbox" name="checkdel" value="<ww:property value='id' />" />
								</td>
								<td align="left">
									<ww:property value="user.name" />
								</td>
								<td align="left">
									<ww:property value="AdministrativeArea.name"/>
								</td>
								<td>
									<div class="pan_btn3"  onclick="javascript:editEntity('<ww:property value="id"/>');">编辑</div>
								</td>
							</tr>
						</ww:iterator>
						<tr style="background-color: #fff;height: 30px;">
							<td align="center" colspan="8">
								<ww:property value="page.pageSplit" />	
							</td>
						</tr>
					</table>
				</div>
			</form>
	</div>
</body>
</html>