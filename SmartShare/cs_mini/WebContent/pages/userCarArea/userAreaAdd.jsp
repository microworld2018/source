<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Insert title here</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
$().ready(function (){
	var id = '<ww:property value="#request.userArea.id" />';
	if (id == ""){
		$("#eform").attr('action','<%=path%>/userArea/userAreaAdd.action');
	}else{
		$("#eform").attr('action','<%=path%>/userArea/userAreaUpdate.action');	
	}
	$('#eform').validate({
		errorClass : 'text-danger',
		rules: {
			"carArea.userId": {
				required: true
			},
			"carArea.areaId":{
				required: true
			}
		},
		messages: {
			"carArea.userId": {
				required: "请选择用户！"
			},
			"carArea.areaId":{
				required: "请选择区域！"
			}
		}
	});
});

function isValid(){
	if ($("#eform").valid()){
		return true;
	}else{
		return false;
	}
}
function getForm(){
	return $("#eform");
}

/* $(document).ready(function () { 
	  $("#sel").bind("change",function(){ 
	    if($(this).val()==0){
	      return; 
	    } 
	    else{
	    	
	    	alert($(this).val());
	    } 
	  }); 
	}); */
</script>
</head>
<body >
	<div class="table_con tanchuang" >
		<form name="eform" id="eform" method="post" action="">
		<input type="hidden" name="carArea.id" id="carArea.id"
				value="<ww:property value="#request.userArea.id" />">
			<table class="t1" >
				<%-- <tr class="trr" >
					<th >
						<span >角色：</span>
					</th>
					<td>
						<select name="eEntity.groupId" style="width:60%" id="sel">
							<option value="0">请选择用户组</option>
							<ww:iterator value="#request.groupList">
								
								<option value='<ww:property value="groupId"/>'  <ww:if test="eEntity.groupId==groupId">selected="selected"</ww:if> ><ww:property value="groupName"/></option>
							</ww:iterator>  
						</select>
					</td>
				</tr> --%>
				<tr class="trr" >
					<th >
						<span >用户：</span>
					</th>
					<td>
						<select name="carArea.userId" style="width:60%">
							<option value="">请选择用户</option>
							<ww:iterator value="#request.userList">
								<option value='<ww:property value="id"/>' <ww:if test="#request.userArea.userId==id">selected="selected"</ww:if>>
									<ww:property value="name"/>
								</option>
							</ww:iterator>  
						</select>
					</td>
				</tr>
				<tr class="trr" >
					<th >
						<span >管理区域：</span>
					</th>
					<td>
						<select name="carArea.areaId" style="width:60%">
							<option value="">请选择管理区域</option>
							<ww:iterator value="#request.areaList">
								<option value='<ww:property value="id"/>' <ww:if test="#request.userArea.areaId==id">selected="selected"</ww:if>>
									<ww:property value="name"/>
								</option>
							</ww:iterator>  
						</select>
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>