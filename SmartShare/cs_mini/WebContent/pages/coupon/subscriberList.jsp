<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<script type="text/javascript">
function searchEntity(){
	$("#sform").submit();
}

// 发放优惠券
function sendCouponSub(){
	var ob = document.getElementsByName("checkdel");
	var check = false;
	for (var i = 0; i < ob.length; i++) {
		if (ob[i].checked) {
			check = true;
			break;
		}
	}
	if (!check) {
		alertinfo("请选择要发放的用户！");
		return false;
	}
	alertconfirm("确认发放优惠券？",function (){
		showLoading();
		$.post('sendCouponSub.action',$('#sform').serialize(),r_delete,'json').error(requestError);
	});	
}


//全部用户发放
function senAllSub(id){
	alertconfirm("确认给所有用户发放优惠券吗？",function (){
		showLoading();
		$.post('sendCouponSub.action',{"all":"1","coupon.id":id},r_delete,'json').error(requestError);
	});	
}




function r_delete(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("发放成功！", function(){
				window.location.href="<%=path%>/coupon/couponActionList.action";
		    });
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}


function corBack(){
	window.location.href="<%=path%>/coupon/couponActionList.action";
}
</script>
<title>Insert title here</title>

<%@ include file="/pages/common/common_head.jsp"%>

</head>
<body class="SubPage">
	<div class="container-fluid">
			<form name="sform" class="form-horizontal" id="sform" method="post" action="<%=path%>/coupon/subscriberList.action">
				<input type="hidden" name="page.orderFlag" id="page.orderFlag"
						value="<ww:property value="page.orderFlag"/>">
				<input type="hidden" name="page.orderString" id="page.orderString"
						value="<ww:property value="page.orderString"/>">
				<input type="hidden" name="coupon.id" value="<ww:property value="coupon.id"/>" />
				<div class="ControlBlock">
					<div class="row SelectBlock">
						<div class="col-sm-4 col-xs-6" style="width: 500px;">
							<div class="form-group">
								<label for="cph" class="col-xs-4 control-label">姓名：</label>
								<div class="col-xs-8">
									<input type="text" name="subscriber.name" value="<ww:property value="subscriber.name"/>"/>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-xs-6" style="width: 320px;">
							<div class="form-group">
							 	<label for="car.carVehicleModel.brand" class="col-xs-4 control-label">手机号：</label>
							 	<div class="col-xs-8">
								 	<input type="text" name="subscriber.phoneNo" value="<ww:property value="subscriber.phoneNo"/>"/>
								</div>
							 </div>
						</div>
						<div class="col-sm-4 col-xs-6">
							<div class="form-group">
							 	<label for="car.carVehicleModel.brand" class="col-xs-4 control-label">性别：</label>
							 	<div class="col-xs-8">
								 	<%-- <input type="text" name="subscriber.phoneNo" value="<ww:property value="subscriber.phoneNo"/>"/> --%>
								 	<select name="subscriber.sex" class="form-control">
								 		<option value="">全部</option>
								 		<option value="man" >男</option>
								 		<option value="woman" >女</option>
								 	</select>
								</div>
							 </div>
						</div>
					 </div>
					 <div class="row SubmitButtonBlock" style="margin-right: 20%">
							<div class="col-sm-2 col-sm-offset-3 col-xs-4"><a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a></div>
							<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="sendCouponSub();" target="_blank"><i class="fa fa-floppy-o"></i>指定用户发放</a></div>
							<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="corBack();" target="_blank"><i class="fa fa-floppy-o"></i>返回</a></div>
  				     		<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="senAllSub('<ww:property value="coupon.id"/>');" target="_blank"><i class="fa fa-floppy-o"></i>全部用户发放</a></div>
  				     </div>
  				</div>
				<div class="row TableBlock">
					<table class="table table-striped table-bordered table-condensed">
						<tr class="ths" id="tab_bg_cl">
							<td  width="68" height="50">
								<input type="checkbox" name="checkdelcheckall" onclick="funCheck('','checkdel')" />
							</td>
							<td width="100">
								手机号
							</td>
							<td width="80">
								姓名
							</td>
							<td width="100">
								状态
							</td>
							<td>
								锁定状态
							</td>
							<td>
								驾驶证号
							</td>
						</tr>
						<ww:iterator value="page.results" id="data" status="rl">
							<tr
								
								 <ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
								<td align="center">
									<input type="checkbox" name="checkdel" value="<ww:property value='id' />" />
								</td>
								<td align="left">
									<ww:property value="phoneNo" />
								</td>
								
								<td align="left">
									<ww:property value="name"/>
								</td>
								<td align="left">
									<ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_UNCONFIRMED ==state">
										<span class="label label-default">资料未提交</span>
									</ww:if>
									<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_WAIT_CONFIRMED ==state">
										<span class="label label-primary">资料待审核</span>
									</ww:elseif>
									<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NO_CONFIRMED ==state">
										<span class="label label-warning">审核未通过</span>
									</ww:elseif>
									<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NORMAL ==state">
										<span class="label label-success">资料已审核</span>
									</ww:elseif>
									<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NORMAL ==state">
										<span class="label label-danger">未知</span>
									</ww:elseif>
								</td>
								<td align="left">
									<ww:if test="4==eventState">半锁</ww:if>
									<ww:elseif test="5==eventState">全锁</ww:elseif>
								</td>
								<td align="center">
									<ww:property value="drivingLicenseNo" />
								</td>
							</tr>
						</ww:iterator>
						<tr style="background-color: #fff;height: 30px;">
							<td align="center" colspan="8">
								<ww:property value="page.pageSplit" />	
							</td>
						</tr>
					</table>
				</div>
			</form>
	</div>
</body>
</html>