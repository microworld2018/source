<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>押金充退管理列表</title>
<%@ include file="/pages/common/common_head.jsp"%>

<script type="text/javascript">

function unpaidDebit(id){
	alertconfirm("确定此操作吗？",function (){
		$.ajax({
			  type: 'POST',
			  url: '<%=path %>/refund/immediately.action',
			  data:{"id":id},
			  dataType: 'json',
			  success: function(data){
				  if(data.result == 0){
					  hideLoading();
					  alertinfo("操作成功！");
					  window.location.href="<%=path%>/refund/rechargeRecordList.action";
				  }else{
					  alert(data.msg);
				  }
			  }
		});
	});
}
	
function searchEntity(){
	$("#sform").submit();
}
	
function editEntity(id,hasAbnormal){
	
	if (hasAbnormal==1){
		alertconfirm("该用户有异常订单或未支付订单未处理，确定要退款吗？",function (){
	        window.location.href="<%=path%>/refund/getRefundRecordById.action?refundRecord.id="+id;
		});
	} else {
        window.location.href="<%=path%>/refund/getRefundRecordById.action?refundRecord.id="+id;		
	}
}

</script>
</head>
<body  class="SubPage">
<div class="container-fluid">
			<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path %>/refund/rechargeRecordList.action">
				<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-sm-4 col-xs-6">
						 <div class="form-group">
						 	<label for="subPhoneNo" class="col-xs-4 control-label">手机号：</label>
						 	<div class="col-xs-8">
						 		<input type="text" class="kd form-control" name="subPhoneNo" value='<ww:property value="subPhoneNo" />' />
						 	</div>
						 </div>
					 </div>
					 <div class="col-sm-4 col-xs-6">
						<div class="form-group">
						 	<label for="subName" class="col-xs-4 control-label">姓名：</label>
						 	<div class="col-xs-8">
								<input type="text" name="subName" class="kd form-control" value='<ww:property value="subName" />'/>
							</div>
						 </div>
					 </div>
					 <div class="col-sm-4 col-xs-6">
						<div class="form-group">
						 	<label for="tradeOrderNo" class="col-xs-4 control-label">充值流水号：</label>
						 	<div class="col-xs-8">
								<input type="text" name="tradeOrderNo" class="kd form-control" value='<ww:property value="tradeOrderNo" />'/>
							</div>
						 </div>
					 </div>
					 <div class="col-sm-4 col-xs-6">
						<div class="form-group">
						 	<label for="refundTradeNo" class="col-xs-4 control-label">退款流水号：</label>
						 	<div class="col-xs-8">
								<input type="text" name="refundTradeNo" class="kd form-control" value='<ww:property value="refundTradeNo" />'/>
							</div>
						 </div>
					 </div>
				 </div>
				 	
				 <div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4"><a class="btn btn-block Button1" onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a></div>
  				 </div>
			</div>
				
			
			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td width="50"  >
							姓名
						</td>
						<td width="50"  >
							手机号	
						</td>
						<td width="50"  >
							充值金额	
						</td>
						<td width="50"  >
							充值流水号
						</td>
						<td width="50"  >
							充值时间
						</td>
						<td width="50"  >
							退款金额
						</td>
						<td width="50" >
							退款时间
						</td>
						
						<td width="50">
							退款流水号
						</td>
						<td width="50">
							操作
						</td>
					</tr>
			
					<ww:iterator value="rechargeRefundRecordList" id="data" status="rl">
						<tr <ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
						<tr>
							<td>
								<ww:property value="subName" />
							</td>
							<td>
								<ww:property value="subPhoneNo" />
							</td>
							<td>
								<ww:property value="money" />
							</td>
							<td>
								<ww:property value="tradeOrderNo" />
							</td>
							<td>
								<ww:property value="rechargeTimeStr" />
							</td>
							<td>
								<ww:property value="refundMoney" />
							</td>
							<td>
								<ww:property value="refundTimeStr" />
							</td>
							<td>
								<ww:property value="refundTradeNo" />
							</td>
							
							<td>
								<ww:if test="refundStatus == 1">
									<ww:if test="immediately != 1">
										<div class="qzbtn fl" onclick="javascript:unpaidDebit('<ww:property value="refundRecordId"/>');" 
										style="background-color: #22C599;color:#ffffff;cursor:pointer;">立即退款</div>
									</ww:if>
									<ww:elseif test="immediately == 1">
										<div class="qzbtn fl" style="background-color: #AAAAAA;color:#ffffff;">立即退款</div>
									</ww:elseif>
								</ww:if>                           
							</td>
				    	</tr>
					</ww:iterator>
					<tr >
						<td align="center" colspan="16">
							<ww:property value="page.pageSplit" />	
						</td>
					</tr>
				</table>
			</div>
			</form>
			</div>
</body>
</html>