<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>退款申请管理列表</title>
<%@ include file="/pages/common/common_head.jsp"%>

<script type="text/javascript">
	
function searchEntity(){
	$("#sform").submit();
}
	
function editEntity(id,hasAbnormal){
	
	if (hasAbnormal==1){
	alertconfirm("该用户有异常订单或未支付订单未处理，确定要退款吗？",function (){
        window.location.href="<%=path%>/refund/getRefundRecordById.action?refundRecord.id="+id;
	});} else
	{
        window.location.href="<%=path%>/refund/getRefundRecordById.action?refundRecord.id="+id;		
	}
}

</script>
</head>
<body  class="SubPage">
<div class="container-fluid">
			<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path %>/refund/refundApplication.action">
				<input type="hidden" name="page.orderFlag" id="page.orderFlag"
						value="<ww:property value="page.orderFlag"/>">
				<input type="hidden" name="page.orderString" id="page.orderString"
						value="<ww:property value="page.orderString"/>">                       
				
				<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-sm-4 col-xs-6">
						 <div class="form-group">
						 	<label for="refundRecord.subscriber.phoneNo" class="col-xs-4 control-label">手机号：</label>
						 	<div class="col-xs-8">
						 		<input type="text" class="kd form-control" name="refundRecord.subscriber.phoneNo" value='<ww:property value="refundRecord.subscriber.phoneNo" />' />
						 	</div>
						 </div>
						 <div class="form-group">
						 	<label for="refundRecord.status" class="col-xs-4 control-label">退款状态：</label>
						 	<div class="col-xs-8">
						 		<select name="refundRecord.status" class="form-control">
						 			<option value="0">全部</option>
						 			<option value="1" <ww:if test="refundRecord.status == 1">selected="selected"</ww:if>>申请退款</option>
						 			<option value="2" <ww:if test="refundRecord.status == 2">selected="selected"</ww:if>>退款中</option>
						 			<option value="3" <ww:if test="refundRecord.status == 3">selected="selected"</ww:if>>退款中断</option>
						 			<option value="4" <ww:if test="refundRecord.status == 4">selected="selected"</ww:if>>已退款</option>
						 		</select>
						 	</div>
						 </div>
						
					 </div>
					 <div class="col-sm-4 col-xs-6">
						<div class="form-group">
						 	<label for="refundRecord.subscriber.name" class="col-xs-4 control-label">姓名：</label>
						 	<div class="col-xs-8">
								<input type="text" name="refundRecord.subscriber.name" class="kd form-control" value='<ww:property value="refundRecord.subscriber.name" />'/>
							</div>
						 </div>
						 <div class="form-group">
						 	<label for="refundRecord.subscriber.state" class="col-xs-4 control-label">审核状态：</label>
						 	<div class="col-xs-8">
						 		<select  class="form-control" name="refundRecord.subscriber.state" id="refundRecord.subscriber.state">
							   		<option>全部</option>
									<option value="1" <ww:if test="1==refundRecord.subscriber.state">selected</ww:if> >资料未提交</option>
									<option value="2" <ww:if test="2==refundRecord.subscriber.state">selected</ww:if> >资料待审核</option>
									<option value="3" <ww:if test="3==refundRecord.subscriber.state">selected</ww:if> >资料已审核</option>
									<option value="4" <ww:if test="4==refundRecord.subscriber.state">selected</ww:if> >审核未通过</option>
								</select>
							</div>
						 </div>
					 </div>
					<div class="col-sm-4 col-xs-6">
						  <div class="form-group">
						 	<label for="refundRecord.subscriber.sex" class="col-xs-4 control-label">性别：</label>
						 	<div class="col-xs-8">
						 		<select  class="form-control" name="refundRecord.subscriber.sex" id="subscriber.sex">
							   		<option value="">全部</option>
									<option value='<ww:property value="@com.dearho.cs.subscriber.pojo.Subscriber@SEX_MAN"/>'  <ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@SEX_MAN.equals(refundRecord.subscriber.sex)">selected=true</ww:if> >男</option>
									<option value='<ww:property value="@com.dearho.cs.subscriber.pojo.Subscriber@SEX_WOMAN"/>' <ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@SEX_WOMAN.equals(refundRecord.subscriber.sex)">selected=true</ww:if> >女</option>
								</select>
							</div>
						 </div>
						
					 </div>
				 </div>
				 	
				 <div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4"><a class="btn btn-block Button1" onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a></div>
  				 </div>
			</div>
				
			
			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td width="50"  >
							手机号	
						</td>
						<td width="50"  >
							姓名
						</td>
						<td width="20"  >
							性别</td>
						<td width="50"  >
							审核状态
						</td>
						<td width="50"  >充值时间</td>
						<td width="50"  >
							申请时间
						</td>
						<td width="50" >
							申请金额
						</td>
						
						<td width="50">应退金额</td>
						<td width="100">
							退款状态
						</td>
						<td width="50">充值流水号</td>
						<td width="50">退款流水号</td>
						<td width="50">支付类型</td>
						<td width="50">
							操作
						</td>
					</tr>
			
					<ww:iterator value="page.results" id="data" status="rl">
						<tr
							
							 <ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
							<tr>
							  <td >
								<ww:property value="subscriber.phoneNo" />
							</td>
							
							<td >
								<ww:property value="subscriber.name" />
							</td>
							
							<td>
								<ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@SEX_MAN.equals(subscriber.sex)">男</ww:if> 
										<ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@SEX_WOMAN.equals(subscriber.sex)">女</ww:if>
							</td>
							<td >
								<ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_UNCONFIRMED ==subscriber.state">
										<span class="label label-default">资料未提交</span>
									</ww:if>
									<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_WAIT_CONFIRMED ==subscriber.state">
										<span class="label label-primary">资料待审核</span>
									</ww:elseif>
									<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NO_CONFIRMED ==subscriber.state">
										<span class="label label-warning">审核未通过</span>
									</ww:elseif>
									<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NORMAL ==subscriber.state">
										<span class="label label-success">资料已审核</span>
									</ww:elseif>
									<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NORMAL ==subscriber.state">
										<span class="label label-danger">未知</span>
									</ww:elseif>
							</td>
							<td ><ww:property value="depositTime" /></td>
							<td >
								<ww:property value="transDateString(createTime)" />
							</td>
							<td >
								<ww:property value="formatAmount(money)" />
							</td>
							<td >
								<ww:property value="formatAmount(actualMoney)" />
							</td>
							<td >
								<ww:if test="status == 1">
										<span class="label label-default">申请退款</span>
								</ww:if>
								<ww:elseif test="status == 2">
										<span class="label label-primary">退款中</span>
								</ww:elseif>
								<ww:elseif test="status == 3">
										<span class="label label-warning">退款中断</span>
								</ww:elseif>
								<ww:elseif test="status == 4">
										<span class="label label-success">已退款</span>
								</ww:elseif>
							</td>
							<td ><ww:property value="rechargeTradeNo" /></td>
							<td ><ww:property value="tradeOrderNo2" /></td>
							<td ><ww:property value="@com.dearho.cs.sys.util.DictUtil@getCnNameByCode('12',payType)" /></td>
							<td >
							<ww:if test="hasPrivilegeUrl('/refund/getRefundRecordById.action')">
                            <ww:if test="(editable == 1 && status != 4) || immediately == 1">
								<div class="pan_btn1"  onclick="javascript:editEntity('<ww:property value="id"/>','<ww:property value="hasAbnormal"/>');">编辑</div>
							</ww:if>
							</ww:if>                            
							</td>
						</tr>
					</ww:iterator>
					<tr >
						<td align="center" colspan="16">
							<ww:property value="page.pageSplit" />	
						</td>
					</tr>
				</table>
			</div>
			</form>
			</div>
</body>
</html>