<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>编辑地勤订单</title>

<%@ include file="/pages/common/common_head.jsp"%>



<script type="text/javascript">
function cancel(){
	window.history.back();  
}

$(function (){
 	$('#datetimepicker6').datetimepicker({
		language: 'zh-CN',
	    format: 'yyyy-mm-dd hh:ii:ss',
	    autoclose: true,
	    minView: 0,
	    minuteStep:1
	});
    $('#datetimepicker7').datetimepicker({
        useCurrent: false,
        language: 'zh-CN',
        format: 'yyyy-mm-dd hh:ii:ss',
        autoclose: true,
        minView: 0,
        minuteStep:1
    });
    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
})



function okEdit(){
	/* $("#myForm").submit(); */
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/orders/saveGroundOrderEdit.action',
		  data:$('#myForm').serialize(),
		  dataType: 'json',
		  success: function(data){
			if(data.result == 0){
				window.location.href="<%=path %>/orders/groundOrders.action";
			}else{
				 alertinfo(data.msg);
			}
		  }
	});
}



</script>

</head>
<body style="overflow-y:auto;" class="sgglPage">
      <div class="tc">
		<form id="myForm" name="myForm" method="post">
			<input type="hidden" name="carAccident.id" id="carAccident.id" value="<ww:property value="carAccident.id" />">
			<input type="hidden" name="groundOrder.id"  value="<ww:property value="groundOrder.id" />" />
		    <table class="table table-bordered table-condensed">
			 	<tbody>
                <tr>
	                <td class="CaptionTd"><span>订单编号</span>:</td>
	                <td class="ContentTd"> <ww:property value="groundOrder.orderNo" /></td>  
	                <td class="CaptionTd"><span>订单状态</span>:</td>
	                <td>
	                <select class="form-control" name="groundOrder.orderState" id="groundOrder.orderState">
						<option value=""  <ww:if test="groundOrder.orderState == ''">selected=true</ww:if> >全部</option>	
						<option value="0"  <ww:if test="groundOrder.orderState == 0">selected=true</ww:if> >用车中</option>	
						<option value="1"  <ww:if test="groundOrder.orderState == 1">selected=true</ww:if> >用车结束</option>	
					</select>
	                </td>   
             </tr>
             <tr>
	                <td><span>使用车辆</span>:</td>
	                <td><ww:property value="groundOrder.vehicleModelName" /></td>  
	                <td><span>车牌号码</span>:</td>
	                <td><ww:property value="groundOrder.carNum" /></td>   
             </tr>
             <tr>
	                <td><span>用车人</span>:</td>
	                <td><ww:property value="groundOrder.subName" /></td>   
	                <td><span>取车地点</span>:</td>
	                <td><ww:property value="groundOrder.takeDotId" /></td>  
             </tr>
             <tr>
	                <td><span>预定时间</span>:</td>
	                <td><ww:property value="transDateString(groundOrder.takeTime)" /></td>  
	                <td><span>还车地点</span>:</td>
	                <td><ww:property value="groundOrder.backDotId" /></td>   
             </tr>
             <tr>
           		<td><span>支付时间</span>:</td>
                <td>
               		<div class='col-md-5' style="height: 35px;">
				        <div class="form-group">
				            <div class='input-group date' id='datetimepicker6'>
				                <input type='text' name="groundOrder.backTimeStr" class="form-control" value="<ww:property value="transDateString(groundOrder.backTime)" />"/>
				                <span class="input-group-addon">
				                    <span class="glyphicon glyphicon-calendar"></span>
				                </span>
				            </div>
				        </div>
				    </div>
                </td>  
                <td><span>使用时长</span>:</td>
                <td><ww:property value="orders.ordersDuration" /> 分钟 </td>
             </tr>
             <tr class="Border">
				   <td><span>使用里程</span>:</td>
	               <td><ww:property value="formatAmount(ordersDetail.mileage)" /> 公里 </td>
				   <td><span>费用</span>:</td>
	               <td><ww:property value="orders.totalFee" /> 元</td>  
	           	</tr>
			 </tbody>
			</table>

			 <table class="table table-bordered table-condensed SubTable">
			<tbody>
             
	           	
             
			</tbody>
		</table>
			
	
			<div class="row">
				<div class="col-xs-4 col-xs-offset-4">
					<div class="btt">
				 		<div class="qzbtn fl" onclick="okEdit();" style="background-color: #16a085">确&nbsp;&nbsp;定</div>
                        <div class="qzbtn fl" onclick="cancel();">返&nbsp;&nbsp;回</div>
                	</div>
			</div>
			</div>
		</form>
	</div>
</body>
</html>