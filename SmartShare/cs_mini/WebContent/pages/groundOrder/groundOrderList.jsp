<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>地勤订单管理</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
$(function(){
	/*时间选择*/
	$("#sform .TimeSelect").datetimepicker({
		language: 'zh-CN',
		todayHighlight: 'true',
		todayBtn: 'linked',
		minView: 4,
		autoclose: true,
		minuteStep: 1,
		format: "yyyy-mm-dd"
	});
});


function searchEntity(){
	$("#sform").submit();
}

function cancelOrderForm(id,name){
	var pars={
			"id":id
		};
	alertconfirm("确认取消【"+name+"】订单吗？",function (){
		showLoading();
		$.post('<%=path%>/orders/ordersCancel.action',pars,r_cancel,'json').error(requestError);
	});	
}

function r_cancel(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("取消成功！", function(){
		    	$('#sform').submit();		
		    });
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}
function showDetail(id){
	window.location.href="<%=path%>/orders/groundOrderDetail.action?id="+id+"&type=0";
}

/* 编辑订单详情 */
function ordersDetailEdit(id){
	window.location.href="<%=path%>/orders/groundOrderDetail.action?id="+id+"&type=1";
}


//打开车辆选择页面
function updateCar(id){
	window.location.href="<%=path%>/ordercar/getCarList.action?orderId="+id;
}

//打开后台下订单页面
function openAddGroundOrder(){
	window.location.href="<%=path %>/pages/groundOrder/groundOrderAdd.jsp";
}

//查看订单车辆轨迹
function lookCarLocus(id){
	<%-- window.location.href="<%=path%>/carlocus/openOrderCarLocus.action?orderId="+id; --%>
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/carlocus/viewOrderTrajectory.action',
		  data:{"groundOrderId":id},
	      type: 'get',
		  dataType: 'json',
		  success: function(data){
			  if(data.result == 0 || data.result == "0"){
				  window.location.href="<%=path%>/carlocus/openOrderCarLocus.action?groundOrderId="+id;
			  }else{
				  alertinfo(data.msg);
			  }
		  }
		});
}



function okReturnCar(id){
		var dialoguser = $.dialog({
		    id:'subscriberdailogid', 
		    title:'确认还车',
		    content : "url:<%=path%>/orders/getDotList.action?orderId="+id,
			resize:false,
			fixed:true,
			width:900,
			height:650,
		    lock: true,
	 		max: false,
		    min: false,
		    close: function () {
		        this.hide();
		        restoreInfo('hospitalinfo');
		        return true;
		    },
		    init: function(){
		    	if (typeof this.content.isError != 'undefined'){
		    		$(":button").slice(0,1).hide();
		    	}
		    }
		});
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path%>/orders/groundOrders.action">
			<%-- <input type="hidden" name="page.orderFlag" id="page.orderFlag" value="<ww:property value="page.orderFlag"/>"> 
			<input type="hidden" name="page.orderString" id="page.orderString" value="<ww:property value="page.orderString"/>"> --%>
			<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="takeTime" class="col-xs-4 control-label">预定时间</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control TimeSelect" name="takeTime" id="startTime" value="<ww:property value="takeTime"/>">
							</div>
						</div>
						<div class="form-group">
							<label for="groundOrder.orderNo" class="col-xs-4 control-label">订单编号</label>
							<div class="col-xs-8">
								<input class="form-control" name="groundOrder.orderNo" id="groundOrder.orderNo" type="text"
									   value="<ww:property value="groundOrder.orderNo"/>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-xs-4 control-label">用车原因</label>
							<div class="col-xs-8">
								<select class="form-control" name="groundOrder.tuneCarReason" id="groundOrder.tuneCarReason">
									<option value="" <ww:if test='groundOrder.tuneCarReason==""'>selected=true</ww:if>>全部</option>
									<option value="内部调用车辆" <ww:if test='"内部调用车辆".equals(groundOrder.tuneCarReason)'>selected=true</ww:if>>内部调用车辆</option>
									<option value="维修车辆" <ww:if test='"维修车辆".equals(groundOrder.tuneCarReason)'>selected=true</ww:if>>维修车辆</option>	
									<option value="车辆充电" <ww:if test='"车辆充电".equals(groundOrder.tuneCarReason)'>selected=true</ww:if>>车辆充电</option>	
									<option value="车辆救援" <ww:if test='"车辆救援".equals(groundOrder.tuneCarReason)'>selected=true</ww:if>>车辆救援</option>	
									<option value="其他" <ww:if test='"其他".equals(groundOrder.tuneCarReason)'>selected=true</ww:if>>其他</option>	
								</select>
							</div>
						</div>
					</div>

					<div class="col-xs-4">
						<div class="form-group">
							<label for="backTime" class="col-xs-4 control-label">支付时间</label>
							<div class="col-xs-8">
								<input type="text" class="form-control TimeSelect" name="backTime" id="backTime" value="<ww:property value="backTime"/>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">车牌号</label>
							<div class="col-xs-8">
								<input type="text" class="form-control" name="carNumber" value="<ww:property value="carNumber"/>">
							</div>
						</div>
					</div>

					<div class="col-xs-4">
						<div class="form-group">
							<label for="groundOrder.orderState" class="col-xs-4 control-label">订单状态</label>
							<div class="col-xs-8">
								<select class="form-control" name="groundOrder.orderState" id="orderState">
									<option value="" <ww:if test="groundOrder.orderState == ''">selected=true</ww:if> >全部</option>	
									<option value="0" <ww:if test="groundOrder.orderState == 0">selected=true</ww:if> >用车中</option>	
									<option value="1" <ww:if test="groundOrder.orderState == 1">selected=true</ww:if> >用车结束</option>	
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">手机号</label>
							<div class="col-xs-8">
								<input type="text" class="form-control" name="subPhoneNum" value="<ww:property value="subPhoneNum"/>">
							</div>
						</div>
					</div>
					
					
				</div>
				
				<div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank">
						<i class="fa fa-search"></i>查询</a>
					</div>
					<ww:if test="hasPrivilegeUrl('/groundOrder/groundOrderAdd.jsp')">
						<div class="col-sm-2 col-xs-4">
							<a class="btn btn-block Button2" onclick="openAddGroundOrder();" target="_blank">
							<i class="fa fa-floppy-o"></i>添加订单</a>
						</div>
					</ww:if>
  				</div>
				
			</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td>订单编号</td>
						<td>订单状态</td>
						<td>手机号</td>
						<td>使用车辆</td>
						<td>预定时间</td>
						<td>支付时间</td>
						<td>用车人</td>
						<td>用车原因</td>
						<td>操作</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" <ww:if test="#rl.even"> class="trs"</ww:if>>
							<tr>
						    <td align="center"><ww:property value="orderNo" /></td>
							<td align="center"><ww:property value="#dictUtil.getCnNameByCode('18',orderState)" /></td>
							<td align="center"><ww:property value="subPhoneNum" /></td>
							<td align="center"><ww:property value="carNum" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(takeTime,'MM-dd HH:mm')" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(backTime,'MM-dd HH:mm')" /></td>
							<td align="center"><ww:property value="subName" /></td>
							<td align="center"><ww:property value="tuneCarReason" /></td>
							<td align="center">
								<ww:if test="hasPrivilegeUrl('/groundOrder/groundOrderDetail.action')">
									<div class="pan_btn4" onclick="javascript:showDetail('<ww:property value="id"/>');">详情</div> 	
								</ww:if>
								<ww:if test="hasPrivilegeUrl('/carlocus/viewOrderTrajectory.action')">
									<div class="pan_btn3" onclick="javascript:lookCarLocus('<ww:property value="id"/>');">轨迹查看</div>	
								</ww:if>
								<ww:if test="hasPrivilegeUrl('/groundOrder/groundOrderDetail.action')">
									<div class="pan_btn4" onclick="javascript:ordersDetailEdit('<ww:property value="id"/>');">编辑</div> 	
								</ww:if>
							</td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="13"><ww:property value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>