<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>车辆违章管理</title>

<%@ include file="/pages/common/common_head.jsp"%>
<!-- JavaScripts initializations and stuff -->
<script src="<%=path %>/common/js/main/xenon-custom.js"></script>
<script type="text/javascript">
	function editEntity(id){
		window.location.href="<%=path%>/carservice/carViolationGet.action?id="+id;
	}
	function searchEntity(){
		$("#sform").submit();
	}
	$(function(){
		/*时间选择*/
		$("#sform .TimeSelect").datetimepicker({
			language: 'zh-CN',
			todayHighlight: true,
			todayBtn: true,
			minView: 4,
			autoclose: true,
			minuteStep : 1,
			minView : 0,
			format: "yyyy-mm-dd hh:ii",
			endDate:new Date()
		});
	})
	
	
	
	function changeDiscard(id,isDiscard){
		if(isDiscard == '0'){
			alertconfirm("确认删除吗？",function (){
				showLoading();
				var par = {'id':id,'isDiscard':isDiscard};
				$.post('carViolationChangeDiscard.action',par,function(data){
					hideLoading();
					switch(data.result){
						case 0:
							alertok("删除成功！", function(){
						    	$('#sform').submit();		
						    });
							break;
						case 1:
							restoreInfo();
							alerterror(data.msg);
							break;
						case 9:
							document.location = "doError.action";
							break;
					}
				},'json').error(requestError);
			});	
		}
		else{
			var par = {'id':id,'isDiscard':isDiscard};
			showLoading();
			$.post('carViolationChangeDiscard.action',par,function(data){
				hideLoading();
				switch(data.result){
					case 0:
						alertok("找回成功！", function(){
					    	$('#sform').submit();		
					    });
						break;
					case 1:
						restoreInfo();
						alerterror(data.msg);
						break;
					case 9:
						document.location = "doError.action";
						break;
				}
			},'json').error(requestError);
		}
		
	}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form name="sform" class="form-horizontal" id="sform" method="post" action="<%=path%>/carservice/carViolationSearch.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
					value="<ww:property value="page.orderFlag"/>">
			<input type="hidden" name="page.orderString" id="page.orderString"
					value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="carViolation.code" class="col-xs-5 control-label">违章用户名称：</label>
							<div class="col-xs-7">
					    		 <input class="form-control" name="carViolation.memberName" id="carViolation.memberName" type="text" value="<ww:property value="carViolation.memberName"/>">
					    	</div>
					    </div>
					    <div class="form-group">
							<label for="carViolation.plateNumber" class="col-xs-5 control-label">订单号：</label>
							<div class="col-xs-7">
					    		 <input class="form-control" name="carViolation.orderCode" id="carViolation.orderCode" type="text" value="<ww:property value="carViolation.orderCode"/>">
					    	</div>
					    </div>
					</div>
					<div class="col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="carViolation.orderCode" class="col-xs-5 control-label">开始时间：</label>
							<div class="col-xs-7">
					    		 <input class="form-control TimeSelect" name="carViolation.startTime" id="carViolation.startTime" type="text" value="<ww:property value="carViolation.startTime"/>">
					    	</div>
					    </div>
					    <div class="form-group">
							<label for="yycs" class="col-xs-5 control-label">处理状态：</label>
							<div class="col-xs-7">
					    		<select class="form-control" id="yycs" name="carViolation.bizStatus" >
									<option value="" <ww:if test='carViolation.bizStatus == ""'>selected="selected"</ww:if>>全部</option>
									<option value="0" <ww:if test='carViolation.bizStatus == "0"'>selected="selected"</ww:if>>未处理</option>
									<option value="1" <ww:if test='carViolation.bizStatus == "1"'>selected="selected"</ww:if>>已处理</option>
									<option value="2" <ww:if test='carViolation.bizStatus == "2"'>selected="selected"</ww:if>>代办处理</option>
								</select>
					    	</div>
					    </div>
					</div>
					<div class="col-sm-4 col-xs-6">
						<div class="form-group">
							<label for="carViolation.memberName" class="col-xs-5 control-label">结束时间：</label>
							<div class="col-xs-7">
					    		 <input class="form-control TimeSelect" name="carViolation.endTime" id="carViolation.endTime" type="text" value="<ww:property value="carViolation.endTime"/>">
					    	</div>
					    </div>
					</div>
				</div>
				<div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4"><a class="btn btn-block Button1" onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a></div>
					<ww:if test="hasPrivilegeUrl('/carservice/carViolationGet.action')">
						<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="editEntity();" target="_blank"><i class="fa fa-floppy-o"></i>添加</a></div>
					</ww:if>
	  			</div>
	  		</div>
			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td  height="28" width="40"  >
							<input type="checkbox" name="checkdelcheckall" onclick="funCheck('','checkdel')" />
						</td>
						<td>用户名称</td>
						<td>订单编号</td>
						<td>订单开始时间</td>
						<td>订单结束时间</td>
						<td>违章时间</td>
						<td>违章代码</td>
						<td>违章内容</td>
						<td>扣分</td>
						<td>罚款</td>
						<td>处理状态</td>
						<td width="157">
							操作
						</td>
					</tr>
			
					<ww:iterator value="page.results" id="data" status="rl">
						<tr
							 <ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
							<td align="center">
								<input type="checkbox" name="checkdel" value="<ww:property value='id' />" />
							</td>
							<td align="left">
								<ww:property value="memberName" />
							</td>
							<td align="left">
								<ww:property value="orderCode" />
							</td>
							<td align="left">
								<ww:property value="startTime" />
							</td>
							<td align="left">
								<ww:property value="endTime" />
							</td>
							<td align="left">
								<ww:property value="transDate12String(happenTime)" />
							</td>
							<td align="left">
								<ww:property value="illegalCode" />
							</td>
							
							<td align="center">
								<ww:property value="remark" />
							</td>
							<td align="left">
								<ww:property value="score" />
							</td>
							<td align="left">
								<ww:property value="money" />
							</td>
							<td align="left">
								<ww:if test='bizStatus=="0"'>
									未处理
								</ww:if>
								<ww:if test='bizStatus=="1"'>
									已处理
								</ww:if>
								<ww:if test='bizStatus=="2"'>
									代办处理
								</ww:if>
							</td>
							<td align="center">
								<ww:if test="hasPrivilegeUrl('/carservice/carViolationGet.action')">
									<ww:if test="isDiscard == 0">
										<div class="pan_btn3"  onclick="javascript:editEntity('<ww:property value="id"/>');">编辑</div>
									</ww:if>
								</ww:if>
								<ww:if test="hasPrivilegeUrl('/carservice/carViolationChangeDiscard.action')">
									<div class="pan_btn4"  onclick="javascript:changeDiscard('<ww:property value="id"/>','<ww:property value="isDiscard"/>');"><ww:if test="isDiscard == 0">删除</ww:if><ww:else>找回</ww:else></div>
								</ww:if>
								<ww:if test="hasPrivilegeUrl('/systemutil/getSysOperateLogByDataId.action')">
									<div class="pan_btn2"  onclick="showLogRecordForDialog('<ww:property value="id" />','<%=path %>');">记录</div>
								</ww:if>
							</td>
						</tr>
					</ww:iterator>
					<tr style="background-color: #fff;height: 30px;">
						<td align="center" colspan="14">
							<ww:property value="page.pageSplit" />	
						</td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>

<script type="text/javascript">

</script>
</html>