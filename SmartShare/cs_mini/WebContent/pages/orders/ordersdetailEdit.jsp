<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>编辑订单</title>

<%@ include file="/pages/common/common_head.jsp"%>



<script type="text/javascript">
function cancel(){
	window.history.back();  
}

$(function (){
 	$('#datetimepicker6').datetimepicker({
		language: 'zh-CN',
	    format: 'yyyy-mm-dd hh:ii:ss',
	    autoclose: true,
	    minView: 0,
	    minuteStep:1
	});
    $('#datetimepicker7').datetimepicker({
        useCurrent: false,
        language: 'zh-CN',
        format: 'yyyy-mm-dd hh:ii:ss',
        autoclose: true,
        minView: 0,
        minuteStep:1
    });
    $("#datetimepicker6").on("dp.change", function (e) {
        $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
        $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
})



function okEdit(){
	/* $("#myForm").submit(); */
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/orders/saveOrderEdit.action',
		  data:$('#myForm').serialize(),
		  dataType: 'json',
		  success: function(data){
			if(data.result == 0){
				window.location.href="<%=path %>/orders/ordersSearch.action";
			}else{
				 alertinfo(data.msg);
			}
		  }
		});
}



</script>

</head>
<body style="overflow-y:auto;" class="sgglPage">
      <div class="tc">
		<form id="myForm" name="myForm" method="post">
			<input type="hidden" name="carAccident.id" id="carAccident.id"
						value="<ww:property value="carAccident.id" />">
			<input type="hidden" name="orders.id"  value="<ww:property value="orders.id" />" />
		     <table class="table table-bordered table-condensed">
			 	<tbody>
                <tr>
	                <td class="CaptionTd"><span>订单编号</span>:</td>
	                <td class="ContentTd"> <ww:property value="orders.ordersNo" /></td>  
	                <td class="CaptionTd"><span>订单状态</span>:</td>
	                <td>
	                <select class="form-control"   name="orders.state" id="orders.state">
							<ww:iterator value="#dictUtil.getDictSelectsByGroupCode('14',1)" id="data" status="rl">
								<option value="<ww:property value="code" />"  <ww:if test="orders.state==code">selected=true</ww:if> ><ww:property value="cnName" /></option>	
							</ww:iterator>
						</select>
	                </td>   
             </tr>
             
              <tr>
	                <td><span>车辆型号</span>:</td>
	                <td><ww:property value="orders.vehicleModelName" /></td>  
	                <td><span>车牌号码</span>:</td>
	                <td><ww:property value="orders.plateNumber" /> </td>   
             </tr>
              <tr>
                 <td>购买保险:</td>
                <td><ww:property value="orderDetail.insuranceName" /></td>
                <td>保险金额:</td>
                <td><ww:property value="orderDetail.insuranceFee" /></td>
              </tr>
              <tr>
	                <td><span>承租人</span>:</td>
	                <td><ww:property value="orders.memberName" /></td>   
	                <td><span>实际还车地点</span>:</td>
	                <td><ww:property value="orders.endSiteId" /> </td>  
             </tr>
             
             <tr>
	                <td><span>取车时间</span>:</td>
	                <td><ww:property value="transDateString(orders.beginTime)" /></td>  
	                <td><span>还车时间</span>:</td>
	                <td><ww:property value="transDateString(orders.endTime)" /> </td>   
             </tr>
               <tr>
	                <td><span>是否需要发票</span>:</td>
	                <td><ww:if test="orders.isBill==1">需要</ww:if><ww:else>不需要</ww:else> </td>   
	                <td><span>总费用</span>:</td>
	                <td><ww:property value="formatAmount(orders.totalFee)" /> </td>   
             </tr>
			 </tbody>
			 </table>

			 <table class="table table-bordered table-condensed SubTable">
			<tbody>
             <tr>
             		<td class="CaptionTd"><span>子订单号</span>:</td>
	                <td class="ContentTd"><ww:property value="orderDetail.ordersDetailNo" /> </td>   
	                
	                <td class="CaptionTd"><span>租赁类型</span>:</td>
	                <td> 
	                	<ww:property value="orderDetail.typeName" /> 
	                </td>   
             </tr>
             
             <tr>
             		<td><span>开始时间</span>:</td>
	                <td>
	               		<div class='col-md-5'>
					        <div class="form-group">
					            <div class='input-group date' id='datetimepicker6'>
					                <input type='text' name="orders.beginTimeStr" class="form-control" value="<ww:property value="transDateString(orderDetail.beginTime)" />"/>
					                <span class="input-group-addon">
					                    <span class="glyphicon glyphicon-calendar"></span>
					                </span>
					            </div>
					        </div>
					    </div>
	                </td>  
	                
	                <td><span>结束时间</span>:</td>
	                <td>
	                	<div class='col-md-5'>
					        <div class="form-group">
					            <div class='input-group date' id='datetimepicker7'>
					                <input type='text' name="orders.endTimeStr"  class="form-control" value="<ww:property value="transDateString(orderDetail.endTime)" />"/>
					                <span class="input-group-addon">
					                    <span class="glyphicon glyphicon-calendar"></span>
					                </span>
					            </div>
					        </div>
					    </div>
	                </td>   
             </tr>
             
             	<tr class="Border">
					<td><span>使用时长</span>:</td>
	                <td><ww:property value="orders.ordersDuration" /> (分钟)</td>  
					<td><span>计时费用</span>:</td>
	                <td><ww:property value="formatAmount(orderDetail.timeFee)" /> 元</td>  
             	</tr>
             
              	<tr class="Border">
					<td><span>使用里程</span>:</td>
		               <td><ww:property value="formatAmount(orderDetail.mileage)" /> 公里 </td>  
					<td><span>里程费用</span>:</td>
		               <td><ww:property value="formatAmount(orderDetail.mileFee)" /> 元</td>  
            	</tr>
             
			</tbody>
		</table>
			
	
			<div class="row">
				<div class="col-xs-4 col-xs-offset-4">
					<div class="btt">
				 		<div class="qzbtn fl" onclick="okEdit();" style="background-color: #16a085">确&nbsp;&nbsp;定</div>
                        <div class="qzbtn fl" onclick="cancel();">返&nbsp;&nbsp;回</div>
                	</div>
			</div>
			</div>
		</form>
	</div>
</body>
</html>