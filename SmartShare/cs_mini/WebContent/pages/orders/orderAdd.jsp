<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>车辆事故</title>

<%@ include file="/pages/common/common_head.jsp"%>


<script type="text/javascript" src="<%=path%>/common/js/FileUploadShow.js"></script>

<script type="text/javascript">
function selectMember(){
	$.dialog({
		id:'searchMemberDia',
	    title:'会员查询',
		content : 'url:<%=path%>/subscriber/showSubscriberList.action?state=order',
		fixed:true,
		width:740,
		height:450,
		resize:false,
		max: false,
	    min: false,
	    lock: true,
	    init: function(){
	    	if (typeof this.content.isError != 'undefined'){
	    		$(":button").slice(0,1).hide();
	    	}
	    }
	});
}
function selectCar(){
	var dotId = $("#branchDotId").val();
	if(dotId == ""){
		alert("请选择取车网点!");
		return false;
	}else{
	$.dialog({
		id:'searchCarDia',
	    title:'车辆查询',
		content : 'url:<%=path%>/orders/getOrderCar.action?branchDotId='+dotId,
		fixed:true,
		width:740,
		height:450,
		resize:false,
 		max: false,
	    min: false,
	    lock: true,
	    init: function(){
	    	if (typeof this.content.isError != 'undefined'){
	    		$(":button").slice(0,1).hide();
	    	}
	    }
	});
	}
}
//查询取车网点
function selectDot(){
		$.dialog({
			id:'searchDotDialog',
		    title:'取车网点',
			content : 'url:<%=path%>/place/branchDotSearch.action?state=page',
			fixed:true,
			width:740,
			height:450,
			resize:false,
			max: false,
		    min: false,
		    lock: true,
		    init: function(){
		    	if (typeof this.content.isError != 'undefined'){
		    		$(":button").slice(0,1).hide();
		    	}
		    }
		});
	}

//取消按钮
function cancel(){
	window.location.href="<%=path %>/orders/ordersSearch.action";
}


$(function(){
	$('#eform').validate({
		errorClass : 'text-danger',
		rules: {
			"memberName":{
				required: true
			},
			"vehiclePlateId":{
				required: true
			},
			"branchDotName":{
				required: true
			}
		},
		messages: {
			"memberName":{
				required: "请选择会员！"
			},
			"vehiclePlateId":{
				required: "请选择租用车辆！"
			},
			"branchDotName":{
				required: "请选择取车网点！"
			}
		}
		
	});
	
	
});


function isValid(){
	if ($("#eform").valid()){
		return true;
	}else{
		return false;
	}
}

function sub(){
var id = '<ww:property value="id" />';
var url="<%=path%>/orders/orderAdd.action";
var re=isValid();
if(re){
	var branchDotId = $("#branchDotId").val();
	var carId = $("#carId").val();
	var memberId = $("#memberId").val();
    $.ajax({
        url: url,
        type: 'post',
        data: {"memberId":memberId,"carId":carId,"branchDotId":branchDotId},
        dataType: 'json',
        success:function(data){
        	if(data.result == 200){
        		alertinfo(data.msg);
        		window.location.href="<%=path %>/orders/ordersSearch.action";
        	}else{
        		alertinfo(data.msg);
        	}
        }
    });
}
}
</script>
</head>
<body style="overflow-y:auto;" class="sgglPage">
      <div class="tc">
		<form name="eform" id="eform" method="post" action="" enctype="multipart/form-data"  >
			<input type="hidden" name="carAccident.id" id="carAccident.id" value="<ww:property value="carAccident.id" />">
			<input type="hidden" name="carAccident.isDiscard" value="<ww:property value="carAccident.isDiscard" />">
			<input type="hidden" name="carAccident.code" value="<ww:property value="carAccident.code" />">
		  <table class="xxgl" border="0"  cellpadding="0" cellspacing="0">
		  	 <tr>
                <td class="zuo1"><span class="xx red">*</span><span>租车会员</span>:</td>
                <td class="you1">
                    <input type="hidden" name="memberId" id="memberId" value="<ww:property value="memberId" />"/>
                 <input name="memberName" id="memberName" type="text" readonly 
                 	class="input_size fl" style="top: 0;left: 0px;position:relative;"
                 		value="<ww:property value="carAccident.memberName" />" />
                  <input  onclick="selectMember();" type="button" value="选择" class="searchinputbut" />
                </td>
             </tr>
           <tr>
              <td class="zuo"><span class="xx red">*</span><span>取车网点</span>:</td>
              <td class="you">
                	<input type="hidden" name="belongDotId" id="branchDotId" value="<ww:property value="belongDotId" />"/>
                  <input  name="belongDotName" id="branchDotName" type="text" readonly 
                	    class="input_size fl" style="top: 0;left: 0px;position:relative;"
                		value="<ww:property value="belongDotName" />" />
			<input onclick="selectDot();" type="button" value="选择" class="searchinputbut" />
              </td>  
          	</tr>
          	
          	  <tr>
	           	 <td class="zuo"><span class="xx red">*</span><span>租用车辆</span>:</td>
	               	<td class="you">
	                	<input type="hidden" name="carId" id="carId" value="<ww:property value="carId" />"/>
	                	<input name="vehiclePlateId"  id="vehiclePlateId" type="text" readonly 
	                	class="input_size fl" style="top: 0;left: 0px;position:relative;"
	                		value="<ww:property value="carAccident.plateNumber" />" />
	                	<input onclick="selectCar();" type="button" value="选择" class="searchinputbut" />
	               </td>   
             </tr>
		  	  
		  	  
              <tr style="height:10px"><td colspan="4" style=" border-top:2px solid #A0AFAE;"></td></tr>
              <tr></tr>
		  	<tr>
                  <td colspan="4">
                      <div class="btt">
                         <div class="sbtn fl" onclick="sub();">提&nbsp;&nbsp;交</div>
                         <div class="qzbtn fl" onclick="cancel();">取&nbsp;&nbsp;消</div>
                      </div>
                  </td>
             </tr>
			</table>
		</form>
	</div>
</body>
</html>