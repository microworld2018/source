<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>提醒缴费</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
function cancel(){
alertconfirm("您有操作未确定，取消则会丢失数据，确定取消？",function (){
	window.history.back(); 
});	
}


//未缴扣款
function unpaidDebit(){
	var paidfees = $("#paidfees").val();//实收金额
	var deposit = $("#deposit").val();//押金金额
	var chargespayable = $("#chargespayable").val();//应收金额
	var subname = $("#subname").val();//会员名称
	var subphone = $("#subphone").val();//会员手机号
	var orderId = $("#orderId").val();//订单ID
	var abnormalType = $("#abnormalType").val();//异常类型
	var remindCode = $("#remindCode").val();
	var abnormalId = $("#abnormalId").val();//事故或违章ID
	alert("订单异常类型："+abnormalType);
	if(paidfees < 0 || paidfees == ""){
		alertinfo("请输入实收费！");
		return false;
	}
	if(chargespayable < 0 || chargespayable == ""){
		alertinfo("请输入应收金额！");
		return false;
	}
	if(paidfees > deposit){
		alertconfirm("确认缴费："+paidfees+"&nbsp元？",function (){
			var abnormalStr = "";
			if(abnormalType == 0){
				abnormalStr = "违章";
			}
			if(abnormalType == 1){
				abnormalStr = "事故";
			}
			var coutent = "【电动侠租车】您有一笔:"+paidfees+"元的"+abnormalStr+"费用需要缴纳，请登录平台及时进行支付！如有疑问，请拨打客服电话400-888-1212";
			//押金不足发送短信提醒充值
			$.ajax({
				  type: 'POST',
				  url: '<%=path%>/orders/remindPayment.action',
				  data:{"subphone":subphone,"coutent":coutent,"orderId":orderId,"remindCode":remindCode},
				  dataType: 'json',
				  success: function(data){
					  if(data.result == 200){
						  window.location.href="<%=path%>/orders/orderAbnormalType.action";
					  }else{
						  alertinfo("系统错误请联系管理员！");
					  }
				  }
				});
		});	
	}else{
		alertconfirm("确认扣款金额："+paidfees+"&nbsp;元？",function (){
			//押金充足直接扣款
			$.ajax({
				  type: 'POST',
				  url: '<%=path %>/orders/orderCharge.action',
				  data:{"paidfees":paidfees,"deposit":deposit,"chargespayable":chargespayable,"subname":subname,"subphone":subphone,"orderId":orderId,"abnormalId":abnormalId,"abnormalType":abnormalType},
				  dataType: 'json',
				  success: function(data){
					  if(data.result == 0 || data.result == "0"){
						  alertinfo("扣款成功！");
						  window.location.href="<%=path%>/orders/orderAbnormalType.action";
					  }else{
						  alertinfo("系统错误请联系管理员！");
					  }
				  }
				});
		});	
	}
}



//提醒客户扣款
function remindPayment(){
	var subphone = $("#subphone").val();//会员手机号
	var paidfees = $("#paidfees").val();//实收金额
	var abnormalType = $("#abnormalType").val();
	var orderId = $("#orderId").val();//订单ID
	var remindCode = $("#remindCode").val();
	if(paidfees < 0 || paidfees == ""){
		alert("请输入实收费！");
		return false;
	}
	var abnormalStr = "";
	if(abnormalType == 0){
		abnormalStr = "违章";
	}
	if(abnormalType == 1){
		abnormalStr = "事故";
	}
	var coutent = "【电动侠租车】您有一笔:"+paidfees+"元的"+abnormalStr+"费用需要缴纳，请登录平台及时进行支付！如有疑问，请拨打客服电话400-888-1212";
	//提醒客户扣款
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/orders/remindPayment.action',
		  data:{"subphone":subphone,"coutent":coutent,"orderId":orderId,"remindCode":remindCode},
		  dataType: 'json',
		  success: function(data){
			  if(data.result == 200){
				  alertinfo("已提醒用户！");
				  window.location.href="<%=path%>/orders/orderAbnormalType.action";
			  }else{
				  alertinfo("系统错误请联系管理员！");
			  }
		  }
		});
}
</script>

</head>
<body style="overflow-y:auto;" class="sgglPage">
      <div class="tc">
		<form>
			<input type="hidden" id="orderId"
						value="<ww:property value="orders.id" />">
			<input type="hidden" id="subphone" value="<ww:property value="orders.memberPhoneNo" />"/>
			<input type="hidden" id="subname" value="<ww:property value="orders.memberName" />"/>
			<input type="hidden" id="abnormalType" value="<ww:property value="orders.abnormalType" />"/>
			<input type="hidden" id="remindCode" value='<ww:property value="orders.remindCode"/>' /> 
			<input type="hidden" id="abnormalId" value='<ww:property value="orders.abnormalId"/>'> 
		     <table class="table table-bordered table-condensed">
			 	<tbody>
                <tr>
	                <td class="CaptionTd"><span>订单编号</span>:</td>
	                <td class="ContentTd"> <ww:property value="orders.ordersNo" /></td>  
	                <td class="CaptionTd"><span>订单状态</span>:</td>
	                <td><ww:property value="@com.dearho.cs.sys.util.DictUtil@getCnNameByCode('14',orders.state)" /></td>   
             </tr>
             
              <tr>
	                <td><span>车辆型号</span>:</td>
	                <td><ww:property value="orders.vehicleModelName" /></td>  
	                <td><span>车牌号码</span>:</td>
	                <td><ww:property value="orders.plateNumber" /> </td>   
             </tr>
             <tr>
                <td>购买保险:</td>
                <td><ww:property value="orders.ordersDetai.insuranceName" /></td>
                <td>保险金额</td>
                <td><ww:property value="orders.ordersDetai.insuranceFee" /></td>
              </tr>
              <tr>
	                <td><span>承租人</span>:</td>
	                <td><ww:property value="orders.memberName" /></td>   
	                <td><span>实际还车地点</span>:</td>
	                <td><ww:property value="orders.endSiteId" /></td> 
             </tr>
             
             <tr>
	                <td><span>取车时间</span>:</td>
	                <td><ww:property value="transDateString(orders.beginTime)" /></td> 
	                <td><span>还车时间</span>:</td>
	                <td><ww:property value="transDateString(orders.endTime)" /> </td>    
             </tr>
             <tr>
             		<td><span>是否需要发票</span>:</td>
             		<td><ww:if test="orders.isBill==1">需要</ww:if><ww:else>不需要</ww:else></td>
             		<td><span>总费用</span>:</td>
             		<td><ww:property value="orders.totalFee" /></td>
             </tr>
			 </tbody>
			 </table>
			 <table class="table table-bordered table-condensed SubTable">
			<tbody>
             <tr>
             		<td class="CaptionTd"><span>订单号</span>:</td>
	                <td class="ContentTd"><ww:property value="orders.ordersNo" /> </td>   
	                <td class="CaptionTd"><span>异常状态</span>:</td>
	                <td> 
	                	<select>
	                		<option <ww:if test="orders.abnormalType == 0">selected="selected"</ww:if> value="0">违章</option>
	                		<option <ww:if test="orders.abnormalType == 1">selected="selected"</ww:if> value="1">事故</option>
	                	</select>
	                </td>   
             </tr>
             <tr>
             		<td><span>应收费用</span>:</td>
	                <td><input type="text" id="chargespayable" value=""/></td>  
	                <td><span>实收费用</span>:</td>
	                <td><input type="text" id="paidfees" value=""/></td>   
             </tr>
           	<tr class="Border">
					<td><span>押金金额</span>:</td>
		            <td><input type="text" id="deposit" readonly value="<ww:property value="orders.deposit" />"/></td> 
		            <td><span>提醒次数</span>:</td>
		            <td><input type="text" readonly value="<ww:property value="orders.remindNumber" />"/></td>   
           	</tr>
			  </tbody>
			 </table>
			<div class="row">
				<div class="col-xs-6 col-xs-offset-3">
					<div class="btt">
                            <div class="qzbtn fl" onclick="remindPayment();" style="background-color: #16a085">提醒缴费</div>
                            <div class="qzbtn fl" onclick="unpaidDebit();" style="background-color: #16a085">未缴扣款</div>
                            <div class="qzbtn fl" onclick="cancel();">取&nbsp;&nbsp;消</div>
                	</div>
				</div>
			</div>
		</form>
	</div>
	
	
	<%-- <div class="cd-popup3">
    <div class="cd-popup-container3">
        <div class="cd-popup-title" id="koukuan">短信提醒</div>
        <div class="cd-buttons" style="margin:15% 0 10% 0;">
       缴费金额：<input type="text" style="line-height:30px;" size="10" id="paymentamount"/>元
        </div>
        <div class="cd-buttons" style="margin:0% 0; line-height:40px;">
        <a href="javascript:remindPayment()" class="anniu-close" >提醒用户充值</a>
        </div>
      <img class="cd-popup-close" src="<%=path%>/admin/common/images/main/clos.png">
    </div>
</div> --%>
	
</body>
</html>