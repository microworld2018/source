<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>异常订单列表</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
$(function(){
	/*时间选择*/
	$("#sform .TimeSelect").datetimepicker({
		language: 'zh-CN',
		todayHighlight: 'true',
		todayBtn: 'linked',
		minView: 4,
		autoclose: true,
		minuteStep: 1,
		format: "yyyy-mm-dd"
	});
});

function searchEntity(){
	$("#sform").submit();
}

/* 编辑异常订单 */
function ordersDetailEdit(id,remindCode,abnormalType,abnormalId){
	window.location.href="<%=path%>/orders/abnormalTypeOrderDetail.action?id="+id+"&remindCode="+remindCode+"&abnormalType="+abnormalType+"&abnormalId="+abnormalId;
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post"
			action="<%=path%>/orders/orderAbnormalType.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-xs-3">
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">开始时间</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control TimeSelect" name="startTime" id="startTime" value="<ww:property value="startTime"/>">
							</div>
						</div>
						<div class="form-group">
							<label for="orders.ordersNo" class="col-xs-4 control-label">订单编号</label>
							<div class="col-xs-8">
								<input class="form-control" name="ordersNo"
									id="ordersNo" type="text"
									value="<ww:property value="ordersNo"/>">
							</div>
						</div>
					</div>

					<div class="col-xs-3">
						<div class="form-group">
							<label for="endTime" class="col-xs-4 control-label">结束时间</label>
							<div class="col-xs-8">
								<input type="text" class="form-control TimeSelect" name="endTime" id="endTime" value="<ww:property value="endTime"/>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">车牌号</label>
							<div class="col-xs-8">
								<input type="text" class="form-control" name="carNumber" value="<ww:property value="carNumber"/>">
							</div>
						</div>
					</div>

					<div class="col-xs-3">
						<div class="form-group">
							<label for="orders.abnormalType" class="col-xs-4 control-label">异常状态</label>
							<div class="col-xs-8">
								<select class="form-control"   name="abnormalType" id="abnormalType">
									<option value="">全部</option>
									<option value="0" <ww:if test='abnormalType == "0"'>selected="selected"</ww:if>>违章</option>
									<option value="1" <ww:if test='abnormalType == "1"'>selected="selected"</ww:if>>事故</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">手机号</label>
							<div class="col-xs-8">
								<input type="text" class="form-control" name="phone" value="<ww:property value="phone"/>">
							</div>
						</div>
					</div>
					<div class="col-xs-3">
						<div class="form-group">
							<label for="orders.isAbnormity" class="col-xs-4 control-label">处理状态</label>
							<div class="col-xs-8">
								<select class="form-control"   name="isAbnormity" id="isAbnormity">
									<option value="">全部</option>
									<option value="1" <ww:if test='isAbnormity == "1"'>selected="selected"</ww:if>>已处理</option>
									<option value="0" <ww:if test='isAbnormity == "0"'>selected="selected"</ww:if>>未处理</option>
								</select>
							</div>
						</div>
				    </div>
				</div>
				
				<div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank">
						<i class="fa fa-search"></i>查询</a>
					</div>
  				</div>
				
			</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						
						<td><a href="javascript:SetOrder('ordersNo')">订单编号<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a>
						</td>
						<td>租用车辆</td>
						<td>购买保险</td>
						<td><a href="javascript:SetOrder('ordersTime')">下单时间<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a></td>
						<td><a href="javascript:SetOrder('beginTime')">取车时间<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a></td>
						<td><a href="javascript:SetOrder('endTime')">还车时间<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a></td>
						<td>承租人</td>
						<td>异常状态</td>
						<td>处理状态</td>
						<td>提醒次数</td>
						<td>操作</td>
					</tr>
					<ww:iterator value="orderList" id="data" status="rl">
						<tr style="font-size:12px;" <ww:if test="#rl.even"> class="trs"</ww:if>>
							<tr>
							  <td align="center">
								<a href="javascript:showOrderDetailForDialog('<ww:property value="id" />','<%=path%>')"><ww:property value="ordersNo" /></a>
							</td>
							<td align="center"><a href="javascript:showCarDetailForDialog('<ww:property value="carId" />','<%=path%>')"><ww:property value="plateNumber" /></a></td>
							<td align="center">59857deec04711e69d4300163e0007f1</td>
							<td align="center"><ww:property value="ordersTimeStr" /></td>
							<td align="center"><ww:property value="beginTimeStr" /></td>
							<td align="center"><ww:property value="endTimeStr" /></td>
							<td align="center"><a href="javascript:showSubscriberDetailForDialog('<ww:property value="memberId" />','<%=path%>')"><ww:property value="memberName" /></br><ww:property value="memberPhoneNo" /></a></td>
							<td>
								<ww:if test="abnormalType == 0">违章</ww:if>
								<ww:if test="abnormalType == 1">事故</ww:if>
							</td>
							<td>
								<ww:if test="abnormalType == 0">
									<ww:if test="bizStatus == 0">未处理</ww:if>
									<ww:if test="bizStatus == 1">已处理</ww:if>
									<ww:if test="bizStatus == 2">代办理</ww:if>
								</ww:if>
								<ww:if test="abnormalType == 1">
									<ww:property value="handleStatusName"/>
								</ww:if>
							</td>
							<td>
								已提醒：<ww:property value="remindNumber"/>次
							</td>
							<td align="center">
								<ww:if test="hasPrivilegeUrl('/orders/abnormalTypeOrderDetail.action')">
								<input type="hidden" value='<ww:property value="isAbnormity"/>'>
									<ww:if test="isAbnormity == 0">
										<div class="pan_btn4"  onclick="javascript:ordersDetailEdit('<ww:property value="id"/>','<ww:property value="remindCode"/>','<ww:property value="abnormalType"/>','<ww:property value="abnormalId"/>');">编辑</div> 	
									</ww:if>	
								</ww:if>
							</td>
						</tr>
					</ww:iterator>
				</table>
			</div>
		</form>
	</div>
</body>
</html>