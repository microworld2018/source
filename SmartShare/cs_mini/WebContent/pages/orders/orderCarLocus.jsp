<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title>订单车辆轨迹回放</title>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=BtxULUWSmvG50D5GKe0ka9Yk"></script>
<script type="text/javascript" src="http://api.map.baidu.com/library/LuShu/1.2/src/LuShu_min.js"></script>
<script type="text/javascript" src="<%=path%>/common/js/richMarker_min.js"></script>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
</script>
</head>
<body class="ddqcjkptPage">
	<div id="carsMapDiv"
		style="width: 100%; height: 100%; margin: 0px 0 0 0px; top: 0px; left: 0px; position: absolute; overflow: hidden;">
		<div class="panel" style="padding: 0px 0px;">
			<div class="panel-heading"></div>
		</div>
		<div
			style="width: 100%; height: 10%; border: 0px solid gray; margin: 0px 0 0 0px; top: 10px; left: 10px; position: absolute;">
			<table id="test">
				<tr>
					<td><div class="btn btn-info" onclick="startlushu()"
							style="margin: 0px 10px; padding: 5px 10px;">轨迹回放</div></td>
							
					<td><div class="btn btn-info" onclick="locusPause()"
							style="margin: 0px 10px; padding: 5px 10px;">回放暂停</div></td>
					<td><div class="btn btn-info" onclick="backOrderList()"
							style="margin: 0px 10px; padding: 5px 10px;">返回</div></td>
				</tr>
			</table>
		</div>
		<!-- 地图展示Start -->
		<div id="allmap"
			style="width: 100%; border: 1px solid gray; height: 91%; margin: 0px 0 0 0px; top: 58px; left: 0px; position: absolute;">

		</div>
		<!-- 地图展示END -->
	</div>
</body>
<!-- 操作地图专用JS方法 -->
<script type="text/javascript">
	  /* 初始化地图并且加载各种控件 Start*/
	  var map = new BMap.Map("allmap");
	  map.centerAndZoom("北京", 14);
	  map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
	  var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT});// 左上角，添加比例尺
	  var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
	  map.addControl(top_left_control);
	  map.addControl(top_left_navigation);
	  //添加定位事件
	  var geolocationControl = new BMap.GeolocationControl();
	  geolocationControl.addEventListener("locationSuccess", function(e){
	    // 定位成功事件
	    var address = '';
	    address += e.addressComponent.province;
	    address += e.addressComponent.city;
	    address += e.addressComponent.district;
	    address += e.addressComponent.street;
	    address += e.addressComponent.streetNumber;
	  });
	  geolocationControl.addEventListener("locationError",function(e){
	    // 定位失败事件
	    alert(e.message);
	  });
	  map.addControl(geolocationControl);
	
	//添加城市切换控件
	  var size = new BMap.Size(10, 20);
	  map.addControl(new BMap.CityListControl({
	      anchor: BMAP_ANCHOR_TOP_RIGHT,
	      offset: size,
	  }));
	 /* 初始化地图加载控件END */ 
/* ****************************************************************************** */
	/* 定义全局变量 */
	var condition = false;
	var points = [];//设置坐标数组
	var lushu;
	
	/* 初始化轨迹数据 */
	$(function(){
		var orderId = '<ww:property value="#request.orderId" />';
		/* 清楚地图覆盖物*/
		map.clearOverlays();
		points = [];//每次查询都清空路线数组
		
		$.ajax({
			  type: 'POST',
			  url: '<%=path%>/carlocus/viewOrderTrajectory.action',
			  data:{"orderId":orderId},
		      type: 'get',
			  dataType: 'json',
			  success: function(data){
			 	if(data.result == 0 || data.result == "0"){
			 		var locus = data.info;
			 		console.log(data.info[0].longitude);
			 		console.log(data.info[0].latitude);
			 		map.centerAndZoom(new BMap.Point(locus[0].longitude,locus[0].latitude), 10);
			 		
					var new_point = new BMap.Point(locus[0].longitude,locus[0].latitude);
					map.panTo(new_point);     
			 		
					
			 		for(var i=0;i<locus.length;i++){
			 			points.push(new BMap.Point(locus[i].longitude,locus[i].latitude));
			 		}
		 			var icon1 = new BMap.Icon('http://source.fooleap.org/marker.png', new BMap.Size(19,25),{anchor: new BMap.Size(9, 25)});//地点
		 			var icon2 = new BMap.Icon('http://developer.baidu.com/map/jsdemo/img/car.png', new BMap.Size(52,26), {anchor: new BMap.Size(27, 13)});//动车
		 			var polyline = new BMap.Polyline(points);//创建折线
		 			
		 			lushu = new BMapLib.LuShu(map, points, {
		 			  landmarkPois:[
		 			  ],//显示的特殊点，似乎是必选参数，可以留空，据说要和距原线路10米内才会暂停，这里就用原线的点
		 			  defaultContent: '',//覆盖物内容，这个填上面的特殊点文字才会显示
		 			  speed: 2000,//路书速度
		 			  icon: icon2,//覆盖物图标，默认是百度的红色地点标注
		 			  autoView: true,//自动调整路线视野
		 			  enableRotation: true,//覆盖物随路线走向
		 			});
		 			map.addOverlay(polyline);//覆盖折线到地图上
		 			map.addOverlay(new BMap.Marker(points[0],{icon:icon1}));//覆盖起点标注到地图上
		 			map.addOverlay(new BMap.Marker(points[points.length-1],{icon:icon1}));//覆盖终点标注到地图上
			 		
			 	}else{
			 		alertinfo(data.msg);
			 		window.location.href="<%=path%>/orders/ordersSearch.action";
			 	}
			  }, error: function(data){
				  alertinfo("服务器异常");
			  }
			});
	})
	
	
	
	function startlushu(){
		lushu.start();//启动路书函数
		lushu.showInfoWindow();
	}
		
	function locusPause(){
		if(points.length < 2){
			alert("请先开始回放轨迹！");
			return false;
		}
		lushu.pause();//路书暂停函数
	}
	/* 查询方法结束 */
	function backOrderList(){
		window.history.back();  
	}
	
</script>
</html>