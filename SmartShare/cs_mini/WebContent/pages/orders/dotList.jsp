<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
<%@ include file="/pages/common/common_head.jsp"%>
</head>
<script type="text/javascript">
/* 查询网点 */
function searchEntity(){
	$('#sform').submit();		
}

function okAlsoCar(id){
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/orders/okAlsoCar.action',
		  data:{"orderId":id},
		  dataType: 'json',
		  success: function(data){
		 		if(data.result == 0){
		 			window.opener.location.reload();
		 		}
		  }, error: function(data){
			  alertinfo("服务器异常");
		  }
		});
}
</script>
<body class="SubPage">
<div class="container-fluid">
			<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path%>/orders/getDotList.action">
			<input type="hidden" id="resultDataInputInfo" value="<ww:iterator value="page.results" id="data" status="rl"><ww:property value="name" />:<ww:property value="address" />:<ww:property value="id" />:<ww:property value="lng" />:<ww:property value="lat" />:<ww:property value="carCount" />,</ww:iterator>"/>
			
				<input type="hidden" name="page.orderFlag" id="page.orderFlag"
						value="<ww:property value="page.orderFlag"/>">
				<input type="hidden" name="page.orderString" id="page.orderString"
						value="<ww:property value="page.orderString"/>">
				<input type="hidden" name="orderId" value="<ww:property value="orderId"/>" />
<div class="ControlBlock">
		<div class="row SelectBlock">
			<div class="col-sm-4 col-xs-6">
				<div class="form-group">
					<label for="branchDot.name" class="col-xs-4 control-label">网点名称</label>
					<div class="col-xs-8">
						<input class="form-control" name="branchDot.name" id="branchDot.name" type="text" value="<ww:property value="branchDot.name"/>">
					</div>
				</div>
				<div class="form-group">
					<label for="sfqy" class="col-xs-4 control-label">是否启用</label>
					<div class="col-xs-8">
						<select class="form-control"   name="branchDot.isActive" id="sfqy">
							<option value="">全部</option>
							<option value="1" <ww:if test="branchDot.isActive==1">selected=true</ww:if> >启用</option>	
							<option value="0" <ww:if test="branchDot.isActive==0">selected=true</ww:if>>未启用</option>	
						</select>				
					</div>
				</div>
			</div>
			
			<div class="col-sm-4 col-xs-6">
				<div class="form-group" style="width: 270%;">
					<label for="xzqh" class="col-xs-4 control-label" style="width: 200px;">所属行政区划</label>
					<div class="col-xs-8">
						<select class="form-control"   name="branchDot.areaId" id="xzqh" style="width: 150px;">
							<option value="">全部</option>
							<ww:iterator value="getAreas()" id="data" status="rl">
								<option value="<ww:property value="id" />"  <ww:if test="branchDot.areaId==id">selected=true</ww:if> ><ww:property value="name" /></option>	
							</ww:iterator>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="branchDot.address" class="col-xs-4 control-label">地址</label>
					<div class="col-xs-8">
						<input class="form-control" name="branchDot.address" id="branchDot.address" type="text" value="<ww:property value="branchDot.address"/>">
					</div>
				</div>
			</div>
		</div>
		<div class="row SubmitButtonBlock">
			<ww:if test="hasPrivilegeUrl('/place/branchDotSearch.action')">
				<div class="col-sm-2 col-xs-3 col-sm-offset-2">
					<a class="btn btn-block Button1"  onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a>
				</div>
			</ww:if>
			<ww:if test="hasPrivilegeUrl('/place/branchDotAdd.action')">
				<div class="col-sm-2 col-xs-3">
					<a class="btn btn-block Button2"  onclick="distributionDot();" target="_blank"><i class="fa fa-floppy-o"></i>分配</a>
				</div>
			</ww:if>
		</div>
	</div>

<div id="branchDotListShowDiv" class="row TableBlock">
					<table class="table table-striped table-bordered table-condensed">
						<tr class="ths" id="tab_bg_cl">
							<td >
								<a href="javascript:SetOrder('code')">编码<img src="<%=path%>/admin/common/images/main/paixu.png"/></a>
							</td>
							<td >
								<a href="javascript:SetOrder('name')">名称<img src="<%=path%>/admin/common/images/main/paixu.png"/></a>
							</td>
							<td >
								地址
							</td>
							<td >
								是否启用
							</td>
							<td>车位数量</td>
							<td>操作</td>
						</tr>
				
						<ww:iterator value="page.results" id="data" status="rl">
							<tr
								 <ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
								<td align="left">
									<ww:property value="code" />
								</td>
								<td align="left">
									<ww:property value="name" />
								</td>
								<td align="left">
									<ww:property value="address" />
								</td>
								<td align="center">
									<ww:if test="isActive==1">是</ww:if>
									<ww:if test="isActive==0">否</ww:if>
								</td>
								<td align="right">
									<ww:property value="totalParkingPlace" />
								</td>
								<td>
									<div class="pan_btn4"  onclick="javascript:okAlsoCar('<ww:property value="orderId"/>');">确认还车</div>
								</td>
							</tr>
						</ww:iterator>
						<tr>
							<td align="right" colspan="11">
								<ww:property value="page.pageSplit" />	
							</td>
						</tr>
					</table>
				</div>
			</form>
</div>
</body>
</html>