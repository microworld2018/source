<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>订单数据报表</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
//搜索功能
function searchEntity(){
	var year = $("#year").val();
	var months = $("#months").val();
	var startTime = "";
	if(year == "" && months == ""){
		$("#sform").attr("action", "<%=path%>/orders/orderDayData.action");
	}else{
		if(months == "" || year == ""){
			alert("请选择准确的时间！");
			return false;
		}
		startTime = year+"-"+months;
		$("#sform").attr("action", "<%=path%>/orders/orderDayData.action?startTime="+startTime);
	}
	
	$("#sform").submit();
}
//导出功能
function exportEntity(){
	$("#sform").attr("action", "<%=path%>/orders/exportOrder.action");
	$("#sform").submit();
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post"
			action="">
			<div class="ControlBlock">
			<!-- <div class="col-xs-2">
					<div class="form-group">
						<label for="startTime" class="col-xs-2 control-label" style="text-align:right;padding-right:0;">年:</label>
						<div class="col-xs-6" style="margin-top:5px;padding-left:5px;">
							<select name="year" id="year" style="width:70%;">
								<option value="">全部</option>
								<option value="2017">2017</option>
								<option value="2016">2016</option>
							</select>
						</div>
					</div>
				</div> -->

				<!-- <div class="col-xs-2">
					<div class="form-group">
						<label for="endTime" class="col-xs-2 control-label" style="text-align:right;padding-right:0;">月:</label>
						<div class="col-xs-6" style="margin-top:5px;padding-left:5px;">
							<select id="months" style="width:70%;">
								<option value="">全部</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
							</select>
						</div>
					</div>
				</div> -->
				<div class="row SubmitButtonBlock" style="padding-top: 0px; padding-bottom: 0px;">
					<!-- <div class="col-sm-2 col-sm-offset-1 col-xs-2">
						<a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank">
						<i class="fa fa-search"></i>查询</a>
					</div> -->
					<div class="col-sm-2 col-sm-offset-2 col-xs-2">
						<a class="btn btn-block Button1"   onclick="exportEntity();" target="_blank">
						<i class="fa fa-search"></i>导出Excel文件</a>
					</div>
  				</div>
			</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td>销售日期</td>
						<td>订单总金额</td>
						<td>优惠券支付金额</td>
						<td>实际支付金额</td>
						<td>订单销售数量</td>
					</tr>
					<ww:iterator value="list" id="data" status="rl">
						<tr style="font-size:12px;" >
							<td align="center"><ww:property value="dateTime" /></td>
							<td align="center"><ww:property value="totalFee3" /></td>
							<td align="center"><ww:property value="couponFee" /></td>
							<td align="center"><ww:property value="tposPayFee" /></td>
							<td align="center"><ww:property value="orderCount" /></td>
						</tr>
					</ww:iterator>
				</table>
			</div>
		</form>
	</div>
</body>
</html>