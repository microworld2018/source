<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>订单还车时段分布分析</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript"
	src="https://img.hcharts.cn/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript"
	src="https://img.hcharts.cn/highcharts/highcharts.js"></script>
<script type="text/javascript"
	src="https://img.hcharts.cn/highcharts/modules/exporting.js"></script>
<script type="text/javascript">
	$(function() {
		var myDate = new Date();
		var year=myDate.getFullYear();
		var month=myDate.getMonth()+1;
		var param = year+"-"+month;
		$("#months option[value='"+month+"']").attr("selected", true);//设置默认选中当前月
		$.ajax({
			  type: 'POST',
			  url: '<%=path %>/orders/orderPeriodAnalysisGiveTime.action', 
			  data:{"param":param,"state":1},
			  dataType: 'json',
			  success: function(data){
				  if(data.result == 1){
					  alert("时间格式错误！");
					  return false;
				  }
				  if(data.result == 2){
					  alert("暂无数据请重新定义查询条件！");
					  return false;
				  }
				  var name = [];
				  var number = [];
				  var s =0;
				  for(var i in data.info){
						 for(var k in data.info[i]){
							 name[s] = k;
							 number[s] = data.info[i][k];
							 s=s+1;
						 }
					 }
				  $('#container').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: '订单还车时段分布分析'
				        },
				        xAxis: {
				            categories:name
				        },
				        series: [{
				            name: '订单数量',
				            data: number
				        }],
				        yAxis: {
				            min: 0,
				            title: {
				                text: '订单数量 (单)'
				            }
				        },
				        plotOptions: {
				            column: {
				            	 dataLabels:{
			                            enabled:true // dataLabels设为true
			                        }
				            }
				        },
						credits : {
							enabled : false
						}
				    });
			  }
		});
	});
	
	
	
	
	
	function searchEntity(){
		var y = $("#year").val();
		var m = $("#months").val();
		var param = "";
		var type = 0;
		if(y == "" && m == ""){
			alert("请选择时间！");
			return false;
		}
		if(m == "" && y != ""){
			param = y;
			type = 0;
		}
		if(m != "" && y != ""){
			param = y+"-"+m;
			type = 1;
		}
		$.ajax({
			  type: 'POST',
			  url: '<%=path %>/orders/orderPeriodAnalysisGiveTime.action',
			  data:{"param":param,"state":type},
			  dataType: 'json',
			  success: function(data){
				  if(data.result == 1){
					  alert("时间格式错误！");
					  return false;
				  }
				  if(data.result == 2){
					  alert("暂无数据请重新定义查询条件！");
					  return false;
				  }
				  var name = [];
				  var number = [];
				  var s =0;
				  for(var i in data.info){
						 for(var k in data.info[i]){
							 name[s] = k;
							 number[s] = data.info[i][k];
							 s=s+1;
						 }
					 }
				  $('#container').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: '订单时段分布分析'
				        },
				        xAxis: {
				            categories:name
				        },
				        series: [{
				            name: '订单数量',
				            data: number
				        }],
				        yAxis: {
				            min: 0,
				            title: {
				                text: '订单数量 (单)'
				            }
				        },
				        plotOptions: {
				            column: {
				            	 dataLabels:{
			                            enabled:true // dataLabels设为true
			                        }
				            }
				        },
						credits : {
							enabled : false
						}
				    });
			  }
			});
	}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
	<form  class="form-horizontal" name="sform" id="sform">
		<div class="ControlBlock">
			<div class="row SelectBlock">
				<div class="col-xs-2">
					<div class="form-group">
						<label for="startTime" class="col-xs-4 control-label" style="text-align:right;padding-right:0;">年:</label>
						<div class="col-xs-8" style="margin-top:5px;padding-left:5px;">
							<select name="year" id="year" style="width:70%;">
								<option value="2017">2017</option>
								<option value="2016">2016</option>
							</select>
						</div>
					</div>
				</div>

				<div class="col-xs-2">
					<div class="form-group">
						<label for="endTime" class="col-xs-4 control-label" style="text-align:right;padding-right:0;">月:</label>
						<div class="col-xs-8" style="margin-top:5px;padding-left:5px;">
							<select id="months" style="width:70%;">
								<option value="">全部</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-2">
				  <a class="btn btn-block Button1" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" onclick="searchEntity();"
						target="_blank"> <i class="fa fa-search"></i>查询
					</a>
				</div>
			</div>
		</div>
		</form>
		<div class="row TableBlock">
			<div id="container" style="min-width:400px;height:400px"></div>
		</div>
	</div>
</body>
</html>