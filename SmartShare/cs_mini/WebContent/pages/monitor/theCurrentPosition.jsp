<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>车辆当前位置</title>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=BtxULUWSmvG50D5GKe0ka9Yk"></script>
<script type="text/javascript" src="http://api.map.baidu.com/library/LuShu/1.2/src/LuShu_min.js"></script>
<script type="text/javascript" src="<%=path%>/common/js/richMarker_min.js"></script>
<%@ include file="/pages/common/common_head.jsp"%>
</head>
<body>
<div id="allmap" style="width: 550px; height: 600px;"></div>
</body>
<script type="text/javascript">
	var latitude = '<ww:property value="#request.latitude" />';
	var longitude = '<ww:property value="#request.longitude" />';
	
	// 百度地图API功能
	var map = new BMap.Map("allmap");
	var point = new BMap.Point(longitude, latitude);
	map.centerAndZoom(point, 15);
	map.enableScrollWheelZoom(true);      //开启鼠标滚轮缩放
	var marker = new BMap.Marker(point);  // 创建标注
	map.addOverlay(marker);               // 将标注添加到地图中
	marker.setAnimation(BMAP_ANIMATION_BOUNCE); //跳动的动画
	//设置地图加载完成进行地址逆解析
	var geoc = new BMap.Geocoder();
	map.addEventListener("tilesloaded",function(){
		console.log(point);
		geoc.getLocation(point, function(rs){
			var addComp = rs.addressComponents;
			var content = '<div style="margin:0;line-height:20px;padding:2px;">' +
            '省：'+addComp.province+'<br/>市：'+addComp.city+'<br/>区：' +addComp.district+'<br/>街道：'+addComp.street+'<br/>门牌号：'+addComp.streetNumber+'</div>';
          /**addComp.province + ", " + addComp.city + ", " + addComp.district + ", " + addComp.street + ", " + addComp.streetNumber*/
			var opts = {
					  width : 150,     // 信息窗口宽度
					  height: 100,     // 信息窗口高度
					  title : ""//信息窗口标题
					}
					var infoWindow = new BMap.InfoWindow(content, opts);  // 创建信息窗口对象 
					marker.addEventListener("click", function(){          
						map.openInfoWindow(infoWindow,point); //开启信息窗口
					});
		});    
	})
</script>
</html>