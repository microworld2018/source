<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>监控中心首页</title>
<%@ include file="/pages/common/common_head.jsp"%>

<script type="text/javascript">

function searchEntity(){
	$("#sform").submit();
}


function okReturnCar(id){
	window.location.href="<%=path%>/monitor/dotCar.action?dotId="+id;
}


$(function(){
	$.ajax({
		type:'post',
		url:'<%=path%>/monitor/getArrayList.action',
		data:{},
		dataType:'json',
		async:false,
		success:function(data){
			console.log(data);
			$('#xzqh').empty();
			$("<option value=''>全部</option>").appendTo($('#xzqh'))
			for(var i=0;i<data.info.length;i++){  
				$("<option value='" + data.info[i].id + "'>"+ data.info[i].name + "</option>").appendTo($('#xzqh'));
			}
		}
	})
});

</script>





</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post"
			action="<%=path %>/monitor/indexMonitor.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="xzqh" class="col-xs-4 control-label">所属行政区划</label>
							<div class="col-xs-8">
		    					<select class="form-control"   name="branchDot.areaId" id="xzqh">
									
								</select>
							</div>
						</div>
					</div>

					<div class="col-xs-4">
						<div class="form-group">
							<label for="endTime" class="col-xs-4 control-label">网点名称：</label>
							<div class="col-xs-8">
								<input type="text" class="form-control TimeSelect" name="branchDot.name">
							</div>
						</div>
					</div>

					<div class="col-xs-4">
						<div class="form-group">
							<ul>
								<li style="list-style-type: none;">平台车辆总数：<ww:property value="allCarNumber"/>&nbsp;&nbsp;&nbsp;租借中车俩：<ww:property value="leaseNumber"/></li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank">
						<i class="fa fa-search"></i>查询</a>
					</div>
  				</div>
				
			</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						
						<td>网点名称</td>
						<td>车辆数</td>
						<td>租借中数辆</td>
						<td>未使用数量</td>
						<td>操作</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" <ww:if test="#rl.even"> class="trs"</ww:if>>
							<td align="center"><ww:property value="name" /></td>
							<td align="center"><ww:property value="carCount" /></td>
							<td align="center"><ww:property value="onLoan" /></td>
							<td align="center"><ww:property value="noUse" /></td>
							<td><div class="pan_btn4"  onclick="javascript:okReturnCar('<ww:property value="id"/>');">查看</div></td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="10"><ww:property
								value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>