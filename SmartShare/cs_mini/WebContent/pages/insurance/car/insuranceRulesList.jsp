<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>保险规则列表</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
/* 根据条件查询保险规则 */
function searchEntity(){
	$("#sform").submit();
}

function getForm(){
	return $("#sform");
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path%>/insurancenew/insuranceRules.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<input type="hidden" name="state" value="carInsurance"/>
			<div class="ControlBlock">
			<div class="row SelectBlock">
				<div class="col-xs-4">
					<div class="form-group">
						<label for="startTime" class="col-xs-4 control-label">保险名称:</label>
						<div class="col-xs-8" style="padding-left:5px;">
							<select name="insuranceRules.id" class="form-control" >
								<option value="">全部</option>
								<ww:iterator value="insuranceList" id="data" status="rl">
									<option value="<ww:property value="id"/>" <ww:if test="insuranceRules.id == id">selected="selected"</ww:if>><ww:property value="name"/> </option>
								</ww:iterator>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-3">
				  <a class="btn btn-block Button1" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" onclick="searchEntity();"
						target="_blank"> <i class="fa fa-search"></i>查询
					</a>
				</div>
			</div>
		</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td>
							<input type="checkbox" name="checkdelcheckall" onclick="funCheck('','checkdel')">
						</td>
						<td>保险名称</td>
						<td>保险基础内容</td>
						<td>保险细则</td>
						<td>创建时间</td>
						<td>更新时间（最近一次）</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" >
							<td align="center">
								<input type="checkbox" name="checkdel" value="<ww:property value="id"/>">
<%-- 								<input type="hidden" name="insuranceDescList" value='<ww:property value="insuranceDescList" />' /> --%>
<%-- 								<input type="hidden" name="insuranceDesc" value="<ww:property value="insuranceDesc"/>" /> --%>
							</td>
							<td align="center">
								<ww:property value="name" />
							</td>
							<td align="center">
								<ww:if test="insuranceDescList == null">
									暂无数据
								</ww:if>
								<ww:else>
									<ww:property value="insuranceDescList.get(0).content" />
								</ww:else>
							</td>
							<td align="center">
								<a href="javaScript:saveInusurance('<ww:property value="id" />','toView')">点击查看保险细则</a>
							</td>
							<td align="center">
								<ww:property value="#dateUtil.formatDate(createTime,'yyyy-MM-dd hh:mm:ss')" />
							</td>
							<td align="center">
								<ww:property value="#dateUtil.formatDate(updateTime,'yyyy-MM-dd hh:mm:ss')" />
							</td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="10"><ww:property value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>