<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>车型保险列表</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
/* 根据条件查询车型保险关联 */
function searchEntity(){
	$("#sform").submit();
}
/* 删除车型保险关联 */
function del(id,name){
	alertconfirm("确认删除（"+name+"）保险关联吗？",function (){
		showLoading();
		$.post('delCarModelInusrance.action',{"carVehicleModel.id":id},r_delete,'json').error(requestError);
	});	
}
/* 删除结果反馈 */
function r_delete(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("删除成功！", function(){
				$("#sform").submit();
		    });
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}
/* 打开车型保险关联新增 */
function saveCarInusurance(id,state){
	window.location.href="<%=path%>/insurancenew/getByIdCarInsurance.action";
}

/* 编辑车型保险关联 */
function saveInusurance(id){
	window.location.href="<%=path%>/insurancenew/getByIdCarInsurance.action?carVehicleModel.id="+id;
}

/* 查看车型保险操作记录 */
function log(carModelName){
	dialoguser = $.dialog({
	    id:'insuranceOperatingLog', 
	    title:'车型保险操作记录',
		content : 'url:<%=path%>/insurancenew/toViewCarInsuranceOperating.action?carModelName='+carModelName,
		fixed:true,
		width:800,
		height:550,
		resize:false,
 		max: false,
	    min: false,
	    lock: true,
	    close: function () {
	        this.hide();
	        restoreInfo('hospitalinfo');
	        return true;
	    },
	    init: function(){
	    	if (typeof this.content.isError != 'undefined'){
	    		$(":button").slice(0,1).hide();
	    	}
	    }
	});
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path%>/insurancenew/carInsuranceNewList.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
			<div class="row SelectBlock">
				<div class="col-xs-4">
					<div class="form-group">
						<label for="startTime" class="col-xs-4 control-label">车型名称:</label>
						<div class="col-xs-8" style="padding-left:5px;">
							<select name="carVehicleModel.id" class="form-control" >
								<option value="">全部</option>
								<ww:iterator value="carVehicleModelList" id="data" status="rl">
									<option value="<ww:property value="id"/>" <ww:if test="carVehicleModel.id == id">selected="selected"</ww:if>><ww:property value="name"/> </option>
								</ww:iterator>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-3">
				  <a class="btn btn-block Button1" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" onclick="searchEntity();"
						target="_blank"> <i class="fa fa-search"></i>查询
					</a>
				</div>
				<div class="col-xs-3">
				  	<a class="btn btn-block Button9" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" onclick="saveCarInusurance('','save');"
						target="_blank"> <i class="fa fa-plus-circle"></i>添加车型保险关联
					</a>
				</div>
			</div>
		</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td>车型</td>
						<td>保险名称</td>
						<td>创建时间</td>
						<td>更新时间（最近一次）</td>
						<td>操作</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" >
							<td align="center">
								<ww:property value="name" />
							</td>
							<td align="center">
								<ww:property value="insuranceName" />
							</td>
							<td align="center">
								<ww:property value="insuranceUpdateTime" />
							</td>
							<td align="center">
								<ww:property value="insuranceUpdateTime" />
							</td>
							<td align="center">
								<div class="pan_btn4"  onclick="saveInusurance('<ww:property value="id"/>','update');">编辑</div> 
								<div class="pan_btn4"  onclick="del('<ww:property value="id"/>','<ww:property value="name"/>');">删除</div> 
								<div class="pan_btn4"  onclick="log('<ww:property value="name"/>');">记录</div> 
							</td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="10"><ww:property value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>