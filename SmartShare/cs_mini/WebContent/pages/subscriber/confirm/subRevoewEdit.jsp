<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>会员审核</title>
<base href="<%=basePath%>">
<%@ include file="/pages/common/common_head.jsp"%>
<style>
a{ text-decoration:none;}
.lsize{ text-align:left;}
.tsize{ text-align:right;}
.rsize{ float:right;margin-right:2%;}
.in_boxt {width:800px; padding:20px 10px; border:1px solid #CCC}
.in_boxt td { padding:10px 3%;}
#non_tr { display:none;}
#on_tos { color:#73DCFF; position:absolute;}
.annius{ float:left; text-align:center; color:#ffffff; background:#F69; border:1px solid #CCC; border-radius:5px; padding:1.2% 10%; margin-right:5%;}
.annius_2{ float:left; text-align:center; color:#ffffff; background:#33F; border:1px solid #CCC; border-radius:5px; padding:1.2% 12%; margin-right:5%;}
.r_tex{ float:left; padding:1.5% 0;}
.kox { margin:30px 10px 10px 0; float:left; }
.annius_sen{ float:left; text-align:center; color:#ffffff; background:#69F; border:1px solid #CCC; border-radius:5px; padding:1.2% 5%; margin-right:5%;}
.annius_out{ float:left; text-align:center; color:#000; background:#CCC; border:1px solid #CCC; border-radius:5px; padding:1.2% 5%; margin-right:5%;}
#dislp { display:none; position:relative; width:300px; border:1px solid #dedede; background:#ffffff; border-radius:1px; padding:10px; top:0px; z-index:1999999;}
.linees { padding:5px 0;}
.submitlk { margin-left:100px; margin-top:20px; width:100px;vertical-align:middle; color:#ffffff; height:30px; line-height:30px; cursor:pointer; background:#169BD5; border:0; border-radius:3px;}
</style>
</head>
<body>
	<div class="in_boxt">
		<form name="eform" id="eform" method="post" action="<%=path%>/subscriberConfirm/doCheck.action">
			<input type="hidden" name="subscriberConfirm.subscriberId" value="<ww:property value="subscriberConfirm.subscriberId" />">
			<input type="hidden" name="subscriberConfirm.id"  value="<ww:property value="subscriberConfirm.id" />">
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="24%" class="tsize">用户姓名</td>
					<td width="70%">
						<input type="text" name="subscriberConfirm.subscriber.name" id="name" size="70%" value="<ww:property value="subscriberConfirm.subscriber.name" />"/>
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">手机号</td>
					<td width="70%">
						<input type="text" name="subscriberConfirm.subscriber.phoneNo" id="phone" value="<ww:property value="subscriberConfirm.subscriber.phoneNo" />" size="70%" />
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">驾驶证号</td>
					<td width="70%">
						<input type="text" maxlength="18"  name="subscriberConfirm.subscriber.drivingLicenseNo" id="drivingNo" value="<ww:property value="subscriberConfirm.subscriber.drivingLicenseNo" />" size="70%" />
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">身份证号</td>
					<td width="70%">
						<input type="text" id="idCard"   maxlength="18"  name="subscriberConfirm.subscriber.IDCard"  value="<ww:property value="subscriberConfirm.subscriber.IDCard" />" size="70%" />
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize"></td>
					<td width="70">
						<a href="javascript:searchIdCard(0)" class="annius_2">身份信息查询</a>
						<a id="inquireNumber" href="javascript:showLogRecordForDialog('<ww:property value="subscriberConfirm.subscriber.IDCard" />','<%=path %>','会员信息审核身份证验证')"></a>
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize"></td>
					<td width="70">
						<a href="javascript:searchIdCard(1)" class="annius_2">不良信息查询</a>
						<a id="inquireNumber1" href="javascript:showLogRecordForDialog('<ww:property value="subscriberConfirm.subscriber.IDCard" />','<%=path %>','会员信息审核身份证验证')"></a>
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">账号状态</td>
					<td width="70%">
						<select name="subscriberConfirm.subscriber.eventState">
							<option value="0" <ww:if test="subscriberConfirm.subscriber.eventState == 0"></ww:if> >正常</option>
							<option value="4" <ww:if test="subscriberConfirm.subscriber.eventState == 4"></ww:if>>半锁</option>
							<option value="5" <ww:if test="subscriberConfirm.subscriber.eventState == 5"></ww:if>>全锁</option>
						</select>
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">家庭住址</td>
					<td width="70%">
						<select id="s_province" name="subscriberConfirm.subscriber.province"></select>
						<select id="s_city" name="subscriberConfirm.subscriber.city" ></select>
						<select id="s_county" name="subscriberConfirm.subscriber.county"></select>
						<input type="text" size="70%" name="subscriberConfirm.subscriber.address" value="<ww:property value="subscriberConfirm.subscriber.address"/>"/>
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">身份证图片</td>
					<td width="70%">
						<ww:if test="subscriberConfirm.subscriber.idCardImg == null">
			  				<img alt="身份证图片" class="img-responsive tcImg"  src="">
			  			</ww:if>
			  			<ww:else>
				  		 	<a href="<%=path %>/subscriberConfirm/showOriginalImage.action?imageType=idCard&subscriberConfirm.subscriber.idCardImg=<ww:property value='subscriberConfirm.subscriber.idCardImg'/>" target="_blank">
								<img class="img-responsive IDImg img-thumbnail" alt="身份证缩略图"  src="<ww:property value="subscriberConfirm.subscriber.idCardImg"/>">
							</a>
			  			</ww:else>
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">驾驶证图片</td>
					<td width="70%">
						<ww:if test="subscriberConfirm.subscriber.drivingLicenseImg == null">
			  				<img alt="驾照图片" class="img-responsive tcImg" src="" >
			  			</ww:if>
			  			<ww:else>
			  			   	<a href="<%=path %>/subscriberConfirm/showOriginalImage.action?imageType=drivingCard&subscriberConfirm.subscriber.drivingLicenseImg=<ww:property value='subscriberConfirm.subscriber.drivingLicenseImg'/>" target="_blank">
								<img class="img-responsive IDImg img-thumbnail" alt="驾驶证缩略图"  src="<ww:property value="subscriberConfirm.subscriber.drivingLicenseImg"/>">
							</a>
			  			</ww:else>
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">手持身份证图片</td>
					<td width="70%">
						<ww:if test="subscriberConfirm.subscriber.drivingLicenseImg == null">
			  				<img alt="手持身份证图片" class="img-responsive tcImg" src="" >
			  			</ww:if>
			  			<ww:else>
			  			   	<a href="<%=path %>/subscriberConfirm/showOriginalImage.action?imageType=handCard&subscriberConfirm.subscriber.handHeldIdCard=<ww:property value='subscriberConfirm.subscriber.handHeldIdCard'/>" target="_blank">
								<img class="img-responsive IDImg img-thumbnail" alt="未上传"  src="<ww:property value="subscriberConfirm.subscriber.handHeldIdCard"/>">
							</a>
			  			</ww:else>
					</td>
				</tr>
				<tr>
					<td width="24%" class="tsize">审核结果</td>
					<td width="70%">
						<select name="subscriberConfirm.isComplete" id="select_id">
							<option value=1>通过</option>
							<option value=0>不通过</option>
						</select>
					</td>
				</tr>
				<tr id="non_tr">
				    <td width="24%" class="tsize">不通过原因</td>
				    <td width="70%">
				    	<textarea cols="73%" rows="5" id="totot" name="subscriberConfirm.resultDesc"></textarea><br/>
					    <a  id="on_tos">选择不同过原因</a>
					    <div class="rsize">限输入200字，已输入0字</div>
					    <div id="dislp">
						    <form>
						      <div class="linees" title="抱歉，请您重新上传身份证件照片。建议在拍摄时，从正面拍摄，将身份证件的四边拍摄到照片里面，避免证件反光，照片需清晰可见全部信息。"><input type="radio" name="thereason" id="problem1" value="抱歉，请您重新上传身份证件照片。建议在拍摄时，从正面拍摄，将身份证件的四边拍摄到照片里面，避免证件反光，照片需清晰可见全部信息。"><label for="problem1">身份证未拍全</label></div>
						      <div class="linees" title="抱歉，请您重新上传驾驶证件照片。建议在拍摄驾驶证照片时，将驾驶证的正页和副页（有档案编号的一页）从皮夹中取出，上下排列并从正面拍摄，拍摄时需将驾驶证的四边拍摄到照片里面，注意避免证件反光，照片需清晰可见全部信息。"><input type="radio" name="thereason" id="problem2" value="抱歉，请您重新上传驾驶证件照片。建议在拍摄驾驶证照片时，将驾驶证的正页和副页（有档案编号的一页）从皮夹中取出，上下排列并从正面拍摄，拍摄时需将驾驶证的四边拍摄到照片里面，注意避免证件反光，照片需清晰可见全部信息。"><label for="problem2">驾驶证未拍全</label></div>
						      <div class="linees" title="请您重新上传真实有效的证件照片，在拍摄时，从正面拍摄，将证件的四边拍摄到照片里面，避免证件反光，照片需清晰可见全部信息。"><input type="radio" name="thereason" id="problem3" value="请您重新上传真实有效的证件照片，在拍摄时，从正面拍摄，将证件的四边拍摄到照片里面，避免证件反光，照片需清晰可见全部信息。"><label for="problem3">上传其他图片</label></div>
						      <div class="linees" title="抱歉，请您按照提示重新上传照片，第一张为身份证正面，第二张是驾驶证正、副页一起拍照上传，建议您在拍摄时，从正面拍摄，将证件的四边拍摄到照片里面，避免证件反光，照片需清晰可见全部信息。"><input type="radio" name="thereason" id="problem4" value="抱歉，请您按照提示重新上传照片，第一张为身份证正面，第二张是驾驶证正、副页一起拍照上传，建议您在拍摄时，从正面拍摄，将证件的四边拍摄到照片里面，避免证件反光，照片需清晰可见全部信息。"><label for="problem4">证件上传顺序不正确</label></div>
						      <div class="linees" title="抱歉，您提供的证件被遮挡，请在拍摄时将证件四边内的内容全部包含到照片中，同时避免证件反光，照片需清晰可见全部信息。"><input type="radio" name="thereason" id="problem5" value="抱歉，您提供的证件被遮挡，请在拍摄时将证件四边内的内容全部包含到照片中，同时避免证件反光，照片需清晰可见全部信息。"><label for="problem5">证件信息有遮挡</label></div>
						      <div class="linees" title="抱歉，由于您拍摄证件时角度的原因，导致您上传的证件照片上有反光点，请您在光线柔和的环境下重新拍摄，照片需清晰并包含全部证件信息。"><input type="radio" name="thereason" id="problem6" value="抱歉，由于您拍摄证件时角度的原因，导致您上传的证件照片上有反光点，请您在光线柔和的环境下重新拍摄，照片需清晰并包含全部证件信息。"><label for="problem6">证件反光</label></div>
						      <div class="linees" title="抱歉，由于您上传的证件照片不清晰，建议您在拍摄时将拍摄焦点锁定在证件中心位置上，拍摄时避免阴影和反光，证件照片需清晰并包含全部信息。"><input type="radio" name="thereason" id="problem7" value="抱歉，由于您上传的证件照片不清晰，建议您在拍摄时将拍摄焦点锁定在证件中心位置上，拍摄时避免阴影和反光，证件照片需清晰并包含全部信息。"><label for="problem7">证件模糊</label></div>
						      <div class="linees" title="您好，由于您的驾驶证（即将/已）过期，暂时无法对您身份进行审核认证，请您在更换驾驶证之后再重新上传。"><input type="radio" name="thereason" id="problem8" value="您好，由于您的驾驶证（即将/已）过期，暂时无法对您身份进行审核认证，请您在更换驾驶证之后再重新上传。"><label for="problem8">驾驶证即将过期或已过期</label></div>
						      <div class="linees" title="抱歉，因在公安系统内验证不到您的身份证信息，暂时不能审核通过，请您到当地公安局证件核发处核实原因，如有疑问请致电客服4008881212。"><input type="radio" name="thereason" id="problem9" value="抱歉，因在公安系统内验证不到您的身份证信息，暂时不能审核通过，请您到当地公安局证件核发处核实原因，如有疑问请致电客服4008881212。"><label for="problem9">核验无身份证信息</label></div>
						      <div class="linees" title="抱歉，因查询不到您驾驶证信息，暂时不能审核通过，请您到当地交管局核实原因后再进行上传，如有疑问请致电客服4008881212。"><input type="radio" name="thereason" id="problem10" value="抱歉，因查询不到您驾驶证信息，暂时不能审核通过，请您到当地交管局核实原因后再进行上传，如有疑问请致电客服4008881212。"><label for="problem10">核验无驾驶证信息</label></div>
						      <div class="linees" title="抱歉，因部分地区系统升级，我们无法查询到您身份证信息，暂时不能审核通过，如有疑问请致电客服4008881212。"><input type="radio" name="thereason" id="problem11" value="抱歉，因部分地区系统升级，我们无法查询到您身份证信息，暂时不能审核通过，如有疑问请致电客服4008881212。"><label for="problem11">特殊地区</label></div>
						      <div class="linees" title="抱歉，经过我们综合审核评定，您的个人信息不符合电动侠租车会员标准，建议您选择其他方式或渠道租车，给您带来的不便深感抱歉。"><input type="radio" name="thereason" id="problem12" value="抱歉，经过我们综合审核评定，您的个人信息不符合电动侠租车会员标准，建议您选择其他方式或渠道租车，给您带来的不便深感抱歉。"><label for="problem12">有违法记录诚信的</label></div>
						      <div class="linees" title="抱歉，经核实您的驾照累计扣分≥9分，我们暂时不能审核通过您的信息，建议您先将记分清除，再重新提交认证。如疑问请致电客服4008881212。"><input type="radio" name="thereason" id="problem13" value="抱歉，经核实您的驾照累计扣分≥9分，我们暂时不能审核通过您的信息，建议您先将记分清除，再重新提交认证。如疑问请致电客服4008881212。"><label for="problem13">驾照记分≥9分</label></div>
						      <input type="button" value="确 定" class="submitlk"  id="btnSubmit" />
						    </form>
					    </div>
				   </td>
			  </tr>
			</table>
		</form>
	</div>
</body>
<script  src="<%=path %>/mobile/common/js/area.js" type="text/javascript"></script>
<script type="text/javascript">
/* 加载区域 */
 _init_area();
$("#s_province option[value='<ww:property value="subscriberConfirm.subscriber.province"/>']").attr("selected",true);
$("#s_province").trigger("change");  
$("#s_city option[value='<ww:property value="subscriberConfirm.subscriber.city"/>']").attr("selected",true);
$("#s_city").trigger("change");  
$("#s_county option[value='<ww:property value="subscriberConfirm.subscriber.county"/>']").attr("selected",true);
$("#s_county").trigger("change");  
/* 查询会员身份验证次数 */
$(function(){ 
	var idCard = $("#idCard").val();
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/systemutil/getSysOperateLogByDataIdNumber.action',
		  data:{"dataId":idCard,"modelName":"会员信息审核身份证验证"},
		  dataType: 'json',
		  success: function(data){
			  $("#inquireNumber").html("已查询:"+ data.info + "次");
		  }
	}) 
	
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/systemutil/getSysOperateLogByDataIdNumber.action',
			data : {
				"dataId" : idCard,"modelName" : "会员不良信息查询"
			},
			dataType : 'json',
			success : function(data) {
				$("#inquireNumber1").text("已查询:"+ data.info + "次");
			}
		})
	});
/* 会员身份信息验证 */
function searchIdCard(type){
	var name = $("#name").val();
	var idCard = $("#idCard").val();
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/subscriberConfirm/searchIdCard.action',
		  data:{"name":name,"idCard":idCard,"type":type},
		  dataType: 'json',
		  success: function(data){
			  $.ajax({
				  type: 'POST',
				  url: '<%=path%>/systemutil/getSysOperateLogByDataIdNumber.action',
				  data:{"dataId":idCard,"modelName":"会员信息审核身份证验证"},
				  dataType: 'json',
				  success: function(data1){
					  $("#inquireNumber").html("已查询:"+ data.info + "次");
				  }
			}) 
			$.ajax({
				  type: 'POST',
				  url: '<%=path%>/systemutil/getSysOperateLogByDataIdNumber.action',
				  data:{"dataId":idCard,"modelName":"会员不良信息查询"},
				  dataType: 'json',
				  success: function(data1){
					  $("#inquireNumber1").html("已查询:"+ data.info + "次");
				  }
			})
			alert(data.info);
		  }
	})
}

/* 表单验证 */
$().ready(function (){
	$('#eform').validate({
		errorClass : 'text-danger',
		rules: {
			"subscriberConfirm.subscriber.drivingLicenseNo":{
				required: true,
				isIdCard:true
			},
			"subscriberConfirm.subscriber.IDCard":{
				required: true,
				isIdCard:true
			},
			"subscriberConfirm.subscriber.name":{
				required: true
			},
			"subscriberConfirm.subscriber.phoneNo":{
				required: true
			}
		},
		messages: {
			"subscriberConfirm.subscriber.drivingLicenseNo":{
				required: "请输入驾驶证号！"
				
			},
			"subscriberConfirm.subscriber.IDCard":{
				required: "请输入身份证号！"
			},
			"subscriberConfirm.subscriber.name":{
				required: "请输入姓名！"
			},
			"subscriberConfirm.subscriber.phoneNo":{
				required: "请输入手机号！"
			}
		}
	});
});
/* 身份证和驾驶证号验证 */
jQuery.validator.addMethod("isIdCard", function(value, element) {   
    var idCard = /^\d{15,18}(\d{2}[A-Za-z0-9])?$/;
    return this.optional(element) || (idCard.test(value));
}, "请输入正确的证件号！");

	
	function getForm(){
		return $("#eform");
	}
	
	function isValid(){
		var result = $("#result").val();
		if(result == 1  && $("#eform").valid()){
			return true;
		}else if(result == 0){
			var resultDesc = $("#resultDesc").val();
			if(resultDesc == ""){
				alertinfo("请输入审核不通过原因！");
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}
	
	
</script>

<script type="text/javascript" language="javascript">
 $("#on_tos").click(function(){
	$("#dislp").show(); 
	 });
 $("#btnSubmit").click(function(){
	$("#dislp").hide(); 
	 });
$("#select_id").change(function(){
var checkValue=$("#select_id").val();
   if (checkValue=002){
	   $("#non_tr").show(); 
	   }
	else{
		$("#non_tr").hide();
		}
	 });
 $("#select_id").change(function(){
var checkValue=$("#select_id").val();
   if (checkValue==001){
	   $("#non_tr").hide(); 
	   }
	else{
		$("#non_tr").show();
		}
	 });
</script>
<script type="text/javascript" language="javascript">
 /*------判断radio是否有选中，获取选中的值--------*/
    $(function(){
         $("#btnSubmit").click(function(){
            var val=$('input:radio[name="thereason"]:checked').val();
            if(val==null){
                alert("未选中审核不通过原因!");
                return false;
            }
            else{
                document.getElementById("totot").value=	$('input:radio[name="thereason"]:checked').val();
            }
           });
     });
</script>
</html>
