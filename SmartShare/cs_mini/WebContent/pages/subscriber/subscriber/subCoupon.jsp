<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>会员优惠券列表</title>
<%@ include file="/pages/common/common_head.jsp"%>
</head>
<body class="SubPage">
	<div class="row TableBlock">
		<table class="table table-striped table-bordered table-condensed">
			<tr class="ths" id="tab_bg_cl">
				<td>优惠券名称</td>
				<td>金额</td>
				<td>数量</td>
				<td>是否使用</td>
				<td>有效期</td>
			</tr>
			<ww:iterator value="list" id="data" status="rl">
				<tr style="font-size:12px;">
					<td align="center"><ww:property value="couponName"/></td>
					<td align="center"><ww:property value="price"/></td>
					<td align="center"><ww:property value="allCount"/></td>
					<td align="center">已使用<ww:property value="useCount"/>张</td>
					<td align="center"><ww:property value="endValidityTime"/></td>
				</tr>
			</ww:iterator>
		</table>
	</div>
</body>
</html>