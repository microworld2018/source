<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>APP开屏编辑</title>
<%@ include file="/pages/common/common_head.jsp"%>

<link href="<%=path%>/common/js/cropper/dist/cropper.css" rel="stylesheet">
<script type="text/javascript" src="<%=path%>/common/js/cropper/dist/cropper.js"></script>

<link href="<%=path%>/common/js/cropper/examples/crop-avatar/css/main.css" rel="stylesheet">
<script type="text/javascript"   src="<%=path%>/common/js/cropper/examples/crop-avatar/js/admin_avatar.js" ></script>

<style>
a { text-decoration:none; color:#007FFF;}
.fl_lf { float:left;}
.addinsurance { padding:25px;}
.ttxgl {width:100%; padding:10px 0;}
.ttxgl span { width:140px; line-height:30px; height:30px;}
.ttxgl .input_tex1 { width:800px; line-height:30px; height:30px;}
.ttxgl_2{width:100%; padding:10px 0;}
.ttxgl_2 span { width:140px; line-height:30px; height:30px;}
.ttxgl_2 .line_in2 {width:800px;}
.ttxgl_2 .line_in2 .ri_ghts { float:left; width:100%; padding:10px 0;}
.ttxgl_2 .line_in2 .ri_ghts .siz_tex2 { width:30px; line-height:33px; height:30px; text-align:right;padding-right:4px;}
.ttxgl_2 .line_in2 .ri_ghts .input_tex2 { margin-left:5px; width:755px; line-height:30px; height:30px; }
.ttxgl_2 .line_in2 .ri_ghts .pict_tex2 { width:22px; height:22px; margin:6px 4px 2px 4px;}
.ttxgl_2 .line_in2 .ri_ghts .pict_del2 { width:22px; height:22px; margin:6px 4px 2px 4px;}
.ttxgl_3 {width:100%; padding:30px 0;}
.ttxgl_3 span { width:140px; line-height:30px; height:30px;}
.ttxgl_3 span button { border:0; background:#FFF; cursor:pointer; font-size:14px; color:#39F; padding:5px;}
.ttxgl_3 span button:hover {border:0; background:#FFF;}
.ttxgl_3 span button.hover {border:0; background:#FFF}
.ttxgl_3 .inbox_word { width:800px; }
.ttxgl_4 {width:100%; padding:30px 0;}
.ttxgl_4 .sumber_anniu {float:left; width:100px; text-align:center; color:#ffffff; background:#169BD5;line-height:30px; height:30px; border-radius:5px; margin-left:350px; }
.ttxgl_4 .nots_anniu{float:left; width:100px; text-align:center; color:#000000; background:#CCC;line-height:30px; height:30px; border-radius:5px; margin-left:100px; }
</style>
<script type="text/javascript" charset="gbk" src="<%=path %>/admin/common/js/ueditor.config.js"></script>
<script type="text/javascript" charset="gbk" src="<%=path %>/admin/common/js/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="gbk" src="<%=path %>/admin/common/js/zh-cn.js"></script>
</head>
<body class="SubPage">
<form action="" name="sform" id="sform">
<input type="hidden" name="appStart.id" value='<ww:property value="appStart.id"/>'>
	<div class="addinsurance fl_lf">
		<div class="ttxgl fl_lf">
			<span class="fl_lf">标题：</span> 
			<input type="text" class="input_tex1 fl_lf" id="title" name="appStart.title" value='<ww:property value="appStart.title"/>' />
		</div>
		<div class="ttxgl_2 fl_lf">
			<span class="fl_lf">开屏图片：</span>
			<!-- <div id="queue" class="ttxgl fl_lf">
				<input id="file_upload" name="startImg" type="file" multiple="true" />
				<a href="javascript:$('#file_upload').uploadify('upload')">开始上传</a>&nbsp;
				<a href="javascript:$('#file_upload').uploadify('cancel')">取消所有上传</a>
			</div> -->
			<button type="button" class="" data-toggle="modal" data-target="#avatar-modal"
			        style="height: 150px;width: 100px;">
				<ww:if test="appStart.startImg != null">
			    	<img id="startImg" src='<ww:property value="appStart.startImg"/>' 
			    	style="width: 100px;height: 151px;margin-left: -8px;margin-top: -3px;">
			    </ww:if>
			    <ww:else>
			    	<img id="startImg" src="" 
			    	style="width: 100px;height: 151px;margin-left: -8px;margin-top: -3px;">
			    </ww:else>
			    <input type="hidden" id="startImgUrl" name="appStart.startImg" value='<ww:property value="appStart.startImg"/>'/>
			</button>
		</div>
		
		<div class="ttxgl fl_lf">
			<span class="fl_lf">内容：
			<!-- <button onclick="getPlainTxt()">预 览</button> --></span>
			<!-- <div class="inbox_word fl_lf">
				<div>
					<script id="editor" type="text/plain" style="width:800px;height:400px;" name="appStart.content"></script>
				</div>
			</div> -->
			<input type="text" class="input_tex1 fl_lf" id="content" name="appStart.content" value='<ww:property value="appStart.content"/>' />
		</div>
		<div class="ttxgl fl_lf">
			<span class="fl_lf">发布设置：</span> 
			<span>
				<input type="radio" name="method" id="start" value="1">
				<labelfor="menry1">立即开始</label>
			</span> 
			<span style="margin-left: 90px;">
				<input type="radio" name="method" id="diy" value="0">
				<labelfor="menry2">自定义</label>
			</span>
		</div>
		<div class="ttxgl fl_lf">
			<div class="">
				<span class="fl_lf">开始时间：</span>
				<div class='col-md-5' style="width: 400px;margin-left: -15px;">
			        <div class="form-group">
			            <div class='input-group date' id='datetimepicker6' data-date-format="yyyy-mm-dd hh:ii">
			                <input type='text' id="startTime" name="appStart.startTimeStr" class="form-control" 
			                	   value="<ww:property value="transDateString(appStart.startTime)" />" readonly/>
			                <span class="input-group-addon" style="width: 0px;">
			                    <span class="glyphicon glyphicon-calendar" style="margin-top: -15px;"></span>
			                </span>
			            </div>
			        </div>
			    </div>
			    
			    <span class="fl_lf">结束时间：</span>
				<div class='col-md-5'>
			        <div class="form-group">
			            <div class='input-group date' id='datetimepicker7' data-date-format="yyyy-mm-dd hh:ii">
			                <input type='text' id="endTime" name="appStart.endTimeStr" class="form-control" 
			                	   value="<ww:property value="transDateString(appStart.endTime)" />" readonly/>
			                <span class="input-group-addon" style="width: 0px;">
			                    <span class="glyphicon glyphicon-calendar" style="margin-top: -15px;"></span>
			                </span>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
		
		<div class="ttxgl_4 fl_lf">
			<ww:if test="appStart.status == 2">
				<a href="javascript:history.go(-1);" class="nots_anniu fl_lf" style="margin-left: 473px">返 回</a>
			</ww:if>
			<ww:else>
				<a href="javascript:saveEntity()" class="sumber_anniu fl_lf" >保 存</a> 
				<a href="javascript:cancelOperation()" class="nots_anniu fl_lf">取 消</a>
			</ww:else>
		</div>
	</div>
</form>

</div>


	<script type="text/javascript">
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
   	/* $(function(){
	    var proinfo="<ww:property value="appStart.content"/>";
    	ue.ready(function() {//编辑器初始化完成再赋值  
            ue.setContent(proinfo);  //赋值给UEditor  
        });
   	}) */
	    //var ue = UE.getEditor('editor');
   	
    $(document).ready(function(){
    	/* var ue = UE.getEditor('editor');
    	var proinfo="<ww:property value="appStart.content"/>";
    	ue.ready(function() {//编辑器初始化完成再赋值  
            ue.setContent(proinfo);  //赋值给UEditor  
        }); */  
    	
    	//日期控件
    	$('#datetimepicker6').datetimepicker({
    		language: 'zh-CN',
    	    autoclose: true,
    	    minView: 0,
    	    minuteStep:1
    	});
        $('#datetimepicker7').datetimepicker({
            useCurrent: false,
            language: 'zh-CN',
            autoclose: true,
            minView: 0,
            minuteStep:1
        });
        /* $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
        });
        $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
        }); */
    })
    
    
    
    /* function isFocus(e){
        alert(UE.getEditor('editor').isFocus());
        UE.dom.domUtils.preventDefault(e)
    }
    function setblur(e){
        UE.getEditor('editor').blur();
        UE.dom.domUtils.preventDefault(e)
    }
    function insertHtml() {
        var value = prompt('插入html代码', '');
        UE.getEditor('editor').execCommand('insertHtml', value)
    }
    function createEditor() {
        enableBtn();
        UE.getEditor('editor');
    }
    function getAllHtml() {
        alert(UE.getEditor('editor').getAllHtml())
    }
    function getContent() {
        var arr = [];
        arr.push("使用editor.getContent()方法可以获得编辑器的内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getContent());
        alert(arr.join("\n"));
    }
    function getPlainTxt() {
        var arr = [];
        arr.push("使用editor.getPlainTxt()方法可以获得编辑器的带格式的纯文本内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getPlainTxt());
        alert(arr.join('\n'))
    }
    function setContent(isAppendTo) {
        var arr = [];
        arr.push("使用editor.setContent('欢迎使用ueditor')方法可以设置编辑器的内容");
        UE.getEditor('editor').setContent('欢迎使用ueditor', isAppendTo);
        alert(arr.join("\n"));
    }
    function setDisabled() {
        UE.getEditor('editor').setDisabled('fullscreen');
        disableBtn("enable");
    }

    function setEnabled() {
        UE.getEditor('editor').setEnabled();
        enableBtn();
    }

    function getText() {
        //当你点击按钮时编辑区域已经失去了焦点，如果直接用getText将不会得到内容，所以要在选回来，然后取得内容
        var range = UE.getEditor('editor').selection.getRange();
        range.select();
        var txt = UE.getEditor('editor').selection.getText();
        alert(txt)
    }

    function getContentTxt() {
        var arr = [];
        arr.push("使用editor.getContentTxt()方法可以获得编辑器的纯文本内容");
        arr.push("编辑器的纯文本内容为：");
        arr.push(UE.getEditor('editor').getContentTxt());
        alert(arr.join("\n"));
    }
    function hasContent() {
        var arr = [];
        arr.push("使用editor.hasContents()方法判断编辑器里是否有内容");
        arr.push("判断结果为：");
        arr.push(UE.getEditor('editor').hasContents());
        alert(arr.join("\n"));
    }
    function setFocus() {
        UE.getEditor('editor').focus();
    }
    function deleteEditor() {
        disableBtn();
        UE.getEditor('editor').destroy();
    }
    function disableBtn(str) {
        var div = document.getElementById('btns');
        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            if (btn.id == str) {
                UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
            } else {
                btn.setAttribute("disabled", "true");
            }
        }
    }
    function enableBtn() {
        var div = document.getElementById('btns');
        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
        }
    }
    function getLocalData () {
        alert(UE.getEditor('editor').execCommand( "getlocaldata" ));
    }

    function clearLocalData () {
        UE.getEditor('editor').execCommand( "clearlocaldata" );
        alert("已清空草稿箱")
    } */
</script>
<script type="text/javascript">
$(document).ready(function() { 
	$('input:radio[name="method"]').click(function(){
		if($('input:radio[name="method"]:checked').val() == 1){
			//立即开始
			$("#datetimepicker6").css("pointer-events","none");//不响应单击事件
			$("#datetimepicker7").css("pointer-events","none");
			$("#startTime").val("");
			$("#endTime").val("");
		}else{
			$("#datetimepicker6").css("pointer-events","");//响应单击事件
			$("#datetimepicker7").css("pointer-events","");
		}
	});  
	
}); 


//保存
function saveEntity(){
	var title = $("#title").val();
	if(title == ""){
		alertinfo("请输入标题！");
		return false;
	}
	var startImg = $("#startImg").attr("src");
	if(startImg == ""){
		alertinfo("请上传开屏图片！");
		return false;
	}
	/* var hasContent = UE.getEditor('editor').hasContents();
	if(!hasContent){
		
	} */
	var val=$('input:radio[name="method"]:checked').val();
	if(val == null){
		alertinfo("请进行发布设置！");
		return false;
	}
	
	if($('input:radio[name="method"]:checked').val() == 0){
		var startTime = $("#startTime").val();
		var endTime = $("#endTime").val();
		if(startTime == ""){
			alertinfo("请选择开始时间！");
			return false;
		}
		if(endTime == ""){
			alertinfo("请选择结束时间！");
			return false;
		}
		showLoading();
		$.ajax({
			  type: 'POST',
			  url: '<%=path%>/appStart/saveOrUpdateAppStart.action',
			  data:$('#sform').serialize(),
			  dataType: 'json',
			  success: function(data){
				  if(data.result == 0){
					  alertok("该开屏已保存成功！", function(){
						  window.location.href="<%=path%>/appStart/appStartList.action";
					    });
				  }else{
					  alertinfo("操作失败！");
				  }
			  }, error: function(data){
				  alert("服务器异常");
			  }
			});
	}else{
		alertconfirm("立即开始使用此开屏将结束其他正在使用的开屏，确定立即使用此开屏吗？",function(){
			showLoading();
			$.ajax({
				  type: 'POST',
				  url: '<%=path%>/appStart/saveOrUpdateAppStart.action',
				  data:$('#sform').serialize(),
				  dataType: 'json',
				  success: function(data){
					  if(data.result == 0){
						  alertok("已开始使用该开屏！", function(){
							  window.location.href="<%=path%>/appStart/appStartList.action";
						    });
					  }else{
						  alertinfo("操作失败！");
					  }
				  }, error: function(data){
					  alert("服务器异常");
				  }
				});
			
		});
	}
	
	
}
//取消
function cancelOperation(){
	alertconfirm("取消该操作，将丢失已有内容，确认取消？",function(){
		window.location.href="<%=path%>/appStart/appStartList.action";
	});
}

</script>

<div class="container" id="crop-avatar">
   <!-- Cropping modal -->
		    <div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
		      <div class="modal-dialog modal-lg">
		        <div class="modal-content">
		          <form class="avatar-form" action="<%=path %>/appStart/uploadStartImg.action" enctype="multipart/form-data" method="post">
		          	<input type="hidden" name="imageData" id="imageData">
		            <div class="modal-header">
		                <button class="close" data-dismiss="modal" type="button">&times;</button>
		              <h4 class="modal-title" id="avatar-modal-label">上传开屏图片</h4>
		            </div>
		            <div class="modal-body">
		              <div class="avatar-body">
		
		                <!-- Upload image and data -->
		                <div class="avatar-upload">
		                  <input class="avatar-src" name="avatar_src" type="hidden">
		                  <input class="avatar-data" name="avatar_data" type="hidden">
		                  <label for="avatarInput">上传路径</label>
		                  <input class="avatar-input" id="avatarInput" name="upload" type="file">
		                </div>
		
		                <!-- Crop and preview -->
		                <div class="row">
		                  <div class="col-md-9">
		                    <div class="avatar-wrapper"></div>
		                  </div>
		                  <div class="col-md-3">
		                    <div class="avatar-preview preview-lg"></div>
		                  <!--   <div class="avatar-preview preview-md"></div>
		                    <div class="avatar-preview preview-sm"></div> -->
		                  </div>
		                </div>
		
		                <div class="row avatar-btns">
		                  <div class="col-md-9">
		                    <div class="btn-group">
		                      <button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">左转</button>
		                      <button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15度</button>
		                      <button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30度</button>
		                      <button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45度</button>
		                    </div>
		                    <div class="btn-group">
		                      <button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">右转</button>
		                      <button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15度</button>
		                      <button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30度</button>
		                      <button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45度</button>
		                    </div>
		                  </div>
		                  <div class="col-md-3">
		                    <button class="btn btn-primary btn-block avatar-save" type="submit">完成</button>
		                  </div>
		                </div>
		              </div>
		            </div>
		            <!-- <div class="modal-footer">
		              <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
		            </div> -->
		          </form>
		        </div>
		      </div>
		    </div><!-- /.modal -->
		    <!-- Loading state -->
		    <div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
		  </div>
<!-- 上传图片  end -->
</body>
</html>