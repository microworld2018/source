<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<title>车辆监控系统</title>
<script type="text/javascript" src="http://api.map.baidu.com/api?v=2.0&ak=BtxULUWSmvG50D5GKe0ka9Yk"></script>
<script type="text/javascript" src="http://api.map.baidu.com/library/LuShu/1.2/src/LuShu_min.js"></script>
<script type="text/javascript" src="<%=path%>/common/js/richMarker_min.js"></script>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
$(function(){
	/*时间选择*/
	$(" .TimeSelect").datetimepicker({
		language: 'zh-CN',
		todayHighlight: 'true',
		todayBtn: 'linked',
		minView: 0,
		autoclose: true,
		minuteStep: 1,
		format: "yyyy-mm-dd hh:ii:ss"
	});
});
</script>
</head>
<body class="ddqcjkptPage">
	<div id="carsMapDiv"
		style="width: 100%; height: 100%; margin: 0px 0 0 0px; top: 0px; left: 0px; position: absolute; overflow: hidden;">
		<div class="panel" style="padding: 0px 0px;">
			<div class="panel-heading"></div>
		</div>
		<div
			style="width: 100%; height: 10%; border: 0px solid gray; margin: 0px 0 0 0px; top: 10px; left: 10px; position: absolute;">
			<table id="test">
				<tr>
				<td width="150"><span style="margin-right: 3px;">按</span>
					<select class="form-control" style="display: inline-block; width: 85%;" id="selectTypeSel">
						<option value="0">车牌号码</option><option value="1">车机号</option></select></td>
					<td>：</td>
					<td><div class="nr">
							<input type="text" class="kd form-control"
								id="selectTypeValueInput" />
						</div></td>
					<td>
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">开始时间</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control TimeSelect" name="startTime" id="startTime" value="<ww:property value="startTime"/>">
							</div>
						</div>
					</td>
					<td>
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">结束时间</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control TimeSelect" name="endTime" id="endTime" value="<ww:property value="endTime"/>">
							</div>
						</div>
					</td>
					<td><div class="btn btn-danger" onclick="searchCars();"
							style="margin: 0 10px; padding: 5px 10px;">查询</div></td>


					<td><div class="btn btn-info" onclick="startlushu()"
							style="margin: 0px 10px; padding: 5px 10px;">轨迹回放</div></td>
							
					<td><div class="btn btn-info" onclick="locusPause()"
							style="margin: 0px 10px; padding: 5px 10px;">回放暂停</div></td>
				</tr>
			</table>
		</div>
		<!-- 地图展示Start -->
		<div id="allmap"
			style="width: 100%; border: 1px solid gray; height: 91%; margin: 0px 0 0 0px; top: 58px; left: 0px; position: absolute;">

		</div>
		<!-- 地图展示END -->
	</div>
</body>
<!-- 操作地图专用JS方法 -->
<script type="text/javascript">
	  /* 初始化地图并且加载各种控件 Start*/
	  var map = new BMap.Map("allmap");
	  map.centerAndZoom(new BMap.Point(116.331398,39.897445), 10);
	  map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
	  var top_left_control = new BMap.ScaleControl({anchor: BMAP_ANCHOR_BOTTOM_LEFT});// 左上角，添加比例尺
	  var top_left_navigation = new BMap.NavigationControl();  //左上角，添加默认缩放平移控件
	  map.addControl(top_left_control);
	  map.addControl(top_left_navigation);
	  //添加定位事件
	  var geolocationControl = new BMap.GeolocationControl();
	  geolocationControl.addEventListener("locationSuccess", function(e){
	    // 定位成功事件
	    var address = '';
	    address += e.addressComponent.province;
	    address += e.addressComponent.city;
	    address += e.addressComponent.district;
	    address += e.addressComponent.street;
	    address += e.addressComponent.streetNumber;
	  });
	  geolocationControl.addEventListener("locationError",function(e){
	    // 定位失败事件
	    alert(e.message);
	  });
	  map.addControl(geolocationControl);
	
	//添加城市切换控件
	  var size = new BMap.Size(10, 20);
	  map.addControl(new BMap.CityListControl({
	      anchor: BMAP_ANCHOR_TOP_RIGHT,
	      offset: size,
	      // 切换城市之间事件
	      // onChangeBefore: function(){
	      //    alert('before');
	      // },
	      // 切换城市之后事件
	      // onChangeAfter:function(){
	      //   alert('after');
	      // }
	  }));
	 /* 初始化地图加载控件END */ 
/* ****************************************************************************** */
	/* 定义全局变量 */
	var condition = false;
	var points = [];//设置坐标数组
	var lushu;
	
	/* 查询方法开始 */
	function searchCars(){
		//开始时间
		var startTime = $("#startTime").val();
		var start=new Date(startTime.replace("-", "/").replace("-", "/"));
		
		//结束时间
		var endTime = $("#endTime").val();
		var end=new Date(endTime.replace("-", "/").replace("-", "/")); 
		
		//车牌号或者车机号 
		var carNumber = $("#selectTypeValueInput").val(); 
		
		//获取查询类型(0:车牌号，1:车机号)
		var selectTypeSel = $("#selectTypeSel").val();
		
		//判断开始时间和结束时间是否为空
		 if(startTime == "" || endTime == ""){
			 alert("开始时间和结束时间不能为空！");
			 condition;
			 return  false;
		 }
		
		//判断开始时间是否大于结束时间
	    if(end<start){  
	    	alert("开始时间不能大于结束时间！");
	    	condition;
	        return false;  
	    }
		
		//判断车牌号或者车机号是否为空
		if(carNumber == ""){
			alert("请填写车牌号或者车机号！");
			condition;
	        return false; 
		}
		
		/* 清楚地图覆盖物*/
		map.clearOverlays();
		points = [];//每次查询都清空路线数组
		
		$.ajax({
			  type: 'POST',
			  url: '<%=path%>/carlocus/findLocusCondition.action',
			  data:{"carType":selectTypeSel,"startTime":startTime,"endTime":endTime,"carNumber":carNumber},
			  dataType: 'json',
			  success: function(data){
			 	if(data.result == 0 || data.result == "0"){
			 		var locus = data.info;
					map.centerAndZoom(new BMap.Point(locus[0].longitude,locus[0].latitude), 10);
					var new_point = new BMap.Point(locus[0].longitude,locus[0].latitude);
					map.panTo(new_point);  
			 		for(var i=0;i<locus.length;i++){
			 			points.push(new BMap.Point(locus[i].longitude,locus[i].latitude));
			 		}
		 			console.log(points);
		 			var icon1 = new BMap.Icon('http://source.fooleap.org/marker.png', new BMap.Size(19,25),{anchor: new BMap.Size(9, 25)});//地点
		 			var icon2 = new BMap.Icon('http://source.fooleap.org/power-car.png', new BMap.Size(30, 30), {anchor: new BMap.Size(15, 15)});//动车
		 			var polyline = new BMap.Polyline(points);//创建折线
		 			lushu = new BMapLib.LuShu(map, points, {
		 			  landmarkPois:[
		 			    {lng:points[0].lng,lat:points[0].lat,html:'开始',pauseTime:1},
		 			    {lng:points[points.length-1].lng,lat:points[points.length-1].lat,html:'终点',pauseTime:1},
		 			  ],//显示的特殊点，似乎是必选参数，可以留空，据说要和距原线路10米内才会暂停，这里就用原线的点
		 			  defaultContent: '',//覆盖物内容，这个填上面的特殊点文字才会显示
		 			  speed: 20000,//路书速度
		 			  icon: icon2,//覆盖物图标，默认是百度的红色地点标注
		 			  autoView: true,//自动调整路线视野
		 			  enableRotation: true,//覆盖物随路线走向
		 			});
		 			map.addOverlay(polyline);//覆盖折线到地图上
		 			map.addOverlay(new BMap.Marker(points[0],{icon:icon1}));//覆盖起点标注到地图上
		 			map.addOverlay(new BMap.Marker(points[points.length-1],{icon:icon1}));//覆盖终点标注到地图上
			 		
			 	}else{
			 		alert(data.msg);
			 	}
			  }, error: function(data){
				  alert("服务器异常");
			  }
			});
	}
	
	
	
	function startlushu(){
		if(points.length <  2){
			alert("请选择查询条件！");
			return false;
		}
		lushu.start();//启动路书函数
		lushu.showInfoWindow();
	}
		
	function locusPause(){
		if(points.length < 2){
			alert("请先开始回放轨迹！");
			return false;
		}
		lushu.pause();//路书暂停函数
	}
	/* 查询方法结束 */
</script>
</html>