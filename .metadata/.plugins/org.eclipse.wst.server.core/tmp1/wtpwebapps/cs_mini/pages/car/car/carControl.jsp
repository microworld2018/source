<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>操作记录</title>

<%@ include file="/pages/common/common_head.jsp"%>

<script type="text/javascript">


	//当前GPS数据
	function getGpsInfo(){
		$("#content").html("");
		
		var url = "<%=path%>/car/carsStatus.action";
		var sn_no = $("#sn_no").val();
		
		$.ajax({
			type:'post',
			url:url,
			data:{"sn":sn_no,"type":"gpsInfo"},
			async:false,
			dataType:'json',
			success:function(data){
				
			}
		});
	}
	

	//车辆控制
	function carControl(value){
		
		$("#content").html("");
		
		var url = "<%=path%>/car/carsStatus.action";
		var sn_no = $("#sn_no").val();
		$.ajax({
			type:'post',
			url:url,
			data:{"sn":sn_no,"type":value},
			dataType:'json',
			async:false,
			success:function(data){
				
				var html = "";
				if(data.msg != '' && data.msg != null){
					
					html += "<table class='table table-bordered table-condensed'>";
					html += "<tbody>";
					html += "<tr>";
					html += "<th><span>"+data.msg+"</span></th>";
					html += "</tr>";
					
					html += "</tbody>";
					html += "</table>";
					
					$("#content").append(html);
				}
			}
		})
	}

	//获取车辆状态数据
	function getCarsStatus(){
		var sn_no = $("#sn_no").val();
		var url = "<%=path%>/car/carsStatus.action";
		 $.ajax({
			type:'post',
			url:url,
			data:{"sn":sn_no,"type":"carStatus"},
			dataType:'json',
			async:false,
			success:function(data){
				if(data.result == '1'){
					if(data.info != null && data.info.length > 0){
						
						$("#content").html("");
						var html = "";
						$.each(data.info,function(i){
							
							var obj = data.info[i];
							
							var electricity = obj.electricity;//剩余电量 百分比
							var mileage = obj.mileage;//续航里程
							
							var engineStatus = obj.engineStatus;//发动机状态
							if(engineStatus == '1'){
								engineStatus = "启动";
							}else{
								engineStatus = "关闭";
							}
							
							var chargeStatus = obj.chargeStatus;//充电状态
							
							if(chargeStatus == '1'){
								chargeStatus = "正在充电";
							}else{
								chargeStatus = "未充电";
							}
							
							var totalMileage = obj.totalMileage;//车辆当前总里程
							
// 							html += "<span>剩余电量 百分比:</span><span>"+obj.electricity+"</span>";
							html += "<table class='table table-bordered table-condensed'>";
							html += "<tbody>";
							html += "<tr>";
							html += "<th><span>剩余电量 百分比</span>:</th><td>"+electricity+"</td>";
							html += "<th><span>续航里程（km）</span>:</th><td>"+mileage+"</td>";
							html += "</tr>";
							html += "<tr>";
							html += "<th><span>发动机状态</span>:</th><td>"+engineStatus+"</td>";
							html += "<th><span>充电状态</span>:</th><td>"+chargeStatus+"</td>";
							html += "</tr>";
							
							html += "<tr>";
							html += "<th><span>车辆当前总里程（km）</span>:</th><td colspan=3>"+totalMileage+"</td>";
							html += "</tr>";
							
							html += "</tbody>";
							html += "</table>";
						})
						
						$("#content").append(html);
					}
				}else{
					alert(data.result);
				}
			}
		})
		
	}
	
	
	
</script>

</head>
<body class="SubPage">
	<input type="hidden" id="sn_no" value="<ww:property value="deviceBinding.deviceNo"/>">
	<div class="row SubmitButtonBlock">
			 
		<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="carControl('openDoor');" target="_blank">开中控锁</a></div>	 
			 	
		<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="carControl('closeDoor');" target="_blank" >关中控锁</a></div>
		
		<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="carControl('powerFailure');" target="_blank" >远程断电</a></div>
     </div>
     
     <div class="row SubmitButtonBlock">
     	<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="carControl('restorePower');" target="_blank" >远程恢复供电</a></div>
		
		<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="carControl('lockPowerOff');" target="_blank" >关中控锁并断电</a></div>
		
		<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="carControl('openLockPower');" target="_blank">开中控锁并恢复供电</a></div>
     	
     </div>	
     
     <div class="row SubmitButtonBlock">
     	<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="carControl('findCar');" target="_blank" >远程寻车</a></div>
    
    	<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="getCarsStatus();" target="_blank" >车辆状态数据</a></div>
    	
    	<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button2" onclick="getGpsInfo()" target="_blank" >车辆当前GPS数据</a></div>
    
     </div>
     
     <div class="row SubmitButtonBlock" id="content">
     	
     </div>
     
		
</body>
</html>