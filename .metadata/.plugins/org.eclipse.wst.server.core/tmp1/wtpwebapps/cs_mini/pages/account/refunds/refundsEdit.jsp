<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>退款编辑</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
function cancel(){
	alertconfirm("您有操作未确定，取消则会丢失数据，确定取消？",function (){
		window.location.href="<%=path%>/refund/refundApplication.action";
	});	
}

function unpaidDebit(){
	var id = $("#reid").val();
	var subId = $("#subId").val();
	var money = Number($("#money").val());
	var amount = Number($("#amount").val());
	var tradeOrderNo2 = $("#tradeOrderNo2").val();
	var payType = $("#payType").val();
	alertconfirm("确定退款"+money+"元？",function (){
		if(money > amount){
			alert("退款金额不能大于押金金额！");
			return false;
		}
		if(money < 0){
			alert("退款金额不能为负数！");
			return false;
		}
		$.ajax({
			  type: 'POST',
			  url: '<%=path %>/refund/okRefundRecord.action',
			  data:{"id":id,"subId":subId,"money":money,"tradeOrderNo2":tradeOrderNo2,"payType":payType},
			  dataType: 'json',
			  success: function(data){
				  if(data.result == 0){
					  hideLoading();
					  alert("退款成功！");
					  window.location.href="<%=path%>/refund/refundApplication.action";
				  }else{
					  alert(data.msg);
				  }
			  }
			});
	});
}

</script>

</head>
<body style="overflow-y:auto;" class="sgglPage">
      <div class="tc">
		<form>
			<input type="hidden" name="refundRecord.id" id="reid"
						value="<ww:property value="refundRecord.id" />">
			<input type="hidden" id="subId" value="<ww:property value="refundRecord.subscriber.id" />"/>
		     <input type="hidden" id="amount" value='<ww:property value="refundRecord.accDeposit.amount"/>' />
			<input type="hidden" id="payType" value='<ww:property value="refundRecord.payType"/>'>             
		     <table class="table table-bordered table-condensed">
			 	<tbody>
                <tr>
	                <td class="CaptionTd"><span>手机号</span>:</td>
	                <td class="ContentTd"> 
	                	<ww:property value="refundRecord.subscriber.phoneNo" />
	                </td>  
	                <td class="CaptionTd"><span>姓名</span>:</td>
	                <td>
	                	<ww:property value="refundRecord.subscriber.name" />
	                </td>   
             	</tr>
             	<tr>
	                <td><span>性别</span>:</td>
	                <td>
	                	<ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@SEX_MAN.equals(refundRecord.subscriber.sex)">男</ww:if> 
						<ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@SEX_WOMAN.equals(refundRecord.subscriber.sex)">女</ww:if>
	                </td>  
	                <td><span>审核状态</span>:</td>
	                <td>
	                	<ww:if test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_UNCONFIRMED ==refundRecord.subscriber.state">
							<span class="label label-default">资料未提交</span>
						</ww:if>
						<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_WAIT_CONFIRMED ==refundRecord.subscriber.state">
							<span class="label label-primary">资料待审核</span>
						</ww:elseif>
						<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NO_CONFIRMED ==refundRecord.subscriber.state">
							<span class="label label-warning">审核未通过</span>
						</ww:elseif>
						<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NORMAL ==refundRecord.subscriber.state">
							<span class="label label-success">资料已审核</span>
						</ww:elseif>
						<ww:elseif test="@com.dearho.cs.subscriber.pojo.Subscriber@STATE_NORMAL ==refundRecord.subscriber.state">
							<span class="label label-danger">未知</span>
						</ww:elseif>
	                </td>   
             </tr>
              <tr>
	                <td><span>申请退款金额</span>:</td>
	                <td>
	                	<ww:property value="formatAmount(refundRecord.money)" />
	                </td>   
	                <td><span>应退金额</span>:</td>
	                <td>
	                	<input type="text" id="money" class="form-control" value="<ww:property value="formatAmount(refundRecord.actualMoney)"/>" readonly="true"/>
	                </td> 
             </tr>
             
             <tr>
	                <td><span>申请时间</span>:</td>
	                <td>
	                	<ww:property value="transDateString(refundRecord.createTime)" />
	                </td> 
	                <td><span>退款状态</span>:</td>
	                <td>
	                	<ww:if test="refundRecord.status == 1">
								<span class="label label-default">申请退款</span>
						</ww:if>
						<ww:elseif test="refundRecord.status == 2">
								<span class="label label-primary">退款中</span>
						</ww:elseif>
						<ww:elseif test="refundRecord.status == 3">
								<span class="label label-warning">退款中断</span>
						</ww:elseif>
						<ww:elseif test="refundRecord.status == 4">
								<span class="label label-success">已退款</span>
						</ww:elseif>
	                </td>    
             </tr>
             <tr>
               <td height="33">充值流水号:</td>
               <td><ww:property value="refundRecord.rechargeTradeNo" /></td>
               <td>退款流水号:</td>
               <td><input type="text" id="tradeOrderNo2" name="refundRecord.tradeOrderNo2" class="form-control" maxlength="32"/></td>
             </tr>
             <tr>
               <td height="33">&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
               <td>&nbsp;</td>
             </tr>
	           </tbody>
			 </table>
			<div class="row">
				<div class="col-xs-6 col-xs-offset-3">
					<div class="btt">
                            <div class="qzbtn fl" onclick="unpaidDebit();" style="background-color: #16a085">确&nbsp;&nbsp;定</div>
                            <div class="qzbtn fl" onclick="cancel();">取&nbsp;&nbsp;消</div>
                	</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>