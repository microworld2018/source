<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>车辆选择界面</title>

<%@ include file="/pages/common/common_head.jsp"%>


<script type="text/javascript">
	function searchEntity(){
		$("#sform").submit();
	}
	var models = [];
	function changeModel(thisval){
		$('#car\\.modelId').empty();
		$('#car\\.modelId').append($('<option value="">全部</option>'));
		if(models.length == 0){
			var modelsStr = $('#allModelInput').val();
			if(modelsStr != null && modelsStr != ""){
				var modelStrs = modelsStr.split(',');
				$.each(modelStrs,function(index,modelStr){
					if(modelStr == null || modelStr == ""){
						return true;
					}
					var model = {};
					model.id=modelStr.split(":")[0];
					model.name=modelStr.split(":")[1];
					model.brand=modelStr.split(":")[2];
					models.push(model);
				});
			}
		}
		$.each(models,function(index,model){
			if(thisval != null && thisval != ''){
				if(model.brand == thisval){
					var opt = "<option value='"+model.id+"'>"+model.name+"</option>";
					$('#car\\.modelId').append($(opt));
				}
			}
			else{
				var opt = "<option value='"+model.id+"'>"+model.name+"</option>";
				$('#car\\.modelId').append($(opt));
			}
			
			
		});
	}
	
	
	
/* 订单绑定车辆开始 */
function cancelOrderForm(carId,orderId){
	alertconfirm("确认更换车辆？",function (){
		showLoading();
		$.ajax({
			  type: 'POST',
			  url: '<%=path%>/ordercar/updateOrderCar.action',
			  data:{"carId":carId,"orderId":orderId},
			  dataType: 'json',
			  success: function(data){
			 	if(data.result == 0 || data.result == "0"){
			 		window.location.href="<%=path %>/orders/ordersSearch.action";		
			 	}else{
			 		alert(data.msg);
			 	}
			  }, error: function(data){
				  alert("服务器异常");
			  }
			});
	});	
}

/* 返回订单页面 */
function cancel(){
	window.history.back();  
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
			<form name="sform" class="form-horizontal" id="sform" method="post" action="<%=path%>/ordercar/getCarList.action">
				<input type="hidden" name="page.orderFlag" id="page.orderFlag"
						value="<ww:property value="page.orderFlag"/>">
				<input type="hidden" name="page.orderString" id="page.orderString"
						value="<ww:property value="page.orderString"/>">
				<input type="hidden" id="allModelInput" value="
					<ww:iterator value="getAllModel('null')" id="data" status="rl"><ww:property value="id" />:<ww:property value="name" />:<ww:property value="brand" />,</ww:iterator>"/>
				
				<input type="hidden" name="orderId" value="<ww:property value="orderId" />"/>
				
				<div class="ControlBlock">
					<div class="row SelectBlock">
						<div class="col-sm-4 col-xs-6">
							<div class="form-group">
								<label for="cph" class="col-xs-4 control-label">车牌号：</label>
								<div class="col-xs-8">
									<input type="text" id="cph"  class="form-control" name="car.vehiclePlateId" value='<ww:property value="car.vehiclePlateId" />' />
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-xs-6">
							<div class="form-group">
							 	<label for="car.carVehicleModel.brand" class="col-xs-4 control-label">车辆品牌：</label>
							 	<div class="col-xs-8">
								 	<select  class="form-control" onchange="changeModel(this.value)"  name="car.carVehicleModel.brand" id="car.carVehicleModel.brand">
										<ww:iterator value="#dictUtil.getDictSelectsByGroupCode('10',1)" id="data" status="rl">
											<option value="<ww:property value="id" />"  <ww:if test="car.carVehicleModel.brand==id">selected=true</ww:if> ><ww:property value="cnName" /></option>	
										</ww:iterator>
									</select>
								</div>
							 </div>
						</div>
						<div class="row SubmitButtonBlock">
							  <div class="row SubmitButtonBlock">
					 			<ww:if test="hasPrivilegeUrl('/carVehicleModel/carVehicleModelSearch.action')">
									<div class="col-sm-2 col-sm-offset-3 col-xs-4"><a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a></div>
								</ww:if>
								<ww:if test="hasPrivilegeUrl('/carVehicleModel/carVehicleModelSearch.action')">
									<div class="col-sm-2 col-xs-4"><a class="btn btn-block Button1" onclick="cancel();">返回订单</a></div>
								</ww:if>
  				     		</div>
						</div>	
					 </div>
					
  				</div>
				<div class="row TableBlock">
					<table class="table table-striped table-bordered table-condensed">
						<tr class="ths" id="tab_bg_cl">
							<td width="100">
								<a href="javascript:SetOrder('vehiclePlateId')">车牌号<img src="<%=path%>/admin/common/images/main/paixu.png"/></a>
							</td>
							<td width="80">
								车辆品牌
							</td>
							<td width="100">
								车辆型号
							</td>
							<td>
								所属网点
							</td>
							<td>
								车架号
							</td>
							<td width="157">
								操作
							</td>
						</tr>
						<ww:iterator value="page.results" id="data" status="rl">
							<tr
								
								 <ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
								<td align="left">
									<ww:property value="vehiclePlateId" />
								</td>
								
								<td align="left">
									<ww:property value="#dictUtil.getCnNameByGroupCodeAndDictId('10',carVehicleModel.brand)"/>
								</td>
								<td align="left">
									<ww:property value="carVehicleModel.name" />
								</td>
								<td align="left">
									<ww:property value="getBelongDotName(id)" />
								</td>
								<td align="center">
									<ww:property value="vin" />
								</td>
								<td>
									<div class="pan_btn3"  onclick="javascript:cancelOrderForm('<ww:property value="id"/>','<ww:property value="orderId" />');">绑定</div>
								</td>
							</tr>
						</ww:iterator>
						<tr style="background-color: #fff;height: 30px;">
							<td align="center" colspan="8">
								<ww:property value="page.pageSplit" />	
							</td>
						</tr>
					</table>
				</div>
			</form>
	</div>
</body>
</html>