<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>订单数据报表</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
$(function(){
	/*时间选择*/
	$("#sform .TimeSelect").datetimepicker({
		language: 'zh-CN',
		todayHighlight: 'true',
		todayBtn: 'linked',
		minView: 4,
		autoclose: true,
		minuteStep: 1,
		format: "yyyy-mm-dd"
	});
	var payType1 = "<ww:property value="payType"/>";
	$("#payType option[value='"+payType1+"']").attr("selected", true);
});
//搜索功能
function searchEntity(){
	$("#sform").attr("action", "<%=path%>/orders/lokOrdersDataReprot.action");
	$("#sform").submit();
}

function exportEntity(){
	$("#sform").attr("action", "<%=path%>/orderdata/getorderdata.action");
	$("#sform").submit();
	$("#sform").attr("action", "<%=path%>/orders/lokOrdersDataReprot.action");
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post"
			action="">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">承租人</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control" name="memberName" value="<ww:property value="memberName"/>"/>
							</div>
						</div>
						
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="endTime" class="col-xs-4 control-label">联系方式</label>
							<div class="col-xs-8">
								<input type="text" class="form-control" name="phone" value="<ww:property value="phone"/>"/>
							</div>
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">车牌号</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control" name="carNumber" value="<ww:property value="carNumber"/>"/>
							</div>
						</div>
					</div>
				</div>
				<div class="row SelectBlock">
					<div class="col-xs-3" style="margin-top: 15px;">
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">开始时间</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control TimeSelect" name="startTime" id="startTime" value="<ww:property value="startTime"/>">
							</div>
						</div>
					</div>
					<div class="col-xs-3" style="margin-top: 15px;">
						<div class="form-group">
							<label for="endTime" class="col-xs-4 control-label">结束时间</label>
							<div class="col-xs-8">
								<input type="text" class="form-control TimeSelect" name="endTime" id="endTime" value="<ww:property value="endTime"/>">
							</div>
						</div>
					</div>
					<div class="col-xs-3" style="margin-top: 15px;">
						<div class="form-group">
							<label for="endTime" class="col-xs-4 control-label">支付类型</label>
							<div class="col-xs-8">
								<select  class="form-control" name="payType" id="payType">
									<option value="">全部</option>
									<option value="1">账户支付</option>
									<option value="3">支付宝</option>
									<option value="5">微信</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-xs-3" style="margin-top: 15px;">
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">是否支付</label>
							<div class="col-xs-8">
		    					<select name="payStatus"  class="form-control" id="payStatus">
		    						<option value="" <ww:if test="payStatus == ''">selected="selected"</ww:if>>全部</option>
		    						<option value="1" <ww:if test="payStatus == 1">selected="selected"</ww:if>>已支付</option>
		    						<option value="0" <ww:if test="payStatus == 0">selected="selected"</ww:if>>未支付</option>
		    					</select>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank">
						<i class="fa fa-search"></i>查询</a>
					</div>
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1"   onclick="exportEntity();" target="_blank">
						<i class="fa fa-search"></i>导出Excel文件</a>
					</div>
  				</div>
			</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td>订单编号</td>
						<td>承租人</td>
						<td>联系方式</td>
						<td>租用车辆</td>
						<td>下单时间</td>
						<td>开始时间</td>
						<td>结束时间</td>
						<td>订单金额</td>
						<td>优惠卷金额</td>
						<td>购买保险</td>
						<td>应付金额</td>
						<td>实际支付金额</td>
						<td>支付类型</td>
						<td>支付状态</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" >
							<td align="center"><a href="javascript:showOrderDetailForDialog('<ww:property value="id" />','<%=path%>')"><ww:property value="ordersNo" /></a></td>
							<td align="center"><ww:property value="memberName" /></td>
							<td align="center"><ww:property value="memberPhoneNo" /></td>
							<td align="center"><ww:property value="plateNumber" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(ordersTime,'MM-dd HH:mm')" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(beginTime,'MM-dd HH:mm')" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(endTime,'MM-dd HH:mm')" /></td>
							<td align="center"><ww:property value="totalFee" /></td>
							<td align="center"><ww:property value="couponFee" /></td>
							<td align="center">
								<ww:property value="ordersDetai.insuranceName" />
								</br>
								<ww:property value="ordersDetai.insuranceFee" />
							</td>
							<td align="center"><ww:property value="totalFee" /></td>
							<td align="center"><ww:property value="actualFee" /></td>
							<td align="center"><ww:property value="@com.dearho.cs.sys.util.DictUtil@getCnNameByCode('12',payStyle)" /></td>
							<td align="center"><ww:property value="@com.dearho.cs.sys.util.DictUtil@getCnNameByCode('201',payStatus)" /></td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="100"><ww:property
								value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>