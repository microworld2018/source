<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>订单详情</title>

<%@ include file="/pages/common/common_head.jsp"%>



<script type="text/javascript">
function cancel(){
<%-- 	window.location.href="<%=path %>/orders/ordersSearch.action"; --%>
window.history.back();  
}
</script>

</head>
<body style="overflow-y:auto;" class="sgglPage">
      <div class="tc">
		<form>
			<input type="hidden" name="carAccident.id" id="carAccident.id"
						value="<ww:property value="carAccident.id" />">
		     <table class="table table-bordered table-condensed">
			 	<tbody>
                <tr>
	                <td class="CaptionTd"><span>订单编号</span>:</td>
	                <td class="ContentTd"> <ww:property value="orders.ordersNo" /></td>  
	                <td class="CaptionTd"><span>订单状态</span>:</td>
	                <td><ww:property value="@com.dearho.cs.sys.util.DictUtil@getCnNameByCode('14',orders.state)" /></td>   
             </tr>
             
              <tr>
	                <td><span>车辆型号</span>:</td>
	                <td><ww:property value="orders.vehicleModelName" /></td>  
	                <td><span>车牌号码</span>:</td>
	                <td><ww:property value="orders.plateNumber" /> </td>   
             </tr>
              <tr>
                <td>购买保险:</td>
                <td><ww:property value="orderDetail.insuranceName" /></td>
                <td>保险金额:</td>
                <td><ww:property value="orderDetail.insuranceFee" /></td>
              </tr>
              <tr>
	                <td><span>承租人</span>:</td>
	                <td><ww:property value="orders.memberName" /></td>   
	                <td><span>是否需要发票</span>:</td>
	                <td><ww:if test="orders.isBill==1">需要</ww:if><ww:else>不需要</ww:else> </td> 
             </tr>
             
             <tr>
	                <td><span>还车时间</span>:</td>
	                <td><ww:property value="transDateString(orders.endTime)" /> </td>
	                 <td><span>还车地点</span>:</td>
	                <td><ww:property value="orders.endSiteId" /> </td>   
             </tr>
             <tr>
             		 <td><span>取车时间</span>:</td>
	                <td><ww:property value="transDateString(orders.beginTime)" /></td>  
             		<td><span>取车地点</span>:</td>
             		<td><ww:property value="orders.beginSiteId" /></td>
             </tr>
			 </tbody>
			 </table>
			 <table class="table table-bordered table-condensed SubTable">
			<tbody>
             <tr>
             		<td class="CaptionTd"><span>子订单号</span>:</td>
	                <td class="ContentTd"><ww:property value="orderDetail.ordersDetailNo" /> </td>   
	                
	                <td class="CaptionTd"><span>租赁类型</span>:</td>
	                <td> 
	                	<ww:property value="orderDetail.typeName" /> 
	                </td>   
             </tr>
             <tr>
             		<td><span>开始时间</span>:</td>
	                <td><ww:property value="transDateString(orderDetail.beginTime)" /></td>  
	                <td><span>结束时间</span>:</td>
	                <td><ww:property value="transDateString(orderDetail.endTime)" /> </td>   
             </tr>
           	<tr class="Border">
					<td><span>使用时长</span>:</td>
		               <td><ww:property value="orders.ordersDuration" /> 分钟</td>  
					<td><span>计时费用</span>:</td>
		               <td><ww:property value="formatAmount(orderDetail.timeFee)" /> 元 </td>  
           	</tr>
            <tr class="Border">
					<td><span>使用里程</span>:</td>
		               <td><ww:property value="formatAmount(orderDetail.mileage)" /> 公里 </td>  
					<td><span>里程费用</span>:</td>
		               <td><ww:property value="formatAmount(orderDetail.mileFee)" /> 元</td>  
          	</tr>
             <tr class = "Border">
             		<td><span>优惠劵金额</span>:</td>
             		<td><ww:property value="orders.couponFee"/></td>
             		<td><span>订单总金额</span>:</td>
             		<td><ww:property value="orders.totalFee"/></td>
             </tr>
             <tr class="Border">
	                <td><span>订单状态</span>:</td>
	                <td>
						<ww:property value="#dictUtil.getCnNameByCode('201',orders.payStatus)" />
	                </td>  
					<td><span>实际支付金额</span>:</td>
	                <td>
	                	<ww:property value="formatAmount(orders.tposPayFee)"/> 元 
	                </td>  
             </tr>
			  </tbody>
			 </table>
			<div class="row">
				<div class="col-xs-4 col-xs-offset-4">
				<div class="btt">
                            <div class="qzbtn fl" onclick="cancel();">返&nbsp;&nbsp;回</div>
                         </div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>