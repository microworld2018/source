<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>订单时长分析</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript"
	src="https://img.hcharts.cn/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript"
	src="https://img.hcharts.cn/highcharts/highcharts.js"></script>
<script type="text/javascript"
	src="https://img.hcharts.cn/highcharts/modules/exporting.js"></script>
<script type="text/javascript">
	$(function() {
		var myDate = new Date();
		var year=myDate.getFullYear();
		var month=myDate.getMonth()+1;
		var param = year+"-"+month;
		$("#months option[value='"+month+"']").attr("selected", true);//设置默认选中当前月
		$.ajax({
			  type: 'POST',
			  url: '<%=path %>/orders/orderTime.action',
			  data:{"param":param,"state":1},
			  dataType: 'json',
			  success: function(data){
				  if(data.result == 1){
					  alert("暂无数据！");
					  return false;
				  }
				  var name = [];
				  var number = [];
				  var s = 0;
				  for(var i in data.info){
					  name[s] = i;
					  number[s] = data.info[i];
					  s = s+1;
				  }
				  $('#container').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: '订单时长统计'
				        },
				        xAxis: {
				            categories:["0~2小时","2~4小时","4~8小时","8~24小时","24小时以上"]
				        },
				        series: [{
				            name: '订单数量',
				            data: [data.info["0~2"],data.info["2~4"],data.info["4~8"],data.info["8~24"],data.info["24"]]
				        }],
				        yAxis: {
				            min: 0,
				            title: {
				                text: '订单数量 (单)'
				            }
				        },
				        plotOptions: {
				            column: {
				            	 dataLabels:{
			                            enabled:true // dataLabels设为true
			                        }
				            }
				        },
						credits : {
							enabled : false
						}
				    });
			  }
		});
	});
	
	
	
	
	
	function searchEntity(){
		var y = $("#year").val();
		var m = $("#months").val();
		var param = "";
		var type = 0;
		if(y == "" && m == ""){
			alert("请选择时间！");
			return false;
		}
		if(m == "" && y != ""){
			param = y;
			type = 0;
		}
		if(m != "" && y != ""){
			param = y+"-"+m;
			type = 1;
		}
		$.ajax({
			  type: 'POST',
			  url: '<%=path %>/orders/orderTime.action',
			  data:{"param":param,"state":type},
			  dataType: 'json',
			  success: function(data){
				  if(data.result == 1){
					  alert("暂无数据！");
					  return false;
				  }
				  $('#container').highcharts({
				        chart: {
				            type: 'column'
				        },
				        title: {
				            text: '订单时长统计'
				        },
				        xAxis: {
				            categories:["0~2小时","2~4小时","4~8小时","8~24小时","24小时以上"]
				        },
				        series: [{
				            name: '订单数量',
				            data: [data.info["0~2"],data.info["2~4"],data.info["4~8"],data.info["8~24"],data.info["24"]]
				        }],
				        yAxis: {
				            min: 0,
				            title: {
				                text: '订单数量 (单)'
				            }
				        },
				        plotOptions: {
				            column: {
				            	 dataLabels:{
			                            enabled:true // dataLabels设为true
			                        }
				            }
				        },
						credits : {
							enabled : false
						}
				    });
			  }
			});
	}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
	<form  class="form-horizontal" name="sform" id="sform">
		<div class="ControlBlock">
			<div class="row SelectBlock">
				<div class="col-xs-2">
					<div class="form-group">
						<label for="startTime" class="col-xs-4 control-label" style="text-align:right;padding-right:0;">年:</label>
						<div class="col-xs-8" style="margin-top:5px;padding-left:5px;">
							<select name="year" id="year" style="width:70%;">
								<option value="2017">2017</option>
								<option value="2016">2016</option>
							</select>
						</div>
					</div>
				</div>

				<div class="col-xs-2">
					<div class="form-group">
						<label for="endTime" class="col-xs-4 control-label" style="text-align:right;padding-right:0;">月:</label>
						<div class="col-xs-8" style="margin-top:5px;padding-left:5px;">
							<select id="months" style="width:70%;">
								<option value="">全部</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
								<option value="6">6</option>
								<option value="7">7</option>
								<option value="8">8</option>
								<option value="9">9</option>
								<option value="10">10</option>
								<option value="11">11</option>
								<option value="12">12</option>
							</select>
						</div>
					</div>
				</div>
				<div class="col-xs-2">
				  <a class="btn btn-block Button1" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" onclick="searchEntity();"
						target="_blank"> <i class="fa fa-search"></i>查询
					</a>
				</div>
			</div>
		</div>
		</form>
		<div class="row TableBlock">
			<div id="container" style="min-width:400px;height:400px"></div>
		</div>
	</div>
</body>
</html>