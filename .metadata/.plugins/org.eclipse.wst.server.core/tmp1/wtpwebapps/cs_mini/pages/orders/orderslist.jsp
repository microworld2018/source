<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>订单管理</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
$(function(){
	/*时间选择*/
	$("#sform .TimeSelect").datetimepicker({
		language: 'zh-CN',
		todayHighlight: 'true',
		todayBtn: 'linked',
		minView: 4,
		autoclose: true,
		minuteStep: 1,
		format: "yyyy-mm-dd"
	});
});


function searchEntity(){
	$("#sform").submit();
}

function cancelOrderForm(id,name){
	var pars={
			"id":id
		};
	alertconfirm("确认取消【"+name+"】订单吗？",function (){
		showLoading();
		$.post('<%=path%>/orders/ordersCancel.action',pars,r_cancel,'json').error(requestError);
	});	
}

function r_cancel(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("取消成功！", function(){
		    	$('#sform').submit();		
		    });
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}
function showDetail(id){
	window.location.href="<%=path%>/orders/ordersDetail.action?id="+id+"&type=0";
}

/* 编辑订单详情 */
function ordersDetailEdit(id){
	window.location.href="<%=path%>/orders/ordersDetail.action?id="+id+"&type=1";
}


//打开车辆选择页面
function updateCar(id){
	window.location.href="<%=path%>/ordercar/getCarList.action?orderId="+id;
}

//打开后台下订单页面
function openAddOrder(){
	window.location.href="<%=path %>/pages/orders/orderAdd.jsp";
}

//查看订单车辆轨迹
function lookCarLocus(id){
	<%-- window.location.href="<%=path%>/carlocus/openOrderCarLocus.action?orderId="+id; --%>
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/carlocus/viewOrderTrajectory.action',
		  data:{"orderId":id},
	      type: 'get',
		  dataType: 'json',
		  success: function(data){
			  if(data.result == 0 || data.result == "0"){
				  window.location.href="<%=path%>/carlocus/openOrderCarLocus.action?orderId="+id;
			  }else{
				  alertinfo(data.msg);
			  }
		  }
		});
}



function okReturnCar(id){
		var dialoguser = $.dialog({
		    id:'subscriberdailogid', 
		    title:'确认还车',
		    content : "url:<%=path%>/orders/getDotList.action?orderId="+id,
			resize:false,
			fixed:true,
			width:900,
			height:650,
		    lock: true,
	 		max: false,
		    min: false,
		    close: function () {
		        this.hide();
		        restoreInfo('hospitalinfo');
		        return true;
		    },
		    init: function(){
		    	if (typeof this.content.isError != 'undefined'){
		    		$(":button").slice(0,1).hide();
		    	}
		    }
		});
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post"
			action="<%=path%>/orders/ordersSearch.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">开始时间</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control TimeSelect" name="startTime" id="startTime" value="<ww:property value="startTime"/>">
							</div>
						</div>
						<div class="form-group">
							<label for="orders.ordersNo" class="col-xs-4 control-label">订单编号</label>
							<div class="col-xs-8">
								<input class="form-control" name="orders.ordersNo"
									id="orders.ordersNo" type="text"
									value="<ww:property value="orders.ordersNo"/>">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-xs-4 control-label">支付状态</label>
							<div class="col-xs-8">
								<select class="form-control"   name="orders.payStatus" id="orders.payStatus">
										<option value=""  <ww:if test='orders.payStatus==""'>selected=true</ww:if>>全部</option>
										<option value=1 <ww:if test='orders.payStatus==1'>selected=true</ww:if>>已支付</option>
										<option value=0 <ww:if test='orders.payStatus==0'>selected=true</ww:if>>未支付</option>	
								</select>
							</div>
						</div>
					</div>

					<div class="col-xs-4">
						<div class="form-group">
							<label for="endTime" class="col-xs-4 control-label">结束时间</label>
							<div class="col-xs-8">
								<input type="text" class="form-control TimeSelect" name="endTime" id="endTime" value="<ww:property value="endTime"/>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">车牌号</label>
							<div class="col-xs-8">
								<input type="text" class="form-control" name="carNumber" value="<ww:property value="carNumber"/>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">超时时间</label>
							<div class="col-xs-8">
								<select class="form-control"   name="timeout" id="timeout">
										<option value=""  <ww:if test='timeout==""'>selected=true</ww:if>>全部</option>
										<option value="1" <ww:if test='timeout=="1"'>selected=true</ww:if>>一天</option>
										<option value="3" <ww:if test='timeout=="3"'>selected=true</ww:if>>三天</option>
										<option value="7" <ww:if test='timeout=="7"'>selected=true</ww:if>>七天以上</option>	
								</select>
							</div>
						</div>
						
					</div>

					<div class="col-xs-4">
						<div class="form-group">
							<label for="orders.state" class="col-xs-4 control-label">订单状态</label>
							<div class="col-xs-8">
								<select class="form-control"   name="orders.state" id="orders.state">
									<ww:iterator value="#dictUtil.getDictSelectsByGroupCode('14',1)" id="data" status="rl">
										<option value="<ww:property value="code" />"  <ww:if test="orders.state==code">selected=true</ww:if> ><ww:property value="cnName" /></option>	
									</ww:iterator>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-xs-4 control-label">手机号</label>
							<div class="col-xs-8">
								<input type="text" class="form-control" name="phone" value="<ww:property value="phone"/>">
							</div>
						</div>
					</div>
					
					
				</div>
				
				<div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank">
						<i class="fa fa-search"></i>查询</a>
					</div>
					<div class="col-sm-2 col-xs-4">
						<a class="btn btn-block Button2" onclick="openAddOrder();" target="_blank">
						<i class="fa fa-floppy-o"></i>添加订单</a>
					</div>
  				</div>
				
			</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						
						<td><a href="javascript:SetOrder('ordersNo')">订单编号<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a>
						</td>
						<td><a href="javascript:SetOrder('state')">订单状态<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a></td>
						<td><a href="javascript:SetOrder('payState')">支付状态<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a></td>
						<td>租用车辆</td>
						<td>购买保险</td>
						<td><a href="javascript:SetOrder('ordersTime')">下单时间<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a></td>
						<td><a href="javascript:SetOrder('beginTime')">取车时间<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a></td>
						<td><a href="javascript:SetOrder('endTime')">还车时间<img src="<%=path_common_head %>/admin/common/images/main/paixu.png"/></a></td>
						<td>承租人</td>
						<td>优惠券名称</td>
						<td>优惠券金额</td>
						<td>网点</td>        
						<td>操作</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" <ww:if test="#rl.even"> class="trs"</ww:if>>
							<tr>
							  <td align="center">
								<a href="javascript:showOrderDetailForDialog('<ww:property value="id" />','<%=path%>')"><ww:property value="ordersNo" /></a>
							</td>
							<td align="center"><ww:property value="#dictUtil.getCnNameByCode('14',state)" /></td>
							<td align="center"><ww:property value="#dictUtil.getCnNameByCode('201',payStatus)" /></td>
							<td align="center"><a href="javascript:showCarDetailForDialog('<ww:property value="carId" />','<%=path%>')"><ww:property value="plateNumber" /></a></td>
							<td align="center"><ww:property value="ordersDetai.insuranceName" /></br><ww:property value="ordersDetai.insuranceFee" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(ordersTime,'MM-dd HH:mm')" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(beginTime,'MM-dd HH:mm')" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(endTime,'MM-dd HH:mm')" /></td>
							<td align="center"><a href="javascript:showSubscriberDetailForDialog('<ww:property value="memberId" />','<%=path%>')"><ww:property value="memberName" /></a></td>
							<td align="center"><ww:property value="couponName" /></td>
							<td align="center"><ww:property value="formatAmount(couponFee)" /></td>
							<td align="center"><ww:property value="branchDotName" /></td>      <%-- zhanght 2017-5-23add--%>
							<td align="center">
								<ww:if test="hasPrivilegeUrl('/orders/ordersCancel.action')">
									<ww:if test="state.equals(\"1\")||state.equals(\"3\")">
										<div class="pan_btn3"  onclick="javascript:cancelOrderForm('<ww:property value="id"/>','<ww:property value="ordersNo"/>');">取消订单 </div>	
									</ww:if>	
								</ww:if>	
								
								
								<ww:if test="hasPrivilegeUrl('/ordercar/getCarList.action')">
									<ww:if test="state.equals(\"1\")">
										<div class="pan_btn3"  onclick="javascript:updateCar('<ww:property value="id"/>');">更换车辆 </div>	
									</ww:if>	
								</ww:if>
								
								
								<ww:if test="hasPrivilegeUrl('/orders/ordersDetail.action')">
									<ww:if test="state.equals(\"1\")||state.equals(\"3\")||state.equals(\"4\")">
										<div class="pan_btn4"  onclick="javascript:showDetail('<ww:property value="id"/>');">详情</div> 	
									</ww:if>
								</ww:if>
								<ww:if test="state.equals(\"1\")||state.equals(\"3\")||state.equals(\"4\")">
										<div class="pan_btn3"  onclick="javascript:lookCarLocus('<ww:property value="id"/>');">轨迹查看</div>	
								</ww:if>
								<%-- <ww:if test="hasPrivilegeUrl('/orders/ordersDetail.action')">
									<ww:if test="state.equals(\"3\")">
										<div class="pan_btn4"  onclick="javascript:okReturnCar('<ww:property value="id"/>');">确认还车</div> 	
									</ww:if>
								</ww:if> --%>
								<ww:if test="hasPrivilegeUrl('/orders/ordersDetail.action')">
									<div class="pan_btn4"  onclick="javascript:ordersDetailEdit('<ww:property value="id"/>');">编辑</div> 	
								</ww:if>
							</td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="13"><ww:property value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>