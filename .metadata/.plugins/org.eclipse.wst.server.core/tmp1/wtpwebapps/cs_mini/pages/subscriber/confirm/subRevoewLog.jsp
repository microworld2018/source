<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>会员审核记录表</title>
<%@ include file="/pages/common/common_head.jsp"%>
</head>
<body class="SubPage">
	<div class="row TableBlock">
		<table class="table table-striped table-bordered table-condensed">
			<tr class="ths" id="tab_bg_cl">
				<td>身份证号</td>
				<td>审核人姓名</td>
				<td>审核时间</td>
				<td>审核状态</td>
				<td>锁定状态</td>
				<td>审核不通过原因</td>
			</tr>
			<ww:iterator value="list" id="data" status="rl">
				<tr style="font-size:12px;">
					<td align="center"><ww:property value="idCard"/></td>
					<td align="center"><ww:property value="reviewUserName"/></td>
					<td align="center"><ww:property value="reviewTime"/></td>
					<td align="center">
						<ww:if test="reciewIsPassing == 0">审核不通过</ww:if>
						<ww:if test="reciewIsPassing == 1">审核通过</ww:if>
					</td>
					<td align="center">
						<ww:if test="lockedState == 0">正常</ww:if>
						<ww:if test="lockedState == 4">半锁</ww:if>
						<ww:if test="lockedState == 5">全锁</ww:if>
					</td>
					<td>
						<ww:property value="reviewReason" />
					</td>
				</tr>
			</ww:iterator>
		</table>
	</div>
</body>
</html>