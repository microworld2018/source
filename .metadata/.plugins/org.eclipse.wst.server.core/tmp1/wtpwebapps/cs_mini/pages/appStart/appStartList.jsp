<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>保险规则列表</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
/* 根据条件查询保险规则 */
function searchEntity(){
	$("#sform").submit();
}
/* 立即结束开屏操作 */
function over(id){
	alertconfirm("立即结束使用此开屏将无法再恢复使用，确定立即结束此开屏吗？",function (){
		showLoading();
		$.post('overAppStart.action',{"appStart.id":id},function(data){
			hideLoading();
			switch(data.result){
				case 0:
					alertok("已结束使用该开屏！", function(){
						$("#sform").submit();
				    });
					break;
				case 1:
					restoreInfo();
					alerterror(data.msg);
					break;
				case 9:
					document.location = "doError.action";
					break;
			}
		},'json').error(requestError);
	});
}
/* 立即开始开屏操作 */
function start(id){
	alertconfirm("立即开始使用此开屏将结束其他正在使用的开屏，确定立即使用此开屏吗？",function (){
		showLoading();
		$.post('startAppStart.action',{"appStart.id":id},function(data){
			hideLoading();
			switch(data.result){
				case 0:
					alertok("已开始使用该开屏！", function(){
						$("#sform").submit();
				    });
					break;
				case 1:
					restoreInfo();
					alerterror(data.msg);
					break;
				case 9:
					document.location = "doError.action";
					break;
			}
		},'json').error(requestError);
	});
}

/* 删除开屏*/
function del(id,title){
	alertconfirm("确认删除（"+title+"）开屏吗？",function (){
		showLoading();
		$.post('delAppStart.action',{"appStart.id":id},r_delete,'json').error(requestError);
	});	
}
/* 删除结果反馈 */
function r_delete(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("删除成功！", function(){
				$("#sform").submit();
		    });
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}
/* 打开添加或者编辑开屏页面 */
function saveAppStart(id,state){
	window.location.href="<%=path%>/appStart/getByIdAppStart.action?id="+id+"&state="+state;
}

</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path%>/appStart/appStartList.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
			<div class="row SelectBlock">
				<div class="col-xs-4">
					<div class="form-group">
						<label for="appStart.title" class="col-xs-4 control-label">标题关键字:</label>
						<div class="col-xs-8" style="padding-left:5px;">
							<input type="text" class="form-control" name="appStart.title" value="<ww:property value="appStart.title"/>">
						</div>
					</div>
				</div>
				<div class="col-xs-3">
				  <a class="btn btn-block Button1" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" 
				     onclick="searchEntity();" target="_blank"> <i class="fa fa-search"></i>查询
					</a>
				</div>
				<div class="col-xs-3">
				  	<a class="btn btn-block Button9" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" 
				  	   onclick="saveAppStart('<ww:property value="id"/>','save');" target="_blank"> <i class="fa fa-plus-circle"></i>新增
					</a>
				</div>
			</div>
		</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td>缩略图</td>
						<td>APP开屏页面标题</td>
						<td>操作</td>
						<!-- <td>创建时间</td>
						<td>更新时间（最近一次）</td>
						<td>操作</td> -->
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" >
							<td align="center">
								<ww:if test="startImg != null">
								   <img class="img-responsive IDImg img-thumbnail" alt="开屏缩略图" src="<ww:property value="startImg"/>" style="width: 56px;height: 100px;">
						  		</ww:if>
							</td>
							
							<td align="center" style="font-size: larger;">
								<ww:property value="title"/>&nbsp;&nbsp;&nbsp;&nbsp;
								<ww:if test="status == 0">
									<div class="pan_btn4" onclick="saveAppStart('<ww:property value="id"/>','update');">编辑</div>
								</ww:if>
								<ww:if test="status == 2">
									<div class="pan_btn4" onclick="saveAppStart('<ww:property value="id"/>','update');">查看</div>
								</ww:if>
								<br><br>
								开始时间：<ww:property value="#dateUtil.formatDate(startTime,'yyyy-MM-dd HH:mm:ss')"/>   &nbsp;&nbsp;&nbsp;&nbsp;   
								结束时间：<ww:property value="#dateUtil.formatDate(endTime,'yyyy-MM-dd HH:mm:ss')"/>
							</td>
							
							<td align="center">
								<ww:if test="status == 1">
									<div class="pan_btn4"  onclick="over('<ww:property value="id"/>');">立即结束</div>
								</ww:if>
								<ww:if test="status == 0">
									<div class="pan_btn4"  onclick="start('<ww:property value="id"/>');">立即开始</div> 
									<br><br>
									<div class="pan_btn4"  onclick="del('<ww:property value="id"/>','<ww:property value="title"/>');">删除</div> 
								</ww:if>
								<ww:if test="status == 2">
									<div class="pan_btn4">已结束</div> 
									<br><br>
									<div class="pan_btn4"  onclick="del('<ww:property value="id"/>','<ww:property value="title"/>');">删除</div>
								</ww:if>
							</td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="10"><ww:property value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>