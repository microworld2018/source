<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>保险规则列表</title>
<%@ include file="/pages/common/common_head.jsp"%>
<style>
a { text-decoration:none; color:#007FFF;}
.fl_lf { float:left;}
.addinsurance { padding:25px;}
.ttxgl {width:100%; padding:10px 0;}
.ttxgl span { width:140px; line-height:30px; height:30px;}
.ttxgl .input_tex1 { width:800px; line-height:30px; height:30px;}
.ttxgl_2{width:100%; padding:10px 0;}
.ttxgl_2 span { width:140px; line-height:30px; height:30px;}
.ttxgl_2 .line_in2 {width:800px;}
.ttxgl_2 .line_in2 .ri_ghts { float:left; width:100%; padding:10px 0;}
.ttxgl_2 .line_in2 .ri_ghts .siz_tex2 { width:30px; line-height:33px; height:30px; text-align:right;padding-right:4px;}
.ttxgl_2 .line_in2 .ri_ghts .input_tex2 { margin-left:5px; width:755px; line-height:30px; height:30px; }
.ttxgl_2 .line_in2 .ri_ghts .pict_tex2 { width:22px; height:22px; margin:6px 4px 2px 4px;}
.ttxgl_2 .line_in2 .ri_ghts .pict_del2 { width:22px; height:22px; margin:6px 4px 2px 4px;}
.ttxgl_3 {width:100%; padding:30px 0;}
.ttxgl_3 span { width:140px; line-height:30px; height:30px;}
.ttxgl_3 span button { border:0; background:#FFF; cursor:pointer; font-size:14px; color:#39F; padding:5px;}
.ttxgl_3 span button:hover {border:0; background:#FFF;}
.ttxgl_3 span button.hover {border:0; background:#FFF}
.ttxgl_3 .inbox_word { width:800px; }
.ttxgl_4 {width:100%; padding:30px 0;}
.ttxgl_4 .sumber_anniu {float:left; width:100px; text-align:center; color:#ffffff; background:#169BD5;line-height:30px; height:30px; border-radius:5px; margin-left:350px; }
.ttxgl_4 .nots_anniu{float:left; width:100px; text-align:center; color:#000000; background:#CCC;line-height:30px; height:30px; border-radius:5px; margin-left:450px; }
</style>
<script type="text/javascript" charset="gbk" src="<%=path %>/admin/common/js/ueditor.config.js"></script>
<script type="text/javascript" charset="gbk" src="<%=path %>/admin/common/js/ueditor.all.min.js"> </script>
<script type="text/javascript" charset="gbk" src="<%=path %>/admin/common/js/zh-cn.js"></script>
</head>
<body class="SubPage">
<form action="" name="sform" id="sform">
<input type="hidden" id="number" value='<ww:property value="insuranceRules.insuranceDescList.size()"/>'>
<input type="hidden" name="insuranceRules.id" value='<ww:property value="insuranceRules.id"/>'>
	<div class="addinsurance fl_lf">
		<div class="ttxgl fl_lf">
			<span class="fl_lf">保险名称：</span> <input type="text" class="input_tex1 fl_lf" name="insuranceRules.name" value='<ww:property value="insuranceRules.name"/>' />
		</div>
		<div class="ttxgl_2 fl_lf">
			<span class="fl_lf">保险基础内容：</span>
			<div class="line_in2 fl_lf">
				<ww:iterator value="insuranceRules.insuranceDescList" status="status">
				<input type="hidden" name="insuranceDescIdArr" value='<ww:property value="id"/>'>
					<div class="ri_ghts">
						<input type="text" class="input_tex2 fl_lf" name="insuranceDescArr" value='<ww:property value="content"/>' />
					</div>
				</ww:iterator>
			</div>
		</div>
		<div class="ttxgl_3 fl_lf">
			<span class="fl_lf">保险细则 <br />
			<!-- <button onclick="getPlainTxt()">预 览</button> --></span>
			<div class="inbox_word fl_lf">
				<div>
					<script id="editor" type="text/plain" style="width:800px;height:400px;" name="insuranceRules.insuranceDesc"></script>
				</div>
			</div>
		</div>


		<div class="ttxgl_4 fl_lf" style="text-align: center;">
<!-- 			<a href="javascript:saveEntity()" class="sumber_anniu fl_lf" >保 存</a>  -->
			<a href="javascript:cancelOperation()" class="nots_anniu fl_lf">返回</a>
		</div>
	</div>
</form>
	<script type="text/javascript">
    //实例化编辑器
    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
    var ue = UE.getEditor('editor');
    
    $(document).ready(function(){
    	var ue = UE.getEditor('editor');
    	var proinfo="<ww:property value="insuranceRules.insuranceDesc"/>";
    	ue.ready(function() {//编辑器初始化完成再赋值  
            ue.setContent(proinfo);  //赋值给UEditor  
        });  
    })
    
    
    
    function isFocus(e){
        alert(UE.getEditor('editor').isFocus());
        UE.dom.domUtils.preventDefault(e)
    }
    function setblur(e){
        UE.getEditor('editor').blur();
        UE.dom.domUtils.preventDefault(e)
    }
    function insertHtml() {
        var value = prompt('插入html代码', '');
        UE.getEditor('editor').execCommand('insertHtml', value)
    }
    function createEditor() {
        enableBtn();
        UE.getEditor('editor');
    }
    function getAllHtml() {
        alert(UE.getEditor('editor').getAllHtml())
    }
    function getContent() {
        var arr = [];
        arr.push("使用editor.getContent()方法可以获得编辑器的内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getContent());
        alert(arr.join("\n"));
    }
    function getPlainTxt() {
        var arr = [];
        arr.push("使用editor.getPlainTxt()方法可以获得编辑器的带格式的纯文本内容");
        arr.push("内容为：");
        arr.push(UE.getEditor('editor').getPlainTxt());
        alert(arr.join('\n'))
    }
    function setContent(isAppendTo) {
        var arr = [];
        arr.push("使用editor.setContent('欢迎使用ueditor')方法可以设置编辑器的内容");
        UE.getEditor('editor').setContent('欢迎使用ueditor', isAppendTo);
        alert(arr.join("\n"));
    }
    function setDisabled() {
        UE.getEditor('editor').setDisabled('fullscreen');
        disableBtn("enable");
    }

    function setEnabled() {
        UE.getEditor('editor').setEnabled();
        enableBtn();
    }

    function getText() {
        //当你点击按钮时编辑区域已经失去了焦点，如果直接用getText将不会得到内容，所以要在选回来，然后取得内容
        var range = UE.getEditor('editor').selection.getRange();
        range.select();
        var txt = UE.getEditor('editor').selection.getText();
        alert(txt)
    }

    function getContentTxt() {
        var arr = [];
        arr.push("使用editor.getContentTxt()方法可以获得编辑器的纯文本内容");
        arr.push("编辑器的纯文本内容为：");
        arr.push(UE.getEditor('editor').getContentTxt());
        alert(arr.join("\n"));
    }
    function hasContent() {
        var arr = [];
        arr.push("使用editor.hasContents()方法判断编辑器里是否有内容");
        arr.push("判断结果为：");
        arr.push(UE.getEditor('editor').hasContents());
        alert(arr.join("\n"));
    }
    function setFocus() {
        UE.getEditor('editor').focus();
    }
    function deleteEditor() {
        disableBtn();
        UE.getEditor('editor').destroy();
    }
    function disableBtn(str) {
        var div = document.getElementById('btns');
        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            if (btn.id == str) {
                UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
            } else {
                btn.setAttribute("disabled", "true");
            }
        }
    }
    function enableBtn() {
        var div = document.getElementById('btns');
        var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
        for (var i = 0, btn; btn = btns[i++];) {
            UE.dom.domUtils.removeAttributes(btn, ["disabled"]);
        }
    }
    function getLocalData () {
        alert(UE.getEditor('editor').execCommand( "getlocaldata" ));
    }

    function clearLocalData () {
        UE.getEditor('editor').execCommand( "clearlocaldata" );
        alert("已清空草稿箱")
    }
</script>
<script type="text/javascript">
$(document).ready(function() { 
	var number = $("#number").val();
	if(number == null){
		number = 9;
	}else{
		number = 9-number+1;
	}
	var InputsWrapper   = $("#InputsWrapper"); //Input boxes wrapper ID  
	var AddButton       = $("#AddMoreFileBox"); //Add button ID  
	var x = InputsWrapper.length; //initlal text box count  
	var FieldCount=1; //to keep track of text box added 
	$(AddButton).click(function (e)  //on add input button click  
	{  
	        if(x <= number) //max input box allowed  
	        {  
	        	 FieldCount++; //text box added increment  
	            //add input box  
	           <!-- $(InputsWrapper).append('<div><input type="text" name="mytext[]" id="field_'+ FieldCount +'" value="Text '+ FieldCount +'"/><a href="#" class="removeclass"><input type="button" value="删除"></a></div>');--> 
				$(InputsWrapper).append('<div class="ri_ghts" id="field_'+ FieldCount +'"  value="Text '+ FieldCount +'"><input type="text" class="input_tex2 fl_lf" name="insuranceDescArr" /><div class="pict_del2 fl_lf removeclass"><img src="<%=path %>/admin/common/img/dlss.png"></div></div>') 
	            x++; //text box increment 
	           
	        }  
	return false;  
	});  
	  
	$("body").on("click",".removeclass", function(e){ //user click on remove text  
	        if( x > 1 ) {  
	                $(this).parent('div').remove(); //remove text box  
	                x--; //decrement textbox  
	        }  
	return false;  
	})   
  
}); 


//取消
function cancelOperation(){
	window.location.href="<%=path%>/insurancenew/insuranceRules.action";
}

</script>
</body>
</html>