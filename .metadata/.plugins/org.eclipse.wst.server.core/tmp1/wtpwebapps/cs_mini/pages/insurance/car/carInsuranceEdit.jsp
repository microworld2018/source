<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>车型保险新增</title>
<%@ include file="/pages/common/common_head.jsp"%>
<style>
a {
	text-decoration: none;
	color: #007FFF;
}

.fl_lf {
	float: left;
}

.addmodels {
	width: 100%;
	padding: 25px;
}

.modelsline {
	width: 100%;
	padding: 30px 24px;
	border: 1px solid #EEEEEE;
}

.modelsline span {
	width: 140px;
	line-height: 30px;
	height: 30px;
}

.modelsline .selec_tex1 {
	min-width: 300px;
	line-height: 30px;
	height: 30px;
}

.modelsline .addbuttons {
	margin-left: 100px;
	height: 30px;
}

.modelsline .addbuttons .addbao {
	width: 100px;
	line-height: 29px;
	height: 29px;
	text-align: center;
	color: #ffffff;
	background: #169BD5;
	border-radius: 5px;
	cursor: pointer;
}

.modelsline .addbuttons .menu_top {
	width: 100px;
	height: 30px;
	position: absolute;
}

.modelsline .addbuttons .menu_lists {
	display: none;
	width: 420px;
	text-align: left;
	position: relative;
	z-index: 99999;
	border: 1px solid #CCC;
	left: -10px;
	top: 1px;
	border-radius: 3px;
	background: #ffffff;
	color: #000000;
}

.modelsline .addbuttons .menu_lists .toplabe {
	text-align: left;
	margin-left: 0;
	width: 400px;
	padding: 0 10px;
	height: 40px;
	line-height: 40px;
	background: #CEF3FF;
}

.modelsline .addbuttons .menu_lists .toplabe span {
	width: 40px;
	height: 40px;
	position: absolute;
	right: 0;
	top: 7px;
}

.modelsline .addbuttons .menu_lists .menusbox {
	width: 400px;
	padding: 10px;
}

.modelsline .addbuttons .menu_lists .menusbox .in_chickbox {
	width: 120px;
	padding: 5px 7px 5px 5px;
	font-size: 12px;
}

.modelsline .addbuttons .menu_lists .menusbox .in_chickbox input {
	vertical-align: middle;
}

.modelsline .addbuttons .menu_lists .menusbox .saved_mus {
	float: left;
	width: 70px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	color: #ffffff;
	background: #169BD5;
	margin: 30px 0 20px 100px;
	border-radius: 5px;
	cursor: pointer;
}

.modelsline .addbuttons .menu_lists .menusbox .cancel_mus {
	float: left;
	width: 70px;
	height: 30px;
	line-height: 30px;
	text-align: center;
	color: #000000;
	background: #CCC;
	margin: 30px 0 20px 60px;
	border-radius: 5px;
	cursor: pointer;
}

.modelsbox {
	width: 100%;
	border: 1px solid #EEEEEE;
	margin: 10px 0;
}

.modelsbox .topboxlabe {
	width: 100%;
	padding: 0 10px;
	height: 40px;
	line-height: 40px;
	background: #CEF3FF;
}

.modelsbox .topboxlabe span {
	float: right;
	width: 40px;
	height: 40px;
	color: #F06;
	cursor: pointer;
}

.modelsbox .textboxx {
	width: 860px;
	padding: 20px 44px;
}

.modelsbox .counttopline {
	width: 860px;
	padding: 15px 0px;
}

.modelsbox .counttopline span {
	width: 140px;
	line-height: 30px;
	height: 30px;
}

.modelsbox .counttopline .input_tex1 {
	width: 300px;
	line-height: 28px;
	height: 28px;
}

.modelsbox .counttopline .input_tex1old {
	padding: 0 5px;
	line-height: 30px;
	height: 30px;
}

.modelsbox .counttopline .input_tex1righ {
	float: right;
	line-height: 30px;
	height: 30px;
	padding: 0 5px;
}

.modelsbox .counttopline .input_tex1righ span {
	padding: 0 15px;
}

.modelsbox .counttopbox {
	width: 860px;
	padding: 15px 0px;
}

.modelsbox .counttopbox span {
	width: 140px;
	line-height: 30px;
	height: 30px;
}

.modelsbox .counttopbox .rightboxtext {
	width: 720px;
	line-height: 25px;
	font-size: 12px;
}

.modelsbox .counttopbox .Checknius {
	width: 100px;
	line-height: 30px;
	font-size: 14px;
	text-align: center;
	color: #ffffff;
	background: #169BD5;
	border-radius: 5px;
	cursor: pointer;
}
</style>

</head>
<body>
<input type="hidden" id="insuranceNumber" <ww:if test='carVehicleModel.id != ""'>value="<ww:property value="number" />"</ww:if><ww:else>value="0"</ww:else>/>
	<div class="addmodels fl_lf" id="basis">
		<div class="modelsline fl_lf">
			<span class="fl_lf">车型</span> <select class="selec_tex1 fl_lf" id="carId">
				<ww:iterator value="carVehicleModelList" id="data" status="rl">
					<option value="<ww:property value="id"/>/<ww:property value="name"/>" <ww:if test="carVehicleModel.id == id">selected="selected"</ww:if>><ww:property value="name"/> </option>
				</ww:iterator>
			</select>
			<div class="addbuttons fl_lf">
				<div class="addbao fl_lf" style="margin-left:100px;" id="association" onclick="showInsurance()">+关联保险</div>
				<div class="addbao fl_lf" style="margin-left:50px;"  onclick="saveCarInsurance()">+保存</div>
				<div class="addbao fl_lf" style="margin-left:50px;"  onclick="cancelOperation()">取消</div>
			</div>
		</div>
		<ww:iterator value="carInsuranceNewList" id="data" status="status">
		<input type="hidden" value="<ww:property value="insuranceRules.id" />" id="insurance<ww:property value="#status.index+1"/>">
		<input type="hidden" value="<ww:property value="insuranceRules.name" />" id="insuranceName<ww:property value="#status.index+1"/>">
			<div class="modelsbox fl_lf" id="divId<ww:property value="#status.index+1"/>">
				<div class="topboxlabe">
					<ww:property value="insuranceRules.name" /></br><span id="delopenbox" onclick="delInsurance('divId<ww:property value="#status.index+1"/>','<ww:property value="id"/>')">删除</span>
				</div>
				<div class="textboxx fl_lf">
					<div class="counttopline fl_lf">
						<span class="fl_lf">保费：</span>
						<input class="input_tex1 fl_lf" id="price<ww:property value="#status.index+1"/>" type="text" value='<ww:property value="amount" />'/>
						<div class="input_tex1old fl_lf">元</div>
							<div class="input_tex1righ">
								<span>是否必买：</span> <span>
								<input type="radio" name="Sexmery<ww:property value="#status.index+1"/>" id="menry<ww:property value="#status.index"/>" <ww:if test="isUsed == 1">checked="checked"</ww:if> value="1">
								<labelfor="menry1">必买</label></span> <span>
								<input type="radio" name="Sexmery<ww:property value="#status.index+1"/>" id="menry<ww:property value="#status.index"/>" <ww:if test="isUsed == 0">checked="checked"</ww:if> value="0">
								<labelfor="menry2">非必买</label></span>
							</div>
					</div>
					<div class="counttopbox fl_lf">
						<span class="fl_lf">保险基础内容：</span>
						<div class="rightboxtext fl_lf">
							<ww:iterator  value="insuranceRules.insuranceDescList" id="data" status="rl">
								<ww:property value="content" /></br>
							</ww:iterator>
						</div>
					</div>
					<div class="counttopbox fl_lf">
						<span class="fl_lf">保险细则：</span>
						<div class="Checknius fl_lf" onclick="toViewInsurance('<ww:property value="insuranceRules.id" />')">点击查看</div>
					</div>
				</div>
			</div>
		</ww:iterator>
	</div>
	<script type="text/javascript">
		function showInsurance(){
			$.dialog({
			    id:'useredit', 
			    title:"保险规则列表",
				content : 'url:<%=path%>/insurancenew/insuranceRules.action?state=carInsurance',
				fixed:true,
				width:950,
				height:600,
				resize:false,
		 		max: false,
			    min: false,
			    lock: true,
			    ok: function(){
			    	var form = this.content.getForm();
			    	$.ajax({
			  		  type: 'POST',
			  		  url: '<%=path%>/insurancenew/getSelectInsurance.action',
			  		  data:form.serialize(),
			  		  dataType: 'json',
			  		  success: function(data){
			  			 if(data.result == 0){
			  				var insuranceNumber = $("#insuranceNumber").val();
		  					if(insuranceNumber > 0){
		  						insuranceNumber = insuranceNumber*1+data.info.length*1;
// 		  						alert(insuranceNumber);
		  						$("#insuranceNumber").val(insuranceNumber);
		  					}else{
		  						//alert(123);
		  						$("#insuranceNumber").val(data.info.length);
		  						insuranceNumber = data.info.length;
		  					}
			  				 for(var i=0;i<data.info.length;i++){
			  					//获取保险基础内容
			  					var insuranceBasisList = data.info[i].insuranceDescList;
			  					var insuranceBasis = "";
			  					var k = 1;
			  					if(insuranceBasisList != ""){
			  						for(var j=insuranceBasisList.length-1;j>0;j--){
			  							insuranceBasis = insuranceBasis+k+"、"+insuranceBasisList[j].content+"</br>";
			  							k++;
			  						}
			  					}
			  					var insurance = "<div class=\"modelsbox fl_lf\" id=\"divId"+insuranceNumber+"\"><input type=\"hidden\" value=\""+data.info[i].id+"\" id=\"insurance"+insuranceNumber+"\"><input type=\"hidden\" value=\""+data.info[i].name+"\" id=\"insuranceName"+insuranceNumber+"\"><div class=\"topboxlabe\">"+data.info[i].name+"<span id=\"delopenbox\" onclick=\"delInsurance('divId"+insuranceNumber+"','')\">删除</span></div><div class=\"textboxx fl_lf\">"
			  					+"<div class=\"counttopline fl_lf\"><span class=\"fl_lf\">保费：</span><input id=\"price"+insuranceNumber+"\" class=\"input_tex1 fl_lf\" type=\"text\"  /><div class=\"input_tex1old fl_lf\">元</div>"
			  					+"<div class=\"input_tex1righ\"><span>是否必买：</span><span><input type=\"radio\" name=\"Sexmery"+insuranceNumber+"\" id=\"menry1\" checked=\"checked\" value=\"1\" ><label for=\"menry1\">必买</label></span>"
			  					+"<span><input type=\"radio\" name=\"Sexmery"+insuranceNumber+"\" id=\"menry2\" value=\"0\"><label for=\"menry2\">非必买</label></span></div></div>"
			  					+"<div class=\"counttopbox fl_lf\"><span class=\"fl_lf\">保险基础内容：</span><div class=\"rightboxtext fl_lf\">"+insuranceBasis
			  					+"</div></div><div class=\"counttopbox fl_lf\"><span class=\"fl_lf\">保险细则：</span><div class=\"Checknius fl_lf\" onclick=\"toViewInsurance('"+data.info[i].id+"')\">点击查看</div></div></div></div>";
			  					$("#basis").append(insurance);//追加保险信息
// 			  					$("#association").css("display","none");//隐藏关联按钮 
			  					insuranceNumber--;
			  				 }
			  			 }
			  		  }, error: function(data){
			  			  alert("服务器异常");
			  		  }
			  		});
			    	
			    },
			    okVal:"确定",
			    cancelVal: '关闭',
			    cancel: true,
			    close: function () {
			        this.hide();
			        restoreInfo('hospitalinfo');
			        return true;
			    },
			    init: function(){
			    	if (typeof this.content.isError != 'undefined'){
			    		$(":button").slice(0,1).hide();
			    	}
			    }
			});
		}
		
		/* 取消操作 */
		function cancelOperation(){
			window.location.href="<%=path%>/insurancenew/carInsuranceNewList.action";
		}
		/* 删除保险规则 */
		function delInsurance(id,insId){
			 //判断是否已关联车型
			 if(insId != ""){
				 //如果关联则去后台进行删除
				 $.ajax({
			  		  type: 'POST',
			  		  url: '<%=path%>/insurancenew/insuranceById.action',
			  		  data:{"carInsuranceNew.id":insId},
			  		  dataType: 'json',
			  		  success: function(data){
			  			  if(data.result == 0){
			  				$("#"+id).remove();
			  				var insuranceNumber = $("#insuranceNumber").val();
			  				 $("#insuranceNumber").val(insuranceNumber-1)
			  			  }
			  		  }
				});
			 }else{
				 //未关联直接删除当前div
				 $("#"+id).remove();
					var insuranceNumber = $("#insuranceNumber").val();
					 $("#insuranceNumber").val(insuranceNumber-1)
			 }
		}
		/* 查看保险细则 */
		function toViewInsurance(id){
			var dialoguser = $.dialog({
			    id:'insuranceToView', 
			    title:'保险细则',
				content : 'url:<%=path%>/insurancenew/getByIdInsurance.action?state=toView&id='+id,
				fixed:true,
				width:980,
				height:650,
				resize:false,
		 		max: false,
			    min: false,
			    lock: true,
			    close: function () {
			        this.hide();
			        restoreInfo('hospitalinfo');
			        return true;
			    },
			    init: function(){
			    	if (typeof this.content.isError != 'undefined'){
			    		$(":button").slice(0,1).hide();
			    	}
			    }
			});
		}
		
	/* 保存车辆保险关联关系 */
	function saveCarInsurance(state){
		var carId = $("#carId").val();
		var carModel = carId.split('/');
		var insuranceNumber = $("#insuranceNumber").val();
		if(insuranceNumber == 0){
			alertinfo("请选择保险！");
			return false;
		}
		if(state != ""){
			$.ajax({
		  		  type: 'POST',
		  		  url: '<%=path%>/insurancenew/delCarModelInusrance.action',
		  		  data:{"carVehicleModel.id":carModel[0]},
		  		  dataType: 'json',
		  		  success: function(data){
		  			  if(data.result == 0){
	  					for(var i=1;i <= insuranceNumber;i++){
	  						var price = "price"+i;
	  						var inusranceId = "insurance"+i;
	  						var Sexmery = "Sexmery"+i;
	  						//alert(Sexmery);
	  						var insuranceName = "insuranceName"+i;
	  						price = $("#"+price).val();
	  						var isBuy = $("input:checked[name='"+Sexmery+"']").val();
	  						//alert(isBuy);
	  						inusranceId = $("#"+inusranceId).val();
	  						insuranceName = $("#"+insuranceName).val();
	  						$.ajax({
	  					  		  type: 'POST',
	  					  		  url: '<%=path%>/insurancenew/saveCarInsurance.action',
	  					  		  data:{"carInsuranceNew.carTypeId":carModel[0],"carInsuranceNew.insuranceId":inusranceId,"carInsuranceNew.amount":price,"carInsuranceNew.isUsed":isBuy,"carModelName":carModel[1],"insuranceName":insuranceName},
	  					  		  dataType: 'json',
	  					  		  success: function(data){
	  					  			  if(data.result == 0){
	  					  				 alertok("操作完成跳转回列表！", function(){
	  										  window.location.href="<%=path%>/insurancenew/carInsuranceNewList.action";
	  									    });
	  					  			  }
	  					  		  }
	  						});
	  					}
		  			  }else{
		  				  alertinfo(data.msg);
		  			  }
		  		  }
			});
		}else{
			for(var i=1;i <= insuranceNumber;i++){
					var price = "price"+i;
					var inusranceId = "insurance"+i;
					var Sexmery = "Sexmery"+i;
					var insuranceName = "insuranceName"+i;
					price = $("#"+price).val();
					var isBuy = $("input:checked[name='"+Sexmery+"']").val();
					inusranceId = $("#"+inusranceId).val();
					insuranceName = $("#"+insuranceName).val();
					$.ajax({
				  		  type: 'POST',
				  		  url: '<%=path%>/insurancenew/saveCarInsurance.action',
				  		  data:{"carInsuranceNew.carTypeId":carModel[0],"carInsuranceNew.insuranceId":inusranceId,"carInsuranceNew.amount":price,"carInsuranceNew.isUsed":isBuy,"carModelName":carModel[1],"insuranceName":insuranceName},
				  		  dataType: 'json',
				  		  success: function(data){
				  			  if(data.result == 0){
				  				 alertok("操作完成跳转回列表！", function(){
									  window.location.href="<%=path%>/insurancenew/carInsuranceNewList.action";
								    });
				  			  }
				  		  }
					});
				}
		}
	}
	</script>
</body>
</html>