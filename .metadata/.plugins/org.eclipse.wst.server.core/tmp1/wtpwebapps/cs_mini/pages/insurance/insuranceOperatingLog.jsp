<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>会员审核记录表</title>
<%@ include file="/pages/common/common_head.jsp"%>
</head>
<body class="SubPage">
	<div class="row TableBlock">
		<table class="table table-striped table-bordered table-condensed">
			<tr class="ths" id="tab_bg_cl">
				<td>保险名称</td>
				<td>操作内容</td>
				<td>操作时间</td>
				<td>操作人</td>
			</tr>
			<ww:iterator value="insuranceOperationLogList" id="data" status="rl">
				<tr style="font-size:12px;">
					<td align="center"><ww:property value="name"/></td>
					<td align="center"><ww:property value="operationContent"/></td>
					<td align="center"><ww:property value="#dateUtil.formatDate(time,'yyyy-MM-dd hh:mm:ss')"/></td>
					<td align="center"><ww:property value="creatorName"/></td>
				</tr>
			</ww:iterator>
		</table>
	</div>
</body>
</html>