<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>新增或修改优惠劵信息</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
$(function(){
	/*时间选择*/
	$("#eform .TimeSelect").datetimepicker({
		language: 'zh-CN',
		todayHighlight: 'true',
		todayBtn: 'linked',
		minView: 4,
		autoclose: true,
		minuteStep: 5,
		format: "yyyy-mm-dd"
	});
});



$().ready(function (){
	$("#eform").attr('action','<%=path%>/coupon/couponActionAdd.action');
	$('#eform').validate({
		errorClass : 'text-danger',
		rules: {
			"coupon.couponName": {
				required: true
			},
			"coupon.price":{
				required: true
			},
			"coupon.number":{
				required: true,
				rangelength:[0,10]
			},
			"coupon.orderUserPrice":{
				required: true
			},
			"coupon.sendStartTime":{
				required: true
			}
			,
			"coupon.sendEndTime":{
				required: true
			}
			,
			"coupon.couponType":{
				required: true
			}
			,
			"coupon.validPeriod":{
				required: true,
				rangelength:[0,4]
			}
		},
		messages: {
			"coupon.couponName": {
				required: "请输入优惠券名称！"
			},
			"coupon.price":{
				required: "请输入优惠券金额！"
			}
			,
			"coupon.number":{
				required: "请输入发放数量！",
				rangelength:"不能超过10位数字"
			}
			,
			"coupon.orderUserPrice":{
				required: "请输入可使用订单金额！"
			},
			"coupon.sendStartTime":{
				required: "时间不能为空！"
			}
			,
			"coupon.sendEndTime":{
				required: "时间不能为空！"
			}
			,
			"coupon.couponType":{
				required: "请选择优惠券发放类型！"
			}
			,
			"coupon.validPeriod":{
				required: "请输入优惠卷有效时间！"
			}
		}
	});
	
});

function isValid(){
	if ($("#eform").valid()){
		//发放时间验证
		var sendStarTime = $("#sendStarTime").val();
		var start=new Date(sendStarTime.replace("-", "/").replace("-", "/")); 
		var sendEndTime = $("#sendEndTime").val();
		var end=new Date(sendEndTime.replace("-", "/").replace("-", "/"));
		
		//使用时间验证
		var starTime = $("#starTime").val();
		var start1=new Date(starTime.replace("-", "/").replace("-", "/")); 
		var endTime = $("#endTime").val();
		var end1=new Date(endTime.replace("-", "/").replace("-", "/"));
		
		if(start > end){
			alert("发放开始时间不能大于发放结束时间！");
			return false;
		}
		if(start1 > end1){
			alert("使用开始时间不能大于使用结束时间！");
			return false;
		}
		/* var ss = $("#test").val();
		alert(ss); */
		return true;
	}else{
		return false;
	}
}
function getForm(){
	return $("#eform");
}
</script>
</head>
<body >
	<div class="table_con tanchuang" >
		<form name="eform" id="eform" method="post" action="">
		<input type="hidden" name="coupon.id" value="<ww:property value="coupon.id"/>"/>
			<table class="t1" >
				<tr class="trr" >
					<th style="width: 200px;"><span >优惠券名称：</span></th>
					<td><input type="text" name="coupon.couponName" value="<ww:property value="coupon.couponName"/>"/></td>
				</tr>
				<tr class="trr" >
					<th><span >优惠券金额：</span></th>
					<td><input type="text" name="coupon.price" value="<ww:property value="coupon.price"/>"/></td>
				</tr>
				<tr class="trr" >
					<th><span >可使用订单金额：</span></th>
					<td><input type="text" name="coupon.orderUserPrice" value="<ww:property value="coupon.orderUserPrice"/>"/></td>
				</tr>
				<tr class="trr" >
					<th><span >发行数量：</span></th>
					<td><input type="text" name="coupon.number" value="<ww:property value="coupon.number"/>"/></td>
				</tr>
				<tr class="trr" >
					<th><span >优惠券描述：</span></th>
					<td><input type="text" name="coupon.remark" value="<ww:property value="coupon.remark"/>"/></td>
				</tr>
				<tr class="trr" >
					<th><span >发放类型：</span></th>
					<td>
						<select name="coupon.couponType" id="test" style="width:60%" <ww:if test="coupon.sendNumber > 0">disabled="disabled"</ww:if> >
								<ww:iterator value="#dictUtil.getDictSelectsByGroupCode('202',2)" id="data" status="rl">
									<option value="<ww:property value="code" />"  
									<ww:if test="coupon.couponType.toString()==code">selected=true</ww:if> ><ww:property value="cnName" /></option>	
								</ww:iterator>
						</select>
						<ww:if test="coupon.sendNumber > 0">
						</ww:if>
					</td>
				</tr>
				<tr class="trr" >
					<th><span >可用时间：</span></th>
					<td>
						<select name="coupon.isHoliday" style="width:60%" >
								<option value="0" <ww:if test="coupon.isHoliday == 0">selected=true</ww:if>>通用券</option>
								<option value="2" <ww:if test="coupon.isHoliday == 2">selected=true</ww:if>>假日用券</option>
								<option value="1" <ww:if test="coupon.isHoliday == 1">selected=true</ww:if>>工作日用券</option>
						</select>
					</td>
				</tr>
				<tr class="trr" >
					<th><span >发放开始日期：</span></th>
					<td>
						<input type="text" class="form-control TimeSelect" name="coupon.sendStartTime" id="sendStarTime" value="<ww:property value="#dateUtil.formatDate(coupon.sendStartTime,'yyyy-MM-dd')"/>">
					</td>
				</tr>
				<tr class="trr" >
					<th><span >发放结束日期：</span></th>
					<td>
						<input type="text" class="form-control TimeSelect" name="coupon.sendEndTime" id="sendEndTime" value="<ww:property value="#dateUtil.formatDate(coupon.sendEndTime,'yyyy-MM-dd')"/>">
					</td>
				</tr>
				<tr class="trr" >
					<th><span >使用开始日期：</span></th>
					<td>
						<input type="text" class="form-control TimeSelect" name="coupon.startValidityTime" id="starTime" value="<ww:property value="#dateUtil.formatDate(coupon.startValidityTime,'yyyy-MM-dd')"/>">
					</td>
				</tr>
				<tr class="trr" >
					<th><span >使用结束日期：</span></th>
					<td>
						<input type="text" class="form-control TimeSelect" name="coupon.endValidityTime" id="endTime" value="<ww:property value="#dateUtil.formatDate(coupon.endValidityTime,'yyyy-MM-dd')"/>">
					</td>
				</tr>
				<tr class="trr">
					<th><span>优惠卷有效期</span></th>
					<td>
						<input type="text" name="coupon.validPeriod" value="<ww:property value="coupon.validPeriod"/>"><font color="#FF0000">(天)</font>
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>