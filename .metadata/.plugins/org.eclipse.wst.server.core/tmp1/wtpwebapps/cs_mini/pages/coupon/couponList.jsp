<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>优惠券管理</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
/* 添加优惠券 */
function openAddCoupon(id){
	var dialoguser = $.dialog({
	    id:'useredit', 
	    title:(id == "")?"添加优惠券":"编辑优惠券",
		content : 'url:<%=path%>/coupon/opCouponUpdateView.action?coupon.id='+id,
		fixed:true,
		width:550,
		height:600,
		resize:false,
 		max: false,
	    min: false,
	    lock: true,
	    ok: function(){
	    	var valid = this.content.isValid();
	    	if (valid){
	    		var form = this.content.getForm();
	    		showLoading(parent);
	    		$.post(form.attr("action"),form.serialize(),r_savedata,'json').error(requestError);
	    	}
	    	return false;
	    },
	    okVal:isnull(id)?'添加':'保存',
	    cancelVal: '关闭',
	    cancel: true,
	    close: function () {
	        this.hide();
	        restoreInfo('hospitalinfo');
	        return true;
	    },
	    init: function(){
	    	if (typeof this.content.isError != 'undefined'){
	    		$(":button").slice(0,1).hide();
	    	}
	    }
	});
	
}

function r_savedata(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("操作成功！", function(){
				$('#sform').submit();	
		    });
			break;
		case 1:
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
	return false;
}





/* 发放优惠券 */
function openSendCoupon(id){
	
	window.location.href="<%=path%>/coupon/subscriberList.action?coupon.id="+id;

}


/**查询优惠券*/
function searchEntity(){
	$("#sform").submit();
}

</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post"
			action="<%=path%>/coupon/couponActionList.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">优惠券名称</label>
							<div class="col-xs-8">
		    					<input type="text" class="form-control TimeSelect" name="coupon.couponName" id="startTime" value="<ww:property value="startTime"/>">
							</div>
						</div>
						
					</div>
					<div class="col-xs-4">
						<div class="form-group">
							<label for="endTime" class="col-xs-4 control-label">优惠券金额</label>
							<div class="col-xs-8">
								<input type="text" class="form-control TimeSelect" name="coupon.price" id="endTime" value="<ww:property value="endTime"/>">
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="row SubmitButtonBlock">
				<ww:if test="hasPrivilegeUrl('/coupon/couponActionList.action')">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1"   onclick="searchEntity();" target="_blank">
						<i class="fa fa-search"></i>查询</a>
					</div>
				</ww:if>
				<ww:if test="hasPrivilegeUrl('/coupon/opCouponUpdateView.action')">
					<div class="col-sm-2 col-xs-4">
						<a class="btn btn-block Button2" onclick="openAddCoupon('');" target="_blank">
						<i class="fa fa-floppy-o"></i>添加优惠券</a>
					</div>
				</ww:if>
  				</div>
			</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">
						<td>优惠券名称</td>
						<td>发放类型</td>
						<td>优惠券金额</td>
						<td>可使用订单金额</td>
						<td>发行数量</td>
						<td>发放数量</td>
						<td>发放开始日期</td>
						<td>发放结束日期</td>
						<td>有效时间</td>
						<td>用券时间</td>
						<td>操作</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" >
							<td align="center"><ww:property value="couponName" /></td>
							<td align="center"><ww:property value="#dictUtil.getCnNameByCode('202',couponType)" /></td>
							<td align="center"><ww:property value="price" /></td>
							<td align="center"><ww:property value="orderUserPrice" /></td>
							<td align="center"><ww:property value="number" /></td>
							<td align="center"><ww:property value="sendNumber" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(sendStartTime,'yyyy-MM-dd')" /></td>
							<td align="center"><ww:property value="#dateUtil.formatDate(sendEndTime,'yyyy-MM-dd')" /></td>
							<td align="center"><ww:property value="validPeriod" /></td>
							<td align="center">
								<ww:if test="isHoliday == 0">
									通用券
								</ww:if>
								<ww:if test="isHoliday == 1">
									工作日用券
								</ww:if>
								<ww:if test="isHoliday == 2">
									节假日用券
								</ww:if>
							</td>
							<td align="center">
							<ww:if test="hasPrivilegeUrl('/coupon/opCouponUpdateView.action')">
								<div class="pan_btn3"  onclick="javascript:openAddCoupon('<ww:property value="id"/>')">编辑 </div>
							</ww:if>
							<ww:if test="hasPrivilegeUrl('/coupon/subscriberList.action')">
								<div class="pan_btn3"  onclick="javascript:openSendCoupon('<ww:property value="id"/>')">发放 </div>		
							</ww:if>
							</td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="12"><ww:property value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>