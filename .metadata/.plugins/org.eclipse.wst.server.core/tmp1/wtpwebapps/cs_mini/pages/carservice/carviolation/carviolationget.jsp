<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>车辆违章</title>

<%@ include file="/pages/common/common_head.jsp"%>
<style>
.btt .fl {
	float: left;
	display: inline-block;
	margin: 0 10px;
	cursor: pointer;
	padding: 8px 40px;
	font-size: 12px;
	background: #e86d4d;
	color: #FFF;
	border-radius: 3px;
}

.form-control_2 {
	display: block;
	height: 34px;
	padding: 6px 12px;
	font-size: 12px;
	line-height: 1.42857143;
	color: #555;
	background-color: #fff;
	background-image: none;
	border: 1px solid #ccc;
	border-radius: 4px;
	-webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
	-webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow
		ease-in-out .15s;
	-o-transition: border-color ease-in-out .15s, box-shadow ease-in-out
		.15s;
	transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
}
</style>
<script type="text/javascript">
	$(function(){
		var id = '<ww:property value="id" />';
		var url="";
		if (id == "" || id == "undefined"){
			url="<%=path%>/carservice/carViolationAdd.action";
		}else{
			url="<%=path%>/carservice/carViolationUpdate.action";	
		}
		$("#eform").attr('action',url);
		$('#eform').validate({
			errorClass : 'text-danger',
			rules: {
				"orderId":{
					required: true
				},
				"memberId":{
					required: true
				},
				"vehiclePlateId":{
					required: true
				},
				"carViolation.happenTime":{
					required: true
				},
				"carViolation.money":{
					number: true
				},
				"carViolation.score":{
					number: true
				},
				"carViolation.desc":{
					violationDescSc: true
				}
			},
			messages: {
				"orderId":{
					required: "请选择违章订单!"
				},
				"memberId":{
					required: "请选择违章会员！"
				},
				"vehiclePlateId":{
					required: "请选择违章车辆！"
				},
				"carViolation.happenTime":{
					required: "请选择发生时间！"
				},
				"carViolation.money":{
					number: "请输入数字！"
				},
				"carViolation.score":{
					number: "请输入数字！"
				}
			}
			
		});
		
		val_check_SpecialChar("violationCodeSc,violationDescSc");
		$('.timeselect').datetimepicker({
			language: 'zh-CN',
			todayHighlight: true,
			todayBtn: true,
			minView: 4,
			autoclose: true,
			minuteStep : 1,
			minView : 0,
			format: "yyyy-mm-dd hh:ii",
			endDate:new Date()
		});
	});
	
	
	function isValid(){
		if ($("#eform").valid()){
			return true;
		}else{
			return false;
		}
	}
	//取消按钮
	function cancel(){
		window.location.href="<%=path%>/carservice/carViolationSearch.action";
	}
	
	function sub(){
		var id = '<ww:property value="id" />';
		var url="";
		if (id == "" || id == "undefined"){
			url="<%=path%>/carservice/carViolationAdd.action";
		}else{
			url="<%=path%>/carservice/carViolationUpdate.action";	
		}
		var re=isValid();
		if(re){
			$.post(url,$("#eform").serialize(),r_saveCar,'json').error(requestError);
		}
	}
	function r_saveCar(data){
		switch(data.result){
			case 0:
				alertok(data.msg, function(){
					window.location.href="<%=path%>/carservice/carViolationSearch.action";
			    });
				break;
			case 1:
				alerterror(data.msg);
				break;
			case 9:
				document.location = "doError.action";
				break;
		}
	}
//订单查询方法
function selectOrder(){
	var plateNumber = $("#plateNumber").val();
	var ordersTimeStr = $("#ordersTimeStr").val();
	if(plateNumber == ""){
		alert("请输入车牌号！");
		return false;
	}
	if(ordersTimeStr == ""){
		alert("请选择违章时间！");
		return false;
	}
	$.ajax({
		  type: 'POST',
		  url: '<%=path%>/carservice/searchViolationOrder.action',
			data : {
				"orders.plateNumber" : plateNumber,
				"orders.ordersTimeStr" : ordersTimeStr
			},
			dataType : 'json',
			success : function(data) {
				if (data.result == 1) {
					alert(data.msg);
					return false;
				}
				$("#orderNo").val(data.info.orderNo);
				$("#memberName").val(data.info.memberName);
				$("#carId").val(data.info.carId);
				$("#memberId").val(data.info.memberId);
				$("#orderId").val(data.info.id);
			}
		});
	}
</script>
</head>
<body style="overflow-y: auto;">
	<div class="zhang">
		<form name="eform" id="eform" method="post" action="">
			<input type="hidden" name="carViolation.id" id="carViolation.id"
				value="<ww:property value="carViolation.id" />"> <input
				type="hidden" id="carId" name="carViolation.carId" value='<ww:property value="carViolation.carId"/>'/> <input
				type="hidden" id="memberId" name="carViolation.memberId" value='<ww:property value="carViolation.memberId"/>'/> <input
				type="hidden" id="orderId" name="carViolation.orderId" value='<ww:property value="carViolation.orderId"/>'/>
				<input type="hidden" name="carViolation.isDiscard" value='<ww:property value="carViolation.isDiscard"/>'>
			<div
				style="font-size: 16px; padding-left: 10px; padding-bottom: 10px;">违章查询</div>
			<table class="xxgl" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="150" style="padding-left: 30px;"><span>违章车牌号</span></td>

					<td width="250" style="padding-left: 30px;"><span>违章时间</span></td>
				</tr>
				<tr>
					<td width="200" style="padding-left: 30px;"><input
						style="width: 200px" type="text" id="plateNumber" value='<ww:property value="carViolation.plateNumber"/>'/></td>
					<td width="200" style="padding-left: 30px;"><input
						style="width: 200px" type="text" class="input_size fl timeselect"
						id="ordersTimeStr" name="carViolation.startTime" value='<ww:property value="#dateUtil.formatDate(carViolation.happenTime,'yyyy-MM-dd HH:mm')"/>' /></td>
					<td class="btt">
						<div class="sbtn fl" style="paading: 10px;"  onclick="selectOrder()">查&nbsp;&nbsp;询</div>
					</td>
				</tr>
			</table>
			<hr style="height: 1px; border: none; border-top: 1px solid #e2e2e2;" />
			<div
				style="font-size: 16px; padding-left: 10px; padding-bottom: 10px;">订单信息</div>
			<div style="float: left; margin-right: 30px; padding-left: 30px;">
				订单号：<input type="text" id="orderNo" value='<ww:property value="carViolation.orderCode"/>' <ww:if test='carViolation.id != ""'>readOnly="true" </ww:if>/>
			</div>
			<div style="float: left">
				订车人：<input type="text" id="memberName" value='<ww:property value="carViolation.memberName"/>' <ww:if test='carViolation.id != ""'>readOnly="true" </ww:if>/>
			</div>
			<div style="float: left">
				购买保险：<input type="text" id="insurance" value='<ww:property value="carViolation.insuranceStr"/>' readOnly="true" style="width: 300px"/>
			</div>            
			<div style="clear: both"></div>
			<hr style="height: 1px; border: none; border-top: 1px solid #e2e2e2;" />
			<div
				style="font-size: 16px; padding-left: 10px; padding-bottom: 10px;">违章信息</div>

			<table class="xxgl" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td style="margin-right: 30px; padding-left: 30px;"><span>违章地点</span></td>
				</tr>
				<tr>
					<td width="100%" style="margin-right: 30px; padding-left: 30px;">
						<input class="input_size fl" type="text"
						style="top: 0; left: 0px; position: relative;" maxlength="30"
						name="carViolation.sendAddress" id="carViolation.sendAddress"
						value="<ww:property value="carViolation.sendAddress" />" />
					</td>
				</tr>
				<tr>
					<td style="margin-right: 30px; padding-left: 30px;"><span>违章内容</span></td>
				</tr>
				<tr>
					<td style="margin-right: 30px; padding-left: 30px;"><textarea
							name="carViolation.remark" style="width: 100%" rows="6"
							class="textarea_size"><ww:property
								value="carViolation.remark" /></textarea></td>
				</tr>
				<tr>
					<td width="100%" style="margin-right: 30px; padding-left: 30px;">
						<div style="float: left; width: 30%; margin-right: 20px;">
							<div style="">违章类型（类型代码）</div>
							<input type="text" size="10" name="carViolation.illegalCode" value="<ww:property
								value="carViolation.illegalCode" />"/>
						</div>
						<div style="float: left; width: 10%; margin-right: 20px;">
							<div style="">扣分</div>
							<div style="float: left; width: 80%;">
								<input type="text" name="carViolation.score" value='<ww:property value="carViolation.score"/>'/>
							</div>
							<div style="float: left; padding-top: 10px;">分</div>
						</div>
						<div style="float: left; width: 15%; margin-right: 20px;">
							<div style="">罚款</div>
							<div style="float: left; width: 80%;">
								<input type="text" name="carViolation.money" value='<ww:property value="carViolation.money"/>'/>
							</div>
							<div style="float: left; padding-top: 10px;">元</div>
						</div>
						<div style="float: left; width: 15%; margin-right: 20px;">
							<div style="">处理费</div>
							<input type="text" size="8" name="carViolation.agencyAoney" value='<ww:property value="carViolation.agencyAoney"/>'/>
						</div>
						<div style="float: left; width: 20%; margin-right: 20px;">
							<div style="">处理状态</div>
							<select style="width: 90px;" name="carViolation.bizStatus">
								<option value="0" <ww:if test='carViolation.bizStatus=="0"'>selected="selected"</ww:if>>未处理</option>
								<option value="1" <ww:if test='carViolation.bizStatus=="1"'>selected="selected"</ww:if>>已处理</option>
								<option value="2" <ww:if test='carViolation.bizStatus=="2"'>selected="selected"</ww:if>>代办处理</option>
							</select>
						</div>
					</td>
				</tr>
				<tr></tr>
				<tr>
					<td colspan="4">
						<div class="btt">
							<div class="sbtn fl" onclick="sub();">提&nbsp;&nbsp;交</div>
							<div class="qzbtn fl" onclick="cancel();">取&nbsp;&nbsp;消</div>
						</div>
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>