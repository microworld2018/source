<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>地勤订单详情</title>

<%@ include file="/pages/common/common_head.jsp"%>



<script type="text/javascript">
function cancel(){
<%-- 	window.location.href="<%=path %>/orders/ordersSearch.action"; --%>
window.history.back();  
}
</script>

</head>
<body style="overflow-y:auto;" class="sgglPage">
      <div class="tc">
		<form>
			<input type="hidden" name="carAccident.id" id="carAccident.id" value="<ww:property value="carAccident.id" />">
		    <table class="table table-bordered table-condensed">
			 	<tbody>
                <tr>
	                <td class="CaptionTd"><span>订单编号</span>:</td>
	                <td class="ContentTd"> <ww:property value="groundOrder.orderNo" /></td>  
	                <td class="CaptionTd"><span>订单状态</span>:</td>
	                <td><ww:property value="@com.dearho.cs.sys.util.DictUtil@getCnNameByCode('18',groundOrder.orderState)" /></td>   
                </tr>
             
                <tr>
	                <td><span>使用车辆</span>:</td>
	                <td><ww:property value="groundOrder.vehicleModelName" /></td>  
	                <td><span>车牌号码</span>:</td>
	                <td><ww:property value="groundOrder.carNum" /> </td>   
                </tr>
              <tr>
	                <td><span>用车人</span>:</td>
	                <td><ww:property value="groundOrder.subName" /></td>   
	                <td><span>取车地点</span>:</td>
	                <td><ww:property value="groundOrder.takeDotId" /></td>
              </tr>
             
              <tr>
	                <td><span>预定时间</span>:</td>
	                <td><ww:property value="transDateString(groundOrder.takeTime)" /> </td>
	                 <td><span>还车地点</span>:</td>
	                <td><ww:property value="groundOrder.backDotId" /> </td>   
              </tr>
              <tr>
             		<td><span>支付时间</span>:</td>
	                <td><ww:property value="transDateString(groundOrder.backTime)" /></td>  
             		<td><span>使用时长</span>:</td>
             		<td><ww:property value="orders.ordersDuration" /> 分钟</td>
              </tr>
              <tr>
             		<td><span>使用里程</span>:</td>
	                <td><ww:property value="formatAmount(ordersDetail.mileage)" /> 公里 </td>
             		<td><span>费用</span>:</td>
             		<td><ww:property value="orders.totalFee"/> 元 </td>
              </tr>
			 </tbody>
			 </table>
			<div class="row">
				<div class="col-xs-4 col-xs-offset-4">
				<div class="btt">
                            <div class="qzbtn fl" onclick="cancel();">返&nbsp;&nbsp;回</div>
                         </div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>