<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>监控中心首页</title>
<%@ include file="/pages/common/common_head.jsp"%>

<script type="text/javascript">

function searchEntity(){
	$("#sform").submit();
}
/**车辆操控*/
function carControl(id,state){
	var flag = check_device(id);//校验车辆是否绑定设备
	var url = "<%=path%>/car/carControl.action?id="+id+"&state="+state;
	if(flag){
		 $.dialog({
			    id:'logRecordList', 
			    title:"车辆操控",
				content :"url:"+url,
				fixed:true,
				width:600,
				height:500,
				resize:false,
		 		max: false,
			    min: false,
			    lock: true,
			    close: true,
			    init: function(){
			    	if (typeof this.content.isError != 'undefined'){
			    		$(":button").slice(0,1).hide();
			    	}
			    }
			}); 
	}else{
		alertinfo("当前车辆未绑定设备！");
	}
}
function check_device(id){
	var flag = true;
	var url = "<%=path%>/car/carControl.action";
	
	$.ajax({
		type:'post',
		url:url,
		data:{"id":id},
		dataType:'json',
		async:false,
		success:function(data){
			if(data.result == '0'){
				alert(data.msg);
				flag = false;
			}
		}
	})
	return flag;
}



/* 查看车辆当前位置 */
function openThisLocation(id){
	var flag = check_device(id);//校验车辆是否绑定设备
	if(flag){
		var dialoguser = $.dialog({
		    id:'useredit', 
		    title:"车辆当前位置",
			content : 'url:<%=path%>/monitor/opThisLocation.action?car.id='+ id,
				fixed : true,
				width : 550,
				height : 600,
				resize : false,
				max : false,
				min : false,
				lock : true,
				cancelVal : '关闭',
				cancel : true,
				close : function() {
					this.hide();
					restoreInfo('hospitalinfo');
					return true;
				},
				init : function() {
					if (typeof this.content.isError != 'undefined') {
						$(":button").slice(0, 1).hide();
					}
				}
			});
	}
	

	}

$(function(){
	var dotId = '<ww:property value="dotId" />';
	$.ajax({
		type:'post',
		url:'<%=path%>/monitor/queryDotList.action',
		data:{},
		dataType:'json',
		async:false,
		success:function(data){
			$('#dot').empty();
			for(var i=0;i<data.info.length;i++){  
				if(data.info[i].id == dotId){
					$("<option value='" + data.info[i].id + "' selected='selected'>"+ data.info[i].name + "</option>").appendTo($('#dot'));
				}else{
					$("<option value='" + data.info[i].id + "'>"+ data.info[i].name + "</option>").appendTo($('#dot'));
				}
			}
		}
	})
})



function returnDotList(){
	window.location.href="<%=path %>/monitor/indexMonitor.action";
}
</script>





</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path%>/monitor/dotCar.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
				value="<ww:property value="page.orderFlag"/>"> <input
				type="hidden" name="page.orderString" id="page.orderString"
				value="<ww:property value="page.orderString"/>">
			<div class="ControlBlock">
				<div class="row SelectBlock">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="startTime" class="col-xs-4 control-label">车牌号：</label>
							<div class="col-xs-8">
								<input type="text" class="form-control TimeSelect" name="car.vehiclePlateId" <%-- value="<ww:property value="car.vehiclePlateId" />" --%>>
							</div>
						</div>
					</div>

					<div class="col-xs-4">
						<div class="form-group">
							<label for="endTime" class="col-xs-4 control-label">切换网点：</label>
							<div class="col-xs-8">
								<select class="form-control" name="dotId" id="dot">
								</select>
							</div>
						</div>
					</div>

				</div>

				<div class="row SubmitButtonBlock">
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1" onclick="searchEntity();" target="_blank"> <i class="fa fa-search"></i>查询</a>
					</div>
					<div class="col-sm-2 col-sm-offset-3 col-xs-4">
						<a class="btn btn-block Button1" onclick="returnDotList();" target="_blank">返回</a>
					</div>
				</div>
			</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed">
					<tr class="ths" id="tab_bg_cl">

						<td>车牌号</td>
						<td>车辆品牌</td>
						<td>车辆型号</td>
						<td>车架号</td>
						<td>当前电量</td>
						<td>可行驶里程</td>
						<td>车辆状态</td>
						<td>操作</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size: 12px;"
							<ww:if test="#rl.even"> class="trs"</ww:if>>
							<td align="center"><ww:property value="vehiclePlateId" /></td>
							<td align="center"><ww:property
									value="#dictUtil.getCnNameByGroupCodeAndDictId('10',carVehicleModel.brand)" /></td>
							<td align="center"><ww:property value="modelName" /></td>
							<td align="center"><ww:property value="vin" /></td>
							<td align="center"><ww:property value="" /></td>
							<td align="center"><ww:property value="" /></td>
							<td align="center"><ww:property
									value="#dictUtil.getCnNameByGroupCodeAndDictId('carBizState',bizState)" /></td>
							<td>
								<div class="pan_btn4"
									onclick="javascript:openThisLocation('<ww:property value="id"/>');">当前位置</div>
								<div class="pan_btn4"
									onclick="javascript:carControl('<ww:property value="id"/>','control');">车辆操控</div>
								<div class="pan_btn2"
									onclick="showLogRecordForDialog('<ww:property value="id" />','<%=path%>','车辆管理');">记录</div>
							</td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="10"><ww:property
								value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div>
</body>
</html>