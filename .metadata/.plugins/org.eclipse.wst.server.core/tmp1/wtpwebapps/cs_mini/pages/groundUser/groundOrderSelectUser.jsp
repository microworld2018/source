<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Insert title here</title>
<%@ include file="/pages/common/common_head.jsp"%>

<script type="text/javascript">
function searchEntity(){
	$("#sform").submit();
}

var api = frameElement.api, W = api.opener;
var branchDotEditDoc = W;
api.button({
    id:'valueOk',
	name:'确定',
	callback:ok,
	focus: true
});
api.button({
    id:'valueCancel',
	name:'取消'
});

function ok(){
		var str=$("input[name='checkdelcheckall']:checked").val();
		if(typeof(str)=="undefined" || str == null || str == ''){
			alert("请选择网点");
			return false;
		}
		if(str!=""){
			var branchDotDoc=str.split(",");
			branchDotEditDoc.$("#memberId").val(branchDotDoc[0]);
			branchDotEditDoc.$("#memberName").val(branchDotDoc[1]);
		}
};

function confirmTr(tr){
	var str = $(tr).find("input[name='checkdelcheckall']").val();
	if(str!=""){
		var rbdotinfos=str.split(",");
		branchDotEditDoc.$("#memberId").val(rbdotinfos[0]);
		branchDotEditDoc.$("#memberName").val(rbdotinfos[1]);
		
	}else{
		return false;
	}
	api.close();
}

</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
			<form class="form-horizontal" name="sform" id="sform" method="post" action="groundUser.action">
			
				<input type="hidden" name="page.orderFlag" id="page.orderFlag"
						value="<ww:property value="page.orderFlag"/>">
				<input type="hidden" name="page.orderString" id="page.orderString"
						value="<ww:property value="page.orderString"/>">
				<input type="hidden" name="state" value="order">

			<div class="ControlBlock">
						<div class="row SelectBlock">
							<div class="col-xs-6">
								<div class="form-group">
									<label for="carAccident.code" class="col-xs-5 control-label">地勤姓名</label>
									<div class="col-xs-7">
							    		<input class="form-control" name="groundUser.name"  type="text" >
							    	</div>
							    </div>
							    <!-- <div class="form-group">
									<label for="carAccident.plateNumber" class="col-xs-5 control-label">驾驶证号</label>
									<div class="col-xs-7">
							    		<input class="form-control" name="subscriber.drivingLicenseNo"  type="text" >
							    	</div>
							    </div> -->
						    </div>
						    <div class="col-xs-6">
								<div class="form-group">
									<label for="carAccident.orderCode" class="col-xs-5 control-label">手机号</label>
									<div class="col-xs-7">
							    		<input class="form-control" name="groundUser.phone"  type="text">
							    	</div>
							    </div>
						    </div>
						    
					    </div>
					    <div class="row SubmitButtonBlock">
							<div class="col-xs-4 col-xs-offset-4"><a class="btn btn-block Button1" onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a></div>
			  		    </div>
			</div>
			<div class="row TableBlock" id="branchDotListShowDiv">
				<table class="table table-striped table-bordered table-condensed">
						<tr class="ths" id="tab_bg_cl">
							<td  width="68" height="50">
							</td>
							<td >地勤姓名</td>
							<td >手机号</td>
							<!-- <td >驾驶证号</td> -->
							<td >人员状态</td>
						</tr>
						<ww:iterator value="page.results" id="data" status="rl">
							<tr ondblclick="confirmTr(this);"
								<ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
								<td align="center">
									<input type="radio" name="checkdelcheckall" 
									value="<ww:property value="id" />,<ww:property value="name" />">
								</td>
								<td align="left"><ww:property value="name" /></td>
								<td align="left"><ww:property value="phone" /></td>
								<%-- <td align="left"><ww:property value="drivingLicenseNo" /></td> --%>
								<td align="center">
									<ww:if test="isUse == 0">
										<span class="label label-success">正常</span>
									</ww:if>
									<ww:elseif test="isUse == 1">
										<span class="label label-primary">未使用</span>
									</ww:elseif>
								</td>
							</tr>
						</ww:iterator>
						<tr>
							<td align="center" colspan="9">
								<ww:property value="page.pageSplit" />	
							</td>
						</tr>
					</table>
				</div>
			</form>
</div>
</body>

</html>