<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>地勤人员列表</title>

<%@ include file="/pages/common/common_head.jsp"%>

<script type="text/javascript">
function searchEntity(){
	$('#sform').submit();		
}
function editEntity(id){
	var dialoguser = $.dialog({
	    id:'useredit', 
	    title:(id == "")?"新添地勤":"编辑地勤",
		content : 'url:<%=path%>/groundUser/groundUserGet.action?id='+id,
		fixed:true,
		width:1200,
		height:600,
		resize:false,
 		max: false,
	    min: false,
	    lock: true,
	    ok: function(){
	    	var valid = this.content.isValid();
	    	if (valid){
	    		var form = this.content.getForm();
	    		showLoading(parent);
	    		$.post(form.attr("action"),form.serialize(),r_savedata,'json').error(requestError);
	    	}
	    	return false;
	    },
	    okVal:isnull(id)?'添加':'保存',
	    cancelVal: '关闭',
	    cancel: true,
	    close: function () {
	        this.hide();
	        restoreInfo('hospitalinfo');
	        return true;
	    },
	    init: function(){
	    	if (typeof this.content.isError != 'undefined'){
	    		$(":button").slice(0,1).hide();
	    	}
	    }
	});
	
}

function r_savedata(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("保存成功！", function(){
				$('#sform').submit();	
		    });
			break;
		case 1:
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
	return false;
}
function deleteGroundUser(id){
	alertconfirm("删除后不可恢复，确认删除？",function (){
		showLoading();
		$.post('deleteGroundUser.action?id='+id,$('#sform').serialize(),r_delete,'json').error(requestError);
	});	
}
function r_delete(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("删除成功！", function(){
		    	$('#sform').submit();		
		    });
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}

function resetPassword(id){
	alertconfirm("确认要重置密？",function (){
		showLoading();
		$.post('<%=path%>/user/resetPassword.action?id='+id,$('#sform').serialize(),r_reset_password,'json').error(requestError);
	});	
}

function r_reset_password(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("重置成功！");
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}

/* 分配网点 */
<%-- function distributionDot(id){
	window.location.href="<%=path%>/userdot/getdotList.action?id="+id;
}

function showUserDotList(id){
	window.location.href="<%=path%>/userdot/lookUserDot.action?userDot.userId="+id;
} --%>

</script>
</head>
<body class="SubPage">
<div class="container-fluid">
		<form class="form-horizontal"  name="sform" id="sform" method="post" action="<%=path%>/groundUser/groundUserList.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag" value="<ww:property value="page.orderFlag"/>">
			<input type="hidden" name="page.orderString" id="page.orderString" value="<ww:property value="page.orderString"/>">
					
	<div class="ControlBlock">
		<div class="row SelectBlock">
			<div class="col-sm-4 col-xs-12">
				<div class="form-group">
					<label for="groundUser.name" class="col-xs-4 control-label">真实名称：</label>
					<div class="col-xs-8">
						<input class="form-control"name="groundUser.name" id="groundUser.name" type="text" value="<ww:property value="groundUser.name"/>" />
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-xs-12">
				<div class="form-group">
					<label for="groundUser.phone" class="col-xs-4 control-label"> 手机号：</label>
					<div class="col-xs-8">
						<input class="form-control"name="groundUser.phone" id="groundUser.phone" type="text" value="<ww:property value="groundUser.phone"/>" />
					</div>
				</div>
			</div>
			<div class="col-sm-4 col-xs-12">
				<div class="form-group">
					<label for="groundUser.subDotName" class="col-xs-4 control-label"> 所辖网点：</label>
					<div class="col-xs-8">
						<input class="form-control"name="groundUser.subDotName" id="groundUser.subDotName" type="text" value="<ww:property value="groundUser.subDotName"/>" />
					</div>
				</div>
			</div>
		</div>
		<div class="row SubmitButtonBlock">
			<div class="col-sm-2 col-sm-offset-3 col-xs-4">
				<a class="btn btn-block Button1"  onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a>
			</div>
			<ww:if test="hasPrivilegeUrl('/groundUser/groundUserGet.action')"><div class="col-sm-2 col-xs-4">
				<a class="btn btn-block Button2"  onclick="editEntity('');" target="_blank"><i class="fa fa-floppy-o"></i>添加</a>
			</div></ww:if>
			<%-- <ww:if test="hasPrivilegeUrl('/user/userDelete.action')"><div class="col-sm-2 col-xs-4">
				<a class="btn btn-block Button3"  onclick="deleteEntity('');" target="_blank"><i class="fa fa-trash-o"></i>删除</a>
			</div></ww:if> --%>
		</div>
	</div>
					
			
<div class="row TableBlock">
 				<table class="table table-striped table-bordered table-condensed">
						<tr class="ths" id="tab_bg_cl">
							<!-- <td>
								<input type="checkbox" name="checkdelcheckall"
													onclick="funCheck('','checkdel')">
							</td> -->
							<td>真实姓名</td>
							<td>手机号</td>
							<td>所辖网点</td>
							<td>操作</td>
						</tr>
				
						<ww:iterator value="groundUserList" id="data" status="rl">
							<tr
								<ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
								<%-- <td align="center">
									<input type="checkbox" name="checkdel" 
										value="<ww:property value="id"/>">
								</td> --%>
								<td align="left">
									<ww:property value="name" />
								</td>
								<td align="left">
									<ww:property value="phone" />
								</td>
								<td align="left">
									<ww:iterator value="subDotList">
										<ww:property value="dotName"/><br/>
									</ww:iterator>
								</td>
								<td align="center">
									<ww:if test="hasPrivilegeUrl('/groundUser/groundUserGet.action')">
										<div class="pan_btn3"  onclick="javascript:editEntity('<ww:property value="id"/>');">编辑</div>
									</ww:if>
									<ww:if test="hasPrivilegeUrl('/groundUser/deleteGroundUser.action')">
										<div class="pan_btn2"  onclick="javascript:deleteGroundUser('<ww:property value="id"/>');">删除</div>
									</ww:if>
								</td>
							</tr>
						</ww:iterator>
						<%-- <tr style="background-color: #fff;height: 30px;">
							<td align="center" colspan="5">
								<ww:property value="page.pageSplit" />	
							</td>
						</tr> --%>
					</table>
 			</div>
		</form>
</div>
</body>
</html>