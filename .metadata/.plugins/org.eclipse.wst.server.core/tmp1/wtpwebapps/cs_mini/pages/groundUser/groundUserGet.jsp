<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Insert title here</title>
<%@ include file="/pages/common/common_head.jsp"%>
<style type="text/css">
	input[type=text]{
		width:170px;
	}
</style>
<script type="text/javascript">

function saveEntity(id){
	
}

function r_savedata(data){

}
$().ready(function (){
	$("input[type=checkbox]").attr("class","");//清空checkedbox的样式
	var id = '<ww:property value="id" />';
	if (id == ""){
		$("#eform").attr('action','<%=path%>/groundUser/groundUserAdd.action');
	}else{
		$("#eform").attr('action','<%=path%>/groundUser/groundUserUpdate.action');
		$("#groundUserPhone").attr("readonly",true);
	}
	$('#eform').validate({
		errorClass : 'text-danger',
		rules: {
			"groundUser.phone": {
				required: true,
				maxlength : 11
			},
			"groundUser.password":{
				required: true,
				maxlength : 60
			},
			"groundUser.name":{
				required: true,
				maxlength : 60
			}
		},
		messages: {
			"groundUser.phone": {
				required: "请输入登陆手机号！",
				maxlength: "手机号过长，最大为11个字符！"
			},
			"groundUser.password":{
				required: "请输入密码！",
				maxlength : "密码过长，最大为60个字符！"
			},"groundUser.name":{
				required: "请输入真实姓名！",
				maxlength: "姓名过长，最大为60个字符！"
			}
		}
	});
	var subDot = document.getElementsByName('branchDotDoc');//所有网点
    var subDotList = ${subDotList};//用户绑定网点	
   	for(var j = 0,l = subDotList.length;j < l;j++){
    	for(var i = 0;i < subDot.length;i++){
	    	if(subDot[i].value == subDotList[j].dotId){
				subDot[i].checked = true;
    		}
    	}
	} 
	
});

function isValid(){
	if ($("#eform").valid()){
		return true;
	}else{
		return false;
	}
}
function getForm(){
	return $("#eform");
}
</script>
</head>
<body >
	<div class="table_con tanchuang" >
		<form name="eform" id="eform" method="post" action="">
			<input type="hidden" name="groundUser.id" id="groundUser.id" value="<ww:property value="groundUser.id" />">
			<table class="t1" >
				<tr class="trr" >
					<th >
						<span >登录手机号：</span>
					</th>
					<td>
						<input type="text" id="groundUserPhone" name="groundUser.phone" class="input_size" 
						       value="<ww:property value="groundUser.phone" />"/>
						<span class="rr red">*</span>
		    		</td> 
				</tr>
				<tr class="trr" >
					<th >
						<span >密码：</span>
					</th>
					<td><input type="text" name="groundUser.password" id="groundUser.password"  class="input_size"  value="<ww:property value="groundUser.password" />"/><span class="rr red">*</span>
		    		</td> 
				</tr>
				<tr class="trr" >
					<th >
						<span >真实姓名：</span>
					</th>
					<td><input type="text" name="groundUser.name" id="groundUser.name"  class="input_size"  value="<ww:property value="groundUser.name" />"/>
		    		</td> 
				</tr>
				
				
				<tr class="trr" >
					<th >
						<span >所辖网点：</span>
					</th>
					<td>
						<select name="areas.id" id="areasSelect" style="width: 170px;" disabled="disabled">
							<option value="">请在下方选择所辖网点</option>
							<%-- <ww:iterator value="areas">
								<option value='<ww:property value="id"/>'  <ww:if test="areas.id==id">selected="selected"</ww:if> ><ww:property value="name"/></option>
							</ww:iterator> --%>  
						</select>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<table class="table table-striped table-bordered table-condensed">
							<tr class="ths" id="tab_bg_cl">
								<td></td>
								<td align="center">
									<%-- <a href="javascript:SetOrder('code')">编码<img src="<%=path%>/admin/common/images/main/paixu.png"/></a> --%>
									编码
								</td>
								<td align="center">
									<%-- <a href="javascript:SetOrder('name')">名称<img src="<%=path%>/admin/common/images/main/paixu.png"/></a> --%>
									名称
								</td>
								<td align="center">地址</td>
								<td align="center">是否启用</td>
								<td align="center">行政区划</td>
								<td align="center">还车网点</td>
								<td align="center">车位数量</td>
							</tr>
					
							<ww:iterator value="#request.branchDotList" id="dotData" status="rl">
								<tr <ww:if test="#rl.even"> class="trs"</ww:if> ondblclick="confirmTr(this)" style="font-size:12px;" >
									<td align="center">
										<input type="checkbox" name="branchDotDoc" 
												value="<ww:property value="id" />" />
									</td>
									<td align="left">
										<ww:property value="code" />
									</td>
									<td align="center">
										<ww:property value="name" />
									</td>
									<td align="center">
										<ww:property value="address" />
									</td>
									<td align="center">
										<ww:if test="isActive==1">是</ww:if>
										<ww:if test="isActive==0">否</ww:if>
									</td>
									<td align="center">
										<ww:property value="getAreaName(areaId)" />
									</td>
									<td align="center">
										<ww:if test="returnbackDotName == null || returnbackDotName == ''">本地</ww:if>
										<ww:else><ww:property value="returnbackDotName"/></ww:else>
									</td>
									<td align="center">
										<ww:property value="totalParkingPlace" />
									</td>
								</tr>
							</ww:iterator>
							<%-- <tr>
								<td align="right" colspan="8">
									<ww:property value="page.pageSplit" />	
								</td>
							</tr> --%>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</div>
</body>
</html>