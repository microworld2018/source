<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.sys.util.DictUtil'" id="dictUtil" />
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<ww:bean name="'com.dearho.cs.util.StringHelper'" id="stringHelper" />
<%@ page import="com.dearho.cs.sys.util.DictUtil" language="java" %>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>推送通知列表</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
$(function(){
	
});
/* 根据条件查询保险规则 */
function searchEntity(){
	$("#sform").submit();
}
/* 删除保险基础规则 */
function del(id,name){
	alertconfirm("确认删除（"+name+"）保险规则吗？",function (){
		showLoading();
		$.post('delInsuranceRules.action',{"insuranceRules.id":id},r_delete,'json').error(requestError);
	});	
}
/* 删除结果反馈 */
function r_delete(data){
	hideLoading();
	switch(data.result){
		case 0:
			alertok("删除成功！", function(){
				$("#sform").submit();
		    });
			break;
		case 1:
			restoreInfo();
			alerterror(data.msg);
			break;
		case 9:
			document.location = "doError.action";
			break;
	}
}
/* 打开新建或者编辑通知页面 */
function saveInusurance(id,state){
	window.location.href="<%=path%>/pushNotification/getByIdPushNotification.action?id="+id+"&state="+state;
}
/* 查看保险操作记录 */
function log(id){
// 	dialoguser = $.dialog({
// 	    id:'insuranceOperatingLog', 
// 	    title:'保险操作记录',
<%-- 		content : 'url:<%=path%>/pushNotification/toViewInsuranceOperating.action?id='+id, --%>
// 		fixed:true,
// 		width:800,
// 		height:550,
// 		resize:false,
//  		max: false,
// 	    min: false,
// 	    lock: true,
// 	    close: function () {
// 	        this.hide();
// 	        restoreInfo('hospitalinfo');
// 	        return true;
// 	    },
// 	    init: function(){
// 	    	if (typeof this.content.isError != 'undefined'){
// 	    		$(":button").slice(0,1).hide();
// 	    	}
// 	    }
// 	});

$.ajax({
	  type: 'POST',
	  url: '<%=path%>/pushNotification/toViewInsuranceOperating.action',
	  data:{"id":id},
	  dataType: 'json',
	  success: function(data){
		  if(data.result == 0){
			  alertok("推送成功！", function(){
				  window.location.href="<%=path %>/pushNotification/pushNotification.action";
			    });
		  }else{
			  alertinfo("操作失败！");
		  }
	  }, error: function(data){
		  alert("服务器异常");
	  }
	});
}
</script>
</head>
<body class="SubPage">
	<div class="container-fluid">
		<form class="form-horizontal" name="sform" id="sform" method="post" action="<%=path%>/pushNotification/pushNotification.action">
			<%-- <input type="hidden" name="page.orderFlag" id="page.orderFlag" value="<ww:property value="page.orderFlag"/>"> 
			<input type="hidden" name="page.orderString" id="page.orderString" value="<ww:property value="page.orderString"/>"> --%>
			<div class="ControlBlock">
			<div class="row SelectBlock">
				<div class="col-xs-4">
					<div class="form-group">
						<label for="pushNotification.title" class="col-xs-4 control-label">通知标题:</label>
						<div class="col-xs-8" style="padding-left:5px;">
							<input type="text" class="form-control" name="pushNotification.title" value="<ww:property value="pushNotification.title"/>">
						</div>
					</div>
				</div>
				<div class="col-xs-3">
				  <a class="btn btn-block Button1" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" onclick="searchEntity();"
						target="_blank"> <i class="fa fa-search"></i>查询
					</a>
				</div>
				<div class="col-xs-3">
				  	<a class="btn btn-block Button9" style="background: rgb(34, 197, 153);color: rgb(255, 255, 255); display: block; width: 100%;" onclick="saveInusurance('<ww:property value="id"/>','save');"
						target="_blank"> <i class="fa fa-plus-circle"></i>新建通知
					</a>
				</div>
			</div>
		</div>

			<div class="row TableBlock">
				<table class="table table-striped table-bordered table-condensed" >
					<tr class="ths" id="tab_bg_cl">
						<td>通知标题</td>
						<td>通知内容</td>
						<td>通知类型</td>
						<td>创建时间</td>
						<td>更新时间（最近一次）</td>
						<td>推送次数</td>
						<td>操作</td>
					</tr>
					<ww:iterator value="page.results" id="data" status="rl">
						<tr style="font-size:12px;" >
							<td align="center">
								<ww:property value="title" />
							</td>
							<td align="center" >
								<ww:property value="#stringHelper.subString(content)" />
							</td>
							<td align="center">
								<ww:if test="type == 1">活动通知</ww:if>
							</td>
							<td align="center">
								<ww:property value="#dateUtil.formatDate(createTime,'yyyy-MM-dd HH:mm:ss')" />
							</td>
							<td align="center">
								<ww:property value="#dateUtil.formatDate(updateTime,'yyyy-MM-dd HH:mm:ss')" />
							</td>
							<td align="center">
								<ww:property value="pushCount"/>
							</td>
							<td align="center">
								<div class="pan_btn4"  onclick="saveInusurance('<ww:property value="id"/>','update');">编辑</div> 
								<div class="pan_btn4"  onclick="log('<ww:property value="id"/>');">推送</div> 
							</td>
						</tr>
					</ww:iterator>
					<tr>
						<td align="right" colspan="10"><ww:property value="page.pageSplit" /></td>
					</tr>
				</table>
			</div>
		</form>
	</div> 
</body>
</html>