<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@  taglib prefix="ww" uri="webwork"%>
<ww:bean name="'com.dearho.cs.util.DateUtil'" id="dateUtil" />
<%
String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Insert title here</title>
<%@ include file="/pages/common/common_head.jsp"%>
<script type="text/javascript">
function searchEntity(){
	$('#sform').submit();		
}
</script>
</head>
<body class="SubPage">
<div class="container-fluid">
		<form class="form-horizontal"  name="sform" id="sform" method="post" action="<%=path %>/appRunLog/getAppRunLog.action">
			<input type="hidden" name="page.orderFlag" id="page.orderFlag"
					value="<ww:property value="page.orderFlag"/>">
			<input type="hidden" name="page.orderString" id="page.orderString"
					value="<ww:property value="page.orderString"/>">
					<div class="ControlBlock">
		<div class="row SelectBlock">
			<div class="col-sm-4 col-xs-12">
				<div class="form-group">
					<label for="sEntity.name" class="col-xs-4 control-label">订单编号</label>
					<div class="col-xs-8">
						<input class="form-control"name="runLog.orderOn" type="text" value="<ww:property value="runLog.orderOn"/>" />
					</div>
				</div>
				
			</div>
			<div class="col-sm-4 col-xs-12">
				<div class="form-group">
					<label for="sEntity.name" class="col-xs-4 control-label">车牌号</label>
					<div class="col-xs-8">
						<input class="form-control"name="runLog.licensePlateNumber"  type="text" value="<ww:property value="runLog.licensePlateNumber"/>" />
					</div>
				</div>
				
			</div>
		</div>
		<div class="row SubmitButtonBlock">
			<div class="col-sm-2 col-sm-offset-3 col-xs-4">
				<a class="btn btn-block Button1"  onclick="searchEntity();" target="_blank"><i class="fa fa-search"></i>查询</a>
			</div>
		</div>
	</div>
<div class="row TableBlock">
 				<table class="table table-striped table-bordered table-condensed">
						<tr class="ths" id="tab_bg_cl">
							<td>
								<input type="checkbox" name="checkdelcheckall"
													onclick="funCheck('','checkdel')">
							</td>
							
							<td style="width: 10%">
								订单编号
							</td>
							<td  style="width: 10%">
								用户名
							</td>
							<td  style="width: 10%">
								登录IP
							</td>
							<td  style="width: 10%">
								车牌号
							</td>
							<td  style="width: 42%">
								操作内容
							</td>
							<td  style="width: 13%">
								操作时间
							</td>
						</tr>
						<ww:iterator value="page.results" id="data" status="rl">
							<tr
								
								 <ww:if test="#rl.even"> class="trs"</ww:if> style="font-size:12px;">
								<td align="center" >
									<input type="checkbox" name="checkdel" 
										value="<ww:property value="id"/>">
								</td>
								<td align="left">
								<a href="javascript:showOrderDetailByNoForDialog('<ww:property value="orderOn" />','<%=path%>')"><ww:property value="orderOn" /></a>
								</td>
								<td align="left">
									<ww:property value="operatorName" />
								</td>
								<td align="left">
									<ww:property value="operatorIp" />
								</td>
								<td align="left">
									<ww:property value="licensePlateNumber" />
								</td>
								<td align="left">
									<ww:property value="operatorContent" />
								</td>
								<td align="left">
									<ww:property value="transDateString(createDate)" />
								</td>
								
							</tr>
						</ww:iterator>
						<tr style="background-color: #fff;height: 30px;">
							<td align="center" colspan="100">
								<ww:property value="page.pageSplit" />	
							</td>
						</tr>
					</table>
 			</div>
		</form>
</div>
</body>
</html>